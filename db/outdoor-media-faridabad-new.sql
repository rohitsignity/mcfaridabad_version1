-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 02, 2018 at 12:36 PM
-- Server version: 5.6.33-0ubuntu0.14.04.1
-- PHP Version: 5.6.37-1+ubuntu14.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `outdoor-media-faridabad-new`
--

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE IF NOT EXISTS `addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_code` varchar(255) NOT NULL COMMENT 'belongs to company',
  `type` enum('mailing','permanent') NOT NULL DEFAULT 'mailing',
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `pincode` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'used to track active/blocked record',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `address_profes`
--

CREATE TABLE IF NOT EXISTS `address_profes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `address_profes`
--

INSERT INTO `address_profes` (`id`, `name`) VALUES
(1, 'Voter id'),
(2, 'Aadhaar card'),
(3, 'Ration Card'),
(4, 'Driving License'),
(5, 'Passport');

-- --------------------------------------------------------

--
-- Table structure for table `annual_turnovers`
--

CREATE TABLE IF NOT EXISTS `annual_turnovers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `annual_turnover` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `annual_turnovers`
--

INSERT INTO `annual_turnovers` (`id`, `annual_turnover`, `created`, `modified`) VALUES
(1, '0- 5 cr', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, '5-10 cr', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, '10-25 cr', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'greater than 25 cr', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `applied_bids`
--

CREATE TABLE IF NOT EXISTS `applied_bids` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `tender_id` int(11) NOT NULL,
  `status` enum('0','1','2','3') NOT NULL DEFAULT '0' COMMENT '0 => ''processing'',1 => ''correct'', 2 => ''Not Applied'', 3=> ''Incorrect''',
  `paid` enum('0','1') NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `applied_bids_documents`
--

CREATE TABLE IF NOT EXISTS `applied_bids_documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applied_bid_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `document` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `awards`
--

CREATE TABLE IF NOT EXISTS `awards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bid_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `bids`
--

CREATE TABLE IF NOT EXISTS `bids` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `amount` decimal(65,2) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tender_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `parent_id` int(255) NOT NULL,
  `type` varchar(255) NOT NULL DEFAULT '0' COMMENT '0 => Public, 1 => Private',
  `guest_enabled` tinyint(4) NOT NULL,
  `sort` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='saved parent & sub categories' AUTO_INCREMENT=36 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `parent_id`, `type`, `guest_enabled`, `sort`, `status`, `created`, `modified`) VALUES
(1, 'Type A/OMDs on public transport services / street furniture and public transport system', 0, '0', 0, 0, '1', '2016-12-27 16:06:14', '2016-12-27 16:06:14'),
(2, 'Type B/Advertising Outdoor Media Device on Public Transport System', 0, '0', 0, 0, '0', '2016-12-27 16:06:14', '2016-12-27 16:06:14'),
(3, 'Type B/Advertising Outdoor Media Device on Commercial Advertising Structure on Public Land', 0, '0', 0, 0, '1', '2016-12-27 16:06:14', '2016-12-27 16:06:14'),
(4, 'Type C/Advertising Outdoor Media Device on Commercial Advertising Structure on Private Land', 0, '1', 0, 0, '1', '2016-12-27 16:06:14', '2016-12-27 16:06:14'),
(5, 'Type D/Events', 0, '0,1', 1, 0, '1', '2016-12-27 16:06:14', '2016-12-27 16:06:14'),
(6, 'Type E/Landscape Advertising', 0, '0', 0, 0, '1', '2016-12-27 16:06:14', '2016-12-27 16:06:14'),
(7, 'Type F/Shop Signage', 0, '0,1', 0, 0, '1', '2016-12-27 16:06:14', '2016-12-27 16:06:14'),
(8, 'Type G/Innovative Advertising', 0, '0,1', 0, 0, '1', '2016-12-27 16:06:14', '2016-12-27 16:06:14'),
(9, 'Type H/Cinema Advertising', 0, '0,1', 0, 0, '1', '2016-12-27 16:06:14', '2016-12-27 16:06:14'),
(10, 'Type I/Inside Commercial Buildings and public Buildings', 0, '0', 0, 0, '1', '2016-12-27 16:06:14', '2016-12-27 16:06:14'),
(11, 'Bus and IPT Shelters', 1, '0', 0, 0, '1', '2016-12-27 16:08:08', '2016-12-27 16:08:08'),
(12, 'Bus and IPT Route Markers', 1, '0', 0, 0, '1', '2016-12-27 16:08:08', '2016-12-27 16:08:08'),
(13, 'Foot over bridge, Signage gantries, Toilet Blocks, Urinals', 1, '0', 0, 0, '1', '2016-12-27 16:08:08', '2016-12-27 16:08:08'),
(14, 'Cycle station', 1, '0', 0, 0, '1', '2016-12-27 16:08:08', '2016-12-27 16:08:08'),
(15, 'Police booth, Parking booth, Telephone booth, Prepaid taxi Booth, Vending kiosks, drinking water facility, bus/train booking facility, Outside colonies to facilitate directory/payment of bills', 1, '0', 0, 0, '1', '2016-12-27 16:08:08', '2016-12-27 16:08:08'),
(16, 'Sitting Bench, Garbage bin', 1, '0', 0, 0, '1', '2016-12-27 16:08:08', '2016-12-27 16:08:08'),
(17, 'Metro/MRTS', 1, '0', 0, 0, '1', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(18, 'Traffic Barricading', 1, '0', 0, 0, '1', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(19, 'Public transport vehicle', 1, '0', 0, 0, '1', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(20, 'Hoarding', 3, '0', 0, 0, '1', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(21, 'Wall Wraps', 3, '0', 0, 0, '1', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(22, 'Billboards', 4, '0', 0, 0, '0', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(23, 'Wall Wraps', 4, '0', 0, 1, '1', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(24, 'Religious, Political and Conferences', 5, '0', 0, 0, '1', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(25, 'Tree Guards', 6, '0', 0, 0, '1', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(26, 'Self Advertising', 7, '0', 0, 0, '1', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(27, 'Innovative Advertising', 8, '0', 0, 0, '1', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(28, 'In Cinema on screen Advertising including slides and advertisement flims', 9, '0', 0, 0, '1', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(29, 'Inside Commercial Buldings and public Buldings', 10, '0', 0, 0, '1', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(30, 'Unipoles', 4, '0', 0, 0, '0', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(31, 'Building Boards', 4, '0', 0, 0, '0', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(32, 'Others', 1, '0', 0, 0, '1', '2018-01-11 12:00:00', '2018-01-11 12:00:00'),
(33, 'Entertainment and Exhibitions events', 5, '0', 0, 0, '1', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(34, 'Unipoles, billboards, building boards', 4, '0', 0, 0, '1', '2018-05-31 12:54:00', '2018-05-31 12:54:00'),
(35, 'Multiple OMDs', 4, '0', 0, 2, '1', '2018-05-31 12:58:00', '2018-05-31 12:58:00');

-- --------------------------------------------------------

--
-- Table structure for table `category_fields`
--

CREATE TABLE IF NOT EXISTS `category_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(255) NOT NULL,
  `field_id` int(255) NOT NULL,
  `sort_order` int(255) NOT NULL DEFAULT '0',
  `custom_validation` longtext NOT NULL COMMENT 'JSON Format. Eg: [{"minValue":10,"error":"error message here"},{"maxValue":50,"error":"error message here"}]',
  `custom_calculations` longtext NOT NULL COMMENT 'JSON Format. Eg: {"fields":["filedKey1","filedKey2"],"operation":"multiply"}',
  `custom_disable` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=465 ;

--
-- Dumping data for table `category_fields`
--

INSERT INTO `category_fields` (`id`, `category_id`, `field_id`, `sort_order`, `custom_validation`, `custom_calculations`, `custom_disable`) VALUES
(6, 11, 6, 3, '', '', '0'),
(7, 11, 7, 6, '', '', '0'),
(8, 11, 30, 7, '', '', '0'),
(9, 17, 68, 0, '', '', '0'),
(15, 17, 4, 6, '', '', '0'),
(16, 17, 5, 7, '', '', '0'),
(17, 17, 3, 12, '', '', '0'),
(18, 17, 7, 9, '', '', '0'),
(19, 17, 30, 10, '', '', '0'),
(21, 18, 11, 0, '', '', '0'),
(22, 18, 12, 1, '', '', '0'),
(23, 18, 4, 2, '', '', '0'),
(24, 18, 5, 3, '', '', '0'),
(25, 18, 3, 7, '', '', '0'),
(26, 18, 30, 5, '', '', '0'),
(27, 18, 6, 6, '', '', '0'),
(28, 19, 13, 0, '', '', '0'),
(29, 19, 14, 1, '', '', '0'),
(30, 20, 15, 0, '', '', '0'),
(32, 20, 17, 2, '', '', '0'),
(33, 20, 18, 3, '', '', '0'),
(34, 20, 12, 4, '{"operationalValidation": { "field" : ["total_facade_area"], "operator" : "*","defaultValue" : "0.75" ,"unit" : "Facade", "oprand" : "greater", "title": "Total Advertising Space" }}', '{"fields":["omd_height","omd_width"],"operation":"multiply","defaultValue" : "1"}', '1'),
(35, 20, 4, 5, '', '', '0'),
(36, 20, 5, 6, '', '', '0'),
(37, 20, 3, 11, '', '', '0'),
(38, 20, 7, 8, '', '', '0'),
(39, 20, 6, 9, '', '', '0'),
(40, 20, 30, 10, '', '', '0'),
(41, 22, 15, 1, '', '', '0'),
(43, 22, 17, 2, '', '', '0'),
(44, 22, 18, 3, '', '', '0'),
(45, 22, 12, 4, '{"operationalValidation": { "field" : ["total_facade_area"], "operator" : "*","defaultValue" : "0.75" ,"unit" : "Facade", "oprand" : "greater", "title": "Total Advertising Space" }}', '{"fields":["omd_height","omd_width"],"operation":"multiply","defaultValue" : "1"}', '1'),
(46, 22, 4, 9, '', '', '0'),
(47, 22, 5, 10, '', '', '0'),
(48, 22, 3, 11, '', '', '0'),
(49, 22, 7, 8, '', '', '0'),
(51, 22, 19, 7, '', '{"fields":["omd_height","omd_width","omd_price"],"operation":"multiply","defaultValue" : "12"}', '1'),
(52, 33, 20, 0, '', '', '0'),
(53, 33, 21, 1, '{"operationalValidation": {"field": ["event_duration"],"operator": "zero","defaultValue": "30","unit": "Days","oprand": "greater","title": "Duration of Event"}}', '', '0'),
(54, 33, 22, 3, ' {"depend":{"omd_no_others":{"type":"text","value":[],"message":"Please select one option","default":"","fieldType": "text"}}} ', '', '0'),
(55, 33, 23, 4, ' {"depend":{"omd_no_venue":{"type":"text","value":[],"message":"Please select one option","default":"","fieldType": "text"}}} ', '', '0'),
(56, 33, 7, 2, '', '', '0'),
(58, 25, 11, 0, '', '', '0'),
(59, 25, 24, 1, '', '', '0'),
(60, 25, 4, 2, '', '', '0'),
(61, 25, 5, 3, '', '', '0'),
(62, 25, 3, 8, '', '', '0'),
(63, 25, 7, 5, '', '', '0'),
(64, 25, 6, 6, '', '', '0'),
(65, 25, 30, 7, '', '', '0'),
(66, 26, 25, 0, '', '', '0'),
(79, 28, 27, 0, '', '', '0'),
(81, 28, 3, 7, '', '', '0'),
(82, 28, 105, 3, '', '{"fields": ["no_of_screens_applied", "tenure", "discount"],"operation": "bodmas","equation": {"multiply": ["defaultValue", "no_of_screens_applied", "tenure"],"percentageDiscount": ["discount"]},"defaultValue": "25000"}', '1'),
(83, 29, 29, 0, '', '', '0'),
(85, 29, 3, 6, '', '', '0'),
(88, 12, 85, 0, '', '', '0'),
(93, 12, 6, 5, '', '', '0'),
(94, 12, 7, 6, '', '', '0'),
(95, 12, 30, 7, '', '', '0'),
(96, 13, 86, 0, '', '', '0'),
(101, 13, 6, 5, '', '', '0'),
(102, 13, 7, 6, '', '', '0'),
(103, 13, 30, 7, '', '', '0'),
(104, 14, 87, 0, '', '', '0'),
(109, 14, 6, 5, '', '', '0'),
(110, 14, 7, 6, '', '', '0'),
(111, 14, 30, 7, '', '', '0'),
(112, 15, 88, 0, '', '', '0'),
(117, 15, 6, 5, '', '', '0'),
(118, 15, 7, 6, '', '', '0'),
(119, 15, 30, 7, '', '', '0'),
(120, 16, 89, 0, '', '', '0'),
(125, 16, 6, 5, '', '', '0'),
(126, 16, 7, 6, '', '', '0'),
(127, 16, 30, 7, '', '', '0'),
(128, 21, 15, 0, '', '', '0'),
(130, 21, 17, 2, '', '', '0'),
(131, 21, 18, 3, '', '', '0'),
(132, 21, 12, 4, '{"operationalValidation": { "field" : ["total_facade_area"], "operator" : "*","defaultValue" : "0.75" ,"unit" : "Facade", "oprand" : "greater", "title": "Total Advertising Space" }}', '{"fields":["omd_height","omd_width"],"operation":"multiply","defaultValue" : "1"}', '1'),
(133, 21, 4, 5, '', '', '0'),
(134, 21, 5, 6, '', '', '0'),
(135, 21, 3, 11, '', '', '0'),
(136, 21, 7, 8, '', '', '0'),
(137, 21, 6, 9, '', '', '0'),
(138, 21, 30, 10, '', '', '0'),
(139, 23, 15, 1, '', '', '0'),
(141, 23, 17, 2, '', '', '0'),
(142, 23, 18, 3, '', '', '0'),
(143, 23, 12, 4, '{"operationalMultyValidation": {"defaultValue": "0.2","field": ["total_facade_area"],"0": {"operator": "*","defaultValue": "0.20","unit": "Facade","oprand": "greater","title": "Total Advertising Space"}}}', '{"fields":["omd_height","omd_width"],"operation":"multiply","defaultValue" : "1"}', '1'),
(144, 23, 4, 9, '', '', '0'),
(145, 23, 5, 10, '', '', '0'),
(146, 23, 3, 11, '', '', '0'),
(147, 23, 7, 8, '', '', '0'),
(149, 23, 105, 7, '', '{"fields": ["omd_height","omd_width", "tenure", "discount"],"operation": "bodmas","equation": {"multiply": ["defaultValue", "omd_height","omd_width","tenure"],"percentageDiscount": ["discount"]},"defaultValue": "3000"}', '1'),
(150, 11, 33, 8, '', '', '0'),
(151, 11, 98, 9, '', '', '0'),
(152, 11, 97, 10, '', '', '0'),
(153, 11, 36, 11, '', '', '0'),
(154, 11, 96, 12, '', '', '0'),
(157, 12, 33, 8, '', '', '0'),
(158, 12, 98, 9, '', '', '0'),
(159, 12, 97, 10, '', '', '0'),
(160, 12, 36, 11, '', '', '0'),
(161, 12, 96, 12, '', '', '0'),
(164, 13, 33, 8, '', '', '0'),
(165, 13, 98, 9, '', '', '0'),
(166, 13, 97, 10, '', '', '0'),
(167, 13, 36, 11, '', '', '0'),
(168, 13, 96, 12, '', '', '0'),
(171, 14, 33, 8, '', '', '0'),
(172, 14, 98, 9, '', '', '0'),
(173, 14, 97, 10, '', '', '0'),
(174, 14, 36, 11, '', '', '0'),
(175, 14, 96, 12, '', '', '0'),
(178, 15, 33, 8, '', '', '0'),
(179, 15, 98, 9, '', '', '0'),
(180, 15, 97, 10, '', '', '0'),
(181, 15, 36, 11, '', '', '0'),
(182, 15, 96, 12, '', '', '0'),
(185, 16, 33, 8, '', '', '0'),
(186, 16, 98, 9, '', '', '0'),
(187, 16, 97, 10, '', '', '0'),
(188, 16, 36, 11, '', '', '0'),
(189, 16, 96, 12, '', '', '0'),
(192, 17, 33, 12, '', '', '0'),
(193, 17, 98, 13, '', '', '0'),
(194, 17, 97, 14, '', '', '0'),
(195, 17, 36, 15, '', '', '0'),
(196, 17, 96, 16, '', '', '0'),
(199, 18, 33, 7, '', '', '0'),
(200, 18, 98, 8, '', '', '0'),
(201, 18, 97, 9, '', '', '0'),
(202, 18, 36, 10, '', '', '0'),
(203, 18, 96, 11, '', '', '0'),
(206, 19, 33, 2, '', '', '0'),
(207, 19, 98, 3, '', '', '0'),
(208, 19, 97, 4, '', '', '0'),
(209, 19, 36, 5, '', '', '0'),
(210, 19, 96, 6, '', '', '0'),
(213, 20, 33, 11, '', '', '0'),
(214, 20, 34, 12, '', '', '0'),
(215, 20, 35, 13, '', '', '0'),
(216, 20, 36, 14, '', '', '0'),
(217, 20, 37, 15, '', '', '0'),
(220, 21, 33, 11, '', '', '0'),
(221, 21, 34, 12, '', '', '0'),
(222, 21, 35, 13, '', '', '0'),
(223, 21, 36, 14, '', '', '0'),
(224, 21, 37, 15, '', '', '0'),
(227, 22, 33, 12, '', '', '0'),
(228, 22, 34, 13, '', '', '0'),
(229, 22, 35, 14, '', '', '0'),
(230, 22, 36, 15, '', '', '0'),
(231, 22, 37, 16, '', '', '0'),
(234, 23, 33, 12, '', '', '0'),
(235, 23, 34, 13, '', '', '0'),
(236, 23, 35, 14, '', '', '0'),
(237, 23, 36, 15, '', '', '0'),
(238, 23, 37, 16, '', '', '0'),
(242, 33, 34, 8, '', '', '0'),
(243, 33, 35, 7, '', '', '0'),
(245, 33, 37, 10, '', '', '0'),
(248, 25, 33, 8, '', '', '0'),
(249, 25, 34, 9, '', '', '0'),
(250, 25, 35, 10, '', '', '0'),
(251, 25, 36, 11, '', '', '0'),
(252, 25, 37, 12, '', '', '0'),
(255, 26, 33, 1, '', '', '0'),
(256, 26, 34, 2, '', '', '0'),
(257, 26, 35, 3, '', '', '0'),
(258, 26, 36, 4, '', '', '0'),
(259, 26, 37, 5, '', '', '0'),
(267, 30, 15, 1, '', '', '0'),
(268, 30, 17, 2, '', '', ''),
(269, 30, 18, 3, '', '', ''),
(270, 30, 12, 4, '{"operationalValidation": { "field" : ["total_facade_area"], "operator" : "*","defaultValue" : "0.75" ,"unit" : "Facade", "oprand" : "greater", "title": "Total Advertising Space" }}', '{"fields":["omd_height","omd_width"],"operation":"multiply","defaultValue" : "1"}', '1'),
(271, 30, 4, 9, '', '', ''),
(272, 30, 5, 10, '', '', ''),
(273, 30, 3, 11, '', '', ''),
(274, 30, 7, 8, '', '', ''),
(276, 30, 19, 7, '', '{"fields":["omd_height","omd_width","omd_price"],"operation":"multiply","defaultValue" : "12"}', '1'),
(277, 30, 33, 12, '', '', ''),
(278, 30, 34, 13, '', '', ''),
(279, 30, 35, 14, '', '', ''),
(280, 30, 36, 15, '', '', ''),
(281, 30, 37, 16, '', '', '0'),
(282, 31, 15, 1, '', '', '0'),
(283, 31, 17, 2, '', '', ''),
(284, 31, 18, 3, '', '', ''),
(285, 31, 12, 4, '{"operationalValidation": { "field" : ["total_facade_area"], "operator" : "*","defaultValue" : "0.75" ,"unit" : "Facade", "oprand" : "greater", "title": "Total Advertising Space" }}', '{"fields":["omd_height","omd_width"],"operation":"multiply","defaultValue" : "1"}', '1'),
(286, 31, 4, 9, '', '', ''),
(287, 31, 5, 10, '', '', ''),
(288, 31, 3, 11, '', '', ''),
(289, 31, 7, 8, '', '', ''),
(291, 31, 19, 7, '', '{"fields":["omd_height","omd_width","omd_price"],"operation":"multiply","defaultValue" : "12"}', '1'),
(292, 31, 33, 12, '', '', ''),
(293, 31, 34, 13, '', '', ''),
(294, 31, 35, 14, '', '', '0'),
(295, 31, 36, 15, '', '', ''),
(296, 31, 37, 16, '', '', '0'),
(297, 29, 4, 4, '', '', '0'),
(298, 29, 5, 5, '', '', '0'),
(299, 22, 60, 0, '', '', '0'),
(300, 30, 60, 0, '', '', '0'),
(301, 31, 60, 0, '', '', '0'),
(302, 23, 103, 0, '', '', '0'),
(303, 22, 63, 6, '', '{"fields":["omd_type_list"],"operation":"onSelect","options":{"Small Portraits":"3200","Posters":"3750","Super 8’s":"3750","Portraits":"4300","Supersites":"4800","Spectacular":"4800"},"defaultValue" : ""}', '0'),
(305, 30, 63, 6, '', '{"fields":["omd_type_list"],"operation":"onSelect","options":{"Small Portraits":"3200","Posters":"3750","Super 8’s":"3750","Portraits":"4300","Supersites":"4800","Spectacular":"4800"},"defaultValue" : ""}', '0'),
(306, 31, 63, 6, '', '{"fields":["omd_type_list"],"operation":"onSelect","options":{"Small Portraits":"3200","Posters":"3750","Super 8’s":"3750","Portraits":"4300","Supersites":"4800","Spectacular":"4800"},"defaultValue" : ""}', '0'),
(307, 22, 61, 5, '', '', '0'),
(308, 23, 61, 5, '', '', '0'),
(309, 30, 61, 5, '', '', '0'),
(310, 31, 61, 5, '', '', '0'),
(311, 29, 105, 3, '', '', '1'),
(312, 22, 64, 17, '', '', '0'),
(313, 23, 64, 17, '', '', '0'),
(314, 30, 64, 17, '', '', '0'),
(315, 31, 64, 17, '', '', '0'),
(316, 29, 36, 7, '', '', '0'),
(317, 29, 64, 8, '', '', '0'),
(318, 22, 30, 8, '', '', '0'),
(319, 23, 30, 8, '', '', '0'),
(320, 30, 30, 8, '', '', '0'),
(321, 31, 30, 8, '', '', '0'),
(322, 29, 30, 3, '', '', '0'),
(323, 29, 33, 9, '', '', '0'),
(324, 29, 34, 10, '', '', '0'),
(325, 29, 35, 11, '', '', '0'),
(326, 29, 37, 12, '', '', '0'),
(327, 28, 30, 4, '', '', '0'),
(328, 28, 4, 5, '', '', '0'),
(329, 28, 5, 6, '', '', '0'),
(330, 28, 33, 8, '', '', '0'),
(342, 19, 30, 2, '', '', '0'),
(346, 20, 64, 16, '', '', '0'),
(347, 21, 64, 16, '', '', '0'),
(348, 33, 30, 5, '', '', '0'),
(350, 25, 64, 13, '', '', '0'),
(352, 26, 64, 6, '', '', '0'),
(354, 33, 70, 0, '', '', '0'),
(355, 33, 71, 6, '', '{"fields": ["omd_no_venue"],"operation": "multiply","defaultValue": "12000"}', '0'),
(356, 33, 72, 8, '', '{"fields": ["omd_no_others"],"operation": "multiply","defaultValue": "30000"}', '0'),
(357, 33, 73, 10, '', '{"fields": ["total_omd_fees_venue","total_omd_fees_other_location"],"operation": "addition","defaultValue": "0"}', '0'),
(358, 33, 74, 7, '', '{"fields": ["omd_no_venue"],"operation": "multiply","defaultValue": "12000"}', '0'),
(359, 33, 75, 9, '', '{"fields": ["omd_no_others"],"operation": "multiply","defaultValue": "30000"}', '0'),
(360, 11, 82, 0, '', '', '0'),
(361, 11, 83, 1, '', '', '0'),
(362, 11, 84, 2, '', '', '0'),
(363, 11, 19, 4, '', '', '1'),
(364, 32, 99, 0, '', '', '0'),
(365, 32, 6, 2, '', '', '0'),
(366, 32, 7, 2, '', '', '0'),
(367, 32, 30, 3, '', '', '0'),
(368, 32, 33, 4, '', '', '0'),
(369, 32, 98, 5, '', '', '0'),
(370, 32, 97, 6, '', '', '0'),
(371, 32, 36, 7, '', '', '0'),
(372, 32, 96, 8, '', '', '0'),
(373, 32, 83, 0, '', '', '0'),
(374, 32, 84, 1, '', '', '0'),
(375, 32, 19, 3, '', '', '1'),
(376, 24, 20, 0, '', '', '0'),
(377, 24, 21, 1, '{"operationalValidation": {"field": ["event_duration"],"operator": "zero","defaultValue": "30","unit": "Days","oprand": "greater","title": "Duration of Event"}}', '', '0'),
(378, 24, 22, 3, ' {"depend":{"omd_no_others":{"type":"text","value":[],"message":"Please select one option","default":"","fieldType": "text"}}} ', '', '0'),
(379, 24, 23, 4, ' {"depend":{"omd_no_venue":{"type":"text","value":[],"message":"Please select one option","default":"","fieldType": "text"}}} ', '', '0'),
(380, 24, 7, 2, '', '', '0'),
(382, 24, 34, 8, '', '', '0'),
(383, 24, 35, 7, '', '', '0'),
(385, 24, 37, 10, '', '', '0'),
(386, 24, 30, 5, '', '', '0'),
(388, 24, 70, 0, '', '', '0'),
(394, 34, 15, 1, '', '', '0'),
(395, 34, 17, 2, '', '{"fields":["omd_private_dimensions"],"operation":"dimensionChange","defaultValue" : "1"}', '1'),
(396, 34, 18, 3, '', '{"fields":["omd_private_dimensions"],"operation":"dimensionChange","defaultValue" : "1"}', '1'),
(397, 34, 12, 4, '{"operationalMultyValidation": {"defaultValue": "0.2","field": ["total_facade_area", "omd_private_dimensions"],"0": {"operator": "*","defaultValue": "0.20","unit": "Facade","oprand": "greater","title": "Total Advertising Space"},"1": {"operator": "***","defaultValue": "","unit": "Facade","oprand": "greater","title": "Total Advertising Space"}}}', '', '1'),
(398, 34, 4, 9, '', '', '0'),
(399, 34, 5, 10, '', '', '0'),
(400, 34, 3, 11, '', '', '0'),
(401, 34, 7, 8, '', '', '0'),
(402, 34, 105, 7, '', '{"fields": ["total_advertising_space", "tenure", "discount"],"operation": "bodmas","equation": {"multiply": ["defaultValue", "total_advertising_space","tenure"],"percentageDiscount": ["discount"]},"defaultValue": "4500"}', '1'),
(403, 34, 33, 12, '', '', '0'),
(404, 34, 34, 13, '', '', '0'),
(405, 34, 35, 14, '', '', '0'),
(406, 34, 36, 15, '', '', '0'),
(407, 34, 37, 16, '', '', '0'),
(408, 34, 101, 0, '', '', '0'),
(410, 34, 61, 5, '', '', '0'),
(411, 34, 64, 17, '', '', '0'),
(412, 34, 30, 8, '', '', '0'),
(413, 34, 102, 0, '', '{"fields": ["omd_type_private_list"],"operation": "onSelect","options": {"Standard OMDs": ["4.6 x 3.05", "6.1 x 3.05"],"Unipole": ["4.5 x 2.25", "5.0 x 2.5"],"Large Size Billboards": ["7.6 x 3.8", "10.0 x 5.0"]},"defaultValue": ""}', '0'),
(414, 34, 103, 1, '', '', '0'),
(415, 34, 104, 6, '', '{"fields": ["tenure"],"operation": "discountCalculate","data": [{"6": 2},{"9": 3},{"12": 5}],"defaultValue": ""}', '0'),
(416, 23, 104, 6, '', '{"fields": ["tenure"],"operation": "discountCalculate","data": [{"6": 2},{"9": 3},{"12": 5}],"defaultValue": ""}', '0'),
(417, 28, 103, 1, '', '', '0'),
(418, 28, 104, 2, '', '{"fields": ["tenure"],"operation": "discountCalculate","data": [{"6": 2},{"9": 3},{"12": 5}],"defaultValue": ""}', '0'),
(419, 29, 103, 1, '', '', '0'),
(420, 29, 104, 2, '', '{"fields": ["tenure"],"operation": "discountCalculate","data": [{"6": 2},{"9": 3},{"12": 5}],"defaultValue": ""}', '0'),
(421, 27, 101, 0, '', '', '0'),
(422, 27, 102, 0, '', '{"fields": ["omd_type_private_list"],"operation": "onSelect","options": {"Standard OMDs": ["4.6 x 3.05", "6.1 x 3.05"],"Unipole": ["4.5 x 2.25", "5.0 x 2.5"],"Large Size Billboards": ["7.6 x 3.8", "10.0 x 5.0"]},"defaultValue": ""}', '0'),
(423, 27, 15, 1, '', '', '0'),
(424, 27, 103, 1, '', '', '0'),
(425, 27, 17, 2, '', '{"fields":["omd_private_dimensions"],"operation":"dimensionChange","defaultValue" : "1"}', '0'),
(426, 27, 18, 3, '', '{"fields":["omd_private_dimensions"],"operation":"dimensionChange","defaultValue" : "1"}', '0'),
(427, 27, 12, 4, '{"operationalMultyValidation": {"defaultValue": "0.2","field": ["total_facade_area", "omd_private_dimensions"],"0": {"operator": "*","defaultValue": "0.20","unit": "Facade","oprand": "greater","title": "Total Advertising Space"},"1": {"operator": "***","defaultValue": "","unit": "Facade","oprand": "greater","title": "Total Advertising Space"}}}', '{"fields":["omd_height","omd_width"],"operation":"multiply","defaultValue" : "1"}', '1'),
(428, 27, 61, 5, '', '', '0'),
(429, 27, 104, 6, '', '{"fields": ["tenure"],"operation": "discountCalculate","data": [{"6": 2},{"9": 3},{"12": 5}],"defaultValue": ""}', '0'),
(430, 27, 105, 7, '', '{"fields": ["total_advertising_space", "tenure", "discount"],"operation": "bodmas","equation": {"multiply": ["defaultValue", "total_advertising_space","tenure"],"percentageDiscount": ["discount"]},"defaultValue": "13500"}', '1'),
(431, 27, 7, 8, '', '', '0'),
(432, 27, 30, 8, '', '', '0'),
(433, 27, 4, 9, '', '', '0'),
(434, 27, 5, 10, '', '', '0'),
(435, 27, 3, 11, '', '', '0'),
(436, 27, 33, 12, '', '', '0'),
(437, 27, 34, 13, '', '', '0'),
(438, 27, 35, 14, '', '', '0'),
(439, 27, 36, 15, '', '', '0'),
(440, 27, 37, 16, '', '', '0'),
(441, 27, 64, 17, '', '', '0'),
(442, 26, 104, 6, '', '{"fields": ["tenure"],"operation": "discountCalculate","data": [{"6": 2},{"9": 3},{"12": 5}],"defaultValue": ""}', '0'),
(443, 26, 30, 8, '', '', '0'),
(446, 26, 103, 0, '', '', '0'),
(452, 26, 105, 7, '', '{"fields": ["omd_height", "omd_width", "tenure", "omd_type_shop_signage","total_facade_area", "discount"],"operation": "bodmas","equation": {"multiply": ["omd_price_shop_signage", "omd_height", "omd_width", "tenure"],"percentageDiscount": ["discount"],"exempted2percentage": {"ON": {"total_facade_area": 2},"multiply": ["omd_height", "omd_width"]}},"defaultValue": "1"}', '1'),
(453, 26, 7, 8, '', '', '0'),
(454, 26, 3, 11, '', '', '0'),
(455, 26, 5, 10, '', '', '0'),
(456, 26, 4, 9, '', '', '0'),
(457, 26, 12, 4, '{"operationalMultyValidation": {"defaultValue": "0.2","field": ["total_facade_area"],"0": {"operator": "*","defaultValue": "0.20","unit": "Facade","oprand": "greater","title": "Total Advertising Space"}}}', '{"fields":["omd_height","omd_width"],"operation":"multiply","defaultValue" : "1"}', '1'),
(458, 26, 18, 2, '', '', '0'),
(459, 26, 17, 3, '', '', '0'),
(460, 26, 15, 1, '', '', '0'),
(461, 26, 106, 0, '', '', '0'),
(462, 26, 107, 0, '', '{"fields": ["omd_type_shop_signage"],"operation": "onSelect","options": {"Outside Wall": "1500","Inside Commercial Building or Public Building": "225"},"defaultValue": ""}', '1'),
(463, 33, 108, 11, '', '', '0'),
(464, 24, 108, 11, '', '', '0');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) NOT NULL,
  `company_code` varchar(255) NOT NULL COMMENT 'Should be unique',
  `representative_id` int(11) NOT NULL,
  `pan_number` varchar(255) NOT NULL COMMENT 'used as unique identifier while register',
  `tin_number` varchar(255) DEFAULT NULL,
  `service_tax_number` varchar(255) DEFAULT NULL,
  `incorporation_date` datetime NOT NULL,
  `security_qus_id` int(255) NOT NULL,
  `answer` varchar(255) NOT NULL,
  `reg_fee` double(10,2) NOT NULL,
  `paid` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'used to track payment status',
  `status` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'used to track active/blocked record',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `company_name`, `company_code`, `representative_id`, `pan_number`, `tin_number`, `service_tax_number`, `incorporation_date`, `security_qus_id`, `answer`, `reg_fee`, `paid`, `status`, `created`, `modified`) VALUES
(1, 'Signity', 'COMP-1', 1, 'aopjncj', '', '', '2017-01-03 00:00:00', 0, '', 0.00, '0', '0', '2017-01-02 17:55:57', '2017-01-02 17:55:57'),
(2, 'Signity', 'COMP-2', 1, 'aopjncj', '', '', '2017-01-04 00:00:00', 0, '', 0.00, '0', '0', '2017-01-02 18:30:00', '2017-01-02 18:30:00'),
(3, 'Signity', 'COMP-3', 1, '1234', '', '', '2017-01-10 00:00:00', 0, '', 0.00, '0', '0', '2017-01-10 11:41:43', '2017-01-10 11:41:43'),
(4, 'Test Company', 'COMP-4', 5, 'abce8142', 'ddf51d411', '5121814', '2017-01-10 00:00:00', 0, '', 0.00, '0', '0', '2017-01-19 17:34:59', '2017-01-19 17:34:59'),
(5, 'Demo Company', 'COMP-5', 4, 'aopjncj', 'ddf51d411', '5121814', '2016-10-01 00:00:00', 0, '', 0.00, '0', '0', '2017-01-20 12:27:50', '2017-01-20 12:27:50');

-- --------------------------------------------------------

--
-- Table structure for table `company_devices`
--

CREATE TABLE IF NOT EXISTS `company_devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_name` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `company_code` varchar(255) NOT NULL COMMENT 'belongs to company',
  `category_id` int(255) NOT NULL,
  `sub_category_id` int(255) NOT NULL,
  `devive_type` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0 => Public, 1 => Private',
  `paid` enum('0','1') NOT NULL DEFAULT '0',
  `fee` double(10,2) NOT NULL,
  `by_admin` int(255) NOT NULL DEFAULT '0',
  `status` enum('under process','approved','rejected','resubmit','document requested') NOT NULL DEFAULT 'under process',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Company''s devices' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `company_device_fields`
--

CREATE TABLE IF NOT EXISTS `company_device_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_device_id` varchar(255) NOT NULL,
  `field_key` varchar(255) NOT NULL,
  `input_value` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='saved values of company devices input fields' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE IF NOT EXISTS `documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id`, `name`, `created`, `modified`) VALUES
(1, 'Pan Card', '2017-05-16 16:55:24', '2017-05-16 16:55:24'),
(2, 'Aadhar Card', '2017-05-16 16:55:53', '2017-05-16 16:55:53'),
(3, 'Driving Licence', '2017-05-30 17:08:35', '2017-05-30 17:08:35'),
(4, 'Bank Statement', '2017-05-30 17:15:12', '2017-05-30 17:15:12'),
(5, 'Ration Card', '2017-05-30 17:15:38', '2017-05-30 17:15:38');

-- --------------------------------------------------------

--
-- Table structure for table `experiences`
--

CREATE TABLE IF NOT EXISTS `experiences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `experiences`
--

INSERT INTO `experiences` (`id`, `name`, `created`, `modified`) VALUES
(1, '<1', '2017-05-16 07:18:21', '2017-05-16 07:18:21'),
(2, '1-2', '2017-05-16 07:18:21', '2017-05-16 07:18:21'),
(3, '2-3', '2017-05-16 07:18:21', '2017-05-16 07:18:21'),
(4, '3-4', '2017-05-16 07:18:21', '2017-05-16 07:18:21'),
(5, '4-5', '2017-05-16 07:18:21', '2017-05-16 07:18:21'),
(6, '>5', '2017-05-16 07:18:21', '2017-05-16 07:18:21');

-- --------------------------------------------------------

--
-- Table structure for table `e_auction_payments`
--

CREATE TABLE IF NOT EXISTS `e_auction_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `tender_id` int(11) NOT NULL,
  `payment_id` varchar(255) NOT NULL,
  `payment_request_id` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `status` varchar(225) NOT NULL,
  `buyer_name` varchar(255) NOT NULL,
  `buyer_phone` varchar(255) NOT NULL,
  `buyer_email` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `unit_price` double(10,2) NOT NULL,
  `amount` double(10,2) NOT NULL,
  `fees` double(10,2) NOT NULL,
  `created_at` datetime NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fields`
--

CREATE TABLE IF NOT EXISTS `fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lable` varchar(255) NOT NULL,
  `field_key` varchar(255) NOT NULL DEFAULT '0',
  `type` enum('text','date','select','radio','checkbox','file','hidden','json') NOT NULL DEFAULT 'text',
  `options` longtext COMMENT 'JSON Format. Eg: ["Yes","No"] | ["OPtion1","Option2","Option3"]',
  `is_disabled` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'if ''1'', set the input field as disabled',
  `default_validations` longtext COMMENT 'JSON Format. Eg: {"noEmpty":"true","minLimit":5,"maxLimit":10,"isNum":"ture","isAlphabit":"true","isAlphanumeric":"true"}',
  `default_calculations` longtext COMMENT 'JSON Format. Eg: {"fields":["filedKey1","filedKey2"],"operation":"multiply"}',
  `info` text NOT NULL,
  `group_in` varchar(255) NOT NULL COMMENT '''1''=>''Step1'',''2''=>''Step2'',''3''=>''Step3''',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='saved device input fileds' AUTO_INCREMENT=109 ;

--
-- Dumping data for table `fields`
--

INSERT INTO `fields` (`id`, `lable`, `field_key`, `type`, `options`, `is_disabled`, `default_validations`, `default_calculations`, `info`, `group_in`) VALUES
(1, 'Size (in sq./ft)', 'size', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(2, 'No. of Panels', 'panels', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(3, 'Location', 'location', 'text', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(4, 'Latitude', 'lat', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(5, 'Longitude', 'lng', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(6, 'Concession Fees', 'concession_fees', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(7, 'Lighting (Lit/Unlit)', 'lighting', 'radio', '["Yes", "No"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(8, 'Material Specification', 'material_specification', 'text', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(9, 'Inside Metro', 'inside_metro', 'radio', '["Yes", "No"]', '0', '{"noEmpty": false,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(10, 'Outside Metro', 'outside_metro', 'radio', '["Yes", "No"]', '0', '{"noEmpty": false,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(11, 'No of Units', 'units', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(12, 'Total Advertising Space (in sq metre.)', 'total_advertising_space', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(13, 'Type of Vehicle', 'type_of_vehicle', 'select', '["Radio Taxi","Auto"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(14, 'License Fee', 'licence_fee', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(15, 'Total Area of Facade (sq metre.)', 'total_facade_area', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(16, 'OMD Id', 'omd_id', 'text', NULL, '0', NULL, NULL, '', '1'),
(17, 'OMD Height (sq metre.)', 'omd_height', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(18, 'OMD Width (sq metre.)', 'omd_width', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(19, 'Total Annual Permission Fees (in Rs.)', 'annual_license_fees', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(20, 'Venue Location', 'venue_location', 'text', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(21, 'Duration of Event (In Days)', 'event_duration', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(22, 'No. of OMD at Venue', 'omd_no_venue', 'text', NULL, '0', '', NULL, '', '1'),
(23, 'No. of OMD at Other Location', 'omd_no_others', 'text', NULL, '0', '', NULL, '', '1'),
(24, 'Size of OMD', 'omd_size', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(25, 'Self Advertising', 'self_advertising', 'hidden', NULL, '0', NULL, NULL, '', '1'),
(26, 'Total No of Screens', 'no_of_screens', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(27, 'No of Screens Applied', 'no_of_screens_applied', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(28, 'No of Months Applied for', 'no_of_months_applied_for', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(29, 'Type of OMD site', 'building_type[]', 'checkbox', '["OMDS installed under one roof","OMDs installed within premises but in open courtyard"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(30, 'Zone', 'zone', 'select', '["Zone1","Zone2","Zone3","Zone4"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(31, 'No. of Panels Inside Metro', 'panels_in_metro', 'text', NULL, '0', '{"noEmpty": false,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(32, 'No. of Panels Outside Metro', 'panels_out_metro', 'text', NULL, '0', '{"noEmpty": false,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(33, 'Latest property tax receipt', 'propert_tax_receipt', 'file', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, 'Property tax receipt/ G8 where OMD is to be installed', '2'),
(34, 'Drawing of the site', 'site_drawings', 'file', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, 'Drawing showing the locality plan, in colour, indicating the proposed positions (GPS coordinates) of the OMDs and the distances in relation to any other structure, building, OMD situated within the radius of 25 m from the proposed OMDs', '2'),
(35, 'Specifications', 'site_specifications', 'file', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, 'Complete specifications showing the dimensions of the OMDs and locations', '2'),
(36, 'Certificate of Structural Engineer', 'structural_engineer_certificate', 'file', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, 'Certifying safety aspects (for more details please refer Byelaw 5(1)(v) )', '2'),
(37, 'Architectural Drawing', 'architectural_drawing', 'file', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, 'Architectural drawings showing the elevation and measurements of the building and measurement and position of every existing and proposed OMDs drawn on a scale of 1:1000.', '2'),
(38, 'The applicant firm/company has no pending dues', 'has_pending_dues', 'radio', '["Yes", "No"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '2'),
(39, 'The applicant firm/company has no court case pending', 'pending_court_case', 'radio', '["Yes", "No"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '2'),
(40, 'Size (in sq./ft)', 'size_inside_metro', 'text', NULL, '0', '{"noEmpty": false,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(41, 'Size (in sq./ft)', 'size_outside_metro', 'text', NULL, '0', '{"noEmpty": false,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(42, 'OMD 1 Size at Venue', 'omd_no_venue_size_1', 'text', NULL, '0', NULL, NULL, '', '1'),
(43, 'OMD 2 Size at Venue', 'omd_no_venue_size_2', 'text', NULL, '0', NULL, NULL, '', '1'),
(44, 'OMD 3 Size at Venue', 'omd_no_venue_size_3', 'text', NULL, '0', NULL, NULL, '', '1'),
(45, 'OMD 4 Size at Venue', 'omd_no_venue_size_4', 'text', NULL, '0', NULL, NULL, '', '1'),
(46, 'OMD 5 Size at Venue', 'omd_no_venue_size_5', 'text', NULL, '0', NULL, NULL, '', '1'),
(47, 'OMD 6 Size at Venue', 'omd_no_venue_size_6', 'text', NULL, '0', NULL, NULL, '', '1'),
(48, 'OMD 1 Size at others', 'omd_no_others_size_1', 'text', NULL, '0', NULL, NULL, '', '1'),
(49, 'OMD 2 Size at others', 'omd_no_others_size_2', 'text', NULL, '0', NULL, NULL, '', '1'),
(50, 'OMD 3 Size at others', 'omd_no_others_size_3', 'text', NULL, '0', NULL, NULL, '', '1'),
(51, 'OMD 4 Size at others', 'omd_no_others_size_4', 'text', NULL, '0', NULL, NULL, '', '1'),
(52, 'OMD 5 Size at others', 'omd_no_others_size_5', 'text', NULL, '0', NULL, NULL, '', '1'),
(53, 'OMD 6 Size at others', 'omd_no_others_size_6', 'text', NULL, '0', NULL, NULL, '', '1'),
(54, 'Size Of OMD( Not More than 75% of the Facade Area)', 'omd_size_less_75', 'radio', '["Yes", "No"]', '0', NULL, NULL, '', '1'),
(55, 'Distance From Intersection(100 m away From Intersection) ', 'intersection_distance', 'radio', '["Yes", "No"]', '0', NULL, NULL, '', '1'),
(56, 'Distance between two OMDs(minimum 50 metres)', 'two_omd_distance', 'radio', '["Yes", "No"]', '0', NULL, NULL, '', '1'),
(57, 'Obstruction to Road User/Pedestrian', 'obstruction_to_road', 'radio', '["Yes", "No"]', '0', NULL, NULL, '', '1'),
(58, 'Rejection Reason', 'reason', 'text', NULL, '0', NULL, NULL, '', '1'),
(59, 'Name and Address of the multiplex/cinema', 'multiplex_name_address', 'text', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(60, 'Type of OMD', 'omd_type_list', 'select', '["Small Portraits","Posters","Super 8’s","Portraits","Supersites","Spectacular"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(61, 'Height of the building', 'building_height', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(62, 'Type of OMD', 'omd_type_wall', 'select', '["Wall Wrap"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(63, 'OMD Price', 'omd_price', 'text', NULL, '1', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(64, 'Agreement between Site Owner and Agency', 'site_owner_agency_agreement', 'file', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, 'Agreement between Site Owner and Agency', '2'),
(65, 'OMDS installed under one roof', 'omd_one_roof', 'json', NULL, '0', NULL, NULL, '', '1'),
(66, 'OMDs installed within premises but in open courtyard', 'omd_premises', 'json', NULL, '0', NULL, NULL, '', '1'),
(67, 'Type of OMD site', 'building_type', 'text', NULL, '0', NULL, NULL, '', '1'),
(68, 'Metro/MRTS Type', 'metro_mrts_categories', 'select', '["Rolling Stock","Inside Station","Station branding"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(69, 'Metro MRTS', 'metro_table', 'json', NULL, '0', NULL, NULL, '', '1'),
(70, 'Event Start Date', 'event_start_date', 'date', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(71, 'OMD Fees at Venue', 'omd_fees_venue', 'text', NULL, '1', '{"noEmpty": false,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(72, 'OMD Fees at Other Location', 'omd_fees_other_location', 'text', NULL, '1', '{"noEmpty": false,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(73, 'Total OMD Fees', 'total_omd_fees', 'text', NULL, '1', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(74, 'Total OMD Fees at Venue', 'total_omd_fees_venue', 'text', NULL, '1', NULL, NULL, '', '1'),
(75, 'Total OMD Fees at Other Location', 'total_omd_fees_other_location', 'text', NULL, '1', NULL, NULL, '', '1'),
(76, 'OMD Other Location 1', 'omd_no_others_location_1', 'text', NULL, '0', NULL, NULL, '', '1'),
(77, 'OMD Other Location 2', 'omd_no_others_location_2', 'text', NULL, '0', NULL, NULL, '', '1'),
(78, 'OMD Other Location 3', 'omd_no_others_location_3', 'text', NULL, '0', NULL, NULL, '', '1'),
(79, 'OMD Other Location 4', 'omd_no_others_location_4', 'text', NULL, '0', NULL, NULL, '', '1'),
(80, 'OMD Other Location 5', 'omd_no_others_location_5', 'text', NULL, '0', NULL, NULL, '', '1'),
(81, 'OMD Other Location 6', 'omd_no_others_location_6', 'text', NULL, '0', NULL, NULL, '', '1'),
(82, 'Shelters Type', 'shelters_type', 'select', '["Bus Shelters","IPT Shelters"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(83, 'Concessioning Authority', 'concessionaire_type', 'select', '["MCG","Other"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(84, 'Name of Concessioning Authority', 'concessionaire_name', 'text', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(85, 'Route markers type', 'route_markers_type', 'select', '["Bus route markers","IPT route markers"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(86, 'Asset Type', 'foot_over_type', 'select', '["Foot over bridges","Signage gantries","Toilet Blocks","Urinals"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(87, 'Asset Type', 'cycle_station_type', 'select', '["Cycle station"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(88, 'Asset Type', 'police_booth_type', 'select', '["Police booth","Parking booth","Telephone booth","Prepaid taxi Booth","Public utility kiosks","Drinking water facility","Bus/train booking facility","Outside colonies to facilitate directory/payment of bills"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(89, 'Asset Type', 'sitting_bench_type', 'select', '["Sitting Bench","Garbage bin","Others"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(90, 'Shelter Data', 'shelter_data', 'json', NULL, '0', NULL, NULL, '', '1'),
(91, 'Bus route markers', 'route_markers_data', 'json', NULL, '0', NULL, NULL, '', ''),
(92, 'Foot Over Data', 'foot_over_data', 'json', NULL, '0', NULL, NULL, '', '1'),
(93, 'Cycle Station Data', 'cycle_station_data', 'json', NULL, '0', NULL, NULL, '', '1'),
(94, 'Police Booth Data', 'police_booth_data', 'json', NULL, '0', NULL, NULL, '', '1'),
(95, 'Sitting Bench Data', 'sitting_bench_data', 'json', NULL, '0', NULL, NULL, '', '1'),
(96, 'Approved Architectural Drawing', 'approved_architectural_drawing', 'file', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, 'Approved Architectural Drawing', '2'),
(97, 'Approved specifications', 'approved_specifications', 'file', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, 'Approved specifications', '2'),
(98, 'Approved drawing of the site', 'approved_drawing_of_site', 'file', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, 'Approved drawing of the site', '2'),
(99, 'OMD Type', 'omd_type_type_a', 'select', '["Unipole","Wall Wrap","Wall Mounted"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(100, 'Typology A Others', 'other_type_a_data', 'json', NULL, '0', NULL, NULL, '', '1'),
(101, 'Type of OMD', 'omd_type_private_list', 'select', '["Standard OMDs", "Unipole", "Large Size Billboards"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(102, 'Dimensions Range (in metres)', 'omd_private_dimensions', 'select', '[]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(103, 'Tenure (In Months) (Min 3 and max 36 months)', 'tenure', 'text', '', '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false,"minValue": 3,"maxValue": 36,"isInteger": true}', NULL, '', '1'),
(104, 'Discount (in Percentage)', 'discount', 'text', NULL, '1', NULL, NULL, '', '1'),
(105, 'Total Permission Fees ( in Rupees )', 'total_permission_fees', 'text', NULL, '0', NULL, NULL, '', '1'),
(106, 'Type Of OMD ', 'omd_type_shop_signage', 'select', '["Outside Wall","Inside Commercial Building or Public Building"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(107, 'OMD Price', 'omd_price_shop_signage', 'text', NULL, '0', NULL, NULL, '', '1'),
(108, 'Permission/NOC from site owner', 'permission_noc_from_site_owner', 'file', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, 'Permission/NOC from site owner', '2');

-- --------------------------------------------------------

--
-- Table structure for table `installments`
--

CREATE TABLE IF NOT EXISTS `installments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `payment_id` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mcg_comments`
--

CREATE TABLE IF NOT EXISTS `mcg_comments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `mcg_level` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `comment_data` longtext NOT NULL,
  `status` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `old_categories`
--

CREATE TABLE IF NOT EXISTS `old_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `parent_id` int(255) NOT NULL,
  `type` varchar(255) NOT NULL DEFAULT '0' COMMENT '0 => Public, 1 => Private',
  `guest_enabled` tinyint(4) NOT NULL,
  `sort` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='saved parent & sub categories' AUTO_INCREMENT=36 ;

--
-- Dumping data for table `old_categories`
--

INSERT INTO `old_categories` (`id`, `title`, `parent_id`, `type`, `guest_enabled`, `sort`, `status`, `created`, `modified`) VALUES
(1, 'Type A/OMDs on public transport services / street furniture and public transport system', 0, '0', 0, 0, '1', '2016-12-27 16:06:14', '2016-12-27 16:06:14'),
(2, 'Type B/Advertising Outdoor Media Device on Public Transport System', 0, '0', 0, 0, '0', '2016-12-27 16:06:14', '2016-12-27 16:06:14'),
(3, 'Type B/Advertising Outdoor Media Device on Commercial Advertising Structure on Public Land', 0, '0', 0, 0, '1', '2016-12-27 16:06:14', '2016-12-27 16:06:14'),
(4, 'Type C/Advertising Outdoor Media Device on Commercial Advertising Structure on Private Land', 0, '1', 0, 0, '1', '2016-12-27 16:06:14', '2016-12-27 16:06:14'),
(5, 'Type D/Events', 0, '0,1', 1, 0, '1', '2016-12-27 16:06:14', '2016-12-27 16:06:14'),
(6, 'Type E/Landscape Advertising', 0, '0', 0, 0, '1', '2016-12-27 16:06:14', '2016-12-27 16:06:14'),
(7, 'Type F/Shop Signage', 0, '0,1', 0, 0, '1', '2016-12-27 16:06:14', '2016-12-27 16:06:14'),
(8, 'Type G/Innovative Advertising', 0, '0,1', 0, 0, '1', '2016-12-27 16:06:14', '2016-12-27 16:06:14'),
(9, 'Type H/Cinema Advertising', 0, '0,1', 0, 0, '1', '2016-12-27 16:06:14', '2016-12-27 16:06:14'),
(10, 'Type I/Inside Commercial Buildings and public Buildings', 0, '0', 0, 0, '1', '2016-12-27 16:06:14', '2016-12-27 16:06:14'),
(11, 'Bus and IPT Shelters', 1, '0', 0, 0, '1', '2016-12-27 16:08:08', '2016-12-27 16:08:08'),
(12, 'Bus and IPT Route Markers', 1, '0', 0, 0, '1', '2016-12-27 16:08:08', '2016-12-27 16:08:08'),
(13, 'Foot over bridge, Signage gantries, Toilet Blocks, Urinals', 1, '0', 0, 0, '1', '2016-12-27 16:08:08', '2016-12-27 16:08:08'),
(14, 'Cycle station', 1, '0', 0, 0, '1', '2016-12-27 16:08:08', '2016-12-27 16:08:08'),
(15, 'Police booth, Parking booth, Telephone booth, Prepaid taxi Booth, Vending kiosks, drinking water facility, bus/train booking facility, Outside colonies to facilitate directory/payment of bills', 1, '0', 0, 0, '1', '2016-12-27 16:08:08', '2016-12-27 16:08:08'),
(16, 'Sitting Bench, Garbage bin', 1, '0', 0, 0, '1', '2016-12-27 16:08:08', '2016-12-27 16:08:08'),
(17, 'Metro/MRTS', 1, '0', 0, 0, '1', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(18, 'Traffic Barricading', 1, '0', 0, 0, '1', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(19, 'Public transport vehicle', 1, '0', 0, 0, '1', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(20, 'Hoarding', 3, '0', 0, 0, '1', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(21, 'Wall Wraps', 3, '0', 0, 0, '1', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(22, 'Billboards', 4, '0', 0, 0, '0', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(23, 'Wall Wraps', 4, '0', 0, 1, '1', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(24, 'Religious, Political and Conferences', 5, '0', 0, 0, '1', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(25, 'Tree Guards', 6, '0', 0, 0, '1', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(26, 'Self Advertising', 7, '0', 0, 0, '1', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(27, 'Innovative Advertising', 8, '0', 0, 0, '1', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(28, 'In Cinema on screen Advertising including slides and advertisement flims', 9, '0', 0, 0, '1', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(29, 'Inside Commercial Buldings and public Buldings', 10, '0', 0, 0, '1', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(30, 'Unipoles', 4, '0', 0, 0, '0', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(31, 'Building Boards', 4, '0', 0, 0, '0', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(32, 'Others', 1, '0', 0, 0, '1', '2018-01-11 12:00:00', '2018-01-11 12:00:00'),
(33, 'Entertainment and Exhibitions events', 5, '0', 0, 0, '1', '2016-12-27 17:03:17', '2016-12-27 17:03:17'),
(34, 'Unipoles, billboards, building boards', 4, '0', 0, 0, '1', '2018-05-31 12:54:00', '2018-05-31 12:54:00'),
(35, 'Multiple OMDs', 4, '0', 0, 2, '1', '2018-05-31 12:58:00', '2018-05-31 12:58:00');

-- --------------------------------------------------------

--
-- Table structure for table `old_category_fields`
--

CREATE TABLE IF NOT EXISTS `old_category_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(255) NOT NULL,
  `field_id` int(255) NOT NULL,
  `sort_order` int(255) NOT NULL DEFAULT '0',
  `custom_validation` longtext NOT NULL COMMENT 'JSON Format. Eg: [{"minValue":10,"error":"error message here"},{"maxValue":50,"error":"error message here"}]',
  `custom_calculations` longtext NOT NULL COMMENT 'JSON Format. Eg: {"fields":["filedKey1","filedKey2"],"operation":"multiply"}',
  `custom_disable` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=461 ;

--
-- Dumping data for table `old_category_fields`
--

INSERT INTO `old_category_fields` (`id`, `category_id`, `field_id`, `sort_order`, `custom_validation`, `custom_calculations`, `custom_disable`) VALUES
(6, 11, 6, 3, '', '', '0'),
(7, 11, 7, 6, '', '', '0'),
(8, 11, 30, 7, '', '', '0'),
(9, 17, 68, 0, '', '', '0'),
(15, 17, 4, 6, '', '', '0'),
(16, 17, 5, 7, '', '', '0'),
(17, 17, 3, 12, '', '', '0'),
(18, 17, 7, 9, '', '', '0'),
(19, 17, 30, 10, '', '', '0'),
(21, 18, 11, 0, '', '', '0'),
(22, 18, 12, 1, '', '', '0'),
(23, 18, 4, 2, '', '', '0'),
(24, 18, 5, 3, '', '', '0'),
(25, 18, 3, 7, '', '', '0'),
(26, 18, 30, 5, '', '', '0'),
(27, 18, 6, 6, '', '', '0'),
(28, 19, 13, 0, '', '', '0'),
(29, 19, 14, 1, '', '', '0'),
(30, 20, 15, 0, '', '', '0'),
(32, 20, 17, 2, '', '', '0'),
(33, 20, 18, 3, '', '', '0'),
(34, 20, 12, 4, '{"operationalValidation": { "field" : ["total_facade_area"], "operator" : "*","defaultValue" : "0.75" ,"unit" : "Facade", "oprand" : "greater", "title": "Total Advertising Space" }}', '{"fields":["omd_height","omd_width"],"operation":"multiply","defaultValue" : "1"}', '1'),
(35, 20, 4, 5, '', '', '0'),
(36, 20, 5, 6, '', '', '0'),
(37, 20, 3, 11, '', '', '0'),
(38, 20, 7, 8, '', '', '0'),
(39, 20, 6, 9, '', '', '0'),
(40, 20, 30, 10, '', '', '0'),
(41, 22, 15, 1, '', '', '0'),
(43, 22, 17, 2, '', '', '0'),
(44, 22, 18, 3, '', '', '0'),
(45, 22, 12, 4, '{"operationalValidation": { "field" : ["total_facade_area"], "operator" : "*","defaultValue" : "0.75" ,"unit" : "Facade", "oprand" : "greater", "title": "Total Advertising Space" }}', '{"fields":["omd_height","omd_width"],"operation":"multiply","defaultValue" : "1"}', '1'),
(46, 22, 4, 9, '', '', '0'),
(47, 22, 5, 10, '', '', '0'),
(48, 22, 3, 11, '', '', '0'),
(49, 22, 7, 8, '', '', '0'),
(51, 22, 19, 7, '', '{"fields":["omd_height","omd_width","omd_price"],"operation":"multiply","defaultValue" : "12"}', '1'),
(52, 33, 20, 0, '', '', '0'),
(53, 33, 21, 1, '{"operationalValidation": {"field": ["event_duration"],"operator": "zero","defaultValue": "30","unit": "Days","oprand": "greater","title": "Duration of Event"}}', '', '0'),
(54, 33, 22, 3, ' {"depend":{"omd_no_others":{"type":"text","value":[],"message":"Please select one option","default":"","fieldType": "text"}}} ', '', '0'),
(55, 33, 23, 4, ' {"depend":{"omd_no_venue":{"type":"text","value":[],"message":"Please select one option","default":"","fieldType": "text"}}} ', '', '0'),
(56, 33, 7, 2, '', '', '0'),
(58, 25, 11, 0, '', '', '0'),
(59, 25, 24, 1, '', '', '0'),
(60, 25, 4, 2, '', '', '0'),
(61, 25, 5, 3, '', '', '0'),
(62, 25, 3, 8, '', '', '0'),
(63, 25, 7, 5, '', '', '0'),
(64, 25, 6, 6, '', '', '0'),
(65, 25, 30, 7, '', '', '0'),
(79, 28, 27, 0, '', '', '0'),
(81, 28, 3, 7, '', '', '0'),
(82, 28, 105, 3, '', '{"fields": ["no_of_screens_applied", "tenure", "discount"],"operation": "bodmas","equation": {"multiply": ["defaultValue", "no_of_screens_applied", "tenure"],"percentageDiscount": ["discount"]},"defaultValue": "25000"}', '1'),
(83, 29, 29, 0, '', '', '0'),
(85, 29, 3, 6, '', '', '0'),
(88, 12, 85, 0, '', '', '0'),
(93, 12, 6, 5, '', '', '0'),
(94, 12, 7, 6, '', '', '0'),
(95, 12, 30, 7, '', '', '0'),
(96, 13, 86, 0, '', '', '0'),
(101, 13, 6, 5, '', '', '0'),
(102, 13, 7, 6, '', '', '0'),
(103, 13, 30, 7, '', '', '0'),
(104, 14, 87, 0, '', '', '0'),
(109, 14, 6, 5, '', '', '0'),
(110, 14, 7, 6, '', '', '0'),
(111, 14, 30, 7, '', '', '0'),
(112, 15, 88, 0, '', '', '0'),
(117, 15, 6, 5, '', '', '0'),
(118, 15, 7, 6, '', '', '0'),
(119, 15, 30, 7, '', '', '0'),
(120, 16, 89, 0, '', '', '0'),
(125, 16, 6, 5, '', '', '0'),
(126, 16, 7, 6, '', '', '0'),
(127, 16, 30, 7, '', '', '0'),
(128, 21, 15, 0, '', '', '0'),
(130, 21, 17, 2, '', '', '0'),
(131, 21, 18, 3, '', '', '0'),
(132, 21, 12, 4, '{"operationalValidation": { "field" : ["total_facade_area"], "operator" : "*","defaultValue" : "0.75" ,"unit" : "Facade", "oprand" : "greater", "title": "Total Advertising Space" }}', '{"fields":["omd_height","omd_width"],"operation":"multiply","defaultValue" : "1"}', '1'),
(133, 21, 4, 5, '', '', '0'),
(134, 21, 5, 6, '', '', '0'),
(135, 21, 3, 11, '', '', '0'),
(136, 21, 7, 8, '', '', '0'),
(137, 21, 6, 9, '', '', '0'),
(138, 21, 30, 10, '', '', '0'),
(139, 23, 15, 1, '', '', '0'),
(141, 23, 17, 2, '', '', '0'),
(142, 23, 18, 3, '', '', '0'),
(143, 23, 12, 4, '{"operationalMultyValidation": {"defaultValue": "0.2","field": ["total_facade_area"],"0": {"operator": "*","defaultValue": "0.20","unit": "Facade","oprand": "greater","title": "Total Advertising Space"}}}', '{"fields":["omd_height","omd_width"],"operation":"multiply","defaultValue" : "1"}', '1'),
(144, 23, 4, 9, '', '', '0'),
(145, 23, 5, 10, '', '', '0'),
(146, 23, 3, 11, '', '', '0'),
(147, 23, 7, 8, '', '', '0'),
(149, 23, 105, 7, '', '{"fields": ["omd_height","omd_width", "tenure", "discount"],"operation": "bodmas","equation": {"multiply": ["defaultValue", "omd_height","omd_width","tenure"],"percentageDiscount": ["discount"]},"defaultValue": "3000"}', '1'),
(150, 11, 33, 8, '', '', '0'),
(151, 11, 98, 9, '', '', '0'),
(152, 11, 97, 10, '', '', '0'),
(153, 11, 36, 11, '', '', '0'),
(154, 11, 96, 12, '', '', '0'),
(157, 12, 33, 8, '', '', '0'),
(158, 12, 98, 9, '', '', '0'),
(159, 12, 97, 10, '', '', '0'),
(160, 12, 36, 11, '', '', '0'),
(161, 12, 96, 12, '', '', '0'),
(164, 13, 33, 8, '', '', '0'),
(165, 13, 98, 9, '', '', '0'),
(166, 13, 97, 10, '', '', '0'),
(167, 13, 36, 11, '', '', '0'),
(168, 13, 96, 12, '', '', '0'),
(171, 14, 33, 8, '', '', '0'),
(172, 14, 98, 9, '', '', '0'),
(173, 14, 97, 10, '', '', '0'),
(174, 14, 36, 11, '', '', '0'),
(175, 14, 96, 12, '', '', '0'),
(178, 15, 33, 8, '', '', '0'),
(179, 15, 98, 9, '', '', '0'),
(180, 15, 97, 10, '', '', '0'),
(181, 15, 36, 11, '', '', '0'),
(182, 15, 96, 12, '', '', '0'),
(185, 16, 33, 8, '', '', '0'),
(186, 16, 98, 9, '', '', '0'),
(187, 16, 97, 10, '', '', '0'),
(188, 16, 36, 11, '', '', '0'),
(189, 16, 96, 12, '', '', '0'),
(192, 17, 33, 12, '', '', '0'),
(193, 17, 98, 13, '', '', '0'),
(194, 17, 97, 14, '', '', '0'),
(195, 17, 36, 15, '', '', '0'),
(196, 17, 96, 16, '', '', '0'),
(199, 18, 33, 7, '', '', '0'),
(200, 18, 98, 8, '', '', '0'),
(201, 18, 97, 9, '', '', '0'),
(202, 18, 36, 10, '', '', '0'),
(203, 18, 96, 11, '', '', '0'),
(206, 19, 33, 2, '', '', '0'),
(207, 19, 98, 3, '', '', '0'),
(208, 19, 97, 4, '', '', '0'),
(209, 19, 36, 5, '', '', '0'),
(210, 19, 96, 6, '', '', '0'),
(213, 20, 33, 11, '', '', '0'),
(214, 20, 34, 12, '', '', '0'),
(215, 20, 35, 13, '', '', '0'),
(216, 20, 36, 14, '', '', '0'),
(217, 20, 37, 15, '', '', '0'),
(220, 21, 33, 11, '', '', '0'),
(221, 21, 34, 12, '', '', '0'),
(222, 21, 35, 13, '', '', '0'),
(223, 21, 36, 14, '', '', '0'),
(224, 21, 37, 15, '', '', '0'),
(227, 22, 33, 12, '', '', '0'),
(228, 22, 34, 13, '', '', '0'),
(229, 22, 35, 14, '', '', '0'),
(230, 22, 36, 15, '', '', '0'),
(231, 22, 37, 16, '', '', '0'),
(234, 23, 33, 12, '', '', '0'),
(235, 23, 34, 13, '', '', '0'),
(236, 23, 35, 14, '', '', '0'),
(237, 23, 36, 15, '', '', '0'),
(238, 23, 37, 16, '', '', '0'),
(241, 33, 33, 6, '', '', '0'),
(242, 33, 34, 7, '', '', '0'),
(243, 33, 35, 8, '', '', '0'),
(244, 33, 36, 9, '', '', '0'),
(245, 33, 37, 10, '', '', '0'),
(248, 25, 33, 8, '', '', '0'),
(249, 25, 34, 9, '', '', '0'),
(250, 25, 35, 10, '', '', '0'),
(251, 25, 36, 11, '', '', '0'),
(252, 25, 37, 12, '', '', '0'),
(267, 30, 15, 1, '', '', '0'),
(268, 30, 17, 2, '', '', ''),
(269, 30, 18, 3, '', '', ''),
(270, 30, 12, 4, '{"operationalValidation": { "field" : ["total_facade_area"], "operator" : "*","defaultValue" : "0.75" ,"unit" : "Facade", "oprand" : "greater", "title": "Total Advertising Space" }}', '{"fields":["omd_height","omd_width"],"operation":"multiply","defaultValue" : "1"}', '1'),
(271, 30, 4, 9, '', '', ''),
(272, 30, 5, 10, '', '', ''),
(273, 30, 3, 11, '', '', ''),
(274, 30, 7, 8, '', '', ''),
(276, 30, 19, 7, '', '{"fields":["omd_height","omd_width","omd_price"],"operation":"multiply","defaultValue" : "12"}', '1'),
(277, 30, 33, 12, '', '', ''),
(278, 30, 34, 13, '', '', ''),
(279, 30, 35, 14, '', '', ''),
(280, 30, 36, 15, '', '', ''),
(281, 30, 37, 16, '', '', '0'),
(282, 31, 15, 1, '', '', '0'),
(283, 31, 17, 2, '', '', ''),
(284, 31, 18, 3, '', '', ''),
(285, 31, 12, 4, '{"operationalValidation": { "field" : ["total_facade_area"], "operator" : "*","defaultValue" : "0.75" ,"unit" : "Facade", "oprand" : "greater", "title": "Total Advertising Space" }}', '{"fields":["omd_height","omd_width"],"operation":"multiply","defaultValue" : "1"}', '1'),
(286, 31, 4, 9, '', '', ''),
(287, 31, 5, 10, '', '', ''),
(288, 31, 3, 11, '', '', ''),
(289, 31, 7, 8, '', '', ''),
(291, 31, 19, 7, '', '{"fields":["omd_height","omd_width","omd_price"],"operation":"multiply","defaultValue" : "12"}', '1'),
(292, 31, 33, 12, '', '', ''),
(293, 31, 34, 13, '', '', ''),
(294, 31, 35, 14, '', '', '0'),
(295, 31, 36, 15, '', '', ''),
(296, 31, 37, 16, '', '', '0'),
(297, 29, 4, 4, '', '', '0'),
(298, 29, 5, 5, '', '', '0'),
(299, 22, 60, 0, '', '', '0'),
(300, 30, 60, 0, '', '', '0'),
(301, 31, 60, 0, '', '', '0'),
(302, 23, 103, 0, '', '', '0'),
(303, 22, 63, 6, '', '{"fields":["omd_type_list"],"operation":"onSelect","options":{"Small Portraits":"3200","Posters":"3750","Super 8’s":"3750","Portraits":"4300","Supersites":"4800","Spectacular":"4800"},"defaultValue" : ""}', '0'),
(305, 30, 63, 6, '', '{"fields":["omd_type_list"],"operation":"onSelect","options":{"Small Portraits":"3200","Posters":"3750","Super 8’s":"3750","Portraits":"4300","Supersites":"4800","Spectacular":"4800"},"defaultValue" : ""}', '0'),
(306, 31, 63, 6, '', '{"fields":["omd_type_list"],"operation":"onSelect","options":{"Small Portraits":"3200","Posters":"3750","Super 8’s":"3750","Portraits":"4300","Supersites":"4800","Spectacular":"4800"},"defaultValue" : ""}', '0'),
(307, 22, 61, 5, '', '', '0'),
(308, 23, 61, 5, '', '', '0'),
(309, 30, 61, 5, '', '', '0'),
(310, 31, 61, 5, '', '', '0'),
(311, 29, 105, 3, '', '', '1'),
(312, 22, 64, 17, '', '', '0'),
(313, 23, 64, 17, '', '', '0'),
(314, 30, 64, 17, '', '', '0'),
(315, 31, 64, 17, '', '', '0'),
(316, 29, 36, 7, '', '', '0'),
(317, 29, 64, 8, '', '', '0'),
(318, 22, 30, 8, '', '', '0'),
(319, 23, 30, 8, '', '', '0'),
(320, 30, 30, 8, '', '', '0'),
(321, 31, 30, 8, '', '', '0'),
(322, 29, 30, 3, '', '', '0'),
(323, 29, 33, 9, '', '', '0'),
(324, 29, 34, 10, '', '', '0'),
(325, 29, 35, 11, '', '', '0'),
(326, 29, 37, 12, '', '', '0'),
(327, 28, 30, 4, '', '', '0'),
(328, 28, 4, 5, '', '', '0'),
(329, 28, 5, 6, '', '', '0'),
(330, 28, 33, 8, '', '', '0'),
(342, 19, 30, 2, '', '', '0'),
(346, 20, 64, 16, '', '', '0'),
(347, 21, 64, 16, '', '', '0'),
(348, 33, 30, 5, '', '', '0'),
(349, 33, 64, 11, '', '', '0'),
(350, 25, 64, 13, '', '', '0'),
(354, 33, 70, 0, '', '', '0'),
(355, 33, 71, 6, '', '{"fields": ["omd_no_venue"],"operation": "multiply","defaultValue": "12000"}', '0'),
(356, 33, 72, 8, '', '{"fields": ["omd_no_others"],"operation": "multiply","defaultValue": "30000"}', '0'),
(357, 33, 73, 10, '', '{"fields": ["total_omd_fees_venue","total_omd_fees_other_location"],"operation": "addition","defaultValue": "0"}', '0'),
(358, 33, 74, 7, '', '{"fields": ["event_duration","omd_no_venue"],"operation": "multiply","defaultValue": "12000"}', '0'),
(359, 33, 75, 9, '', '{"fields": ["event_duration","omd_no_others"],"operation": "multiply","defaultValue": "30000"}', '0'),
(360, 11, 82, 0, '', '', '0'),
(361, 11, 83, 1, '', '', '0'),
(362, 11, 84, 2, '', '', '0'),
(363, 11, 19, 4, '', '', '1'),
(364, 32, 99, 0, '', '', '0'),
(365, 32, 6, 2, '', '', '0'),
(366, 32, 7, 2, '', '', '0'),
(367, 32, 30, 3, '', '', '0'),
(368, 32, 33, 4, '', '', '0'),
(369, 32, 98, 5, '', '', '0'),
(370, 32, 97, 6, '', '', '0'),
(371, 32, 36, 7, '', '', '0'),
(372, 32, 96, 8, '', '', '0'),
(373, 32, 83, 0, '', '', '0'),
(374, 32, 84, 1, '', '', '0'),
(375, 32, 19, 3, '', '', '1'),
(376, 24, 20, 0, '', '', '0'),
(377, 24, 21, 1, '{"operationalValidation": {"field": ["event_duration"],"operator": "zero","defaultValue": "30","unit": "Days","oprand": "greater","title": "Duration of Event"}}', '', '0'),
(378, 24, 22, 3, ' {"depend":{"omd_no_others":{"type":"text","value":[],"message":"Please select one option","default":"","fieldType": "text"}}} ', '', '0'),
(379, 24, 23, 4, ' {"depend":{"omd_no_venue":{"type":"text","value":[],"message":"Please select one option","default":"","fieldType": "text"}}} ', '', '0'),
(380, 24, 7, 2, '', '', '0'),
(381, 24, 33, 6, '', '', '0'),
(382, 24, 34, 7, '', '', '0'),
(383, 24, 35, 8, '', '', '0'),
(384, 24, 36, 9, '', '', '0'),
(385, 24, 37, 10, '', '', '0'),
(386, 24, 30, 5, '', '', '0'),
(387, 24, 64, 11, '', '', '0'),
(388, 24, 70, 0, '', '', '0'),
(394, 34, 15, 1, '', '', '0'),
(395, 34, 17, 2, '', '{"fields":["omd_private_dimensions"],"operation":"dimensionChange","defaultValue" : "1"}', '1'),
(396, 34, 18, 3, '', '{"fields":["omd_private_dimensions"],"operation":"dimensionChange","defaultValue" : "1"}', '1'),
(397, 34, 12, 4, '{"operationalMultyValidation": {"defaultValue": "0.2","field": ["total_facade_area", "omd_private_dimensions"],"0": {"operator": "*","defaultValue": "0.20","unit": "Facade","oprand": "greater","title": "Total Advertising Space"},"1": {"operator": "***","defaultValue": "","unit": "Facade","oprand": "greater","title": "Total Advertising Space"}}}', '', '1'),
(398, 34, 4, 9, '', '', '0'),
(399, 34, 5, 10, '', '', '0'),
(400, 34, 3, 11, '', '', '0'),
(401, 34, 7, 8, '', '', '0'),
(402, 34, 105, 7, '', '{"fields": ["total_advertising_space", "tenure", "discount"],"operation": "bodmas","equation": {"multiply": ["defaultValue", "total_advertising_space","tenure"],"percentageDiscount": ["discount"]},"defaultValue": "4500"}', '1'),
(403, 34, 33, 12, '', '', '0'),
(404, 34, 34, 13, '', '', '0'),
(405, 34, 35, 14, '', '', '0'),
(406, 34, 36, 15, '', '', '0'),
(407, 34, 37, 16, '', '', '0'),
(408, 34, 101, 0, '', '', '0'),
(410, 34, 61, 5, '', '', '0'),
(411, 34, 64, 17, '', '', '0'),
(412, 34, 30, 8, '', '', '0'),
(413, 34, 102, 0, '', '{"fields": ["omd_type_private_list"],"operation": "onSelect","options": {"Standard OMDs": ["4.6 x 3.05", "6.1 x 3.05"],"Unipole": ["4.5 x 2.25", "5.0 x 2.5"],"Large Size Billboards": ["7.6 x 3.8", "10.0 x 5.0"]},"defaultValue": ""}', '0'),
(414, 34, 103, 1, '', '', '0'),
(415, 34, 104, 6, '', '{"fields": ["tenure"],"operation": "discountCalculate","data": [{"6": 2},{"9": 3},{"12": 5}],"defaultValue": ""}', '0'),
(416, 23, 104, 6, '', '{"fields": ["tenure"],"operation": "discountCalculate","data": [{"6": 2},{"9": 3},{"12": 5}],"defaultValue": ""}', '0'),
(417, 28, 103, 1, '', '', '0'),
(418, 28, 104, 2, '', '{"fields": ["tenure"],"operation": "discountCalculate","data": [{"6": 2},{"9": 3},{"12": 5}],"defaultValue": ""}', '0'),
(419, 29, 103, 1, '', '', '0'),
(420, 29, 104, 2, '', '{"fields": ["tenure"],"operation": "discountCalculate","data": [{"6": 2},{"9": 3},{"12": 5}],"defaultValue": ""}', '0'),
(421, 27, 101, 0, '', '', '0'),
(422, 27, 102, 0, '', '{"fields": ["omd_type_private_list"],"operation": "onSelect","options": {"Standard OMDs": ["4.6 x 3.05", "6.1 x 3.05"],"Unipole": ["4.5 x 2.25", "5.0 x 2.5"],"Large Size Billboards": ["7.6 x 3.8", "10.0 x 5.0"]},"defaultValue": ""}', '0'),
(423, 27, 15, 1, '', '', '0'),
(424, 27, 103, 1, '', '', '0'),
(425, 27, 17, 2, '', '{"fields":["omd_private_dimensions"],"operation":"dimensionChange","defaultValue" : "1"}', '0'),
(426, 27, 18, 3, '', '{"fields":["omd_private_dimensions"],"operation":"dimensionChange","defaultValue" : "1"}', '0'),
(427, 27, 12, 4, '{"operationalMultyValidation": {"defaultValue": "0.2","field": ["total_facade_area", "omd_private_dimensions"],"0": {"operator": "*","defaultValue": "0.20","unit": "Facade","oprand": "greater","title": "Total Advertising Space"},"1": {"operator": "***","defaultValue": "","unit": "Facade","oprand": "greater","title": "Total Advertising Space"}}}', '{"fields":["omd_height","omd_width"],"operation":"multiply","defaultValue" : "1"}', '1'),
(428, 27, 61, 5, '', '', '0'),
(429, 27, 104, 6, '', '{"fields": ["tenure"],"operation": "discountCalculate","data": [{"6": 2},{"9": 3},{"12": 5}],"defaultValue": ""}', '0'),
(430, 27, 105, 7, '', '{"fields": ["total_advertising_space", "tenure", "discount"],"operation": "bodmas","equation": {"multiply": ["defaultValue", "total_advertising_space","tenure"],"percentageDiscount": ["discount"]},"defaultValue": "13500"}', '1'),
(431, 27, 7, 8, '', '', '0'),
(432, 27, 30, 8, '', '', '0'),
(433, 27, 4, 9, '', '', '0'),
(434, 27, 5, 10, '', '', '0'),
(435, 27, 3, 11, '', '', '0'),
(436, 27, 33, 12, '', '', '0'),
(437, 27, 34, 13, '', '', '0'),
(438, 27, 35, 14, '', '', '0'),
(439, 27, 36, 15, '', '', '0'),
(440, 27, 37, 16, '', '', '0'),
(441, 27, 64, 17, '', '', '0'),
(442, 26, 104, 6, '', '{"fields": ["tenure"],"operation": "discountCalculate","data": [{"6": 2},{"9": 3},{"12": 5}],"defaultValue": ""}', '0'),
(443, 26, 30, 8, '', '', '0'),
(444, 26, 64, 17, '', '', '0'),
(445, 26, 61, 5, '', '', '0'),
(446, 26, 103, 0, '', '', '0'),
(447, 26, 37, 16, '', '', '0'),
(448, 26, 36, 15, '', '', '0'),
(449, 26, 35, 14, '', '', '0'),
(450, 26, 34, 13, '', '', '0'),
(451, 26, 33, 12, '', '', '0'),
(452, 26, 105, 7, '', '{"fields": ["omd_height","omd_width", "tenure", "discount"],"operation": "bodmas","equation": {"multiply": ["defaultValue", "omd_height","omd_width","tenure"],"percentageDiscount": ["discount"]},"defaultValue": "3000"}', '1'),
(453, 26, 7, 8, '', '', '0'),
(454, 26, 3, 11, '', '', '0'),
(455, 26, 5, 10, '', '', '0'),
(456, 26, 4, 9, '', '', '0'),
(457, 26, 12, 4, '{"operationalMultyValidation": {"defaultValue": "0.2","field": ["total_facade_area"],"0": {"operator": "*","defaultValue": "0.20","unit": "Facade","oprand": "greater","title": "Total Advertising Space"}}}', '{"fields":["omd_height","omd_width"],"operation":"multiply","defaultValue" : "1"}', '1'),
(458, 26, 18, 3, '', '', '0'),
(459, 26, 17, 2, '', '', '0'),
(460, 26, 15, 1, '', '', '0');

-- --------------------------------------------------------

--
-- Table structure for table `old_fields`
--

CREATE TABLE IF NOT EXISTS `old_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lable` varchar(255) NOT NULL,
  `field_key` varchar(255) NOT NULL DEFAULT '0',
  `type` enum('text','date','select','radio','checkbox','file','hidden','json') NOT NULL DEFAULT 'text',
  `options` longtext COMMENT 'JSON Format. Eg: ["Yes","No"] | ["OPtion1","Option2","Option3"]',
  `is_disabled` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'if ''1'', set the input field as disabled',
  `default_validations` longtext COMMENT 'JSON Format. Eg: {"noEmpty":"true","minLimit":5,"maxLimit":10,"isNum":"ture","isAlphabit":"true","isAlphanumeric":"true"}',
  `default_calculations` longtext COMMENT 'JSON Format. Eg: {"fields":["filedKey1","filedKey2"],"operation":"multiply"}',
  `info` text NOT NULL,
  `group_in` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='saved device input fileds' AUTO_INCREMENT=106 ;

--
-- Dumping data for table `old_fields`
--

INSERT INTO `old_fields` (`id`, `lable`, `field_key`, `type`, `options`, `is_disabled`, `default_validations`, `default_calculations`, `info`, `group_in`) VALUES
(1, 'Size (in sq./ft)', 'size', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(2, 'No. of Panels', 'panels', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(3, 'Location', 'location', 'text', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(4, 'Latitude', 'lat', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(5, 'Longitude', 'lng', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(6, 'Concession Fees', 'concession_fees', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(7, 'Lighting (Lit/Unlit)', 'lighting', 'radio', '["Yes", "No"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(8, 'Material Specification', 'material_specification', 'text', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(9, 'Inside Metro', 'inside_metro', 'radio', '["Yes", "No"]', '0', '{"noEmpty": false,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(10, 'Outside Metro', 'outside_metro', 'radio', '["Yes", "No"]', '0', '{"noEmpty": false,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(11, 'No of Units', 'units', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(12, 'Total Advertising Space (in sq metre.)', 'total_advertising_space', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(13, 'Type of Vehicle', 'type_of_vehicle', 'select', '["Radio Taxi","Auto"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(14, 'License Fee', 'licence_fee', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(15, 'Total Area of Facade (sq metre.)', 'total_facade_area', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(16, 'OMD Id', 'omd_id', 'text', NULL, '0', NULL, NULL, '', '1'),
(17, 'OMD Height (sq metre.)', 'omd_height', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(18, 'OMD Width (sq metre.)', 'omd_width', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(19, 'Total Annual Permission Fees (in Rs.)', 'annual_license_fees', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(20, 'Venue Location', 'venue_location', 'text', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(21, 'Duration of Event (In Days)', 'event_duration', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(22, 'No. of OMD at Venue', 'omd_no_venue', 'text', NULL, '0', '', NULL, '', '1'),
(23, 'No. of OMD at Other Location', 'omd_no_others', 'text', NULL, '0', '', NULL, '', '1'),
(24, 'Size of OMD', 'omd_size', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(25, 'Self Advertising', 'self_advertising', 'hidden', NULL, '0', NULL, NULL, '', '1'),
(26, 'Total No of Screens', 'no_of_screens', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(27, 'No of Screens Applied', 'no_of_screens_applied', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(28, 'No of Months Applied for', 'no_of_months_applied_for', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(29, 'Type of OMD site', 'building_type[]', 'checkbox', '["OMDS installed under one roof","OMDs installed within premises but in open courtyard"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(30, 'Zone', 'zone', 'select', '["Zone1","Zone2","Zone3","Zone4"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(31, 'No. of Panels Inside Metro', 'panels_in_metro', 'text', NULL, '0', '{"noEmpty": false,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(32, 'No. of Panels Outside Metro', 'panels_out_metro', 'text', NULL, '0', '{"noEmpty": false,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(33, 'Latest property tax receipt', 'propert_tax_receipt', 'file', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, 'Property tax receipt/ G8 where OMD is to be installed', '2'),
(34, 'Drawing of the site', 'site_drawings', 'file', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, 'Drawing showing the locality plan, in colour, indicating the proposed positions (GPS coordinates) of the OMDs and the distances in relation to any other structure, building, OMD situated within the radius of 25 m from the proposed OMDs', '2'),
(35, 'Specifications', 'site_specifications', 'file', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, 'Complete specifications showing the dimensions of the OMDs and locations', '2'),
(36, 'Certificate of Structural Engineer', 'structural_engineer_certificate', 'file', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, 'Certifying safety aspects (for more details please refer Byelaw 5(1)(v) )', '2'),
(37, 'Architectural Drawing', 'architectural_drawing', 'file', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, 'Architectural drawings showing the elevation and measurements of the building and measurement and position of every existing and proposed OMDs drawn on a scale of 1:1000.', '2'),
(38, 'The applicant firm/company has no pending dues', 'has_pending_dues', 'radio', '["Yes", "No"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '2'),
(39, 'The applicant firm/company has no court case pending', 'pending_court_case', 'radio', '["Yes", "No"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '2'),
(40, 'Size (in sq./ft)', 'size_inside_metro', 'text', NULL, '0', '{"noEmpty": false,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(41, 'Size (in sq./ft)', 'size_outside_metro', 'text', NULL, '0', '{"noEmpty": false,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(42, 'OMD 1 Size at Venue', 'omd_no_venue_size_1', 'text', NULL, '0', NULL, NULL, '', '1'),
(43, 'OMD 2 Size at Venue', 'omd_no_venue_size_2', 'text', NULL, '0', NULL, NULL, '', '1'),
(44, 'OMD 3 Size at Venue', 'omd_no_venue_size_3', 'text', NULL, '0', NULL, NULL, '', '1'),
(45, 'OMD 4 Size at Venue', 'omd_no_venue_size_4', 'text', NULL, '0', NULL, NULL, '', '1'),
(46, 'OMD 5 Size at Venue', 'omd_no_venue_size_5', 'text', NULL, '0', NULL, NULL, '', '1'),
(47, 'OMD 6 Size at Venue', 'omd_no_venue_size_6', 'text', NULL, '0', NULL, NULL, '', '1'),
(48, 'OMD 1 Size at others', 'omd_no_others_size_1', 'text', NULL, '0', NULL, NULL, '', '1'),
(49, 'OMD 2 Size at others', 'omd_no_others_size_2', 'text', NULL, '0', NULL, NULL, '', '1'),
(50, 'OMD 3 Size at others', 'omd_no_others_size_3', 'text', NULL, '0', NULL, NULL, '', '1'),
(51, 'OMD 4 Size at others', 'omd_no_others_size_4', 'text', NULL, '0', NULL, NULL, '', '1'),
(52, 'OMD 5 Size at others', 'omd_no_others_size_5', 'text', NULL, '0', NULL, NULL, '', '1'),
(53, 'OMD 6 Size at others', 'omd_no_others_size_6', 'text', NULL, '0', NULL, NULL, '', '1'),
(54, 'Size Of OMD( Not More than 75% of the Facade Area)', 'omd_size_less_75', 'radio', '["Yes", "No"]', '0', NULL, NULL, '', '1'),
(55, 'Distance From Intersection(100 m away From Intersection) ', 'intersection_distance', 'radio', '["Yes", "No"]', '0', NULL, NULL, '', '1'),
(56, 'Distance between two OMDs(minimum 50 metres)', 'two_omd_distance', 'radio', '["Yes", "No"]', '0', NULL, NULL, '', '1'),
(57, 'Obstruction to Road User/Pedestrian', 'obstruction_to_road', 'radio', '["Yes", "No"]', '0', NULL, NULL, '', '1'),
(58, 'Rejection Reason', 'reason', 'text', NULL, '0', NULL, NULL, '', '1'),
(59, 'Name and Address of the multiplex/cinema', 'multiplex_name_address', 'text', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(60, 'Type of OMD', 'omd_type_list', 'select', '["Small Portraits","Posters","Super 8’s","Portraits","Supersites","Spectacular"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(61, 'Height of the building', 'building_height', 'text', NULL, '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(62, 'Type of OMD', 'omd_type_wall', 'select', '["Wall Wrap"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(63, 'OMD Price', 'omd_price', 'text', NULL, '1', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(64, 'Agreement between Site Owner and Agency', 'site_owner_agency_agreement', 'file', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, 'Agreement between Site Owner and Agency', '2'),
(65, 'OMDS installed under one roof', 'omd_one_roof', 'json', NULL, '0', NULL, NULL, '', '1'),
(66, 'OMDs installed within premises but in open courtyard', 'omd_premises', 'json', NULL, '0', NULL, NULL, '', '1'),
(67, 'Type of OMD site', 'building_type', 'text', NULL, '0', NULL, NULL, '', '1'),
(68, 'Metro/MRTS Type', 'metro_mrts_categories', 'select', '["Rolling Stock","Inside Station","Station branding"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(69, 'Metro MRTS', 'metro_table', 'json', NULL, '0', NULL, NULL, '', '1'),
(70, 'Event Start Date', 'event_start_date', 'date', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(71, 'OMD Fees at Venue', 'omd_fees_venue', 'text', NULL, '1', '{"noEmpty": false,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(72, 'OMD Fees at Other Location', 'omd_fees_other_location', 'text', NULL, '1', '{"noEmpty": false,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(73, 'Total OMD Fees', 'total_omd_fees', 'text', NULL, '1', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(74, 'Total OMD Fees at Venue', 'total_omd_fees_venue', 'text', NULL, '1', NULL, NULL, '', '1'),
(75, 'Total OMD Fees at Other Location', 'total_omd_fees_other_location', 'text', NULL, '1', NULL, NULL, '', '1'),
(76, 'OMD Other Location 1', 'omd_no_others_location_1', 'text', NULL, '0', NULL, NULL, '', '1'),
(77, 'OMD Other Location 2', 'omd_no_others_location_2', 'text', NULL, '0', NULL, NULL, '', '1'),
(78, 'OMD Other Location 3', 'omd_no_others_location_3', 'text', NULL, '0', NULL, NULL, '', '1'),
(79, 'OMD Other Location 4', 'omd_no_others_location_4', 'text', NULL, '0', NULL, NULL, '', '1'),
(80, 'OMD Other Location 5', 'omd_no_others_location_5', 'text', NULL, '0', NULL, NULL, '', '1'),
(81, 'OMD Other Location 6', 'omd_no_others_location_6', 'text', NULL, '0', NULL, NULL, '', '1'),
(82, 'Shelters Type', 'shelters_type', 'select', '["Bus Shelters","IPT Shelters"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(83, 'Concessioning Authority', 'concessionaire_type', 'select', '["MCG","Other"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(84, 'Name of Concessioning Authority', 'concessionaire_name', 'text', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(85, 'Route markers type', 'route_markers_type', 'select', '["Bus route markers","IPT route markers"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(86, 'Asset Type', 'foot_over_type', 'select', '["Foot over bridges","Signage gantries","Toilet Blocks","Urinals"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(87, 'Asset Type', 'cycle_station_type', 'select', '["Cycle station"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(88, 'Asset Type', 'police_booth_type', 'select', '["Police booth","Parking booth","Telephone booth","Prepaid taxi Booth","Public utility kiosks","Drinking water facility","Bus/train booking facility","Outside colonies to facilitate directory/payment of bills"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(89, 'Asset Type', 'sitting_bench_type', 'select', '["Sitting Bench","Garbage bin","Others"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(90, 'Shelter Data', 'shelter_data', 'json', NULL, '0', NULL, NULL, '', '1'),
(91, 'Bus route markers', 'route_markers_data', 'json', NULL, '0', NULL, NULL, '', ''),
(92, 'Foot Over Data', 'foot_over_data', 'json', NULL, '0', NULL, NULL, '', '1'),
(93, 'Cycle Station Data', 'cycle_station_data', 'json', NULL, '0', NULL, NULL, '', '1'),
(94, 'Police Booth Data', 'police_booth_data', 'json', NULL, '0', NULL, NULL, '', '1'),
(95, 'Sitting Bench Data', 'sitting_bench_data', 'json', NULL, '0', NULL, NULL, '', '1'),
(96, 'Approved Architectural Drawing', 'approved_architectural_drawing', 'file', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, 'Approved Architectural Drawing', '2'),
(97, 'Approved specifications', 'approved_specifications', 'file', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, 'Approved specifications', '2'),
(98, 'Approved drawing of the site', 'approved_drawing_of_site', 'file', NULL, '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, 'Approved drawing of the site', '2'),
(99, 'OMD Type', 'omd_type_type_a', 'select', '["Unipole","Wall Wrap","Wall Mounted"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(100, 'Typology A Others', 'other_type_a_data', 'json', NULL, '0', NULL, NULL, '', '1'),
(101, 'Type of OMD', 'omd_type_private_list', 'select', '["Standard OMDs", "Unipole", "Large Size Billboards"]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(102, 'Dimensions Range (in metres)', 'omd_private_dimensions', 'select', '[]', '0', '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}', NULL, '', '1'),
(103, 'Tenure (In Months) (Min 3 and max 36 months)', 'tenure', 'text', '', '0', '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false,"minValue": 3,"maxValue": 36,"isInteger": true}', NULL, '', '1'),
(104, 'Discount (in Percentage)', 'discount', 'text', NULL, '1', NULL, NULL, '', '1'),
(105, 'Total Permission Fees ( in Rupees )', 'total_permission_fees', 'text', NULL, '0', NULL, NULL, '', '1');

-- --------------------------------------------------------

--
-- Table structure for table `old_registration_documents`
--

CREATE TABLE IF NOT EXISTS `old_registration_documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `field` varchar(255) NOT NULL,
  `is_required` int(11) NOT NULL DEFAULT '1',
  `entity` varchar(255) NOT NULL,
  `related_to` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `old_registration_documents`
--

INSERT INTO `old_registration_documents` (`id`, `name`, `field`, `is_required`, `entity`, `related_to`) VALUES
(2, 'PAN Card Copy', 'income_proof', 1, '1,2,3', ''),
(3, 'TIN no', 'tin_no', 1, '2,3,4,5,6', ''),
(4, 'Service tax no', 'service_tax', 1, '2,3,4,5,6', ''),
(5, 'Income tax return for the last year', 'income_tax_return', 1, '1,2,3,4,5,6', ''),
(6, 'Partnership Deed', 'partnership_deed', 1, '3', ''),
(7, 'Address Proof of the Partnership Firm', 'address_proof_partnership_firm', 1, '3', ''),
(8, 'Partnership Registration Certificate (if Registered Partnership)', 'partnership_registration_certificate', 0, '3', ''),
(9, 'PAN Card of the Partners', 'pan_card_partners', 1, '4', ''),
(10, 'PAN card of LLP', 'pan_card_llp', 1, '4', ''),
(11, 'DIN numbers', 'din_number', 1, '4,5', ''),
(12, 'Registration certificate of LLP', 'registration_certificate_llp', 1, '4', ''),
(13, 'Address proof of LLP', 'address_proof_llp', 1, '4', ''),
(14, 'Address Proof of the Partners', 'address_proof_partners', 1, '4', ''),
(15, 'Audited Balance sheet of last three years(if applicable)', 'audited_balance_sheet', 0, '4,5,6', ''),
(16, 'Authorization/power of attorney for authorised signatory/person', 'power_of_attorney', 1, '4,5,6', ''),
(17, 'Memorandum of Association', 'association_memorandum', 1, '4,5,6', ''),
(18, 'Undertaking of non-Blacklisting', 'undertaking_non_blacklisting', 1, '1,2,3,4,5,6', ''),
(19, 'Undertaking of No dues pending with any municipality of Haryana', 'no_dues_pending', 1, '1,2,3,4,5,6', ''),
(20, 'Experience in the field of advertising in last three years', 'advertising_experience', 1, '1,2,3,4,5,6', ''),
(21, 'Details of advertisement rights/permissions secured in last 5 years in any of the municipality of Haryana', 'details_advertisement_permissions', 1, '1,2,3,4,5,6', ''),
(22, 'Curriculum Vitae', 'partner_cv', 1, '1,2,3,4,5,6', ''),
(23, 'PAN Card of the directors', 'directors_pan_card', 1, '5', ''),
(24, 'PAN card of Company', 'company_pan_card', 1, '5', ''),
(25, 'Registration certificate of company', 'company_registration_certificate', 1, '5', ''),
(26, 'Address proof of company', 'company_address_proof', 1, '5', ''),
(27, 'Address Proof of the Directors', 'directors_address_proof', 1, '5', ''),
(28, 'Articles of Association', 'association_articles', 1, '5,6', ''),
(29, 'Curriculum Vitae of each Director', 'director_cv', 1, '5', ''),
(30, 'PAN Card of the Organization', 'organization_pan_card', 1, '6', ''),
(31, 'Registration certificate of Organization', 'organization_registration_certificate', 1, '6', ''),
(32, 'Address proof of Organisation', 'organisation_address_proof', 1, '6', ''),
(33, 'Copy of board resolution by the Board of Directors', 'board_resolution_copy', 1, '5', '');

-- --------------------------------------------------------

--
-- Table structure for table `omd_additional_docs`
--

CREATE TABLE IF NOT EXISTS `omd_additional_docs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `reason` text NOT NULL,
  `is_uploaded` enum('0','1') NOT NULL DEFAULT '0',
  `filename` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=63 ;

--
-- Dumping data for table `omd_additional_docs`
--

INSERT INTO `omd_additional_docs` (`id`, `request_id`, `name`, `reason`, `is_uploaded`, `filename`, `created`, `modified`) VALUES
(1, 1, 'Agreement', 'Copy of agreement with owner reqd. which is not attached', '1', '1506578713_98cCOG5hNrK2gC7cLvSA_1.pdf', '2017-09-27 18:43:29', '2017-09-28 11:35:13'),
(2, 1, 'Specification', 'Specification of OMD be defined alongwith sizes', '1', '1506578727_JOCOGrzG1QQcxgzz0cWV_2.pdf', '2017-09-27 18:43:29', '2017-09-28 11:35:27'),
(3, 2, 'Drawing', 'Not attached', '1', '1507201323_Puf1qZuuvmC2RYVhGCjh_3.pdf', '2017-10-04 18:59:18', '2017-10-05 16:32:03'),
(4, 2, 'Specification', 'not defined', '1', '1507201360_2QmUYhFwolIWEsmEBn6u_4.jpg', '2017-10-04 18:59:18', '2017-10-05 16:32:40'),
(5, 3, 'Drawing', 'not attached', '1', '1507201118_lkgqOyBfvUt0A6O9nKCZ_5.pdf', '2017-10-04 19:00:21', '2017-10-05 16:28:38'),
(6, 3, 'Specification', 'not defined', '1', '1507201132_ywkcOCNjuIt6wHlaXxoh_6.jpg', '2017-10-04 19:00:21', '2017-10-05 16:28:52'),
(7, 4, 'agreement', 'not attached', '1', '1507186231_1YwCFetktmX2OigTy7zA_7.pdf', '2017-10-04 19:01:51', '2017-10-05 12:20:31'),
(8, 4, 'specification', 'not defined', '1', '1507186257_vld7eXCzZhQ1WacF2yx6_8.pdf', '2017-10-04 19:01:51', '2017-10-05 12:20:57'),
(9, 5, 'revised specification if any', 'contradictory to size specified', '1', '1508999706_1pR9EFwhtKaaSJlkczIy_9.pdf', '2017-10-10 18:55:21', '2017-10-26 12:05:06'),
(10, 6, 'structural drawing , architectural plan,specifications, detail of site etc.', 'not already attached', '0', '', '2017-12-01 11:33:05', '2017-12-01 11:33:05'),
(11, 7, 'Architectural plan', ' attached online', '1', '1517895059_3BIC9yMRPG8b0An7VNts_11.jpeg', '2018-02-06 10:42:45', '2018-02-06 11:00:59'),
(12, 7, 'Location of site plan', ' attached online', '1', '1517895134_k283uv4nACPQIpQcZTgF_12.jpeg', '2018-02-06 10:42:45', '2018-02-06 11:02:14'),
(13, 8, 'Architectural Drawing', 'required', '1', '1519214262_ZqvRo6aAphsBBKegm1Xj_13.jpg', '2018-02-20 16:19:19', '2018-02-21 17:27:42'),
(14, 8, 'sanctioned commercial building plan', 'required', '1', '1519214291_DELOnAGvo2JIZrLX5AEn_14.pdf', '2018-02-20 16:19:19', '2018-02-21 17:28:11'),
(15, 9, 'Agreement Not attached/Uploaded Online between M/sRapid Metro or M/s Interactive Communication Services (I) Pvt Ltd.', 'Agreement Not attached/Uploaded Online between M/sRapid Metro or M/s Interactive Communication Services (I) Pvt Ltd.', '0', '', '2018-03-17 10:55:46', '2018-03-17 10:55:46'),
(16, 10, 'drawing', 'not submitted', '0', '', '2018-04-06 15:26:05', '2018-04-06 15:26:05'),
(17, 10, 'specifications', 'not submitted', '0', '', '2018-04-06 15:26:05', '2018-04-06 15:26:05'),
(18, 10, 'certificate from structural engineer', 'not submitted', '0', '', '2018-04-06 15:26:05', '2018-04-06 15:26:05'),
(19, 11, 'Architectural Plan', 'Please attach', '0', '', '2018-05-01 14:09:11', '2018-05-01 14:09:11'),
(20, 11, 'Site Plan', 'Please attach', '0', '', '2018-05-01 14:09:11', '2018-05-01 14:09:11'),
(21, 11, 'Location Map', 'Please attach', '0', '', '2018-05-01 14:09:11', '2018-05-01 14:09:11'),
(22, 12, 'Pan Card', 'Mandatory', '1', '1527851352_oGKQCEqLsCc6fQ9757wq_22.jpg', '2018-06-01 16:38:55', '2018-06-01 16:39:12'),
(23, 13, 'Pan Card', 'Mandatory', '1', '1528184309_CdIKD6Jz8fT23NRUz9P9_23.jpg', '2018-06-05 13:08:19', '2018-06-05 13:08:29'),
(24, 14, 'Pan Card', 'Mandatory', '1', '1528185855_9IyBVUb4c3Bcr0UvpYbC_24.jpg', '2018-06-05 13:34:07', '2018-06-05 13:34:15'),
(25, 15, 'Pan Card', 'no reason', '1', '1528189705_wcmSK0VWOks1XAUNIw3X_25.jpg', '2018-06-05 14:38:14', '2018-06-05 14:38:25'),
(26, 16, 'Pan Card', 'No Reason', '1', '1528190661_AGeorWs4An7B2vkNbDrv_26.jpg', '2018-06-05 14:54:12', '2018-06-05 14:54:21'),
(27, 16, 'Aadhar Card', 'No Reason', '1', '1528190668_lp6kjdVOAVLbCZz3V27w_27.jpg', '2018-06-05 14:54:12', '2018-06-05 14:54:28'),
(28, 17, 'Pan Card', 'reason', '1', '1528196973_YRX4Z8Uj6DkDoIbeRFfQ_28.jpg', '2018-06-05 16:39:25', '2018-06-05 16:39:33'),
(29, 17, 'New Do do do', 'reason', '1', '1528196975_F6uEJXli9eR85Pd5X7o4_29.jpg', '2018-06-05 16:39:25', '2018-06-05 16:39:35'),
(30, 18, 'Pan Card', 'Reason', '1', '1528259550_pwm05eeEgEtsMpAaY2j0_30.jpg', '2018-06-06 10:02:22', '2018-06-06 10:02:30'),
(31, 18, 'No No No', 'No Reason', '1', '1528259552_cVCdadWsKkI9R4aWjoBz_31.jpg', '2018-06-06 10:02:22', '2018-06-06 10:02:32'),
(32, 19, 'Ding ding', 'no no ', '1', '1528260683_gyTUYIWZXKAhXJ11Z1mQ_32.jpg', '2018-06-06 10:21:16', '2018-06-06 10:21:23'),
(33, 19, 'no on', 'on no', '1', '1528260685_VFj7tqpLYLefk89iQ6hO_33.jpg', '2018-06-06 10:21:16', '2018-06-06 10:21:25'),
(34, 20, 'qwerty form', 'qwerty', '1', '1528276726_RRGRJYE0CNnWtYJfzG12_34.jpg', '2018-06-06 14:48:38', '2018-06-06 14:48:46'),
(35, 21, 'Adhar card', 'Photo & details has not visible.', '0', '', '2018-06-22 11:46:10', '2018-06-22 11:46:10'),
(36, 22, 'pan card', 'Photo & details has not visible.', '1', '1529653823_SSIyqxampejnsKsyMFnB_36.jpg', '2018-06-22 13:19:19', '2018-06-22 13:20:23'),
(37, 23, 'abc', 'test', '1', '1531376835_1HHmvDap4qfa8ZeDwlO2_37.jpg', '2018-07-12 11:54:46', '2018-07-12 11:57:15'),
(38, 23, 'xyz', 'test 1', '1', '1531376840_d6jHqEZXFy8tUDv6tBxx_38.pdf', '2018-07-12 11:54:46', '2018-07-12 11:57:20'),
(39, 23, '1', 'test 2', '1', '1531376855_rqZQcPWgt5ORs7ZSpZ3L_39.pdf', '2018-07-12 11:54:46', '2018-07-12 11:57:35'),
(40, 24, 'a', 'a', '1', '1531382086_ABTALgjLG97lzcSDJraO_40.jpg', '2018-07-12 13:24:17', '2018-07-12 13:24:46'),
(41, 24, 'a', 'a', '1', '1531382088_lcfzaXsbE0VfCPPn598L_41.jpg', '2018-07-12 13:24:17', '2018-07-12 13:24:48'),
(42, 24, 'a', 'a', '1', '1531382090_ig7RsZubqF0MRfm2dOeS_42.jpg', '2018-07-12 13:24:17', '2018-07-12 13:24:50'),
(43, 24, 'a', 'a', '1', '1531382092_O97qYXN47WQpdXhGWMRn_43.jpg', '2018-07-12 13:24:17', '2018-07-12 13:24:52'),
(44, 25, 'wd', 'dd', '0', '', '2018-07-12 16:04:23', '2018-07-12 16:04:23'),
(45, 26, 'tg', 'gtg', '1', '1531396299_v3jte13sI2DQ1yU2xsVp_45.png', '2018-07-12 17:20:55', '2018-07-12 17:21:39'),
(46, 26, 'gg', 'gg', '1', '1531396302_Na6TLs4fq0dIyPmm3irJ_46.png', '2018-07-12 17:20:55', '2018-07-12 17:21:42'),
(47, 26, 'tg', 'gym', '1', '1531396306_mwPkemQRRxHFHOytgDIG_47.png', '2018-07-12 17:20:55', '2018-07-12 17:21:46'),
(48, 27, 'Pan Card', 'mana', '1', '1531823184_txz11IRORffCQVRr3rfG_48.jpg', '2018-07-17 15:55:58', '2018-07-17 15:56:24'),
(49, 27, 'test demo', 'dfgdf', '1', '1531823187_NMd64IhccNnQKBug76iR_49.jpg', '2018-07-17 15:55:58', '2018-07-17 15:56:27'),
(50, 28, 'Pan Card', 'mana', '1', '1531823250_eTYs2KOhmSKPqjRs2JgT_50.jpg', '2018-07-17 15:57:02', '2018-07-17 15:57:30'),
(51, 29, 'driveing licence', 'TEST4', '1', '1532082138_i5bAj6zAHUxDvQak4qks_51.jpg', '2018-07-20 15:50:57', '2018-07-20 15:52:18'),
(52, 29, 'ABC', 'TEST5', '1', '1532082143_BGqEchvGnlYFr9fKgPkY_52.jpg', '2018-07-20 15:50:57', '2018-07-20 15:52:23'),
(53, 30, 'sws', 'sws', '0', '', '2018-09-11 13:06:59', '2018-09-11 13:06:59'),
(54, 30, 'sws', 'wdw', '0', '', '2018-09-11 13:06:59', '2018-09-11 13:06:59'),
(55, 31, 'frgr', 'rt', '1', '1536735399_9m2S7h3rC7ZmhdEKwGm3_55.png', '2018-09-12 12:11:42', '2018-09-12 12:26:39'),
(56, 31, 'rt', 'rt', '1', '1536735401_QDDM0nNG6CkfZm76Ebyg_56.jpg', '2018-09-12 12:11:42', '2018-09-12 12:26:41'),
(57, 31, 'tr', 'ett', '1', '1536735403_onrMdwoqgZEV8AGilFj1_57.png', '2018-09-12 12:11:42', '2018-09-12 12:26:43'),
(58, 32, '1', '1', '0', '', '2018-09-13 18:48:59', '2018-09-13 18:48:59'),
(59, 32, '2', '2', '0', '', '2018-09-13 18:48:59', '2018-09-13 18:48:59'),
(60, 32, '3', '3', '0', '', '2018-09-13 18:48:59', '2018-09-13 18:48:59'),
(61, 32, '4', '4', '0', '', '2018-09-13 18:48:59', '2018-09-13 18:48:59'),
(62, 33, 'Adhaar card', 'Its mandatory document', '1', '1539321776_vHWLoLs0EpRKqyiV6FyX_62.jpg', '2018-10-12 10:52:01', '2018-10-12 10:52:56');

-- --------------------------------------------------------

--
-- Table structure for table `omd_additional_doc_requests`
--

CREATE TABLE IF NOT EXISTS `omd_additional_doc_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `omd_additional_doc_requests`
--

INSERT INTO `omd_additional_doc_requests` (`id`, `device_id`, `created`, `modified`) VALUES
(1, 18, '2017-09-27 18:43:29', '2017-09-27 18:43:29'),
(2, 26, '2017-10-04 18:59:18', '2017-10-04 18:59:18'),
(3, 21, '2017-10-04 19:00:21', '2017-10-04 19:00:21'),
(4, 18, '2017-10-04 19:01:51', '2017-10-04 19:01:51'),
(5, 37, '2017-10-10 18:55:21', '2017-10-10 18:55:21'),
(6, 75, '2017-12-01 11:33:05', '2017-12-01 11:33:05'),
(7, 80, '2018-02-06 10:42:45', '2018-02-06 10:42:45'),
(8, 84, '2018-02-20 16:19:19', '2018-02-20 16:19:19'),
(9, 105, '2018-03-17 10:55:46', '2018-03-17 10:55:46'),
(10, 100, '2018-04-06 15:26:05', '2018-04-06 15:26:05'),
(11, 93, '2018-05-01 14:09:11', '2018-05-01 14:09:11'),
(12, 216, '2018-06-01 16:38:55', '2018-06-01 16:38:55'),
(13, 220, '2018-06-05 13:08:19', '2018-06-05 13:08:19'),
(14, 221, '2018-06-05 13:34:07', '2018-06-05 13:34:07'),
(15, 222, '2018-06-05 14:38:14', '2018-06-05 14:38:14'),
(16, 223, '2018-06-05 14:54:12', '2018-06-05 14:54:12'),
(17, 224, '2018-06-05 16:39:25', '2018-06-05 16:39:25'),
(18, 225, '2018-06-06 10:02:22', '2018-06-06 10:02:22'),
(19, 226, '2018-06-06 10:21:16', '2018-06-06 10:21:16'),
(20, 227, '2018-06-06 14:48:38', '2018-06-06 14:48:38'),
(21, 242, '2018-06-22 11:46:10', '2018-06-22 11:46:10'),
(22, 244, '2018-06-22 13:19:19', '2018-06-22 13:19:19'),
(23, 260, '2018-07-12 11:54:46', '2018-07-12 11:54:46'),
(24, 262, '2018-07-12 13:24:17', '2018-07-12 13:24:17'),
(25, 270, '2018-07-12 16:04:23', '2018-07-12 16:04:23'),
(26, 274, '2018-07-12 17:20:55', '2018-07-12 17:20:55'),
(27, 277, '2018-07-17 15:55:58', '2018-07-17 15:55:58'),
(28, 277, '2018-07-17 15:57:02', '2018-07-17 15:57:02'),
(29, 279, '2018-07-20 15:50:57', '2018-07-20 15:50:57'),
(30, 310, '2018-09-11 13:06:59', '2018-09-11 13:06:59'),
(31, 311, '2018-09-12 12:11:42', '2018-09-12 12:11:42'),
(32, 316, '2018-09-13 18:48:59', '2018-09-13 18:48:59'),
(33, 317, '2018-10-12 10:52:01', '2018-10-12 10:52:01');

-- --------------------------------------------------------

--
-- Table structure for table `omd_payments`
--

CREATE TABLE IF NOT EXISTS `omd_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `payment_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `panels`
--

CREATE TABLE IF NOT EXISTS `panels` (
  `id` char(36) NOT NULL DEFAULT '',
  `request_id` char(36) NOT NULL,
  `panel` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `element` varchar(255) DEFAULT NULL,
  `summary` varchar(255) DEFAULT NULL,
  `content` blob,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_panel` (`request_id`,`panel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `payment_id` varchar(255) NOT NULL,
  `payment_type` varchar(255) NOT NULL,
  `type_id` varchar(255) NOT NULL,
  `tracking_id` varchar(255) NOT NULL,
  `bank_ref_no` varchar(255) NOT NULL,
  `order_status` varchar(255) NOT NULL,
  `failure_message` text NOT NULL,
  `payment_mode` varchar(255) NOT NULL,
  `card_name` varchar(255) NOT NULL,
  `status_code` varchar(255) NOT NULL,
  `status_message` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `billing_name` varchar(255) NOT NULL,
  `billing_address` varchar(255) NOT NULL,
  `billing_city` varchar(255) NOT NULL,
  `billing_state` varchar(255) NOT NULL,
  `billing_zip` varchar(255) NOT NULL,
  `billing_country` varchar(255) NOT NULL,
  `billing_tel` varchar(255) NOT NULL,
  `billing_email` varchar(255) NOT NULL,
  `delivery_name` varchar(255) NOT NULL,
  `delivery_address` varchar(255) NOT NULL,
  `delivery_city` varchar(255) NOT NULL,
  `delivery_state` varchar(255) NOT NULL,
  `delivery_zip` varchar(255) NOT NULL,
  `delivery_country` varchar(255) NOT NULL,
  `delivery_tel` varchar(255) NOT NULL,
  `merchant_param1` varchar(255) NOT NULL,
  `merchant_param2` varchar(255) NOT NULL,
  `merchant_param3` varchar(255) NOT NULL,
  `merchant_param4` varchar(255) NOT NULL,
  `merchant_param5` varchar(255) NOT NULL,
  `vault` varchar(255) NOT NULL,
  `offer_type` varchar(255) NOT NULL,
  `offer_code` varchar(255) NOT NULL,
  `discount_value` varchar(255) NOT NULL,
  `mer_amount` varchar(255) NOT NULL,
  `eci_value` varchar(255) NOT NULL,
  `retry` varchar(255) NOT NULL,
  `response_code` varchar(255) NOT NULL,
  `billing_notes` text NOT NULL,
  `trans_date` datetime NOT NULL,
  `bin_country` varchar(255) NOT NULL,
  `response` longtext NOT NULL,
  `g8_response` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_code` varchar(255) NOT NULL COMMENT 'belongs to company',
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `is_primary` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'only one profile will be the primary user profile for a company',
  `status` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'used to track active/blocked record',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `registration_documents`
--

CREATE TABLE IF NOT EXISTS `registration_documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `field` varchar(255) NOT NULL,
  `is_required` int(11) NOT NULL DEFAULT '1',
  `entity` varchar(255) NOT NULL,
  `related_to` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `registration_documents`
--

INSERT INTO `registration_documents` (`id`, `name`, `field`, `is_required`, `entity`, `related_to`) VALUES
(2, 'PAN Card Copy', 'income_proof', 1, '1,2,3', ''),
(3, 'TIN no', 'tin_no', 1, '2,3,4,5,6', ''),
(4, 'Service tax no', 'service_tax', 1, '2,3,4,5,6', ''),
(5, 'Income tax return for the last year', 'income_tax_return', 1, '1,2,3,4,5,6', ''),
(6, 'Partnership Deed', 'partnership_deed', 1, '3', ''),
(7, 'Address Proof of the Partnership Firm', 'address_proof_partnership_firm', 1, '3', ''),
(8, 'Partnership Registration Certificate (if Registered Partnership)', 'partnership_registration_certificate', 0, '3', ''),
(9, 'PAN Card of the Partners', 'pan_card_partners', 1, '4', ''),
(10, 'PAN card of LLP', 'pan_card_llp', 1, '4', ''),
(11, 'DIN numbers', 'din_number', 1, '4,5', ''),
(12, 'Registration certificate of LLP', 'registration_certificate_llp', 1, '4', ''),
(13, 'Address proof of LLP', 'address_proof_llp', 1, '4', ''),
(14, 'Address Proof of the Partners', 'address_proof_partners', 1, '4', ''),
(15, 'Audited Balance sheet of last three years(if applicable)', 'audited_balance_sheet', 0, '4,5,6', ''),
(16, 'Authorization/power of attorney for authorised signatory/person', 'power_of_attorney', 1, '4,5,6', ''),
(17, 'Memorandum of Association', 'association_memorandum', 1, '4,5,6', ''),
(18, 'Undertaking of non-Blacklisting', 'undertaking_non_blacklisting', 1, '1,2,3,4,5,6', ''),
(19, 'Undertaking of No dues pending with any municipality of Haryana', 'no_dues_pending', 1, '1,2,3,4,5,6', ''),
(20, 'Experience in the field of advertising in last three years', 'advertising_experience', 1, '1,2,3,4,5,6', ''),
(21, 'Details of advertisement rights/permissions secured in last 5 years in any of the municipality of Haryana', 'details_advertisement_permissions', 1, '1,2,3,4,5,6', ''),
(22, 'Curriculum Vitae', 'partner_cv', 1, '1,2,3,4,5,6', ''),
(23, 'PAN Card of the directors', 'directors_pan_card', 1, '5', ''),
(24, 'PAN card of Company', 'company_pan_card', 1, '5', ''),
(25, 'Registration certificate of company', 'company_registration_certificate', 1, '5', ''),
(26, 'Address proof of company', 'company_address_proof', 1, '5', ''),
(27, 'Address Proof of the Directors', 'directors_address_proof', 1, '5', ''),
(28, 'Articles of Association', 'association_articles', 1, '5,6', ''),
(29, 'Curriculum Vitae of each Director', 'director_cv', 1, '5', ''),
(30, 'PAN Card of the Organization', 'organization_pan_card', 1, '6', ''),
(31, 'Registration certificate of Organization', 'organization_registration_certificate', 1, '6', ''),
(32, 'Address proof of Organisation', 'organisation_address_proof', 1, '6', ''),
(33, 'Copy of board resolution by the Board of Directors', 'board_resolution_copy', 1, '5', ''),
(34, 'Undertaking of blacklisting', 'undertaking_of_blacklisting', 1, '0', ''),
(35, 'No dues pending with MCG', 'no_dues_pending_with_mcg', 1, '0', '');

-- --------------------------------------------------------

--
-- Table structure for table `representatives`
--

CREATE TABLE IF NOT EXISTS `representatives` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `representative` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `representatives`
--

INSERT INTO `representatives` (`id`, `representative`, `created`, `modified`) VALUES
(1, 'Individual/Owner', '2016-12-27 10:30:12', '2016-12-27 10:30:12'),
(2, 'Sole Proprietorship', '2016-12-27 10:30:12', '2016-12-27 10:30:12'),
(3, 'Partnership', '2016-12-27 10:30:12', '2016-12-27 10:30:12'),
(4, 'Limited Liability Partnership (LLP)', '2016-12-27 10:30:12', '2016-12-27 10:30:12'),
(5, 'Company', '2016-12-27 10:30:12', '2016-12-27 10:30:12'),
(6, 'Others Not for Profit(Company/Society/Trust)', '2016-12-27 10:30:12', '2016-12-27 10:30:12');

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE IF NOT EXISTS `requests` (
  `id` char(36) NOT NULL,
  `url` varchar(255) NOT NULL,
  `content_type` varchar(255) DEFAULT NULL,
  `status_code` int(11) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `requested_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `security_qus`
--

CREATE TABLE IF NOT EXISTS `security_qus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(500) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `security_qus`
--

INSERT INTO `security_qus` (`id`, `question`, `created`, `modified`) VALUES
(1, 'What was Your School Name ?', '2016-12-26 11:30:16', '2016-12-26 11:30:16'),
(2, 'What is Your Nick Name ?', '2016-12-26 11:30:16', '2016-12-26 11:30:16'),
(3, 'Your Home Town Name ?', '2016-12-26 11:30:16', '2016-12-26 11:30:16'),
(4, 'What is Your Pet Name ?', '2016-12-26 11:30:16', '2016-12-26 11:30:16'),
(5, 'What is Your Dream Job ?', '2016-12-26 11:30:16', '2016-12-26 11:30:16');

-- --------------------------------------------------------

--
-- Table structure for table `super_admins`
--

CREATE TABLE IF NOT EXISTS `super_admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `level` (`level`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `super_admins`
--

INSERT INTO `super_admins` (`id`, `name`, `email`, `password`, `level`, `created`, `modified`) VALUES
(1, 'Level 1', 'level1@smc.com', '$2y$10$MP3ld1VSVfNKe5I.iD6EUeRWTjhQ3VYKIV.Hm3l7nHMzuPbR/8hMi', 1, '2017-04-24 13:04:12', '2017-04-24 13:04:12'),
(2, 'Level 2', 'level2@smc.com', '$2y$10$GJITu4JvTXMrmxgMcLg2G.znBM5CRIan3WUmzIb4EcOKh8p0juyuC', 2, '2017-04-24 13:04:40', '2017-04-24 13:04:40'),
(3, 'Level 3', 'level3@smc.com', '$2y$10$hphg8bvaHouTYrPfmRYe6uZFHe7FBZRbfGsYwfdTQV6/ZOpQ1M4Am', 3, '2017-04-24 13:05:05', '2017-04-24 13:05:05');

-- --------------------------------------------------------

--
-- Table structure for table `tenders`
--

CREATE TABLE IF NOT EXISTS `tenders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `department_name` varchar(255) NOT NULL,
  `issuing_authority` varchar(255) NOT NULL,
  `authorised_by` varchar(255) NOT NULL,
  `device_id` int(11) NOT NULL,
  `user_segment` int(11) NOT NULL,
  `experience` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `end_date_updated` datetime NOT NULL,
  `application_date` date NOT NULL,
  `doc_download_date` datetime NOT NULL,
  `bid_doc` varchar(255) NOT NULL,
  `min_amount` double NOT NULL,
  `document_fee` double NOT NULL,
  `tender_value` double NOT NULL,
  `email` varchar(255) NOT NULL,
  `contact` bigint(20) NOT NULL,
  `web_link` varchar(255) NOT NULL,
  `doc_names` text NOT NULL COMMENT 'String Separetd by #$@*',
  `publish_type` enum('0','1') DEFAULT '0' COMMENT '0 => Automatic, 1 => Manually',
  `publish_status` enum('0','1') NOT NULL DEFAULT '0',
  `is_awarded` enum('0','1') NOT NULL DEFAULT '0',
  `terms_cond` longtext NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tender_comments`
--

CREATE TABLE IF NOT EXISTS `tender_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tender_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `stage` int(11) NOT NULL,
  `comment` text NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tender_devices`
--

CREATE TABLE IF NOT EXISTS `tender_devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tender_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tender_documents`
--

CREATE TABLE IF NOT EXISTS `tender_documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tender_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tender_users`
--

CREATE TABLE IF NOT EXISTS `tender_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tender_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `display_name` varchar(500) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `type` enum('user','MCG1','MCG2','MCG3','MCG4','MCG5','admin','assistant_engineer','deputy_engineer','town_planner','deputy_commissioner','guest') NOT NULL DEFAULT 'user',
  `zone` varchar(255) NOT NULL,
  `security_qus_id` int(255) NOT NULL,
  `answer` text NOT NULL,
  `company_code` varchar(255) NOT NULL,
  `representative_id` int(11) NOT NULL,
  `user_token` varchar(250) NOT NULL,
  `otp` varchar(10) NOT NULL,
  `is_mobile_verified` enum('0','1') NOT NULL DEFAULT '0',
  `rejection_reason` varchar(500) NOT NULL,
  `comments` text NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT 'used to track active/blocked record',
  `paid` enum('0','1') NOT NULL DEFAULT '0',
  `admin_approved` enum('0','1','2','3') NOT NULL DEFAULT '0' COMMENT '''1''=>''Approve by ZTO4'',''2''=>''Approve by MCG4'',''3''=>''Hold by ZTO4''',
  `security_hash` varchar(300) NOT NULL,
  `doc_uploaded` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=259 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `display_name`, `user_name`, `email`, `mobile`, `password`, `type`, `zone`, `security_qus_id`, `answer`, `company_code`, `representative_id`, `user_token`, `otp`, `is_mobile_verified`, `rejection_reason`, `comments`, `status`, `paid`, `admin_approved`, `security_hash`, `doc_uploaded`, `created`, `modified`) VALUES
(2, 'ZTO 1', '', 'zto1@mcf.gov.in', '9463678910', '$2y$10$7HieeHJuVhDrzORV2ks/XeEJaGl9NU1sjFH4coVCyPRmLBSiR7WwC', 'MCG1', 'Zone1', 1, 'test', '', 0, '', '0', '0', '', '', '1', '1', '0', '', 0, '2017-01-02 18:12:33', '2018-07-11 12:35:11'),
(7, 'AE', '', 'aeadvt@mcf.gov.in', '9463678910', '$2y$10$.g5CwyAKkt18tkI6HoVgaOOdtz4sgafaARSlgUNXr3/.5xhTIlebe', 'MCG2', '', 1, 'test', '', 0, '', '0852', '0', '', '', '1', '1', '0', '87c1e97c0925d13e8eaf3276fec6769atxrKfF', 0, '2017-01-12 12:36:07', '2018-07-12 11:08:30'),
(8, 'EE', '', 'eeadvt@mcf.gov.in', '9463678910', '$2y$10$tjzUsCbRinMzAHJ7R8FumuC3t9iSz22K5QGatSDtcm2Av1yzHhLYy', 'MCG3', '', 2, 'demo', '', 0, '', '0', '0', '', '', '1', '1', '0', '', 0, '2017-01-12 12:37:32', '2018-01-16 11:08:31'),
(12, 'ADMINISTRATOR', '', 'itmanagermcf', '9463678910', '$2y$10$v87s3Ajyfm6f4DAqSHHNYuq1/8Qi3yUW7rx/lfPtjfgWROCcVRnJ6', 'admin', '', 1, 'test', '', 0, '', '0', '0', '', '', '1', '1', '0', '', 0, '2017-01-18 13:05:02', '2017-01-18 13:05:02'),
(41, 'JC', '', 'jcadvt@mcf.gov.in', '9780697567', '$2y$10$l1JQ8KwMMIG6uYndXtWbNOIaQbAOZ8UewzukeTIkIJLFgOuK3Ghc2', 'MCG4', '', 0, '', '', 0, '', '', '0', '', '', '1', '0', '0', '', 0, '2017-06-20 15:15:25', '2018-06-21 11:44:39'),
(91, 'ZTO 2', '', 'zto2@mcf.gov.in', '9463678910', '$2y$10$/p9LT8.ldvutapU.m/Pj4.RUt9cN1cRyW70HdftqtxjFi.aJSVylm', 'MCG1', 'Zone2', 0, '', '', 0, '', '', '0', '', '', '1', '0', '0', '', 0, '2017-08-02 16:56:01', '2018-01-16 10:53:10'),
(92, 'ZTO 3', '', 'zto3@mcf.gov.in', '9463678910', '$2y$10$K3A6.yLffRNZAymBJV3MZePb2gcOo1VM1dI3FNjdLYjZDF4yAl23C', 'MCG1', 'Zone3', 0, '', '', 0, '', '', '0', '', '', '1', '0', '0', '', 0, '2017-08-02 16:57:03', '2018-01-16 10:55:46'),
(93, 'ZTO 4', '', 'zto4@mcf.gov.in', '9463678910', '$2y$10$B0mNhKBdSmIX96numec4eOH9DIxkd9L1ixcw./Fu2AmvT1HRZ9GNG', 'MCG1', 'Zone4', 0, '', '', 0, '', '', '0', '', '', '1', '0', '0', '', 0, '2017-08-02 16:57:45', '2018-01-31 11:58:09'),
(233, 'Rohit', '', 'rohit@signitysolutions.com', '7696316890', '$2y$10$9QimHlSd9UTJDVRx2k5oj.KOlHdephOTSrAKQuzOH5q2tFjZmT/Rq', 'user', '', 2, 'ROHIT', '', 1, '', '', '1', '', '', '1', '1', '2', '', 1, '2018-07-19 13:37:29', '2018-07-20 15:03:15'),
(254, 'Kp', '', 'kp@yopmail.com', '9814701830', '$2y$10$57/THYWrJwK4jNy8/QQZOu5qs7up2J4dOZmb7HDRBGhRFEY0Xha1a', 'MCG1', 'Zone1', 0, '', '', 0, '', '', '0', '', '', '1', '0', '0', '', 0, '2018-08-27 16:21:54', '2018-08-27 16:21:54');

-- --------------------------------------------------------

--
-- Table structure for table `user_additional_docs`
--

CREATE TABLE IF NOT EXISTS `user_additional_docs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `reason` text NOT NULL,
  `is_uploaded` enum('0','1') NOT NULL DEFAULT '0',
  `filename` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_additional_doc_requests`
--

CREATE TABLE IF NOT EXISTS `user_additional_doc_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_directors`
--

CREATE TABLE IF NOT EXISTS `user_directors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `director_user_id` int(11) NOT NULL,
  `director_name` varchar(255) NOT NULL,
  `din` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `user_directors`
--

INSERT INTO `user_directors` (`id`, `director_user_id`, `director_name`, `din`) VALUES
(1, 55, 'Mr. Gour Gupta', ''),
(2, 68, 'Ajay Kumar', ''),
(3, 73, 'Saroj Gupta', ''),
(4, 74, 'Prashant Luthra', ''),
(5, 74, 'V.K. Luthra', ''),
(6, 83, 'Kamalpreet singh Anand', ''),
(7, 90, 'Dharamvir singh', ''),
(8, 115, 'Arshad Nizam', '00646862'),
(9, 122, 'Veena Vasudeva', '05324491'),
(10, 123, 'Anoop Singh', '06744784'),
(11, 124, 'UMA SHARMA', '07131797'),
(12, 144, 'PUSHPA DHINGRA', '05145744'),
(13, 172, 'RISHABH TELANG', '07322550'),
(14, 172, 'ANKIT NAGORI', '06672135'),
(15, 174, 'Pooja Patnaik', '07656717'),
(16, 229, 'Director 2', 'Din 2'),
(17, 229, 'Director 3', 'Din 3');

-- --------------------------------------------------------

--
-- Table structure for table `user_documents`
--

CREATE TABLE IF NOT EXISTS `user_documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `field` varchar(255) NOT NULL,
  `document` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_profiles`
--

CREATE TABLE IF NOT EXISTS `user_profiles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_profile_id` int(11) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_address` varchar(300) NOT NULL,
  `correspondence_address` text NOT NULL,
  `director_name` varchar(255) NOT NULL,
  `din` varchar(255) NOT NULL,
  `company_mobile` bigint(20) NOT NULL,
  `company_email` varchar(255) NOT NULL,
  `annual_turnover` int(11) NOT NULL,
  `experience` int(11) NOT NULL,
  `SMC_party_code` varchar(255) NOT NULL,
  `incorporation_date` datetime NOT NULL,
  `company_password` varchar(255) NOT NULL,
  `property_id` varchar(255) NOT NULL,
  `property_tax_receipt` varchar(300) NOT NULL,
  `income_tax_return` varchar(300) NOT NULL,
  `din_number` varchar(300) NOT NULL,
  `income_proof` varchar(300) NOT NULL,
  `voter_id` varchar(255) NOT NULL,
  `adhaar_card` varchar(300) NOT NULL,
  `service_tax` varchar(300) NOT NULL,
  `turnover_certificate` varchar(300) NOT NULL,
  `exp_certificate` varchar(300) NOT NULL,
  `balance_sheet` varchar(300) NOT NULL,
  `letter_by_board_directors` varchar(300) NOT NULL,
  `incorporation_certificate` varchar(300) NOT NULL,
  `association_memorandum` varchar(300) NOT NULL,
  `association_articles` varchar(300) NOT NULL,
  `ration_card` varchar(300) NOT NULL,
  `driving_license` varchar(300) NOT NULL,
  `passport` varchar(300) NOT NULL,
  `tin_no` varchar(300) NOT NULL,
  `partnership_deed` varchar(300) NOT NULL,
  `address_proof_partnership_firm` varchar(300) NOT NULL,
  `partnership_registration_certificate` varchar(300) NOT NULL,
  `pan_card_partners` varchar(300) NOT NULL,
  `pan_card_llp` varchar(300) NOT NULL,
  `registration_certificate_llp` varchar(300) NOT NULL,
  `address_proof_llp` varchar(300) NOT NULL,
  `address_proof_partners` varchar(300) NOT NULL,
  `audited_balance_sheet` varchar(300) NOT NULL,
  `power_of_attorney` varchar(300) NOT NULL,
  `authorised_contact_person` varchar(300) NOT NULL,
  `undertaking_non_blacklisting` varchar(300) NOT NULL,
  `no_dues_pending` varchar(300) NOT NULL,
  `advertising_experience` varchar(300) NOT NULL,
  `details_advertisement_permissions` varchar(300) NOT NULL,
  `partner_cv` varchar(300) NOT NULL,
  `directors_pan_card` varchar(300) NOT NULL,
  `company_pan_card` varchar(300) NOT NULL,
  `company_registration_certificate` varchar(300) NOT NULL,
  `company_address_proof` varchar(300) NOT NULL,
  `directors_address_proof` varchar(300) NOT NULL,
  `organization_pan_card` varchar(300) NOT NULL,
  `director_cv` varchar(300) NOT NULL,
  `organization_registration_certificate` varchar(300) NOT NULL,
  `organisation_address_proof` varchar(300) NOT NULL,
  `address_type` int(11) NOT NULL,
  `address_proof` varchar(300) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_profile_id` (`user_profile_id`),
  KEY `user_profile_id_2` (`user_profile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_segments`
--

CREATE TABLE IF NOT EXISTS `user_segments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `min_turnover` double NOT NULL,
  `max_turnover` double NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user_segments`
--

INSERT INTO `user_segments` (`id`, `name`, `min_turnover`, `max_turnover`, `created`, `modified`) VALUES
(1, 'Vendor Type A', 0, 2000000, '2017-03-09 10:06:06', '2017-03-09 10:06:06');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
