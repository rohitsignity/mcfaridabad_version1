<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SuperAdminsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SuperAdminsTable Test Case
 */
class SuperAdminsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SuperAdminsTable
     */
    public $SuperAdmins;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.super_admins'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SuperAdmins') ? [] : ['className' => 'App\Model\Table\SuperAdminsTable'];
        $this->SuperAdmins = TableRegistry::get('SuperAdmins', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SuperAdmins);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
