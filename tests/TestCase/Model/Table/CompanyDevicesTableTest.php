<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CompanyDevicesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CompanyDevicesTable Test Case
 */
class CompanyDevicesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CompanyDevicesTable
     */
    public $CompanyDevices;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.company_devices',
        'app.categories',
        'app.sub_categories',
        'app.company_device_fields'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CompanyDevices') ? [] : ['className' => 'App\Model\Table\CompanyDevicesTable'];
        $this->CompanyDevices = TableRegistry::get('CompanyDevices', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CompanyDevices);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
