<?php

namespace App\View;

use Cake\View\View;

class DashboardView extends View {

    public function initialize() {
        parent::initialize();
        // Load paginator Helper
        $this->loadHelper('Paginator', ['templates' => 'paginator-templates']);
    }

}
