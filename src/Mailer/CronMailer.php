<?php

namespace App\Mailer;

use Cake\Mailer\Mailer;

class CronMailer extends Mailer {

    public function omdPaymentReminder($companyDevice,$fees) {
        $data = $companyDevice;
        $data['fees'] = $fees;
        $this->from(['advt@mcg.gov.in' => 'Outdoor Media'])->viewVars($data)->to($companyDevice['email'])->emailFormat('html')->subject('Pending Payment [ Outdoor Media ]')->set(compact('data'));
    }
    
    public function omdInstallmentReminder($device,$installment) {
        $data['device'] = $device;
        $data['installment'] = $installment;
        $this->from(['advt@mcg.gov.in' => 'Outdoor Media'])->viewVars($data)->to($data['device']['email'])->emailFormat('html')->subject('Pending Installment Payment [ Outdoor Media ]')->set(compact('data'));
    }

}
