<?php

namespace App\Mailer;

use Cake\Mailer\Mailer;

class TenderMailer extends Mailer {

    public function tenderPublish($user, $tender) {
        $this->from(['donotreplyonme@gmail.com' => 'Outdoor Media'])->to($user['email'])->viewVars(['user' => $user, 'tender' => $tender])->emailFormat('html')->subject('Tender Published');
    }
    
    public function notifyAward($user,$tender) {
        $this->from(['donotreplyonme@gmail.com' => 'Outdoor Media'])->to($user['email'])->viewVars(['user' => $user,'tender' => $tender])->emailFormat('html')->subject('Congratulations - Tender Awarded');
    }
    
    public function applicationPayment($user,$tender_id) {
        $userData['user'] = $user;
        $devicePath = WWW_ROOT . 'uploads' . DS . 'receipt' . DS.'tender'.DS;
        $attachments['receipt.pdf'] = $devicePath.'tender_'.$tender_id.'_'.$userData['user']->id.'.pdf';
        $this
            ->from(['harjeet.signity@gmail.com' => 'Outdoor Media'])
            ->viewVars($userData)
            ->to($userData['user']->email)
//            ->to('yogesh@signitysolutions.in')
            ->emailFormat('html')
            ->attachments($attachments)
            ->subject('Outdoor-media tender application')
            ->set(compact('data'));
    }

}
