<?php

namespace App\Mailer;

use Cake\Mailer\Mailer;

class OmdMailer extends Mailer {

    public function notifyOmdApproved($user,$device,$fees=null) {
        $userData['user'] = $user;
        $userData['device'] = $device;
        if ($device['sub_category_id'] == 17 || $device['sub_category_id'] == 24 || (($device['sub_category_id'] == 26) && ($fees!=null && $fees == 0) ) ) {
        //if($device['sub_category_id'] == 17 || $device['sub_category_id'] == 24) {
            $devicePath = WWW_ROOT . 'uploads' . DS . 'permission_docs' . DS;
            $attachments['permission_letter.pdf'] = $devicePath . 'permission_letter_' . $device['id'] . '_' . $user['id'] . '.pdf';
        } else {
            $devicePath = WWW_ROOT . 'uploads' . DS . 'omd_letters' . DS;
            $attachments['approval_letter.pdf'] = $devicePath . 'omd_letter_' . $device['id'] . '_' . $user['id'] . '.pdf';
        }
        $this->from(['harjeet.signity@gmail.com' => 'Outdoor Media'])->viewVars($userData)->to($user['email'])->emailFormat('html')->attachments($attachments)->subject('Outdoor-media OMD application ( MCFOMD'.$device['id'].' ) Approved')->set(compact('data'));
    }
    
    public function notifyOmdRejected($user,$device) {
        $userData['user'] = $user;
        $userData['device'] = $device;
        $devicePath = WWW_ROOT . 'uploads' . DS . 'omd_letters' . DS;
        $attachments['approval_letter.pdf'] = $devicePath . 'omd_letter_' . $device->id . '_' . $user['id'] . '.pdf';
        $this->from(['harjeet.signity@gmail.com' => 'Outdoor Media'])->viewVars($userData)->to($user['email'])->emailFormat('html')->attachments($attachments)->subject('Outdoor-media OMD application ( MCFOMD'.$device->id.' ) Rejected')->set(compact('data'));
    }

}
