<?php

namespace App\Mailer;

use Cake\Mailer\Mailer;

class UserMailer extends Mailer {
		
    public function forgotPassword($user) {
		
        $this
			->from(['harjeet.signity@gmail.com' => 'Outdoor Media'])
            ->to($user['email'])
            ->emailFormat('html')
            ->subject('Password Reset')
            ->set(compact('user'));
    }
	

    public function emailVerification($user,$url) {
        $userData['user'] = $user;
        $userData['url'] = $url;
        
        $this
            ->from(['harjeet.signity@gmail.com' => 'Outdoor Media'])
            ->viewVars($userData)
            ->to($user['email'])
//            ->to('yogesh@signitysolutions.in')
            ->emailFormat('html')
            ->subject('Outdoor-media Registration')
            ->set(compact('user'));
    }
    
    public function pendingEmail($user) {
        $userData['user'] = $user;
        $devicePath = WWW_ROOT . 'uploads' . DS . 'receipt' . DS;
        $attachments['receipt.pdf'] = $devicePath.'receipt.pdf';
        $this
            ->from(['harjeet.signity@gmail.com' => 'Outdoor Media'])
            ->viewVars($userData)
            ->to($user->email)
//            ->to('yogesh@signitysolutions.in')
            ->emailFormat('html')
            ->attachments($attachments)
            ->subject('Outdoor-media Registration')
            ->set(compact('user'));
    }
    
    public function registrationReceipt($user) {
        $userData['user'] = $user;
        $devicePath = WWW_ROOT . 'uploads' . DS . 'receipt' . DS.'registration'.DS;
        $attachments['receipt.pdf'] = $devicePath.'registration_'.$user->id.'_'.$user->id.'.pdf';
        $this
            ->from(['harjeet.signity@gmail.com' => 'Outdoor Media'])
            ->viewVars($userData)
            ->to($user->email)
//            ->to('yogesh@signitysolutions.in')
            ->emailFormat('html')
            ->attachments($attachments)
            ->subject('Outdoor-media Registration')
            ->set(compact('user'));
    }
    
    public function deviceRegistrationReceipt($user) {
        $userData['user'] = $user['user'];
        $devicePath = WWW_ROOT . 'uploads' . DS . 'receipt' . DS.'device_registration'.DS;
        $attachments['receipt.pdf'] = $devicePath.'device_registration_'.$user['deviceId'].'_'.$user['user']['id'].'.pdf';
        $this
            ->from(['harjeet.signity@gmail.com' => 'Outdoor Media'])
            ->viewVars($userData)
            ->to($user['user']['email'])
//            ->to('yogesh@signitysolutions.in')
            ->emailFormat('html')
            ->attachments($attachments)
            ->subject('Outdoor-media Device Registration')
            ->set(compact('user'));
    }
    
    public function approvedEmail($user) {
        $userData['user'] = $user;
        
        $this
            ->from(['harjeet.signity@gmail.com' => 'Outdoor Media'])
            ->viewVars($userData)
            ->to($user->email)
            //->to('sakshi@signitysolutions.in')
            ->emailFormat('html')
            ->subject('Outdoor-media Approved Account')
            ->set(compact('user'));
    }
    
    public function rejectedEmail($user) {
        $userData['user'] = $user;
        
        $this
            ->from(['harjeet.signity@gmail.com' => 'Outdoor Media'])
            ->viewVars($userData)
            ->to($user->email)
            //->to('sakshi@signitysolutions.in')
            ->emailFormat('html')
            ->subject('Outdoor-media Rejected Account')
            ->set(compact('user'));
    }
    
    public function requestAdditionalDocs($user, $documents) {
        $userData['user'] = $user;
        $userData['documents'] = $documents;
        $this->from(['harjeet.signity@gmail.com' => 'Outdoor Media'])->viewVars($userData)->to($user['email'])->emailFormat('html')->subject('Outdoor-media requested additional documents')->set(compact('user'));
    }
    
}
