<?php

namespace App\Traits;

use Cake\ORM\TableRegistry;

trait Bids {

    protected function _getAwardedBidUser($tenderId) {
        $bidsTable = TableRegistry::get('bids');
        $awardedBid = $bidsTable->find('all')->select(['user_id' => 'bids.user_id', 'display_name' => 'u.display_name', 'email' => 'u.email', 'mobile' => 'u.mobile', 'user_name' => 'u.user_name', 'amount' => 'bids.amount'])->join([
            'ab' => [
                'table' => 'awards',
                'type' => 'RIGHT',
                'conditions' => 'bids.id = ab.bid_id'
            ],
            'u' => [
                'table' => 'users',
                'type' => 'LEFT',
                'conditions' => 'bids.user_id = u.id'
            ]
        ])->where(['bids.tender_id' => $tenderId])->hydrate(false)->first();
        return $awardedBid;
    }
    
    protected function _getAppliedBids($tenderId) {
        $appliedBidsTable = TableRegistry::get('AppliedBids');
        $appliedBids = $appliedBidsTable->find('all')->select([
            'AppliedBids.id',
            'AppliedBids.user_id',
            'display_name' => 'u.display_name',
            'user_name' => 'u.user_name',
            'email' => 'u.email',
            'mobile' => 'u.mobile',
            'AppliedBids.status',
        ])->join([
            'u' => [
                'table' => 'users',
                'type' => 'LEFT',
                'conditions' => 'AppliedBids.user_id = u.id'
            ]
        ])->where(['AppliedBids.tender_id' => $tenderId,'AppliedBids.paid' => '1'])->order(['AppliedBids.status' => 'asc','AppliedBids.created' => 'desc']);
        return $appliedBids;
    }
    
    protected function _getAppliedBid($appliedBidId) {
        $appliedBidsTable = TableRegistry::get('AppliedBids');
        $appliedBid = $appliedBidsTable->find('all')->select([
            'AppliedBids.id',
            'AppliedBids.user_id',
            'AppliedBids.tender_id',
            'display_name' => 'u.display_name',
            'user_name' => 'u.user_name',
            'email' => 'u.email',
            'mobile' => 'u.mobile',
            'AppliedBids.paid',
            'AppliedBids.status',
        ])->join([
            'u' => [
                'table' => 'users',
                'type' => 'LEFT',
                'conditions' => 'AppliedBids.user_id = u.id'
            ]
        ])->where(['AppliedBids.id' => $appliedBidId])->hydrate(false)->first();
        return $appliedBid;
    }
    
    protected function _getAppliedBidDocuments($appliedBidId) {
        $appliedBidsDocumentsTable = TableRegistry::get('AppliedBidsDocuments');
        $appliedBidsDocuments = $appliedBidsDocumentsTable->find('all')->where(['applied_bid_id' => $appliedBidId])->hydrate(false)->toArray();
        return $appliedBidsDocuments;
    }
    
    protected function _getBidApplicationStatus($tenderId,$userId) {
        $appliedBidsTable = TableRegistry::get('AppliedBids');
        $appliedBid = $appliedBidsTable->find('all')->where(['tender_id' => $tenderId,'user_id' => $userId,'paid' => '1'])->hydrate(false)->first();
        return $appliedBid;
    }

}
