<?php

namespace App\Traits;

use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
trait Devices {

    protected function _getDeviceData($deviceId) {
        $whereIn = '';
        if(is_array($deviceId)) {
            $whereIn = 'IN';
        }
        $devices = [];
        $fieldsKeysArr = [];
        $conn = ConnectionManager::get('default');
        $conn->execute('SET group_concat_max_len = 5555555');
        $deviceTable = TableRegistry::get('CompanyDevices');
        $devicesArr = $deviceTable->find('all')->select([
                    'id',
                    'device_name',
                    'company_code',
                    'status',
                    'created',
                    'modified' => 'lastComment.modified',
                    'company_name' => 'u.display_name',
                    'representative' => 'r.representative',
                    'user_id' => 'u.id',
                    'name' => 'u.display_name',
                    'phone' => 'u.mobile',
                    'email' => 'u.email',
                    'incorporation_date' => 'DATE(up.incorporation_date)',
                    'address' => 'up.company_address',
                    'correspondence_address' => 'up.correspondence_address',
                    'typology' => 'pc.title',
                    'sub_category' => 'sc.title',
                    'category_id' => 'CompanyDevices.category_id',
                    'sub_category_id' => 'CompanyDevices.sub_category_id',
                    'deviceData' => 'deviceData.fieldData',
                    'payment_id' => 'omdP.payment_id',
                ])->join([
                    'u' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' => 'CompanyDevices.user_id  = u.id AND CompanyDevices.by_admin = 0'
                    ],
                    'up' => [
                        'table' => 'user_profiles',
                        'type' => 'LEFT',
                        'conditions' => 'u.id = up.user_profile_id'
                    ],
                    'r' => [
                        'table' => 'representatives',
                        'type' => 'LEFT',
                        'conditions' => 'u.representative_id = r.id'
                    ],
                    'pc' => [
                        'table' => 'categories',
                        'type' => 'LEFT',
                        'conditions' => 'CompanyDevices.category_id = pc.id'
                    ],
                    'sc' => [
                        'table' => 'categories',
                        'type' => 'LEFT',
                        'conditions' => 'CompanyDevices.sub_category_id = sc.id'
                    ],
                    'omdP' => [
                        'table' => '(SELECT * FROM (SELECT * FROM `omd_payments` ORDER BY `id` DESC) tbl GROUP BY `device_id`)',
                        'type' => 'LEFT',
                        'conditions' => 'CompanyDevices.id = omdP.device_id'
                    ],
                    'deviceData' => [
                        'table' => "(SELECT `company_device_id`, GROUP_CONCAT(CONCAT(`field_key`,'*^$',`input_value`) SEPARATOR '#@!') AS fieldData FROM `company_device_fields` GROUP BY `company_device_id`)",
                        'type' => 'LEFT',
                        'conditions' => 'CompanyDevices.id = deviceData.company_device_id'
                    ],
                    'lastComment' => [
                        'table' => '(SELECT * FROM (SELECT * FROM `mcg_comments` ORDER BY `id` DESC ) AS tbl GROUP BY `device_id`)',
                        'type' => 'LEFT',
                        'conditions' => 'CompanyDevices.id = lastComment.device_id'
                    ]
                ])->where(['CompanyDevices.id '.$whereIn => $deviceId])->hydrate(false)->toArray();
        foreach ($devicesArr as $key => $devicesItem) {
            $devices[$key] = $devicesItem;
            $deviceDataArr = explode('#@!', $devicesItem['deviceData']);
            $deviceFields = [];
            foreach ($deviceDataArr as $deviceDataItem) {
                $deviceDataItemArr = explode('*^$', $deviceDataItem);
                $fieldsKeysArr[] = $deviceDataItemArr[0];
                $deviceFields[] = array(
                    'field' => $deviceDataItemArr[0],
                    'value' => $deviceDataItemArr[1]
                );
            }
            $devices[$key]['fields'] = $deviceFields;
            unset($devices[$key]['deviceData']);
        }
        $fieldsTable = TableRegistry::get('Fields');
        $fieldsArr = $fieldsTable->find('all')->select(['field_key', 'type', 'lable'])->where(['field_key IN' => $fieldsKeysArr])->hydrate(false);
        $fieldsData = $fieldsArr->toArray();
        if ($devices) {
            if(!is_array($deviceId)) {
                $response = reset($devices);
                $response['fieldsData'] = $fieldsData;
            } else {
                $response = array_map(function($arr) use($fieldsData){
                    $arr['fieldsData'] = $fieldsData;
                    return $arr;
                },$devices);
            }
            return $response;
        }
        return [];
    }

    protected function _getDevice($deviceId) {
        $deviceTable = TableRegistry::get('CompanyDevices');
        $device = $deviceTable->get($deviceId);
        return $device;
    }

    protected function _getPublicDevicesBySubCategory($subCategoryId) {
        $companyDevicesTable = TableRegistry::get('CompanyDevices');
        $companyDevices = $companyDevicesTable->find('all')->where(['sub_category_id' => $subCategoryId, 'devive_type' => '0', 'status' => 'approved'])->hydrate(false)->toArray();
        return $companyDevices;
    }
   
    
    protected function _getDevicesByIds($deviceIds) {
        $companyDevicesTable = TableRegistry::get('CompanyDevices');
        $companyDevices = $companyDevicesTable->find('all')->where(['id IN' => $deviceIds])->hydrate(false)->toArray();
        return $companyDevices;
    }
    
    protected function _getDeviceFieldValues($deviceId) {
        $companyDeviceFieldsTable = TableRegistry::get('CompanyDeviceFields');
        $companyDeviceFields = $companyDeviceFieldsTable->find('all')->where(['company_device_id' => $deviceId])->hydrate(false)->toArray();
        return $companyDeviceFields;
    }
    
    protected function _getTenureMonths($tenure) {
        $tenureData = [
            "3 months" => 3,
            "6 months" => 6,
            "9 months" => 9,
            "1 year" => 12,
            "2 years" => 24,
            "3 years" => 36
        ];
        return $tenureData[$tenure];
    }
    
    protected function _getDeviceInstallments($deviceId) {
        $installmentsTable = TableRegistry::get('Installments');
        $installments = $installmentsTable->find('all')->select(['id','device_id','amount','start_date','end_date','payment_id','created','modified','g8_response' => 'p.g8_response'])->join([
            'p' => [
                'table' => 'payments',
                'type' => 'LEFT',
                'conditions' => 'Installments.payment_id  = p.id'
            ]
        ])->where(['device_id' => $deviceId])->hydrate(false)->toArray();
        return $installments;
    }
    
    protected function _getDeviceInstallment($id) {
        $installmentsTable = TableRegistry::get('Installments');
        $installment = $installmentsTable->find('all')->where(['id' =>  $id])->hydrate(false)->first();
        return $installment;
    }
    
    protected function _getDevicePendingInstallmentCount() {
        $currentDate = date('Y-m-d');
//        $currentDate = '2019-07-02';
        $installmentsTable = TableRegistry::get('Installments');
        $installmentsCount = $installmentsTable->find('all')->where([
            ['start_date <' => $currentDate],
            ['payment_id' => 0]
        ])->order(['start_date' => 'DESC'])->hydrate(false)->count();
        return $installmentsCount;
    }

}
