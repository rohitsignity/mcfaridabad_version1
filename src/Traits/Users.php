<?php

namespace App\Traits;

use Cake\ORM\TableRegistry;
use Cake\Routing\Router;

trait Users {

    protected function _getUserData($userId) {
        $usersTable = TableRegistry::get('Users');
        $user = $usersTable->find('all')->select(['Users.id', 'Users.display_name', 'Users.email', 'Users.otp', 'Users.mobile', 'Users.is_mobile_verified', 'Users.paid', 'Users.admin_approved', 'Users.doc_uploaded','Users.company_code', 'Users.status', 'company_name' => 'c.company_name', 'representative' => 'r.representative', 'Users.representative_id', 'pan_number' => 'c.pan_number','is_token' => 'IF(Users.user_token = "",0,1)','Users.created'])->join([
                    'c' => [
                        'table' => 'companies',
                        'type' => 'LEFT',
                        'conditions' => 'Users.company_code = c.company_code'
                    ],
                    'r' => [
                        'table' => 'representatives',
                        'type' => 'LEFT',
                        'conditions' => 'Users.representative_id = r.id'
                    ]
                ])->where(['Users.id' => $userId])->hydrate(false)->first();
        return $user;
    }

    protected function _getUserByEmailId($email) {
        $usersTable = TableRegistry::get('Users');
        $user = $usersTable->find('all')->select(['Users.id', 'Users.email', 'Users.doc_uploaded', 'representative' => 'r.representative'])->join([
                    'r' => [
                        'table' => 'representatives',
                        'type' => 'LEFT',
                        'conditions' => 'Users.representative_id = r.id'
                    ]
                ])->where(['email' => trim($email), 'status !=' => '2'])->hydrate(false)->first();
        return $user;
    }

    protected function _getUserDocuments($userId) {
        $userDocumentsTable = TableRegistry::get('UserDocuments');
        $userDocuments = $userDocumentsTable->find('all')->where(['user_id' => $userId])->hydrate(false)->toArray();
        return $userDocuments;
    }

    protected function _getUsersDocumentsbyIds($userIds) {
        $response = [];
        if (count($userIds)) {
            $userDocumentsTable = TableRegistry::get('UserDocuments');
            $userDocuments = $userDocumentsTable->find('all')->where(['user_id IN' => $userIds])->hydrate(false)->toArray();
            $userDocumentFields = array_values(array_unique(array_filter(array_map(function($arr) {
                                        return $arr['field'];
                                    }, $userDocuments))));
            $registrationDocumentsTable = TableRegistry::get('RegistrationDocuments');
            $registrationDocuments = [];
            if ($userDocumentFields) {
                $registrationDocuments = $registrationDocumentsTable->find('all')->select(['name', 'field'])->where(['field IN' => $userDocumentFields])->hydrate(false)->toArray();
            }
            $userProfilesTable = TableRegistry::get('UserProfiles');
            $userAddressproofs = $userProfilesTable->find('all')->select(['user_id' => 'UserProfiles.user_profile_id', 'UserProfiles.address_proof', 'address_proof_label' => 'apt.name'])->join([
                        'apt' => [
                            'table' => 'address_profes',
                            'type' => 'LEFT',
                            'conditions' => 'UserProfiles.address_type = apt.id'
                        ]
                    ])->where(['UserProfiles.user_profile_id IN' => $userIds])->hydrate(false)->toArray();
            foreach ($userIds as $userId) {
                $userDocumentsArr = array_map(function($arr) use($registrationDocuments) {
                    $field = $arr['field'];
                    $fieldArr = array_values(array_filter($registrationDocuments, function($item) use($field) {
                                return $item['field'] == $field;
                            }));
                    return [
                        'user_id' => $arr['user_id'],
                        'label' => $fieldArr[0]['name'],
                        'field' => $field,
                        'document' => $arr['document'] ? Router::url('/uploads/register_documents/' . $arr['document']) : ''
                    ];
                }, array_filter($userDocuments, function($arr) use($userId) {
                            return $arr['user_id'] == $userId;
                        }));

                $userAddressproofArr = array_values(array_filter($userAddressproofs, function($arr) use($userId) {
                            return $arr['user_id'] == $userId;
                        }));
                if($userAddressproofArr) {
                    array_unshift($userDocumentsArr, ['user_id' => $userAddressproofArr[0]['user_id'], 'label' => $userAddressproofArr[0]['address_proof_label'], 'field' => 'address_proof', 'document' => Router::url('/uploads/register_documents/' . $userAddressproofArr[0]['address_proof'])]);
                }
                $response[$userId] = $userDocumentsArr;
            }
        }
        return $response;
    }

    protected function _getDocumentsByEntity($entityId) {
        $registrationDocumentsTable = TableRegistry::get('RegistrationDocuments');
        $registrationDocuments = $registrationDocumentsTable->find('all')->where(['FIND_IN_SET("' . $entityId . '",entity)'])->hydrate(false)->toArray();
        return $registrationDocuments;
    }

    protected function _getUserDetail($userId) {
        $usersTable = TableRegistry::get('Users');
        $user = $usersTable->find('all')->where(['id' => $userId])->hydrate(false)->first();
        return $user;
    }

    protected function _getUserOpenTenders($userId) {
        $userProfilesTable = TableRegistry::get('UserProfiles');
        $userProfile = $userProfilesTable->find('all')->where(['user_profile_id' => $userId])->hydrate(false)->first();
        $tendersTable = TableRegistry::get('Tenders');
        $openTenders = $tendersTable->find('all')->select(['id'])->where(['user_segment' => $userProfile['annual_turnover'], 'experience' => $userProfile['experience'], 'DATE(application_date) >=' => date('Y-m-d')])->hydrate(false)->toArray();
        $response = array_values(array_filter(array_map(function($arr) {
                            return $arr['id'];
                        }, $openTenders)));
        return $response;
    }

    protected function _getUserProfile($userId) {
        $userProfilesTable = TableRegistry::get('UserProfiles');
        $userProfile = $userProfilesTable->find('all')->where(['user_profile_id' => $userId])->hydrate(false)->first();
        return $userProfile;
    }

    protected function _getUserProfileDetail($userId) {
        $usersTable = TableRegistry::get('Users');
        $user = $usersTable->find('all')->select([
                    'Users.id',
                    'Users.display_name',
                    'Users.user_name',
                    'Users.email',
                    'Users.mobile',
                    'representative' => 'r.representative',
                    'company_name' => 'up.company_name',
                    'company_address' => 'up.company_address',
                    'correspondence_address' => 'up.correspondence_address',
                    'director_name' => 'up.director_name',
                    'din' => 'up.din',
                    'authorised_contact_person' => 'up.authorised_contact_person',
                    'SMC_party_code' => 'up.SMC_party_code',
                    'incorporation_date' => 'up.incorporation_date',
                    'annual_turnover' => 'at.annual_turnover',
                    'experience' => 'ex.name',
                    'income_proof' => 'up.income_proof',
                    'turnover_certificate' => 'up.turnover_certificate',
                    'exp_certificate' => 'up.exp_certificate'
                ])->join([
                    'up' => [
                        'table' => 'user_profiles',
                        'type' => 'LEFT',
                        'conditions' => 'Users.id = up.user_profile_id'
                    ],
                    'at' => [
                        'table' => 'annual_turnovers',
                        'type' => 'LEFT',
                        'conditions' => 'up.annual_turnover = at.id'
                    ],
                    'ex' => [
                        'table' => 'experiences',
                        'type' => 'LEFT',
                        'conditions' => 'up.experience = ex.id'
                    ],
                    'r' => [
                        'table' => 'representatives',
                        'type' => 'LEFT',
                        'conditions' => 'Users.representative_id = r.id'
                    ]
                ])->where(['Users.id' => $userId])->hydrate(false)->first();
        return $user;
    }

    protected function _checkAlldocumentsUploaded($userData) {
        $allDcouments = $this->_getDocumentsByEntity($userData['representative_id']);
        $userDocuments = $this->_getUserDocuments($userData['id']);
        $allDcoumentsFields = array_map(function($arr) {
            return $arr['field'];
        }, $allDcouments);
        $userDocumentsFields = array_map(function($arr) {
            return $arr['field'];
        }, $userDocuments);
        $pendingDocuments = array_diff($allDcoumentsFields, $userDocumentsFields);
        if (count($pendingDocuments)) {
            return false;
        }
        return true;
    }
    
     protected function _checkAllguestdocumentsUploaded($userData) {
        $allDcouments = $this->_getDocumentsByEntity(0);
        $userDocuments = $this->_getUserDocuments($userData['id']);
        $allDcoumentsFields = array_map(function($arr) {
            return $arr['field'];
        }, $allDcouments);
        $userDocumentsFields = array_map(function($arr) {
            return $arr['field'];
        }, $userDocuments);
        $pendingDocuments = array_diff($allDcoumentsFields, $userDocumentsFields);
        if (count($pendingDocuments)) {
            return false;
        }
        return true;
    }


    protected function _getUsersDirectors($userIds = []) {
        if(count($userIds)) {
            $userDirectorsTable = TableRegistry::get('UserDirectors');
            $userDirectors = $userDirectorsTable->find('all')->where(['director_user_id IN' => $userIds])->order(['id' => 'ASC'])->hydrate(false)->toArray();
            return $userDirectors;
        }
        return [];
    }
    
    protected function _getUsersAdditionalDocuments($userIds = []) {
        if(count($userIds)) {
            $userAdditionalDocsTable = TableRegistry::get('UserAdditionalDocs');
            $userAdditionalDocs = $userAdditionalDocsTable->find('all')->select(['UserAdditionalDocs.id','UserAdditionalDocs.name','UserAdditionalDocs.reason','UserAdditionalDocs.filename','user_id' => 'uadr.user_id','request_id'=> 'uadr.id','created' =>'uadr.created'])->join(['uadr' => [
                'table' => 'user_additional_doc_requests',
                'type' => 'LEFT',
                'conditions' => 'UserAdditionalDocs.request_id = uadr.id'
            ]])->where(['uadr.user_id IN' => $userIds])->order(['uadr.id' => 'DESC','UserAdditionalDocs.id' => 'DESC'])->hydrate(false)->toArray();
            return $userAdditionalDocs;
        }
        return [];
    }

}
