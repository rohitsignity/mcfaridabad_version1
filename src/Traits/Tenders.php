<?php

namespace App\Traits;

use Cake\ORM\TableRegistry;

trait Tenders {
    protected function _checkTenderExpired($tenderId) {
        $bidsTable = TableRegistry::get('Bids');
        $tendersTable = TableRegistry::get('Tenders');
        $tender = $tendersTable->find('all')->where(['id' => $tenderId])->hydrate(false)->first();
//        $expirationTime = date('Y-m-d H:i:s', strtotime($tender['end_date']));
        $expirationTime = date('Y-m-d H:i:s', strtotime($tender['end_date_updated']));
//        $lastBid = $bidsTable->find('all')->where(['tender_id' => $tenderId])->order(['id' => 'DESC'])->hydrate(false)->first();
//        if ($lastBid) {
//            $lastBidTime = date('Y-m-d H:i:s', strtotime($lastBid['created']));
//            $dayEndTime = date('Y-m-d 23:59:00', strtotime($tender['end_date']));
//            $to_time = strtotime($dayEndTime);
//            $from_time = strtotime($expirationTime);
//            $minutes = round(abs($to_time - $from_time) / 60, 2);
//            $intervals = ceil($minutes / 15) + 1;
//            $extensionStartTime = date('Y-m-d H:i:s', strtotime($tender['end_date'] . ' -15 minute'));
//            for ($i = 0; $i < $intervals; $i++) {
//                $addTime = ' +' . $this->__getAdditionalTimeLimit($i) . ' minute';
//                if ($lastBidTime >= $extensionStartTime && $lastBidTime <= date('Y-m-d H:i:s', strtotime($extensionStartTime . $addTime))) {
//                    $expirationTime = date('Y-m-d H:i:s', strtotime($tender['end_date'] . $addTime)) > $dayEndTime ? $dayEndTime : date('Y-m-d H:i:s', strtotime($tender['end_date'] . $addTime));
//                    break;
//                }
//            }
//        }
        if ($expirationTime < date('Y-m-d H:i:s')) {
            return true;
        }
        return false;
    }
    
    protected function _checkTenderExpiredNew($tender) {
        $expirationTime = date('Y-m-d H:i:s', strtotime($tender['end_date_updated']));
        if ($expirationTime < date('Y-m-d H:i:s')) {
            return true;
        }
        return false;
    }
    
    protected function _updateTenderExpiration($tender) {
        $extensionStartTime = date('Y-m-d H:i:s', strtotime($tender['end_date_updated'] . ' -15 minute'));
        if(date('Y-m-d H:i:s') > $extensionStartTime && date('Y-m-d H:i:s') < date('Y-m-d H:i:s', strtotime($tender['end_date_updated']))) {
            $newTime  = date('Y-m-d H:i:s', strtotime($tender['end_date_updated'] . ' +15 minute'));
            $tendersTable = TableRegistry::get('Tenders');
            $tendersTable->query()->update()->set(['end_date_updated' => $newTime])->where(['id' => $tender['id']])->execute();
        }
    }

    protected function _getTenderExpirationTime($tenderId) {
        $tendersTable = TableRegistry::get('Tenders');
//        $bidsTable = TableRegistry::get('Bids');
        $tender = $tendersTable->find('all')->where(['id' => $tenderId])->hydrate(false)->first();
        $expirationTime = date('Y-m-d H:i:s', strtotime($tender['end_date_updated']));
//        $lastBid = $bidsTable->find('all')->where(['tender_id' => $tenderId])->order(['id' => 'DESC'])->hydrate(false)->first();
//        if ($lastBid) {
//            $lastBidTime = date('Y-m-d H:i:s', strtotime($lastBid['created']));
//            $dayEndTime = date('Y-m-d 23:59:00', strtotime($tender['end_date']));
//            $to_time = strtotime($dayEndTime);
//            $from_time = strtotime($expirationTime);
//            $minutes = round(abs($to_time - $from_time) / 60, 2);
//            $intervals = ceil($minutes / 15) + 1;
//            $extensionStartTime = date('Y-m-d H:i:s', strtotime($tender['end_date'] . ' -15 minute'));
//            for ($i = 0; $i < $intervals; $i++) {
//                $addTime = ' +' . $this->__getAdditionalTimeLimit($i) . ' minute';
//                if ($lastBidTime >= $extensionStartTime && $lastBidTime <= date('Y-m-d H:i:s', strtotime($extensionStartTime . $addTime))) {
//                    $expirationTime = date('Y-m-d H:i:s', strtotime($tender['end_date'] . $addTime)) > $dayEndTime ? $dayEndTime : date('Y-m-d H:i:s', strtotime($tender['end_date'] . $addTime));
//                    break;
//                }
//            }
//        }
        return $expirationTime;
    }

    private function __getAdditionalTimeLimit($code) {
        return ($code + 1) * 15;
    }
    
    protected function _tenderDetail($tenderId) {
        $tendersTable = TableRegistry::get('Tenders');
        $tender = $tendersTable->find('all')->where(['id' => $tenderId])->hydrate(false)->first();
        return $tender;
    }
    
    protected function _getTenderUsers($tenderId) {
        $tenderUsersTable = TableRegistry::get('TenderUsers');
        $tenderUsers = $tenderUsersTable->find('all')->select(['id' => 'u.id','display_name' => 'u.display_name', 'user_name' => 'u.user_name'])->join([
            'u'=> [
                'table' => 'users',
                'type' => 'LEFT',
                'conditions' => 'TenderUsers.user_id = u.id'
            ]
        ])->where(['TenderUsers.tender_id' => $tenderId])->hydrate(false)->toArray();
        return $tenderUsers;
    }
    
    protected function _getTenderRequiredDocuments($tenderId) {
        $tenderDocumentsTable = TableRegistry::get('TenderDocuments');
        $tenderDocuments = $tenderDocumentsTable->find('all')->select(['id' => 'd.id', 'name' => 'd.name'])->join([
            'd' => [
                'table' => 'documents',
                'type' => 'LEFT',
                'conditions' => 'TenderDocuments.document_id = d.id'
            ]
        ])->where(['TenderDocuments.tender_id' => $tenderId])->hydrate(false)->toArray();
        return $tenderDocuments;
    }
    
    protected function _getUsersAnualTurnover($turnoverId) {
        $annualTurnoversTable = TableRegistry::get('AnnualTurnovers');
        $annualTurnover = $annualTurnoversTable->find('all')->where(['id' => $turnoverId])->hydrate(false)->first();
        return $annualTurnover;
    }
    
    protected function _getUsersExperience($experienceId) {
        $experiencesTable = TableRegistry::get('Experiences');
        $experience = $experiencesTable->find('all')->where(['id' => $experienceId])->hydrate(false)->first();
        return $experience;
    }
    
    protected function _getTenderDeviceIds($tenderId) {
        $tenderDevicesTable = TableRegistry::get('TenderDevices');
        $tenderDevices  = $tenderDevicesTable->find('all')->where(['tender_id' => $tenderId])->hydrate(false)->toArray();
        $tenderDevicesIds = array_map(function($arr){
            return $arr['device_id'];
        }, $tenderDevices);
        return $tenderDevicesIds;
    }

}
