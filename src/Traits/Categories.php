<?php

namespace App\Traits;

use Cake\ORM\TableRegistry;

trait Categories {

    protected function _getPublicCategories() {
        $categoriesTable = TableRegistry::get('Categories');
        $publicCategories = $categoriesTable->find('all')->where(['FIND_IN_SET("0",type)','parent_id' => 0])->hydrate(false)->toArray();
        return $publicCategories;
    }

    protected function _getSubCategories($categoryId) {
        $categoriesTable = TableRegistry::get('Categories');
        $subCategories = $categoriesTable->find('all')->where(['parent_id' => $categoryId])->hydrate(false)->toArray();
        return $subCategories;
    }

    protected function _getSubCategoriesArr($categoryId) {
        $response = [];
        $categoriesTable = TableRegistry::get('Categories');
        $subCategories = $categoriesTable->find('all')->where(['parent_id' => $categoryId])->hydrate(false)->toArray();
        array_walk($subCategories, function($arr) use(&$response){
            $response[$arr['id']] = $arr['title'];
        });
        return $response;
    }

}
