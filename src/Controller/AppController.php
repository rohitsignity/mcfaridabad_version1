<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Routing\Router;
use App\Traits\Devices;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    
    use Devices;

    protected $omdTypes = ['private' => 'Private', 'public' => 'Public'];
    protected $rolesArray = ['user' => 'user', 'MCG1' => 'MCG1', 'MCG2' => 'MCG2', 'MCG3' => 'MCG3', 'MCG4' => 'MCG4', 'MCG5' => 'MCG5'];
    protected $loggedIn = false;
    protected $user = [];
    protected $bidLimit = 3;
    protected $bidIncrimentPercentage = 10;
    
    protected $rolesPermissionArray = ['MCG3','MCG4'];

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login',
            ],
            'loginRedirect' => [
                'controller' => 'dashboard',
                'action' => 'index'
            ],
            'authError' => 'Did you really think you are allowed to see that?',
            'authenticate' => [
                'Form' => [
                    'userModel' => 'Users',
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ],
                    'scope' => array('Users.status' => 1,'Users.type !=' => 'admin','Users.type !=' => 'guest')
                ]
            ],
            'storage' => 'Session'
        ]);
        $this->Auth->allow(['forgotPassword']);
        $this->base_url = Router::url('/', true);

        $this->set('pendingInstallments', 0);
        $this->set('loggedIn', false);
        if ($this->Auth->user('id')) {
			//============= add by rohit =======================
			if($this->Auth->user('type') == 'admin' || $this->Auth->user('type') == 'guest' ){
				$this->Flash->error(__('Invalid Username/Password. Please try again!'));
				return $this->redirect($this->Auth->logout());
			}
			//===================================================
            $userType = strtolower(preg_replace('/[0-9]+/', '', $this->Auth->user('type')));
            $this->set('loggedIn', true);
            $this->loggedIn = true;
            $this->set('user', $this->Auth->user());
            $this->set('userType', $userType);
            $this->user = $this->Auth->user();
        }
        if($this->Auth->user('type') == 'MCG4') {
            $pendingInstallments = $this->_getDevicePendingInstallmentCount();
            $this->set('pendingInstallments', $pendingInstallments);
        }
        $session = $this->request->session();
        $omdType = $session->read('omdType');
        $this->set('omdType',$omdType);
        $this->set('omdTypes', $this->omdTypes);
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Network\Response|null|void
     */
    public function beforeRender(Event $event) {
        $excludedControllers = ['Companies'];
        if ($this->Auth) {
            if ($this->Auth->user('id') && !$this->Auth->user('company_code') && !in_array($this->request->params['controller'], $excludedControllers) && $this->Auth->user('type') == 'user') {
               // return $this->redirect(['controller' => 'Companies', 'action' => 'index']);
            }
        }
        if (!array_key_exists('_serialize', $this->viewVars) && in_array($this->response->type(), ['application/json', 'application/xml'])) {
            $this->set('_serialize', true);
        }
    }

    protected function _generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    
    protected function _generateRandomName($length = 10) {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    
    protected function _generateOTP($length = 10) {
        $characters = '1234567890';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function array_group_by($array, $key) {
        $return = array();
        foreach ($array as $v) {
            $return[$v[$key]][] = $v;
        }
        return $return;
    }

    protected function _jsonResponse($array = []) {
        echo json_encode($array);
        die;
    }

    protected function _ajaxCheck() {
        if (!$this->request->is('ajax')) {
            exit('You are not allowed to Access this URL');
        }
    }

}
