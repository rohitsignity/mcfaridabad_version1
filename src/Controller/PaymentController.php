<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;
use Cake\Mailer\MailerAwareTrait;
use App\Traits\Users;
use App\Traits\Devices;
use Cake\Routing\Router;

class PaymentController extends AppController {

    use MailerAwareTrait,
        Users,
        Devices;

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['response']);
    }

    public function index() {
        return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
    }

    public function response() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $paymentTable = TableRegistry::get('Payments');
            $this->loadComponent('Ccavenue');
            $this->loadComponent('G8');
            $response = $this->Ccavenue->decrypt($this->request->data['encResp']);
            $paymentId = $this->request->data['orderNo'];
            $paymentDetail = $paymentTable->find('all')->where(['payment_id' => $paymentId, 'user_id' => $this->Auth->user('id')])->hydrate(false)->first();
            $responseArr = explode('&', $response);
            $paymentSatatus = explode('=', $responseArr[3]);
            //$ammountArr = explode('=', $responseArr[10]);
            $ammountArr = explode('=', $responseArr[35]);
            $paymentData = [];
            foreach ($responseArr as $responseItem) {
                $responseItemArr = explode('=', $responseItem);
                $paymentData[$responseItemArr[0]] = $responseItemArr[1];
            }
            $paymentData['response'] = $response;
            $paymentEntity = $paymentTable->newEntity($paymentData);
            $paymentEntity->id = $paymentDetail['id'];
            $paymentTable->save($paymentEntity);
            if ($paymentSatatus[1] == 'Success') {
                $userTable = TableRegistry::get('Users');
                $this->loadComponent('Pdf');
                if ($paymentDetail['payment_type'] == 'registration') {
                    $userProfileData = $this->_getUserProfileDetail($paymentDetail['user_id']);
                    $userTable = TableRegistry::get('Users');
                    $userTable->query()->update()->set(['paid' => 1])->where(['id' => $paymentDetail['user_id']])->execute();
                    $pdfData = [
                        'amount' => $ammountArr[1],
                        'id' => $paymentDetail['user_id'],
                        'user_id' => $paymentDetail['user_id'],
                        'path' => 'registration',
                        'r_no' => $paymentDetail['id']
                    ];
                    $session = $this->request->session();
                    $session->write('Registration.receipt', Router::url('/uploads/receipt/registration/registration_' . $paymentDetail['user_id'] . '_' . $paymentDetail['user_id'] . '.pdf'));
                    $this->Pdf->generateReceipt($pdfData);
                    $g8Data = [
                        'CUSTOMER_ADDRESS' => $userProfileData['company_address'],
                        'AREA_NAME' => 'Independent colony',
                        'REF_1' => 'Registration fee Rs. ' . number_format($paymentEntity->mer_amount, 0, '', ''),
                        'REF_2' => '749:' . $paymentEntity->mer_amount,
                        'MOBILE_NUMBER' => $userProfileData['mobile'],
                        'INSTRUMENT_TYPE' => $paymentEntity->payment_mode,
                        'TXN_REF_NO' => $paymentEntity->tracking_id,
                        'CUSTOMER_NAME' => $userProfileData['display_name'],
                        'BOOKING_REF_ID' => $paymentEntity->order_id,
                        'TXN_AMOUNT' => number_format($paymentData['mer_amount'], 0, '', ''),
                        'RECEIPT_TYPE' => '94',
                        'TXN_DATE' => date('d-M-Y'),
                        'OUTDOOR_ID' => $paymentEntity->id,
                        'EMAIL_ID' => $userProfileData['email']
                    ];
                    $g8Response = $this->G8->send($g8Data);
                    $paymentTable = TableRegistry::get('Payments');
                    $g8ResponsePayment['g8_response'] = json_encode($g8Response);
                    $paymentEntity = $paymentTable->newEntity($g8ResponsePayment);
                    $paymentEntity->id = $paymentDetail['id'];
                    $paymentTable->save($paymentEntity);
                    $openTendersIds = $this->_getUserOpenTenders($paymentDetail['user_id']);
                    if (count($openTendersIds)) {
                        $userTenderData = [];
                        foreach ($openTendersIds as $openTendersId) {
                            $userTenderData[] = [
                                'tender_id' => $openTendersId,
                                'user_id' => $paymentDetail['user_id']
                            ];
                        }
                        $tenderUsersTable = TableRegistry::get('TenderUsers');
                        $tenderUsersEntities = $tenderUsersTable->newEntities($userTenderData);
                        $tenderUsersTable->saveMany($tenderUsersEntities);
                    }
                    $userDataObj = $userTable->find('all')->where(['id' => $paymentDetail['user_id']])->first();
                    $this->getMailer('User')->send('registrationReceipt', [$userDataObj]);
                    $this->Flash->success("Your payment has been successfully recieved.");
                    return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
                } else if ($paymentDetail['payment_type'] == 'device_final_pay') {
                    $deviceDetail = $this->_getDeviceData($paymentDetail['type_id']);
                    $fees = 0;
                    $area = '--N/A--';
                    $location = '--N/A--';
                    $tenure = 12;

                    $tenureArr = array_values(array_filter($deviceDetail['fields'], function($arr) {
                                return $arr['field'] == 'tenure';
                            }));
                    if ($tenureArr) {
                        $tenure = $tenureArr[0]['value'];
                    }
                    $totalLicenseFees = array_values(array_filter($deviceDetail['fields'], function($arr) {
                                return $arr['field'] == 'annual_license_fees';
                            }));
                    if (!$totalLicenseFees) {
                        $totalLicenseFees = array_values(array_filter($deviceDetail['fields'], function($arr) {
                                    return $arr['field'] == 'concession_fees';
                                }));
                    }
                    if (!$totalLicenseFees) {
                        $totalLicenseFees = array_values(array_filter($deviceDetail['fields'], function($arr) {
                                    return $arr['field'] == 'total_omd_fees';
                                }));
                    }
                    if (!$totalLicenseFees) {
                        $totalLicenseFees = array_values(array_filter($deviceDetail['fields'], function($arr) {
                                    return $arr['field'] == 'total_permission_fees';
                                }));
                    }
                    if (count($totalLicenseFees)) {
                        $fees = number_format($totalLicenseFees[0]['value']);
                    }
                    $startDate = date('Y-m-01', strtotime($deviceDetail['modified'] . ' +1 months'));
                    $endDate = date('Y-m-d', strtotime($startDate . ' +' . $tenure . ' months'));
                    $pdfData = [
                        'id' => $deviceDetail['id'],
                        'user_id' => $deviceDetail['user_id'],
                        'status' => 'approved',
                        'date' => date('d M, Y'),
                        'receipt_id' => 'MCF-' . $paymentDetail['id'],
                        'approval_date' => date('d M, Y', strtotime($deviceDetail['modified'])),
                        'to' => $deviceDetail['name'],
                        'address' => $deviceDetail['address'],
                        'file_no' => date('d.H.Y.m.' . $deviceDetail['id'], strtotime($deviceDetail['created'])),
                        'app_no' => 'MCFOMD' . date('Ymd', strtotime($deviceDetail['created'])) . $deviceDetail['id'],
                        'app_date' => date('d M, Y', strtotime($deviceDetail['created'])),
                        'omd_id' => 'MCFOMD' . $deviceDetail['id'] . 'U' . $deviceDetail['user_id'],
                        'payment_mode_name' => $paymentDetail['card_name'],
                        'fees' => $fees,
                        'area' => $area,
                        'location' => $location,
                        'category_id' => $deviceDetail['category_id'],
                        'typology' => $deviceDetail['typology'],
                        'sub_category' => $deviceDetail['sub_category'],
                        'sub_category_id' => $deviceDetail['sub_category_id'],
                        'start' => date('d-M-Y', strtotime($startDate)),
                        'end' => date('d-M-Y', strtotime($endDate . '-1 day')),
                        'fields' => $deviceDetail['fields'],
                        'fieldsData' => $deviceDetail['fieldsData']
                    ];
                    if ($deviceDetail['sub_category_id'] == 24 || $deviceDetail['sub_category_id'] == 33) {
                        $eventStartDate = array_values(array_filter($deviceDetail['fields'], function($arr) {
                                    return $arr['field'] == 'event_start_date';
                                }));
                        $eventDuration = array_values(array_filter($deviceDetail['fields'], function($arr) {
                                    return $arr['field'] == 'event_duration';
                                }));
                        if (count($eventStartDate)) {
                            $pdfData['start'] = date('d-M-Y', strtotime($eventStartDate[0]['value']));
                            $pdfData['end'] = date('d-M-Y', strtotime($eventStartDate[0]['value']));
                            if (count($eventDuration)) {
                                $numberofDays = $eventDuration[0]['value'] - 1;
                                if ($numberofDays > 0) {
                                    $pdfData['end'] = date('d-M-Y', strtotime($eventStartDate[0]['value'] . ' +' . $numberofDays . ' days'));
                                }
                            }
                        }
                    }
                    $this->Pdf->generatePermissionLeter($pdfData);
                    $pdfDataR = [
                        'amount' => number_format($ammountArr[1], 0, '', ''),
                        'id' => $paymentDetail['type_id'],
                        'user_id' => $paymentDetail['user_id'],
                        'path' => 'permission',
                        'r_no' => $paymentDetail['id']
                    ];
                    $session = $this->request->session();
                    $session->write('Registration.receipt', Router::url('/uploads/receipt/permission/permission_' . $paymentDetail['type_id'] . '_' . $paymentDetail['user_id'] . '.pdf'));
                    $this->Pdf->generateReceipt($pdfDataR);
                    $g8Data = [
                        'CUSTOMER_ADDRESS' => $deviceDetail['address'],
                        'AREA_NAME' => 'Independent colony',
                        'REF_1' => 'Annual license fee Rs. ' . number_format($paymentEntity->mer_amount, 0, '', ''),
                        'REF_2' => '751:' . $paymentEntity->mer_amount,
                        'MOBILE_NUMBER' => $deviceDetail['phone'],
                        'INSTRUMENT_TYPE' => $paymentEntity->payment_mode,
                        'TXN_REF_NO' => $paymentEntity->tracking_id,
                        'CUSTOMER_NAME' => $deviceDetail['name'],
                        'BOOKING_REF_ID' => $paymentEntity->order_id,
                        'TXN_AMOUNT' => number_format($paymentData['mer_amount'], 0, '', ''),
                        'RECEIPT_TYPE' => '94',
                        'TXN_DATE' => date('d-M-Y'),
                        'OUTDOOR_ID' => $paymentEntity->id,
                        'EMAIL_ID' => $deviceDetail['email']
                    ];
                    $g8Response = $this->G8->send($g8Data);
                    $g8ResponsePayment['g8_response'] = json_encode($g8Response);
                    $paymentTable = TableRegistry::get('Payments');
                    $paymentEntity = $paymentTable->newEntity($g8ResponsePayment);
                    $paymentEntity->id = $paymentDetail['id'];
                    $paymentTable->save($paymentEntity);
                    $omdPaymentTable = TableRegistry::get('OmdPayments');
                    $finalPaymentData = [
                        'device_id' => $paymentDetail['type_id'],
                        'amount' => number_format($paymentData['amount'], 0, '', ''),
                        'payment_id' => $paymentDetail['id']
                    ];
                    $omdPaymentEntity = $omdPaymentTable->newEntity($finalPaymentData);
                    $omdPaymentTable->save($omdPaymentEntity);
                    $this->Flash->success(__('Your payment has been successfully recieved.'));
                    return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
                } else if ($paymentDetail['payment_type'] == 'device_installment') {
                    $installmentTable = TableRegistry::get('Installments');
                    $installment = $this->_getDeviceInstallment($paymentDetail['type_id']);
                    $installments = $this->_getDeviceInstallments($installment['device_id']);
                    $deviceDetail = $this->_getDeviceData($installment['device_id']);
                    $notFirstInstallment = array_filter($installments, function($arr) {
                        return $arr['payment_id'] != 0;
                    });
                    if (!$notFirstInstallment) {
                        $fees = 0;
                        $area = '--N/A--';
                        $location = '--N/A--';
                        $tenure = 12;
                        $tenureArr = array_values(array_filter($deviceDetail['fields'], function($arr) {
                                    return $arr['field'] == 'tenure';
                                }));
                        if ($tenureArr) {
                            $tenure = $tenureArr[0]['value'];
                        }
                        $totalLicenseFees = array_values(array_filter($deviceDetail['fields'], function($arr) {
                                    return $arr['field'] == 'annual_license_fees';
                                }));
                        if (!$totalLicenseFees) {
                            $totalLicenseFees = array_values(array_filter($deviceDetail['fields'], function($arr) {
                                        return $arr['field'] == 'concession_fees';
                                    }));
                        }
                        if (!$totalLicenseFees) {
                            $totalLicenseFees = array_values(array_filter($deviceDetail['fields'], function($arr) {
                                        return $arr['field'] == 'total_omd_fees';
                                    }));
                        }
                        if (!$totalLicenseFees) {
                            $totalLicenseFees = array_values(array_filter($deviceDetail['fields'], function($arr) {
                                        return $arr['field'] == 'total_permission_fees';
                                    }));
                        }
                        if (count($totalLicenseFees)) {
                            $fees = number_format($totalLicenseFees[0]['value']);
                        }
                        $startDate = date('Y-m-01', strtotime($deviceDetail['modified'] . ' +1 months'));
                        $endDate = date('Y-m-d', strtotime($startDate . ' +' . $tenure . ' months'));
                        $pdfData = [
                            'id' => $deviceDetail['id'],
                            'user_id' => $deviceDetail['user_id'],
                            'status' => 'approved',
                            'date' => date('d M, Y'),
                            'receipt_id' => 'MCF-' . $paymentDetail['id'],
                            'approval_date' => date('d M, Y', strtotime($deviceDetail['modified'])),
                            'to' => $deviceDetail['name'],
                            'address' => $deviceDetail['address'],
                            'file_no' => date('d.H.Y.m.' . $deviceDetail['id'], strtotime($deviceDetail['created'])),
                            'app_no' => 'MCFOMD' . date('Ymd', strtotime($deviceDetail['created'])) . $deviceDetail['id'],
                            'app_date' => date('d M, Y', strtotime($deviceDetail['created'])),
                            'omd_id' => 'MCFOMD' . $deviceDetail['id'] . 'U' . $deviceDetail['user_id'],
                            'payment_mode_name' => $paymentDetail['card_name'],
                            'fees' => $fees,
                            'area' => $area,
                            'location' => $location,
                            'category_id' => $deviceDetail['category_id'],
                            'typology' => $deviceDetail['typology'],
                            'sub_category' => $deviceDetail['sub_category'],
                            'sub_category_id' => $deviceDetail['sub_category_id'],
                            'start' => date('d-M-Y', strtotime($startDate)),
                            'end' => date('d-M-Y', strtotime($endDate . '-1 day')),
                            'fields' => $deviceDetail['fields'],
                            'fieldsData' => $deviceDetail['fieldsData']
                        ];
                        if ($deviceDetail['sub_category_id'] == 24 || $deviceDetail['sub_category_id'] == 33) {
                            $eventStartDate = array_values(array_filter($deviceDetail['fields'], function($arr) {
                                        return $arr['field'] == 'event_start_date';
                                    }));
                            $eventDuration = array_values(array_filter($deviceDetail['fields'], function($arr) {
                                        return $arr['field'] == 'event_duration';
                                    }));
                            if (count($eventStartDate)) {
                                $pdfData['start'] = date('d-M-Y', strtotime($eventStartDate[0]['value']));
                                $pdfData['end'] = date('d-M-Y', strtotime($eventStartDate[0]['value']));
                                if (count($eventDuration)) {
                                    $numberofDays = $eventDuration[0]['value'] - 1;
                                    if ($numberofDays > 0) {
                                        $pdfData['end'] = date('d-M-Y', strtotime($eventStartDate[0]['value'] . ' +' . $numberofDays . ' days'));
                                    }
                                }
                            }
                        }
                        $this->Pdf->generatePermissionLeter($pdfData);
                    }
                    $pdfDataR = [
                        'amount' => $ammountArr[1],
                        'id' => $paymentDetail['type_id'],
                        'user_id' => $paymentDetail['user_id'],
                        'path' => 'device_installment',
                        'r_no' => $paymentDetail['id']
                    ];
                    $session = $this->request->session();
                    $session->write('Registration.receipt', Router::url('/uploads/receipt/device_installment/device_installment_' . $paymentDetail['type_id'] . '_' . $paymentDetail['user_id'] . '.pdf'));
                    $this->Pdf->generateReceipt($pdfDataR);
                    $g8Data = [
                        'CUSTOMER_ADDRESS' => $deviceDetail['address'],
                        'AREA_NAME' => 'Independent colony',
                        'REF_1' => 'OMD installment Rs. ' . number_format($paymentEntity->mer_amount, 0, '', ''),
                        'REF_2' => '751:' . $paymentEntity->mer_amount,
                        'MOBILE_NUMBER' => $deviceDetail['phone'],
                        'INSTRUMENT_TYPE' => $paymentEntity->payment_mode,
                        'TXN_REF_NO' => $paymentEntity->tracking_id,
                        'CUSTOMER_NAME' => $deviceDetail['name'],
                        'BOOKING_REF_ID' => $paymentEntity->order_id,
                        'TXN_AMOUNT' => number_format($paymentData['mer_amount'], 0, '', ''),
                        'RECEIPT_TYPE' => '94',
                        'TXN_DATE' => date('d-M-Y'),
                        'OUTDOOR_ID' => $paymentEntity->id,
                        'EMAIL_ID' => $deviceDetail['email']
                    ];
                    $g8Response = $this->G8->send($g8Data);
                    $g8ResponsePayment['g8_response'] = json_encode($g8Response);
                    $paymentTable = TableRegistry::get('Payments');
                    $paymentEntity = $paymentTable->newEntity($g8ResponsePayment);
                    $paymentEntity->id = $paymentDetail['id'];
                    $paymentTable->save($paymentEntity);
                    $omdPaymentTable = TableRegistry::get('OmdPayments');
                    $finalPaymentData = [
                        'device_id' => $deviceDetail['id'],
                        'amount' => number_format($paymentData['amount'], 0, '', ''),
                        'payment_id' => $paymentDetail['id']
                    ];
                    $omdPaymentEntity = $omdPaymentTable->newEntity($finalPaymentData);
                    $omdPaymentTable->save($omdPaymentEntity);
                    $installmentEntity = $installmentTable->newEntity(['payment_id' => $paymentDetail['id']]);
                    $installmentEntity->id = $paymentDetail['type_id'];
                    $installmentTable->save($installmentEntity);
                    $this->Flash->success(__('Your payment has been successfully recieved.'));
                    return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
                } else {
                    $deviceDetail = $this->_getDeviceData($paymentDetail['type_id']);
                    $CompanyDevicesTable = TableRegistry::get('CompanyDevices');
                    $CompanyDevicesTable->query()->update()->set(['paid' => 1])->where(['id' => $paymentDetail['type_id']])->execute();
                    $pdfData = [
                        'amount' => $ammountArr[1],
                        'id' => $paymentDetail['type_id'],
                        'user_id' => $paymentDetail['user_id'],
                        'path' => 'device_registration',
                        'r_no' => $paymentDetail['id']
                    ];
                    $session = $this->request->session();
                    $session->write('Registration.receipt', Router::url('/uploads/receipt/device_registration/device_registration_' . $paymentDetail['type_id'] . '_' . $paymentDetail['user_id'] . '.pdf'));
                    $this->Pdf->generateReceipt($pdfData);
                    $g8Data = [
                        'CUSTOMER_ADDRESS' => $deviceDetail['address'],
                        'AREA_NAME' => 'Independent colony',
                        'REF_1' => 'Application fee Rs. ' . number_format($paymentEntity->mer_amount, 0, '', ''),
                        'REF_2' => '750:' . $paymentEntity->mer_amount,
                        'MOBILE_NUMBER' => $deviceDetail['phone'],
                        'INSTRUMENT_TYPE' => $paymentEntity->payment_mode,
                        'TXN_REF_NO' => $paymentEntity->tracking_id,
                        'CUSTOMER_NAME' => $deviceDetail['name'],
                        'BOOKING_REF_ID' => $paymentEntity->order_id,
                        'TXN_AMOUNT' => number_format($paymentData['mer_amount'], 0, '', ''),
                        'RECEIPT_TYPE' => '94',
                        'TXN_DATE' => date('d-M-Y'),
                        'OUTDOOR_ID' => $paymentEntity->id,
                        'EMAIL_ID' => $deviceDetail['email']
                    ];
                    $g8Response = $this->G8->send($g8Data);
                    $g8ResponsePayment['g8_response'] = json_encode($g8Response);
                    $paymentEntity = $paymentTable->newEntity($g8ResponsePayment);
                    $paymentEntity->id = $paymentDetail['id'];
                    $paymentTable->save($paymentEntity);
                    $userData = $this->_getUserData($paymentDetail['user_id']);
                    $mailData = [
                        'user' => $userData,
                        'deviceId' => $paymentDetail['type_id']
                    ];
                    $this->getMailer('User')->send('deviceRegistrationReceipt', [$mailData]);
                    $this->Flash->success(__('New Device have been successfully registered.'));
                    return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
                }
            } else {
                $this->Flash->error("Payment Failed. Please try again.");
                return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
            }
        } else {
            $this->Flash->error("Invalid Request");
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }
    }

}
