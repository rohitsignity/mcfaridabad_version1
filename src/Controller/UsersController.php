<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;
use Cake\Mailer\MailerAwareTrait;
use App\Traits\Users;

class UsersController extends AppController {

    use MailerAwareTrait,
        Users;

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['register', 'reset-password', 'resetPassword', 'verify-email', 'verifyEmail', 'registration', 'checkAvailability', 'getRegistrationDocs', 'uploadDoc', 'markNotApplicable']);
    }

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Cookie');
    }

    public function index() {
        $this->viewBuilder()->className('User');
        return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
    }

    public function login() {
        $userId = $this->Auth->user('id');
        if ($userId) {
            return $this->redirect(['controller' => 'home', 'action' => 'index']);
        }
        if ($this->Cookie->read('info') && !$_POST) {
            $this->request->data = $this->Cookie->read('info');
        }
        $post = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $post = $this->Users->patchEntity($post, $this->request->data, ['validate' => 'Login']);
            $user = $this->Auth->identify();
            if (isset($_POST['remember'])) {
                $this->Cookie->config(['expires' => '+10 day', 'httpOnly' => true]);
                $this->Cookie->write('info', $this->request->data);
            } else {
                $this->Cookie->delete('info');
            }
            if ($user) {
                $this->Auth->setUser($user);
                
                $userType = strtolower(preg_replace('/[0-9]+/', '', $this->Auth->user('type')));
                if ($userType == 'smc') {
                    return $this->redirect(['controller' => 'Smc', 'action' => 'index']);
                }
                return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
            }
            if (!$post->errors()) {
                $this->Flash->error(__('Invalid Username/Password. Please try again!'));
            }
        }
        $this->set(compact('post'));
        $this->set(compact('securityQuestions'));
        $this->set(compact('representatives'));
        $this->set(compact('experiences'));
        $this->set(compact('turnovers'));
        $this->set('rolesArray', $this->rolesArray);
    }

    public function register() {
        $this->_ajaxCheck();
        $post = $this->Users->newEntity();
        $profile = TableRegistry::get('UserProfiles');
        $postProfile = $profile->newEntity();
        $response['error'] = 0;
        $directorsErrors = [];
        $emailId = $this->request->data['email'];
        if ($emailId) {
            $userbyEmail = $this->_getUserByEmailId($emailId);
            if (isset($userbyEmail['doc_uploaded'])) {
                if (!$userbyEmail['doc_uploaded']) {
                    $session = $this->request->session();
                    $session->write('Registration.maskedKey', base64_encode($userbyEmail['id']));
                    $response['error'] = 3;
                    $response['data'] = 'You are already Registered as "' . $userbyEmail['representative'] . '". Please upload the required documents.';
                    $this->_jsonResponse($response);
                }
            }
        }
        $allDirectors = [];
        /* Registration */
        $this->__validateAdditionalDirectors($this->request->data, $directorsErrors);
        if ($this->request->is('post')) {
            $vendorSaved = null;
            $post = $this->Users->patchEntity($post, $this->request->data);
            if (!count($directorsErrors)) {
                $vendorSaved = $this->Users->save($post);
            }
            if ($vendorSaved) {
                $session = $this->request->session();
                $session->write('Registration.maskedKey', base64_encode($vendorSaved->id));
                $representativeId = $this->request->data['representative_id'];
                if (in_array($representativeId, [1, 2, 3, 4, 5, 6])) {
                    if ($this->request->data['address_proof']['size']) {
                        $ext = pathinfo($this->request->data['address_proof']['name'], PATHINFO_EXTENSION);
                        $fileName = time() . '_' . $this->_generateRandomString(20) . '_address_proof.' . $ext;
                        $uploadFile = WWW_ROOT . 'uploads/register_documents/' . $fileName;
                        if (move_uploaded_file($this->request->data['address_proof']['tmp_name'], $uploadFile)) {
                            $this->request->data['address_proof'] = $fileName;
                        }
                    }
                } else {
                    $this->request->data['address_proof'] = '';
                }
                if ($this->request->data['representative_id'] == 5) {
                    if (isset($this->request->data['directors_name'])) {
                        $directorsName = $this->request->data['directors_name'];
                        foreach ($directorsName as $director => $director_name) {
                            $allDirectors[] = [
                                'director_name' => $director_name,
                                'director_user_id' => $vendorSaved->id,
                                'din' => $this->request->data['dins'][$director]
                            ];
                        }
                    }
                    if (count($allDirectors) > 0) {
                        $directors = TableRegistry::get('UserDirectors');
                        $postDirectors = $directors->newEntities($allDirectors);
                        $directors->saveMany($postDirectors);
                    }
                } else {
                    $this->request->data['director_name'] = '';
                }

                $postProfile = $profile->patchEntity($postProfile, $this->request->data);
                $postProfile->user_profile_id = $vendorSaved->id;
                $profile->save($postProfile);
                $response['error'] = 0;
            } else {
                $response['error'] = 1;
                $response['data'] = $post->errors();
                if (count($directorsErrors)) {
                    $response['data'] = array_merge($response['data'], $directorsErrors);
                }
            }
        }
        $this->set(compact('response'));
        $this->set(compact('post'));
        $this->set('_serialize', 'response');
    }

    private function __validateAdditionalDirectors($data, &$directorsErrors) {
        if (isset($data['dins']) && is_array($data['dins'])) {
            foreach ($data['dins'] as $key => $din) {
                if (!trim($din)) {
                    $directorsErrors['dins[' . $key . ']'] = ['_empty' => 'Din Number is required'];
                }
            }
        }
        if(isset($data['directors_name']) && is_array($data['directors_name'])) {
            foreach ($data['directors_name'] as $key => $directorName) {
                if (!trim($directorName)) {
                    $directorsErrors['directors_name[' . $key . ']'] = ['_empty' => 'Director name is required'];
                }
            }
        }
    }

    public function registration() {

        $userId = $this->Auth->user('id');
        if ($userId) {
            return $this->redirect(['controller' => 'home', 'action' => 'index']);
        }

        /* Address Profes* */
        $addressProfes = [];
        $addressProfesData = TableRegistry::get('AddressProfes');
        $addressProfesArr = $addressProfesData->find('all')->hydrate(false)->toArray();
        foreach ($addressProfesArr as $addressProfesItem) {
            $addressProfes[$addressProfesItem['id']] = $addressProfesItem['name'];
        }
        /* Security Questions List */
        $securityQuestions = [];
        $securityQuestionData = TableRegistry::get('SecurityQus');
        $securityQuestion = $securityQuestionData->find()->select(['id', 'question'])->hydrate(false);
        foreach ($securityQuestion->toArray() as $securityQuestionItem) {
            $securityQuestions[$securityQuestionItem['id']] = $securityQuestionItem['question'];
        }
        /* Representatives List */
        $representatives = [];
        $representativesData = TableRegistry::get('Representatives');
        $representative = $representativesData->find()->select(['id', 'representative'])->hydrate(false);
        foreach ($representative->toArray() as $representativeEntity) {
            $representatives[$representativeEntity['id']] = $representativeEntity['representative'];
        }

        /* Annual Turnovers List */
        $turnovers = [];
        $turnoversData = TableRegistry::get('AnnualTurnovers');
        $annualTurnovers = $turnoversData->find()->select(['id', 'annual_turnover'])->hydrate(false);
        foreach ($annualTurnovers->toArray() as $annuaTurn) {
            $turnovers[$annuaTurn['id']] = $annuaTurn['annual_turnover'];
        }

        /* Experiences Lists */
        $experiences = [];
        $experiencesTable = TableRegistry::get('Experiences');
        $experiencesData = $experiencesTable->find('all')->hydrate(false)->toArray();
        array_walk($experiencesData, function($val) use(&$experiences) {
            $experiences[$val['id']] = $val['name'];
        });
        $this->set(compact('securityQuestions'));
        $this->set(compact('representatives'));
        $this->set(compact('experiences'));
        $this->set(compact('turnovers', 'addressProfes'));
        $this->set('rolesArray', $this->rolesArray);
    }

    public function response() {
        $this->autoRender = false;
        echo 'Please do not refresh the page';
        $this->loadComponent('Insta');
        $this->loadComponent('Pdf');
//        $session = $this->request->session();
//        $data = $session->read('Mask.info');
//        $userData = json_decode(base64_decode($data), true);
        $paymentId = $this->request->query('payment_id');
        $paymentRequestId = $this->request->query('payment_request_id');
        $userDataJson = file_get_contents(WWW_ROOT . 'uploads/register_session/' . $paymentRequestId . '.txt');
        $userData = json_decode($userDataJson, true);
        $paymentDetail = $this->Insta->paymentDetail($paymentId);
        $userId = $userData['id'];
        if ($paymentDetail['status'] == 'Credit') {

            $this->Users->query()
                    ->update()
                    ->set(['paid' => 1])
                    ->where(['id' => $userData['id']])
                    ->execute();

            $paymentTable = TableRegistry::get('Payments');
            $paymentData = [
                'user_id' => $userData['id'],
                'payment_id' => $paymentDetail['payment_id'],
                'payment_request_id' => $paymentRequestId,
                'quantity' => $paymentDetail['quantity'],
                'status' => $paymentDetail['status'],
                'buyer_name' => $paymentDetail['buyer_name'],
                'buyer_phone' => $paymentDetail['buyer_phone'],
                'buyer_email' => $paymentDetail['buyer_email'],
                'currency' => $paymentDetail['currency'],
                'unit_price' => $paymentDetail['unit_price'],
                'amount' => $paymentDetail['amount'],
                'fees' => $paymentDetail['fees'],
                'created_at' => date('Y-m-d H:i:s', strtotime($paymentDetail['created_at'])),
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ];
            $pdfData = [
                'amount' => $paymentDetail['amount'],
                'id' => $userId,
                'user_id' => $userId,
                'path' => 'registration'
            ];
            $session = $this->request->session();
            $session->write('Registration.receipt', Router::url('/uploads/receipt/registration/registration_' . $userId . '_' . $userId . '.pdf'));
            $openTendersIds = $this->_getUserOpenTenders($userData['id']);
            if (count($openTendersIds)) {
                $userTenderData = [];
                foreach ($openTendersIds as $openTendersId) {
                    $userTenderData[] = [
                        'tender_id' => $openTendersId,
                        'user_id' => $userData['id']
                    ];
                }
                $tenderUsersTable = TableRegistry::get('TenderUsers');
                $tenderUsersEntities = $tenderUsersTable->newEntities($userTenderData);
                $tenderUsersTable->saveMany($tenderUsersEntities);
            }
            $this->Pdf->generateReceipt($pdfData);
            $paymentTable->query()->insert(['user_id', 'payment_id', 'payment_request_id', 'quantity', 'status', 'buyer_name', 'buyer_phone', 'buyer_email', 'currency', 'unit_price', 'amount', 'fees', 'created_at', 'created', 'modified'])->values($paymentData)->execute();
            $userData = $this->Users->find()->where(['id' => $userData['id']]);
            $userDataArr = $userData->toArray();
            $this->getMailer('User')->send('registrationReceipt', [json_decode($userDataJson)]);
            $this->loadComponent('Sms');
            $this->Sms->sendTextSms($userDataArr[0]['mobile'], 'Registration payment has been recieved. Your profile is under admin approval. We will get back to you soon.');
            $this->Flash->success(__('Registration payment has been recieved. Your profile is under admin approval'));
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        } else {
//            $this->Users->query()->delete()->where(['id' => $userData['id']])->execute();
            $this->Flash->error(__('Something went wrong with Payment Gateway. Please try again'));
            return $this->redirect(['controller' => 'Users', 'action' => 'register']);
        }
    }

    public function logout() {
        $this->Flash->error(__('You are successfully Logged out'));
        $this->Cookie->delete('info');
        return $this->redirect($this->Auth->logout());
    }

    public function forgotPassword() {

        $userData = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $userData = $this->Users->patchEntity($userData, $this->request->data, ['validate' => 'ForgotPassword']);
            if ($userData) {
                $email = $this->request->data['email'];
                $userResult = $this->Users->find('all')->where(['email' => $email])->first();
                if ($userResult) {
                    $userResult['security_hash'] = md5(rand()) . $this->_generateRandomString(6);
                    if ($this->Users->save($userResult)) {
                        $this->getMailer('User')->send('forgotPassword', [$userResult]);
                        $this->Flash->success(__("A link has been sent to {$email}. Please check your email to reset your password."));
                    } else {
                        $this->Flash->error(__('Could not send email. Please try after sometime.'));
                    }
                }
            }
        }
        $this->set(compact('userData'));
    }

    public function manageProfile() {

        $userDataArr = $this->_getUserData($this->Auth->user('id'));
        if (!$userDataArr['is_mobile_verified']) {
            return $this->redirect(['controller' => 'Users', 'action' => 'mobileVerification']);
        }
        if (!$userDataArr['paid']) {
            return $this->redirect(['controller' => 'Users', 'action' => 'regisrationPayment']);
        }
        if ($userDataArr['status'] == 2) {
            $this->Flash->error('Your profile has been rejected by administrator. Please register again or contact MCF support.');
            return $this->redirect($this->Auth->logout());
        }
        if ($userDataArr['admin_approved'] != 2) {
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }
        $this->viewBuilder()->layout('dashboard');

        ///// Changes password functionality ---------------------------------------------------------------------
        $userData = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['password'] = $this->request->data['new_password'];
            $this->request->data['id'] = $this->user['id'];
            $userData = $this->Users->patchEntity($userData, $this->request->data, ['validate' => 'Password']);
            if (empty($userData->errors())) {
                $userData['id'] = $this->user['id'];
                if ($this->Users->save($userData)) {
                    $this->Flash->success('The password is successfully changed');
                    $this->redirect(['controller' => 'Users', 'action' => 'manage-profile']);
                } else {
                    $this->Flash->error('There was an error during the save!');
                }
            }
        }

        ///// Fetching company info -------------------------------------------------------------------------------

        $companyInfo = $this->Users->find('all')
                        ->select([
                            'display_name' => 'Users.display_name',
                            'Users.email',
                            'Users.mobile',
                            'Users.created',
                            'incorporation_date' => 'user_profiles.incorporation_date',
                            'representative' => 'representatives.representative',
                            'authorised_contact_person' => 'user_profiles.authorised_contact_person',
                            'address' => 'user_profiles.company_address',
                            'annual_turnover' => 'annual_turnovers.annual_turnover'
                        ])->join([
                    'table' => 'user_profiles',
                    'type' => 'LEFT',
                    'conditions' => 'Users.id = user_profiles.user_profile_id',
                ])->join([
                    'table' => 'representatives',
                    'type' => 'LEFT',
                    'conditions' => 'Users.representative_id = representatives.id',
                ])->join([
                    'table' => 'annual_turnovers',
                    'type' => 'LEFT',
                    'conditions' => 'user_profiles.annual_turnover = annual_turnovers.id'
                ])->where(['Users.id' => $this->user['id']])->hydrate(false)->first();
        //// Fetching addresses info -------------------------------------------------------------------------------------
        $addressesTable = TableRegistry::get('addresses');
        $addressesInfo = $addressesTable->find('all', [
                    'order' => ['type' => 'ASC']
                ])
                ->select([
                    'type' => 'addresses.type',
                    'address' => 'addresses.address',
                    'city' => 'addresses.city',
                    'state' => 'addresses.state',
                    'pincode' => 'addresses.pincode',
                    'email' => 'addresses.email',
                    'phone' => 'addresses.phone'
                ])
                ->join([
                    'table' => 'companies',
                    'type' => 'LEFT',
                    'conditions' => 'companies.company_code = addresses.company_code',
                ])->where(['companies.company_code' => $this->user['company_code']])
                ->hydrate(false)
                ->toArray();
        $userProfile = $this->_getUserProfile($this->Auth->user('id'));
        $userDocumentsArr = $this->_getUsersDocumentsbyIds([$this->Auth->user('id')]);
        $userDocuments = reset($userDocumentsArr);
        ///// Setting vars -----------------------------------------------------------------------------------------------
        $this->set('addressesInfo', $addressesInfo);
        $this->set('companyInfo', $companyInfo);
        $this->set('userData', $userData);
        $this->set('userProfile', $userProfile);
        $this->set('userDocuments', $userDocuments);
    }

    public function resetPassword() {
        $valid = false;
        $userId = $this->request->query['mask'];
        $securityHash = $this->request->query['id'];
        $userEntity = $this->Users->newEntity();

        if (!empty($userId) && !empty($securityHash)) {
            $findQuery = $this->Users->find('all')->where(['id' => $userId, 'security_hash' => $securityHash])->first();

            if ($findQuery) {
                $valid = true;
            }
        }
        if ($this->request->is('post')) {
            if ($valid) {
                $usersData = $findQuery->toArray();
                $this->request->data['security_hash'] = "";
                $userValidateData = $this->Users->patchEntity($userEntity, $this->request->data, ['validate' => 'ResetPassword']);
                if (empty($userValidateData->errors())) {
                    $userValidateData['id'] = $usersData['id'];
                    if ($this->Users->save($userValidateData)) {
                        $this->Flash->success('The password is successfully changed');
                        $this->redirect(['controller' => 'Users', 'action' => 'login']);
                    } else {
                        $this->Flash->error('There was an error during password reset!');
                    }
                }
            }
        }
        $this->set('valid', $valid);
        $this->set('userEntity', $userEntity);
    }

    public function verifyEmail() {
        $userId = isset($this->request->query['mask']) && $this->request->query['mask'] ? base64_decode($this->request->query['mask']) : '';
        $userToken = $this->request->query['token'];
        if (!empty($userId) && !empty($userToken)) {
            $user = $this->Users->find('all')->where(['id' => $userId])->first();
            if (!$user->id) {
                $this->Flash->error('Invalid URL.');
                return $this->redirect(['controller' => 'users', 'action' => 'login']);
            } else if ($user->user_token) {
                $updateStatus = $this->Users->query()->update()->set(['status' => 1, 'user_token' => ''])->where(['id' => $user->id])->execute();
                if ($updateStatus) {
                    $this->Flash->success('Your email id has been successfully verified. Please login to access you account');
                    return $this->redirect(['controller' => 'users', 'action' => 'login']);
                }
            } else {
                $this->Flash->error('Email account is already verified. Please login.');
                return $this->redirect(['controller' => 'users', 'action' => 'login']);
            }
        }
    }

    public function setOmdType() {
        $this->autoRender = false;
        $this->_ajaxCheck();
        $response['error'] = 1;
        if ($this->request->is('put')) {
            $response['error'] = 0;
            $response['url'] = Router::url(['controller' => 'EAuction', 'action' => 'index']);
            $omdType = $this->request->data['term'];
            $session = $this->request->session();
            $session->write('omdType', $omdType);
        }
        $this->_jsonResponse($response);
    }

    public function checkAvailability() {
        $this->autoRender = false;
        $this->_ajaxCheck();
        $response['error'] = 0;
        $userName = trim($this->request->data['term']);
        $response['title'] = 'Available';
        $response['message'] = 'User name "' . $userName . '" is available.';
        $userTable = TableRegistry::get('Users');
        $userNameExist = $userTable->find('all')->where(['user_name' => $userName])->count();
        if ($userNameExist) {
            $response['error'] = 1;
            $response['title'] = 'Not Available';
            $response['message'] = 'User name "' . $userName . '" already taken. Please try another one.';
        }
        $this->_jsonResponse($response);
    }

    public function mobileVerification() {
        $this->viewBuilder()->layout('dashboard');
        $userArr = $this->_getUserData($this->Auth->user('id'));
        if ($userArr['is_mobile_verified']) {
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }
        $this->set(compact('userArr'));
    }

    public function sendOtp() {
        $this->autoRender = false;
        $this->_ajaxCheck();
        $response['error'] = 0;
        $response['data'] = 'success';
        $mobileNumber = trim($this->request->data('mobile'));
        if (!$mobileNumber) {
            $response['error'] = 1;
            $response['data'] = 'Mobile Number is required';
        } else if (!is_numeric($mobileNumber)) {
            $response['error'] = 1;
            $response['data'] = 'Mobile Number must be numeric';
        } else if (strlen($mobileNumber) < 10 || strlen($mobileNumber) > 10) {
            $response['error'] = 1;
            $response['data'] = 'Mobile Number should of of 10 digits';
        } else {
            $this->loadComponent('Sms');
            $userId = $this->Auth->user('id');
            $otp = $this->_generateOTP(4);
            $this->Sms->sendOtp($mobileNumber, $otp);
            $this->Users->query()->update()->set(['mobile' => $mobileNumber, 'otp' => $otp, 'modified' => date('Y-m-d H:i:s')])->where(['id' => $userId])->execute();
        }
        $this->_jsonResponse($response);
    }

    public function verifyOtp() {
        $this->autoRender = false;
        $otp = trim($this->request->data('otp'));
        $response['error'] = 0;
        $response['data'] = Router::url(['controller' => 'dashboard', 'action' => 'index']);
        if (!$otp) {
            $response['error'] = 1;
            $response['data'] = 'OTP is required';
        } else {
            $userId = $this->Auth->user('id');
            $userData = $this->_getUserDetail($userId);
//            $mobile = $userData['mobile'];
//            $verified = $this->Sms->verifyOtp($mobile, $otp);
            if ($userData['otp'] != $otp) {
                $response['error'] = 1;
                $response['data'] = 'Invalid OTP';
            } else {
                $this->loadComponent('Sms');
                $this->Flash->success('Mobile number successfully Verified');
                $this->Users->query()->update()->set(['otp' => '', 'is_mobile_verified' => '1', 'modified' => date('Y-m-d H:i:s')])->where(['id' => $userId])->execute();
            }
        }
        $this->_jsonResponse($response);
    }

    public function regisrationPayment() {
        $this->viewBuilder()->layout('dashboard');
        $user = $this->_getUserDetail($this->Auth->user('id'));
        if ($user['paid']) {
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }
        $paymentTable = TableRegistry::get('Payments');
        $amount = 10000;
        $paymentId = time() . $this->Auth->user('id');
        $paymentData = [
            'user_id' => $this->Auth->user('id'),
            'payment_id' => $paymentId,
            'payment_type' => 'registration',
            'type_id' => $this->Auth->user('id'),
            'amount' => $amount
        ];
        $paymentEntity = $paymentTable->newEntity($paymentData);
        $paymentTable->save($paymentEntity);
        $this->loadComponent('Ccavenue');
        $redirectionUrl = Router::url(['controller' => 'Payment', 'action' => 'response'], true);
        $url = $this->Ccavenue->getUrl();
        $request = $this->Ccavenue->getPaymentdata($paymentId, $amount, $redirectionUrl);
        $accessCode = $this->Ccavenue->getAccessCode();
        $this->set(compact('url', 'accessCode', 'request'));
    }

    public function getRegistrationDocs() {
        $this->autoRender = false;
        $this->_ajaxCheck();
        if (!$this->request->is('post')) {
            exit("Invalid Request");
        }
        $response['error'] = 1;
        $session = $this->request->session();
        $maskedKey = $session->read('Registration.maskedKey');
        $userId = base64_decode($maskedKey);
        $userData = $this->_getUserData($userId);
        if ($userData) {
            $response['error'] = 0;
            $userDocuments = $this->_getUserDocuments($userId);
            $representativeId = $userData['representative_id'];
            $registrationDocumentsTable = TableRegistry::get('RegistrationDocuments');
            $registrationDocumentsArr = $registrationDocumentsTable->find('all')->select(['id', 'name', 'field', 'is_required', 'related_to'])->where(['FIND_IN_SET("' . $representativeId . '",entity)'])->hydrate(false)->toArray();
            $registrationDocuments = array_map(function($arr) use($userDocuments) {
                $field = $arr['field'];
                $arr['uploaded'] = 0;
                $userDocumentsArr = array_filter($userDocuments, function($element) use($field) {
                    return $element['field'] == $field;
                });
                if (count($userDocumentsArr)) {
                    $arr['uploaded'] = 1;
                }
                return $arr;
            }, $registrationDocumentsArr);
            $response['data'] = $registrationDocuments;
        }
        $this->_jsonResponse($response);
    }

    public function uploadDoc() {
        $this->autoRender = false;
        $response['error'] = 1;
        $documentName = '';
        $documentNameArr = array_keys($this->request->data);
        $session = $this->request->session();
        $maskedKey = $session->read('Registration.maskedKey');
        $userId = base64_decode($maskedKey);
        if (isset($documentNameArr[0])) {
            $documentName = $documentNameArr[0];
        }
        $response['file'] = $documentName;
        if ($this->request->data[$documentName]['size']) {
            $ext = pathinfo($this->request->data[$documentName]['name'], PATHINFO_EXTENSION);
            $allowed = array('gif', 'jpeg', 'png', 'jpg', 'doc', 'docx', 'xls', 'xlsx', 'pdf');
            if (!in_array(strtolower($ext), $allowed)) {
                $response['error'] = 1;
                $response['file'] = $documentName;
                $response['data'] = 'These files extension are allowed: .png, .gif, .jpeg, .jpg, .doc, .docx, .xls, .xlsx, .pdf';
                $this->_jsonResponse($response);
            }
            $fileName = time() . '_' . $this->_generateRandomString(20) . '_' . $documentName . '.' . $ext;
            $uploadFile = WWW_ROOT . 'uploads/register_documents/' . $fileName;
            if (move_uploaded_file($this->request->data[$documentName]['tmp_name'], $uploadFile)) {
                $response['error'] = 0;
                $response['file'] = $documentName;
                $userDocumentsTable = TableRegistry::get('UserDocuments');
                $userDocumentEntity = $userDocumentsTable->newEntity(['user_id' => $userId, 'field' => $documentName, 'document' => $fileName]);
                $userDocumentsTable->save($userDocumentEntity);
            }
        }
        $userData = $this->_getUserData($userId);
        $alldocumentUploaded = $this->_checkAlldocumentsUploaded($userData);
        if ($alldocumentUploaded) {
            $user_token = md5($this->_generateRandomString() . '_' . time());
            $usersTable = TableRegistry::get('Users');
            $usersTable->query()->update()->set(['doc_uploaded' => 1, 'user_token' => $user_token])->where(['id' => $userId])->execute();
            $url = Router::url(['action' => 'verifyEmail', 'token' => $user_token, 'mask' => base64_encode($userId)], true);
            $emailData = $usersTable->find('all')->where(['id' => $userId])->hydrate(false)->first();
            $response['error'] = 2;
            $response['data'] = 'You have been successfully registered. Please check your email to proceed with verification process.';
            $this->getMailer('User')->send('emailVerification', [$emailData, $url]);
        }
        $this->_jsonResponse($response);
    }

    public function markNotApplicable() {
        $this->autoRender = false;
//        $this->_ajaxCheck();
        $session = $this->request->session();
        $maskedKey = $session->read('Registration.maskedKey');
        $userId = base64_decode($maskedKey);
        $userData = $this->_getUserData($userId);
        $field = $this->request->data['name'];
        $response['error'] = 1;
        if ($field) {
            $userDocumentsTable = TableRegistry::get('UserDocuments');
            $userDocumentEntity = $userDocumentsTable->newEntity(['user_id' => $userId, 'field' => $field, 'document' => '']);
            $userDocumentsTable->save($userDocumentEntity);
            $response['error'] = 0;
            $response['file'] = $field;
        }
        $alldocumentUploaded = $this->_checkAlldocumentsUploaded($userData);
        if ($alldocumentUploaded) {
            $user_token = md5($this->_generateRandomString() . '_' . time());
            $usersTable = TableRegistry::get('Users');
            $usersTable->query()->update()->set(['doc_uploaded' => 1, 'user_token' => $user_token])->where(['id' => $userId])->execute();
            $url = Router::url(['action' => 'verifyEmail', 'token' => $user_token, 'mask' => base64_encode($userId)], true);
            $emailData = $usersTable->find('all')->where(['id' => $userId])->hydrate(false)->first();
            $response['error'] = 2;
            $response['data'] = 'You have been successfully registered. Please check your email to proceed with verification process.';
            $this->getMailer('User')->send('emailVerification', [$emailData, $url]);
        }
        $this->_jsonResponse($response);
    }

}
