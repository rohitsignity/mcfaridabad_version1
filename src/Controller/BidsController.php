<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;

class BidsController extends AppController {

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->viewBuilder()->layout('dashboard');
    }

    public function index() {
        
    }

    public function addBid($tenderid = null) {
        $devicePath = Router::url('/') . 'uploads/devices/';
        $tenderId = base64_decode($tenderid);
        $appliedBidsTable = TableRegistry::get('AppliedBids');
        $appliedBids = $appliedBidsTable->find('all')->where(['user_id' => $this->Auth->user('id'), 'tender_id' => $tenderId])->count();
        if ($appliedBids) {
            $this->Flash->error(__('You have already applied for the bid. We will get back to you soon.'));
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }
        $tenderTable = TableRegistry::get('Tenders');
        $tender = $tenderTable->find('all')->select(['Tenders.id', 'Tenders.name', 'Tenders.device_id', 'Tenders.start_date', 'Tenders.end_date', 'Tenders.min_amount', 'Tenders.contact', 'Tenders.web_link', 'Tenders.publish_type', 'Tenders.publish_status', 'Tenders.created', 'Tenders.modified', 'Tenders.doc_names', 'users' => 'GROUP_CONCAT(u.user_id)'])
                        ->join([
                            'u' => [
                                'table' => 'tender_users',
                                'type' => 'LEFT',
                                'conditions' => 'Tenders.id = u.tender_id'
                            ]
                        ])->where(['Tenders.id' => $tenderId])
                        ->group('Tenders.id')
                        ->hydrate(false)->first();
        if (date('Y-m-d') > date('Y-m-d', strtotime($tender['end_date']))) {
            $this->Flash->error(__('Tender has been Expired'));
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }
        $tender['users'] = explode(',', $tender['users']);
        $tender['doc_names'] = array_filter(explode('#$@*', $tender['doc_names']));
        if (!in_array($this->Auth->user('id'), $tender['users'])) {
            $this->Flash->error(__('You are not allowed to access this URL.'));
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }
        $bids = $this->Bids->find('all')->select(['amount', 'bidDate' => 'DATE_FORMAT(created,"%d %b, %Y %h:%i%p")'])->where(['user_id' => $this->Auth->user('id'), 'tender_id' => $tenderId])->order(['created' => 'DESC'])->hydrate(false)->toArray();
        $deviceData = $this->__getDeviceData($tender['device_id']);
        $this->set(compact('bids'));
        $this->set(compact('devicePath'));
        $this->set(compact('deviceData'));
        $this->set(compact('tender'));
    }

    private function __getDeviceData($deviceId) {
        $devices = [];
        $fieldsKeysArr = [];
        $deviceTable = TableRegistry::get('CompanyDevices');
        $devicesArr = $deviceTable->find('all')->select([
                    'id',
                    'company_code',
                    'status',
                    'created',
                    'company_name' => 'c.company_name',
                    'representative' => 'r.representative',
                    'name' => 'u.display_name',
                    'phone' => 'u.mobile',
                    'email' => 'u.email',
                    'incorporation_date' => 'c.incorporation_date',
                    'address' => 'CONCAT(am.address,", ",am.city,", ",am.state)',
                    'permanent_address' => 'pm.address',
                    'permanent_city' => 'pm.city',
                    'permanent_state' => 'pm.state',
                    'permanent_pincode' => 'pm.pincode',
                    'permanent_email' => 'pm.email',
                    'permanent_phone' => 'pm.phone',
                    'mailing_address' => 'am.address',
                    'mailing_city' => 'am.city',
                    'mailing_state' => 'am.state',
                    'mailing_pincode' => 'am.pincode',
                    'mailing_email' => 'am.email',
                    'mailing_phone' => 'am.phone',
                    'typology' => 'pc.title',
                    'sub_category' => 'sc.title',
                    'deviceData' => 'deviceData.fieldData'
                ])->join([
                    'u' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' => 'CompanyDevices.company_code = u.company_code AND CompanyDevices.by_admin = 0'
                    ],
                    'c' => [
                        'table' => 'companies',
                        'type' => 'LEFT',
                        'conditions' => 'CompanyDevices.company_code = c.company_code AND CompanyDevices.by_admin = 0'
                    ],
                    'r' => [
                        'table' => 'representatives',
                        'type' => 'LEFT',
                        'conditions' => 'c.representative_id = r.id'
                    ],
                    'pm' => [
                        'table' => 'addresses',
                        'type' => 'LEFT',
                        'conditions' => 'CompanyDevices.company_code = pm.company_code AND pm.type = "permanent" AND CompanyDevices.by_admin = 0'
                    ],
                    'am' => [
                        'table' => 'addresses',
                        'type' => 'LEFT',
                        'conditions' => 'CompanyDevices.company_code = am.company_code AND am.type = "mailing" AND CompanyDevices.by_admin = 0'
                    ],
                    'pc' => [
                        'table' => 'categories',
                        'type' => 'LEFT',
                        'conditions' => 'CompanyDevices.category_id = pc.id'
                    ],
                    'sc' => [
                        'table' => 'categories',
                        'type' => 'LEFT',
                        'conditions' => 'CompanyDevices.sub_category_id = sc.id'
                    ],
                    'deviceData' => [
                        'table' => "(SELECT `company_device_id`, GROUP_CONCAT(CONCAT(`field_key`,'*^$',`input_value`) SEPARATOR '#@!') AS fieldData FROM `company_device_fields` GROUP BY `company_device_id`)",
                        'type' => 'LEFT',
                        'conditions' => 'CompanyDevices.id = deviceData.company_device_id'
                    ]
                ])->where(['CompanyDevices.id' => $deviceId])->hydrate(false)->toArray();
        foreach ($devicesArr as $key => $devicesItem) {
            $devices[$key] = $devicesItem;
            $deviceDataArr = explode('#@!', $devicesItem['deviceData']);
            $deviceFields = [];
            foreach ($deviceDataArr as $deviceDataItem) {
                $deviceDataItemArr = explode('*^$', $deviceDataItem);
                $fieldsKeysArr[] = $deviceDataItemArr[0];
                $deviceFields[] = array(
                    'field' => $deviceDataItemArr[0],
                    'value' => $deviceDataItemArr[1]
                );
            }
            $devices[$key]['fields'] = $deviceFields;
            $addresses = [
                'company' => [
                    ['label' => 'Company Name/Building Owner', 'value' => $devicesItem['company_name']],
                    ['label' => 'Representative', 'value' => $devicesItem['representative']],
                    ['label' => 'Name', 'value' => $devicesItem['name']],
                    ['label' => 'Telephone No.', 'value' => $devicesItem['phone']],
                    ['label' => 'E-mail', 'value' => $devicesItem['email']],
                    ['label' => 'Date Of Incorporation', 'value' => date('d M, Y', strtotime($devicesItem['incorporation_date']))],
                ],
                'Permanent' => [
                    ['label' => 'Address', 'value' => $devicesItem['permanent_address']],
                    ['label' => 'City', 'value' => $devicesItem['permanent_city']],
                    ['label' => 'State', 'value' => $devicesItem['permanent_state']],
                    ['label' => 'Pincode', 'value' => $devicesItem['permanent_pincode']],
                    ['label' => 'Email', 'value' => $devicesItem['permanent_email']],
                    ['label' => 'Telephone No.', 'value' => $devicesItem['permanent_phone']]
                ],
                'Mailing' => [
                    ['label' => 'Address', 'value' => $devicesItem['mailing_address']],
                    ['label' => 'City', 'value' => $devicesItem['mailing_city']],
                    ['label' => 'State', 'value' => $devicesItem['mailing_state']],
                    ['label' => 'Pincode', 'value' => $devicesItem['mailing_pincode']],
                    ['label' => 'Email', 'value' => $devicesItem['mailing_email']],
                    ['label' => 'Telephone No.', 'value' => $devicesItem['mailing_phone']],
                ]
            ];
            $devices[$key]['address'] = $addresses;
            unset($devices[$key]['deviceData']);
            unset($devices[$key]['permanent_address']);
            unset($devices[$key]['permanent_city']);
            unset($devices[$key]['permanent_state']);
            unset($devices[$key]['permanent_pincode']);
            unset($devices[$key]['permanent_email']);
            unset($devices[$key]['permanent_phone']);
            unset($devices[$key]['mailing_address']);
            unset($devices[$key]['mailing_city']);
            unset($devices[$key]['mailing_state']);
            unset($devices[$key]['mailing_pincode']);
            unset($devices[$key]['mailing_email']);
            unset($devices[$key]['mailing_phone']);
        }
        $fieldsTable = TableRegistry::get('Fields');
        $fieldsArr = $fieldsTable->find('all')->select(['field_key', 'type', 'lable'])->where(['field_key IN' => $fieldsKeysArr])->hydrate(false);
        $fieldsData = $fieldsArr->toArray();
        if ($devices) {
            $response = reset($devices);
            $response['fieldsData'] = $fieldsData;
            return $response;
        }
        return [];
    }

    public function saveBids() {
        $this->autoRender = false;
        $this->_ajaxCheck();
        if ($this->request->is('post')) {
            $response['error'] = 0;
            $tenderId = $this->request->data['id'];
            $bidAmount = $this->request->data['bid'];
            $userId = $this->Auth->user('id');
            $allBids = $this->Bids->find('all')->where(['tender_id' => $tenderId, 'user_id' => $userId])->hydrate(false)->toArray();
            $lateAmmountData = array_values(array_filter($allBids, function($arr) use($bidAmount) {
                        return $arr['amount'] == $bidAmount;
                    }));
            if (count($allBids) >= $this->bidLimit) {
                $response['error'] = 1;
                $response['message'] = 'You have Exceeded the limit of ' . $this->bidLimit . ' bids';
                $this->_jsonResponse($response);
            } else if (count($lateAmmountData)) {
                $response['error'] = 1;
                $response['message'] = 'You have already bid for the Amount ' . $bidAmount . '. Please Try another one.';
                $this->_jsonResponse($response);
            }
            $data = [
                'amount' => $bidAmount,
                'user_id' => $userId,
                'tender_id' => $tenderId
            ];
            $bidEntity = $this->Bids->newEntity($data);
            $saved = $this->Bids->save($bidEntity);
            if ($saved) {
                $docuemtsData = [];
                $documents = $this->request->data['document_name'];
                foreach ($documents as $key => $document) {
                    if (isset($this->request->data['document_' . $key])) {
                        $ext = pathinfo($this->request->data['document_' . $key]['name'], PATHINFO_EXTENSION);
                        $fileName = time() . '_' . $this->_generateRandomString(20) . '_' . $userId . '.' . $ext;
                        $uploadFile = WWW_ROOT.'uploads/bidDocuments/' . $fileName;
                        move_uploaded_file($this->request->data['document_' . $key]['tmp_name'], $uploadFile);
                        $docuemtsData[] = [
                            'doc_name' => $document,
                            'doc_file' => $fileName,
                            'bid_id' => $saved->id
                        ];
                    }
                }
                if ($docuemtsData) {
                    $bidDocumentsTable = TableRegistry::get('BidDocuments');
                    $bidDocumentsEntities = $bidDocumentsTable->newEntities($docuemtsData);
                    $bidDocumentsTable->saveMany($bidDocumentsEntities);
                }
            }
            $response['bids'] = $this->Bids->find('all')->select(['amount', 'bidDate' => 'DATE_FORMAT(created,"%d %b, %Y %h:%i%p")'])->where(['user_id' => $this->Auth->user('id'), 'tender_id' => $tenderId])->order(['created' => 'DESC'])->hydrate(false)->toArray();
            if (count($response['bids']) < $this->bidLimit) {
                $response['message'] = 'You can bid ' . ($this->bidLimit - count($response['bids'])) . ' more time(s)';
            } else {
                $response['message'] = 'You have reached the Limit of Bids';
            }
            $this->_jsonResponse($response);
        }
        exit("You are not allowed to access this URL");
    }

    public function applyBid() {
        $this->autoRender = false;
        $response['error'] = 0;
        $this->_ajaxCheck();
        if ($this->request->is('post')) {
            $tenderId = $this->request->data['id'];
            $userId = $this->Auth->user('id');

            $tenderTable = TableRegistry::get('Tenders');
            $tender = $tenderTable->find('all')->where(['id' => $tenderId])->hydrate(false)->first();
            if (date('Y-m-d') > date('Y-m-d', strtotime($tender['end_date']))) {
                $response['error'] = 1;
                $response['message'] = 'You cannot apply for this tender.Tender already exprired.';
                $this->_jsonResponse($response);
            }
            $appliedBidsTable = TableRegistry::get('AppliedBids');
            $appliedBids = $appliedBidsTable->find('all')->where(['user_id' => $userId, 'tender_id' => $tenderId])->count();
            if ($appliedBids) {
                $response['error'] = 1;
                $response['message'] = 'You have already applied, We will get back to you soon.';
                $this->_jsonResponse($response);
            }
            $appliedBidEntity = $appliedBidsTable->newEntity(['user_id' => $userId, 'tender_id' => $tenderId]);
            $savedApplication = $appliedBidsTable->save($appliedBidEntity);
            if ($savedApplication) {
                $bidDocumentsTable = TableRegistry::get('BidDocuments');
                $docuemtsData = [];
                $documents = isset($this->request->data['document_name']) ? $this->request->data['document_name'] : array();
                foreach ($documents as $key => $document) {
                    if (isset($this->request->data['document_' . $key])) {
                        $ext = pathinfo($this->request->data['document_' . $key]['name'], PATHINFO_EXTENSION);
                        $fileName = time() . '_' . $this->_generateRandomString(20) . '_' . $userId . '.' . $ext;
                        $uploadFile = WWW_ROOT.'uploads/bidDocuments/' . $fileName;
                        move_uploaded_file($this->request->data['document_' . $key]['tmp_name'], $uploadFile);
                        $docuemtsData[] = [
                            'doc_name' => $document,
                            'doc_file' => $fileName,
                            'application_id' => $savedApplication->id
                        ];
                    }
                }
                if ($docuemtsData) {
                    $bidDocumentsEntities = $bidDocumentsTable->newEntities($docuemtsData);
                    $bidDocumentsTable->saveMany($bidDocumentsEntities);
                }
            }
            $response['message'] = 'Thank you showing Interest, We will get back to you soon.';
            $this->_jsonResponse($response);
        }
    }

}
