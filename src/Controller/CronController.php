<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Mailer\MailerAwareTrait;
use App\Traits\Devices;

class CronController extends AppController {

    use MailerAwareTrait, Devices;

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow();
    }

    public function index() {
        exit("You are not allowed to access this page");
    }

    public function omdInstallmentReminder() {
        $this->autoRender = false;
        $this->loadComponent('Sms');
        $currentDate = date('Y-m-d');
//        $currentDate = '2019-03-31';
        $date15DaysAfter = date('Y-m-d', strtotime($currentDate.' + 15 days'));
        $dateBeforeOneday = date('Y-m-d', strtotime($currentDate.' +1 day'));
        $installmentsTable = TableRegistry::get('Installments');
        $installments = $installmentsTable->find('all')->where([
            'OR' => [
                ['start_date' => $date15DaysAfter],
                ['start_date' => $dateBeforeOneday]
            ],
            'AND' => [
                'payment_id' => 0
            ]
        ])->hydrate(false)->toArray();
        $omdIds = array_map(function($arr) {
            return $arr['device_id'];
        }, $installments);
        if(!$installments) {
            die("No pending Payments");
        }
        $devices = $this->_getDeviceData($omdIds);
        foreach ($installments as $installment) {
            $device_id = $installment['device_id'];
            $amount = number_format($installment['amount'],2);
            $deviceInfoArr = array_values(array_filter($devices,function($arr) use($device_id){
                return $arr['id'] == $device_id;
            }));
            if(!$deviceInfoArr) {
                continue;
            }
            $deviceInfo = reset($deviceInfoArr);
            $this->getMailer('Cron')->send('omdInstallmentReminder', [$deviceInfo, $installment]);
            $sms = 'The annual/quarterly permission fee of Rs.'.$amount.' is due for payment. Kindly pay it before '.date('d M, Y',strtotime($installment['start_date']));
            $this->Sms->sendTextSms($deviceInfo['phone'],$sms);
        }
        die(count($installments).' installments processed');
    }

    public function omdPaymentReminder() {
        $this->autoRender = false;
        $this->loadComponent('Sms');
        $conn = ConnectionManager::get('default');
        $conn->execute('SET group_concat_max_len = 5555555');
        $companyDevicesTable = TableRegistry::get('CompanyDevices');
        $companyDevicesArr = $companyDevicesTable->find()->select([
                    'id' => 'CompanyDevices.id',
                    'user_id' => 'CompanyDevices.user_id',
                    'payment_id' => 'omdPayment.payment_id',
                    'display_name' => 'users.display_name',
                    'email' => 'users.email',
                    'mobile' => 'users.mobile',
                    'last_date' => 'omdModification.last_date',
                    'deviceData' => 'deviceData.fieldData'
                ])->join([
                    'table' => "(SELECT `company_device_id`, GROUP_CONCAT(CONCAT(`field_key`,'*^$',`input_value`) SEPARATOR '#@!') AS fieldData FROM `company_device_fields` GROUP BY `company_device_id`)",
                    'alias' => 'deviceData',
                    'type' => 'LEFT',
                    'conditions' => 'CompanyDevices.id = deviceData.company_device_id'
                ])->join([
                    'table' => '(SELECT * FROM (SELECT * FROM `omd_payments` ORDER BY `id` DESC) tbl GROUP BY `device_id`)',
                    'alias' => 'omdPayment',
                    'type' => 'LEFT',
                    'conditions' => 'CompanyDevices.id = omdPayment.device_id'
                ])->join([
                    'table' => '(SELECT *, DATE_ADD(tbl.modified, INTERVAL 5 DAY) AS notification_date, DATE_ADD(tbl.modified, INTERVAL 7 DAY) AS last_date FROM (SELECT * FROM `mcg_comments` ORDER BY `id` DESC) tbl GROUP BY `device_id`)',
                    'alias' => 'omdModification',
                    'type' => 'LEFT',
                    'conditions' => 'CompanyDevices.id = omdModification.device_id'
                ])->join([
                    'table' => 'users',
                    'type' => 'LEFT',
                    'conditions' => 'CompanyDevices.user_id = users.id'
                ])->where(['CompanyDevices.status' => 'approved', 'omdPayment.payment_id IS' => NULL, 'DATE(omdModification.notification_date)' => date('Y-m-d')])->hydrate(false)->order(['CompanyDevices.id' => 'DESC'])->toArray();
        $companyDevices = array_map(function($arr) {
            $deviceData = [];
            $data = explode('#@!', $arr['deviceData']);
            array_walk($data, function($val) use(&$deviceData) {
                $valArr = explode('*^$', $val);
                $deviceData[] = array('key' => $valArr[0], 'value' => $valArr[1]);
            });
            $arr['deviceData'] = $deviceData;
            return $arr;
        }, $companyDevicesArr);
        foreach ($companyDevices as $companyDevice) {
            $fees = 0;
            $fieldsData = $companyDevice['deviceData'];
            $feesArr = array_values(array_filter($fieldsData, function($arr) {
                        return $arr['key'] == 'annual_license_fees';
                    }));
            if (!$feesArr) {
                $feesArr = array_values(array_filter($fieldsData, function($arr) {
                            return $arr['key'] == 'concession_fees';
                        }));
            }
            if (!$feesArr) {
                $feesArr = array_values(array_filter($fieldsData, function($arr) {
                            return $arr['key'] == 'total_omd_fees';
                        }));
            }
            if ($feesArr) {
                $fees = $feesArr[0]['value'];
            }
            if ($fees) {
                $sms = 'Your final payment of Rs. ' . number_format($fees, 2) . ' is pending for the OMD Device. Please pay before ' . date('d M, Y h:iA', strtotime($companyDevice['last_date'])) . '. Otherwise your application will be canceled.';
                $this->getMailer('Cron')->send('omdPaymentReminder', [$companyDevice, $fees]);
                $this->Sms->sendTextSms($companyDevice['mobile'], $sms);
            }
        }
        die('Notification Sent');
    }

}
