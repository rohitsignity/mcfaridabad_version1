<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use App\Traits\Tenders;
use App\Traits\Users;
use App\Traits\Devices;
use App\Traits\Bids;
use Cake\Mailer\MailerAwareTrait;

class EAuctionController extends AppController {

    use Tenders,
        Users,
        Devices,
        Bids,
        MailerAwareTrait;

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        return $this->redirect(['controller' => 'Dashboard','action' => 'index']);
        $this->viewBuilder()->layout('dashboard');
        $session = $this->request->session();
        $omdType = $session->read('omdType');
        if ($omdType != 'public') {
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }
        $userData = $this->_getUserData($this->Auth->user('id'));
        if (!$userData['is_mobile_verified']) {
            return $this->redirect(['controller' => 'Users', 'action' => 'mobileVerification']);
        }
        if (!$userData['paid']) {
            return $this->redirect(['controller' => 'Users', 'action' => 'regisrationPayment']);
        }
//        if ($this->user['representative_id'] == 1) {
//            $this->Flash->error('You are not allowed to access this URL.');
//            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
//        }
    }

    public function index() {
        $limit = 10;
        $this->paginate = [
            'limit' => $limit
        ];
        $tendersTable = TableRegistry::get('Tenders');
        $tendersArr = $tendersTable->find('all')->select([
                    'Tenders.id',
                    'Tenders.name',
                    'Tenders.tender_value',
                    'Tenders.start_date',
                    'end_date' => 'Tenders.end_date_updated',
                    'Tenders.application_date',
                    'application_status' => 'IF(ab.status IS NULL OR ab.status = "",2,ab.status)',
                    'paid' => 'IF(ab.paid IS NULL OR ab.paid="",0,ab.paid)'
                ])->join([
                    'at' => [
                        'table' => '(SELECT DISTINCT tender_id FROM tender_users WHERE user_id = ' . $this->user['id'] . ')',
                        'type' => 'RIGHT',
                        'conditions' => 'Tenders.id = at.tender_id'
                    ],
                    'ab' => [
                        'table' => '(SELECT * FROM applied_bids WHERE `paid` = "1" AND `user_id` = ' . $this->user['id'] . ')',
                        'type' => 'LEFT',
                        'conditions' => 'Tenders.id = ab.tender_id'
                    ]
                ])->where(['Tenders.publish_status' => '1', 'Tenders.status' => '1'])->order(['Tenders.id' => 'DESC'])->hydrate(false);
//        $tenders = $this->paginate($tendersTable->find('all')->order(['created' => 'DESC'])->hydrate(false))->toArray();
        $tenders = $this->paginate($tendersArr)->toArray();
        $session = $this->request->session();
        $receipt = $session->read('Tender.receipt');
        $session->delete('Tender.receipt');
        $this->set(compact('limit','receipt'));
        $this->set(compact('tenders'));
    }

    public function downloadBidDocument() {
        $this->autoRender = false;
        $this->_ajaxCheck();
        $response['error'] = 1;
        $tenderId = $this->request->data['id'];
        $tendersTable = TableRegistry::get('Tenders');
        $tender = $tendersTable->find('all')->where(['id' => $tenderId])->hydrate(false)->first();
        if (date('Y-m-d', strtotime($tender['doc_download_date'])) <= date('Y-m-d')) {
            $response['error'] = 0;
            $response['url'] = Router::url('/uploads/bid_instructions_documents/' . $tender['bid_doc']);
            $fileExtensionArr = pathinfo($tender['bid_doc']);
            $fileExtension = $fileExtensionArr['extension'];
            $response['doc'] = 'Instructions.' . $fileExtension;
        } else {
            $response['message'] = 'You cannot download this document before ' . date('d M, Y', strtotime($tender['doc_download_date']));
        }
        $this->_jsonResponse($response);
    }

    public function startBid($tenderId = null) {
        if (!$tenderId) {
            return $this->redirect(['action' => 'index']);
        }
        $bidApplicationArr = $this->_getBidApplicationStatus($tenderId, $this->user['id']);
        if(!$bidApplicationArr) {
            $this->Flash->error(__('Your application not found for this Tender'));
            return $this->redirect(['action' => 'index']);
        } else if($bidApplicationArr['status'] != 1) {
            $this->Flash->error(__('You are not allowed to access this URL.'));
            return $this->redirect(['action' => 'index']);
        }
        $tenderUsersTable = TableRegistry::get('TenderUsers');
        $tenderUser = $tenderUsersTable->find('all')->where(['tender_id' => $tenderId, 'user_id' => $this->user['id']])->hydrate(false)->first();
        if (!$tenderUser) {
            $this->Flash->error(__('You are not allowed to access this URL.'));
            return $this->redirect(['action' => 'index']);
        }
        $tendersTable = TableRegistry::get('Tenders');
        $tender = $tendersTable->get($tenderId)->toArray();
        if (date('Y-m-d H:i:s', strtotime($tender['start_date'])) > date('Y-m-d H:i:s')) {
            $this->Flash->error(__('Biddig will start on ' . date('d M, Y h:iA', strtotime($tender['start_date'])) . '.'));
            return $this->redirect(['action' => 'index']);
        } else if (date('Y-m-d H:i:s', strtotime($tender['end_date_updated'])) < date('Y-m-d')) {
            $this->Flash->error(__('Tender has been Expired'));
            return $this->redirect(['action' => 'index']);
        }
        if (!$this->__verifyPayment($tenderId)) {
            return $this->redirect(['controller' => 'EAuction', 'action' => 'paymentVerification', $tenderId]);
        }
        $bidIncrimentPercentage = $this->bidIncrimentPercentage;
        $this->set(compact('bidIncrimentPercentage'));
        $this->set(compact('tender'));
    }

    private function __verifyPayment($tenderId) {
        $eAuctionPaymentsTable = TableRegistry::get('Payments');
        $eAuctionPayment = $eAuctionPaymentsTable->find('all')->where(['user_id' => $this->user['id'], 'type_id' => $tenderId,'type' => 'tender','status' => '1'])->hydrate(false)->first();
        if (!$eAuctionPayment) {
            return false;
        }
        return true;
    }

    public function paymentVerification($tenderId = null) {
        if(!$tenderId) {
            return $this->redirect(['action' => 'index']);
        }
        $tendersTable = TableRegistry::get('Tenders');
        $tender = $tendersTable->get($tenderId)->toArray();
        if ($this->_checkTenderExpired($tender['id'])) {
            $this->Flash->error(__('Tender has been Expired'));
            return $this->redirect(['action' => 'index']);
        }
        $paymentId = md5(time().'_'. $this->_generateRandomString(10)).time();
        $documentFee = $tender['document_fee'];
        $intialAmount = ($tender['tender_value'] * 5) / 100;
        $amount = 1;
        $user = $this->_getUserDetail($this->Auth->user('id'));
//        $amount = $intialAmount + $documentFee;
        $cancelUrl = Router::url(['controller' => 'EAuction', 'action' => 'index']);
        $this->loadComponent('Billdesk');
        $this->Billdesk->setAmount($amount);
        $this->Billdesk->setTransactionType("tender");
        $this->Billdesk->setUniqueId($paymentId);
        $liveUrl = $this->Billdesk->getLiveUrl();
        $pageMode = $this->Billdesk->getPageMode();
        $gatewayString	= $this->Billdesk->getPaymentStr();
        $paymentData = [
            'user_id' => $user['id'],
            'payment_id' => $paymentId,
            'type' => 'tender',
            'type_id' => $tenderId,
            'quantity' => 1,
            'buyer_name' => $user['display_name'],
            'buyer_phone' => $user['mobile'],
            'buyer_email' => $user['email'],
            'currency' => '₹',
            'unit_price' => $amount,
            'amount' => $amount
        ];
        $paymentTable = TableRegistry::get('payments');
        $paymentEntity = $paymentTable->newEntity($paymentData);
        $paymentTable->save($paymentEntity);
        $this->set(compact('confirmUrl','liveUrl','pageMode','gatewayString'));
        $this->set(compact('cancelUrl'));
        $this->set(compact('documentFee'));
        $this->set(compact('intialAmount'));
    }

    public function response() {
        $this->autoRender = false;
        $paymentId = $this->request->query('payment_id');
        $paymentRequestId = $this->request->query('payment_request_id');
        $paymentDataJson = file_get_contents('uploads/e_auction_session/' . $paymentRequestId . '.txt');
        $paymentData = json_decode($paymentDataJson, true);
        $tenderId = $paymentData['tenderId'];
        $this->loadComponent('Insta');
        $this->loadComponent('Pdf');
        $paymentDetail = $this->Insta->paymentDetail($paymentId);
        if ($paymentDetail['status'] == 'Credit') {
            $paymentTable = TableRegistry::get('EAuctionPayments');
            $paymentData = [
                'tender_id' => $tenderId,
                'user_id' => $paymentData['user']['id'],
                'payment_id' => $paymentDetail['payment_id'],
                'payment_request_id' => $paymentRequestId,
                'quantity' => $paymentDetail['quantity'],
                'status' => $paymentDetail['status'],
                'buyer_name' => $paymentDetail['buyer_name'],
                'buyer_phone' => $paymentDetail['buyer_phone'],
                'buyer_email' => $paymentDetail['buyer_email'],
                'currency' => $paymentDetail['currency'],
                'unit_price' => $paymentDetail['unit_price'],
                'amount' => $paymentDetail['amount'],
                'fees' => $paymentDetail['fees'],
                'created_at' => date('Y-m-d H:i:s', strtotime($paymentDetail['created_at'])),
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ];
            $paymentEntity = $paymentTable->newEntity($paymentData);
            $paymentTable->save($paymentEntity);
            $appliedBidsTable = TableRegistry::get('AppliedBids');
            $appliedBidsLast = $appliedBidsTable->find('all')->where(['tender_id' => $tenderId, 'user_id' => $paymentData['user_id']])->order(['id' => 'desc'])->hydrate(false)->first();
            $appliedBidsTable->query()->update()->set(['paid' => '1'])->where(['tender_id' => $tenderId, 'user_id' => $paymentData['user_id'], 'id' => $appliedBidsLast['id']])->execute();
            $pdfData = [
                'amount' => $paymentDetail['amount'],
                'id' => $tenderId,
                'user_id' => $paymentData['user_id'],
                'path'=> 'tender'
            ];
            $this->Pdf->generateReceipt($pdfData);
            $session = $this->request->session();
            $session->write('Tender.receipt', Router::url('/uploads/receipt/tender/tender_'.$tenderId.'_'.$paymentData['user_id'].'.pdf'));
            $this->getMailer('Tender')->send('applicationPayment', [json_decode($paymentDataJson)]);
//            $this->Flash->success(__('You have successfully paid for the Tender. You can start bidding now.'));
//            return $this->redirect(['controller' => 'EAuction', 'action' => 'startBid', $tenderId]);
            $this->Flash->success(__('You have successfully paid for the Tender application. You application is under admin aproval process.'));
            return $this->redirect(['action' => 'index']);
        }
        $this->Flash->error(__('Something Went wrong with the Payment. Please try again.'));
        return $this->redirect(['controller' => 'EAuction', 'action' => 'index']);
    }

    public function getBids() {
        $this->autoRender = false;
        $this->_ajaxCheck();
        $response['error'] = 0;
        $bidsTable = TableRegistry::get('Bids');
        $tenderId = $this->request->data['id'];
        $lastestBid = 'Start Bidding';
        $tenderExpired = $this->_checkTenderExpired($tenderId);
        $bidsData = $bidsTable->find('all')->select(['name' => 'sub.name', 'amount' => 'sub.amount'])->from(['sub' => '(SELECT * FROm bids WHERE tender_id = "' . $tenderId . '" ORDER BY id DESC)'])->group(['sub.user_id'])->order(['sub.amount' => 'DESC'])->hydrate(false)->toArray();
        $nextValue = 0;
        if (!$bidsData) {
            $tendersTable = TableRegistry::get('Tenders');
            $tender = $tendersTable->find('all')->where(['id' => $tenderId])->hydrate(false)->first();
            $nextValue = $tender['min_amount'];
        } else {
            $lastBid = reset($bidsData);
            $lastestBid = $lastBid['amount'];
            $nextValue = $lastBid['amount'] + (($lastBid['amount'] * $this->bidIncrimentPercentage) / 100);
        }
        $response['nextValue'] = $nextValue;
        $response['bids'] = $bidsData;
        $response['lastestBid'] = $lastestBid;
        $response['expirationTime'] = date('d M, Y h:iA', strtotime($this->_getTenderExpirationTime($tenderId)));
        if ($tenderExpired) {
            $response['error'] = 1;
            $response['title'] = 'Time Over!';
            $response['message'] = 'Bidding time Over. We will get back to you soon.';
        }
        $this->_jsonResponse($response);
    }

    public function addBid() {
        $this->autoRender = false;
        $response['error'] = 0;
//        $this->_ajaxCheck();
        $bidsTable = TableRegistry::get('Bids');
        $tenderId = $this->request->data['id'];
        $tender = $this->_tenderDetail($tenderId);
        $tenderExpired = $this->_checkTenderExpiredNew($tender);
        $lastBid = $bidsTable->find('all')->where(['tender_id' => $tenderId])->order(['id' => 'DESC'])->hydrate(false)->first();
        $newBidAmount = isset($lastBid['amount']) ? ($lastBid['amount'] + (($lastBid['amount'] * $this->bidIncrimentPercentage) / 100)) : 0;
        $amount = $this->request->data['amount'];
        if ($tenderExpired) {
            $response['error'] = 1;
            $response['title'] = 'Time Over!';
            $response['message'] = 'Bidding time Over. We will get back to you soon.';
        } else if ($amount < $newBidAmount) {
            $response['error'] = 1;
            $response['title'] = 'Error!';
            $response['message'] = 'Bidding Amount must be greater than equal to ' . $newBidAmount;
        } else {
            $data = [
                'name' => $this->_generateRandomName(5) . ' ' . $this->_generateRandomName(6),
                'amount' => $amount,
                'user_id' => $this->user['id'],
                'tender_id' => $tenderId
            ];
            $bidsEntity = $bidsTable->newEntity($data);
            $bidsTable->save($bidsEntity);
            $this->_updateTenderExpiration($tender);
        }
        $this->_jsonResponse($response);
    }

    public function checkStart() {
        $this->autoRender = false;
        $response['error'] = 0;
        $tenderId = $this->request->data['id'];
        $tendersTable = TableRegistry::get('Tenders');
        $tender = $tendersTable->find('all')->where(['id' => $tenderId])->hydrate(false)->first();
        if (date('Y-m-d H:i:s', strtotime($tender['start_date'])) > date('Y-m-d H:i:s')) {
            $response['error'] = 1;
            $response['message'] = 'Bidding will open at ' . date('h:iA', strtotime($tender['start_date']));
        }
        $this->_jsonResponse($response);
    }

    public function apply($tenderId = null) {
        if (!$tenderId) {
            return $this->redirect(['action' => 'index']);
        }
        $errors = [];
        $appliedBidsTable = TableRegistry::get('AppliedBids');
        $checkAlreadyApplied = $appliedBidsTable->find('all')->where(['tender_id' => $tenderId, 'user_id' => $this->user['id'], 'paid' => '1'])->first();
        if ($checkAlreadyApplied) {
            if (!$checkAlreadyApplied->status) {
                $this->Flash->error("Your bid application is under process");
            } else {
                $this->Flash->error("Your bid application is already processed");
            }
            return $this->redirect(['action' => 'index']);
        }
        $tender = $this->_tenderDetail($tenderId);
        if (date('Y-m-d') > date('Y-m-d', strtotime($tender['application_date']))) {
            $this->Flash->error("Application Submission time is over.");
            return $this->redirect(['action' => 'index']);
        } else if(date('Y-m-d') == date('Y-m-d', strtotime($tender['start_date'])) && (date('H:i:s')) > date('H:i:s', strtotime($tender['start_date']))) {
            $this->Flash->error("Application can not be submitted after bidding start time.");
            return $this->redirect(['action' => 'index']);
        }
        $post = $appliedBidsTable->newEntity();
        if ($this->request->is('post')) {
            $this->__validateApplicationDocs($errors);
            if (!count($errors)) {
                $post = $appliedBidsTable->patchEntity($post, $this->request->data);
                $post->user_id = $this->user['id'];
                $post->tender_id = $tenderId;
                $appliedBidSaved = $appliedBidsTable->save($post);
                if ($appliedBidSaved) {
                    if (isset($this->request->data['document'])) {
                        $bidDocumentsData = [];
                        $bidDocuments = $this->request->data['document'];
                        foreach ($bidDocuments as $docId => $bidDocument) {
                            $ext = pathinfo($bidDocument['name'], PATHINFO_EXTENSION);
                            $fileName = time() . '_' . $this->_generateRandomString(20) . '_' . $tenderId . '_' . $this->user['id'] . '.' . $ext;
                            $uploadFile = WWW_ROOT.'uploads/applied_bids_documents/' . $fileName;
                            if (move_uploaded_file($bidDocument['tmp_name'], $uploadFile)) {
                                $bidDocumentsData[] = [
                                    'applied_bid_id' => $appliedBidSaved->id,
                                    'document_id' => $docId,
                                    'document' => $fileName
                                ];
                            }
                        }
                        if (count($bidDocumentsData)) {
                            $appliedBidsDocumentsTable = TableRegistry::get('AppliedBidsDocuments');
                            $bidDocumentsEntities = $appliedBidsDocumentsTable->newEntities($bidDocumentsData);
                            $appliedBidsDocumentsTable->saveMany($bidDocumentsEntities);
                        }
                    }
                    if (!$this->__verifyPayment($tenderId)) {
                        return $this->redirect(['controller' => 'EAuction', 'action' => 'paymentVerification', $tenderId]);
                    }
                    $this->Flash->success("You application has successfully sent to SMC and under verifiation. we will get back to you soon.");
                    return $this->redirect(['action' => 'index']);
                }
            }
        }
        $devicePath = Router::url('/') . 'uploads/devices/';
        $tenderDocuments = $this->_getTenderRequiredDocuments($tenderId);
        $deviceIds = $this->_getTenderDeviceIds($tenderId);
        $devices = $this->_getDevicesByIds($deviceIds);
        $this->set(compact('tender', 'tenderDocuments', 'devices', 'post', 'errors','devicePath'));
    }

    private function __validateApplicationDocs(&$errors) {
        if (isset($this->request->data['document'])) {
            $allowedExtensions = ['gif', 'jpeg', 'png', 'jpg', 'doc', 'docx', 'xls', 'xlsx', 'pdf'];
            foreach ($this->request->data['document'] as $key => $document) {
                if (!$document['size']) {
                    $errors[$key] = ' is required';
                } else {
                    $ext = pathinfo($document['name'], PATHINFO_EXTENSION);
                    if (!in_array($ext, $allowedExtensions)) {
                        $errors[$key] = ' file extension should be .png, .gif, .jpeg, .jpg, .doc, .docx, .xls, .xlsx, .pdf';
                    }
                }
            }
        }
    }
    
    public function getDeviceInfo() {
        $this->autoRender = false;
        $this->_ajaxCheck();
        $deviceId = $this->request->data['deviceId'];
        $response = $this->_getDeviceData($deviceId);
        $this->_jsonResponse($response);
    }

}
