<?php

namespace App\Controller\Component;

use Cake\Controller\Component;

class G8Component extends Component {

    //private $url = 'http://demo.mcg.gov.in/OPBIRS/v2/opbirs.svc/UpdatePaymentOM';
    private $url = 'http://portal_mcf.simplexsolutions.com/OPBIRS/v2/opbirs.svc/UpdatePaymentOM';
    private $workingKey = 'OM123';
    //private $recieptUrl = 'http://demo.mcg.gov.in/PrintG8Report.aspx?id=';
    private $recieptUrl = 'http://portal_mcf.simplexsolutions.com/PrintG8Report.aspx?id=';

    
    public function send($data) {
		
        $content = $this->__getPayLoad($data);
        $curl = curl_init($this->url);
	curl_setopt($curl, CURLOPT_USERAGENT, $user_agent);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

        $json_response = curl_exec($curl);

        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($status != 200) {
//            die("Error: call to URL $this->url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
        }
        curl_close($curl);
        $response = json_decode($json_response, true);
        return $response;
    }
    
    private function __getPayLoad($data) {
        $data['WORKING_KEY'] = $this->workingKey;
        $dataJson = json_encode($data);
        return $dataJson;
    }
    
    public function receiptUrl($id) {
        return $this->recieptUrl.$id;
    }

}
