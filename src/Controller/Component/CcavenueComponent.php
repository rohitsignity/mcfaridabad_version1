<?php

namespace App\Controller\Component;

use Cake\Controller\Component;

class CcavenueComponent extends Component {

    private $merchantId = '138130';
    private $access_code = 'AVAB01FE78CN99BANC';
    private $workingKey = 'C135C739574EA30D5B26FD049A7F1D6C';
    private $url = 'https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction';

    private function encrypt($plainText) {
        $secretKey = $this->hextobin(md5($this->workingKey));
        $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
        $openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', 'cbc', '');
        $blockSize = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, 'cbc');
        $plainPad = $this->pkcs5_pad($plainText, $blockSize);
        if (mcrypt_generic_init($openMode, $secretKey, $initVector) != -1) {
            $encryptedText = mcrypt_generic($openMode, $plainPad);
            mcrypt_generic_deinit($openMode);
        }
        return bin2hex($encryptedText);
    }

    public function getUrl() {
        return $this->url;
    }

    public function getAccessCode() {
        return $this->access_code;
    }

    public function decrypt($encryptedText) {
        $secretKey = $this->hextobin(md5($this->workingKey));
        $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
        $encryptedText = $this->hextobin($encryptedText);
        $openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', 'cbc', '');
        mcrypt_generic_init($openMode, $secretKey, $initVector);
        $decryptedText = mdecrypt_generic($openMode, $encryptedText);
        $decryptedText = rtrim($decryptedText, "\0");
        mcrypt_generic_deinit($openMode);
        return $decryptedText;
    }

    private function pkcs5_pad($plainText, $blockSize) {
        $pad = $blockSize - (strlen($plainText) % $blockSize);
        return $plainText . str_repeat(chr($pad), $pad);
    }

    private function hextobin($hexString) {
        $length = strlen($hexString);
        $binString = "";
        $count = 0;
        while ($count < $length) {
            $subString = substr($hexString, $count, 2);
            $packedString = pack("H*", $subString);
            if ($count == 0) {
                $binString = $packedString;
            } else {
                $binString .= $packedString;
            }

            $count += 2;
        }
        return $binString;
    }
    
    public function getPaymentdata($paymentId,$amount,$redirectionUrl) {
        $paymentData = [];
        $paymentData[] = 'tid='. time();
        $paymentData[] = 'merchant_id='. $this->merchantId;
        $paymentData[] = 'order_id='.$paymentId;
        $paymentData[] = 'amount='.$amount;
        $paymentData[] = 'currency=INR';
        $paymentData[] = 'redirect_url='.$redirectionUrl;
        $paymentData[] = 'cancel_url='.$redirectionUrl;
        $paymentData[] = 'language=EN';
        $paymentData[] = 'merchant_param1=ADV';
        $paymentString = implode('&',$paymentData);
        return $this->encrypt($paymentString);
    }

}
