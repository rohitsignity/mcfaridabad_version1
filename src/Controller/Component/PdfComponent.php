<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Dompdf\Dompdf;
use Dompdf\Options;
use Cake\Routing\Router;
use App\Traits\Utility;

class PdfComponent extends Component {

    use Utility;

    public function generateReceipt($data = []) {
        set_time_limit(120);
        $options = new Options();
        $options->set('defaultFont', 'Courier');
        $options->set('isRemoteEnabled', TRUE);
        $options->set('debugKeepTemp', TRUE);
        $options->set('isHtml5ParserEnabled', true);
        $dompdf = new Dompdf($options);
        $html = $this->__getReceiptHtml($data);
        $dompdf->set_paper(array(0, 0, 520, 800), 'landscape');
        $dompdf->load_html($html);
        $dompdf->render();
        file_put_contents(WWW_ROOT . 'uploads/receipt/' . $data['path'] . '/' . $data['path'] . '_' . $data['id'] . '_' . $data['user_id'] . '.pdf', $dompdf->output());
    }

    private function __getReceiptHtml($data) {
        $amount = $this->_currencyInWords($data['amount']);
        //$title = 'Registration Fee';
        $title = 'Acknowledgement';
        $type = 'registration';
        $code = 'MCFVENDOR' . $data['id'] . 'U' . $data['user_id'];
        //$receipt_no = $data['user_id'] . 'OutdoorId:' . $data['r_no'];
         $receipt_no = $data['user_id'];
        $acknowledge_type = 'Reg. Id';
        if ($data['path'] == 'tender') {
            $title = 'Tender Application Fee';
            $type = 'tender application';
            $code = 'MCFTENDER' . $data['id'] . 'U' . $data['user_id'];
            $acknowledge_type = 'OMD Id';
            $receipt_no = $data['user_id'] . '_' . $data['id'];
        } else if ($data['path'] == 'device_registration') {
            $title = 'Acknowledgement';
            $type = 'OMD Processing Fee';
            $code = 'MCFOMD' . $data['id'] . 'U' . $data['user_id'];
            $acknowledge_type = 'OMD Id';
            $receipt_no = $data['user_id'] . '_' . $data['id'];
        } else if($data['path'] == 'permission') {
            $title = 'Acknowledgement';
            $type = 'OMD application';
            $code = 'MCFOMD' . $data['id'] . 'U' . $data['user_id'];
            $acknowledge_type = 'OMD Id';
            $receipt_no = $data['user_id'] . '_' . $data['id'];
        } else if($data['path'] == 'device_installment') {
            //$title = 'OMD Installment Fees';
            $title = 'Acknowledgement';
            $type = 'OMD installment';
            $code = 'MCFOMD' . $data['id'] . 'U' . $data['user_id'];
            $acknowledge_type = 'OMD Id';
            $receipt_no = $data['user_id'] . '_' . $data['id'];
        }
        $html = '<!doctype html>';
        $html .= '<html>';
        $html .= '<head>';
        $html .= '<meta charset="utf-8">';
        $html .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
        $html .= '<meta name="viewport" content="width=device-width, initial-scale=1">';
        $html .= '<link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">';
        $html .= '<title>SMC</title>';
        $html .= '<style type="text/css">body { font-family: Lato; font-family: \'Lato\';}</style>';
        $html .= '</head>';
        $html .= '<body style="font-family: \'Lato\', sans-serif; max-width:100%">';
        $html .= '<table style="border:1px solid #000; width:100%">';
        $html .= '<tbody>';
        $html .= '<tr style="background:#fed9c1;height:400px;">';
        //$html .= '<td style="background:#fed9c1;padding: 25px;vertical-align: middle; width: 25px;"><img src="' . WWW_ROOT . 'img/receipt-title.png"></td>';
	$html .= '<td style="background:#fed9c1;padding: 25px;vertical-align: middle; width: 25px;"></td>';
       // $html .= '<td style="background:#fed9c1;padding: 25px;vertical-align: middle; width: 25px;"><img src="' . WWW_ROOT . 'img/acknowledgement-title.png"></td>';
        $html .= '<td style="background:#fff; border-left:1px solid #000;" width="650">';
        $html .= '<table style="text-align:center;width:100%; background: url(\'' . WWW_ROOT . 'img/logo-bg-receipt.png\');">';
        $html .= '<tbody>';
        $html .= '<tr>';
        $html .= '<td style="text-align:center; font-size:28px;text-transform:uppercase;color:#333333; font-weight:700;letter-spacing:1px;padding-left:300px;">' . $title . '</td>';
        $html .= '<td style="text-align:right;"><img src="' . WWW_ROOT . 'img/logo.png"/></td>';
        $html .= '</tr>';
        $html .= '<td style="text-align:center; font-size:28px;text-transform:uppercase;color:#333333; font-weight:700;letter-spacing:1px;padding-left:300px;"></td>';
        $html .= '<td style="text-align:right;">Payment Id : '.$data['r_no'].'</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td style="text-align:center;" colspan="2">';
        $html .= '<h2 style="margin:10px 100px;padding:5px;background:#edecec;border-radius: 5px; font-size:15px; font-weight:200;letter-spacing:0.5">Acknowledgement of ' . $type . ' fee on mcfaridabad.org ('.$acknowledge_type.' MCF-' . $receipt_no . ')</h2></td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="2" style="font-size:18px;color:#3892b5; padding: 0px 190px 10px 190px; line-height: 1.3;">Payment amounting to <b>Rs ' . $data['amount'] . '/-(Rupees ' . $amount . ')</b> towards the ' . $type . ' process on mcfaridabad.org has been successfully processed.</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="2">';
        $html .= '<h4 style="margin:0 200px; padding:5px;border:2px dashed #333333;color:#333333;font-size:23px;font-weight:200;">Your ' . $type . ' number is <b style="font-weight:bold;">' . $code . '</b></h4></td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="2" style="color:#333333; font-size:17px;letter-spacing:0.5px;">Please quote the above number for future correspondence.</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="2" style="text-align:right;color:#333333; font-size:17px;padding-right: 70px;padding-top: 35px;"><span style="margin: 0 40px 0 0;">-SD-</span><br>Commissioner</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="2" style="color:#838181;font-size: 15px;padding: 10px 20px 0 20px;letter-spacing: 0.2px;">Note:- ' . ucfirst($type) . ' is subject to verification & approval of documents by MCF.The above ' . $type . ' does not authorize display of advertisements governed by relevant policies, rules and byelaws of Advertisement.</td>';
        $html .= '</tr>';
        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '</body>';
        $html .= '</html>';
        return $html;
    }

    public function generateOmdLetter($data = []) {
        set_time_limit(120);
        $html = $this->__getOmdLetterHtml($data);
        $options = new Options();
        $options->set('isHtml5ParserEnabled', true);
        $dompdf = new Dompdf($options);
        $dompdf->load_html($html);
        $dompdf->set_paper("a4", "portrait");
        $dompdf->render();
        file_put_contents(WWW_ROOT . 'uploads/omd_letters/omd_letter_' . $data['id'] . '_' . $data['user_id'] . '.pdf', $dompdf->output());
    }

    private function __getOmdLetterHtml($data) {
        $html = '<!DOCTYPE HTML>';
        $html .= '<html>';
        $html .= '<head>';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
        $html .= '<title>Approval letter by the Faridabad Municipal Corporation</title>';
        $html .= '<style>';
        $html .= '@page { margin: 40px 0 0 0; }';
        $html .= 'body{margin:0;padding:0}';
        $html .= 'p{margin:0 0 10px;}';
        $html .= '</style>';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table border="0" cellpadding="0" cellspacing="5" width="520" align="center" style="max-width:100%; padding: 0; line-height:1.4; font-size:18px;box-shadow: 0 4px 15px 5px rgba(0, 0, 0, 0.1);" >';
        $html .= '<tr>';
        $html .= '<th colspan="2" style="text-align: center; margin: 10px 0 0 0;"><strong>Approval letter by the Municipal Corporation Faridabad</strong> </th>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td width="50%"> No. &nbsp; ' . $data['file_no'] . '</td>';
        $html .= '<td width="50%" style="text-align: right;">Date:&nbsp; ' . $data['date'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td width="20%">To. &nbsp;<p> ' . $data['to'] . '</p><p>' . $data['address'] . '</p></td>';
        $html .= '<td width="80%"> </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="2"><p> Please refer to your application no.  ' . $data['app_no'] . '., dated ' . $data['app_date'] . ' for installation of New Outdoor Media Device/ renewal for Display of Outdoor Advertisement.</p>  </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="2">Dear Sir, </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="2" style="text-indent:50px;"><p>This is with reference to your application regarding 
        installation of New Outdoor Media device/ renewal for display of Outdoor Advertisement by your 
        Company. Firm/ Agency with the Municipal Corporation Faridabad. </p></td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="2"><p >It is to inform that following decision has been taken in consideration 
        of your application: </p>';
        if ($data['status'] == 'approved') {
            $html .= '<ol>';
            $html .= '<li>Your application for New media is approved for erection/ display of Outdoor media Device 
            from ' . $data['start'] . ' to ' . $data['end'] . ' of ' . $data['area'] . ' sq. m. at ' . $data['location'] . '(location) of 
            the Municipal Corporation Faridabad. </li>';
            $html .= '<li>You are hereby directed to deposit the fees of Rs. ' . $data['fees'] . '. within 
            15 days of issuance of this letter. </li>';
            $html .= '<li>The unique ID allotted to new OMD is ' . $data['omd_id'] . '</li>';
            $html .= '</ol>';
        }
        $html .= '</td>';
        $html .= '</tr>';
        if ($data['status'] != 'approved') {
            $html .= '<tr>';
            $html .= '<td colspan="2"> <p style="margin:0px;">Your application for new Media/ renewal 
            is rejected on account of the following: </p> </td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td colspan="2">';
            $html .= '<ul style="margin-top:0px;">';
            $html .= '<li>' . $data['reason'] . '</li>';
            $html .= '</ul>';
            $html .= '</td>';
            $html .= '</tr>';
        }
        $html .= '<tr>';
        $html .= '<td colspan="2">';
        $html .= '<p>Thanking you. </p>';
        $html .= '<p>Commissioner, <br>Municipal Corporation Faridabad</p></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</body>';
        $html .= '</html>';
        return $html;
    }

    public function generateLoiLeter($data) {
        set_time_limit(120);
        $html = $this->__LOIHTML($data);
        $options = new Options();
        $options->set('isHtml5ParserEnabled', true);
        $dompdf = new Dompdf($options);
        $dompdf->load_html($html);
        $dompdf->set_paper("a4", "portrait");
        $dompdf->render();
        file_put_contents(WWW_ROOT . 'uploads/omd_letters/omd_letter_' . $data['id'] . '_' . $data['user_id'] . '.pdf', $dompdf->output());
//        file_put_contents(WWW_ROOT . 'uploads/demo/demo.pdf', $dompdf->output());
    }

    private function __LOIHTML($data) {
//        pr($data);
//        die;
        $html = '<!doctype html>';
        $html .= '<html>';

        $html .= '<head>';
        $html .= '<meta charset="utf-8">';
        $html .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
        $html .= '<meta name="viewport" content="width=device-width, initial-scale=1">';
        $html .= '<title>SMC</title>';
        $html .= '<style>';
        $html .= '@page { margin: 100px 25px; } header { position: fixed; top: -90px; left: 0px; right: 0px; height: 50px; } footer { position: fixed; bottom: -60px; left: 0px; right: 0px; height: 50px; } .pdf-wraper { page-break-after: always; } .pdf-wraper:last-child { page-break-after: never; }.pdf-wraper{padding: 20px;}.pdf-content { padding: 30px 0;}.padright{padding-right: 5%;}.frmfld9ii > p, .frmfld13 > p{display: inline-block; text-align: left;} .frmfld14{ border-top:3px solid #ccc; text-align: center;padding: 10px 0;}.addrighttxt{ display: inline-block; text-align: center; padding: 0 15px;}.pdf-content {padding: 30px 0;} .clearfix {clear:both !important;} .col-sm-2 {width: 16.66666667%;} .col-sm-10 {width: 83.33333333%;} .text-right { text-align: right;} table, th, td {border: 1px solid black;border-collapse: collapse;}th, td {padding: 15px;}';
        $html .= '</style>';

        $html .= '</head>';

        $html .= '<body>';
        $html .= '<header style="padding-bottom:30px;"><img src="' . WWW_ROOT . 'img/faridabad-logo.jpg" /></header>';
        $html .= '<footer>';
        $html .= '<hr>';
       // $html .= '<p style="text-align:center;"># C-1, Tower- B, 3rd Floor, Infocity, Sector-34, Faridabad – 121102, Haryana, India</p>';
        $html .= '<p style="text-align:center;">Website: mcfaridabad.org &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Phone: 0129-2411664 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E-mail: eeadvt@mcf.gov.in</p>';

        $html .= '</div>';
        $html .= '</footer>';
        $html .= '<main>';
        $html .= '<div class="pdf-wraper">';
        $html .= '<div class = "pdf-content">';
        $html .= '<div class = "clearfix" style="clear:both;">';
        $html .= '<div class = "frmfld2i" style="width:100%;">';
        $html .= '<p>From<br/>';
        $html .= 'Commissioner, <br>';
        $html .= 'Municipal Corporation <br>';
        $html .= 'Faridabad</p>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class = "clearfix frmfld4">';
        $html .= '<div class = "col-sm-12 frmfld4i" style="width:80%;float:left;">';
        $html .= '<p>To<br>';
        $html .= 'M/s ' . $data['to'] . '<br>';
        $html .= str_replace(',', ',<br>', $data['address']);
        $html .= '</p>';
        $html .= '<p><b class = "padright">No. ' . $data['app_no'] . '</b> <b>Dated: ' . $data['date'] . '</b></p>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class = "clearfix frmfld5">';
        $html .= '<div class = "col-sm-2 frmfld5i" style="width:100%; float:left;">';
        $html .= '<p><b>Sub: - Letter of Intent for Display of OMD.</b></p>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class = "clearfix frmfld6">';
        $html .= '<div class = "col-sm-12 frmfld6i" style="width:100%;float:left;">';
        $html .= '<p>1. &nbsp;&nbsp; In reference to your online application for permission dated ' . $data['start'] . ', it has been decided to provisionally issue letter of intent for display of OMD(s) at the site(s) specified in this letter. </p>';
        $html .= '<p>Accordingly, license fee is leviable under section 122 of the Haryana Municipal Corporation Act, 1994, read with byelaw 16 of Haryana Municipal Corporation Advertisement Byelaws, 2018, as amended from time to time & notification dated 21.05.2018 on the license fees. You are, therefore, requested to deposit an amount of <b> Rs. ' . $data['fees'] . '</b>. in terms of byelaw 16(1) of the Haryana Municipal Corporation Advertisement Byelaws, 2018, as amended from time to time, within seven days through D.D in favor of “Commissioner, Municipal Corporation Faridabad or via online payment gateway on http://adv.mcfaridabad.org/. Details of license fees is as under:-</p>';
        $html .= '<div class="pdf-table">';
        $html .= '<table class="table table-bordered">';
        $html .= '<thead>';
        $html .= '<tr><th>Sr. No</th> <th>Category/ Location of advertisements</th><th>Permission fees</th><th>Amount</th>';
        $html .= '</tr> </thead>';
        $html .= '<tbody>';
        $html .= '<tr><td>1</td> <td>' . $data['typology'] . '</td> <td>' . $data['fees'] . '</td><td>' . $data['fees'] . '</td></tr>';
        $html .= '<tr><td colspan="3" style="text-align:right;">License fees to be paid in advance for one year</td><td>Rs ' . $data['fees'] . '</td></tr>';
        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '</div>';
        $html .= '</div>';

        $html .= '<div class="clearfix frmfld7">';

        $html .= '<div class="col-sm-12 frmfld7i" style="width:100%;float:left;">';
        $html .= '<p>1. &nbsp;&nbsp; Permission letter for display of OMDs will be issued subject to the following conditions:</p>';
        $html .= '<p>(a) &nbsp;&nbsp; Payment of the license fees within prescribed time limit of seven days from the issue of this letter.  </p>';
        $html .= '<p>(b) &nbsp;&nbsp; Submission of Bank Guarantee of an amount equal to the quarterly license fees i.e. Rs. ' . $data['fees'] . ' within 30 days, payable to MCF valid for the entire license period in terms of byelaw 16 (3) of the Haryana Municipal Corporation Advertisement Byelaws, 2018, as amended from time to time. </p>';
        $html .= '<p>(c) &nbsp;&nbsp; Submission of undertaking within 30 days that you will comply with Haryana Municipal Corporation Act, 1994 and the Haryana Municipal Corporation Advertisement Byelaws, 2018, as amended from time to time. </p>';
        $html .= '<p>(d) &nbsp;&nbsp; Execution of tripartite agreement within 30 days between you, the owner(s) of site(s) approved for the display of advertisements & the Municipal Corporation, Faridabad in terms of the Annexure 6 of the Haryana Municipal Corporation Advertisement Byelaws, 2018 as amended from time to time. </p>';
        $html .= '<p>(e) &nbsp;&nbsp; Submission of a public liability insurance policy within 30 days, in the joint names of yourself and MCF, for respective rights, interests and liabilities to the third parties in respect of accidental death, bodily injury to persons or accidental damage to the property, as per byelaw 24(1) of the Haryana Municipal Corporation Advertisement Byelaws, 2018.</p>';
        $html .= '<p>(f) &nbsp;&nbsp; Submission of an undertaking within 30 days indemnifying MCF as per byelaw 25 of the Haryana Municipal Corporation Advertisement Byelaws, 2018, as amended from time to time</p>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="clearfix frmfld8" style="clear:both;">';

        $html .= '<div class="col-sm-12 frmfld8i" style="width:100%;float:left;">';
        $html .= '<p>3. &nbsp;&nbsp; In case of non compliance of aforesaid conditions, this letter of intent may be deemed to be withdrawn without any further notice.</p>';
        $html .= '<p>On the compliance of the aforesaid conditions, permission letter for display of OMD(s) of the specified site(s) would be issued.</p>';
        $html .= '<p> Executive Engineer-Advertisement,<br>For Commissioner,<br>Municipal Corporation,<br>Faridabad<br>Dated </p>';
        $html .= '</div>';
        $html .= '</div>';



        $html .= '<div class="frmfld10" style="clear:both;">';
        $html .= '<p>Endst No. ' . $data['app_no'] . '</p>';
        $html .= '</div>';
        $html .= '<div class="col-sm-12 frmfld11" style="clear:both;">';
        $html .= '<p>A copy of the above is forwarded to the following for information & necessary action please.</p>';
        $html .= '</div>';
        $html .= '<div class="col-sm-12 frmfld12" style="">';
        $html .= '<p>1.&nbsp;&nbsp;Commissioner, Municipal Corporation, Faridabad.</p>';
        $html .= '<p>2.&nbsp;&nbsp;Additional Commissioner, Municipal Corporation, Faridabad.</p>';
        $html .= '<p>3.&nbsp;&nbsp;Executive Engineer (Advt.), Municipal Corporation, Faridabad.</p>';
        $html .= '<p>4.&nbsp;&nbsp;Assistant Engineer-(Advt), Municipal Corporation, Faridabad with the direction to ensure that no advertisement may be displayed until commencement of period that will be mentioned in permission letter.</p>';
        $html .= '<p>5.&nbsp;&nbsp;CFC, Municipal Corporation, Faridabad.</p>';
        $html .= '</div>';
        $html .= '<div class="col-sm-12 frmfld13 text-right" style="">';
        $html .= '<p> Executive Engineer-Advertisement,<br>For Commissioner,<br>Municipal Corporation,<br>Faridabad </p>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</main>';
        $html .= '</body>';
        $html .= '</html>';
        return $html;
    }

    public function generatePermissionLeter($data) {
        set_time_limit(120);
        $html = $this->__permissionLeterPermission($data);
        $options = new Options();
        $options->set('isHtml5ParserEnabled', true);
        $dompdf = new Dompdf($options);
        $dompdf->load_html($html);
        $dompdf->set_paper("a4", "portrait");
        $dompdf->render();
        file_put_contents(WWW_ROOT . 'uploads/permission_docs/permission_letter_' . $data['id'] . '_' . $data['user_id'] . '.pdf', $dompdf->output());
        return true;
    }

    private function __permissionLeterPermission($data) {
        $fields = $data['fields'];
        $fieldsData = $data['fieldsData'];
        $fieldsTableData = array_values(array_filter($fieldsData, function($arr) {
                    return !in_array($arr['type'], ['file', 'json']);
                }));
        $tableFields = array_values(array_filter($fieldsData, function($arr) {
                    return $arr['type'] == 'json';
                }));
        $tableFieldsKeys = array_map(function($arr) {
            return $arr['field_key'];
        }, $tableFields);
        $tables = array_values(array_filter($fields, function($arr) use($tableFieldsKeys) {
                    return in_array($arr['field'], $tableFieldsKeys);
                }));
        $html = '<!doctype html>';
        $html .= '<html>';

        $html .= '<head>';
        $html .= '<meta charset="utf-8">';
        $html .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
        $html .= '<meta name="viewport" content="width=device-width, initial-scale=1">';
        $html .= '<title>SMC</title>';
        $html .= '<style>';
        $html .= '@page { margin: 100px 25px; } header { position: fixed; top: -90px; left: 0px; right: 0px; height: 50px; } footer { position: fixed; bottom: -60px; left: 0px; right: 0px; height: 50px; } .pdf-wraper { page-break-after: always; } .pdf-wraper:last-child { page-break-after: never; }.pdf-wraper{padding: 20px;}.pdf-content { padding: 30px 0;}.padright{padding-right: 5%;}.frmfld9ii > p, .frmfld13 > p{display: inline-block; text-align: left;} .frmfld14{ border-top:3px solid #ccc; text-align: center;padding: 10px 0;}.addrighttxt{ display: inline-block; text-align: center; padding: 0 15px;}.pdf-content {padding: 30px 0;} .clearfix {clear:both !important;} .col-sm-2 {width: 16.66666667%;} .col-sm-10 {width: 83.33333333%;} .text-right { text-align: right;} table, th, td {border: 1px solid black;border-collapse: collapse;}th, td {padding: 15px;}';
        $html .= '</style>';

        $html .= '</head>';

        $html .= '<body>';
        $html .= '<header style="padding-bottom:30px;"><img src="' . WWW_ROOT . 'img/faridabad-logo.jpg" /></header>';
        $html .= '<footer>';
        $html .= '<hr>';
        //$html .= '<p style="text-align:center;"># C-1, Tower- B, 3rd Floor, Infocity, Sector-34, Faridabad – 122004, Haryana, India</p>';
        $html .= '<p style="text-align:center;">Website: mcfaridabad.org &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Phone: 0124-2212031 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E-mail: eeadvt@mcf.gov.in</p>';

        $html .= '</div>';
        $html .= '</footer>';
        $html .= '<main>';
        $html .= '<div class="pdf-wraper">';
        $html .= '<div class = "pdf-content">';
        $html .= '<div class = "clearfix" style="clear:both;">';
        $html .= '<div class = "frmfld2i" style="width:100%;">';
        $html .= '<p>From<br/>';
        $html .= 'Commissioner, <br>';
        $html .= 'Municipal Corporation <br>';
        $html .= 'Faridabad</p>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class = "clearfix frmfld4">';
        $html .= '<div class = "col-sm-12 frmfld4i" style="width:80%;float:left;">';
        $html .= '<p>To<br>';
        $html .= 'M/s ' . $data['to'] . '<br>';
        $html .= str_replace(',', ',<br>', $data['address']);
        $html .= '</p>';
        $html .= '<p><b class = "padright">No. ' . $data['app_no'] . '</b> <b>Dated: ' . $data['date'] . '</b></p>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class = "clearfix frmfld5">';
        $html .= '<div class = "col-sm-2 frmfld5i" style="width:100%; float:left;">';
        $html .= '<p><b>Sub: - Permission for display of Outdoor media Device (OMD) applied online vide application ID ' . $data['omd_id'] . '. on adv.mcf.gov.in by M/s ' . $data['to'] . '.</b></p>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class = "clearfix frmfld6">';
        $html .= '<div class = "col-sm-12 frmfld6i" style="width:100%;float:left;">';
        $html .= '<p>In continuation of the LOI issued vide Memo No.- ' . $data['app_no'] . ' dated ' . $data['approval_date'] . ' and payment of license fees of Rs. ' . $data['fees'] . ' vide receipt no ' . $data['receipt_id'] . ' dated ' . $data['date'] . ',Bank Guarantee of Rs ' . $data['fees'] . ' drawn on ' . $data['payment_mode_name'] . ' (Bank) submitted on ' . $data['date'] . ', permission is hereby granted for the display of OMD from ' . $data['start'] . ' to ' . $data['end'] . ' as per details mentioned below subject to the following conditions:</p>';
        $html .= '<div class="pdf-table">';
        $html .= '<table class="table table-bordered">';
        $html .= '<thead>';
        $html .= '<tr><th>Sr. No</th> <th>Category/ Location of advertisements</th><th>Sub Category</th><th>Permission fees</th><th>Amount</th>';
        $html .= '</tr> </thead>';
        $html .= '<tbody>';
        $html .= '<tr><td>1</td> <td>' . $data['typology'] . '</td><td>' . $data['sub_category'] . '</td><td>' . $data['fees'] . '</td><td>' . $data['fees'] . '</td></tr>';
        $html .= '<tr><td colspan="4" style="text-align:right;">Permission fees to be paid</td><td>Rs ' . $data['fees'] . '</td></tr>';
        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '</div>';
        $srno = 1;
        $html .= '<div class="pdf-table" ><br>';
        $html .= '<table class="table table-bordered" style="width:100%;float:left;">';
        $html .= '<tbody>';
        foreach ($fieldsTableData as $fieldsTableItem) {
            $field_key = $fieldsTableItem['field_key'];
            $fieldValueArr = array_values(array_filter($fields, function($arr) use($field_key) {
                        return $arr['field'] == $field_key;
                    }));
            $html .= '<tr>';
            $html .= '<th>' . $srno . '</th>';
            $html .= '<th>' . $fieldsTableItem['lable'] . '</th>';
            $html .= '<td>' . $fieldValueArr[0]['value'] . '</td>';
            $html .= '</tr>';
            $srno++;
        }
        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '</div>';
        if (count($tableFields)) {
            foreach ($tableFields as $tableField) {
                $tableKey = $tableField['field_key'];
                $tableArr = array_map(function($arr){
                    $arr['value'] = stripcslashes($arr['value']);
                    return $arr;
                },array_values(array_filter($tables, function($arr) use($tableKey) {
                            return $arr['field'] == $tableKey;
                        })));
                if ($data['category_id'] == 10) {
                    $tableValues = json_decode($tableArr[0]['value'], true);
                    $labelArr = array_values(array_filter($fieldsData,function($arr) use($tableKey){
                        return $arr['field_key'] == $tableKey;
                    }));
                    $html .= '<div class="pdf-table clearfix" style="width:100%;"><br>';
                    $html .= '<h2>'.$labelArr[0]['lable'].'</h2><br>';
                    $html .= '<table class="table table-bordered">';
                    $html .= '<thead>';
                    $html .= '<tr>';
                    $html .= '<th>S. No.</th>';
                    $html .= '<th>OMD Site</th>';
                    $html .= '<th>Height(in m)</th>';
                    $html .= '<th>Width(in m)</th>';
                    $html .= '<th>Area(in sq. m)</th>';
                    $html .= '<th>Status</th>';
                    $html .= '</tr>';
                    $html .= '</thead>';
                    $html .= '<tbody>';
                    $srno = 1;
                    foreach ($tableValues['data'] as $tableValuesArr) {
                        $html .= '<tr>';
                        $html .= '<td>'.$srno.'</td>';
                        $html .= '<td>'.$tableValuesArr['type'].'</td>';
                        $html .= '<td>'.$tableValuesArr['height'].'</td>';
                        $html .= '<td>'.$tableValuesArr['width'].'</td>';
                        $html .= '<td>'.$tableValuesArr['area'].'</td>';
                        $html .= '<td>'.($tableValuesArr['action'] == 1 ? 'Approved' : ($tableValuesArr['action'] == 3 ? 'Rejected' : 'Under Process')).'</td>';
                        $html .= '</tr>';
                        $srno++;
                    }
                    $html .= '</tbody>';
                    $html .= '<tfoot>';
                    $html .= '<tr>';
//                    $html .= '<td colspan="6" style="text-align: right;">Permission Fees(in Rs.):'.$tableValues['licence_fees'].'</td>';
                    $html .= '</tr>';
                    $html .= '<tr>';
                    $html .= '<td colspan="6" style="text-align: right;">Total Area(in sq. m):'.$tableValues['area'].'</td>';
                    $html .= '</tr>';
                    $html .= '<tr>';
                    $html .= '<td colspan="6" style="text-align: right;">Total Permission Fees(in Rs.):'.$tableValues['annual_fees'].'</td>';
                    $html .= '</tr>';
                    $html .= '</tfoot>';
                    $html .= '</table>';
                    $html .= '</div>';
                } elseif($data['sub_category_id'] == 17) {
                    $tableValues = json_decode($tableArr[0]['value'], true);
                    if(trim($tableValues['metro_mrts_categories']) == 'Inside Station') {
                        $html .= '<div class="pdf-table clearfix" style="width:100%;"><br>';
                        $html .= '<h2>'.$tableValues['metro_mrts_categories'].'</h2><br>';
                        $html .= '<table class="table table-bordered">';
                        $html .= '<thead>';
                        $html .= '<tr>';
                        $html .= '<th>S.no</th>';
                        $html .= '<th>OMD Site</th>';
                        $html .= '<th>Height(in m)</th>';
                        $html .= '<th>Width(in m)</th>';
                        $html .= '<th>Area(in sq. m)</th>';
                        $html .= '</tr>';
                        $html .= '</thead>';
                        $html .= '<tbody>';
                        $srno = 1;
                        foreach ($tableValues['data'] as $tableValuesMtr) {
                            $html .= '<tr>';
                            $html .= '<td>'.$srno.'</td>';
                            $html .= '<td>'.$tableValuesMtr['omd_type'].'</td>';
                            $html .= '<td>'.$tableValuesMtr['omd_height'].'</td>';
                            $html .= '<td>'.$tableValuesMtr['omd_width'].'</td>';
                            $html .= '<td>'.$tableValuesMtr['omd_area'].'</td>';
                            $html .= '</tr>';
                            $srno++;
                        }
                        $html .= '</tbody>';
                        $html .= '<tfoot>';
                        $html .= '<tr>';
                        $html .= '<td colspan="5">Total Area(in sq. m) : '.$tableValues['total_area'].'</td>';
                        $html .= '</tr>';
                        $html .= '</tfoot>';
                        $html .= '</table>';
                        $html .= '</div>';
                    } else {
                        foreach ($tableValues['data'] as $tableValueMtr) {
                            $html .= '<div class="pdf-table clearfix" style="width:100%;"><br>';
                            $html .= '<h2>'.$tableValueMtr['metro_type'].'</h2><br>';
                            $html .= '<table class="table table-bordered">';
                            $html .= '<thead>';
                            $html .= '<tr>';
                            $html .= '<th>S. No</th>';
                            $html .= '<th>Train no.</th>';
                            $html .= '<th>No. Of coaches</th>';
                            if(isset($tableValueMtr['table'][0]['ad_panels'])) {
                                $html .= '<th>No. of coaches for display of advertisement</th>';
                            }
                            $html .= '</tr>';
                            $html .= '</thead>';
                            $html .= '<tbody>';
                            $srno = 1;
                            foreach ($tableValueMtr['table'] as $tableValueMtrItem) {
                                $html .= '<tr>';
                                $html .= '<td>'.$srno.'</td>';
                                $html .= '<td>'.$tableValueMtrItem['train_no'].'</td>';
                                $html .= '<td>'.$tableValueMtrItem['coches'].'</td>';
                                if(isset($tableValueMtrItem['ad_panels'])) {
                                    $html .= '<td>'.$tableValueMtrItem['ad_panels'].'</td>';
                                }
                                $html .= '</tr>';
                                $srno++;
                            }
                            $html .= '</tbody>';
                            $html .= '</table>';
                            $html .= '</div>';
                        }
                    }
                } else if($data['sub_category_id'] == 32) {
                    $otherLabelArr = array_values(array_filter($data['fields'],function($arr){
                        return $arr['field'] == 'omd_type_type_a';
                    }));
                    $otherLabel = $otherLabelArr[0]['value'];
                    $html .= '<div class="pdf-table clearfix" style="width:100%;"><br>';
                    $html .= '<table class="table table-bordered">';
                    $srno = 1;
                    $html .= '<thead>';
                    $html .= '<tr>';
                    $html .= '<th>S. No.</th>';
                    $html .= '<th>Type of Asset</th>';
                    $html .= '<th>Location</th>';
                    $html .= '<th>Height (in m)</th>';
                    $html .= '<th>Width (in m)</th>';
                    $html .= '<th>Total Area(in sq m)</th>';
                    $html .= '<th>Latitude</th>';
                    $html .= '<th>Longitude</th>';
                    $html .= '</tr>';
                    $html .= '</thead>';
                    $html .= '<tbody>';
                    $tableValues = json_decode($tableArr[0]['value'], true);
                    foreach ($tableValues as $item) {
                        $html .= '<tr>';
                        $html .= '<td>'.$srno.'</td>';
                        $html .= '<td>'.$otherLabel.'</td>';
                        $html .= '<td>'.$item['location'].'</td>';
                        $html .= '<td>'.$item['height'].'</td>';
                        $html .= '<td>'.$item['width'].'</td>';
                        $html .= '<td>'.$item['area'].'</td>';
                        $html .= '<td>'.$item['lat'].'</td>';
                        $html .= '<td>'.$item['lng'].'</td>';
                        $html .= '</tr>';
                        $srno++;
                    }
                    $html .= '</tbody>';
                    $html .= '</table>';
                    $html .= '</div>';
                } else {
                    $html .= '<div class="pdf-table clearfix" style="width:100%;"><br>';
                    $html .= '<table class="table table-bordered">';
                    $tableValues = json_decode($tableArr[0]['value'], true);
                    $srno = 1;
                    $html .= '<thead>';
                    $html .= '<tr>';
                    $html .= '<th>S. No.</th>';
                    $html .= '<th>Location</th>';
                    $html .= '<th>' . (isset($tableValues[0]['panels']) ? 'No. of Panels' : 'Number of Devices') . '</th>';
                    $html .= '<th>Total Area(in sq m)</th>';
                    $html .= '<th>Latitude</th>';
                    $html .= '<th>Longitude</th>';
                    $html .= '</tr>';
                    $html .= '</thead>';
                    $html .= '<tbody>';
                    foreach ($tableValues as $tableValue) {
                        $html .= '<tr>';
                        $html .= '<td>' . $srno . '</td>';
                        $html .= '<td>' . $tableValue['location'] . '</td>';
                        $html .= '<td>' . (isset($tableValue['panels']) ? $tableValue['panels'] : $tableValue['device']) . '</td>';
                        $html .= '<td>' . $tableValue['area'] . '</td>';
                        $html .= '<td>' . $tableValue['lat'] . '</td>';
                        $html .= '<td>' . $tableValue['lng'] . '</td>';
                        $html .= '</tr>';
                        $srno++;
                    }
                    $html .= '</tbody>';
                    $html .= '</table>';
                    $html .= '</div>';
                }
            }
        }
        $html .= '</div>';

        $html .= '<div class="clearfix frmfld7">';

        $html .= '<div class="col-sm-12 frmfld7i" style="width:100%;float:left;">';
        $html .= '<p>a.) &nbsp;That the agency shall comply with the provisions of Haryana Municipal Corporation Act, 1994 and Haryana Municipal Corporation Advertisement Byelaws, 2018, as amended from time to time.</p>';
        $html .= '<p>b.) &nbsp;That the OMD(s) shall comply with the approved size and other permissible limits as contained in Schedule 1 of the Haryana Municipal Corporation Advertisement Bye-laws 2018, as amended from time to time.</p>';
        $html .= '<p>c.) &nbsp;That the period of license to display the OMD(s) at the specified sites shall commence from ' . $data['start'] . ' to ' . $data['end'] . '</p>';
        $html .= '<p>d.) &nbsp;That the license fee for each year shall be deposited 30 days prior to the lapse of permission fees paid, failing which the permission is liable to be cancelled forthwith.</p>';
        $html .= '<p>e.) &nbsp;That the display can immediately be removed by Municipal Corporation, Faridabad, without any notice, if any violation or contravention of the provisions of the Haryana Municipal Corporation Advertisement Byelaws, 2018, as amended from time to time, is found at any time.</p>';
        $html .= '<p>f.) &nbsp;That without prejudice to any action for removal of display by MCF, action can be also taken by MCF in terms of the provisions of byelaw 22 of the Haryana Municipal Corporation Advertisement Byelaws, 2018 including liability to fine / penalty, debarring of the property for display of advertisement, declaration of all sites unauthorized, blacklisting of owners of property &amp; directors for a period of 3 years.</p>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="clearfix frmfld8" style="clear:both;">';

        $html .= '<div class="col-sm-12 frmfld8i" style="width:100%;float:left;">';
        $html .= '<p>3. &nbsp;&nbsp; In case of non compliance of aforesaid conditions, this letter of intent may be deemed to be withdrawn without any further notice.</p>';
        $html .= '<p>On the compliance of the aforesaid conditions, permission letter for display of OMD(s) of the specified site(s) would be issued.</p>';
        $html .= '<p> Executive Engineer-Advertisement,<br>For Commissioner,<br>Municipal Corporation,<br>Faridabad<br>Dated </p>';
        $html .= '</div>';
        $html .= '</div>';



        $html .= '<div class="frmfld10" style="clear:both;">';
        $html .= '<p>Endst No. ' . $data['app_no'] . '</p>';
        $html .= '</div>';
        $html .= '<div class="col-sm-12 frmfld11" style="clear:both;">';
        $html .= '<p>A copy of the above is forwarded to the following for information & necessary action please.</p>';
        $html .= '</div>';
        $html .= '<div class="col-sm-12 frmfld12" style="">';
        $html .= '<p>1.&nbsp;&nbsp;Commissioner, Municipal Corporation, Faridabad.</p>';
        $html .= '<p>2.&nbsp;&nbsp;Additional Commissioner, Municipal Corporation, Faridabad.</p>';
        $html .= '<p>3.&nbsp;&nbsp;Executive Engineer (Advt.), Municipal Corporation, Faridabad.</p>';
        $html .= '<p>4.&nbsp;&nbsp;Assistant Engineer-(Advt), Municipal Corporation, Faridabad with the direction to ensure that no advertisement may be displayed until commencement of period that will be mentioned in permission letter.</p>';
        $html .= '<p>5.&nbsp;&nbsp;Owner of the property.</p>';
        $html .= '<p>6.&nbsp;&nbsp;CFC, Municipal Corporation, Faridabad.</p>';
        $html .= '</div>';
        $html .= '<div class="col-sm-12 frmfld13 text-right" style="">';
        $html .= '<p> Executive Engineer-Advertisement,<br>For Commissioner,<br>Municipal Corporation,<br>Faridabad </p>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</main>';
        $html .= '</body>';
        $html .= '</html>';
        return $html;
    }

}
