<?php

namespace App\Controller\Component;

use Cake\Controller\Component;

class BilldeskComponent extends Component {

    private $merchant_code = "SMC";
    private $security_id = "smc";
    private $checksum = "1OOLPJlpNo3o";
    // ORDER NAME PER FIX [PROVIDED BY SMC] //
    private $code = "SMCBSB";
    // RETURN URL //
    private $liveUrl = "https://www.suratmunicipal.gov.in/epay/PrepareRequestDIMS.aspx";
    private $return_url = "https://www.suratmunicipal.gov.in/epay/ResponseRequest_DIMS.aspx";
    private $pageMode = 2;
    private $gatewayStr = null;
    private $paymentData = [];

    private function prepairPaymentStr() {
        $productinfo = $this->code . "" . $this->paymentData['uniqueId'];
        $str = $this->merchant_code . "|" . $this->paymentData['uniqueId'] . "|NA|" . $this->paymentData['amount'] . "|NA|NA|NA|INR|NA|R|" . $this->security_id . "|NA|NA|F|" . $productinfo . "|" . $this->code . "|" . $this->paymentData['type'] . "|NA|NA|NA|NA|" . $this->return_url;
        $checksum = crc32($str . "|" . $this->checksum);
        $checksum = sprintf("%u", $checksum);
        $this->gatewayStr = $str . '|' . $checksum;
    }

    public function setAmount($amount) {
        $amount = (int) $amount;
        $this->paymentData['amount'] = number_format($amount, 2, ".", "");
    }

    public function setUniqueId($uid) {
        $this->paymentData['uniqueId'] = !empty($uid) ? $uid : time();
    }

    public function getPaymentStr() {
        $this->prepairPaymentStr();
        return $this->gatewayStr;
    }

    public function setTransactionType($type) {
        $this->paymentData['type'] = $type;
    }

    public function getLiveUrl() {
        return $this->liveUrl;
    }
    
    public function getPageMode() {
        return $this->pageMode;
    }

}
