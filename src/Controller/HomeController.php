<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Description of HomeController
 *
 * @author Signity Solutions Pvt. Ltd.
 */
class HomeController extends AppController {

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow();
    }

    public function beforeRender(Event $event) {
        parent::beforeRender($event);
    }

    public function index() {
//        $this->loadComponent('Pdf');
//        $pdfData = [
//            'amount' => 100,
//            'id' => 1000,
//            'user_id' => 1000,
//            'path' => 'demo',
//            'r_no' => 535
//        ];
//        $this->Pdf->generateReceipt($pdfData);
    }

}
