<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Event\Event;
use App\Traits\Users;
use App\Traits\Devices;
use Cake\Mailer\MailerAwareTrait;
use Cake\Datasource\ConnectionManager;

class McgController extends AppController {

    use Users,
        MailerAwareTrait,
        Devices;

    public $paginate = [
        'limit' => 10
    ];

    public function initialize() {
        parent::initialize();
        // Set the layout
        $this->viewBuilder()->layout('dashboard');
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $userType = strtolower(preg_replace('/[0-9]+/', '', $this->Auth->user('type')));
        if ($userType != 'mcg') {
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }
    }

    public function index() {
        $status = trim($this->request->query('status'));
        $status = ($status == 'pending' ? 'under process' : $status);
        $comapnyDevicesTable = TableRegistry::get('companyDevices');
        $devicesQuery = $comapnyDevicesTable->find('all')->select(['companyDevices_id' => 'companyDevices.id', 'companyDevices_status' => 'companyDevices.status', 'companyDeviceFields_fieldData' => 'companyDeviceFields.fieldData'])->join([
            'table' => "(SELECT `company_device_id`, GROUP_CONCAT(CONCAT(`field_key`,'=',`input_value`) SEPARATOR '#@!') AS fieldData FROM `company_device_fields` GROUP BY `company_device_id`)",
            'alias' => 'companyDeviceFields',
            'type' => 'LEFT',
            'conditions' => 'companyDevices.id = companyDeviceFields.company_device_id',
        ]);
        $where['companyDevices.paid'] = '1';
        if ($status) {
            $where['companyDevices.status'] = $status;
        }
        $devicesQuery = $devicesQuery->where($where);

        $devicesQuery = $devicesQuery->hydrate(false);
        $devices = $this->paginate($devicesQuery)->toArray();

        $latLngArray = [];

        foreach ($devices as $deviceKey => $device) {
            $tempDevice = explode('#@!', $device['companyDeviceFields_fieldData']);

            foreach ($tempDevice as $singleTempDevice) {
                $deviceFields = explode('=', $singleTempDevice);
                if (in_array($deviceFields[0], ['lat', 'lng', 'location', 'concession_fees'])) {
                    $devices[$deviceKey]['companyDevice_' . $deviceFields[0]] = $deviceFields[1];
                }
            }

            if (isset($devices[$deviceKey]['companyDevice_lat']) && isset($devices[$deviceKey]['companyDevice_lat'])) {
                $latLngArray[] = array('lat' => $devices[$deviceKey]['companyDevice_lat'], 'lng' => $devices[$deviceKey]['companyDevice_lng']);
            }
        }
        $latLngArray = addslashes(json_encode($latLngArray));

        /// Set paginated sorKey and Order Icon Name
        $iconName = "bottom";
        if (isset($this->Paginator->request->params['paging']['companyDevices'])) {
            $paginationVars = $this->Paginator->request->params['paging']['companyDevices'];
            $iconName = $paginationVars['direction'] == 'desc' ? 'top' : 'bottom';
        }
        $this->set(compact('devices'));
        $this->set(compact('latLngArray'));
        $this->set(compact('iconName'));
    }

    public function pendingDevices() {
        $addressProofesData = TableRegistry::get('AddressProfes');
        $addressProofesArr = $addressProofesData->find('all')->hydrate(false)->toArray();

        $addressProof = [];
        foreach ($addressProofesArr as $addressProofItem) {
            $addressProof[$addressProofItem['id']] = $addressProofItem['name'];
        }
        $search = $this->request->query('s');
        $mcgUserLevel = preg_replace("/[^0-9,.]/", "", $this->user['type']);
        $comments = [];
        $devicesArr = $this->__getDevices('under process', $search);
        $userIds = array_values(array_unique(array_filter(array_map(function($arr) {
                                    return $arr['user_id'];
                                }, $devicesArr))));
        $deviceids = array_map(function($arr) {
            return $arr['id'];
        }, $devicesArr);
        $usersDocuments = $this->_getUsersDocumentsbyIds($userIds);
        $devices = [];
        $fields = [];
        $fieldsKeysArr = [];
        $deviceFields = [];
        $typologyData = [];
        $addresses = [];
        $fieldszoneArr = [];
        foreach ($devicesArr as $key => $devicesItem) {
            $devices[$key] = $devicesItem;
            $deviceDataArr = explode('#@!', $devicesItem['deviceData']);
            foreach ($deviceDataArr as $deviceDataItem) {
                $deviceDataItemArr = explode('*^$', $deviceDataItem);
                $fieldsKeysArr[] = $deviceDataItemArr[0];
                $deviceFields[$devicesItem['id']][] = array(
                    'field' => $deviceDataItemArr[0],
                    'value' => in_array($deviceDataItemArr[0], ['omd_one_roof', 'omd_premises', 'metro_table', 'shelter_data', 'route_markers_data', 'foot_over_data', 'cycle_station_data', 'police_booth_data', 'sitting_bench_data', 'other_type_a_data']) ? json_decode(stripslashes($deviceDataItemArr[1]), true) : $deviceDataItemArr[1]
                );
            }
            $typologyData[$devicesItem['id']] = array(
                'typology_id' => $devicesItem['typology_id'],
                'typology' => $devicesItem['typology'],
                'sub_category_id' => $devicesItem['sub_category_id'],
                'sub_category' => $devicesItem['sub_category'],
            );
            unset($devices[$key]['deviceData']);
            unset($devices[$key]['typology']);
            unset($devices[$key]['sub_category']);
            unset($devices[$key]['sub_category_id']);
        }
        if ($devices) {
            $deviceIds = array_map(function($arr) {
                return $arr['id'];
            }, $devices);
            $mcgCommentsTable = TableRegistry::get('McgComments');
            $mcgCommentsArr = $mcgCommentsTable->find('all')->select(['device_id', 'mcg_level', 'user_name' => 'u.display_name', 'type' => 'u.type', 'comment', 'comment_data', 'status', 'created'])->join([
                        'table' => 'users',
                        'alias' => 'u',
                        'type' => 'LEFT',
                        'conditions' => 'McgComments.user_id = u.id'
                    ])->where(['device_id IN' => $deviceIds])->order(['McgComments.id' => 'DESC'])->hydrate(false);
            $mcgComments = $mcgCommentsArr->toArray();
            array_walk($mcgComments, function($value) use(&$comments) {
                $value['comment'] = trim(preg_replace('/\s\s+/', '<br>', $value['comment']));
                $value['comment_data'] = json_decode($value['comment_data'], true);
                $value['created'] = date('d M, Y h:iA', strtotime($value['created']));
                $comments[$value['device_id']][] = $value;
            });
            $fieldsKeysDataArr = array_merge($fieldsKeysArr, ['reason', 'obstruction_to_road', 'two_omd_distance', 'intersection_distance', 'omd_size_less_75']);
            $fieldsKeys = array_values(array_unique($fieldsKeysDataArr));
            $fieldsTable = TableRegistry::get('Fields');
            $fieldsArr = $fieldsTable->find('all')->select(['field_key', 'type', 'lable'])->where(['field_key IN' => $fieldsKeys])->hydrate(false);
            $fieldsData = $fieldsArr->toArray();
            array_walk($fieldsData, function($value) use(&$fields) {
                $fields[$value['field_key']] = array(
                    'label' => $value['lable'],
                    'type' => $value['type']
                );
            });
            
            //============= 07-12-2018 ===============
            
            $fieldszoneArr = $fieldsTable->find('all')->select(['field_key', 'type', 'lable','options'])->where(['field_key IN' => 'zone'])->hydrate(false)->first();
           
            //======== End of 07-12-2018 =============
            
            $devices = array_map(function($arr) use($comments, $mcgUserLevel) {
                $deviceComment = isset($comments[$arr['id']]) ? $comments[$arr['id']] : array();
                $deviceCommentItem = reset($deviceComment);
                $mcgUnder = 1;
                if ($deviceCommentItem) {
                    if ($deviceCommentItem['status'] == 'Return for Re submission') {
                        $mcgUnder = 2;
                    } else if ($deviceCommentItem['status'] == 'document requested') {
                        $mcgUnder = 2;
                    } else if ($deviceCommentItem['status'] != 'Return') {
                        $mcgUnder = $deviceCommentItem['mcg_level'] + 1;
                    } else {
                        $mcgUnder = $deviceCommentItem['mcg_level'] - 1;
                    }
                }
                $arr['sort_order'] = $mcgUnder;
                if ($mcgUserLevel == $mcgUnder) {
                    $arr['sort_order'] = 0;
                }
                $arr['status'] = $arr['status'] . ' - MCG' . $mcgUnder;
                return $arr;
            }, $devices);
        }
        $deviceData = [];
        array_walk($devices, function($val, $key) use(&$deviceData) {
            $val['annual_turnover'] = number_format($val['annual_turnover']);
            $val['incorporation_date'] = date('d M, Y', strtotime($val['incorporation_date']));
            $deviceData[$val['id']] = $val;
        });

        $OmdAdditionalDocstable = TableRegistry::get('OmdAdditionalDocs');
        $addtionalrequestfiles = [];
        $addtionaldocumentsuploaded = [];
        if ($deviceids) {
            $addtionalrequestfiles = $OmdAdditionalDocstable->find('all')->select(['OmdAdditionalDocs.name', 'OmdAdditionalDocs.reason', 'OmdAdditionalDocs.is_uploaded', 'OmdAdditionalDocs.filename', 'created' => 'omd_additional_doc_requests.created', 'device_id' => 'omd_additional_doc_requests.device_id'])
                            ->join([
                                'omd_additional_doc_requests' => [
                                    'table' => 'omd_additional_doc_requests',
                                    'type' => 'LEFT',
                                    'conditions' => 'OmdAdditionalDocs.request_id = omd_additional_doc_requests.id'
                                ]
                            ])
                            ->where(['omd_additional_doc_requests.device_id IN' => $deviceids])->order(['OmdAdditionalDocs.request_id' => 'DESC'])->hydrate(false)->toArray();
            $addtionalrequestfiles = array_map(function($arr) {
                $ext = pathinfo($arr['filename'], PATHINFO_EXTENSION);
                $arr['filename'] = Router::url('/uploads/omd_additional_device_docs/' . $arr['filename']);
                $arr['ext'] = '.' . $ext;
                $arr['created'] = date('d M, Y h:iA', strtotime($arr['created']));
                return $arr;
            }, $addtionalrequestfiles);
            $addtionaldocumentsuploadedArr = $this->array_group_by($addtionalrequestfiles, 'device_id');
            $addtionaldocumentsuploaded = array_map(function($arr) {
                return $this->array_group_by($arr, 'created');
            }, $addtionaldocumentsuploadedArr);
        }
        
        $deviceImagePath = Router::url('/') . 'uploads/devices/';
        $this->set(compact('devices'));
        $this->set(compact('deviceImagePath'));
        $this->set(compact('mcgUserLevel'));
        $this->set(compact('search'));
        $this->set('fields', addslashes(json_encode($fields)));
        $this->set('fields', addslashes(json_encode($fields)));
        $this->set('fieldszoneArr', addslashes(json_encode($fieldszoneArr)));
		$this->set('deviceData', addslashes(json_encode($deviceData)));
        $this->set('deviceFields', addslashes(json_encode($deviceFields)));
        $this->set('typologyData', addslashes(json_encode($typologyData)));
        $this->set('addresses', addslashes(json_encode($addresses)));
        $this->set('comments', addslashes(json_encode($comments)));
        $this->set('addressProof', addslashes(json_encode($addressProof)));
        $this->set('usersDocuments', addslashes(json_encode($usersDocuments)));
        $this->set('addionaldocs', addslashes(json_encode($addtionaldocumentsuploaded)));
    }

    public function rejectedDevices() {
	
        $addressProofesData = TableRegistry::get('AddressProfes');
        $addressProofesArr = $addressProofesData->find('all')->hydrate(false)->toArray();

        $addressProof = [];
        foreach ($addressProofesArr as $addressProofItem) {
            $addressProof[$addressProofItem['id']] = $addressProofItem['name'];
        }
        $comments = [];
        $search = $this->request->query('s');
        $devicesArr = $this->__getDevices('rejected', $search);
        $userIds = array_values(array_unique(array_filter(array_map(function($arr) {
                                    return $arr['user_id'];
                                }, $devicesArr))));
        $deviceids = array_map(function($arr) {
            return $arr['id'];
        }, $devicesArr);
        $usersDocuments = $this->_getUsersDocumentsbyIds($userIds);
        $devices = [];
        $fields = [];
        $fieldsKeysArr = [];
        $deviceFields = [];
        $typologyData = [];
        $addresses = [];
        foreach ($devicesArr as $key => $devicesItem) {
            $devices[$key] = $devicesItem;
            $deviceDataArr = explode('#@!', $devicesItem['deviceData']);
            foreach ($deviceDataArr as $deviceDataItem) {
                $deviceDataItemArr = explode('*^$', $deviceDataItem);
                $fieldsKeysArr[] = $deviceDataItemArr[0];
                $deviceFields[$devicesItem['id']][] = array(
                    'field' => $deviceDataItemArr[0],
                    'value' => in_array($deviceDataItemArr[0], ['omd_one_roof', 'omd_premises', 'metro_table', 'shelter_data', 'route_markers_data', 'foot_over_data', 'cycle_station_data', 'police_booth_data', 'sitting_bench_data', 'other_type_a_data']) ? json_decode($deviceDataItemArr[1], true) : $deviceDataItemArr[1]
                );
            }
            $typologyData[$devicesItem['id']] = array(
                'typology_id' => $devicesItem['typology_id'],
                'typology' => $devicesItem['typology'],
                'sub_category_id' => $devicesItem['sub_category_id'],
                'sub_category' => $devicesItem['sub_category'],
            );
            unset($devices[$key]['deviceData']);
            unset($devices[$key]['typology']);
            unset($devices[$key]['sub_category']);
            unset($devices[$key]['sub_category_id']);
        }
        if ($devices) {
            $deviceIds = array_map(function($arr) {
                return $arr['id'];
            }, $devices);
            $mcgCommentsTable = TableRegistry::get('McgComments');
            $mcgCommentsArr = $mcgCommentsTable->find('all')->select(['device_id', 'mcg_level', 'user_name' => 'u.display_name', 'type' => 'u.type', 'comment', 'comment_data', 'status', 'created'])->join([
                        'table' => 'users',
                        'alias' => 'u',
                        'type' => 'LEFT',
                        'conditions' => 'McgComments.user_id = u.id'
                    ])->where(['device_id IN' => $deviceIds])->order(['mcg_level' => 'DESC'])->hydrate(false);
            $mcgComments = $mcgCommentsArr->toArray();
            array_walk($mcgComments, function($value) use(&$comments) {
                $value['comment'] = trim(preg_replace('/\s\s+/', '<br>', $value['comment']));
                $value['comment_data'] = json_decode($value['comment_data'], true);
                $value['created'] = date('d M, Y h:iA', strtotime($value['created']));
                $comments[$value['device_id']][] = $value;
            });
            $fieldsKeysDataArr = array_merge($fieldsKeysArr, ['reason', 'obstruction_to_road', 'two_omd_distance', 'intersection_distance', 'omd_size_less_75']);
            $fieldsKeys = array_values(array_unique($fieldsKeysDataArr));
            $fieldsTable = TableRegistry::get('Fields');
            $fieldsArr = $fieldsTable->find('all')->select(['field_key', 'type', 'lable'])->where(['field_key IN' => $fieldsKeys])->hydrate(false);
            $fieldsData = $fieldsArr->toArray();
            array_walk($fieldsData, function($value) use(&$fields) {
                $fields[$value['field_key']] = array(
                    'label' => $value['lable'],
                    'type' => $value['type']
                );
            });
        }
        $deviceData = [];
        array_walk($devices, function($val, $key) use(&$deviceData) {
            $val['annual_turnover'] = number_format($val['annual_turnover']);
            $val['incorporation_date'] = date('d M, Y', strtotime($val['incorporation_date']));
            $deviceData[$val['id']] = $val;
        });
        $OmdAdditionalDocstable = TableRegistry::get('OmdAdditionalDocs');
        $addtionalrequestfiles = [];
        $addtionaldocumentsuploaded = [];
        if ($deviceids) {
            $addtionalrequestfiles = $OmdAdditionalDocstable->find('all')->select(['OmdAdditionalDocs.name', 'OmdAdditionalDocs.reason', 'OmdAdditionalDocs.is_uploaded', 'OmdAdditionalDocs.filename', 'created' => 'omd_additional_doc_requests.created', 'device_id' => 'omd_additional_doc_requests.device_id'])
                            ->join([
                                'omd_additional_doc_requests' => [
                                    'table' => 'omd_additional_doc_requests',
                                    'type' => 'LEFT',
                                    'conditions' => 'OmdAdditionalDocs.request_id = omd_additional_doc_requests.id'
                                ]
                            ])
                            ->where(['omd_additional_doc_requests.device_id IN' => $deviceids])->order(['OmdAdditionalDocs.request_id' => 'DESC'])->hydrate(false)->toArray();
            $addtionalrequestfiles = array_map(function($arr) {
                $ext = pathinfo($arr['filename'], PATHINFO_EXTENSION);
                $arr['filename'] = Router::url('/uploads/omd_additional_device_docs/' . $arr['filename']);
                $arr['ext'] = '.' . $ext;
                $arr['created'] = date('d M, Y h:iA', strtotime($arr['created']));
                return $arr;
            }, $addtionalrequestfiles);
            $addtionaldocumentsuploadedArr = $this->array_group_by($addtionalrequestfiles, 'device_id');
            $addtionaldocumentsuploaded = array_map(function($arr) {
                return $this->array_group_by($arr, 'created');
            }, $addtionaldocumentsuploadedArr);
        }
        
        //============= 07-12-2018 ===============
	
		$fieldsTable1 = TableRegistry::get('Fields');
		$fieldszoneArr = $fieldsTable1->find('all')->select(['field_key', 'type', 'lable','options'])->where(['field_key IN' => 'zone'])->hydrate(false)->first();
		//======== End of 07-12-2018 =============
      
        $deviceImagePath = Router::url('/') . 'uploads/devices/';
        $this->set(compact('devices'));
        $this->set(compact('search'));
        $this->set(compact('deviceImagePath'));
        $this->set('fields', addslashes(json_encode($fields)));
        $this->set('fieldszoneArr', addslashes(json_encode($fieldszoneArr)));
        $this->set('deviceFields', addslashes(json_encode($deviceFields)));
        $this->set('deviceData', addslashes(json_encode($deviceData)));
        $this->set('typologyData', addslashes(json_encode($typologyData)));
        $this->set('addresses', addslashes(json_encode($addresses)));
        $this->set('comments', addslashes(json_encode($comments)));
        $this->set('addressProof', addslashes(json_encode($addressProof)));
        $this->set('usersDocuments', addslashes(json_encode($usersDocuments)));
        $this->set('addionaldocs', addslashes(json_encode($addtionaldocumentsuploaded)));
    }

    public function approvedDevices() {
        $addressProofesData = TableRegistry::get('AddressProfes');
        $addressProofesArr = $addressProofesData->find('all')->hydrate(false)->toArray();

        $addressProof = [];
        foreach ($addressProofesArr as $addressProofItem) {
            $addressProof[$addressProofItem['id']] = $addressProofItem['name'];
        }
        $search = $this->request->query('s');
        $comments = [];
        $devicesArr = $this->__getDevices('approved', $search);
        $userIds = array_values(array_unique(array_filter(array_map(function($arr) {
                                    return $arr['user_id'];
                                }, $devicesArr))));
        $deviceids = array_map(function($arr) {
            return $arr['id'];
        }, $devicesArr);
        $usersDocuments = $this->_getUsersDocumentsbyIds($userIds);
        $devices = [];
        $fields = [];
        $fieldsKeysArr = [];
        $deviceFields = [];
        $typologyData = [];
        $addresses = [];
        foreach ($devicesArr as $key => $devicesItem) {
            $devices[$key] = $devicesItem;
            $deviceDataArr = explode('#@!', $devicesItem['deviceData']);
            foreach ($deviceDataArr as $deviceDataItem) {
                $deviceDataItemArr = explode('*^$', $deviceDataItem);
                $fieldsKeysArr[] = $deviceDataItemArr[0];
                $deviceFields[$devicesItem['id']][] = array(
                    'field' => $deviceDataItemArr[0],
                    'value' => in_array($deviceDataItemArr[0], ['omd_one_roof', 'omd_premises', 'metro_table', 'shelter_data', 'route_markers_data', 'foot_over_data', 'cycle_station_data', 'police_booth_data', 'sitting_bench_data', 'other_type_a_data']) ? json_decode($deviceDataItemArr[1], true) : $deviceDataItemArr[1]
                );
            }
            $typologyData[$devicesItem['id']] = array(
                'typology_id' => $devicesItem['typology_id'],
                'typology' => $devicesItem['typology'],
                'sub_category_id' => $devicesItem['sub_category_id'],
                'sub_category' => $devicesItem['sub_category'],
            );
            unset($devices[$key]['deviceData']);
            unset($devices[$key]['typology']);
            unset($devices[$key]['sub_category_id']);
            unset($devices[$key]['sub_category']);
        }
        if ($devices) {
            $deviceIds = array_map(function($arr) {
                return $arr['id'];
            }, $devices);
            $mcgCommentsTable = TableRegistry::get('McgComments');
            $mcgCommentsArr = $mcgCommentsTable->find('all')->select(['device_id', 'mcg_level', 'user_name' => 'u.display_name', 'type' => 'u.type', 'comment', 'comment_data', 'status', 'created'])->join([
                        'table' => 'users',
                        'alias' => 'u',
                        'type' => 'LEFT',
                        'conditions' => 'McgComments.user_id = u.id'
                    ])->where(['device_id IN' => $deviceIds])->order(['mcg_level' => 'DESC'])->hydrate(false);
            $mcgComments = $mcgCommentsArr->toArray();
            array_walk($mcgComments, function($value) use(&$comments) {
                $value['comment'] = trim(preg_replace('/\s\s+/', '<br>', $value['comment']));
                $value['comment_data'] = json_decode($value['comment_data'], true);
                $value['created'] = date('d M, Y h:iA', strtotime($value['created']));
                $comments[$value['device_id']][] = $value;
            });
            $fieldsKeysDataArr = array_merge($fieldsKeysArr, ['reason', 'obstruction_to_road', 'two_omd_distance', 'intersection_distance', 'omd_size_less_75']);
            $fieldsKeys = array_values(array_unique($fieldsKeysDataArr));
            $fieldsTable = TableRegistry::get('Fields');
            $fieldsArr = $fieldsTable->find('all')->select(['field_key', 'type', 'lable'])->where(['field_key IN' => $fieldsKeys])->hydrate(false);
            $fieldsData = $fieldsArr->toArray();
            array_walk($fieldsData, function($value) use(&$fields) {
                $fields[$value['field_key']] = array(
                    'label' => $value['lable'],
                    'type' => $value['type']
                );
            });
        }
        $deviceData = [];
        array_walk($devices, function($val, $key) use(&$deviceData) {
            $val['annual_turnover'] = number_format($val['annual_turnover']);
            $val['incorporation_date'] = date('d M, Y', strtotime($val['incorporation_date']));
            $deviceData[$val['id']] = $val;
        });

        $OmdAdditionalDocstable = TableRegistry::get('OmdAdditionalDocs');
        $addtionalrequestfiles = [];
        $addtionaldocumentsuploaded = [];
        if ($deviceids) {
            $addtionalrequestfiles = $OmdAdditionalDocstable->find('all')->select(['OmdAdditionalDocs.name', 'OmdAdditionalDocs.reason', 'OmdAdditionalDocs.is_uploaded', 'OmdAdditionalDocs.filename', 'created' => 'omd_additional_doc_requests.created', 'device_id' => 'omd_additional_doc_requests.device_id'])
                            ->join([
                                'omd_additional_doc_requests' => [
                                    'table' => 'omd_additional_doc_requests',
                                    'type' => 'LEFT',
                                    'conditions' => 'OmdAdditionalDocs.request_id = omd_additional_doc_requests.id'
                                ]
                            ])
                            ->where(['omd_additional_doc_requests.device_id IN' => $deviceids])->order(['OmdAdditionalDocs.request_id' => 'DESC'])->hydrate(false)->toArray();
            $addtionalrequestfiles = array_map(function($arr) {
                $ext = pathinfo($arr['filename'], PATHINFO_EXTENSION);
                $arr['filename'] = Router::url('/uploads/omd_additional_device_docs/' . $arr['filename']);
                $arr['ext'] = '.' . $ext;
                $arr['created'] = date('d M, Y h:iA', strtotime($arr['created']));
                return $arr;
            }, $addtionalrequestfiles);
            $addtionaldocumentsuploadedArr = $this->array_group_by($addtionalrequestfiles, 'device_id');
            $addtionaldocumentsuploaded = array_map(function($arr) {
                return $this->array_group_by($arr, 'created');
            }, $addtionaldocumentsuploadedArr);
        }
        
        //============= 07-12-2018 ===============
		$fieldsTable = TableRegistry::get('Fields');
		$fieldszoneArr = $fieldsTable->find('all')->select(['field_key', 'type', 'lable','options'])->where(['field_key IN' => 'zone'])->hydrate(false)->first();
		//======== End of 07-12-2018 =============
        
        $deviceImagePath = Router::url('/') . 'uploads/devices/';
        $this->set(compact('devices'));
        $this->set(compact('search'));
        $this->set(compact('deviceImagePath'));
        $this->set('fields', addslashes(json_encode($fields)));
        $this->set('fieldszoneArr', addslashes(json_encode($fieldszoneArr)));
        $this->set('deviceData', addslashes(json_encode($deviceData)));
        $this->set('deviceFields', addslashes(json_encode($deviceFields)));
        $this->set('typologyData', addslashes(json_encode($typologyData)));
        $this->set('addresses', addslashes(json_encode($addresses)));
        $this->set('comments', addslashes(json_encode($comments)));
        $this->set('addressProof', addslashes(json_encode($addressProof)));
        $this->set('usersDocuments', addslashes(json_encode($usersDocuments)));
        $this->set('addionaldocs', addslashes(json_encode($addtionaldocumentsuploaded)));
    }

    private function __getDevices($status = 'under process', $search = '') {
        $conn = ConnectionManager::get('default');
        $conn->execute('SET group_concat_max_len = 5555555');
        $devicesTable = TableRegistry::get('CompanyDevices');
        $mcgUserLevel = preg_replace("/[^0-9,.]/", "", $this->user['type']);
        $mcgLevelOrderDeviceIdsQuery = $devicesTable->find('all')->select(['id', 'mcgLevel' => 'IF(tbl1.status IS NULL,1, IF(tbl1.status="Forward",tbl1.mcg_level + 1,IF(tbl1.status = "document requested",2,IF(tbl1.status="Return",tbl1.mcg_level - 1,IF(tbl1.status="Return for Re submission",2,tbl1.mcg_level + 1)))))'])->join([
                    'table' => '(SELECT * FROM (SELECT * FROM `mcg_comments` ORDER BY `id` DESC) tbl GROUP BY `device_id`)',
                    'alias' => 'tbl1',
                    'type' => 'LEFT',
                    'conditions' => 'CompanyDevices.id = tbl1.device_id'
                ])->having(['mcgLevel' => $mcgUserLevel])->hydrate(false);
        $mcgLevelOrderDeviceIds = array_map(function($arr) {
            return $arr['id'];
        }, $mcgLevelOrderDeviceIdsQuery->toArray());
      
        $devicesData = $devicesTable->find('all')->select([
            'id',
            'device_name',
            'company_code',
            'status',
            'created',
            'user_id' => 'u.id',
            'company_name' => 'u.display_name',
            'address' => 'c.company_address',
            'director_name' => 'c.director_name',
            'annual_turnover' => 'c.annual_turnover',
            'smc_party_code' => 'c.SMC_party_code',
            'incorporation_date' => 'c.incorporation_date',
            'representative_id' => 'r.id',
            'representative' => 'r.representative',
            'name' => 'u.display_name',
            'phone' => 'u.mobile',
            'email' => 'u.email',
            'incorporation_date' => 'c.incorporation_date',
            'typology_id' => 'pc.id',
            'typology' => 'pc.title',
            'sub_category_id' => 'sc.id',
            'sub_category' => 'sc.title',
            'property_id' => 'c.property_id',
            'authorised_contact_person' => 'c.authorised_contact_person',
            'address_type' => 'c.address_type',
            'address_proof' => 'c.address_proof',
            'deviceData' => 'deviceData.fieldData'
        ]);
        $devicesData->join([
            'u' => [
                'table' => 'users',
                'type' => 'LEFT',
                'conditions' => 'CompanyDevices.user_id = u.id'
            ],
            'c' => [
                'table' => 'user_profiles',
                'type' => 'LEFT',
                'conditions' => 'CompanyDevices.user_id = c.user_profile_id'
            ],
            'r' => [
                'table' => 'representatives',
                'type' => 'LEFT',
                'conditions' => 'u.representative_id = r.id'
            ],
            'pc' => [
                'table' => 'categories',
                'type' => 'LEFT',
                'conditions' => 'CompanyDevices.category_id = pc.id'
            ],
            'sc' => [
                'table' => 'categories',
                'type' => 'LEFT',
                'conditions' => 'CompanyDevices.sub_category_id = sc.id'
            ],
            'deviceData' => [
                'table' => "(SELECT `company_device_id`, GROUP_CONCAT(CONCAT(`field_key`,'*^$',`input_value`) SEPARATOR '#@!') AS fieldData FROM `company_device_fields` GROUP BY `company_device_id`)",
                'type' => 'LEFT',
                'conditions' => 'CompanyDevices.id = deviceData.company_device_id'
            ]
        ]);
        if ($mcgUserLevel == 1) {
            $devicesData->join([
                'fz' => [
                    'table' => '(SELECT * FROM `company_device_fields` WHERE `field_key` = "zone" GROUP BY `company_device_id`)',
                    'type' => 'LEFT',
                    'conditions' => 'CompanyDevices.id = fz.company_device_id'
                ]
            ]);
            $devicesData->where(['fz.input_value' => $this->user['zone']]);
        }
        $devicesData->where(['CompanyDevices.status' => $status, 'CompanyDevices.paid' => '1']);
        if (trim($search)) {
            $devicesData->where([
                'OR' => [
                    ['CompanyDevices.id' => $search],
                    ['u.display_name LIKE' => '%' . $search . '%'],
                    ['c.company_address LIKE' => '%' . $search . '%']
                ]
            ]);
        }
        if ($mcgLevelOrderDeviceIds) {
            $devicesData->order("CompanyDevices.id IN (" . implode(',', $mcgLevelOrderDeviceIds) . ") DESC");
        }
        $devicesData->order(['CompanyDevices.created' => 'DESC']);
        //echo $devicesData->sql();
        $devicesData->hydrate(false);
        return $this->paginate($devicesData)->toArray();
    }

    public function addComment() {
        $this->_ajaxCheck();
        $response['error'] = 0;
        $response['refresh'] = false;
        $commentTable = TableRegistry::get('McgComments');
        $post = $commentTable->newEntity();
        $mcgUserLevel = preg_replace("/[^0-9,.]/", "", $this->user['type']);
      
        if ($this->request->is('post')) {
            if ($this->request->data['status'] == 'Forward' || $this->request->data['status'] == 'Approve') {
                $post = $commentTable->patchEntity($post, $this->request->data);
            } else {
                $post = $commentTable->patchEntity($post, $this->request->data, ['validate' => 'Hold']);
            }
            
            if ($post->errors()) {
                $response['error'] = 1;
                $response['data'] = $post->errors();
            } else {
                $deviceId = $this->request->data['device'];
                $this->__checkCommentStatus($deviceId, $mcgUserLevel, $commentTable);
                $deviceFieldData = [];
                $deviceTable = TableRegistry::get('CompanyDevices');
                $post->device_id = $this->request->data['device'];
                $post->mcg_level = $mcgUserLevel;
                $post->user_id = $this->user['id'];
                if ($this->request->data['status'] == 'Forward' || $this->request->data['status'] == 'Approve') {
                    unset($this->request->data['reasonCheck']);
                    unset($this->request->data['status']);
                    unset($this->request->data['device']);
                    unset($this->request->data['comment']);
                    foreach ($this->request->data as $key => $item) {
                        $deviceFieldData[] = [
                            'field_key' => $key,
                            'input_value' => $item,
                        ];
                    }
                    //if ($mcgUserLevel == 4) {
                    if ($mcgUserLevel == 3) {
                        $tableData = [];
                        $this->loadComponent('Sms');
                        $deviceDetail = $this->_getDeviceData($deviceId);
                        
                        $fees = 0;
                        $area = '--N/A--';
                        $location = '--N/A--';
                        $tenure = 12;
                        $tenureArr = array_values(array_filter($deviceDetail['fields'], function($arr) {
                                    return $arr['field'] == 'tenure';
                                }));
                        if ($tenureArr) {
                            $tenure = $tenureArr[0]['value'];
                        }
                        $totalLicenseFees = array_values(array_filter($deviceDetail['fields'], function($arr) {
                                    return $arr['field'] == 'annual_license_fees';
                                }));
                        if (!$totalLicenseFees) {
                            $totalLicenseFees = array_values(array_filter($deviceDetail['fields'], function($arr) {
                                        return $arr['field'] == 'concession_fees';
                                    }));
                        }
                        if (!$totalLicenseFees) {
                            $totalLicenseFees = array_values(array_filter($deviceDetail['fields'], function($arr) {
                                        return $arr['field'] == 'total_omd_fees';
                                    }));
                        }
                        if (!$totalLicenseFees) {
                            $totalLicenseFees = array_values(array_filter($deviceDetail['fields'], function($arr) {
                                        return $arr['field'] == 'total_permission_fees';
                                    }));
                        }
                        if (count($totalLicenseFees)) {
                            $fees = number_format($totalLicenseFees[0]['value']);
                        }
                       
                        $recurringSubcategoryIds = [34, 23, 35, 27, 29];
                        if (in_array($deviceDetail['sub_category_id'], $recurringSubcategoryIds)) {
                            $installmentData = [];
                            $permissionStartDate = date('Y-m-01', strtotime('+1 month'));
                            $installmentDivission = 3;
                            if ($tenure > 12) {
                                $installmentDivission = 12;
                            }
                            $noOfInstallments = (int) ($tenure / $installmentDivission);
                            $nonPeriodicMonths = ($tenure % $installmentDivission);
                            $installmentAmountPerMonth = floatval(str_replace(array(' ', ','), '', $fees)) / $tenure;
                            for ($i = 1; $i <= $noOfInstallments; $i++) {
                                $endDate = date('Y-m-d', strtotime($permissionStartDate . ' + ' . $installmentDivission . ' months'));
                                $installmentData[] = [
                                    'device_id' => $deviceDetail['id'],
                                    'amount' => $installmentAmountPerMonth * $installmentDivission,
                                    'start_date' => $permissionStartDate,
                                    'end_date' => date('Y-m-d', strtotime($endDate . '- 1 day'))
                                ];
                                $permissionStartDate = $endDate;
                            }
                            if ($nonPeriodicMonths) {
                                $endDate = date('Y-m-d', strtotime($permissionStartDate . ' + ' . $nonPeriodicMonths . ' months'));
                                $installmentData[] = [
                                    'device_id' => $deviceDetail['id'],
                                    'amount' => $installmentAmountPerMonth * $nonPeriodicMonths,
                                    'start_date' => $permissionStartDate,
                                    'end_date' => date('Y-m-d', strtotime($endDate . '- 1 day'))
                                ];
                            }
                            if ($installmentData) {
                                $installmentTable = TableRegistry::get('Installments');
                                $installmentEntities = $installmentTable->newEntities($installmentData);
                                $installmentTable->saveMany($installmentEntities);
                            }
                        }
                        $totalAdvertisingSpace = array_values(array_filter($deviceDetail['fields'], function($arr) {
                                    return $arr['field'] == 'total_advertising_space';
                                }));
                        if (count($totalAdvertisingSpace)) {
                            $area = number_format($totalAdvertisingSpace[0]['value']);
                        }

                        $locationArr = array_values(array_filter($deviceDetail['fields'], function($arr) {
                                    return $arr['field'] == 'location';
                                }));
                        if (count($locationArr)) {
                            $location = $locationArr[0]['value'];
                        }
                        $userId = $deviceDetail['user_id'];
                        $userDetail = $this->_getUserProfileDetail($userId);
                        $this->loadComponent('Pdf');
                        //================== MCG4 APPOVED FREE FEE CONDITION (rohit kaushal) =================================
                        //if ($deviceDetail['sub_category_id'] == 17 || $deviceDetail['sub_category_id'] == 24 ) {
                        if ($deviceDetail['sub_category_id'] == 17 || $deviceDetail['sub_category_id'] == 24 || (($deviceDetail['sub_category_id'] == 26) && ($fees == 0) ) ) {
                            
                            $startDate = date('Y-m-01', strtotime('+ 1 month'));
                            $endDate = date('Y-m-d', strtotime($startDate . ' +' . $tenure . ' months'));
                            $pdfData = [
                                'id' => $deviceDetail['id'],
                                'user_id' => $deviceDetail['user_id'],
                                'status' => 'approved',
                                'date' => date('d M, Y'),
                                'receipt_id' => '--N/A--',
                                'approval_date' => date('d M, Y', strtotime($deviceDetail['modified'])),
                                'to' => $deviceDetail['name'],
                                'address' => $deviceDetail['address'],
                                'file_no' => date('d.H.Y.m.' . $deviceDetail['id'], strtotime($deviceDetail['created'])),
                                'app_no' => 'MCFOMD' . date('Ymd', strtotime($deviceDetail['created'])) . $deviceDetail['id'],
                                'app_date' => date('d M, Y', strtotime($deviceDetail['created'])),
                                'omd_id' => 'MCFOMD' . $deviceDetail['id'] . 'U' . $deviceDetail['user_id'],
                                'payment_mode_name' => '--N/A--',
                                'fees' => $fees,
                                'area' => $area,
                                'location' => $location,
                                'category_id' => $deviceDetail['category_id'],
                                'typology' => $deviceDetail['typology'],
                                'sub_category' => $deviceDetail['sub_category'],
                                'sub_category_id' => $deviceDetail['sub_category_id'],
                                'start' => date('d M, Y', strtotime($startDate)),
                                'end' => date('d M, Y', strtotime($endDate . '-1 day')),
                                'fields' => $deviceDetail['fields'],
                                'fieldsData' => $deviceDetail['fieldsData']
                            ];
                            if ($deviceDetail['sub_category_id'] == 24) {
                                $eventStartDate = array_values(array_filter($deviceDetail['fields'], function($arr) {
                                            return $arr['field'] == 'event_start_date';
                                        }));
                                $eventDuration = array_values(array_filter($deviceDetail['fields'], function($arr) {
                                            return $arr['field'] == 'event_duration';
                                        }));
                                if (count($eventStartDate)) {
                                    $pdfData['start'] = date('d-M-Y', strtotime($eventStartDate[0]['value']));
                                    $pdfData['end'] = date('d-M-Y', strtotime($eventStartDate[0]['value']));
                                    if (count($eventDuration)) {
                                        $numberofDays = $eventDuration[0]['value'] - 1;
                                        if ($numberofDays > 0) {
                                            $pdfData['end'] = date('d-M-Y', strtotime($eventStartDate[0]['value'] . ' +' . $numberofDays . ' days'));
                                        }
                                    }
                                }
                            }
                           $this->Pdf->generatePermissionLeter($pdfData);
                        } else {
							//================== MCG4 APPOVED PAID FEE CONDITION =================================
                            $startDate = date('Y-m-01', strtotime('+ 1 month'));
                            $endDate = date('Y-m-d', strtotime($startDate . ' +' . $tenure . ' months'));
                            $pdfData = [
                                'id' => $deviceDetail['id'],
                                'user_id' => $userId,
                                'status' => 'approved',
                                'date' => date('d M, Y'),
                                'to' => $deviceDetail['name'],
                                'address' => $deviceDetail['address'],
                                'file_no' => date('d.H.Y.m.' . $deviceDetail['id'], strtotime($deviceDetail['created'])),
                                'app_no' => 'MCFOMD' . date('Ymd', strtotime($deviceDetail['created'])) . $deviceDetail['id'],
                                'app_date' => date('d M, Y', strtotime($deviceDetail['created'])),
                                'omd_id' => 'MCFOMD' . $deviceDetail['id'],
                                'fees' => $fees,
                                'area' => $area,
                                'location' => $location,
                                'typology' => $deviceDetail['typology'],
                                'start' => date('d M, Y', strtotime($startDate)),
                                'end' => date('d M, Y', strtotime($endDate . '-1 day'))
                            ];
                            if ($deviceDetail['sub_category_id'] == 24 || $deviceDetail['sub_category_id'] == 33) {
                                $eventStartDate = array_values(array_filter($deviceDetail['fields'], function($arr) {
                                            return $arr['field'] == 'event_start_date';
                                        }));
                                $eventDuration = array_values(array_filter($deviceDetail['fields'], function($arr) {
                                            return $arr['field'] == 'event_duration';
                                        }));
                                if (count($eventStartDate)) {
                                    $pdfData['start'] = date('d-M-Y', strtotime($eventStartDate[0]['value']));
                                    $pdfData['end'] = date('d-M-Y', strtotime($eventStartDate[0]['value']));
                                    if (count($eventDuration)) {
                                        $numberofDays = $eventDuration[0]['value'] - 1;
                                        if ($numberofDays > 0) {
                                            $pdfData['end'] = date('d-M-Y', strtotime($eventStartDate[0]['value'] . ' +' . $numberofDays . ' days'));
                                        }
                                    }
                                }
                            }
                            $this->Pdf->generateLoiLeter($pdfData);
                        }
                        $message = "OMD (Device Id: " . $deviceId . ") application has been successfully approved.";
                        $this->Sms->sendTextSms($deviceDetail['phone'], $message);
                        $this->getMailer('Omd')->send('notifyOmdApproved', [$userDetail, $deviceDetail,$fees]);
                        $this->Flash->success(__('OMD device (' . $deviceId . ') has been Approved'));
                        $response['refresh'] = true;
                        $deviceTable->query()->update()->set(['status' => 'approved'])->where(['id' => $deviceId])->execute();
                    }
                } else if ($this->request->data['status'] !== 'Forward') {
                    if ($this->request->data['status'] == 'Return for Re submission') {
                        $response['refresh'] = true;
                        $deviceDetail = $this->_getDevice($deviceId);
                        $userId = $deviceDetail->user_id;
                        $userDetail = $this->_getUserProfileDetail($userId);
                        $this->loadComponent('Sms');
                        $message = "You need to resubmit your OMD application. Reason: " . $this->request->data['comment'] . ".";
                        $this->Sms->sendTextSms($userDetail['mobile'], $message);
                        $this->Flash->error(__('OMD device (' . $deviceId . ') has been returned for Resubmission'));
                        $deviceTable->query()->update()->set(['status' => 'resubmit'])->where(['id' => $deviceId])->execute();
                    }
//                    $deviceFieldData[] = [
//                        'field_key' => 'reason',
//                        'input_value' => implode(',', $this->request->data['comment'])
//                    ];

					//if ($mcgUserLevel == 4 && $this->request->data['status'] == 'Reject') {
                    if ($mcgUserLevel == 3 && $this->request->data['status'] == 'Reject') {
                        $this->loadComponent('Sms');
                        $this->loadComponent('Pdf');
                        $deviceDetail = $this->_getDevice($deviceId);
                        $userId = $deviceDetail->user_id;
                        $userDetail = $this->_getUserProfileDetail($userId);
                        $pdfData = [
                            'id' => $deviceDetail->id,
                            'user_id' => $userId,
                            'status' => 'rejected',
                            'date' => date('d M, Y'),
                            'to' => $userDetail['display_name'],
                            'address' => $userDetail['company_address'],
                            'file_no' => date('d.H.Y.m.' . $deviceDetail->id, strtotime($deviceDetail->created)),
                            'app_no' => 'MCFOMD' . date('Ymd', strtotime($deviceDetail->created)) . $deviceDetail->id,
                            'app_date' => date('d M, Y', strtotime($deviceDetail->created)),
                            'reason' => $this->request->data['comment']
                        ];
                        $this->Pdf->generateOmdLetter($pdfData);
                        $this->getMailer('Omd')->send('notifyOmdRejected', [$userDetail, $deviceDetail]);
                        $message = "Your OMD application has been rejected. Reason: " . $this->request->data['comment'];
                        $this->Sms->sendTextSms($userDetail['mobile'], $message);
                        $response['refresh'] = true;
                        if (!$post->errors()) {
                            $this->Flash->error(__('OMD device (' . $deviceId . ') has been Rejected'));
                        }
                        $deviceTable->query()->update()->set(['status' => 'rejected'])->where(['id' => $deviceId])->execute();
                    }
                }
                $post->comment_data = addslashes(json_encode($deviceFieldData));
                $commentTable->save($post);

                $mcgCommentsArr = $commentTable->find('all')->select(['device_id', 'mcg_level', 'user_name' => 'u.display_name', 'type' => 'u.type', 'comment', 'comment_data', 'status', 'created'])->join([
                            'table' => 'users',
                            'alias' => 'u',
                            'type' => 'LEFT',
                            'conditions' => 'McgComments.user_id = u.id'
                        ])->where(['device_id' => $deviceId])->order(['McgComments.id' => 'DESC'])->hydrate(false);
                $mcgComments = $mcgCommentsArr->toArray();
                $comments = [];
                array_walk($mcgComments, function($value) use(&$comments) {
                    $value['comment_data'] = json_decode($value['comment_data'], true);
                    $value['comment'] = trim(preg_replace('/\s\s+/', '<br>', $value['comment']));
                    $value['created'] = date('d M, Y h:iA', strtotime($value['created']));
                    $comments[] = $value;
                });
                $response['error'] = 0;
                $response['data'] = $comments;
            }
        }
        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }

    public function __checkCommentStatus($deviceId, $mcgLevel, $commentTable) {
        $query = $commentTable->find('all')->select(['id'])->where(['device_id' => $deviceId, 'mcg_level' => $mcgLevel])->hydrate(false);
        $lastComment = $commentTable->find('all')->where(['device_id' => $deviceId])->order(['id' => 'DESC'])->hydrate(false)->first();
        if (count($lastComment) && ($lastComment['status'] == 'Return') && (($lastComment['mcg_level'] - 1) != $mcgLevel)) {
            $comments = [];
            $mcgCommentsArr = $commentTable->find('all')->select(['device_id', 'mcg_level', 'user_name' => 'u.display_name', 'type' => 'u.type', 'comment', 'comment_data', 'status', 'created'])->join([
                        'table' => 'users',
                        'alias' => 'u',
                        'type' => 'LEFT',
                        'conditions' => 'McgComments.user_id = u.id'
                    ])->where(['device_id' => $deviceId])->order(['McgComments.id' => 'DESC'])->hydrate(false);
            $mcgComments = $mcgCommentsArr->toArray();
            array_walk($mcgComments, function($value) use(&$comments) {
                $value['comment_data'] = json_decode($value['comment_data'], true);
                $value['comment'] = trim(preg_replace('/\s\s+/', '<br>', $value['comment']));
                $value['created'] = date('d M, Y h:iA', strtotime($value['created']));
                $comments[] = $value;
            });
            $response['error'] = 2;
            $response['data'] = $comments;
            $response['message'] = 'You are not allowed to add comments';
            echo addslashes(json_encode($response));
            exit;
        } else if (count($query->toArray()) && $lastComment['status'] != 'Return' && (($lastComment['mcg_level'] - 1) == $mcgLevel)) {
            $comments = [];
            $mcgCommentsArr = $commentTable->find('all')->select(['device_id', 'mcg_level', 'user_name' => 'u.display_name', 'type' => 'u.type', 'comment', 'comment_data', 'status', 'created'])->join([
                        'table' => 'users',
                        'alias' => 'u',
                        'type' => 'LEFT',
                        'conditions' => 'McgComments.user_id = u.id'
                    ])->where(['device_id' => $deviceId])->order(['McgComments.id' => 'DESC'])->hydrate(false);
            $mcgComments = $mcgCommentsArr->toArray();
            array_walk($mcgComments, function($value) use(&$comments) {
                $value['comment_data'] = json_decode($value['comment_data'], true);
                $value['comment'] = trim(preg_replace('/\s\s+/', '<br>', $value['comment']));
                $value['created'] = date('d M, Y h:iA', strtotime($value['created']));
                $comments[] = $value;
            });
            $response['error'] = 2;
            $response['data'] = $comments;
            $response['message'] = 'This Operation has been alreay perform by MCF' . $mcgLevel . ' user';
            echo addslashes(json_encode($response));
            exit;
        }
    }

    public function users() {
        $userType = strtolower($this->Auth->user('type'));
        if ($userType != 'mcg2' && $userType != 'mcg3') {
            $this->Flash->error("You are not allowed to access this URL.");
            return $this->redirect(['action' => 'index']);
        }
        $usersAdditionalDocuments = [];
        $usersTable = TableRegistry::get('Users');
        $addressProfesData = TableRegistry::get('AddressProfes');
        $addressProfesArr = $addressProfesData->find('all')->hydrate(false)->toArray();
        $usersQuery = $usersTable->find('all')->select([
                    'id',
                    'display_name',
                    'email',
                    'mobile',
                    'type',
                    'comments',
                    'status',
                    'created',
                    'representative_id',
                    'representative' => 'r.representative',
                    'annual_turnover' => 'turnover.annual_turnover',
                    'company_name' => 'display_name',
                    'company_address' => 'up.company_address',
                    'director_name' => 'up.director_name',
                    'company_email' => 'up.company_email',
                    'SMC_party_code' => 'up.SMC_party_code',
                    'property_id' => 'up.property_id',
                    'incorporation_date' => 'up.incorporation_date',
                    'property_tax_receipt' => 'up.property_tax_receipt',
                    'income_tax_return' => 'up.income_tax_return',
                    'din_number' => 'up.din_number',
                    'income_proof' => 'up.income_proof',
                    'voter_id' => 'up.voter_id',
                    'adhaar_card' => 'up.adhaar_card',
                    'service_tax' => 'up.service_tax',
                    'turnover_certificate' => 'up.turnover_certificate',
                    'exp_certificate' => 'up.exp_certificate',
                    'balance_sheet' => 'up.balance_sheet',
                    'letter_by_board_directors' => 'up.letter_by_board_directors',
                    'incorporation_certificate' => 'up.incorporation_certificate',
                    'association_memorandum' => 'up.association_memorandum',
                    'association_articles' => 'up.association_articles',
                    'ration_card' => 'up.ration_card',
                    'driving_license' => 'up.driving_license',
                    'passport' => 'up.passport',
                    'tin_no' => 'up.tin_no',
                    'partnership_deed' => 'up.partnership_deed',
                    'address_proof_partnership_firm' => 'up.address_proof_partnership_firm',
                    'partnership_registration_certificate' => 'up.partnership_registration_certificate',
                    'pan_card_partners' => 'up.pan_card_partners',
                    'pan_card_llp' => 'up.pan_card_llp',
                    'registration_certificate_llp' => 'up.registration_certificate_llp',
                    'address_proof_llp' => 'up.address_proof_llp',
                    'address_proof_partners' => 'up.address_proof_partners',
                    'audited_balance_sheet' => 'up.audited_balance_sheet',
                    'power_of_attorney' => 'up.power_of_attorney',
                    'authorised_contact_person' => 'up.authorised_contact_person',
                    'undertaking_non_blacklisting' => 'up.undertaking_non_blacklisting',
                    'no_dues_pending' => 'up.no_dues_pending',
                    'advertising_experience' => 'up.advertising_experience',
                    'details_advertisement_permissions' => 'up.details_advertisement_permissions',
                    'partner_cv' => 'up.partner_cv',
                    'directors_pan_card' => 'up.directors_pan_card',
                    'company_pan_card' => 'up.company_pan_card',
                    'company_registration_certificate' => 'up.company_registration_certificate',
                    'company_address_proof' => 'up.company_address_proof',
                    'directors_address_proof' => 'up.directors_address_proof',
                    'organization_pan_card' => 'up.organization_pan_card',
                    'director_cv' => 'up.director_cv',
                    'organization_registration_certificate' => 'up.organization_registration_certificate',
                    'organisation_address_proof' => 'up.organisation_address_proof',
                    'address_type' => 'up.address_type',
                    'address_proof' => 'up.address_proof'
                ])->join([
                    'r' => [
                        'table' => 'representatives',
                        'type' => 'LEFT',
                        'conditions' => 'Users.representative_id = r.id'
                    ],
                    'up' => [
                        'table' => 'user_profiles',
                        'type' => 'LEFT',
                        'conditions' => 'Users.id = up.user_profile_id'
                    ],
                    'turnover' => [
                        'table' => 'annual_turnovers',
                        'type' => 'LEFT',
                        'conditions' => 'up.annual_turnover = turnover.id'
                    ]
                ])->where(['Users.type' => 'user', 'Users.status' => '1', 'Users.paid' => '1', 'Users.admin_approved' => '2'])->order(['Users.id' => 'DESC'])->hydrate(false);
        $users = $this->paginate($usersQuery)->toArray();
        $userIds = array_map(function($arr) {
            return $arr['id'];
        }, $users);
        $usersDocuments = $this->_getUsersDocumentsbyIds($userIds);
        $usersAdditionalDocumentsArr = $this->_getUsersAdditionalDocuments($userIds);
        $usersAdditionalDocumentsGrouped = $this->array_group_by($usersAdditionalDocumentsArr, 'user_id');
        array_walk($usersAdditionalDocumentsGrouped, function($val, $key) use(&$usersAdditionalDocuments) {
            $requests = array_map(function($arr) {
                $arr['filename'] = Router::url('/uploads/additonal_user_doc/' . $arr['filename']);
                $arr['created'] = date('d M, Y h:iA', strtotime($arr['created']));
                return $arr;
            }, $val);
            $usersAdditionalDocuments[$key] = $this->array_group_by($requests, 'created');
        });
        $userDetails = [];
        $entityDetails = [];
        $address = [];
        $comments	=	[];
        array_walk($users, function($arr) use(&$userDetails, &$entityDetails, $addressProfesArr) {
            $userDetails[$arr['id']] = [
                ['label' => 'Company Name', 'value' => $arr['display_name'], 'type' => 'field'],
                ['label' => 'Email', 'value' => $arr['email'], 'type' => 'field'],
                ['label' => 'Mobile', 'value' => $arr['mobile'], 'type' => 'field'],
                ['label' => 'Type', 'value' => $arr['type'], 'type' => 'field'],
                ['label' => 'Property Id', 'value' => $arr['property_id'], 'type' => 'field'],
                ['label' => 'Status', 'value' => ($arr['status'] == 1) ? 'Enable' : '', 'type' => 'field'],
                ['label' => 'Representative_id', 'value' => $arr['representative_id'], 'type' => 'field'],
                ['label' => 'Authorised Contact Person', 'value' => $arr['authorised_contact_person'], 'type' => 'field']
            ];
            $entityDetails[$arr['id']] = [
                ['label' => 'Company Name', 'value' => $arr['display_name']],
                ['label' => 'Company Address', 'value' => trim(preg_replace('/\s\s+/', '<br>', $arr['company_address']))],
                ['label' => 'Representative', 'value' => $arr['representative']],
                ['label' => 'Annual Turnover', 'value' => $arr['annual_turnover']],
                ['label' => 'SMC Party Code', 'value' => $arr['SMC_party_code']],
                ['label' => 'Date of Incorporation', 'value' => date('d-m-Y', strtotime($arr['incorporation_date']))]
            ];
            if ($arr['director_name']) {
                $entityDetails[$arr['id']] = array_merge($entityDetails[$arr['id']], [['label' => 'Director Name', 'value' => $arr['director_name']]]);
            }
        });
        
        array_walk($users,function($arr) use (&$comments) {
					if(!empty($arr['comments'])){
						$comments[$arr['id']] = json_decode($arr['comments'],true);
					} else {
						$comments[$arr['id']] = $arr['comments'];
					}
				});
		
        $type = $this->Auth->user('type');
        $this->set(compact('users', 'type'));
        $this->set('userDetails', addslashes(json_encode($userDetails)));
        $this->set('comments', addslashes(json_encode($comments)));
        $this->set('entityDetails', addslashes(json_encode($entityDetails)));
        $this->set('usersDocuments', addslashes(json_encode($usersDocuments)));
        $this->set('usersAdditionalDocuments', addslashes(json_encode($usersAdditionalDocuments)));
    }

    public function holdUsers() {
        $userType = strtolower($this->Auth->user('type'));
        if ($userType != 'mcg2' ) {
            $this->Flash->error("You are not allowed to access this URL.");
            return $this->redirect(['action' => 'index']);
        }
        $usersAdditionalDocuments = [];
        $usersTable = TableRegistry::get('Users');
        $addressProfesData = TableRegistry::get('AddressProfes');
        $addressProfesArr = $addressProfesData->find('all')->hydrate(false)->toArray();
        $usersQuery = $usersTable->find('all')->select([
                    'id',
                    'display_name',
                    'email',
                    'mobile',
                    'type',
                    'status',
                    'created',
                    'representative_id',
                    'representative' => 'r.representative',
                    'annual_turnover' => 'turnover.annual_turnover',
                    'company_name' => 'display_name',
                    'company_address' => 'up.company_address',
                    'director_name' => 'up.director_name',
                    'company_email' => 'up.company_email',
                    'SMC_party_code' => 'up.SMC_party_code',
                    'property_id' => 'up.property_id',
                    'incorporation_date' => 'up.incorporation_date',
                    'property_tax_receipt' => 'up.property_tax_receipt',
                    'income_tax_return' => 'up.income_tax_return',
                    'din_number' => 'up.din_number',
                    'income_proof' => 'up.income_proof',
                    'voter_id' => 'up.voter_id',
                    'adhaar_card' => 'up.adhaar_card',
                    'service_tax' => 'up.service_tax',
                    'turnover_certificate' => 'up.turnover_certificate',
                    'exp_certificate' => 'up.exp_certificate',
                    'balance_sheet' => 'up.balance_sheet',
                    'letter_by_board_directors' => 'up.letter_by_board_directors',
                    'incorporation_certificate' => 'up.incorporation_certificate',
                    'association_memorandum' => 'up.association_memorandum',
                    'association_articles' => 'up.association_articles',
                    'ration_card' => 'up.ration_card',
                    'driving_license' => 'up.driving_license',
                    'passport' => 'up.passport',
                    'tin_no' => 'up.tin_no',
                    'partnership_deed' => 'up.partnership_deed',
                    'address_proof_partnership_firm' => 'up.address_proof_partnership_firm',
                    'partnership_registration_certificate' => 'up.partnership_registration_certificate',
                    'pan_card_partners' => 'up.pan_card_partners',
                    'pan_card_llp' => 'up.pan_card_llp',
                    'registration_certificate_llp' => 'up.registration_certificate_llp',
                    'address_proof_llp' => 'up.address_proof_llp',
                    'address_proof_partners' => 'up.address_proof_partners',
                    'audited_balance_sheet' => 'up.audited_balance_sheet',
                    'power_of_attorney' => 'up.power_of_attorney',
                    'authorised_contact_person' => 'up.authorised_contact_person',
                    'undertaking_non_blacklisting' => 'up.undertaking_non_blacklisting',
                    'no_dues_pending' => 'up.no_dues_pending',
                    'advertising_experience' => 'up.advertising_experience',
                    'details_advertisement_permissions' => 'up.details_advertisement_permissions',
                    'partner_cv' => 'up.partner_cv',
                    'directors_pan_card' => 'up.directors_pan_card',
                    'company_pan_card' => 'up.company_pan_card',
                    'company_registration_certificate' => 'up.company_registration_certificate',
                    'company_address_proof' => 'up.company_address_proof',
                    'directors_address_proof' => 'up.directors_address_proof',
                    'organization_pan_card' => 'up.organization_pan_card',
                    'director_cv' => 'up.director_cv',
                    'organization_registration_certificate' => 'up.organization_registration_certificate',
                    'organisation_address_proof' => 'up.organisation_address_proof',
                    'address_type' => 'up.address_type',
                    'address_proof' => 'up.address_proof'
                ])->join([
                    'r' => [
                        'table' => 'representatives',
                        'type' => 'LEFT',
                        'conditions' => 'Users.representative_id = r.id'
                    ],
                    'up' => [
                        'table' => 'user_profiles',
                        'type' => 'LEFT',
                        'conditions' => 'Users.id = up.user_profile_id'
                    ],
                    'turnover' => [
                        'table' => 'annual_turnovers',
                        'type' => 'LEFT',
                        'conditions' => 'up.annual_turnover = turnover.id'
                    ]
                ])->where(['Users.type' => 'user', 'Users.status' => '1', 'Users.paid' => '1', 'Users.admin_approved' => '3'])->order(['Users.id' => 'DESC'])->hydrate(false);
        $users = $this->paginate($usersQuery)->toArray();
        $userIds = array_map(function($arr) {
            return $arr['id'];
        }, $users);
        $usersDocuments = $this->_getUsersDocumentsbyIds($userIds);
        $usersAdditionalDocumentsArr = $this->_getUsersAdditionalDocuments($userIds);
        $usersAdditionalDocumentsGrouped = $this->array_group_by($usersAdditionalDocumentsArr, 'user_id');
        array_walk($usersAdditionalDocumentsGrouped, function($val, $key) use(&$usersAdditionalDocuments) {
            $requests = array_map(function($arr) {
                $arr['filename'] = Router::url('/uploads/additonal_user_doc/' . $arr['filename']);
                $arr['created'] = date('d M, Y h:iA', strtotime($arr['created']));
                return $arr;
            }, $val);
            $usersAdditionalDocuments[$key] = $this->array_group_by($requests, 'created');
        });
        $userDetails = [];
        $entityDetails = [];
        $address = [];
        array_walk($users, function($arr) use(&$userDetails, &$entityDetails, $addressProfesArr) {
            $userDetails[$arr['id']] = [
                ['label' => 'Company Name', 'value' => $arr['display_name'], 'type' => 'field'],
                ['label' => 'Email', 'value' => $arr['email'], 'type' => 'field'],
                ['label' => 'Mobile', 'value' => $arr['mobile'], 'type' => 'field'],
                ['label' => 'Type', 'value' => $arr['type'], 'type' => 'field'],
                ['label' => 'Property Id', 'value' => $arr['property_id'], 'type' => 'field'],
                ['label' => 'Status', 'value' => ($arr['status'] == 1) ? 'Enable' : '', 'type' => 'field'],
                ['label' => 'Representative_id', 'value' => $arr['representative_id'], 'type' => 'field'],
                ['label' => 'Authorised Contact Person', 'value' => $arr['authorised_contact_person'], 'type' => 'field']
            ];
            $entityDetails[$arr['id']] = [
                ['label' => 'Company Name', 'value' => $arr['display_name']],
                ['label' => 'Company Address', 'value' => $arr['company_address']],
                ['label' => 'Representative', 'value' => $arr['representative']],
                ['label' => 'Annual Turnover', 'value' => $arr['annual_turnover']],
                ['label' => 'SMC Party Code', 'value' => $arr['SMC_party_code']],
                ['label' => 'Date of Incorporation', 'value' => date('d-m-Y', strtotime($arr['incorporation_date']))]
            ];
            if ($arr['director_name']) {
                $entityDetails[$arr['id']] = array_merge($entityDetails[$arr['id']], [['label' => 'Director Name', 'value' => $arr['director_name']]]);
            }
        });
        $type = $this->Auth->user('type');
        $this->set(compact('users', 'type'));
        $this->set('userDetails', addslashes(json_encode($userDetails)));
        $this->set('entityDetails', addslashes(json_encode($entityDetails)));
        $this->set('usersDocuments', addslashes(json_encode($usersDocuments)));
        $this->set('usersAdditionalDocuments', addslashes(json_encode($usersAdditionalDocuments)));
    }

    public function pendingUsers() {
        $rejectionReassons = [];
        $userType = strtolower($this->Auth->user('type'));
        if ($userType != 'mcg2' &&$userType != 'mcg3') {
            $this->Flash->error("You are not allowed to access this URL.");
            return $this->redirect(['action' => 'index']);
        }
        $usersAdditionalDocuments = [];
        $usersTable = TableRegistry::get('Users');
        $addressProfesData = TableRegistry::get('AddressProfes');
        $addressProfesArr = $addressProfesData->find('all')->hydrate(false)->toArray();
        $usersQuery = $usersTable->find('all')->select([
                    'id',
                    'display_name',
                    'email',
                    'mobile',
                    'type',
                    'status',
                    'rejection_reason',
                    'comments',
                    'admin_approved',
                    'created',
                    'representative_id',
                    'representative' => 'r.representative',
                    'annual_turnover' => 'turnover.annual_turnover',
                    'company_name' => 'up.company_name',
                    'company_address' => 'up.company_address',
                    'director_name' => 'up.director_name',
                    'din' => 'up.din',
                    'company_email' => 'up.company_email',
                    'SMC_party_code' => 'up.SMC_party_code',
                    'property_id' => 'up.property_id',
                    'incorporation_date' => 'up.incorporation_date',
                    'property_tax_receipt' => 'up.property_tax_receipt',
                    'income_tax_return' => 'up.income_tax_return',
                    'din_number' => 'up.din_number',
                    'income_proof' => 'up.income_proof',
                    'voter_id' => 'up.voter_id',
                    'adhaar_card' => 'up.adhaar_card',
                    'service_tax' => 'up.service_tax',
                    'turnover_certificate' => 'up.turnover_certificate',
                    'exp_certificate' => 'up.exp_certificate',
                    'balance_sheet' => 'up.balance_sheet',
                    'letter_by_board_directors' => 'up.letter_by_board_directors',
                    'incorporation_certificate' => 'up.incorporation_certificate',
                    'association_memorandum' => 'up.association_memorandum',
                    'association_articles' => 'up.association_articles',
                    'ration_card' => 'up.ration_card',
                    'driving_license' => 'up.driving_license',
                    'passport' => 'up.passport',
                    'tin_no' => 'up.tin_no',
                    'partnership_deed' => 'up.partnership_deed',
                    'address_proof_partnership_firm' => 'up.address_proof_partnership_firm',
                    'partnership_registration_certificate' => 'up.partnership_registration_certificate',
                    'pan_card_partners' => 'up.pan_card_partners',
                    'pan_card_llp' => 'up.pan_card_llp',
                    'registration_certificate_llp' => 'up.registration_certificate_llp',
                    'address_proof_llp' => 'up.address_proof_llp',
                    'address_proof_partners' => 'up.address_proof_partners',
                    'audited_balance_sheet' => 'up.audited_balance_sheet',
                    'power_of_attorney' => 'up.power_of_attorney',
                    'authorised_contact_person' => 'up.authorised_contact_person',
                    'undertaking_non_blacklisting' => 'up.undertaking_non_blacklisting',
                    'no_dues_pending' => 'up.no_dues_pending',
                    'advertising_experience' => 'up.advertising_experience',
                    'details_advertisement_permissions' => 'up.details_advertisement_permissions',
                    'partner_cv' => 'up.partner_cv',
                    'directors_pan_card' => 'up.directors_pan_card',
                    'company_pan_card' => 'up.company_pan_card',
                    'company_registration_certificate' => 'up.company_registration_certificate',
                    'company_address_proof' => 'up.company_address_proof',
                    'directors_address_proof' => 'up.directors_address_proof',
                    'organization_pan_card' => 'up.organization_pan_card',
                    'director_cv' => 'up.director_cv',
                    'organization_registration_certificate' => 'up.organization_registration_certificate',
                    'organisation_address_proof' => 'up.organisation_address_proof',
                    'address_type' => 'up.address_type',
                    'address_proof' => 'up.address_proof'
                ])->join([
                    'r' => [
                        'table' => 'representatives',
                        'type' => 'LEFT',
                        'conditions' => 'Users.representative_id = r.id'
                    ],
                    'up' => [
                        'table' => 'user_profiles',
                        'type' => 'LEFT',
                        'conditions' => 'Users.id = up.user_profile_id'
                    ],
                    'turnover' => [
                        'table' => 'annual_turnovers',
                        'type' => 'LEFT',
                        'conditions' => 'up.annual_turnover = turnover.id'
                    ]
                ])->where(['Users.type' => 'user', 'Users.status' => 1, 'Users.paid' => 1, 'Users.admin_approved IN' => ['0', '1']])->order(['Users.id' => 'DESC'])->hydrate(false);
        $users = $this->paginate($usersQuery)->toArray();
        $userIds = array_map(function($arr) {
            return $arr['id'];
        }, $users);
        $usersDirectors = $this->_getUsersDirectors($userIds);
        $usersDocuments = $this->_getUsersDocumentsbyIds($userIds);
        $usersAdditionalDocumentsArr = $this->_getUsersAdditionalDocuments($userIds);
        $usersAdditionalDocumentsGrouped = $this->array_group_by($usersAdditionalDocumentsArr, 'user_id');
        array_walk($usersAdditionalDocumentsGrouped, function($val, $key) use(&$usersAdditionalDocuments) {
            $requests = array_map(function($arr) {
                $arr['filename'] = Router::url('/uploads/additonal_user_doc/' . $arr['filename']);
                $arr['created'] = date('d M, Y h:iA', strtotime($arr['created']));
                return $arr;
            }, $val);
            $usersAdditionalDocuments[$key] = $this->array_group_by($requests, 'created');
        });
        $userDetails = [];
        $entityDetails = [];
        $turnovers_array = [];
        $comments	=	[];
        // $annual_turnovers = $this->AnnualTurnovers->find('all')->first();
        array_walk($users, function($arr) use(&$userDetails, &$entityDetails, $addressProfesArr, $usersDirectors) {
            $userId = $arr['id'];
            $userDetails[$arr['id']] = [
                ['label' => 'Company Name', 'value' => $arr['display_name'], 'type' => 'field'],
                ['label' => 'Email', 'value' => $arr['email'], 'type' => 'field'],
                ['label' => 'Mobile', 'value' => $arr['mobile'], 'type' => 'field'],
                ['label' => 'Type', 'value' => $arr['type'], 'type' => 'field'],
                ['label' => 'Status', 'value' => ($arr['status'] == 1) ? 'Enable' : '', 'type' => 'field'],
                ['label' => 'admin_approved', 'value' => $arr['admin_approved'], 'type' => 'field'],
                ['label' => 'Representative_id', 'value' => $arr['representative_id'], 'type' => 'field'],
                ['label' => 'Authorised Contact Person', 'value' => $arr['authorised_contact_person'], 'type' => 'field']
            ];
            $entityDetails[$arr['id']] = [
                ['label' => 'Company Name', 'value' => $arr['display_name']],
                ['label' => 'Company Address', 'value' => trim(preg_replace('/\s\s+/', '<br>', $arr['company_address']))],
                ['label' => 'Representative', 'value' => $arr['representative']],
                ['label' => 'Annual Turnover', 'value' => $arr['annual_turnover']],
                ['label' => 'SMC Party Code', 'value' => $arr['SMC_party_code']],
                ['label' => 'Date of Incorporation', 'value' => date('d-m-Y', strtotime($arr['incorporation_date']))]
            ];
            if ($arr['director_name']) {
                $entityDetails[$arr['id']] = array_merge($entityDetails[$arr['id']], [['label' => 'Director1 Name', 'value' => $arr['director_name']]]);
                $entityDetails[$arr['id']] = array_merge($entityDetails[$arr['id']], [['label' => 'Director1 DIN Number', 'value' => $arr['din']]]);
            }
            $userDirectors = array_values(array_filter($usersDirectors, function($directorArr) use($userId) {
                        return $directorArr['director_user_id'] == $userId;
                    }));
            if (count($userDirectors)) {
                foreach ($userDirectors as $key => $userDirector) {
                    $entityDetails[$arr['id']] = array_merge($entityDetails[$arr['id']], [['label' => 'Director' . ($key + 2) . ' Name', 'value' => $userDirector['director_name']]]);
                    $entityDetails[$arr['id']] = array_merge($entityDetails[$arr['id']], [['label' => 'Director' . ($key + 2) . ' DIN Number', 'value' => $userDirector['din']]]);
                }
            }
        });
        /*
				array_walk($users, function($val) use(&$rejectionReassons, $userType) {
					$rejectionReassons[$val['id']] = (($val['admin_approved'] == 0 && $userType == 'mcg1') ? trim(preg_replace('/\s\s+/', ' ', $val['rejection_reason'])) : false);
        });
        */
        array_walk($users, function($val) use(&$comments) {
					if(!empty($val['comments'])){
						$comments[$val['id']] = json_decode($val['comments'],true);
					} else {
						$comments[$val['id']] = $val['comments'];
					}
				});
				$this->set('usertype', $userType);
        $this->set(compact('users', 'rejectionReassons'));
        
        $this->set('comments', addslashes(json_encode($comments)));
        $this->set('userDetails', addslashes(json_encode($userDetails)));
        $this->set('entityDetails', addslashes(json_encode($entityDetails)));
        $this->set('usersDocuments', addslashes(json_encode($usersDocuments)));
        $this->set('usersAdditionalDocuments', addslashes(json_encode($usersAdditionalDocuments)));
    }

    public function approveUsers() {
			$this->_ajaxCheck();
			$this->autoRender = false; // avoid to render view
			if ($this->request->is('ajax')) {
				if ($this->request->is('post')) {
					$usersTable = TableRegistry::get('Users');
					$response['error'] = 0;
					$userId = $this->request->data['user_id'];
					$value = $this->request->data['value'];
					$rejection_reason = $this->request->data['rejection_reason'];
					$userType = strtolower($this->Auth->user('type'));
					if (!empty($userId) && !empty($value)) {
						if ($value == 2) {
								$status = 1;
								$approved = 2;
						} else if ($value == 3) {
								$status = 2;
								$approved = 0;
						} else if ($value == 1 && $userType == "mcg3") {
								$status = 1;
								$approved = 0;
						} else if ($value == 1 && $userType == "mcg2") {
								$status = 1;
								$approved = 1;
						}
            //============= add by rohit kaushal(21-08-2018) ================
						$userType 	= $this->Auth->user('type');
						$userType = str_replace("G","F",$userType);
						$userZone	=	$this->Auth->user('display_name');
						$type 		= $userType;
						if(!empty($userZone)){
							$type .= ' ('.$userZone.')';
						}
						$usersRec = $usersTable->find('all')->select(['id','comments'])->
							where([
								'Users.id' => $userId,
								'Users.type' => 'user', 
								'Users.status' => 1, 
								'Users.paid' => 1
							])->first()->toArray();
							
						$commment_arr	=	[];
						$comment_rec	=	$usersRec['comments'];
						if(!empty($comment_rec)){
							$commment_arr = json_decode($comment_rec,true);
						}
						if(!empty($rejection_reason)){
							$post_comment_arr = [
								'id'=>$this->Auth->user('id'),
								'type'=>$type,
								'comment'=>$rejection_reason,
								'date' => date("j F, Y g:i a")
							];
							array_push($commment_arr,$post_comment_arr);
						}
						$comment_json = json_encode($commment_arr);
						$updateStatus = $usersTable->query()->update()->set(['status' => $status, 'rejection_reason' => $rejection_reason,'comments'=>$comment_json, 'admin_approved' => $approved])->where(['id' => $userId])->execute();
						//============================================================
						if (count($updateStatus) > 0) {
								$this->loadComponent('Sms');
								$userData = $usersTable->find()->where(['id' => $userId])->first();
								$userType	=	strtolower($userType);
								if ($value == 2 && $userType == "mcf3") {
									   //echo "approve".$userData->mobile;
										$this->Sms->sendTextSms($userData->mobile, 'You account has been approved by Administrator. You can now access your dashboard.');
										$response['data'] = 'User has been successfully approved.';
										//$this->getMailer('User')->send('emailVerification', [$userData]);
										$this->getMailer('User')->send('approvedEmail', [$userData]);
								} else if ($value == 3) {
										//echo "reject".$userData->mobile;
										$this->Sms->sendTextSms($userData->mobile, 'You account has been rejected by Administrator. You can register again or contact MCF support.');
										$response['data'] = 'User has been rejected.';
										$this->getMailer('User')->send('rejectedEmail', [$userData]);
								}
						} else {
								$response['error'] = 1;
								$response['data'] = 'User has not been approved.';
						}
					} else {
									$response['error'] = 1;
									$response['data'] = 'User has not been approved.';
							}
					}
				echo json_encode($response);
			}
    }

    public function requestAdditionalDocs() {
        $this->_ajaxCheck();
        $this->autoRender = false;
        $json['error'] = 0;
        $userId = $this->request->data['user_id'];
        $userDetail = $this->_getUserProfileDetail($userId);
        $usersTable = TableRegistry::get('Users');
        $userEntity = $usersTable->newEntity(['admin_approved' => '3'], ['validate' => false]);
        $userEntity->id = $userId;
        $usersTable->save($userEntity);
        $requestsTable = TableRegistry::get('UserAdditionalDocRequests');
        $requestsDocsTable = TableRegistry::get('UserAdditionalDocs');
        $requestEntity = $requestsTable->newEntity(['user_id' => $userId]);
        $requestSaved = $requestsTable->save($requestEntity);
        $documentData = [];
        $counter = 0;
        foreach ($this->request->data['document'] as $key => $docName) {
            $documentData[$counter] = [
                'request_id' => $requestSaved->id,
                'name' => $docName,
                'reason' => $this->request->data['reason'][$key]
            ];
            $counter++;
        }
        $requestsDocsEntities = $requestsDocsTable->newEntities($documentData);
        $requestsDocsTable->saveMany($requestsDocsEntities);
        $this->loadComponent('Sms');
        $message = 'MCF has requested some additional documents. Please upload ' . implode(', ', $this->request->data['document']) . '. Details has been sent on your registered email.';
        $this->Sms->sendTextSms($userDetail['mobile'], $message);
        $this->getMailer('User')->send('requestAdditionalDocs', [$userDetail, $this->request->data['document']]);
        $this->_jsonResponse($json);
    }

    public function requestAdditionalDocsOmd() {
        $deviceId = $this->request->data['device'];
        $this->autoRender = false;
        $json['error'] = 0;
        $deviceTable = TableRegistry::get('CompanyDevices');
        $commentTable = TableRegistry::get('McgComments');
        $requestsTable = TableRegistry::get('OmdAdditionalDocRequests');
        $requestsDocsTable = TableRegistry::get('OmdAdditionalDocs');
        $requestEntity = $requestsTable->newEntity(['device_id' => $deviceId]);
        $requestSaved = $requestsTable->save($requestEntity);
        $documentData = [];
        $counter = 0;
        foreach ($this->request->data['document'] as $key => $docName) {
            $documentData[$counter] = [
                'request_id' => $requestSaved->id,
                'name' => $docName,
                'reason' => $this->request->data['reason'][$key]
            ];
            $counter++;
        }
         //=============== add by rohit (12-12-2018) ==================
        $mcgUserLevel = preg_replace("/[^0-9,.]/", "", $this->user['type']);
        //============================================================
        
        $requestsDocsEntities = $requestsDocsTable->newEntities($documentData);
        $requestsDocsTable->saveMany($requestsDocsEntities);
        $deviceDetail = $this->_getDevice($deviceId);
        $deviceTable->query()->update()->set(['status' => 'document requested'])->where(['id' => $deviceId])->execute();
        $commentTable->query()->insert(['device_id', 'mcg_level', 'user_id', 'comment', 'status', 'created', 'modified'])->values([
            'device_id' => $deviceId,
           // 'mcg_level' => 1,
			'mcg_level' => $mcgUserLevel,
            'user_id' => $this->Auth->user('id'),
            'comment' => implode(', ', $this->request->data['document']),
            'status' => 'document requested',
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s')
        ])->execute();
        $user = $this->_getUserDetail($deviceDetail->user_id);
        $message = 'MCF has requested some additional documents for OMD device (' . $deviceId . '). Please upload ' . implode(', ', $this->request->data['document']);
        $this->loadComponent('Sms');
        $this->Sms->sendTextSms($user['mobile'], $message);
        $this->Flash->success("Device has been returned for additional documents");
//        $this->getMailer('User')->send('requestAdditionalDocs', [$user, $this->request->data['document'], date('d M, Y', strtotime($this->request->data['last_date']))]);
        $this->_jsonResponse($json);
    }

    public function updateMultipleOmdStatus() {
        $this->autoRender = false;
        $items = $this->request->data['items'];
        $appliedOn = $this->request->data['appliedOn'];
        $deviceId = $this->request->data['deviceId'];
        $device = $this->_getDeviceData($deviceId);
        $totalArea = array_sum(array_values(array_map(function($arr) {
                            return $arr['area'];
                        }, $items)));
        $totalAnnualFees = array_sum(array_values(array_map(function($arr) {
                            return $arr['annual_fees'];
                        }, $items)));
        $tenure = array_values(array_filter($device['fields'],function($arr){
            return $arr['field'] == 'tenure';
        }));
        $discount = array_values(array_filter($device['fields'],function($arr){
            return $arr['field'] == 'discount';
        }));
        if($tenure) {
            $totalAnnualFees = $totalAnnualFees * $tenure[0]['value'];
        }
        if($discount) {
            if($discount[0]['value']) {
                $discountAmount = ($totalAnnualFees * $discount[0]['value'])/100;
                $totalAnnualFees = $totalAnnualFees - $discountAmount;
            }
        }
        $appliedItems = addslashes(json_encode($items[$appliedOn]));
        $companyDeviceFieldsTable = TableRegistry::get('CompanyDeviceFields');
        $companyDeviceFieldsTable->query()->update()->set(['input_value' => $appliedItems])->where(['field_key' => $appliedOn, 'company_device_id' => $deviceId])->execute();
        $companyDeviceFieldsTable->query()->update()->set(['input_value' => $totalArea])->where(['field_key' => 'total_advertising_space', 'company_device_id' => $deviceId])->execute();
        $companyDeviceFieldsTable->query()->update()->set(['input_value' => $totalAnnualFees])->where(['field_key' => 'total_permission_fees', 'company_device_id' => $deviceId])->execute();
    }

    public function pendingInstallments() {
        $userType = strtolower($this->Auth->user('type'));
        if ($userType != 'mcg3') {
            $this->Flash->success("You are not allowed to access this URL.");
            return $this->redirect(['action' => 'index']);
        }
        $devices = [];
        $currentDate = date('Y-m-d');
//        $currentDate = '2019-07-02';
        $installmentsTable = TableRegistry::get('Installments');
        $installmentsQuery = $installmentsTable->find('all')->where([
                    ['start_date <' => $currentDate],
                    ['payment_id' => 0]
                ])->order(['start_date' => 'DESC'])->hydrate(false);
        $installmentsArr = $this->paginate($installmentsQuery)->toArray();
        $deviceIds = array_map(function($arr) {
            return $arr['device_id'];
        }, $installmentsArr);
        if ($deviceIds) {
            $devices = $this->_getDeviceData($deviceIds);
        }
        $installments = array_map(function($arr) use($devices) {
            $arr['device'] = [];
            $device_id = $arr['device_id'];
            $device = array_values(array_filter($devices, function($deviceArr) use($device_id) {
                        return $deviceArr['id'] == $device_id;
                    }));
            if ($device) {
                $arr['device'] = reset($device);
            }
            return $arr;
        }, $installmentsArr);
        $this->set(compact('installments'));
    }

    public function changePassword() {
        $userTable = TableRegistry::get('Users');
        $userData = $userTable->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['password'] = $this->request->data['new_password'];
            $this->request->data['id'] = $this->user['id'];
            $userData = $userTable->patchEntity($userData, $this->request->data, ['validate' => 'Password']);
            if (empty($userData->errors())) {
                $userData['id'] = $this->user['id'];
                if ($userTable->save($userData)) {
                    $this->Flash->success('The password is successfully changed');
                    $this->redirect(['action' => 'changePassword']);
                } else {
                    $this->Flash->error('There was an error during the save!');
                }
            }
        }
        $this->set(compact('userData'));
    }

}
