<?php

namespace App\Controller\Guest;

use App\Controller\Guest\AppController;
use Cake\Event\Event;
use App\Traits\Users;
use Cake\ORM\TableRegistry;

class DashboardController extends AppController {

    use Users;
    
    public $paginate = [
        'limit' => 10,
        'order' => [
            'companyDevices.id' => 'desc'
        ]
    ];

    public function initialize() {
        parent::initialize();
        $this->viewBuilder()->layout('dashboard');
        $this->loadComponent('Paginator');
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $userData = $this->_getUserData($this->Auth->user('id'));
        if (!$userData['doc_uploaded']) {
            return $this->redirect(['controller' => 'Users', 'action' => 'profileCompletion']);
        }
    }

    public function index() {
        $user = $this->_getUserData($this->Auth->user('id'));
        $status = trim($this->request->query('status'));
        $status = $status == 'pending' ? 'under process' : $status;
        $comapnyDevicesTable = TableRegistry::get('companyDevices');

        //// Prepare devices array ------------------------------------------------------------------------------------------
        $devices = $comapnyDevicesTable->find('all')->select(['companyDevices_id' => 'companyDevices.id', 'device_name' => 'companyDevices.device_name', 'companyDevices_status' => 'companyDevices.status', 'companyDeviceFields_fieldData' => 'companyDeviceFields.fieldData'])->join([
                    'table' => "(SELECT `company_device_id`, GROUP_CONCAT(CONCAT(`field_key`,'=',`input_value`) SEPARATOR '#@!') AS fieldData FROM `company_device_fields` GROUP BY `company_device_id`)",
                    'alias' => 'companyDeviceFields',
                    'type' => 'LEFT',
                    'conditions' => 'companyDevices.id = companyDeviceFields.company_device_id',
                ])->where(['companyDevices.user_id' => $this->user['id'], 'companyDevices.paid' => '1']);
        if ($status)
            $devices = $devices->andWhere(['status' => $status]);

        $devices = $devices->hydrate(false);
        $devices = $this->paginate($devices)->toArray();
        //// Prepare latLngArray array ---------------------------------------------------------------------------------------
        $latLngArray = [];
        foreach ($devices as $deviceKey => $device) {
            $tempDevice = explode('#@!', $device['companyDeviceFields_fieldData']);

            foreach ($tempDevice as $singleTempDevice) {
                $deviceFields = explode('=', $singleTempDevice);
                if (in_array($deviceFields[0], ['lat', 'lng', 'location', 'concession_fees'])) {
                    $devices[$deviceKey]['companyDevice_' . $deviceFields[0]] = $deviceFields[1];
                }
            }

            if (isset($devices[$deviceKey]['companyDevice_lat']) && isset($devices[$deviceKey]['companyDevice_lat'])) {
                $latLngArray[] = array('lat' => $devices[$deviceKey]['companyDevice_lat'], 'lng' => $devices[$deviceKey]['companyDevice_lng']);
            }
        }
        $latLngArray = json_encode($latLngArray);

        /// Set paginated sorKey and Order Icon Name
        $iconName = "bottom";
        if (isset($this->Paginator->request->params['paging']['companyDevices'])) {
            $paginationVars = $this->Paginator->request->params['paging']['companyDevices'];
            $iconName = $paginationVars['direction'] == 'desc' ? 'top' : 'bottom';
        }
        $userAdditionalDocs = [];
        $userAdditionalDocRequestsTable = TableRegistry::get('UserAdditionalDocRequests');
        $lastAdditionalDocRequest = $userAdditionalDocRequestsTable->find('all')->where(['user_id' => $this->user['id']])->order(['id' => 'DESC'])->hydrate(false)->first();
        if (count($lastAdditionalDocRequest)) {
            $userAdditionalDocsTable = TableRegistry::get('UserAdditionalDocs');
            $userAdditionalDocs = $userAdditionalDocsTable->find('all')->select(['id', 'name', 'reason', 'is_uploaded'])->where(['request_id' => $lastAdditionalDocRequest['id']])->hydrate(false)->toArray();
        }
        $expirationDate = date('d M, Y', strtotime($user['created'] . ' +6 years'));
//        $adminApproved = $user['admin_approved'];
        $adminApproved = 2;
        if($user['is_token'] && $user['email']) {
            $adminApproved = 4;
        }
        $session = $this->request->session();
        $receipt = $session->read('Registration.receipt');
        $session->delete('Registration.receipt');
        //// Set variables array ------------------------------------------------------------------------------------------
        $this->set(compact('devices', 'receipt', 'adminApproved', 'userAdditionalDocs', 'expirationDate'));
        $this->set(compact('latLngArray'));
        $this->set(compact('iconName'));
    }

}
