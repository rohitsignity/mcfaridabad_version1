<?php

namespace App\Controller\Guest;

use App\Controller\Guest\AppController;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use App\Traits\Users;
use Cake\Mailer\MailerAwareTrait;

class UsersController extends AppController {

    use Users,
        MailerAwareTrait;

    public function index() {
        $userId = $this->Auth->user('id');
        if ($userId) {
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }
    }

    public function requestOtp() {
        $this->_ajaxCheck();
        $response['error'] = 0;
        $user = $this->Users->find('all')->select(['id'])->where(['user_name' => $this->request->data('user_name')])->hydrate(false)->toArray();
        $post = $this->Users->newEntity();
        if ($this->request->is('ajax')) {
            $post = $this->Users->patchEntity($post, $this->request->data, ['validate' => 'Guest']);
        }
        if ($post->errors()) {
            $response['error'] = 1;
            $response['data'] = $post->errors();
        } else {
            $otp = $this->_generateOTP(6);
            $this->loadComponent('Sms');
            $message = 'Your one time password for Guest Login is '.$otp.'';
            $this->Sms->sendTextSms($this->request->data('user_name'), $message);
            $post->password = $otp;
            $post->type = 'guest';
            $post->status = '1';
            if($user) {
                $post->id = $user[0]['id'];
            } else {
                $post->status = '1';
                $post->mobile = $this->request->data('user_name');
            }
            $this->Users->save($post);
        }
        $this->set($response);
    }

    public function validateOtp() {
        $this->_ajaxCheck();
        $response['error'] = 0;
        $response['data'] = Router::url(['controller' => 'Dashboard', 'action' => 'index']);
        $post = $this->Users->newEntity();
        if ($this->request->is('ajax')) {
            $post = $this->Users->patchEntity($post, $this->request->data, ['validate' => 'GuestOtp']);
        }
        if ($post->errors()) {
            $response['error'] = 1;
            $response['data'] = $post->errors();
        } else {
            $user = $this->Auth->identify();
            if ($user) {
                $userEntity = $this->Users->newEntity();
                $userEntity->id = $user['id'];
                $userEntity->password = $this->_generateRandomString(20);
                $this->Users->save($userEntity);
                $this->Auth->setUser($user);
            } else {
                $response['error'] = 1;
                $response['data'] = array(
                    'password' => array(
                        '_invalid' => 'Invalid OTP'
                    )
                );
            }
        }
        $this->set($response);
    }

    public function profileCompletion() {
        $this->viewBuilder()->layout('dashboard');
        $userData = $this->_getUserData($this->Auth->user('id'));
        if ($userData['email']) {
            return $this->redirect(['controller' => 'Users', 'action' => 'uploadDocuments']);
        }
        /* Address Profes* */
        $addressProfes = [];
        $addressProfesData = TableRegistry::get('AddressProfes');
        $addressProfesArr = $addressProfesData->find('all')->hydrate(false)->toArray();
        foreach ($addressProfesArr as $addressProfesItem) {
            $addressProfes[$addressProfesItem['id']] = $addressProfesItem['name'];
        }
        /* Security Questions List */
        $securityQuestions = [];
        $securityQuestionData = TableRegistry::get('SecurityQus');
        $securityQuestion = $securityQuestionData->find()->select(['id', 'question'])->hydrate(false);
        foreach ($securityQuestion->toArray() as $securityQuestionItem) {
            $securityQuestions[$securityQuestionItem['id']] = $securityQuestionItem['question'];
        }
        /* Representatives List */
        $representatives = [];
        $representativesData = TableRegistry::get('Representatives');
        $representative = $representativesData->find()->select(['id', 'representative'])->hydrate(false);
        foreach ($representative->toArray() as $representativeEntity) {
            $representatives[$representativeEntity['id']] = $representativeEntity['representative'];
        }

        /* Annual Turnovers List */
        $turnovers = [];
        $turnoversData = TableRegistry::get('AnnualTurnovers');
        $annualTurnovers = $turnoversData->find()->select(['id', 'annual_turnover'])->hydrate(false);
        foreach ($annualTurnovers->toArray() as $annuaTurn) {
            $turnovers[$annuaTurn['id']] = $annuaTurn['annual_turnover'];
        }

        /* Experiences Lists */
        $experiences = [];
        $experiencesTable = TableRegistry::get('Experiences');
        $experiencesData = $experiencesTable->find('all')->hydrate(false)->toArray();
        array_walk($experiencesData, function($val) use(&$experiences) {
            $experiences[$val['id']] = $val['name'];
        });
        $this->set(compact('securityQuestions'));
        $this->set(compact('representatives'));
        $this->set(compact('experiences'));
        $this->set(compact('turnovers', 'addressProfes'));
    }

    public function uploadDocuments() {
        $this->viewBuilder()->layout('dashboard');
        $response['error'] = 1;
        $userId = $this->Auth->user('id');
        $userData = $this->_getUserData($userId);
        if($userData['doc_uploaded']) {
            $this->redirect(['controller' => 'Dashboard', 'action' => 'index']); 
        }
        if ($userData) {
            $response['error'] = 0;
            $userDocuments = $this->_getUserDocuments($userId);
            $representativeId = $userData['representative_id'];
            $registrationDocumentsTable = TableRegistry::get('RegistrationDocuments');
            //$registrationDocumentsArr = $registrationDocumentsTable->find('all')->select(['id', 'name', 'field', 'is_required', 'related_to'])->where(['FIND_IN_SET("' . $representativeId . '",entity)'])->hydrate(false)->toArray();
             $registrationDocumentsArr = $registrationDocumentsTable->find('all')->select(['id', 'name', 'field', 'is_required', 'related_to'])->where(['FIND_IN_SET("0",entity)'])->hydrate(false)->toArray();
            $registrationDocuments = array_map(function($arr) use($userDocuments) {
                $field = $arr['field'];
                $arr['uploaded'] = 0;
                $userDocumentsArr = array_filter($userDocuments, function($element) use($field) {
                    return $element['field'] == $field;
                });
                if (count($userDocumentsArr)) {
                    $arr['uploaded'] = 1;
                }
                return $arr;
            }, $registrationDocumentsArr);
            $response['data'] = $registrationDocuments;
        }
        $this->set(compact('response'));
    }

    public function uploadDoc() {
        $this->autoRender = false;
        $response['error'] = 1;
        $documentName = '';
        $documentNameArr = array_keys($this->request->data);
        $userId = $this->Auth->user('id');
        if (isset($documentNameArr[0])) {
            $documentName = $documentNameArr[0];
        }
        $response['file'] = $documentName;
        if ($this->request->data[$documentName]['size']) {
            $ext = pathinfo($this->request->data[$documentName]['name'], PATHINFO_EXTENSION);
            $allowed = array('gif', 'jpeg', 'png', 'jpg', 'doc', 'docx', 'xls', 'xlsx', 'pdf');
            if (!in_array(strtolower($ext), $allowed)) {
                $response['error'] = 1;
                $response['file'] = $documentName;
                $response['data'] = 'These files extension are allowed: .png, .gif, .jpeg, .jpg, .doc, .docx, .xls, .xlsx, .pdf';
                $this->_jsonResponse($response);
            }
            $fileName = time() . '_' . $this->_generateRandomString(20) . '_' . $documentName . '.' . $ext;
            $uploadFile = WWW_ROOT . 'uploads/register_documents/' . $fileName;
            if (move_uploaded_file($this->request->data[$documentName]['tmp_name'], $uploadFile)) {
                $response['error'] = 0;
                $response['file'] = $documentName;
                $userDocumentsTable = TableRegistry::get('UserDocuments');
                $userDocumentEntity = $userDocumentsTable->newEntity(['user_id' => $userId, 'field' => $documentName, 'document' => $fileName]);
                $userDocumentsTable->save($userDocumentEntity);
            }
        }
        $userData = $this->_getUserData($userId);
        $alldocumentUploaded = $this->_checkAllguestdocumentsUploaded($userData);
       // $alldocumentUploaded = $this->_checkAlldocumentsUploaded($userData);
        if ($alldocumentUploaded) {
            $user_token = md5($this->_generateRandomString() . '_' . time());
            $usersTable = TableRegistry::get('Users');
            $usersTable->query()->update()->set(['doc_uploaded' => 1, 'user_token' => $user_token])->where(['id' => $userId])->execute();
            $url = Router::url(['action' => 'verifyEmail', 'token' => $user_token, 'mask' => base64_encode($userId)], true);
            $emailData = $usersTable->find('all')->where(['id' => $userId])->hydrate(false)->first();
            $response['error'] = 2;
           // $response['data'] = 'You have been successfully registered. Please check your email to proceed with verification process.';
           $response['data'] = 'Please click on the link sent on your email id for account verification.';
           $this->getMailer('User')->send('emailVerification', [$emailData, $url]);
        }
        $this->_jsonResponse($response);
    }

    public function verifyEmail() {
        $userId = isset($this->request->query['mask']) && $this->request->query['mask'] ? base64_decode($this->request->query['mask']) : '';
        $userToken = $this->request->query['token'];
        if (!empty($userId) && !empty($userToken)) {
            $user = $this->Users->find('all')->where(['id' => $userId])->first();
            if (!$user->id) {
                $this->Flash->error('Invalid URL.');
                return $this->redirect(['controller' => 'users', 'action' => 'index','prefix' => 'guest']);
            } else if ($user->user_token) {
                $updateStatus = $this->Users->query()->update()->set(['status' => 1, 'user_token' => ''])->where(['id' => $user->id])->execute();
                if ($updateStatus) {
                    $this->Flash->success('Your email id has been successfully verified. you can access your dashboard');
                    return $this->redirect(['controller' => 'users', 'action' => 'index','prefix' => 'guest']);
                }
            } else {
                $this->Flash->error('Email account is already verified.');
                return $this->redirect(['controller' => 'users', 'action' => 'index','prefix' => 'guest']);
            }
        }
    }

    public function markNotApplicable() {
        $this->autoRender = false;
//        $this->_ajaxCheck();
        $userId = $this->Auth->user('id');
        $userData = $this->_getUserData($userId);
        $field = $this->request->data['name'];
        $response['error'] = 1;
        if ($field) {
            $userDocumentsTable = TableRegistry::get('UserDocuments');
            $userDocumentEntity = $userDocumentsTable->newEntity(['user_id' => $userId, 'field' => $field, 'document' => '']);
            $userDocumentsTable->save($userDocumentEntity);
            $response['error'] = 0;
            $response['file'] = $field;
        }
       // $alldocumentUploaded = $this->_checkAlldocumentsUploaded($userData);
       $alldocumentUploaded = $this->_checkAllguestdocumentsUploaded($userData);
        if ($alldocumentUploaded) {
            $user_token = md5($this->_generateRandomString() . '_' . time());
            $usersTable = TableRegistry::get('Users');
            $usersTable->query()->update()->set(['doc_uploaded' => 1, 'user_token' => $user_token])->where(['id' => $userId])->execute();
            $url = Router::url(['action' => 'verifyEmail', 'token' => $user_token, 'mask' => base64_encode($userId)], true);
            $emailData = $usersTable->find('all')->where(['id' => $userId])->hydrate(false)->first();
            $response['error'] = 2;
            $response['data'] = 'You have been successfully registered. Please check your email to proceed with verification process.';
            $this->getMailer('User')->send('emailVerification', [$emailData, $url]);
        }
        $this->_jsonResponse($response);
    }

    public function register() {
        $this->_ajaxCheck();
        $post = $this->Users->newEntity();
        $profile = TableRegistry::get('UserProfiles');
        $postProfile = $profile->newEntity();
        $response['error'] = 0;
        $response['redirect'] = Router::url(['controller' => 'Users', 'action' => 'uploadDocuments'], true);
        $directorsErrors = [];
        $allDirectors = [];
        /* Registration */
        $this->__validateAdditionalDirectors($this->request->data, $directorsErrors);
        if ($this->request->is('post')) {
            $vendorSaved = null;
            $post = $this->Users->patchEntity($post, $this->request->data, ['validate' => 'GuestInfo']);
            $post->id = $this->Auth->user('id');
            if (!count($directorsErrors)) {
                $vendorSaved = $this->Users->save($post);
            }
            if ($vendorSaved) {
                $representativeId = $this->request->data['representative_id'];
                if (in_array($representativeId, [1, 2, 3, 4, 5, 6])) {
                    if ($this->request->data['address_proof']['size']) {
                        $ext = pathinfo($this->request->data['address_proof']['name'], PATHINFO_EXTENSION);
                        $fileName = time() . '_' . $this->_generateRandomString(20) . '_address_proof.' . $ext;
                        $uploadFile = WWW_ROOT . 'uploads/register_documents/' . $fileName;
                        if (move_uploaded_file($this->request->data['address_proof']['tmp_name'], $uploadFile)) {
                            $this->request->data['address_proof'] = $fileName;
                        }
                    }
                } else {
                    $this->request->data['address_proof'] = '';
                }
                if ($this->request->data['representative_id'] == 5) {
                    if (isset($this->request->data['directors_name'])) {
                        $directorsName = $this->request->data['directors_name'];
                        foreach ($directorsName as $director => $director_name) {
                            $allDirectors[] = [
                                'director_name' => $director_name,
                                'director_user_id' => $vendorSaved->id,
                                'din' => $this->request->data['dins'][$director]
                            ];
                        }
                    }
                    if (count($allDirectors) > 0) {
                        $directors = TableRegistry::get('UserDirectors');
                        $postDirectors = $directors->newEntities($allDirectors);
                        $directors->saveMany($postDirectors);
                    }
                } else {
                    $this->request->data['director_name'] = '';
                }
                $postProfile = $profile->patchEntity($postProfile, $this->request->data);
                $postProfile->user_profile_id = $vendorSaved->id;
                $profile->save($postProfile);
                $response['error'] = 0;
            } else {
                $response['error'] = 1;
                $response['data'] = $post->errors();
                if (count($directorsErrors)) {
                    $response['data'] = array_merge($response['data'], $directorsErrors);
                }
            }
        }
        $this->set(compact('response'));
        $this->set(compact('post'));
        $this->set('_serialize', 'response');
    }

    private function __validateAdditionalDirectors($data, &$directorsErrors) {
        if (isset($data['dins']) && is_array($data['dins'])) {
            foreach ($data['dins'] as $key => $din) {
                if (!trim($din)) {
                    $directorsErrors['dins[' . $key . ']'] = ['_empty' => 'Din Number is required'];
                }
            }
        }
        if (isset($data['directors_name']) && is_array($data['directors_name'])) {
            foreach ($data['directors_name'] as $key => $directorName) {
                if (!trim($directorName)) {
                    $directorsErrors['directors_name[' . $key . ']'] = ['_empty' => 'Director name is required'];
                }
            }
        }
    }
    
    public function profile() {
        $userId = $this->Auth->user('id');
        $this->viewBuilder()->layout('dashboard');
        $userData = $this->_getUserProfileDetail($userId);
        $userDirectors = $this->_getUsersDirectors(array($userId));
        $userDocumentsArr = $this->_getUsersDocumentsbyIds(array($userId));
        $userDocuments = reset($userDocumentsArr);
        $this->set(compact('userData','userDirectors','userDocuments'));
    }

    public function logout() {
        $this->Flash->error(__('You are successfully Logged out'));
        return $this->redirect($this->Auth->logout());
    }

}
