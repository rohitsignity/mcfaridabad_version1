<?php

namespace App\Controller\Guest;

use App\Controller\Guest\AppController;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use App\Traits\Users;
use App\Traits\Devices;
use Cake\Mailer\MailerAwareTrait;

/**
 * CompanyDevices Controller
 *
 * @property \App\Model\Table\CompanyDevicesTable $CompanyDevices
 */
class CompanyDevicesController extends AppController {

    private $placesAPI = 'https://maps.googleapis.com/maps/api/place/textsearch/json?';
    private $placesKey = 'AIzaSyD-V-cjIUZu6DPHpERzLBIV5OnR_AxkRtw';
    /// Initializing pagination settings
    public $paginate = [
        'limit' => 10,
        'order' => [
            'companyDevices.id' => 'desc'
        ]
    ];

    use Users,
        Devices,
        MailerAwareTrait;

    public function initialize() {
        parent::initialize();
        $this->viewBuilder()->layout('dashboard');
    }

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $userData = $this->_getUserData($this->Auth->user('id'));
        if ($userData['status'] == 2) {
            $this->Flash->error('Your profile has been rejected by administrator. Please register again or contact MCG support.');
            return $this->redirect($this->Auth->logout());
        }
        if(!$userData['doc_uploaded']) {
           return $this->redirect(['controller' => 'Users', 'action' => 'profileCompletion']);
        }
        if($userData['email'] && $userData['is_token']) {
            return $this->redirect(['controller' => 'Dashboard','action' => 'index']);
        }
    }

    public function index() {
        $registerUrl = Router::url(["controller" => "CompanyDevices", "action" => "register"]);
        $this->set(compact('registerUrl'));
    }

    public function register() {
        $categories = [];
        $categoriesTable = TableRegistry::get('Categories');
        $categoriesData = $categoriesTable->query()->select(['id', 'title'])->where(['parent_id' => 0,'status' => '1','guest_enabled' => 1])->hydrate(false);
        foreach ($categoriesData->toArray() as $categoryItem) {
            $categories[$categoryItem['id']] = $categoryItem['title'];
        }
        $this->set(compact('categories'));
    }

    public function getCategories() {
        if (!$this->request->is('ajax')) {
            exit("You are not allowed to access this page");
        }
        $categoriesTable = TableRegistry::get('Categories');
        $type = $this->request->data['deviceType'];
        $categoriesData = $categoriesTable->query()->select(['id', 'title'])->where(['parent_id' => 0, 'FIND_IN_SET("' . $type . '",type)'])->hydrate(false);
        $response = $categoriesData->toArray();
        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }

    public function getSubCategory() {
        if (!$this->request->is('ajax')) {
            exit("You are not allowed to access this page");
        }
        $response = [];
        $parent_id = $this->request->data['id'];
        if ($parent_id) {
            $categoriesTable = TableRegistry::get('Categories');
            $categoriesData = $categoriesTable->query()->select(['id', 'title'])->where(['parent_id' => $parent_id])->hydrate(false);
            $response = $categoriesData->toArray();
        }
        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }

    public function getFields() {
        if (!$this->request->is('ajax')) {
            exit("You are not allowed to access this page");
        }
        $categoryId = $this->request->data['id'];
        $categoryFieldsTable = TableRegistry::get('categoryFields');
        $data = $categoryFieldsTable->find()->select(['field_id', 'custom_validation', 'custom_disable', 'custom_calculations', 'lable' => 'f.lable', 'field_key' => 'f.field_key', 'type' => 'f.type', 'options' => 'f.options', 'default_validations' => 'f.default_validations', 'default_calculations' => 'f.default_calculations', 'group_in' => 'f.group_in', 'info' => 'f.info', 'is_disabled' => 'f.is_disabled'])->join([
                    'table' => 'fields',
                    'alias' => 'f',
                    'type' => 'LEFT',
                    'conditions' => 'categoryFields.field_id = f.id',
                ])->where(['category_id' => $categoryId])->order(['sort_order' => 'ASC'])->hydrate(false);
        $response = $data->toArray();
        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }

    public function getLocation() {
        if (!$this->request->is('ajax')) {
            exit("You are not allowed to access this page");
        }
        $response = [];
        $keyword = $this->request->query('s') . ', gurgaon, gurugram';
        if ($keyword) {
            $places = json_decode(file_get_contents($this->placesAPI . 'key=' . $this->placesKey . '&query=' . urlencode($keyword)), true);
            $response = array_map(function($arr) {
                return array(
                    'label' => $arr['formatted_address'],
                    'value' => $arr['formatted_address'],
                    'data' => $arr['geometry']
                );
            }, $places['results']);
        }
        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }

    public function saveDevice() {
        if (!$this->request->is('post')) {
            exit('You are not allowd to access this page');
        }
        $deviceFieldData = [];
        $companyCode = $this->Auth->user('company_code');
        $userId = $this->Auth->user('id');
        $categoryId = $this->request->data['category_id'];
        $subcategory = $this->request->data['subcategory'];
        unset($this->request->data['device_name']);
        unset($this->request->data['category_id']);
        unset($this->request->data['subcategory']);
        unset($this->request->data['devive_type']);
        $companyDevicesTable = TableRegistry::get('CompanyDevices');
        $companyDevice = $companyDevicesTable->newEntity();
        $companyDevice->user_id = $userId;
        $companyDevice->company_code = $companyCode;
        $companyDevice->category_id = $categoryId;
        $companyDevice->sub_category_id = $subcategory;
        if ($categoryId == 10) {
            $this->request->data['building_type'] = implode(', ', $this->request->data['building_type']);
            $this->request->data['total_advertising_space'] = 0;
            if (isset($this->request->data['omd_one_roof_omd_type'])) {
                $omdOneRoofData = [];
                $omdOneRoofCounter = 0;
                $omdOneRoofData['licence_fees'] = $this->request->data['omd_one_roof_licence_fees'];
                $omdOneRoofData['area'] = $this->request->data['omd_one_roof_total_area'];
                $omdOneRoofData['annual_fees'] = $this->request->data['omd_one_roof_total_annual_fees'];
                foreach ($this->request->data['omd_one_roof_omd_type'] as $omdOneRoofKey => $omdOneRoofItem) {
                    $omdOneRoofData['data'][$omdOneRoofCounter] = [
                        'type' => $omdOneRoofItem,
                        'height' => $this->request->data['omd_one_roof_height'][$omdOneRoofKey],
                        'width' => $this->request->data['omd_one_roof_width'][$omdOneRoofKey],
                        'area' => $this->request->data['omd_one_roof_area'][$omdOneRoofKey],
                    ];
                    $omdOneRoofCounter++;
                }
                unset($this->request->data['omd_one_roof_licence_fees']);
                unset($this->request->data['omd_one_roof_total_area']);
                unset($this->request->data['omd_one_roof_total_annual_fees']);
                unset($this->request->data['omd_one_roof_omd_type']);
                unset($this->request->data['omd_one_roof_height']);
                unset($this->request->data['omd_one_roof_width']);
                unset($this->request->data['omd_one_roof_area']);
                $omdRoofAreas = array_sum(array_map(function($arr) {
                            return $arr['area'];
                        }, $omdOneRoofData['data']));
                $this->request->data['total_advertising_space'] += $omdRoofAreas;
                $this->request->data['omd_one_roof'] = json_encode($omdOneRoofData);
            }
            if (isset($this->request->data['omd_premises_omd_type'])) {
                $omdPremisesCounter = 0;
                $omdPremisesData = [];
                $omdPremisesData['licence_fees'] = $this->request->data['omd_premises_licence_fees'];
                $omdPremisesData['area'] = $this->request->data['omd_premises_total_area'];
                $omdPremisesData['annual_fees'] = $this->request->data['omd_premises_total_annual_fees'];
                foreach ($this->request->data['omd_premises_omd_type'] as $omdPremisesKey => $omdPremisesItem) {
                    $omdPremisesData['data'][$omdPremisesCounter] = [
                        'type' => $omdPremisesItem,
                        'height' => $this->request->data['omd_premises_height'][$omdPremisesKey],
                        'width' => $this->request->data['omd_premises_width'][$omdPremisesKey],
                        'area' => $this->request->data['omd_premises_area'][$omdPremisesKey],
                    ];
                    $omdPremisesCounter++;
                }
                unset($this->request->data['omd_premises_licence_fees']);
                unset($this->request->data['omd_premises_total_area']);
                unset($this->request->data['omd_premises_total_annual_fees']);
                unset($this->request->data['omd_premises_omd_type']);
                unset($this->request->data['omd_premises_height']);
                unset($this->request->data['omd_premises_width']);
                unset($this->request->data['omd_premises_area']);
                $omdPremisesAreas = array_sum(array_map(function($arr) {
                            return $arr['area'];
                        }, $omdPremisesData['data']));
                $this->request->data['total_advertising_space'] += $omdPremisesAreas;
                $this->request->data['omd_premises'] = json_encode($omdPremisesData);
            }
        }

        if ($subcategory == 17) {
            $metroData = [];
            $metroData['metro_mrts_categories'] = $this->request->data['metro_mrts_categories'];
            if (strtolower($this->request->data['metro_mrts_categories']) == 'rolling stock') {
                foreach ($this->request->data['metro_type'] as $metroKey => $metro_type) {
                    if ($metro_type == 'inside_metro') {
                        $mertoCounter = 0;
                        $metroData['data'][$metroKey]['metro_type'] = 'Inside Metro';
                        foreach ($this->request->data['inside_metro_train'] as $key => $insideMetroTrain) {
                            $metroData['data'][$metroKey]['table'][$mertoCounter]['train_no'] = $insideMetroTrain;
                            $metroData['data'][$metroKey]['table'][$mertoCounter]['coches'] = $this->request->data['inside_metro_coches'][$key];
                            $metroData['data'][$metroKey]['table'][$mertoCounter]['ad_panels'] = $this->request->data['inside_metro_ad_panels'][$key];
                            $mertoCounter++;
                        }
                        unset($this->request->data['inside_metro_train'], $this->request->data['inside_metro_coches'], $this->request->data['inside_metro_ad_panels']);
                    }
                    if ($metro_type == 'outside_metro') {
                        $mertoCounter = 0;
                        $metroData['data'][$metroKey]['metro_type'] = 'Outside Metro';
                        foreach ($this->request->data['outside_metro_train'] as $key => $outside_metro_train) {
                            $metroData['data'][$metroKey]['table'][$mertoCounter]['train_no'] = $outside_metro_train;
                            $metroData['data'][$metroKey]['table'][$mertoCounter]['coches'] = $this->request->data['outside_metro_coches'][$key];
                            $mertoCounter++;
                        }
                        unset($this->request->data['outside_metro_train'], $this->request->data['outside_metro_coches']);
                    }
                }
                unset($this->request->data['metro_type'], $this->request->data['lat'], $this->request->data['lng'], $this->request->data['location']);
            }
            if (strtolower($this->request->data['metro_mrts_categories']) == 'inside station') {
                $mertoCounter = 0;
                $metroData['total_area'] = $this->request->data['omd_one_roof_total_area'];
                foreach ($this->request->data['inside_station_omd_type'] as $key => $insideStationOmdType) {
                    $metroData['data'][$mertoCounter]['omd_type'] = $insideStationOmdType;
                    $metroData['data'][$mertoCounter]['omd_height'] = $this->request->data['inside_station_omd_height'][$key];
                    $metroData['data'][$mertoCounter]['omd_width'] = $this->request->data['inside_station_omd_width'][$key];
                    $metroData['data'][$mertoCounter]['omd_area'] = $this->request->data['inside_station_omd_area'][$key];
                    $mertoCounter++;
                }
                unset($this->request->data['inside_station_omd_type'], $this->request->data['inside_station_omd_height'], $this->request->data['inside_station_omd_width'], $this->request->data['inside_station_omd_area'], $this->request->data['omd_one_roof_total_area']);
            }
            if (strtolower($this->request->data['metro_mrts_categories']) == 'station branding') {
                $mertoCounter = 0;
                foreach ($this->request->data['station_branding_total_facade'] as $key => $total_facade) {
                    $metroData['data'][$mertoCounter]['facade'] = 'F' . ($mertoCounter + 1);
                    $metroData['data'][$mertoCounter]['total_facade'] = $total_facade;
                    $metroData['data'][$mertoCounter]['actual_facade'] = $this->request->data['station_branding_actual_facade'][$key];
                    $mertoCounter++;
                }
                unset($this->request->data['station_branding_total_facade'], $this->request->data['station_branding_actual_facade']);
            }
            $this->request->data['metro_table'] = json_encode($metroData);
        }
        if ($subcategory == 11) {
            $shelterData = [];
            $counter = 0;
            foreach ($this->request->data['shelter_location'] as $key => $shelter_location) {
                $shelterData[$counter]['location'] = $shelter_location;
                $shelterData[$counter]['panels'] = $this->request->data['shelter_panels'][$key];
                $shelterData[$counter]['area'] = $this->request->data['shelter_area'][$key];
                $shelterData[$counter]['lat'] = $this->request->data['shelter_lat'][$key];
                $shelterData[$counter]['lng'] = $this->request->data['shelter_lng'][$key];
                $counter++;
            }
            unset($this->request->data['shelter_location'], $this->request->data['shelter_panels'], $this->request->data['shelter_area'], $this->request->data['shelter_lat'], $this->request->data['shelter_lng']);
            $this->request->data['shelter_data'] = json_encode($shelterData);
        }
        if ($subcategory == 12) {
            $routeMakerData = [];
            $counter = 0;
            foreach ($this->request->data['route_markers_location'] as $key => $route_markers_location) {
                $routeMakerData[$counter]['location'] = $route_markers_location;
                $routeMakerData[$counter]['device'] = $this->request->data['route_markers_devices'][$key];
                $routeMakerData[$counter]['area'] = $this->request->data['route_markers_area'][$key];
                $routeMakerData[$counter]['lat'] = $this->request->data['route_markers_lat'][$key];
                $routeMakerData[$counter]['lng'] = $this->request->data['route_markers_lng'][$key];
                $counter++;
            }
            unset($this->request->data['route_markers_location'], $this->request->data['route_markers_devices'], $this->request->data['route_markers_area'], $this->request->data['route_markers_lat'], $this->request->data['route_markers_lng']);
            $this->request->data['route_markers_data'] = json_encode($routeMakerData);
        }
        if ($subcategory == 13) {
            $footoverData = [];
            $counter = 0;
            foreach ($this->request->data['foot_over_location'] as $key => $route_markers_location) {
                $footoverData[$counter]['location'] = $route_markers_location;
                $footoverData[$counter]['panels'] = $this->request->data['foot_over_panels'][$key];
                $footoverData[$counter]['area'] = $this->request->data['foot_over_area'][$key];
                $footoverData[$counter]['lat'] = $this->request->data['foot_over_lat'][$key];
                $footoverData[$counter]['lng'] = $this->request->data['foot_over_lng'][$key];
                $counter++;
            }
            $this->request->data['foot_over_data'] = json_encode($footoverData);
            unset($this->request->data['foot_over_location'], $this->request->data['foot_over_panels'], $this->request->data['foot_over_area'], $this->request->data['foot_over_lat'], $this->request->data['foot_over_lng']);
        }
        if ($subcategory == 14) {
            $cycleStation = [];
            $counter = 0;
            foreach ($this->request->data['cycle_station_location'] as $key => $location) {
                $cycleStation[$counter]['location'] = $location;
                $cycleStation[$counter]['panels'] = $this->request->data['cycle_station_panels'][$key];
                $cycleStation[$counter]['area'] = $this->request->data['cycle_station_area'][$key];
                $cycleStation[$counter]['lat'] = $this->request->data['cycle_station_lat'][$key];
                $cycleStation[$counter]['lng'] = $this->request->data['cycle_station_lng'][$key];
                $counter++;
            }
            $this->request->data['cycle_station_data'] = json_encode($cycleStation);
            unset($this->request->data['cycle_station_location'], $this->request->data['cycle_station_panels'], $this->request->data['cycle_station_area'], $this->request->data['cycle_station_lat'], $this->request->data['cycle_station_lng']);
        }
        if ($subcategory == 15) {
            $policeBooth = [];
            $counter = 0;
            foreach ($this->request->data['police_booth_location'] as $key => $location) {
                $policeBooth[$counter]['location'] = $location;
                $policeBooth[$counter]['panels'] = $this->request->data['police_booth_panels'][$key];
                $policeBooth[$counter]['area'] = $this->request->data['police_booth_area'][$key];
                $policeBooth[$counter]['lat'] = $this->request->data['police_booth_lat'][$key];
                $policeBooth[$counter]['lng'] = $this->request->data['police_booth_lng'][$key];
                $counter++;
            }
            $this->request->data['police_booth_data'] = json_encode($policeBooth);
            unset($this->request->data['police_booth_location'], $this->request->data['police_booth_panels'], $this->request->data['police_booth_area'], $this->request->data['police_booth_lat'], $this->request->data['police_booth_lng']);
        }
        if ($subcategory == 16) {
            $sittingBench = [];
            $counter = 0;
            foreach ($this->request->data['sitting_bench_location'] as $key => $location) {
                $sittingBench[$counter]['location'] = $location;
                $sittingBench[$counter]['panels'] = $this->request->data['sitting_bench_panels'][$key];
                $sittingBench[$counter]['area'] = $this->request->data['sitting_bench_area'][$key];
                $sittingBench[$counter]['lat'] = $this->request->data['sitting_bench_lat'][$key];
                $sittingBench[$counter]['lng'] = $this->request->data['sitting_bench_lng'][$key];
                $counter++;
            }
            $this->request->data['sitting_bench_data'] = json_encode($sittingBench);
            unset($this->request->data['sitting_bench_location'], $this->request->data['sitting_bench_panels'], $this->request->data['sitting_bench_area'], $this->request->data['sitting_bench_lat'], $this->request->data['sitting_bench_lng']);
        }
        if($subcategory == 32) {
            $otherTypeA = [];
            $counter = 0;
            foreach ($this->request->data['other_type_a_location'] as $key => $location) {
                $otherTypeA[$counter]['location'] = $location;
                $otherTypeA[$counter]['height'] = $this->request->data['other_type_a_height'][$key];
                $otherTypeA[$counter]['width'] = $this->request->data['other_type_a_width'][$key];
                $otherTypeA[$counter]['area'] = $this->request->data['other_type_a_area'][$key];
                $otherTypeA[$counter]['lat'] = $this->request->data['other_type_a_lat'][$key];
                $otherTypeA[$counter]['lng'] = $this->request->data['other_type_a_lng'][$key];
                $counter++;
            }
            $this->request->data['other_type_a_data'] = json_encode($otherTypeA);
            unset($this->request->data['other_type_a_location'], $this->request->data['other_type_a_height'], $this->request->data['other_type_a_width'], $this->request->data['other_type_a_area'], $this->request->data['other_type_a_lat'], $this->request->data['other_type_a_lng']);
        }
        if($subcategory == 24) {
            $companyDevice->paid = '1';
        }
        if ($companyDevicesTable->save($companyDevice)) {
            foreach ($this->request->data as $key => $item) {
                if (is_array($item) && $item['error'] == 0) {
                    $ext = pathinfo($this->request->data[$key]['name'], PATHINFO_EXTENSION);
                    $fileName = time() . '_' . $this->_generateRandomString(20) . '.' . $ext;
                    $uploadFile = WWW_ROOT . 'uploads/devices/' . $fileName;
                    if (move_uploaded_file($this->request->data[$key]['tmp_name'], $uploadFile)) {
                        $item = $fileName;
                    }
                }
                $deviceFieldData[] = [
                    'company_device_id' => $companyDevice->id,
                    'field_key' => $key,
                    'input_value' => $item,
                ];
            }
            $companyDeviceFieldsTable = TableRegistry::get('CompanyDeviceFields');
            $companyDeviceFields = $companyDeviceFieldsTable->newEntities($deviceFieldData);
            $companyDeviceFieldsTable->saveMany($companyDeviceFields);
            if($subcategory == 24) {
                $this->Flash->success(__('Your application has been successfully recieved and under appoval process.'));
                return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
            }
            $paymentTable = TableRegistry::get('Payments');
            $amount = 1000;
            if ($categoryId == 10) {
                $amount = 5000;
            }
            $paymentId = time() . $this->Auth->user('id');
            $paymentData = [
                'user_id' => $this->Auth->user('id'),
                'payment_id' => $paymentId,
                'payment_type' => 'device_registration',
                'type_id' => $companyDevice->id,
                'amount' => $amount
            ];
            $paymentEntity = $paymentTable->newEntity($paymentData);
            $paymentTable->save($paymentEntity);
            $this->loadComponent('Ccavenue');
            $redirectionUrl = Router::url(['controller' => 'Payment', 'action' => 'response'], true);
            $url = $this->Ccavenue->getUrl();
            $request = $this->Ccavenue->getPaymentdata($paymentId, $amount, $redirectionUrl);
            $accessCode = $this->Ccavenue->getAccessCode();
            $this->set(compact('url', 'accessCode', 'request'));
        }
    }

    public function response() {
        $this->autoRender = false;
        $this->loadComponent('Insta');
        $paymentId = $this->request->query('payment_id');
        $paymentRequestId = $this->request->query('payment_request_id');
        $paymentDataJson = file_get_contents(WWW_ROOT . 'uploads/devices_session/' . $paymentRequestId . '.txt');
        $paymentData = json_decode($paymentDataJson, true);
        $deviceId = $paymentData['deviceId'];
        $userId = $paymentData['user']['id'];
        $paymentDetail = $this->Insta->paymentDetail($paymentId);
        if ($paymentDetail['status'] == 'Credit') {
            $this->loadComponent('Pdf');
            $this->CompanyDevices->query()->update()->set(['paid' => 1])->where(['id' => $paymentData['deviceId']])->execute();
            $paymentTable = TableRegistry::get('Payments');
            $paymentData = [
                'user_id' => $paymentData['user']['id'],
                'payment_id' => $paymentDetail['payment_id'],
                'payment_request_id' => $paymentRequestId,
                'quantity' => $paymentDetail['quantity'],
                'status' => $paymentDetail['status'],
                'buyer_name' => $paymentDetail['buyer_name'],
                'buyer_phone' => $paymentDetail['buyer_phone'],
                'buyer_email' => $paymentDetail['buyer_email'],
                'currency' => $paymentDetail['currency'],
                'unit_price' => $paymentDetail['unit_price'],
                'amount' => $paymentDetail['amount'],
                'fees' => $paymentDetail['fees'],
                'created_at' => date('Y-m-d H:i:s', strtotime($paymentDetail['created_at'])),
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ];
            $pdfData = [
                'amount' => $paymentDetail['amount'],
                'id' => $deviceId,
                'user_id' => $userId,
                'path' => 'device_registration'
            ];
            $session = $this->request->session();
            $session->write('Registration.receipt', Router::url('/uploads/receipt/device_registration/device_registration_' . $deviceId . '_' . $userId . '.pdf'));
            $this->Pdf->generateReceipt($pdfData);
            $this->getMailer('User')->send('deviceRegistrationReceipt', [json_decode($paymentDataJson)]);
            $paymentTable->query()->insert(['user_id', 'payment_id', 'payment_request_id', 'quantity', 'status', 'buyer_name', 'buyer_phone', 'buyer_email', 'currency', 'unit_price', 'amount', 'fees', 'created_at', 'created', 'modified'])->values($paymentData)->execute();
            $this->Flash->success(__('New Device have been successfully registered.'));
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }
        die;
    }

    public function applicationStatus() {

        //// Prepare categories array ------------------------------------------------------------------------------
        $categories = [];
        $categoriesTable = TableRegistry::get('Categories');
        $categoriesData = $categoriesTable->query()->select(['id', 'title'])->where(['parent_id' => 0])->hydrate(false);
        foreach ($categoriesData->toArray() as $categoryItem) {
            $categories[$categoryItem['id']] = $categoryItem['title'];
        }

        //// Prepare devices array ----------------------------------------------------------------------------------
        $devices = $this->CompanyDevices->find('all')->select(['companyDevices_id' => 'CompanyDevices.id', 'device_name' => 'CompanyDevices.device_name','sub_category_id' => 'CompanyDevices.sub_category_id', 'companyDevices_status' => 'CompanyDevices.status', 'companyDeviceFields_fieldData' => 'companyDeviceFields.fieldData','payment_id' => 'omdPayment.payment_id', 'created' => 'CompanyDevices.created', 'modified' => 'lastComment.modified'])->join([
                    'table' => "(SELECT `company_device_id`, GROUP_CONCAT(CONCAT(`field_key`,'=',`input_value`) SEPARATOR '#@!') AS fieldData FROM `company_device_fields` GROUP BY `company_device_id`)",
                    'alias' => 'companyDeviceFields',
                    'type' => 'LEFT',
                    'conditions' => 'CompanyDevices.id = companyDeviceFields.company_device_id',
                ])->join([
                    'table' => '(SELECT * FROM (SELECT * FROM `mcg_comments` ORDER BY `id` DESC ) AS tbl GROUP BY `device_id`)',
                    'alias' => 'lastComment',
                    'type' => 'LEFT',
                    'conditions' => 'CompanyDevices.id = lastComment.device_id'
                ])->join([
                    'table' => '(SELECT * FROM (SELECT * FROM `omd_payments` ORDER BY `id` DESC) tbl GROUP BY `device_id`)',
                    'alias' => 'omdPayment',
                    'type' => 'LEFT',
                    'conditions' => 'CompanyDevices.id = omdPayment.device_id'
                ])->where(['CompanyDevices.user_id' => $this->user['id'], 'CompanyDevices.paid' => '1'])->order(['CompanyDevices.id' => 'DESC']);


        //// Handling search request ------------------------------------------------------------------------------
        if ($this->request->is('post')) {
            $categoryId = $this->request->data['category_id'];
            $subcategory = $this->request->data['subcategory'];
            $deviceId = $this->request->data['device_id'];

            if (!empty($deviceId)) {
                $devices = $devices->andWhere(['id' => $deviceId]);
            } elseif (!empty($categoryId) || !empty($subcategory)) {

                if ($categoryId) {
                    $devices = $devices->andWhere(['category_id' => $categoryId]);
                }

                if ($subcategory) {
                    $devices = $devices->andWhere(['sub_category_id' => $subcategory]);
                }
            }
        }

        $devices = $this->paginate($devices->hydrate(false))->toArray();

        foreach ($devices as $deviceKey => $device) {
            $deviceData = [];
            $tempDevice = explode('#@!', $device['companyDeviceFields_fieldData']);
            array_walk($tempDevice, function($val) use(&$deviceData) {
                $valArr = explode('=', $val);
                $deviceData[] = $valArr;
            });

            $feesArr = array_values(array_filter($deviceData, function($arr) {
                        return $arr[0] === 'annual_license_fees';
                    }));
            if (!$feesArr) {
                $feesArr = array_values(array_filter($deviceData, function($arr) {
                            return $arr[0] === 'concession_fees';
                        }));
            }
            if (!$feesArr) {
                $feesArr = array_values(array_filter($deviceData, function($arr) {
                            return $arr[0] === 'total_omd_fees';
                        }));
            }

            foreach ($tempDevice as $singleTempDevice) {
                $deviceFields = explode('=', $singleTempDevice);
                if (in_array($deviceFields[0], ['lat', 'lng', 'location', 'concession_fees'])) {
                    $devices[$deviceKey]['companyDevice_' . $deviceFields[0]] = $deviceFields[1];
                }
            }
            $devices[$deviceKey]['fees'] = count($feesArr) ? $feesArr[0][1] : 0;
            $devices[$deviceKey]['pay_status'] = 0;
            if ($device['companyDevices_status'] == 'approved') {
                if (date('Y-m-d H:i:s') < date('Y-m-d H:i:s', strtotime($device['modified'] . ' + 7 days'))) {
                    $devices[$deviceKey]['pay_status'] = 1;
                } else if($device['payment_id']) {
                    $devices[$deviceKey]['pay_status'] = 1;
                } else {
                    $devices[$deviceKey]['pay_status'] = 2;
                }
            }
        }
        $docPath = Router::url('/uploads/omd_letters/');
        $docPathPermission = Router::url('/uploads/permission_docs/');
        //// Setting Vars -----------------------------------------------------------------------------------------
        $this->set(compact('categories', 'docPath','docPathPermission'));
        $this->set(compact('devices'));
        if (isset($subcategory)) {
            $this->set(compact('subcategory'));
        }
    }

    public function device($deviceId = null) {
        if (!$deviceId) {
            return $this->redirect(['action' => 'applicationStatus']);
        }
        $devideData = $this->_getDeviceData($deviceId);
        if ($devideData['user_id'] != $this->Auth->user('id')) {
            $this->Flash->error('You are not allowed to access this URL.');
            return $this->redirect(['action' => 'applicationStatus']);
        }
        $this->set(compact('devideData'));
    }

    public function requestAdditionalDocs() {
        $data = $this->request->data;
        $additionaldocTable = TableRegistry::get('omd_additional_doc_requests');
        $lastRquest = $additionaldocTable->find('all')->where(['device_id' => $data['id']])->order(['id' => 'DESC'])->hydrate(false)->first();
        $checkadditionaldoc = $additionaldocTable->find('all', [
                    'order' => ['omd_additional_doc_requests.id' => 'DESC']
                ])->join([
                    'table' => 'omd_additional_docs',
                    'type' => 'LEFT',
                    'conditions' => 'omd_additional_docs.request_id = omd_additional_doc_requests.id',
                ])
                ->select(['id' => 'omd_additional_docs.id',
                    'doc_name' => 'omd_additional_docs.name',
                    'reason' => 'omd_additional_docs.reason',
                    'uploaded' => 'omd_additional_docs.is_uploaded'])
                ->where(['device_id' => $data['id'], 'omd_additional_docs.request_id' => $lastRquest['id']])
                ->hydrate(false)
                ->toArray();

        echo json_encode($checkadditionaldoc);
        die;
    }

    public function uploadDoc() {

        $this->autoRender = false;
        $response['error'] = 1;
        $documentName = '';
        $documentNameArr = array_keys($this->request->data);
        $userId = $this->Auth->user('id');
        if (isset($documentNameArr[0])) {
            $documentName = $documentNameArr[0];
        }
        $response['file'] = $documentName;
        if ($this->request->data[$documentName]['size']) {
            $ext = pathinfo($this->request->data[$documentName]['name'], PATHINFO_EXTENSION);
            $allowed = array('gif', 'jpeg', 'png', 'jpg', 'doc', 'docx', 'xls', 'xlsx', 'pdf');
            if (!in_array(strtolower($ext), $allowed)) {
                $response['error'] = 1;
                $response['file'] = $documentName;
                $response['data'] = 'These files extension are allowed: .png, .gif, .jpeg, .jpg, .doc, .docx, .xls, .xlsx, .pdf';
                $this->_jsonResponse($response);
            }
            $fileName = time() . '_' . $this->_generateRandomString(20) . '_' . $documentName . '.' . $ext;
            $uploadFile = WWW_ROOT . 'uploads/omd_additional_device_docs/' . $fileName;
            if (move_uploaded_file($this->request->data[$documentName]['tmp_name'], $uploadFile)) {
                $response['error'] = 0;
                $response['file'] = $documentName;
                $userDocumentsTable = TableRegistry::get('omd_additional_docs');
                $userDocumentEntity = $userDocumentsTable->newEntity(['is_uploaded' => '1', 'filename' => $fileName]);
                $userDocumentEntity->id = $documentName;
                $userDocumentsTable->save($userDocumentEntity);
            }
        }
        $userData = $this->_getUserData($userId);

        $additionaldocTable = TableRegistry::get('omd_additional_docs');
        $checkadditionaldoc = $additionaldocTable->find('all')->select(['request_id'])
                ->where(['id' => $documentName])
                ->hydrate(false)
                ->first();
        $request_id = reset($checkadditionaldoc);
        $check_count = $additionaldocTable->find('all')
                ->where(['request_id' => $request_id, 'is_uploaded' => '0'])
                ->hydrate(false)
                ->count();

        if ($check_count == 0) {
            $devicetable = TableRegistry::get('omd_additional_doc_requests');
            $ydeviceid = $devicetable->find('all')->select(['device_id'])
                    ->where(['id' => $request_id])
                    ->hydrate(false)
                    ->first();

            $deviceid = reset($ydeviceid);

            $companydevicetable = TableRegistry::get('company_devices');
            $query = $companydevicetable->query();
            $query->update()
                    ->set(['status' => 'under process'])
                    ->where(['id' => $deviceid])
                    ->execute();
        }

        $this->_jsonResponse($response);
    }

    public function resubmit($deviceId = null) {
        $categories = [];
        $subCategories = [];
        if (!$deviceId) {
            return $this->redirect(['action' => 'applicationStatus']);
        }
        $devideData = $this->_getDeviceData($deviceId);
        if ($devideData['status'] != 'resubmit') {
            $this->Flash->error("You are not allowed to access this URL.");
            return $this->redirect(['action' => 'applicationStatus']);
        }
        $fieldsData = array_map(function($arr) {
            if (in_array($arr['field'], ['omd_one_roof', 'omd_premises', 'metro_table', 'shelter_data', 'route_markers_data', 'foot_over_data', 'cycle_station_data', 'police_booth_data', 'sitting_bench_data','other_type_a_data'])) {
                $arr['value'] = json_decode($arr['value'], true);
            }
            return $arr;
        }, $devideData['fields']);
        if ($devideData['user_id'] != $this->Auth->user('id')) {
            $this->Flash->error('You are not allowed to access this URL.');
            return $this->redirect(['action' => 'applicationStatus']);
        }
        $deviceName = $devideData['device_name'];
        $subCategory = $devideData['sub_category_id'];
        $category = $devideData['category_id'];
        $categoriesTable = TableRegistry::get('Categories');
        $categoriesData = $categoriesTable->query()->select(['id', 'title', 'parent_id'])->where(['id IN' => [$devideData['category_id'], $devideData['sub_category_id']]])->hydrate(false);
        foreach ($categoriesData->toArray() as $categoryItem) {
            if (!$categoryItem['parent_id']) {
                $categories[$categoryItem['id']] = $categoryItem['title'];
            } else {
                $subCategories[$categoryItem['id']] = $categoryItem['title'];
            }
        }
        $mcgCommentsTable = TableRegistry::get('McgComments');
        $mcgComment = $mcgCommentsTable->find('all')->where(['device_id' => $deviceId])->order(['id' => 'DESC'])->hydrate(false)->first();
        $reason = $mcgComment['comment'];

        $this->set(compact('categories', 'subCategories', 'deviceName', 'subCategory', 'category', 'fieldsData', 'deviceId', 'reason'));
    }

    public function resubmitDevice($deviceId = null) {
        $this->autoRender = false;
        if (!$deviceId) {
            return $this->redirect(['action' => 'applicationStatus']);
        }
        $deviceFieldData = [];
        $companyCode = $this->Auth->user('company_code');
        $userId = $this->Auth->user('id');
        $categoryId = $this->request->data['category_id'];
        $subcategory = $this->request->data['subcategory'];
        unset($this->request->data['device_name']);
        unset($this->request->data['category_id']);
        unset($this->request->data['subcategory']);
        unset($this->request->data['devive_type']);
        $companyDevicesTable = TableRegistry::get('CompanyDevices');
        $companyDevice = $companyDevicesTable->newEntity();
        $companyDevice->user_id = $userId;
        $companyDevice->company_code = $companyCode;
        $companyDevice->category_id = $categoryId;
        $companyDevice->sub_category_id = $subcategory;
        $companyDevice->status = 'under process';
        $companyDevice->id = $deviceId;
        if ($categoryId == 10) {
            $deviceValues = $this->_getDeviceFieldValues($deviceId);
            $this->request->data['building_type'] = implode(', ', $this->request->data['building_type']);
            $this->request->data['total_advertising_space'] = 0;
            if (isset($this->request->data['omd_one_roof_omd_type'])) {
                $omdOneRoofData = [];
                $omdOneRoofCounter = 0;
                $omdOneRoofData['licence_fees'] = $this->request->data['omd_one_roof_licence_fees'];
                $omdOneRoofData['area'] = $this->request->data['omd_one_roof_total_area'];
                $omdOneRoofData['annual_fees'] = $this->request->data['omd_one_roof_total_annual_fees'];
                foreach ($this->request->data['omd_one_roof_omd_type'] as $omdOneRoofKey => $omdOneRoofItem) {
                    $omdOneRoofData['data'][$omdOneRoofCounter] = [
                        'type' => $omdOneRoofItem,
                        'height' => $this->request->data['omd_one_roof_height'][$omdOneRoofKey],
                        'width' => $this->request->data['omd_one_roof_width'][$omdOneRoofKey],
                        'area' => $this->request->data['omd_one_roof_area'][$omdOneRoofKey],
                    ];
                    $omdOneRoofCounter++;
                }
                unset($this->request->data['omd_one_roof_licence_fees']);
                unset($this->request->data['omd_one_roof_total_area']);
                unset($this->request->data['omd_one_roof_total_annual_fees']);
                unset($this->request->data['omd_one_roof_omd_type']);
                unset($this->request->data['omd_one_roof_height']);
                unset($this->request->data['omd_one_roof_width']);
                unset($this->request->data['omd_one_roof_area']);
                $omdOneRoofDataArr = $omdOneRoofData['data'];
                $omd_one_roofValues = array_values(array_filter($deviceValues, function($arr) {
                            return $arr['field_key'] == 'omd_one_roof';
                        }));
                $omd_one_roofValuesArr = json_decode($omd_one_roofValues[0]['input_value'], true);
                foreach ($omdOneRoofDataArr as $omdOneRoofDataKey => $omdOneRoofDataItem) {
                    $omdOneRoofData['data'][$omdOneRoofDataKey]['action'] = $omd_one_roofValuesArr['data'][$omdOneRoofDataKey]['action'] == 2 ? 0 : $omd_one_roofValuesArr['data'][$omdOneRoofDataKey]['action'];
                    $omdOneRoofData['data'][$omdOneRoofDataKey]['comment'] = $omd_one_roofValuesArr['data'][$omdOneRoofDataKey]['comment'];
                }
                $omdRoofAreas = array_sum(array_map(function($arr) {
                            if ($arr['action'] != 3) {
                                return $arr['area'];
                            }
                            return 0;
                        }, $omdOneRoofData['data']));
                $this->request->data['total_advertising_space'] += $omdRoofAreas;
                $this->request->data['omd_one_roof'] = json_encode($omdOneRoofData);
            }
            if (isset($this->request->data['omd_premises_omd_type'])) {
                $omdPremisesCounter = 0;
                $omdPremisesData = [];
                $omdPremisesData['licence_fees'] = $this->request->data['omd_premises_licence_fees'];
                $omdPremisesData['area'] = $this->request->data['omd_premises_total_area'];
                $omdPremisesData['annual_fees'] = $this->request->data['omd_premises_total_annual_fees'];
                foreach ($this->request->data['omd_premises_omd_type'] as $omdPremisesKey => $omdPremisesItem) {
                    $omdPremisesData['data'][$omdPremisesCounter] = [
                        'type' => $omdPremisesItem,
                        'height' => $this->request->data['omd_premises_height'][$omdPremisesKey],
                        'width' => $this->request->data['omd_premises_width'][$omdPremisesKey],
                        'area' => $this->request->data['omd_premises_area'][$omdPremisesKey],
                    ];
                    $omdPremisesCounter++;
                }
                unset($this->request->data['omd_premises_licence_fees']);
                unset($this->request->data['omd_premises_total_area']);
                unset($this->request->data['omd_premises_total_annual_fees']);
                unset($this->request->data['omd_premises_omd_type']);
                unset($this->request->data['omd_premises_height']);
                unset($this->request->data['omd_premises_width']);
                unset($this->request->data['omd_premises_area']);
                $omdPremisesDataArr = $omdPremisesData['data'];
                $omd_premisesValues = array_values(array_filter($deviceValues, function($arr) {
                            return $arr['field_key'] == 'omd_premises';
                        }));
                $omd_premisesValuesArr = json_decode($omd_premisesValues[0]['input_value'], true);
                foreach ($omdPremisesDataArr as $omdPremisesDataKey => $omdPremisesDataItem) {
                    $omdPremisesData['data'][$omdPremisesDataKey]['action'] = $omd_premisesValuesArr['data'][$omdPremisesDataKey]['action'] == 2 ? 0 : $omd_premisesValuesArr['data'][$omdPremisesDataKey]['action'];
                    $omdPremisesData['data'][$omdPremisesDataKey]['comment'] = $omd_premisesValuesArr['data'][$omdPremisesDataKey]['comment'];
                }
                $omdPremisesAreas = array_sum(array_map(function($arr) {
                            if ($arr['action'] != 3) {
                                return $arr['area'];
                            }
                            return 0;
                        }, $omdPremisesData['data']));
                $this->request->data['total_advertising_space'] += $omdPremisesAreas;
                $this->request->data['omd_premises'] = json_encode($omdPremisesData);
            }
        }
        if ($subcategory == 17) {
            $metroData = [];
            $metroData['metro_mrts_categories'] = $this->request->data['metro_mrts_categories'];
            if (strtolower($this->request->data['metro_mrts_categories']) == 'rolling stock') {
                foreach ($this->request->data['metro_type'] as $metroKey => $metro_type) {
                    if ($metro_type == 'inside_metro') {
                        $mertoCounter = 0;
                        $metroData['data'][$metroKey]['metro_type'] = 'Inside Metro';
                        foreach ($this->request->data['inside_metro_train'] as $key => $insideMetroTrain) {
                            $metroData['data'][$metroKey]['table'][$mertoCounter]['train_no'] = $insideMetroTrain;
                            $metroData['data'][$metroKey]['table'][$mertoCounter]['coches'] = $this->request->data['inside_metro_coches'][$key];
                            $metroData['data'][$metroKey]['table'][$mertoCounter]['ad_panels'] = $this->request->data['inside_metro_ad_panels'][$key];
                            $mertoCounter++;
                        }
                        unset($this->request->data['inside_metro_train'], $this->request->data['inside_metro_coches'], $this->request->data['inside_metro_ad_panels']);
                    }
                    if ($metro_type == 'outside_metro') {
                        $mertoCounter = 0;
                        $metroData['data'][$metroKey]['metro_type'] = 'Outside Metro';
                        foreach ($this->request->data['outside_metro_train'] as $key => $outside_metro_train) {
                            $metroData['data'][$metroKey]['table'][$mertoCounter]['train_no'] = $outside_metro_train;
                            $metroData['data'][$metroKey]['table'][$mertoCounter]['coches'] = $this->request->data['outside_metro_coches'][$key];
                            $mertoCounter++;
                        }
                        unset($this->request->data['outside_metro_train'], $this->request->data['outside_metro_coches']);
                    }
                }
                unset($this->request->data['metro_type'], $this->request->data['lat'], $this->request->data['lng'], $this->request->data['location']);
            }
            if (strtolower($this->request->data['metro_mrts_categories']) == 'inside station') {
                $mertoCounter = 0;
                $metroData['total_area'] = $this->request->data['omd_one_roof_total_area'];
                foreach ($this->request->data['inside_station_omd_type'] as $key => $insideStationOmdType) {
                    $metroData['data'][$mertoCounter]['omd_type'] = $insideStationOmdType;
                    $metroData['data'][$mertoCounter]['omd_height'] = $this->request->data['inside_station_omd_height'][$key];
                    $metroData['data'][$mertoCounter]['omd_width'] = $this->request->data['inside_station_omd_width'][$key];
                    $metroData['data'][$mertoCounter]['omd_area'] = $this->request->data['inside_station_omd_area'][$key];
                    $mertoCounter++;
                }
                unset($this->request->data['inside_station_omd_type'], $this->request->data['inside_station_omd_height'], $this->request->data['inside_station_omd_width'], $this->request->data['inside_station_omd_area'], $this->request->data['omd_one_roof_total_area']);
            }
            if (strtolower($this->request->data['metro_mrts_categories']) == 'station branding') {
                $mertoCounter = 0;
                foreach ($this->request->data['station_branding_total_facade'] as $key => $total_facade) {
                    $metroData['data'][$mertoCounter]['facade'] = 'F' . ($mertoCounter + 1);
                    $metroData['data'][$mertoCounter]['total_facade'] = $total_facade;
                    $metroData['data'][$mertoCounter]['actual_facade'] = $this->request->data['station_branding_actual_facade'][$key];
                    $mertoCounter++;
                }
                unset($this->request->data['station_branding_total_facade'], $this->request->data['station_branding_actual_facade']);
            }
            $this->request->data['metro_table'] = json_encode($metroData);
        }
        if ($subcategory == 11) {
            $shelterData = [];
            $counter = 0;
            foreach ($this->request->data['shelter_location'] as $key => $shelter_location) {
                $shelterData[$counter]['location'] = $shelter_location;
                $shelterData[$counter]['panels'] = $this->request->data['shelter_panels'][$key];
                $shelterData[$counter]['area'] = $this->request->data['shelter_area'][$key];
                $shelterData[$counter]['lat'] = $this->request->data['shelter_lat'][$key];
                $shelterData[$counter]['lng'] = $this->request->data['shelter_lng'][$key];
                $counter++;
            }
            unset($this->request->data['shelter_location'], $this->request->data['shelter_panels'], $this->request->data['shelter_area'], $this->request->data['shelter_lat'], $this->request->data['shelter_lng']);
            $this->request->data['shelter_data'] = json_encode($shelterData);
        }
        if ($subcategory == 12) {
            $routeMakerData = [];
            $counter = 0;
            foreach ($this->request->data['route_markers_location'] as $key => $route_markers_location) {
                $routeMakerData[$counter]['location'] = $route_markers_location;
                $routeMakerData[$counter]['device'] = $this->request->data['route_markers_devices'][$key];
                $routeMakerData[$counter]['area'] = $this->request->data['route_markers_area'][$key];
                $routeMakerData[$counter]['lat'] = $this->request->data['route_markers_lat'][$key];
                $routeMakerData[$counter]['lng'] = $this->request->data['route_markers_lng'][$key];
                $counter++;
            }
            unset($this->request->data['route_markers_location'], $this->request->data['route_markers_devices'], $this->request->data['route_markers_area'], $this->request->data['route_markers_lat'], $this->request->data['route_markers_lng']);
            $this->request->data['route_markers_data'] = json_encode($routeMakerData);
        }
        if ($subcategory == 13) {
            $footoverData = [];
            $counter = 0;
            foreach ($this->request->data['foot_over_location'] as $key => $route_markers_location) {
                $footoverData[$counter]['location'] = $route_markers_location;
                $footoverData[$counter]['panels'] = $this->request->data['foot_over_panels'][$key];
                $footoverData[$counter]['area'] = $this->request->data['foot_over_area'][$key];
                $footoverData[$counter]['lat'] = $this->request->data['foot_over_lat'][$key];
                $footoverData[$counter]['lng'] = $this->request->data['foot_over_lng'][$key];
                $counter++;
            }
            $this->request->data['foot_over_data'] = json_encode($footoverData);
            unset($this->request->data['foot_over_location'], $this->request->data['foot_over_panels'], $this->request->data['foot_over_area'], $this->request->data['foot_over_lat'], $this->request->data['foot_over_lng']);
        }
        if ($subcategory == 14) {
            $cycleStation = [];
            $counter = 0;
            foreach ($this->request->data['cycle_station_location'] as $key => $location) {
                $cycleStation[$counter]['location'] = $location;
                $cycleStation[$counter]['panels'] = $this->request->data['cycle_station_panels'][$key];
                $cycleStation[$counter]['area'] = $this->request->data['cycle_station_area'][$key];
                $cycleStation[$counter]['lat'] = $this->request->data['cycle_station_lat'][$key];
                $cycleStation[$counter]['lng'] = $this->request->data['cycle_station_lng'][$key];
                $counter++;
            }
            $this->request->data['cycle_station_data'] = json_encode($cycleStation);
            unset($this->request->data['cycle_station_location'], $this->request->data['cycle_station_panels'], $this->request->data['cycle_station_area'], $this->request->data['cycle_station_lat'], $this->request->data['cycle_station_lng']);
        }
        if ($subcategory == 15) {
            $policeBooth = [];
            $counter = 0;
            foreach ($this->request->data['police_booth_location'] as $key => $location) {
                $policeBooth[$counter]['location'] = $location;
                $policeBooth[$counter]['panels'] = $this->request->data['police_booth_panels'][$key];
                $policeBooth[$counter]['area'] = $this->request->data['police_booth_area'][$key];
                $policeBooth[$counter]['lat'] = $this->request->data['police_booth_lat'][$key];
                $policeBooth[$counter]['lng'] = $this->request->data['police_booth_lng'][$key];
                $counter++;
            }
            $this->request->data['police_booth_data'] = json_encode($policeBooth);
            unset($this->request->data['police_booth_location'], $this->request->data['police_booth_panels'], $this->request->data['police_booth_area'], $this->request->data['police_booth_lat'], $this->request->data['police_booth_lng']);
        }
        if ($subcategory == 16) {
            $sittingBench = [];
            $counter = 0;
            foreach ($this->request->data['sitting_bench_location'] as $key => $location) {
                $sittingBench[$counter]['location'] = $location;
                $sittingBench[$counter]['panels'] = $this->request->data['sitting_bench_panels'][$key];
                $sittingBench[$counter]['area'] = $this->request->data['sitting_bench_area'][$key];
                $sittingBench[$counter]['lat'] = $this->request->data['sitting_bench_lat'][$key];
                $sittingBench[$counter]['lng'] = $this->request->data['sitting_bench_lng'][$key];
                $counter++;
            }
            $this->request->data['sitting_bench_data'] = json_encode($sittingBench);
            unset($this->request->data['sitting_bench_location'], $this->request->data['sitting_bench_panels'], $this->request->data['sitting_bench_area'], $this->request->data['sitting_bench_lat'], $this->request->data['sitting_bench_lng']);
        }
        if($subcategory == 32) {
            $otherTypeA = [];
            $counter = 0;
            foreach ($this->request->data['other_type_a_location'] as $key => $location) {
                $otherTypeA[$counter]['location'] = $location;
                $otherTypeA[$counter]['height'] = $this->request->data['other_type_a_height'][$key];
                $otherTypeA[$counter]['width'] = $this->request->data['other_type_a_width'][$key];
                $otherTypeA[$counter]['area'] = $this->request->data['other_type_a_area'][$key];
                $otherTypeA[$counter]['lat'] = $this->request->data['other_type_a_lat'][$key];
                $otherTypeA[$counter]['lng'] = $this->request->data['other_type_a_lng'][$key];
                $counter++;
            }
            $this->request->data['other_type_a_data'] = json_encode($otherTypeA);
            unset($this->request->data['other_type_a_location'], $this->request->data['other_type_a_height'], $this->request->data['other_type_a_width'], $this->request->data['other_type_a_area'], $this->request->data['other_type_a_lat'], $this->request->data['other_type_a_lng']);
        }
        if ($companyDevicesTable->save($companyDevice)) {
            foreach ($this->request->data as $key => $item) {
                if (is_array($item) && $item['error'] == 0) {
                    $ext = pathinfo($this->request->data[$key]['name'], PATHINFO_EXTENSION);
                    $fileName = time() . '_' . $this->_generateRandomString(20) . '.' . $ext;
                    $uploadFile = 'uploads/devices/' . $fileName;
                    if (move_uploaded_file($this->request->data[$key]['tmp_name'], $uploadFile)) {
                        $item = $fileName;
                    }
                }
                $deviceFieldData[] = [
                    'company_device_id' => $companyDevice->id,
                    'field_key' => $key,
                    'input_value' => $item,
                ];
            }
            $companyDeviceFieldsTable = TableRegistry::get('CompanyDeviceFields');
            $companyDeviceFieldsTable->query()->delete()->where(['company_device_id' => $companyDevice->id])->execute();
            $companyDeviceFields = $companyDeviceFieldsTable->newEntities($deviceFieldData);
            $companyDeviceFieldsTable->saveMany($companyDeviceFields);
            $this->Flash->success("Device has been resubmitted.");
            return $this->redirect(['action' => 'applicationStatus']);
        }
        $this->Flash->error("Something went wrong on server. Please try again.");
        return $this->redirect(['action' => 'applicationStatus']);
    }

    public function payFinalAmount($deviceId = null) {
        $device = $this->_getDeviceData($deviceId);
        if($device['payment_id']) {
            $this->Flash->error("You already paid for this OMD device.");
            return $this->redirect(['action' => 'applicationStatus']);
        }
        $deviceData = $device['fields'];
        $feesArr = array_values(array_filter($deviceData, function($arr) {
                    return $arr['field'] === 'annual_license_fees';
                }));
        if (!$feesArr) {
            $feesArr = array_values(array_filter($deviceData, function($arr) {
                        return $arr['field'] === 'concession_fees';
                    }));
        }
        if (!$feesArr) {
            $feesArr = array_values(array_filter($deviceData, function($arr) {
                        return $arr['field'] === 'total_omd_fees';
                    }));
        }
        if ($feesArr) {
            $amount = $feesArr[0]['value'];
            $paymentTable = TableRegistry::get('Payments');
            $paymentId = time() . $this->Auth->user('id');
            $paymentData = [
                'user_id' => $this->Auth->user('id'),
                'payment_id' => $paymentId,
                'payment_type' => 'device_final_pay',
                'type_id' => $deviceId,
                'amount' => $amount
            ];
            $paymentEntity = $paymentTable->newEntity($paymentData);
            $paymentTable->save($paymentEntity);
            $this->loadComponent('Ccavenue');
            $redirectionUrl = Router::url(['controller' => 'Payment', 'action' => 'response'], true);
            $url = $this->Ccavenue->getUrl();
            $request = $this->Ccavenue->getPaymentdata($paymentId, $amount, $redirectionUrl);
            $accessCode = $this->Ccavenue->getAccessCode();
            $this->set(compact('url', 'accessCode', 'request'));
        }
    }

}
