<?php

namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;

/**
 * CompanyDevices Controller
 *
 * @property \App\Model\Table\CompanyDevicesTable $CompanyDevices
 */
class CompanyDevicesController extends AppController {

    private $placesAPI = 'https://maps.googleapis.com/maps/api/place/textsearch/json?';
    private $placesKey = 'AIzaSyD-V-cjIUZu6DPHpERzLBIV5OnR_AxkRtw';
    /// Initializing pagination settings
    public $paginate = [
        'limit' => 10,
        'order' => [
            'companyDevices.id' => 'desc'
        ]
    ];

    public function initialize() {
        parent::initialize();
        $this->viewBuilder()->layout('dashboard');
    }

    public function register() {
        $categories = [];
        $categoriesTable = TableRegistry::get('Categories');
        $categoriesData = $categoriesTable->query()->select(['id', 'title'])->where(['parent_id' => 0])->hydrate(false);
        foreach ($categoriesData->toArray() as $categoryItem) {
            $categories[$categoryItem['id']] = $categoryItem['title'];
        }
        $this->set(compact('categories'));
    }

    public function getCategories() {
        if (!$this->request->is('ajax')) {
            exit("You are not allowed to access this page");
        }
        $categoriesTable = TableRegistry::get('Categories');
        $type = $this->request->data['deviceType'];
        $categoriesData = $categoriesTable->query()->select(['id', 'title'])->where(['parent_id' => 0,'FIND_IN_SET("'.$type.'",type)'])->hydrate(false);
        $response = $categoriesData->toArray();
        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }

    public function getSubCategory() {
        if (!$this->request->is('ajax')) {
            exit("You are not allowed to access this page");
        }
        $response = [];
        $parent_id = $this->request->data['id'];
        if ($parent_id) {
            $categoriesTable = TableRegistry::get('Categories');
            $categoriesData = $categoriesTable->query()->select(['id', 'title'])->where(['parent_id' => $parent_id])->hydrate(false);
            $response = $categoriesData->toArray();
        }
        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }

    public function getFields() {
        if (!$this->request->is('ajax')) {
            exit("You are not allowed to access this page");
        }
        $categoryId = $this->request->data['id'];
        $categoryFieldsTable = TableRegistry::get('categoryFields');
        $data = $categoryFieldsTable->find()->select(['field_id', 'custom_validation', 'custom_disable', 'custom_calculations', 'lable' => 'f.lable', 'field_key' => 'f.field_key', 'type' => 'f.type', 'options' => 'f.options', 'default_validations' => 'f.default_validations', 'default_calculations' => 'f.default_calculations', 'group_in' => 'f.group_in','info' => 'f.info', 'is_disabled' => 'f.is_disabled'])->join([
                    'table' => 'fields',
                    'alias' => 'f',
                    'type' => 'LEFT',
                    'conditions' => 'categoryFields.field_id = f.id',
                ])->where(['category_id' => $categoryId])->order(['sort_order' => 'ASC'])->hydrate(false);
        $response = $data->toArray();
        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }

    public function getLocation() {
        if (!$this->request->is('ajax')) {
            exit("You are not allowed to access this page");
        }
        $response = [];
        $keyword = $this->request->query('s') . ', gurgaon, gurugram';
        if ($keyword) {
            $places = json_decode(file_get_contents($this->placesAPI . 'key=' . $this->placesKey . '&query=' . urlencode($keyword)), true);
            $response = array_map(function($arr) {
                return array(
                    'label' => $arr['formatted_address'],
                    'value' => $arr['formatted_address'],
                    'data' => $arr['geometry']
                );
            }, $places['results']);
        }
        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }

    public function saveDevice() {
        $this->autoRender = false;
        if (!$this->request->is('post')) {
            exit('You are not allowd to access this page');
        }
        $deviceFieldData = [];
//        $device_name = $this->request->data['device_name'];
        $companyCode = $this->Auth->user('company_code');
        $categoryId = $this->request->data['category_id'];
        $subcategory = $this->request->data['subcategory'];
        $devive_type = $this->request->data['devive_type'];
        unset($this->request->data['device_name']);
        unset($this->request->data['category_id']);
        unset($this->request->data['subcategory']);
        unset($this->request->data['devive_type']);
        $companyDevicesTable = TableRegistry::get('CompanyDevices');
        $companyDevice = $companyDevicesTable->newEntity();
        $companyDevice->by_admin = $this->Auth->user('id');
//        $companyDevice->device_name = $device_name;
        $companyDevice->category_id = $categoryId;
        $companyDevice->sub_category_id = $subcategory;
        $companyDevice->devive_type = $devive_type;
        $companyDevice->status = 'approved';
        $companyDevice->paid = 1;
        if ($companyDevicesTable->save($companyDevice)) {
            foreach ($this->request->data as $key => $item) {
                if (is_array($item) && $item['error'] == 0) {
                    $ext = pathinfo($this->request->data[$key]['name'], PATHINFO_EXTENSION);
                    $fileName = time() . '_' . $this->_generateRandomString(20) . '.' . $ext;
                    $uploadFile = WWW_ROOT.'uploads/devices/' . $fileName;
                    if (move_uploaded_file($this->request->data[$key]['tmp_name'], $uploadFile)) {
                        $item = $fileName;
                    }
                }
                $deviceFieldData[] = [
                    'company_device_id' => $companyDevice->id,
                    'field_key' => $key,
                    'input_value' => $item,
                ];
            }
            $companyDeviceFieldsTable = TableRegistry::get('CompanyDeviceFields');
            $companyDeviceFields = $companyDeviceFieldsTable->newEntities($deviceFieldData);
            if ($companyDeviceFieldsTable->saveMany($companyDeviceFields)) {
                $this->Flash->success(__('New Device have been successfully registered.'));
                return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
            }
        }
    }

}
