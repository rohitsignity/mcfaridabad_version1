<?php

namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;

class ReportsController extends AppController {

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->viewBuilder()->layout('dashboard');
    }

    public function index() {
        $data = array(
            array(
                'text' => 'Registered Users',
                'url' => Router::url(array('action' => 'users'))
            ),
            array(
                'text' => 'OMD Devices',
                'url' => Router::url(array('action' => 'omd'))
            ),
            array(
                'text' => 'OMD Devices Final Payment',
                'url' => Router::url(array('action' => 'omdPayment'))
            )
        );
        $this->set(compact('data'));
    }

    public function users() {
        $users = [];
        $representatives = [];
        $entitiesTypesTable = TableRegistry::get('Representatives');
        $entitiesTypes = $entitiesTypesTable->find()->hydrate(false)->toArray();
        array_walk($entitiesTypes, function($val) use(&$representatives) {
            $representatives[$val['id']] = $val['representative'];
        });
        if ($this->request->is('post')) {
            $where = [
                'Users.type' => 'user',
                'Users.paid' => '1'
            ];
            if ($this->request->data['representative_id']) {
                $where['Users.representative_id'] = $this->request->data['representative_id'];
            }
            if (trim($this->request->data['receipt_id'])) {
                $where['paymentTable.id'] = trim(str_replace('mcg-', '', strtolower($this->request->data['receipt_id'])));
            }
            if (trim($this->request->data['display_name'])) {
                $where['Users.display_name LIKE'] = '%' . trim($this->request->data['display_name']) . '%';
            }
            $userTable = TableRegistry::get('Users');
            $users = $userTable->find()->select([
				'receipt_no' => 'CONCAT("MCF-",paymentTable.id)', 
				'registration_number' => 'CONCAT("MCFVENDOR",Users.id,"U",Users.id)', 
				'display_name', 
				'email', 
				'mobile', 
				'g8_response' => 'paymentTable.g8_response', 
				'payment_tracking_id' => 'paymentTable.tracking_id', 
				'bank_ref_no' => 'paymentTable.bank_ref_no', 
				'cc_amount' => 'paymentTable.amount', 
				'amount' => 'paymentTable.mer_amount', 
				'payment_mode' => 'paymentTable.payment_mode',
				 'card_name' => 'paymentTable.card_name', 
				 'registered_at' => 'Users.created'])->join([
                        'paymentTable' => [
                            'type' => 'LEFT',
                            'table' => '(SELECT * FROM `payments` WHERE `payment_type` = "registration" AND `order_status` = "Success")',
                            'conditions' => 'Users.id = paymentTable.user_id'
                        ]
                    ])->where($where)->order(['Users.created' => 'DESC'])->hydrate(false)->toArray();
            if (isset($this->request->data['request_type']) && $this->request->data['request_type'] == 'export' && !isset($this->request->data['reportSubmit'])) {
                $fileName = 'users_report.csv';
                $csvHeader = array('Receipt No', 'Registration number', 'Company/Individual Name', 'Email', 'Mobile','G8_Receipt_Number', 'Payment Tracking Id (ccavenue)', 'Bank Reference Number', 'Amount (With ccavenue charges)', 'Amount', 'Payment Mode', 'Card Name', 'Registered At');
                
                $users_new = array_map(function($arr) {
					if(!empty($arr['g8_response']) && ($arr['g8_response'] != 'null')){
						$rec = json_decode($arr['g8_response'],true);
						 $arr['g8_response'] = $rec['RECEIPT_NUMBER'];
						
					} else {
						$arr['g8_response'] = '';
					}
                    return $arr;
                }, $users);
                
                $csvData = array_map(function($arr) {
                    return array_values($arr);
                }, $users_new);
                array_unshift($csvData, $csvHeader);
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header('Content-Description: File Transfer');
                header("Content-type: text/csv");
                header("Content-Disposition: attachment; filename={$fileName}");
                header("Expires: 0");
                header("Pragma: public");
                $fh = @fopen('php://output', 'w');
                foreach ($csvData as $item) {
                    fputcsv($fh, $item);
                }
                fclose($fh);
                exit;
            }
        }
        $this->set(compact('representatives', 'users'));
    }

    public function omd() {
        $categories = [];
        $subCategories = [];
        $devices = [];
        $categoriesTable = TableRegistry::get('Categories');
        $categoriesData = $categoriesTable->query()->select(['id', 'title'])->where(['parent_id' => 0])->hydrate(false);
        foreach ($categoriesData->toArray() as $categoryItem) {
            $categories[$categoryItem['id']] = $categoryItem['title'];
        }
        if ($this->request->is('post')) {
            $companyDevicesTable = TableRegistry::get('CompanyDevices');
            $where = [
                'CompanyDevices.paid' => '1'
            ];
            if ($this->request->data['typology']) {
                $where['CompanyDevices.category_id'] = $this->request->data['typology'];
                $subCategoriesTable = TableRegistry::get('Categories');
                $subCategoriesData = $subCategoriesTable->query()->select(['id', 'title'])->where(['parent_id' => $this->request->data['typology']])->hydrate(false);
                foreach ($subCategoriesData->toArray() as $categoryItem) {
                    $subCategories[$categoryItem['id']] = $categoryItem['title'];
                }
            }
            if ($this->request->data['sub_category']) {
                $where['CompanyDevices.sub_category_id'] = $this->request->data['sub_category'];
            }
            if (trim($this->request->data['display_name'])) {
                $where['u.display_name LIKE'] = '%' . trim($this->request->data['display_name']) . '%';
            }
            if (trim($this->request->data['receipt_id'])) {
                $where['paymentTable.id'] = trim(str_replace('mcg-', '', strtolower($this->request->data['receipt_id'])));
            }
            $devices = $companyDevicesTable->find()->select([
					'receipt_no' => 'CONCAT("MCF-",paymentTable.id)', 
					'omd_registration_number' => 'CONCAT("MCFOMD",
					`CompanyDevices`.`id`,"U",`CompanyDevices`.`user_id`)', 
					'display_name' => 'u.display_name', 
					'email' => 'u.email', 
					'mobile' => 'u.mobile',
					'g8_response' => 'paymentTable.g8_response', 
					'typology' => 't.title', 
					'sub_category' => 'sc.title', 
					'payment_tracking_id' => 'paymentTable.tracking_id', 
					'bank_reference_number' => 'paymentTable.bank_ref_no', 
					'cc_amount' => 'paymentTable.amount', 
					'amount' => 'paymentTable.mer_amount', 
					'payment_mode' => 'paymentTable.payment_mode', 
					'card_name' => 'paymentTable.card_name', 
					'CompanyDevices.status', 
					'registered_at' => 'CompanyDevices.created'])->join([
                        'paymentTable' => [
                            'table' => '(SELECT * FROM `payments` WHERE `payment_type` = "device_registration" AND `order_status` = "Success")',
                            'type' => 'LEFT',
                            'conditions' => '`CompanyDevices`.`id` = `paymentTable`.`type_id`'
                        ],
                        'u' => [
                            'table' => 'users',
                            'type' => 'LEFT',
                            'conditions' => '`u`.`id` = `CompanyDevices`.`user_id`'
                        ],
                        't' => [
                            'table' => 'categories',
                            'type' => 'LEFT',
                            'conditions' => 't.id = `CompanyDevices`.`category_id`'
                        ],
                        'sc' => [
                            'table' => 'categories',
                            'type' => 'LEFT',
                            'conditions' => 'sc.id = `CompanyDevices`.`sub_category_id`'
                        ]
                    ])->where($where)->order(['CompanyDevices.created' => 'DESC'])->hydrate(false)->toArray();
            
            if (isset($this->request->data['request_type']) && $this->request->data['request_type'] == 'export' && !isset($this->request->data['reportSubmit'])) {
                $fileName = 'devices_report.csv';
                $csvHeader = array('Receipt No','OMD registration number','Company/Individual Name','Email','Mobile','G8_Receipt_Number','Typology','Sub Category','Payment Tracking Id (ccavenue)','Bank Reference Number','Amount (With ccavenue charges)','Amount','Payment Mode','Card Name','Status','Registered At');
				
				
				$devices_new = array_map(function($arr) {
					if(!empty($arr['g8_response']) && ($arr['g8_response'] != 'null')){
						$rec = json_decode($arr['g8_response'],true);
						 $arr['g8_response'] = $rec['RECEIPT_NUMBER'];
						
					} else {
						$arr['g8_response'] = '';
					}
                    return $arr;
                }, $devices);
                
            
                $csvData = array_map(function($arr) {
                    return array_values($arr);
                }, $devices_new);
                array_unshift($csvData, $csvHeader);
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header('Content-Description: File Transfer');
                header("Content-type: text/csv");
                header("Content-Disposition: attachment; filename={$fileName}");
                header("Expires: 0");
                header("Pragma: public");
                $fh = @fopen('php://output', 'w');
                foreach ($csvData as $item) {
                    fputcsv($fh, $item);
                }
                fclose($fh);
                exit;
            }
        }
        $this->set(compact('categories', 'subCategories', 'devices'));
    }
    
    //================= Final Payment ====================
    public function omdPayment() {
		$categories = [];
        $subCategories = [];
        $categoriesTable = TableRegistry::get('Categories');
        $where  = [];
        $devices = [];
        $categoriesData = $categoriesTable->query()->select(['id', 'title'])->where(['parent_id' => 0])->hydrate(false);
        foreach ($categoriesData->toArray() as $categoryItem) {
            $categories[$categoryItem['id']] = $categoryItem['title'];
        }
        if ($this->request->is('post')) {
			$payment_type_where_sql = 'payment_type = "device_final_pay" OR payment_type = "device_installment"';
			$companyDevicesTable = TableRegistry::get('CompanyDevices');
			if($this->request->data['typology']){
				$where['CompanyDevices.category_id'] = $this->request->data['typology'];
			}
			$subCategoriesTable = TableRegistry::get('Categories');
			$subCategoriesData = $subCategoriesTable->query()->select(['id', 'title'])->where(['parent_id' => $this->request->data['typology']])->hydrate(false);
			foreach ($subCategoriesData->toArray() as $categoryItem) {
				$subCategories[$categoryItem['id']] = $categoryItem['title'];
			}
			
			if ($this->request->data['sub_category']) {
                $where['CompanyDevices.sub_category_id'] = $this->request->data['sub_category'];
            }
            
            if (trim($this->request->data['display_name'])) {
                $where['u.display_name LIKE'] = '%' . trim($this->request->data['display_name']) . '%';
            }
            if (trim($this->request->data['receipt_id'])) {
                $where['paymentTable.id'] = trim(str_replace('mcg-', '', strtolower($this->request->data['receipt_id'])));
            }
            
            if ($this->request->data['payment_type']) {
				$payment_type_where_sql = ($this->request->data['payment_type'] == 'final_payment') ? 'payment_type = "device_final_pay"' : 'payment_type = "device_installment"';
		    }
		    
		    //echo $payment_type_where_sql;
			$devices = $companyDevicesTable->find()->select(
				[
					'receipt_no' => 'CONCAT("MCF-",paymentTable.id)',
					'omd_registration_number' => 'CONCAT("MCFOMD",`CompanyDevices`.`id`,"U",`CompanyDevices`.`user_id`)', 
					'display_name' => 'u.display_name', 
					'email' => 'u.email', 
					'mobile' => 'u.mobile', 
					'g8_response' => 'paymentTable.g8_response',
					'typology' => 't.title', 
					'sub_category' => 'sc.title', 
					'payment_tracking_id' => 'paymentTable.tracking_id', 
					'bank_reference_number' => 'paymentTable.bank_ref_no', 
					'cc_amount' => 'paymentTable.amount', 
					'amount' => 'paymentTable.mer_amount', 
					'payment_mode' => 'paymentTable.payment_mode', 
					'card_name' => 'paymentTable.card_name', 
					'CompanyDevices.status', 
					'registered_at' => 'CompanyDevices.created'
				])->join([
						/*
                        'paymentTable' => [
                            'table' => '(SELECT * FROM `payments` WHERE (payment_type = "device_final_pay" OR payment_type = "device_installment") AND `order_status` = "Success")',
                            'type' => 'INNER',
                            'conditions' => '`CompanyDevices`.`id` = `paymentTable`.`type_id`'
                        ],
                        */
                        'paymentTable' => [
                            'table' => '(SELECT * FROM `payments` WHERE ('.$payment_type_where_sql.') AND `order_status` = "Success")',
                            'type' => 'INNER',
                            'conditions' => '`CompanyDevices`.`id` = IF(paymentTable.payment_type = "device_installment",(
							SELECT installments.device_id FROM `installments` WHERE installments.id = paymentTable.type_id ),paymentTable.type_id)'
                        ],
                         
                        'u' => [
                            'table' => 'users',
                            'type' => 'LEFT',
                            'conditions' => '`u`.`id` = `CompanyDevices`.`user_id`'
                        ],
                        't' => [
                            'table' => 'categories',
                            'type' => 'LEFT',
                            'conditions' => 't.id = `CompanyDevices`.`category_id`'
                        ],
                        'sc' => [
                            'table' => 'categories',
                            'type' => 'LEFT',
                            'conditions' => 'sc.id = `CompanyDevices`.`sub_category_id`'
                        ]
                    ])->where($where)->order(['CompanyDevices.created' => 'DESC'])->hydrate(false)->toArray();
			 if (isset($this->request->data['request_type']) && $this->request->data['request_type'] == 'export' && !isset($this->request->data['reportSubmit'])) {
				$fileName = 'devices_report.csv';
				$csvHeader = array('Receipt No','OMD registration number','Company/Individual Name','Email',
				'Mobile','G8_Receipt_Number','Typology','Sub Category','Payment Tracking Id (ccavenue)','Bank Reference Number',
				'Amount (With ccavenue charges)','Amount','Payment Mode','Card Name','Status','Registered At');
				
				$devices_new = array_map(function($arr) {
					if(!empty($arr['g8_response']) && ($arr['g8_response'] != 'null')){
						$rec = json_decode($arr['g8_response'],true);
						 $arr['g8_response'] = $rec['RECEIPT_NUMBER'];
						
					} else {
						$arr['g8_response'] = '';
					}
                    return $arr;
                }, $devices);
				
				$csvData = array_map(function($arr) {
					return array_values($arr);
				}, $devices_new);
				array_unshift($csvData, $csvHeader);
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header('Content-Description: File Transfer');
				header("Content-type: text/csv");
				header("Content-Disposition: attachment; filename={$fileName}");
				header("Expires: 0");
				header("Pragma: public");
				$fh = @fopen('php://output', 'w');
				foreach ($csvData as $item) {
					fputcsv($fh, $item);
				}
				fclose($fh);
				exit;
			 }		 
            
		}
		$this->set(compact('categories', 'subCategories', 'devices'));
	}

    public function getSubCategory() {
        $this->_ajaxCheck();
        $response = [];
        $parent_id = $this->request->data['id'];
        if ($parent_id) {
            $categoriesTable = TableRegistry::get('Categories');
            $categoriesData = $categoriesTable->query()->select(['id', 'title'])->where(['parent_id' => $parent_id])->hydrate(false);
            $response = $categoriesData->toArray();
        }
        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }

}
