<?php

namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Mailer\MailerAwareTrait;
use App\Traits\Devices;
use App\Traits\Categories;
use App\Traits\Tenders;
use App\Traits\Bids;
use App\Traits\Users;

class TendersController extends AppController {

    use MailerAwareTrait,
        Devices,
        Categories,
        Tenders,
        Bids,
        Users;

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        return $this->redirect(['controller' => 'Dashboard','action' => 'index']);
        $this->Auth->allow(['tenderPublishCron']);
        $this->viewBuilder()->layout('dashboard');
    }

    public function index() {
        $limit = 10;
        $this->paginate = [
            'limit' => $limit
        ];
        $tenders = $this->paginate($this->Tenders->find('all')->order([
                    'publish_status' => 'asc',
                    'created' => 'desc'
        ]));
        $this->set(compact('limit'));
        $this->set(compact('tenders'));
    }

    public function add() {
        $devicePath = Router::url('/') . 'uploads/devices/';
        $typologies = [];
        $userOptions = [];
        $devices = [];
        $userSegments = [];
        $subCategories = [];
        $experiences = [];
        $documents = [];
        $userError = '';
        $tender = $this->Tenders->newEntity();
        $userSegmentsTable = TableRegistry::get('AnnualTurnovers');
        $userSegmentsArr = $userSegmentsTable->find('all')->hydrate(false)->toArray();
        $deviceId = null;
        array_map(function($val) use(&$userSegments) {
            $userSegments[$val['id']] = $val['annual_turnover'];
        }, $userSegmentsArr);
        /* Experiences */
        $experiencesTable = TableRegistry::get('Experiences');
        $experiencesData = $experiencesTable->find('all')->hydrate(false)->toArray();
        array_walk($experiencesData, function($val) use(&$experiences) {
            $experiences[$val['id']] = $val['name'];
        });
        /* Documents */
        $documentsTable = TableRegistry::get('Documents');
        $documentsData = $documentsTable->find('all')->hydrate(false)->toArray();
        array_walk($documentsData, function($val) use(&$documents) {
            $documents[$val['id']] = $val['name'];
        });
        if ($this->request->is('post')) {
            if (!isset($this->request->data['device_id'])) {
                $this->request->data['device_id'] = '';
            }
            if ($this->request->data['typology_id']) {
                $subCategories = $this->_getSubCategoriesArr($this->request->data['typology_id']);
            }
            if ($this->request->data['subcategory_id']) {
                $devicesArr = $this->_getPublicDevicesBySubCategory($this->request->data['subcategory_id']);
                array_walk($devicesArr, function($val) use(&$devices) {
                    $devices[$val['id']] = $val['device_name'];
                });
            }
            if ($this->request->data['bid_doc']['size']) {
                $ext = pathinfo($this->request->data['bid_doc']['name'], PATHINFO_EXTENSION);
                $fileName = time() . '_' . $this->_generateRandomString(20) . '.' . $ext;
                $uploadFile = WWW_ROOT.'uploads/bid_instructions_documents/' . $fileName;
                if (move_uploaded_file($this->request->data['bid_doc']['tmp_name'], $uploadFile)) {
                    $this->request->data['bid_doc'] = $fileName;
                }
            }
            $deviceId = $this->request->data['device_id'];
            $usersTable = TableRegistry::get('Users');
            $userArr = [];
            if (isset($this->request->data['users'])) {
                $userArr = $usersTable->find('all')->select(['id', 'text' => 'display_name', 'company_code'])->where(['id IN' => $this->request->data['users']])->hydrate(false)->toArray();
            } else {
                $this->request->data['users'] = [];
            }
            array_walk($userArr, function($val) use(&$userOptions) {
                $userOptions[$val['id']] = $val['text'];
            });
            $tender = $this->Tenders->patchEntity($tender, $this->request->data);
            $tender->end_date_updated = date('Y-m-d H:i:s',strtotime($this->request->data['end_date']));
            if ($tender->errors()) {
                $errors = $tender->errors();
                $userError = isset($errors['users']) ? reset($errors['users']) : '';
            }
            $saved = $this->Tenders->save($tender);
            if ($saved) {
                $savedArr = $saved->toArray();
                $tenderUsers = [];
                $tenderDevicesData = [];
                foreach ($this->request->data['users'] as $user) {
                    $tenderUsers[] = [
                        'tender_id' => $savedArr['id'],
                        'user_id' => $user
                    ];
                }
                foreach ($this->request->data['device_ids'] as $device_id) {
                    $tenderDevicesData[] = [
                        'tender_id' => $savedArr['id'],
                        'device_id' => $device_id
                    ];
                }
                if ($tenderUsers) {
                    $tenderUsersTable = TableRegistry::get('TenderUsers');
                    $tenderUsersEnitites = $tenderUsersTable->newEntities($tenderUsers);
                    $tenderUsersTable->saveMany($tenderUsersEnitites);
                }
                if($tenderDevicesData) {
                    $tenderDevicesTable = TableRegistry::get('TenderDevices');
                    $tenderDevicesEnitites = $tenderDevicesTable->newEntities($tenderDevicesData);
                    $tenderDevicesTable->saveMany($tenderDevicesEnitites);
                }
                $documentsToSave = [];
                $requiredDocuments = isset($this->request->data['document']) ? $this->request->data['document'] : [];
                foreach ($requiredDocuments as $requiredDocument) {
                    $documentsToSave[] = [
                        'tender_id' => $savedArr['id'],
                        'document_id' => $requiredDocument
                    ];
                }
                if ($documentsToSave) {
                    $tenderDocTable = TableRegistry::get('TenderDocuments');
                    $tenderDocEntities = $tenderDocTable->newEntities($documentsToSave);
                    $tenderDocTable->saveMany($tenderDocEntities);
                }
                if (date('Y-m-d', strtotime($savedArr['start_date'])) == date('Y-m-d') && $savedArr['publish_type'] == 0) {
                    //$this->__publish($savedArr, 1);
                }
                $this->Flash->success(__('Tender successfully added'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Error in adding Tender'));
        }
        /* Public Categories ( Typologies ) */
        $typologiesArr = $this->_getPublicCategories();
        array_walk($typologiesArr, function($arr) use(&$typologies) {
            $typologies[$arr['id']] = $arr['title'];
        });
        $this->set(compact('devicePath', 'deviceId', 'typologies', 'subCategories', 'devices', 'userError', 'userOptions', 'userSegments', 'tender', 'experiences', 'documents'));
    }

    public function validateTender() {
        $this->autoRender = false;
//        $this->_ajaxCheck();
        if (!$this->request->is('post')) {
            exit("You are not allowed to access this URL.");
        }
        $response['error'] = 0;
        $tender = $this->Tenders->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['users'] = isset($this->request->data['users']) ? $this->request->data['users'] : '';
            $this->request->data['device_ids'] = isset($this->request->data['device_ids']) ? $this->request->data['device_ids'] : '';
            $tender = $this->Tenders->patchEntity($tender, $this->request->data);
            if ($tender->errors()) {
                $response['error'] = 1;
                $response['data'] = $tender->errors();
            }
        }
        $this->_jsonResponse($response);
    }

    public function edit($id = null) {
        die("Edit disabled");
        $devicePath = Router::url('/') . 'uploads/devices/';
        $tender = $this->Tenders->get($id);
        if ($tender->publish_status) {
            $this->Flash->error(__('You are not allowed to edit "' . $tender->name . '" tender'));
            return $this->redirect(['controller' => 'Tenders', 'action' => 'index']);
        }
        $typologies = [];
        $userOptions = [];
        $devices = [];
        $userSegments = [];
        $userError = '';
        $deviceId = $tender->device_id;
        $deviceData = $this->_getDevice($deviceId);
        $tender->typology_id = $deviceData->category_id;
        $tender->subcategory_id = $deviceData->sub_category_id;
        $subCategories = $this->_getSubCategoriesArr($deviceData->category_id);
        $tender->start_date = date('Y-m-d H:i:s', strtotime($tender->start_date));
        $tender->end_date = date('Y-m-d H:i:s', strtotime($tender->end_date));
        $tender->doc_download_date = date('Y-m-d', strtotime($tender->doc_download_date));
        $usersTable = TableRegistry::get('Users');
        $devicesArr = $this->_getPublicDevicesBySubCategory($deviceData->sub_category_id);
        array_walk($devicesArr, function($val) use(&$devices) {
            $devices[$val['id']] = $val['device_name'];
        });
        $tenderUsersTable = TableRegistry::get('TenderUsers');
        $tenderUsers = $tenderUsersTable->find('all')->select(['id', 'user_id'])->where(['tender_id' => $id])->hydrate(false)->toArray();
        $users = array_map(function($arr) {
            return $arr['user_id'];
        }, $tenderUsers);
        $tender->users = $users;
        $userArr = $usersTable->find('all')->select(['id', 'text' => 'display_name', 'company_code'])->where(['id IN' => $users])->hydrate(false)->toArray();
        array_walk($userArr, function($val) use(&$userOptions) {
            $userOptions[$val['id']] = $val['text'];
        });
        if ($this->request->is('put')) {
            if (!isset($this->request->data['device_id'])) {
                $this->request->data['device_id'] = '';
            }
            if ($this->request->data['typology_id']) {
                $subCategories = $this->_getSubCategoriesArr($this->request->data['typology_id']);
            }
            if ($this->request->data['subcategory_id']) {
                $devicesArr = $this->_getPublicDevicesBySubCategory($this->request->data['subcategory_id']);
                array_walk($devicesArr, function($val) use(&$devices) {
                    $devices[$val['id']] = $val['device_name'];
                });
            }
            if ($this->request->data['bid_doc']['size']) {
                $ext = pathinfo($this->request->data['bid_doc']['name'], PATHINFO_EXTENSION);
                $fileName = time() . '_' . $this->_generateRandomString(20) . '.' . $ext;
                $uploadFile = WWW_ROOT.'uploads/bid_instructions_documents/' . $fileName;
                if (move_uploaded_file($this->request->data['bid_doc']['tmp_name'], $uploadFile)) {
                    $this->request->data['bid_doc'] = $fileName;
                }
            } else {
                $this->request->data['bid_doc'] = $tender->bid_doc;
            }
            $deviceId = $this->request->data['device_id'];
            $usersTable = TableRegistry::get('Users');
            $userArr = [];
            if (isset($this->request->data['users'])) {
                $userArr = $usersTable->find('all')->select(['id', 'text' => 'display_name', 'company_code'])->where(['id IN' => $this->request->data['users']])->hydrate(false)->toArray();
            } else {
                $this->request->data['users'] = [];
            }
            array_walk($userArr, function($val) use(&$userOptions) {
                $userOptions[$val['id']] = $val['text'];
            });
            $tender = $this->Tenders->patchEntity($tender, $this->request->data);
            if ($tender->errors()) {
                $errors = $tender->errors();
                $userError = isset($errors['users']) ? reset($errors['users']) : '';
            }
            $saved = $this->Tenders->save($tender);
            if ($saved) {
                $savedArr = $saved->toArray();
                $tenderUsers = [];
                foreach ($this->request->data['users'] as $user) {
                    $tenderUsers[] = [
                        'tender_id' => $savedArr['id'],
                        'user_id' => $user
                    ];
                }
                if ($tenderUsers) {
                    $tenderUsersTable->query()->delete()->where(['tender_id' => $id])->execute();
                    $tenderUsersEnitites = $tenderUsersTable->newEntities($tenderUsers);
                    $tenderUsersTable->saveMany($tenderUsersEnitites);
                }
                if (date('Y-m-d', strtotime($savedArr['start_date'])) == date('Y-m-d') && $savedArr['publish_type'] == 0) {
                    $this->__publish($savedArr, 1);
                }
                $this->Flash->success(__('Tender successfully added'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Error in adding Tender'));
        }
        $userSegmentsTable = TableRegistry::get('AnnualTurnovers');
        $userSegmentsArr = $userSegmentsTable->find('all')->hydrate(false)->toArray();
        array_map(function($val) use(&$userSegments) {
            $userSegments[$val['id']] = $val['annual_turnover'];
        }, $userSegmentsArr);
        /* Public Categories ( Typologies ) */
        $typologiesArr = $this->_getPublicCategories();
        array_walk($typologiesArr, function($arr) use(&$typologies) {
            $typologies[$arr['id']] = $arr['title'];
        });
        $this->set(compact('devicePath'));
        $this->set(compact('deviceId'));
        $this->set(compact('typologies'));
        $this->set(compact('subCategories'));
        $this->set(compact('devices'));
        $this->set(compact('userError'));
        $this->set(compact('userOptions'));
        $this->set(compact('userSegments'));
        $this->set(compact('tender'));
    }

    public function publishManual($tenderId = null) {
        if(!$tenderId) {
            return $this->redirect(['action' => 'index']);
        }
        $this->autoRender = false;
        $tender = $this->Tenders->find('all')->select(['Tenders.id', 'Tenders.name', 'Tenders.device_id', 'Tenders.department_name', 'Tenders.tender_value', 'Tenders.issuing_authority', 'Tenders.authorised_by', 'Tenders.email', 'Tenders.start_date', 'Tenders.end_date', 'Tenders.min_amount', 'Tenders.contact', 'Tenders.web_link', 'Tenders.publish_type', 'Tenders.publish_status', 'Tenders.terms_cond', 'Tenders.created', 'Tenders.modified', 'users' => 'GROUP_CONCAT(u.user_id)'])
                        ->join([
                            'u' => [
                                'table' => 'tender_users',
                                'type' => 'LEFT',
                                'conditions' => 'Tenders.id = u.tender_id'
                            ]
                        ])->where(['Tenders.id' => $tenderId])
                        ->group('Tenders.id')
                        ->hydrate(false)->first();
        if($tender['publish_status'] == 1) {
            $this->Flash->error("Tender already published");
            return $this->redirect(['action' => 'index']);
        }
        $tender['users'] = explode(',', $tender['users']);
        $deviceIds = $this->_getTenderDeviceIds($tenderId);
        $tender['devices'] = $this->_getDevicesByIds($deviceIds);
        $this->__publish($tender, 1);
        $this->Flash->success(__('Tender Published Successfully'));
        return $this->redirect(['action' => 'index']);
    }

    public function tenderPublishCron() {
        $this->autoRender = false;
        $tendersArr = $this->Tenders
                ->find('all')
                ->select(['Tenders.id', 'Tenders.name', 'Tenders.device_id', 'Tenders.start_date', 'Tenders.end_date', 'Tenders.min_amount', 'Tenders.tender_value', 'Tenders.contact', 'Tenders.email', 'Tenders.issuing_authority', 'Tenders.authorised_by', 'Tenders.web_link', 'Tenders.publish_type', 'Tenders.publish_status', 'Tenders.created', 'Tenders.modified', 'Tenders.department_name', 'Tenders.terms_cond', 'users' => 'GROUP_CONCAT(u.user_id)'])
                ->join([
                    'u' => [
                        'table' => 'tender_users',
                        'type' => 'LEFT',
                        'conditions' => 'Tenders.id = u.tender_id'
                    ]
                ])
                ->where(['Tenders.publish_type' => '0', 'Tenders.publish_status' => '0', 'DATE(Tenders.start_date)' => date('Y-m-d')])
                ->group('Tenders.id')
                ->hydrate(false)
                ->toArray();
        $tenders = array_map(function($arr) {
            $arr['users'] = explode(',', $arr['users']);
            return $arr;
        }, $tendersArr);
//        $tenders = $this->Tenders->find('all')->where(['publish_type' => '0', 'publish_status' => '0', 'DATE(start_date)' => date('Y-m-d')])->hydrate(false)->toArray();
        $this->__publish($tenders);
        die("Published");
    }

    private function __publish($data = [], $post = NULL) {
        if ($post) {
            $data = [$data];
        }
        foreach ($data as $tender) {
            $mailData = $this->__getTenderData($tender);
            foreach ($mailData['userData'] as $item) {
                $this->getMailer('Tender')->send('tenderPublish', [$item, $tender]);
            }
            $this->Tenders->query()->update()->set(['publish_status' => 1, 'modified' => date('Y-m-d H:i:s')])->where(['id' => $tender['id']])->execute();
        }
    }

    public function getSegmentVendors() {
        $this->_ajaxCheck();
        $this->autoRender = false;
        $where = ['Users.type' => 'user', 'Users.status' => '1','paid' => '1'];
        if (isset($this->request->data['term'])) {
            $where['Users.display_name LIKE'] = $this->request->data['term'] . '%';
        }
        if (isset($this->request->data['segmentId']) && isset($this->request->data['experienceId'])) {
            $where['up.annual_turnover'] = $this->request->data['segmentId'];
            $where['up.experience'] = $this->request->data['experienceId'];
        }
        $usersTable = TableRegistry::get('Users');
        $response = $usersTable->find('all')->select(['Users.id', 'text' => 'Users.display_name', 'Users.company_code'])->join([
                    'up' => [
                        'table' => 'user_profiles',
                        'type' => 'LEFT',
                        'conditions' => 'Users.id = up.user_profile_id'
                    ]
                ])->where($where)->hydrate(false)->toArray();
        $this->_jsonResponse(['results' => $response]);
    }

    private function __getDevices($deviceId = null) {
        $companyDevices = [];
        $companyDevicesTable = TableRegistry::get('CompanyDevices');
        $companyDevicesArr = $companyDevicesTable->find('all')->select(['id', 'device_name'])->where(['status' => 'approved', 'devive_type' => '0', 'id' => $deviceId])->hydrate(false)->toArray();
        array_walk($companyDevicesArr, function($val) use(&$companyDevices) {
            $companyDevices[$val['id']] = $val['device_name'];
        });
        return $companyDevices;
    }

    public function getDeviceInfo() {
        $this->autoRender = false;
        $deviceId = $this->request->data['deviceId'];
        $response = $this->_getDeviceData($deviceId);
        $this->_jsonResponse($response);
    }

    private function __getTenderData($data) {
        $usersTable = TableRegistry::get('Users');
        $userArr = $usersTable->find('all')->select(['display_name', 'email'])->where(['id IN' => $data['users']])->hydrate(false)->toArray();
        //$deviceData = $this->_getDeviceData($data['device_id']);
        $resposne = [
            'userData' => $userArr,
            //'deviceData' => $deviceData
        ];
        return $resposne;
    }

    public function getDevice() {
        $this->autoRender = false;
        $term = $this->request->data['term'];
        $companyDevicesTable = TableRegistry::get('CompanyDevices');
        $response = $companyDevicesTable->find('all')->select(['id', 'text' => 'device_name'])->where(['status' => 'approved', 'devive_type' => '0', 'device_name LIKE' => '%' . $term . '%'])->hydrate(false)->toArray();
        $this->_jsonResponse(['results' => $response]);
    }

    public function liveBid($tenderId = null) {
        if (!$tenderId) {
            return $this->redirect(['action' => 'index']);
        }
        $tendersTable = TableRegistry::get('Tenders');
        $tender = $tendersTable->get($tenderId)->toArray();
        if (date('Y-m-d H:i:s', strtotime($tender['start_date'])) > date('Y-m-d H:i:s')) {
            $this->Flash->error(__('Biddig will start on ' . date('d M, Y h:iA', strtotime($tender['start_date'])) . '.'));
            return $this->redirect(['action' => 'index']);
        }
        $bidIncrimentPercentage = $this->bidIncrimentPercentage;
        $this->set(compact('bidIncrimentPercentage'));
        $this->set(compact('tender'));
    }

    public function getBids() {
        $this->autoRender = false;
        $this->_ajaxCheck();
        $response['error'] = 0;
        $bidsTable = TableRegistry::get('Bids');
        $tenderId = $this->request->data['id'];
        $lastestBid = 'Start Bidding';
        $tenderExpired = $this->_checkTenderExpired($tenderId);
        $bidsData = $bidsTable->find('all')->select(['name', 'amount'])->where(['tender_id' => $tenderId])->order(['id' => 'DESC'])->hydrate(false)->toArray();
//        $bidsData = $bidsTable->find('all')->select(['name' => 'sub.name', 'amount' => 'sub.amount'])->from(['sub' => '(SELECT * FROm bids WHERE tender_id = "' . $tenderId . '" ORDER BY id DESC)'])->group(['sub.user_id'])->order(['sub.amount' => 'DESC'])->hydrate(false)->toArray();
        $nextValue = 0;
        if (!$bidsData) {
            $tendersTable = TableRegistry::get('Tenders');
            $tender = $tendersTable->find('all')->where(['id' => $tenderId])->hydrate(false)->first();
            $nextValue = $tender['min_amount'];
        } else {
            $lastBid = reset($bidsData);
            $lastestBid = $lastBid['amount'];
            $nextValue = $lastBid['amount'] + (($lastBid['amount'] * $this->bidIncrimentPercentage) / 100);
        }
        $response['nextValue'] = $nextValue;
        $response['bids'] = $bidsData;
        $response['lastestBid'] = $lastestBid;
        $response['expirationTime'] = date('d M, Y h:iA', strtotime($this->_getTenderExpirationTime($tenderId)));
        if ($tenderExpired) {
            $response['error'] = 1;
            $response['title'] = 'Time Over!';
            $response['message'] = 'Bidding time Over. We will get back to you soon.';
        }
        $this->_jsonResponse($response);
    }

    public function getPublicDevicesBySubCategory() {
        $this->autoRender = false;
        $this->_ajaxCheck();
        $subCategoryId = $this->request->data['id'];
        $devicesArr = $this->_getPublicDevicesBySubCategory($subCategoryId);
        $devices = array_map(function($arr) {
            return ['id' => $arr['id'], 'text' => $arr['device_name']];
        }, $devicesArr);
        $this->_jsonResponse($devices);
    }

    /*
     * Tender Applications 
     */

    public function tenderApplications() {
        $limit = 10;
        $this->paginate = [
            'limit' => $limit
        ];
        $tendersData = $this->Tenders->find('all')->select([
            'Tenders.id',
            'Tenders.name',
            'Tenders.start_date',
            'end_date' => 'Tenders.end_date_updated',
            'Tenders.min_amount',
            'Tenders.contact',
            'applied_bids' => 'IF(ab.applied_bids IS NULL OR ab.applied_bids = "",0,ab.applied_bids)'
        ])->join([
            'ab' => [
                'table' => '(SELECT `tender_id`, COUNT(`user_id`) AS `applied_bids` FROM `applied_bids` WHERE `status` = "0" AND `paid` = "1" GROUP BY `tender_id`)',
                'type' => 'LEFT',
                'conditions' => 'Tenders.id = ab.tender_id'
            ]
        ])->where(['Tenders.publish_status' => '1'])->order([
            'ab.applied_bids' => 'desc',
            'Tenders.start_date' => 'asc',
            'Tenders.created' => 'desc'
        ]);
        $tenders = $this->paginate($tendersData);
        $this->set(compact('limit'));
        $this->set(compact('tenders'));
    }
    
    public function tenderApplicationsList($tenderId = null) {
        if(!$tenderId) {
            return $this->redirect(['action' => 'tenderApplications']);
        }
        $limit = 10;
        $this->paginate = [
            'limit' => $limit
        ];
        $tender = $this->_tenderDetail($tenderId);
        $appliedBidsData = $this->_getAppliedBids($tenderId);
        $appliedBids = $this->paginate($appliedBidsData)->toArray();
        $this->set(compact('tender','appliedBids','limit'));
    }
    
    public function tenderApplication($appliedBidId = null) {
        if(!$appliedBidId) {
            return $this->redirect(['action' => 'tenderApplications']);
        }
        $appliedBid = $this->_getAppliedBid($appliedBidId);
        if(!$appliedBid['paid']) {
            $this->redirect(['action' => 'tenderApplications']);
        }
        $appliedBidsTable = TableRegistry::get('AppliedBids');
        $post = $appliedBidsTable->newEntity();
        if($this->request->is('post')) {
            if($appliedBid['status']) {
                exit("You are not allowed to access this URL.");
            }
            $post = $appliedBidsTable->patchEntity($post, $this->request->data);
            $post->id = $appliedBidId;
            $appliedBidsTable->save($post);
            $this->Flash->success("Tender Application successfully proccessed.");
            $this->redirect(['action' => 'tenderApplicationsList',$appliedBid['tender_id']]);
        }
        $appliedBidDocuments = $this->_getAppliedBidDocuments($appliedBidId);
        $tender = $this->_tenderDetail($appliedBid['tender_id']);
        $tenderDocuments = $this->_getTenderRequiredDocuments($appliedBid['tender_id']);
        $deviceIds  = $this->_getTenderDeviceIds($appliedBid['tender_id']);
        $devices = $this->_getDevicesByIds($deviceIds);
        $devicePath = Router::url('/') . 'uploads/devices/';
        $this->set(compact('appliedBid','appliedBidDocuments','tender','tenderDocuments','devices','post','devicePath'));
    }
    
    public function getUserDetail() {
        $this->autoRender = false;
        $this->_ajaxCheck();
        $userId = $this->request->data['user_id'];
        if(!$userId) {
            exit("You are not allowed to access this url.");
        }
        $data = $this->_getUserProfileDetail($userId);
        $data['incorporation_date'] = date('d M, Y',strtotime($data['incorporation_date']));
        $data['income_proof'] = Router::url('/uploads/register_documents/'.$data['income_proof']);
        $data['income_proof_ext'] = $ext = pathinfo($data['income_proof'], PATHINFO_EXTENSION);
        $data['turnover_certificate'] = Router::url('/uploads/register_documents/'.$data['turnover_certificate']);
        $data['turnover_certificate_ext'] = $ext = pathinfo($data['turnover_certificate'], PATHINFO_EXTENSION);
        $data['exp_certificate'] = Router::url('/uploads/register_documents/'.$data['exp_certificate']);
        $data['exp_certificate_ext'] = $ext = pathinfo($data['exp_certificate'], PATHINFO_EXTENSION);
        $response['error'] = 0;
        $response['data'] = $data;
        $this->_jsonResponse($response);
    }
    
    public function view($tenderId = null) {
        if(!$tenderId) {
            return $this->redirect(['action' => 'index']);
        }
        $tender = $this->_tenderDetail($tenderId);
        if($tender['status'] != 1) {
            $this->Flash->error("You can not access this page without tender approval");
            return $this->redirect(['action' => 'index']);
        } else if($tender['publish_status'] != 0) {
            $this->Flash->error("You can not access this page after tender publish");
            return $this->redirect(['action' => 'index']);
        }
        $annualTurnover = $this->_getUsersAnualTurnover($tender['user_segment']);
        $experience = $this->_getUsersExperience($tender['experience']);
        $tenderDocuments = $this->_getTenderRequiredDocuments($tenderId);
        $tenderUsers = $this->_getTenderUsers($tenderId);
        $tenderDeviceIds = $this->_getTenderDeviceIds($tenderId);
        $devices = $this->_getDevicesByIds($tenderDeviceIds);
        $devicePath = Router::url('/') . 'uploads/devices/';
        $this->set(compact('devicePath','tender', 'devices', 'tenderUsers', 'tenderDocuments', 'annualTurnover', 'experience'));
    }

}
