<?php

namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use App\Traits\Devices;
use App\Traits\Users;
use Cake\Routing\Router;

class BidsController extends AppController {

    use Devices,
        Users;

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->viewBuilder()->layout('dashboard');
    }

    public function index() {
        $limit = 10;
        $this->paginate = [
            'limit' => $limit
        ];
        $tendersTable = TableRegistry::get('Tenders');
        $tendersArr = $tendersTable->find('all')->join([
                    'at' => [
                        'table' => '(SELECT DISTINCT tender_id FROM applied_bids ORDER BY created DESC)',
                        'type' => 'RIGHT',
                        'conditions' => 'Tenders.id = at.tender_id'
                    ]
                ])->hydrate(false);
        $tenders = $this->paginate($tendersArr)->toArray();
        $this->set(compact('limit'));
        $this->set(compact('tenders'));
    }

    public function appliedBids($tenderId = null) {
        if (!$tenderId) {
            return $this->redirect(['controller' => 'Dashboard']);
        }
        $limit = 10;
        $this->paginate = [
            'limit' => $limit
        ];
        $deviceImagePath = Router::url('/') . 'uploads/devices/';
        $bidDocumentsPath = Router::url('/') . 'uploads/bidDocuments/';
        $appliedBidsTable = TableRegistry::get('AppliedBids');
        $appliedBidsArr = $appliedBidsTable->find('all')->select(['AppliedBids.id', 'AppliedBids.tender_id', 'AppliedBids.user_id', 'tender_name' => 't.name', 'applied_by' => 'u.display_name', 'start_date' => 't.start_date', 'end_date' => 't.end_date', 'device_name' => 'd.device_name', 'AppliedBids.status'])->join([
                    't' => [
                        'table' => 'tenders',
                        'conditions' => 'AppliedBids.tender_id = t.id',
                        'type' => 'LEFT'
                    ],
                    'd' => [
                        'table' => 'company_devices',
                        'conditions' => 't.device_id = d.id',
                        'type' => 'LEFT'
                    ],
                    'u' => [
                        'table' => 'users',
                        'conditions' => 'AppliedBids.user_id = u.id',
                        'type' => 'LEFT'
                    ]
                ])->where(['AppliedBids.tender_id' => $tenderId])->hydrate(false);
        $appliedBids = $this->paginate($appliedBidsArr)->toArray();
        $this->set(compact('bidDocumentsPath'));
        $this->set(compact('deviceImagePath'));
        $this->set(compact('limit'));
        $this->set(compact('appliedBids'));
    }

    public function getAppliedBidDetail() {
        $this->autoRender = false;
        if (!$this->request->is('ajax')) {
            exit('You are not allowed to Access this URL');
        }
        $response['error'] = 0;
        $appliedBidId = $this->request->data['id'];
        $appliedBidsTable = TableRegistry::get('AppliedBids');
        $appliedBid = $appliedBidsTable->find('all')->select(['AppliedBids.id', 'AppliedBids.user_id', 'AppliedBids.tender_id', 'device_id' => 't.device_id'])->join([
                    't' => [
                        'table' => 'tenders',
                        'conditions' => 'AppliedBids.tender_id = t.id',
                        'type' => 'LEFT'
                    ]
                ])->where(['AppliedBids.id' => $appliedBidId])->hydrate(false)->first();

        $bidDocumentsTable = TableRegistry::get('BidDocuments');
        $response['bidDocuments'] = $bidDocumentsTable->find('all')->select(['doc_name', 'doc_file'])->where(['application_id' => $appliedBid['id']])->hydrate(false)->toArray();
        $response['userData'] = $this->_getUserData($appliedBid['user_id']);
        $response['deviceData'] = $this->_getDeviceData($appliedBid['device_id']);
        $response['deviceImagesKeys'] = ['site_image', 'sketch_plan_image', 'structural_engineer_certificate', 'site_drawings'];
        $this->_jsonResponse($response);
    }

}
