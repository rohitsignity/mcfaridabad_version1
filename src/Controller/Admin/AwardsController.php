<?php

namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use App\Traits\Tenders;
use App\Traits\Users;
use App\Traits\Devices;
use App\Traits\Bids;
use Cake\Mailer\MailerAwareTrait;
use Cake\Routing\Router;

class AwardsController extends AppController {

    use Tenders,
        Users,
        Devices,
        Bids,
        MailerAwareTrait;

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->viewBuilder()->layout('dashboard');
        $this->Auth->allow(['index', 'awardTender']);
    }

    public function index() {
        $this->_ajaxCheck();
    }

    public function awardTender() {
        $this->autoRender = false;
        $bidsTable = TableRegistry::get('Bids');
        $tendersTable = TableRegistry::get('Tenders');
        $awardsTable = TableRegistry::get('Awards');
        $notAwardedTenders = $tendersTable->find('all')->where(['publish_status' => '1', 'is_awarded' => '0'])->hydrate(false)->toArray();
        $expiredTenders = array_filter($notAwardedTenders, function($arr) {
            $getExpirationTime = $this->_getTenderExpirationTime($arr['id']);
            return $getExpirationTime < date('Y-m-d H:i:s');
        });
        foreach ($expiredTenders as $expiredTender) {
            $lastBid = $bidsTable->find('all')->where(['tender_id' => $expiredTender['id']])->order(['amount' => 'DESC'])->hydrate(false)->first();
            if ($lastBid) {
                $this->__notifyAward($expiredTender, $lastBid);
                $tendersTable->query()->update()->set(['is_awarded' => '1', 'modified' => date('Y-m-d H:i:s')])->where(['id' => $expiredTender['id']])->execute();
                $awardsEntity = $awardsTable->newEntity(['bid_id' => $lastBid['id']]);
                $awardsTable->save($awardsEntity);
            }
        }
        die("Awarded");
    }

    private function __notifyAward($tenderData, $bidData) {
        $this->loadComponent('Sms');
        $userData = $this->_getUserDetail($bidData['user_id']);
        $message = 'Congratulations! You have won the tender. Please contact SMC for further process.';
        $this->Sms->sendTextSms($userData['mobile'],$message);
        $this->getMailer('Tender')->send('notifyAward', [$userData, $tenderData]);
    }

    public function awardDetail($tenderId = null) {
        $id = $tenderId;
        $this->set(compact('id'));
    }

    public function getAwardDetail() {
        $this->autoRender = false;
        $this->_ajaxCheck();
        if ($this->request->is('post')) {
            $response = [];
            $postData = array_values(array_filter(explode('_', $this->request->data['mask'])));
            $timeStamp = reset($postData);
            $id = end($postData);
            $expireTimeStamp = strtotime('+15 minutes', $timeStamp);
            if (time() <= $expireTimeStamp) {
                $tenderData = $this->_tenderDetail($id);
                if ($this->_getTenderExpirationTime($id) < date('Y-m-d H:i:s')) {
                    $bidsTable = TableRegistry::get('Bids');
                    $awardsTable = TableRegistry::get('Awards');
                    $tendersTable = TableRegistry::get('Tenders');
                    $lastBid = $bidsTable->find('all')->where(['tender_id' => $id])->order(['amount' => 'DESC'])->hydrate(false)->first();
                    if (!$tenderData['is_awarded']) {
                        if ($lastBid) {
                            $this->__notifyAward($tenderData, $lastBid);
                            $tendersTable->query()->update()->set(['is_awarded' => '1', 'modified' => date('Y-m-d H:i:s')])->where(['id' => $tenderData['id']])->execute();
                            $awardsEntity = $awardsTable->newEntity(['bid_id' => $lastBid['id']]);
                            $awardsTable->save($awardsEntity);
                        }
                    }
                    $response['error'] = 0;
                    $response['title'] = 'success';
                    $response['message'] = 'success';
                    $response['data'] = [
                        'user' => $this->_getAwardedBidUser($tenderData['id']),
                        'tender' => $tenderData
                    ];
                    $response['url'] = Router::url(['controller' => 'Tenders','action' => 'index']);
                } else {
                    $response['error'] = 2;
                    $response['title'] = 'Permission Denied';
                    $response['message'] = 'You are not allowed to access this page';
                    $response['url'] = Router::url(['controller' => 'Dashboard','action' => 'index']);
                }
            } else {
                $response['error'] = 1;
                $response['title'] = 'Expired';
                $response['message'] = 'Page has been expired.';
                $response['url'] = Router::url(['controller' => 'Tenders','action' => 'index']);
            }
            $this->_jsonResponse($response);
        }
    }

}
