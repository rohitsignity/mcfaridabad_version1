<?php

namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Event\Event;

class DocumentsController extends AppController {

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        return $this->redirect(['controller' => 'Dashboard','action' => 'index']);
        $this->viewBuilder()->layout('dashboard');
    }

    public function index() {
        $limit = 10;
        $page = isset($this->request->query['page']) ? $this->request->query['page'] : 1;
        $this->paginate = [
            'limit' => $limit
        ];
        $documents = $this->paginate($this->Documents)->toArray();
        $this->set(compact('documents','page','limit'));
    }

    public function add() {
        $document = $this->Documents->newEntity();
        if ($this->request->is('post')) {
            $document = $this->Documents->patchEntity($document, $this->request->data);
            if ($this->Documents->save($document)) {
                $this->Flash->success("Document successfully added");
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set(compact('document'));
    }
    
    public function addAjax() {
        $this->autoRender = false;
        $this->_ajaxCheck();
        $response = [];
        $document = $this->Documents->newEntity();
        if ($this->request->is('post')) {
            $document = $this->Documents->patchEntity($document, $this->request->data);
            if(!$document->errors()) {
                $response['error'] = 0;
                $response['data'] = $this->Documents->save($document);
            } else {
                $response['error'] = 1;
                $response['data'] = $document->errors();
            }
        }
        $this->_jsonResponse($response);
    }

}
