<?php

namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Routing\Router;

class SuperAdminsController extends AppController {
    
    public function index() {
        die;
        $superAdmins = $this->paginate($this->SuperAdmins);
        $this->set(compact('superAdmins'));
        $this->set('_serialize', ['superAdmins']);
    }

    public function view($id = null) {
        die;
        $superAdmin = $this->SuperAdmins->get($id, [
            'contain' => []
        ]);

        $this->set('superAdmin', $superAdmin);
        $this->set('_serialize', ['superAdmin']);
    }

    public function add() {
        die;
        $superAdmin = $this->SuperAdmins->newEntity();
        if ($this->request->is('post')) {
            $superAdmin = $this->SuperAdmins->patchEntity($superAdmin, $this->request->data);
            if ($this->SuperAdmins->save($superAdmin)) {
                $this->Flash->success(__('The super admin has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The super admin could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('superAdmin'));
        $this->set('_serialize', ['superAdmin']);
    }

    public function edit($id = null) {
        die;
        $superAdmin = $this->SuperAdmins->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $superAdmin = $this->SuperAdmins->patchEntity($superAdmin, $this->request->data);
            if ($this->SuperAdmins->save($superAdmin)) {
                $this->Flash->success(__('The super admin has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The super admin could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('superAdmin'));
        $this->set('_serialize', ['superAdmin']);
    }

    public function delete($id = null) {
        die;
        $this->request->allowMethod(['post', 'delete']);
        $superAdmin = $this->SuperAdmins->get($id);
        if ($this->SuperAdmins->delete($superAdmin)) {
            $this->Flash->success(__('The super admin has been deleted.'));
        } else {
            $this->Flash->error(__('The super admin could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function authanticateSuperAdmin() {
        $this->autoRender = false;
//        $this->_ajaxCheck();
        if ($this->request->is('post')) {
            $response = [];
            $level = $this->request->data['level'];
            $password = $this->request->data['password'];
            $superAdmin = $this->SuperAdmins->find('all')->where(['level' => $level])->hydrate(false)->first();
            $checkPassword = (new DefaultPasswordHasher)->check($password, $superAdmin['password']);
            $response['url'] = false;
            if ($checkPassword) {
                $response['error'] = 0;
                $response['message'] = 'Success';
                if ($level == 3) {
                    $response['url'] = Router::url(['controller' => 'Awards', 'action' => 'awardDetail']);
                    $response['mask'] = time();
                }
            } else {
                $response['error'] = 1;
                $response['message'] = 'Password does not matched';
            }
            $this->_jsonResponse($response);
        }
    }

}
