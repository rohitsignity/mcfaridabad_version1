<?php

namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Mailer\Email;
use Cake\Mailer\MailerAwareTrait;
use Cake\Routing\Router;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\TableRegistry;
use App\Traits\Users;

class UsersController extends AppController {

    use MailerAwareTrait, Users;

    public $paginate = [
        'limit' => 10
    ];

    public function initialize() {
        parent::initialize();
    }

    public function index() {
        $this->viewBuilder()->layout('dashboard');
        $addressProfesData = TableRegistry::get('AddressProfes');
        $addressProfesArr = $addressProfesData->find('all')->hydrate(false)->toArray();
        $usersQuery = $this->Users->find('all')->select([
                    'id',
                    'display_name',
                    'email',
                    'mobile',
                    'type',
                    'status',
                    'created',
                    'representative_id',
                    'representative' => 'r.representative',
                    'annual_turnover' => 'turnover.annual_turnover',
                    'company_name' => 'display_name',
                    'company_address' => 'up.company_address',
                    'director_name' => 'up.director_name',
                    'company_email' => 'up.company_email',
                    'SMC_party_code' => 'up.SMC_party_code',
                    'property_id' => 'up.property_id',
                    'incorporation_date' => 'up.incorporation_date',
                    'property_tax_receipt' => 'up.property_tax_receipt',
                    'income_tax_return' => 'up.income_tax_return',
                    'din_number' => 'up.din_number',
                    'income_proof' => 'up.income_proof',
                    'voter_id' => 'up.voter_id',
                    'adhaar_card' => 'up.adhaar_card',
                    'service_tax' => 'up.service_tax',
                    'turnover_certificate' => 'up.turnover_certificate',
                    'exp_certificate' => 'up.exp_certificate',
                    'balance_sheet' => 'up.balance_sheet',
                    'letter_by_board_directors' => 'up.letter_by_board_directors',
                    'incorporation_certificate' => 'up.incorporation_certificate',
                    'association_memorandum' => 'up.association_memorandum',
                    'association_articles' => 'up.association_articles',
                    'ration_card' => 'up.ration_card',
                    'driving_license' => 'up.driving_license',
                    'passport' => 'up.passport',
                    'tin_no' => 'up.tin_no',
                    'partnership_deed' => 'up.partnership_deed',
                    'address_proof_partnership_firm' => 'up.address_proof_partnership_firm',
                    'partnership_registration_certificate' => 'up.partnership_registration_certificate',
                    'pan_card_partners' => 'up.pan_card_partners',
                    'pan_card_llp' => 'up.pan_card_llp',
                    'registration_certificate_llp' => 'up.registration_certificate_llp',
                    'address_proof_llp' => 'up.address_proof_llp',
                    'address_proof_partners' => 'up.address_proof_partners',
                    'audited_balance_sheet' => 'up.audited_balance_sheet',
                    'power_of_attorney' => 'up.power_of_attorney',
                    'authorised_contact_person' => 'up.authorised_contact_person',
                    'undertaking_non_blacklisting' => 'up.undertaking_non_blacklisting',
                    'no_dues_pending' => 'up.no_dues_pending',
                    'advertising_experience' => 'up.advertising_experience',
                    'details_advertisement_permissions' => 'up.details_advertisement_permissions',
                    'partner_cv' => 'up.partner_cv',
                    'directors_pan_card' => 'up.directors_pan_card',
                    'company_pan_card' => 'up.company_pan_card',
                    'company_registration_certificate' => 'up.company_registration_certificate',
                    'company_address_proof' => 'up.company_address_proof',
                    'directors_address_proof' => 'up.directors_address_proof',
                    'organization_pan_card' => 'up.organization_pan_card',
                    'director_cv' => 'up.director_cv',
                    'organization_registration_certificate' => 'up.organization_registration_certificate',
                    'organisation_address_proof' => 'up.organisation_address_proof',
                    'address_type' => 'up.address_type',
                    'address_proof' => 'up.address_proof'
                ])->join([
                    'r' => [
                        'table' => 'representatives',
                        'type' => 'LEFT',
                        'conditions' => 'Users.representative_id = r.id'
                    ],
                    'up' => [
                        'table' => 'user_profiles',
                        'type' => 'LEFT',
                        'conditions' => 'Users.id = up.user_profile_id'
                    ],
                    'turnover' => [
                        'table' => 'annual_turnovers',
                        'type' => 'LEFT',
                        'conditions' => 'up.annual_turnover = turnover.id'
                    ]
                ])->where(['Users.type' => 'user', 'Users.status' => '1', 'Users.paid' => '1', 'Users.admin_approved' => '2'])->order(['Users.id' => 'DESC'])->hydrate(false);
        $users = $this->paginate($usersQuery)->toArray();
        $userIds = array_map(function($arr){
            return $arr['id'];
        }, $users);
        $usersDocuments = $this->_getUsersDocumentsbyIds($userIds);
        $userDetails = [];
        $entityDetails = [];
        $address = [];
        array_walk($users, function($arr) use(&$userDetails, &$entityDetails, $addressProfesArr) {
            $userDetails[$arr['id']] = [
                ['label' => 'Company Name', 'value' => $arr['display_name'], 'type' => 'field'],
                ['label' => 'Email', 'value' => $arr['email'], 'type' => 'field'],
                ['label' => 'Mobile', 'value' => $arr['mobile'], 'type' => 'field'],
                ['label' => 'Type', 'value' => $arr['type'], 'type' => 'field'],
                ['label' => 'Property Id', 'value' => $arr['property_id'], 'type' => 'field'],
                ['label' => 'Status', 'value' => ($arr['status'] == 1) ? 'Enable' : '', 'type' => 'field'],
                ['label' => 'Representative_id', 'value' => $arr['representative_id'], 'type' => 'field'],
                ['label' => 'Authorised Contact Person', 'value' => $arr['authorised_contact_person'], 'type' => 'field']
            ];
            $fileArr = [];
            if (in_array($arr['representative_id'], [1, 2, 3, 4, 5, 6])) {
                $addressType = $arr['address_type'];
                $addressTypeArr = array_values(array_filter($addressProfesArr, function($item) use($addressType) {
                            return $item['id'] == $addressType;
                        }));
                if($addressTypeArr) {
                    array_push($fileArr, ['label' => $addressTypeArr[0]['name'], 'value' => Router::url('/uploads/register_documents/' . $arr['address_proof']), 'type' => 'file']);
                }
            }
            if (in_array($arr['representative_id'], [1, 2, 3])) {
                array_push($fileArr, ['label' => 'Pan Card', 'value' => Router::url('/uploads/register_documents/' . $arr['income_proof']), 'type' => 'file']);
            }
            if (in_array($arr['representative_id'], [2, 3, 4, 5, 6])) {
                array_push($fileArr, ['label' => 'TIN no', 'value' => Router::url('/uploads/register_documents/' . $arr['tin_no']), 'type' => 'file']);
                array_push($fileArr, ['label' => 'Service tax no', 'value' => Router::url('/uploads/register_documents/' . $arr['service_tax']), 'type' => 'file']);
            }
            if (in_array($arr['representative_id'], [1, 2, 3, 4, 5, 6])) {
                array_push($fileArr, ['label' => 'Income tax return for the last year', 'value' => Router::url('/uploads/register_documents/' . $arr['income_tax_return']), 'type' => 'file']);
                array_push($fileArr, ['label' => 'Undertaking of non-Blacklisting', 'value' => Router::url('/uploads/register_documents/' . $arr['undertaking_non_blacklisting']), 'type' => 'file']);
                array_push($fileArr, ['label' => 'Undertaking of No dues pending with any municipality of Haryana', 'value' => Router::url('/uploads/register_documents/' . $arr['no_dues_pending']), 'type' => 'file']);
                array_push($fileArr, ['label' => 'Experience in the field of advertising in last three years', 'value' => Router::url('/uploads/register_documents/' . $arr['advertising_experience']), 'type' => 'file']);
                array_push($fileArr, ['label' => 'Details of advertisement rights/permissions secured in last 5 years in any of the municipality of Haryan', 'value' => Router::url('/uploads/register_documents/' . $arr['details_advertisement_permissions']), 'type' => 'file']);
                array_push($fileArr, ['label' => 'Curriculum Vitae', 'value' => Router::url('/uploads/register_documents/' . $arr['partner_cv']), 'type' => 'file']);
            }
            if (in_array($arr['representative_id'], [3])) {
                array_push($fileArr, ['label' => 'Partnership Deed', 'value' => Router::url('/uploads/register_documents/' . $arr['partnership_deed']), 'type' => 'file']);
                array_push($fileArr, ['label' => 'Address Proof of the Partnership Firm', 'value' => Router::url('/uploads/register_documents/' . $arr['address_proof_partnership_firm']), 'type' => 'file']);
            }
            if ($arr['partnership_registration_certificate']) {
                array_push($fileArr, ['label' => 'Partnership Registration Certificate', 'value' => Router::url('/uploads/register_documents/' . $arr['partnership_registration_certificate']), 'type' => 'file']);
            }
            if (in_array($arr['representative_id'], [4])) {
                array_push($fileArr, ['label' => 'PAN Card of the Partners', 'value' => Router::url('/uploads/register_documents/' . $arr['pan_card_partners']), 'type' => 'file']);
                array_push($fileArr, ['label' => 'PAN card of LLP', 'value' => Router::url('/uploads/register_documents/' . $arr['pan_card_llp']), 'type' => 'file']);
                array_push($fileArr, ['label' => 'Registration certificate of LLP', 'value' => Router::url('/uploads/register_documents/' . $arr['registration_certificate_llp']), 'type' => 'file']);
                array_push($fileArr, ['label' => 'Address proof of LLP', 'value' => Router::url('/uploads/register_documents/' . $arr['address_proof_llp']), 'type' => 'file']);
                array_push($fileArr, ['label' => 'Address Proof of the Partners', 'value' => Router::url('/uploads/register_documents/' . $arr['address_proof_partners']), 'type' => 'file']);
            }
            if (in_array($arr['representative_id'], [4, 5])) {
                array_push($fileArr, ['label' => 'DIN no', 'value' => Router::url('/uploads/register_documents/' . $arr['din_number']), 'type' => 'file']);
            }
            if ($arr['audited_balance_sheet']) {
                array_push($fileArr, ['label' => 'Audited Balance sheet of last three years', 'value' => Router::url('/uploads/register_documents/' . $arr['audited_balance_sheet']), 'type' => 'file']);
            }
            if (in_array($arr['representative_id'], [4, 5, 6])) {
                array_push($fileArr, ['label' => 'Authorization/power of attorney for authorised signatory/person', 'value' => Router::url('/uploads/register_documents/' . $arr['power_of_attorney']), 'type' => 'file']);
//                array_push($fileArr, ['label' => 'Authorised Contact Person', 'value' => Router::url('/uploads/register_documents/' . $arr['authorised_contact_person']), 'type' => 'file']);
                array_push($fileArr, ['label' => 'Memorandum of Association', 'value' => Router::url('/uploads/register_documents/' . $arr['association_memorandum']), 'type' => 'file']);
            }
            if (in_array($arr['representative_id'], [5])) {
                array_push($fileArr, ['label' => 'PAN Card of the directors', 'value' => Router::url('/uploads/register_documents/' . $arr['directors_pan_card']), 'type' => 'file']);
                array_push($fileArr, ['label' => 'PAN card of Company', 'value' => Router::url('/uploads/register_documents/' . $arr['company_pan_card']), 'type' => 'file']);
                array_push($fileArr, ['label' => 'Registration certificate of company', 'value' => Router::url('/uploads/register_documents/' . $arr['company_registration_certificate']), 'type' => 'file']);
                array_push($fileArr, ['label' => 'Address proof of company', 'value' => Router::url('/uploads/register_documents/' . $arr['company_address_proof']), 'type' => 'file']);
                array_push($fileArr, ['label' => 'Address Proof of the Directors', 'value' => Router::url('/uploads/register_documents/' . $arr['directors_address_proof']), 'type' => 'file']);
                array_push($fileArr, ['label' => 'Curriculum Vitae of each Director', 'value' => Router::url('/uploads/register_documents/' . $arr['director_cv']), 'type' => 'file']);
            }
            if (in_array($arr['representative_id'], [5, 6])) {
                array_push($fileArr, ['label' => 'Articles of Association', 'value' => Router::url('/uploads/register_documents/' . $arr['association_articles']), 'type' => 'file']);
            }
            if (in_array($arr['representative_id'], [6])) {
                array_push($fileArr, ['label' => 'PAN Card of the Organization', 'value' => Router::url('/uploads/register_documents/' . $arr['organization_pan_card']), 'type' => 'file']);
                array_push($fileArr, ['label' => 'Registration certificate of Organization', 'value' => Router::url('/uploads/register_documents/' . $arr['organization_registration_certificate']), 'type' => 'file']);
                array_push($fileArr, ['label' => 'Address proof of Organisation', 'value' => Router::url('/uploads/register_documents/' . $arr['organisation_address_proof']), 'type' => 'file']);
            }
            if ($fileArr) {
                $userDetails[$arr['id']] = array_merge($userDetails[$arr['id']], $fileArr);
            }
            $entityDetails[$arr['id']] = [
                ['label' => 'Company Name', 'value' => $arr['display_name']],
                ['label' => 'Company Address', 'value' => $arr['company_address']],
                ['label' => 'Representative', 'value' => $arr['representative']],
                ['label' => 'Annual Turnover', 'value' => $arr['annual_turnover']],
                ['label' => 'SMC Party Code', 'value' => $arr['SMC_party_code']],
                ['label' => 'Date of Incorporation', 'value' => date('d-m-Y', strtotime($arr['incorporation_date']))]
            ];
            if ($arr['director_name']) {
                $entityDetails[$arr['id']] = array_merge($entityDetails[$arr['id']], [['label' => 'Director Name', 'value' => $arr['director_name']]]);
            }
        });
        $this->set(compact('users'));
        $this->set('userDetails', addslashes(json_encode($userDetails)));
        $this->set('entityDetails', addslashes(json_encode($entityDetails)));
        $this->set('usersDocuments', addslashes(json_encode($usersDocuments)));
    }

    public function mcgUsers() {
        $this->viewBuilder()->layout('dashboard');
        $usersQuery = $this->Users->find('all')->where(['type LIKE' => 'MCG%'])->order(['id' => 'DESC'])->hydrate(false);
        $users = $this->paginate($usersQuery)->toArray();
        $this->set(compact('users'));
    }

    public function login() {
        if ($this->Auth->user('id')) {
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }
        $post = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $post = $this->Users->patchEntity($post, $this->request->data, ['validate' => 'Login']);
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
            }
            $this->Flash->error(__('Invalid Username/Password.'));
        }
        $this->set(compact('post'));
    }

    public function logout() {
        $this->Flash->error(__('You are successfully Logged out'));
        return $this->redirect($this->Auth->logout());
    }

    public function add() {
        $this->viewBuilder()->layout('dashboard');
        $post = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $post = $this->Users->patchEntity($post, $this->request->data, ['validate' => 'McgAdd']);
            $post->status = 1;
            if ($this->Users->save($post)) {
                $this->Flash->success(__('MCG User added successfully'));
                return $this->redirect(['controller' => 'Users', 'action' => 'mcgUsers']);
            }
        }
        $this->set(compact('post'));
    }

    public function edit($id = null) {
        $this->viewBuilder()->layout('dashboard');
        $user = $this->Users->get($id);
        if ($this->request->is(['post', 'put'])) {
            $this->Users->patchEntity($user, $this->request->data, ['validate' => 'McgAdd']);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('MCG User updated successfully'));
                return $this->redirect(['controller' => 'Users', 'action' => 'mcgUsers']);
            }
        }
        unset($user->password);
        $this->set(compact('user'));
    }

    public function approveUsers() {
        $this->autoRender = false; // avoid to render view
        if ($this->request->is('ajax')) {
            if ($this->request->is('post')) {
                $response['error'] = 0;
                $userId = $this->request->data['user_id'];
                $value = $this->request->data['value'];
                $rejection_reason = $this->request->data['rejection_reason'];
                if (!empty($userId) && !empty($value)) {
                    if ($value == 1) {
                        $status = 1;
                        $approved = 1;
                    } else if ($value == 2) {
                        $status = 2;
                        $approved = 0;
                    }
                    $updateStatus = $this->Users->query()->update()->set(['status' => $status, 'rejection_reason' => $rejection_reason, 'admin_approved' => $approved])->where(['id' => $userId])->execute();
                    if (count($updateStatus) > 0) {
                        $this->loadComponent('Sms');
                        $userData = $this->Users->find()->where(['id' => $userId])->first();
                        if ($value == 1) {
                            $this->Sms->sendTextSms($userData->mobile, 'You account has been approved by Administrator. You can now access your dashboard.');
                            $response['data'] = 'User has been successfully approved.';
                            //$this->getMailer('User')->send('emailVerification', [$userData]);
                            $this->getMailer('User')->send('approvedEmail', [$userData]);
                        } else if ($value == 2) {
                            $this->Sms->sendTextSms($userData->mobile, 'You account has been rejected by Administrator. You can register again or contact MCG support.');
                            $response['data'] = 'User has been rejected.';
                            $this->getMailer('User')->send('rejectedEmail', [$userData]);
                        }
                    } else {
                        $response['error'] = 1;
                        $response['data'] = 'User has not been approved.';
                    }
                } else {
                    $response['error'] = 1;
                    $response['data'] = 'User has not been approved.';
                }
            }
            echo addslashes(json_encode($response));
        }
    }

    public function authanticateAdmin() {
        $this->autoRender = false;
        $this->_ajaxCheck();
        if ($this->request->is('post')) {
            $response = [];
            $password = $this->request->data['password'];
            $userId = $this->Auth->user('id');
            $user = $this->Users->get($userId);
            $passwordmatched = (new DefaultPasswordHasher)->check($password, $user->password);
            if ($passwordmatched) {
                $response['error'] = 0;
                $response['message'] = 'Success';
            } else {
                $response['error'] = 1;
                $response['message'] = 'Password does not matched';
            }
            $this->_jsonResponse($response);
        }
    }

}
