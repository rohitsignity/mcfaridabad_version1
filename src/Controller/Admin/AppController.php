<?php

namespace App\Controller\Admin;

use Cake\Controller\Controller;
use Cake\Event\Event;

/**
 * Description of AppController
 *
 * @author yogesh
 */
class AppController extends Controller {

    private $user;
    protected $bidIncrimentPercentage = 10;

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
    }

    public function initialize() {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login',
            ],
            'loginRedirect' => [
                'controller' => 'dashboard',
                'action' => 'index'
            ],
            'authError' => 'Did you really think you are allowed to see that?',
            'authenticate' => [
                'Form' => [
                    'userModel' => 'Users',
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ],
                    'scope' => array('Users.status' => 1, 'type' => 'admin')
                ]
            ],
            'storage' => ['className' => 'Session', 'key' => 'Auth.Admin']
        ]);
        $this->set('loggedIn', false);
        if ($this->Auth->user('id')) {
            $userType = strtolower(preg_replace('/[0-9]+/', '', $this->Auth->user('type')));
            $this->set('loggedIn', true);
            $this->loggedIn = true;
            $this->set('user', $this->Auth->user());
            $this->set('userType', $userType);
            $this->user = $this->Auth->user();
        }
    }

    protected function _generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function array_group_by($array, $key) {
        $return = array();
        foreach ($array as $v) {
            $return[$v[$key]][] = $v;
        }
        return $return;
    }
    
    protected function _jsonResponse($data = []) {
        echo json_encode($data);
        die;
    }
    
    protected function _ajaxCheck() {
        if (!$this->request->is('ajax')) {
            exit('You are not allowed to Access this URL');
        }
    }

}
