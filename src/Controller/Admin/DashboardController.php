<?php

namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Dompdf\Dompdf;
use Dompdf\Options;

class DashboardController extends AppController {

    /// Initializing pagination settings
    public $paginate = [
        'limit' => 10,
        'order' => [
            'companyDevices.id' => 'desc'
        ]
    ];

    public function initialize() {
        parent::initialize();
        $this->viewBuilder()->layout('dashboard');
        // Load paginator Component	
        $this->loadComponent('Paginator');
    }

    public function index() {
        $comments = [];
        $status = trim($this->request->query('status'));
        $devicesArr = $this->__getDevices($status);
        $devices = [];
        $fields = [];
        $fieldsKeysArr = [];
        $deviceFields = [];
        $typologyData = [];
        $addresses = [];
        $latLngArray = [];

        foreach ($devicesArr as $key => $devicesItem) {
            $devices[$key] = $devicesItem;
            $deviceDataArr = explode('#@!', $devicesItem['deviceData']);
            foreach ($deviceDataArr as $deviceDataItem) {
                $deviceDataItemArr = explode('*^$', $deviceDataItem);
                $fieldsKeysArr[] = $deviceDataItemArr[0];
                $deviceFields[$devicesItem['id']][] = array(
                    'field' => $deviceDataItemArr[0],
                    'value' => $deviceDataItemArr[1]
                );

                /// Prepare latLngArry
                if ('lat' == $deviceDataItemArr[0]) {
                    $latLngArray[$key]['lat'] = $deviceDataItemArr[1];
                } elseif ('lng' == $deviceDataItemArr[0]) {
                    $latLngArray[$key]['lng'] = $deviceDataItemArr[1];
                }

                // Adding location & concession fee data in devices
                if (in_array($deviceDataItemArr[0], ['location', 'concession_fees'])) {
                    $devices[$key][$deviceDataItemArr[0]] = $deviceDataItemArr[1];
                }
            }
            $typologyData[$devicesItem['id']] = array(
                'typology' => $devicesItem['typology'],
                'sub_category' => $devicesItem['sub_category'],
            );
//            $addresses[$devicesItem['id']] = [
//                'company' => [
//                    ['label' => 'Company Name/Building Owner', 'value' => $devicesItem['company_name']],
//                    ['label' => 'Representative', 'value' => $devicesItem['representative']],
//                    ['label' => 'Name', 'value' => $devicesItem['name']],
//                    ['label' => 'Telephone No.', 'value' => $devicesItem['phone']],
//                    ['label' => 'E-mail', 'value' => $devicesItem['email']],
//                    ['label' => 'Date Of Incorporation', 'value' => date('d M, Y', strtotime($devicesItem['incorporation_date']))],
//                ],
//                'Permanent' => [
//                    ['label' => 'Address', 'value' => $devicesItem['permanent_address']],
//                    ['label' => 'City', 'value' => $devicesItem['permanent_city']],
//                    ['label' => 'State', 'value' => $devicesItem['permanent_state']],
//                    ['label' => 'Pincode', 'value' => $devicesItem['permanent_pincode']],
//                    ['label' => 'Email', 'value' => $devicesItem['permanent_email']],
//                    ['label' => 'Telephone No.', 'value' => $devicesItem['permanent_phone']]
//                ],
//                'Mailing' => [
//                    ['label' => 'Address', 'value' => $devicesItem['mailing_address']],
//                    ['label' => 'City', 'value' => $devicesItem['mailing_city']],
//                    ['label' => 'State', 'value' => $devicesItem['mailing_state']],
//                    ['label' => 'Pincode', 'value' => $devicesItem['mailing_pincode']],
//                    ['label' => 'Email', 'value' => $devicesItem['mailing_email']],
//                    ['label' => 'Telephone No.', 'value' => $devicesItem['mailing_phone']],
//                ]
//            ];
            unset($devices[$key]['deviceData']);
            unset($devices[$key]['typology']);
            unset($devices[$key]['sub_category']);
//            unset($devices[$key]['permanent_address']);
//            unset($devices[$key]['permanent_city']);
//            unset($devices[$key]['permanent_state']);
//            unset($devices[$key]['permanent_pincode']);
//            unset($devices[$key]['permanent_email']);
//            unset($devices[$key]['permanent_phone']);
//            unset($devices[$key]['mailing_address']);
//            unset($devices[$key]['mailing_city']);
//            unset($devices[$key]['mailing_state']);
//            unset($devices[$key]['mailing_pincode']);
//            unset($devices[$key]['mailing_email']);
//            unset($devices[$key]['mailing_phone']);
        }
        if ($devices) {
            $deviceIds = array_map(function($arr) {
                return $arr['id'];
            }, $devices);
            $mcgCommentsTable = TableRegistry::get('McgComments');
            $mcgCommentsArr = $mcgCommentsTable->find('all')->select(['device_id', 'mcg_level', 'user_name' => 'u.display_name', 'type' => 'u.type', 'comment', 'comment_data', 'status', 'created'])->join([
                        'table' => 'users',
                        'alias' => 'u',
                        'type' => 'LEFT',
                        'conditions' => 'McgComments.user_id = u.id'
                    ])->where(['device_id IN' => $deviceIds])->order(['mcg_level' => 'DESC'])->hydrate(false);
            $mcgComments = $mcgCommentsArr->toArray();
            array_walk($mcgComments, function($value) use(&$comments) {
                $value['comment_data'] = json_decode($value['comment_data'], true);
                $comments[$value['device_id']][] = $value;
            });
            $fieldsKeysDataArr = array_merge($fieldsKeysArr, ['reason', 'obstruction_to_road', 'two_omd_distance', 'intersection_distance', 'omd_size_less_75']);
            $fieldsKeys = array_values(array_unique($fieldsKeysDataArr));
            $fieldsTable = TableRegistry::get('Fields');
            $fieldsArr = $fieldsTable->find('all')->select(['field_key', 'type', 'lable'])->where(['field_key IN' => $fieldsKeys])->hydrate(false);
            $fieldsData = $fieldsArr->toArray();
            array_walk($fieldsData, function($value) use(&$fields) {
                $fields[$value['field_key']] = array(
                    'label' => $value['lable'],
                    'type' => $value['type']
                );
            });
        }
        $deviceData = [];
        array_walk($devices, function($val, $key) use(&$deviceData) {
            $val['annual_turnover'] = number_format($val['annual_turnover']);
            $val['incorporation_date'] = date('d M, Y', strtotime($val['incorporation_date']));
            $deviceData[$val['id']] = $val;
        });
        $deviceImagePath = Router::url('/') . 'uploads/devices/';
        $latLngArray = json_encode($latLngArray);
        //// Set variables array ------------------------------------------------------------------------------------------
        $this->set(compact('devices'));
        $this->set(compact('deviceImagePath'));
        $this->set('fields', json_encode($fields));
        $this->set('deviceData', json_encode($deviceData));
        $this->set('deviceFields', json_encode($deviceFields));
        $this->set('typologyData', json_encode($typologyData));
        $this->set('addresses', json_encode($addresses));
        $this->set('comments', json_encode($comments));
        $this->set(compact('latLngArray'));
        $this->set(compact('iconName'));
    }

    private function __getDevices($status = '') {
        $devicesTable = TableRegistry::get('CompanyDevices');
        $mcgUserLevel = preg_replace("/[^0-9,.]/", "", $this->user['type']);
        $mcgLevelOrderDeviceIdsQuery = $devicesTable->find('all')->select(['id', 'mcgLevel' => '(IFNULL(tbl1.mcg_level,0) + 1)'])->join([
                    'table' => '(SELECT * FROM (SELECT * FROM `mcg_comments` ORDER BY `mcg_level` DESC) tbl GROUP BY `device_id`)',
                    'alias' => 'tbl1',
                    'type' => 'LEFT',
                    'conditions' => 'CompanyDevices.id = tbl1.device_id'
                ])->having(['mcgLevel' => $mcgUserLevel])->hydrate(false);
        $mcgLevelOrderDeviceIds = array_map(function($arr) {
            return $arr['id'];
        }, $mcgLevelOrderDeviceIdsQuery->toArray());
        $devicesData = $devicesTable->find('all')->select([
            'id',
            'company_code',
            'status',
            'created',
            'company_name' => 'c.company_name',
            'address' => 'c.company_address',
            'director_name' => 'c.director_name',
            'annual_turnover' => 'c.annual_turnover',
            'smc_party_code' => 'c.SMC_party_code',
            'incorporation_date' => 'c.incorporation_date',
            'representative' => 'r.representative',
            'name' => 'u.display_name',
            'phone' => 'u.mobile',
            'email' => 'u.email',
            'incorporation_date' => 'c.incorporation_date',
            'typology' => 'pc.title',
            'sub_category' => 'sc.title',
            'deviceData' => 'deviceData.fieldData'
        ]);
        $devicesData->join([
            'u' => [
                'table' => 'users',
                'type' => 'LEFT',
                'conditions' => 'CompanyDevices.user_id = u.id'
            ],
            'c' => [
                'table' => 'user_profiles',
                'type' => 'LEFT',
                'conditions' => 'CompanyDevices.user_id = c.user_profile_id'
            ],
            'r' => [
                'table' => 'representatives',
                'type' => 'LEFT',
                'conditions' => 'u.representative_id = r.id'
            ],
            'pc' => [
                'table' => 'categories',
                'type' => 'LEFT',
                'conditions' => 'CompanyDevices.category_id = pc.id'
            ],
            'sc' => [
                'table' => 'categories',
                'type' => 'LEFT',
                'conditions' => 'CompanyDevices.sub_category_id = sc.id'
            ],
            'deviceData' => [
                'table' => "(SELECT `company_device_id`, GROUP_CONCAT(CONCAT(`field_key`,'*^$',`input_value`) SEPARATOR '#@!') AS fieldData FROM `company_device_fields` GROUP BY `company_device_id`)",
                'type' => 'LEFT',
                'conditions' => 'CompanyDevices.id = deviceData.company_device_id'
            ]
        ]);
        $where['CompanyDevices.paid'] = '1';
        if (!empty($status)) {
            $where['CompanyDevices.status'] = $status;
        }
        $devicesData->where($where);
        $devicesData->order(['CompanyDevices.created' => 'DESC']);
        $devicesData->hydrate(false);
        //return $devicesData->toArray();
        return $this->paginate($devicesData)->toArray();
    }

    private function __getDevices1($status = '') {

        $devicesTable = TableRegistry::get('CompanyDevices');
        $devicesData = $devicesTable->find('all')->select([
            'id',
            'company_code',
            'status',
            'created',
            'company_name' => 'c.company_name',
            'representative' => 'r.representative',
            'name' => 'u.display_name',
            'phone' => 'u.mobile',
            'email' => 'u.email',
            'incorporation_date' => 'c.incorporation_date',
            'address' => 'CONCAT(am.address,", ",am.city,", ",am.state)',
            'permanent_address' => 'pm.address',
            'permanent_city' => 'pm.city',
            'permanent_state' => 'pm.state',
            'permanent_pincode' => 'pm.pincode',
            'permanent_email' => 'pm.email',
            'permanent_phone' => 'pm.phone',
            'mailing_address' => 'am.address',
            'mailing_city' => 'am.city',
            'mailing_state' => 'am.state',
            'mailing_pincode' => 'am.pincode',
            'mailing_email' => 'am.email',
            'mailing_phone' => 'am.phone',
            'typology' => 'pc.title',
            'sub_category' => 'sc.title',
            'deviceData' => 'deviceData.fieldData'
        ]);
        $devicesData->join([
            'u' => [
                'table' => 'users',
                'type' => 'LEFT',
                'conditions' => 'CompanyDevices.company_code = u.company_code AND CompanyDevices.by_admin = 0'
            ],
            'c' => [
                'table' => 'companies',
                'type' => 'LEFT',
                'conditions' => 'CompanyDevices.company_code = c.company_code AND CompanyDevices.by_admin = 0'
            ],
            'r' => [
                'table' => 'representatives',
                'type' => 'LEFT',
                'conditions' => 'c.representative_id = r.id'
            ],
            'pm' => [
                'table' => 'addresses',
                'type' => 'LEFT',
                'conditions' => 'CompanyDevices.company_code = pm.company_code AND pm.type = "permanent" AND CompanyDevices.by_admin = 0'
            ],
            'am' => [
                'table' => 'addresses',
                'type' => 'LEFT',
                'conditions' => 'CompanyDevices.company_code = am.company_code AND am.type = "mailing" AND CompanyDevices.by_admin = 0'
            ],
            'pc' => [
                'table' => 'categories',
                'type' => 'LEFT',
                'conditions' => 'CompanyDevices.category_id = pc.id'
            ],
            'sc' => [
                'table' => 'categories',
                'type' => 'LEFT',
                'conditions' => 'CompanyDevices.sub_category_id = sc.id'
            ],
            'deviceData' => [
                'table' => "(SELECT `company_device_id`, GROUP_CONCAT(CONCAT(`field_key`,'*^$',`input_value`) SEPARATOR '#@!') AS fieldData FROM `company_device_fields` GROUP BY `company_device_id`)",
                'type' => 'LEFT',
                'conditions' => 'CompanyDevices.id = deviceData.company_device_id'
            ]
        ]);

        if (!empty($status)) {
            $devicesData->where(['CompanyDevices.status' => $status]);
        }

        $devicesData->order(['CompanyDevices.created' => 'DESC']);
        $devicesData->hydrate(false);
        //return $devicesData->toArray();
        return $this->paginate($devicesData)->toArray();
    }

    public function applicationStatus() {

        //// Prepare categories array ------------------------------------------------------------------------------
        $categories = [];
        $categoriesTable = TableRegistry::get('Categories');
        $companyDeviceTable = TableRegistry::get('CompanyDevices');
        $categoriesData = $categoriesTable->query()->select(['id', 'title'])->where(['parent_id' => 0])->hydrate(false);
        foreach ($categoriesData->toArray() as $categoryItem) {
            $categories[$categoryItem['id']] = $categoryItem['title'];
        }

        //// Prepare devices array ----------------------------------------------------------------------------------
        $devices = $companyDeviceTable->find('all')->select(['companyDevices_id' => 'CompanyDevices.id', 'companyDevices_status' => 'CompanyDevices.status', 'companyDeviceFields_fieldData' => 'companyDeviceFields.fieldData'])->join([
                    'table' => "(SELECT `company_device_id`, GROUP_CONCAT(CONCAT(`field_key`,'=',`input_value`) SEPARATOR '#@!') AS fieldData FROM `company_device_fields` GROUP BY `company_device_id`)",
                    'alias' => 'companyDeviceFields',
                    'type' => 'LEFT',
                    'conditions' => 'CompanyDevices.id = companyDeviceFields.company_device_id',
                ])->where(['CompanyDevices.paid' => '1']);


        //// Handling search request ------------------------------------------------------------------------------
        if ($this->request->is('post')) {
            $categoryId = $this->request->data['category_id'];
            $subcategory = $this->request->data['subcategory'];
            $deviceId = $this->request->data['device_id'];

            if (!empty($deviceId)) {
                $devices = $devices->andWhere(['id' => $deviceId]);
            } elseif (!empty($categoryId) || !empty($subcategory)) {

                if ($categoryId) {
                    $devices = $devices->andWhere(['category_id' => $categoryId]);
                }

                if ($subcategory) {
                    $devices = $devices->andWhere(['sub_category_id' => $subcategory]);
                }
            }
        }

        $devices = $this->paginate($devices->hydrate(false))->toArray();

        foreach ($devices as $deviceKey => $device) {
            $tempDevice = explode('#@!', $device['companyDeviceFields_fieldData']);

            foreach ($tempDevice as $singleTempDevice) {
                $deviceFields = explode('=', $singleTempDevice);
                if (in_array($deviceFields[0], ['lat', 'lng', 'location', 'concession_fees'])) {
                    $devices[$deviceKey]['companyDevice_' . $deviceFields[0]] = $deviceFields[1];
                }
            }
        }

        //// Setting Vars -----------------------------------------------------------------------------------------
        $this->set(compact('categories'));
        $this->set(compact('devices'));
        if (isset($subcategory)) {
            $this->set(compact('subcategory'));
        }
    }

    public function pdf() {
        $this->autoRender = false;
        $options = new Options();
        $options->set('defaultFont', 'Courier');
        $options->set('isRemoteEnabled', TRUE);
        $options->set('debugKeepTemp', TRUE);
        $options->set('isHtml5ParserEnabled', true);
        $dompdf = new Dompdf($options);
        $html = '<!doctype html>';
        $html .= '<html>';
        $html .= '<head>';
        $html .= '<meta charset="utf-8">';
        $html .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
        $html .= '<meta name="viewport" content="width=device-width, initial-scale=1">';
        $html .= '<link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">';
        $html .= '<title>SMC</title>';
        $html .= '<style type="text/css">body { font-family: Lato; font-family: \'Lato\';}</style>';
        $html .= '</head>';
        $html .= '<body style="font-family: \'Lato\', sans-serif; max-width:100%">';
        $html .= '<table style="border:1px solid #000; width:100%">';
        $html .= '<tbody>';
        $html .= '<tr style="background:#fed9c1;height:400px;">';
        $html .= '<td style="background:#fed9c1;padding: 25px;vertical-align: middle; width: 25px;"><img src="'.Router::url('/img',true).'/receipt-title.png"></td>';
        $html .= '<td style="background:#fff; border-left:1px solid #000;" width="650">';
        $html .= '<table style="text-align:center;width:100%; background: url(\''.Router::url('/img',true).'/logo-bg-receipt.png\');">';
        $html .= '<tbody>';
        $html .= '<tr>';
        $html .= '<td style="text-align:center; font-size:28px;text-transform:uppercase;color:#333333; font-weight:700;letter-spacing:1px;padding-left:300px;">Tender Application Fee</td>';
        $html .= '<td style="text-align:right;"><img src="'.Router::url('/img',true).'/logo-receipt.png"/></td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td style="text-align:center;" colspan="2">';
        $html .= '<h2 style="margin:10px 100px;padding:5px;background:#edecec;border-radius: 5px; font-size:15px; font-weight:200;letter-spacing:0.5">Acknowledgement of registration fee on www.suratmunicipal.gov.in</h2></td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="2" style="font-size:18px;color:#3892b5; padding: 0px 190px 10px 190px; line-height: 1.3;">Payment amounting to <b>Rs 2000/-(Rupees Ten Thousand Only)</b> towards the registration process on www.suratmunicipal.gov.in has been successfully processed.</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="2">';
        $html .= '<h4 style="margin:0 200px; padding:5px;border:2px dashed #333333;color:#333333;font-size:23px;font-weight:200;">Your registration number is <b style="font-weight:bold;">GJSMC0042</b></h4></td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="2" style="color:#333333; font-size:17px;letter-spacing:0.5px;">Please quote the above number for future correspondence.</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="2" style="text-align:right;color:#333333; font-size:17px;padding-right: 70px;padding-top: 35px;">SD Commissioner</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="2" style="color:#838181;font-size: 15px;padding: 10px 20px 0 20px;letter-spacing: 0.2px;">Note:- Registration is subject to verification & approval of documents by SMC.The above registration does not authorize display of advertisements governed by relevant policies, rules and byelaws of Advertisement.</td>';
        $html .= '</tr>';
        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '</body>';
        $html .= '</html>';
         $dompdf->set_paper(array(0, 0, 500, 800), 'landscape');
        $dompdf->load_html($html);
        $dompdf->render();
        file_put_contents(WWW_ROOT.'uploads/receipt/document.pdf', $dompdf->output());
    }

}
