<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\View\Helper\PaginatorHelper;
use Cake\Routing\Router;
use App\Traits\Users;

/**
 * Description of HomeController
 *
 * @author Signity Solutions Pvt. Ltd.
 */
class DashboardController extends AppController {

    use Users;

    /// Initializing pagination settings
    public $paginate = [
        'limit' => 10,
        'order' => [
            'companyDevices.id' => 'desc'
        ]
    ];

    public function initialize() {
        parent::initialize();
        $this->viewBuilder()->layout('dashboard');
        $this->loadComponent('Paginator');
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $userType = strtolower(preg_replace('/[0-9]+/', '', $this->Auth->user('type')));
        if ($userType == 'mcg') {
            return $this->redirect(['controller' => 'Mcg', 'action' => 'index']);
        }
        $userData = $this->_getUserData($this->Auth->user('id'));
        if (!$userData['is_mobile_verified']) {
            return $this->redirect(['controller' => 'Users', 'action' => 'mobileVerification']);
        }
        if (!$userData['paid']) {
            return $this->redirect(['controller' => 'Users', 'action' => 'regisrationPayment']);
        }
        if ($userData['status'] == 2) {
            $this->Flash->error('Your profile has been rejected by administrator. Please register again or contact MCR support.');
            return $this->redirect($this->Auth->logout());
        }
    }

    public function beforeRender(Event $event) {
        parent::beforeRender($event);
    }

    public function index() {
        $user = $this->_getUserData($this->Auth->user('id'));
        $status = trim($this->request->query('status'));
        $status = $status == 'pending' ? 'under process' : $status;
        $comapnyDevicesTable = TableRegistry::get('companyDevices');

        //// Prepare devices array ------------------------------------------------------------------------------------------
        $devices = $comapnyDevicesTable->find('all')->select(['companyDevices_id' => 'companyDevices.id', 'device_name' => 'companyDevices.device_name', 'companyDevices_status' => 'companyDevices.status', 'companyDeviceFields_fieldData' => 'companyDeviceFields.fieldData'])->join([
                    'table' => "(SELECT `company_device_id`, GROUP_CONCAT(CONCAT(`field_key`,'=',`input_value`) SEPARATOR '#@!') AS fieldData FROM `company_device_fields` GROUP BY `company_device_id`)",
                    'alias' => 'companyDeviceFields',
                    'type' => 'LEFT',
                    'conditions' => 'companyDevices.id = companyDeviceFields.company_device_id',
                ])->where(['companyDevices.user_id' => $this->user['id'], 'companyDevices.paid' => '1']);
                
        if ($status)
            $devices = $devices->andWhere(['status' => $status]);

        $devices = $devices->hydrate(false);
        $devices = $this->paginate($devices)->toArray();
        //// Prepare latLngArray array ---------------------------------------------------------------------------------------
        $latLngArray = [];
        $allFees = ["annual_license_fees","concession_fees","total_omd_fees","total_permission_fees"];
        foreach ($devices as $deviceKey => $device) {
            $tempDevice = explode('#@!', $device['companyDeviceFields_fieldData']);

            foreach ($tempDevice as $singleTempDevice) {
                $deviceFields = explode('=', $singleTempDevice);
                if (in_array($deviceFields[0], ['lat', 'lng', 'location', 'concession_fees'])) {
                    $devices[$deviceKey]['companyDevice_' . $deviceFields[0]] = $deviceFields[1];
                }
                if(in_array($deviceFields[0], $allFees)) {
                    $devices[$deviceKey]['companyDevice_concession_fees'] = $deviceFields[1];
                }
            }
            if (isset($devices[$deviceKey]['companyDevice_lat']) && isset($devices[$deviceKey]['companyDevice_lat'])) {
                $latLngArray[] = array('lat' => $devices[$deviceKey]['companyDevice_lat'], 'lng' => $devices[$deviceKey]['companyDevice_lng']);
            }
        }
        $latLngArray = json_encode($latLngArray);

        /// Set paginated sorKey and Order Icon Name
        $iconName = "bottom";
        if (isset($this->Paginator->request->params['paging']['companyDevices'])) {
            $paginationVars = $this->Paginator->request->params['paging']['companyDevices'];
            $iconName = $paginationVars['direction'] == 'desc' ? 'top' : 'bottom';
        }
        $userAdditionalDocs = [];
        $userAdditionalDocRequestsTable = TableRegistry::get('UserAdditionalDocRequests');
        $lastAdditionalDocRequest = $userAdditionalDocRequestsTable->find('all')->where(['user_id' => $this->user['id']])->order(['id' => 'DESC'])->hydrate(false)->first();
        if (count($lastAdditionalDocRequest)) {
            $userAdditionalDocsTable = TableRegistry::get('UserAdditionalDocs');
            $userAdditionalDocs = $userAdditionalDocsTable->find('all')->select(['id', 'name', 'reason', 'is_uploaded'])->where(['request_id' => $lastAdditionalDocRequest['id']])->hydrate(false)->toArray();
        }
        $expirationDate = date('d M, Y',strtotime($user['created'] . ' +6 years'));
        $adminApproved = $user['admin_approved'];
        $session = $this->request->session();
        $receipt = $session->read('Registration.receipt');
        $session->delete('Registration.receipt');
        //// Set variables array ------------------------------------------------------------------------------------------
        $this->set(compact('devices', 'receipt', 'adminApproved', 'userAdditionalDocs','expirationDate'));
        $this->set(compact('latLngArray'));
        $this->set(compact('iconName'));
    }

    public function payments() {
        $user = $this->_getUserData($this->Auth->user('id'));
        if (!$user['admin_approved']) {
            return $this->redirect(['action' => 'index']);
        }
    }

    public function reports() {
        $user = $this->_getUserData($this->Auth->user('id'));
        if (!$user['admin_approved']) {
            return $this->redirect(['action' => 'index']);
        }
    }

    private function array_group($array, $key) {
        $return = array();
        foreach ($array as $v) {
            $return[$v[$key]]['status'] = $v['companyDevices_status'];
            $return[$v[$key]][$v['companyDeviceFields_field_key']] = $v['companyDeviceFields_input_value'];
        }
        return $return;
    }

    public function uploadDoc() {
        $this->autoRender = false;
        $response['error'] = 1;
        $documentName = '';
        $documentNameArr = array_keys($this->request->data);
        if (isset($documentNameArr[0])) {
            $documentName = $documentNameArr[0];
        }
        $userDocumentsTable = TableRegistry::get('user_additional_docs');
        $response['file'] = $documentName;
        if ($this->request->data[$documentName]['size']) {
            $ext = pathinfo($this->request->data[$documentName]['name'], PATHINFO_EXTENSION);
            $allowed = array('gif', 'jpeg', 'png', 'jpg', 'doc', 'docx', 'xls', 'xlsx', 'pdf');
            if (!in_array(strtolower($ext), $allowed)) {
                $response['error'] = 1;
                $response['file'] = $documentName;
                $response['data'] = 'These files extension are allowed: .png, .gif, .jpeg, .jpg, .doc, .docx, .xls, .xlsx, .pdf';
                $this->_jsonResponse($response);
            }
            $fileName = time() . '_' . $this->_generateRandomString(20) . '_' . $documentName . '.' . $ext;
            $uploadFile = WWW_ROOT . 'uploads/additonal_user_doc/' . $fileName;
            if (move_uploaded_file($this->request->data[$documentName]['tmp_name'], $uploadFile)) {
                $response['error'] = 0;
                $response['file'] = $documentName;
                $userDocumentEntity = $userDocumentsTable->newEntity(['is_uploaded' => '1', 'filename' => $fileName]);
                $userDocumentEntity->id = $documentName;
                $userDocumentsTable->save($userDocumentEntity);
            }
        }
        if($response['error'] == 0) {
            $requestIdArr = $userDocumentsTable->find('all')->select(['request_id'])->where(['id' => $documentName])->hydrate(false)->first();
            $requestId = $requestIdArr['request_id'];
            $notUploadDocs = $userDocumentsTable->find('all')->where(['is_uploaded' => '0','request_id' => $requestId])->count();
            if(!$notUploadDocs) {
                $response['error'] = 2;
                $usersTable = TableRegistry::get('Users');
                $usersEntity = $usersTable->newEntity(['admin_approved' => '0'],['validate' => false]);
                $usersEntity->id = $this->Auth->user('id');
                $usersTable->save($usersEntity);
            }
        }
        $this->_jsonResponse($response);
    }

}
