<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

/**
 * Companies Controller
 *
 * @property \App\Model\Table\CompaniesTable $Companies
 */
class CompaniesController extends AppController {

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        if($this->user['type'] != 'user') {
            exit("You are not allowed to access this page");
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $representative = [];
        $representativeData = TableRegistry::get('Representatives');
        $representativeQuery = $representativeData->find()->select(['id', 'representative'])->hydrate(false);
        foreach ($representativeQuery->toArray() as $representativeItem) {
            $representative[$representativeItem['id']] = $representativeItem['representative'];
        }
        $company = $this->Companies->newEntity();
        if ($this->request->is('post')) {
            $company = $this->Companies->patchEntity($company, $this->request->data);
            $companyArr = $this->Companies->save($company);
            if ($companyArr) {

                //// Update company code -----------------------------------------------------------------------------------
                $companyArrData = $companyArr->toArray();
                $companyId = $companyArrData['id'];
                $companyCode = 'COMP-' . $companyId;
                $this->Companies->query()->update()->set(['company_code' => $companyCode])->where(['id' => $companyId])->execute();
                $usersTable = TableRegistry::get('Users');
                $usersTable->query()->update()->set(['company_code' => $companyCode])->where(['id' => $this->Auth->user('id')])->execute();
                $authUser = $this->Auth->user();
                $authUser['company_code'] = $companyCode;
                $this->Auth->setUser($authUser);

                //// Save addresses -------------------------------------------------------------------------------------
                $this->saveAddresses($this->request->data, $companyCode);

                //// Save profiles -----------------------------------------------------------------------------------
                $this->saveProfiles($this->request->data, $companyCode);

                return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
            }
        }
        $this->set(compact('representative'));
        $this->set(compact('company'));
        $this->set('_serialize', ['company']);
    }

    private function saveProfiles($formData, $companyCode) {

        $profilesTable = TableRegistry::get('Profiles');
        $profilesData = [
            [
                'company_code' => $companyCode,
                'name' => $formData['name'],
                'phone' => $formData['phone'],
                'email' => $formData['email'],
                'is_primary' => 1,
                'status' => 1
            ]
        ];

        if (isset($formData['name_1'])) {
            $profilesData[] = [
                'company_code' => $companyCode,
                'name' => $formData['name_1'],
                'phone' => $formData['phone_1'],
                'email' => $formData['email_1'],
                'is_primary' => 0,
                'status' => 1
            ];
        }

        if (isset($formData['name_2'])) {
            $profilesData[] = [
                'company_code' => $companyCode,
                'name' => $formData['name_2'],
                'phone' => $formData['phone_2'],
                'email' => $formData['email_2'],
                'is_primary' => 0,
                'status' => 1
            ];
        }

        $profilesQuery = $profilesTable->query();
        foreach ($profilesData as $profile) {
            $profilesQuery->insert(['company_code', 'name', 'phone', 'email', 'is_primary', 'status'])->values($profile);
        }
        $profilesQuery->execute();
    }

    private function saveAddresses($formData, $companyCode) {
        $addressesTable = TableRegistry::get('Addresses');
        $addressesData = [
            [
                'company_code' => $companyCode,
                'type' => 'mailing',
                'address' => $formData['address'],
                'city' => $formData['city'],
                'state' => $formData['state'],
                'pincode' => $formData['pincode'],
                'email' => $formData['email_corresponding'],
                'phone' => $formData['contact'],
                'status' => 1
            ]
        ];
        $permanentAddress = [
            'company_code' => $companyCode,
            'type' => 'permanent',
            'address' => $formData['address'],
            'city' => $formData['city'],
            'state' => $formData['state'],
            'pincode' => $formData['pincode'],
            'email' => $formData['email_corresponding'],
            'phone' => $formData['contact'],
            'status' => 1
        ];
        if (!isset($formData['permanentAddressCheck'])) {
            $permanentAddress = [
                'company_code' => $companyCode,
                'type' => 'permanent',
                'address' => $formData['p_address'],
                'city' => $formData['p_city'],
                'state' => $formData['p_state'],
                'pincode' => $formData['p_pincode'],
                'email' => $formData['p_email_corresponding'],
                'phone' => $formData['p_contact'],
                'status' => 1
            ];
        }
        array_push($addressesData, $permanentAddress);
        $addressQuery = $addressesTable->query();
        foreach ($addressesData as $address) {
            $addressQuery->insert(['company_code', 'type', 'address', 'city', 'state', 'pincode', 'email', 'phone', 'status'])->values($address);
        }
        $addressQuery->execute();
    }

    public function validateCompany() {
        if (!$this->request->is('ajax')) {
            exit("You are not allowed to access this page");
        }
        /* Validate Company */
        $post = $this->Companies->newEntity();
        $response['error'] = 0;
        if ($this->request->is('post')) {

            $post = $this->Companies->patchEntity($post, $this->request->data);
            if (!empty($post->errors())) {
                $response['error'] = 1;
                $response['data'] = $post->errors();
            } else {
                $formData = $post->toArray();
                $representativeData = TableRegistry::get('Representatives');
                $representativeQuery = $representativeData->find()->where(['id' => $formData['representative_id']])->first()->toArray();
                $formData['representative_id'] = $representativeQuery['representative'];

                if (isset($formData['permanentAddressCheck'])) {
                    $formData['p_address'] = $formData['address'];
                    $formData['p_city'] = $formData['city'];
                    $formData['p_state'] = $formData['state'];
                    $formData['p_pincode'] = $formData['pincode'];
                    $formData['p_email_corresponding'] = $formData['email'];
                    $formData['p_contact'] = $formData['phone'];
                }
                $response['data'] = $formData;
            }
        }
        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }

}
