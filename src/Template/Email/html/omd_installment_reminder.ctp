<p>Hi <?php echo $data['device']['company_name']?></p>
<p>The annual/quarterly permission fee of Rs.<?php echo number_format($data['installment']['amount'],2);?> is due for payment. Kindly pay it before <?php echo date('d M, Y',strtotime($data['installment']['start_date']));?>.</p>
<p>Thanks</p>
<p>Municipal Corporation Faridabad</p>
