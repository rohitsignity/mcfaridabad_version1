<p>Hi <?php echo $data['display_name']?></p>
<p>Payment of Rs. <?php echo number_format($data['fees'],2);?> is pending. Please pay it before <?php echo date('d M, Y h:iA',strtotime($data['last_date']));?>. Otherwise your application will be canceled</p>
<p>Thanks</p>
<p>Municipal Corporation Faridabad</p>
