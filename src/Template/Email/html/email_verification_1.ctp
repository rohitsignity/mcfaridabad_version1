<?php
$verifyUrl	 =	$this->Url->build(['prefix' => false, "controller" => "Users", "action" => "login"], true);	

$message	 =	"<p>Hi {$user->display_name},</p>";

if(($user->status) == 0)
{
    $message	.=	"<p>Your profile is under processing. We will get back to you soon. </p>";
   
}
 else if(($user->status) == 1)
 {
    $message	.=	"<p>Your account has been approved by admin. You can now login to the website, please click on <a href='{$verifyUrl}'>{$verifyUrl}</a> link.</p></p>";
}
 else {
     $message	.=	"<p>Your account has not been approved by admin. You can register again, please click on <a href='{$verifyUrl}'>{$verifyUrl}</a> link.</p></p>";
}


$message	.=	"Thanks.";

echo $message;
