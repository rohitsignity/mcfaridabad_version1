<b>Hi <?php echo $user->display_name; ?></b>
<br><br>
<p>Your account has been approved by admin. You can now login to the website, please click <a href="<?php echo $this->Url->build(['prefix' => false, "controller" => "Users", "action" => "login"], true); ?>">here</a> to login.</p>

<br><br>
<i>Thank you</i>