<?php

$forgotUrl	 =	$this->Url->build([ "controller" => "Users", "action" => "resetPassword"], true)."?mask={$user['id']}&id={$user['security_hash']}";	
						
$message	 =	"<p>Hi {$user['display_name']},</p>";
$message	.=	"<p>You have just requested password reset. To reset your password, please click on <a href='{$forgotUrl}'>{$forgotUrl}</a> link.</p>";
$message	.=	"Thanks.";

echo $message;
