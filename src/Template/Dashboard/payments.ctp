<div id="page-content-wrapper">
	<!-- Keep all page content within the page-content inset div! -->
	<div class="page-content inset paymentpg">
		<div class="headings-all dash-in">
			<h1>Payment</h1>
		</div>
		<div class="device-list">
			<div class="table-responsive">
				<table class="table">
					<thead class="thead-default">
						<tr>
							<th>Sr.</th>
							<th>Applicant Name</th>
							<th>Device Id</th>
							<th>Location</th>
							<th>Status</th>
							<th>&nbsp;</th>
							<th>Discription</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th scope="row">1.</th>
							<td>Lorem Text</td>
							<td>AB1234567</td>
							<td>Loren impsum</td>
							<td class="sts-pending">Pending</td>
							<td><span class="pendbtn btn btn-default">Pay now</span></td>
							<td>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.</td>

						</tr>
						<tr>
							<th scope="row">2.</th>
							<td>Lorem Text</td>
							<td>AB1234567</td>
							<td>Loren impsum</td>
							<td class="sts-apprd">Approved</td>
							<td></td>
							<td>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.</td>
						</tr>
						<tr>
							<th scope="row">3.</th>
							<td>Lorem Text</td>
							<td>AB1234567</td>
							<td>Loren impsum</td>
							<td class="sts-pending">Pending</td>
							<td><span class="pendbtn btn btn-default">Pay now</span></td>
							<td>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.</td>
						</tr>
						<tr>
							<th scope="row">4.</th>
							<td>Lorem Text</td>
							<td>AB1234567</td>
							<td>Loren impsum</td>
							<td class="sts-pending">Pending</td>
							<td><span class="pendbtn btn btn-default">Pay now</span></td>
							<td>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.</td>
						</tr>
						<tr>
							<th scope="row">5.</th>
							<td>Lorem Text</td>
							<td>AB1234567</td>
							<td>Loren impsum</td>
							<td class="sts-rejected">Rejected</td>
							<td></td>
							<td>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.</td>
						</tr>
					</tbody>
				</table>
			</div>


		</div>
		<div class="gen-challan">
			<h2 class="h2class"> 
			<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Generate Challan<img src="<?php echo $this->Url->image('arrow-down.png'); ?>"/></a>
			</h2>
			<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
				<div class="panel-body">
					Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a au
				</div>
				<div class="pdf-dwn">
					<a href="#"><img src="<?php echo $this->Url->image('adobe.png'); ?>" />
						<button class="btn">Download as PDF </button>
					</a>
				</div>

			</div>
		</div>
		<div class="gen-challan up-challan">
			<h2 class="h2class"> 
			<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Uploada Receipt For Challan<img src="<?php echo $this->Url->image('arrow-down.png'); ?>"/></a>
			</h2>
			<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
				<div class="panel-body">
					Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a au
				</div>
				<div class="pdf-dwn">
					<a href="#">
						<button class="btn"><span class="glyphicon glyphicon-upload"></span>Upload Challan</button>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
