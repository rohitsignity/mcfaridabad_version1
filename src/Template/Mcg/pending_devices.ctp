<div id="page-content-wrapper">
    <!-- Keep all page content within the page-content inset div! -->
    <div class="page-content inset dashboard">
        <div class="dash-head">
			<?php  $user_type = str_replace("G","F",$user['type']);?>
            <p>Welcome <?php echo $user['display_name'] . ' (' . $user_type . ')'; ?></p>
            <h1>Pending devices</h1>
        </div>
        <div class="device-list">
            <form>
                <div id="custom-search-input" class="custm-srch">
                    <div class="input-group col-md-6">
                        <input class="search-query form-control" placeholder="Search" type="text" name="s" value="<?php echo $search; ?>">
                        <span class="input-group-btn">
                            <button class="btn" type="button">
                                <span class=" glyphicon glyphicon-search"></span>
                            </button>
                        </span>
                    </div>
                </div>
            </form>
            <?php
            $firstDevice = reset($devices);
            ?>
            <div class="table-responsive">
                <table class="table">
                    <thead class="thead-default">
                        <tr>
                            <th>OMD Id</th>
                            <th>Company Name/Individual Name</th>
                            <th>Company Address</th>
                            <th>Date & Time</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if ($devices) {
                            foreach ($devices as $device) {
                                ?>
                                <tr deviceRow="<?php echo $device['id']; ?>" class="deviceRow <?php echo "device_" . $device['id']; ?>">
                                    <th scope="row"><?php echo $device['id']; ?></th>
                                    <td><?php echo $device['company_name'] ? $device['company_name'] : $device['name']; ?></td>
                                    <td><?php echo $device['address']; ?></td>
                                    <td><?php echo date('d-m-Y h:iA', strtotime($device['created'])); ?></td>
                                    <td class="sts-pending" style="<?php echo $device['sort_order'] == 0 ? 'color: red;' : '' ?>">
										<?php
										$status = str_replace("G","F",$device['status']);
										 echo $status; 
										 
										 ?>
									</td>
                                </tr>
                                <?php
                            }
                            if ($this->Paginator->numbers()) {
                                ?>
                                <tr>
                                    <td colspan="5"><ul class="pagingPages"><?php echo $this->Paginator->prev() . $this->Paginator->numbers() . $this->Paginator->next(); ?></ul></td>
                                </tr>
                                <?php
                            }
                        } else {
                            ?>
                            <tr><td colspan="5" style="text-align: center;">No Record Found</td></tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div style="display: none;" id="deviceBlock">
            <div class="row">
                <div class="col-md-6">
                    <div class="mcgform regist-form">
                        <h2 class="formh2">Device Details</h2>
                        <div class="row">
                            <form class="prevwfm" id="deviceDetailBlock"></form>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mcgform regist-form ">
                        <h2 class="formh2">Company/Individual Details</h2>
                        <div class="row">
                            <form class="prevwfm" id="deviceAddressDetailBlock"></form>
                        </div>
                    </div>
                </div>
            </div>
            <div id="mutipleOmdTable" class="row"></div>
            <h2 class="imgsite formh2">Photographs of Site</h2>
            <div class="row" id="siteImages">
                <div class="col-md-3">
                    <div class="siteimg">
                        <img src="<?php echo $this->Url->image('notfound.jpg'); ?>" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="siteimg">
                        <img src="<?php echo $this->Url->image('notfound.jpg'); ?>" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="siteimg">
                        <img src="<?php echo $this->Url->image('notfound.jpg'); ?>" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="siteimg">
                        <img src="<?php echo $this->Url->image('notfound.jpg'); ?>" />
                    </div>
                </div>
            </div>
            <div id="addtionaldocs" class="siteimg"></div>
            <h2 class="imgsite formh2 mcgborder">Process OMD applications </h2>
            <div class="comt-blk" id="previous-comments" style="display: none;">
                <label for="exampleInputEmail1">Previous Comments</label>
                <div class="form-group">
                    <!--<textarea class="form-control" placeholder="Comments"></textarea>-->
                </div>
            </div>
            <form action="javascript:void(0)" id="mcgCommentForm">
                <?php /* ?>
                  <div class="radioselect mcgborder">
                  <div class="form-group">
                  <p>Size Of OMD( Not More than 75% of the Facade Area) </p>
                  <div class="radiobtn pull-right">
                  <label><input name="omd_size_less_75" value="Yes" id="exampleInputEmail1" placeholder="Enter Company Name" type="radio"><p>Yes</p></label>
                  <label><input name="omd_size_less_75" value="No" id="exampleInputEmail1" placeholder="Enter Company Name" type="radio"><p>No</p></label>
                  </div>
                  </div>
                  <div class="form-group">
                  <p>Distance From Intersection(100 m away From Intersection) </p>
                  <div class="radiobtn pull-right">
                  <label><input name="intersection_distance" value="Yes" id="exampleInputEmail1" placeholder="Enter Company Name" type="radio"><p>Yes</p></label>
                  <label><input name="intersection_distance" value="No" id="exampleInputEmail1" placeholder="Enter Company Name" type="radio"><p>No</p></label>
                  </div>
                  </div>
                  <div class="form-group">
                  <p>Distance between two OMDs(minimum 50 metres) </p>
                  <div class="radiobtn pull-right">
                  <label><input name="two_omd_distance" value="Yes" id="exampleInputEmail1" placeholder="Enter Company Name" type="radio"><p>Yes</p></label>
                  <label><input name="two_omd_distance" value="No" id="exampleInputEmail1" placeholder="Enter Company Name" type="radio"><p>No</p></label>
                  </div>
                  </div>
                  <div class="form-group">
                  <p>Obstruction to Road User/Pedestrian </p>
                  <div class="radiobtn pull-right">
                  <label><input name="obstruction_to_road" value="Yes" id="" placeholder="Enter Company Name" type="radio"><p>Yes</p></label>
                  <label><input name="obstruction_to_road" value="No" id="" placeholder="Enter Company Name" type="radio"><p>No</p></label>
                  </div>
                  </div>
                  </div>
                  <div class="reasonblk clearfix form-group">
                  <b for="">Reasons</b>
                  <div class="ckeckI"><label><input type="checkbox" name="reasonCheck[]" value="Upload Images">Upload Images</label></div>
                  <div class="ckeckI"><label><input type="checkbox" name="reasonCheck[]" value="Coordinates">Coordinates</label></div>
                  <div class="ckeckI"><label><input type="checkbox" name="reasonCheck[]" value="Other">Other</label></div>
                  </div>
                  <?php */ ?>
                <div class="comt-blk">
                    <label for="exampleInputEmail1">Comments</label>
                    <div class="form-group">
                        <textarea class="form-control" placeholder="Comments" name="comment"></textarea>
                    </div>
                </div>
                <input type="hidden" name="status">
                <input type="hidden" name="device">
                <div class="submit">
                    <input class="btn btn-default approvd commentSubmit" value="<?php echo $user['type'] == 'MCG3' ? 'Approve' : 'Forward'; ?>" type="button">
                    <?php
                    if ($user['type'] != 'MCG1') {
                        ?>
                        <input class="btn btn-default hld commentSubmit" value="Return" type="button">
                        <?php
                    }
                    if ($user['type'] == 'MCG2') {
                        ?>
                        <input class="btn btn-default approvd commentSubmit" value="Return for Re submission" type="button" style="width: auto;">
                        <a href="javascript:void(0)" class="btn btn-default approvd" style="width: auto;" onclick="Mcg.requestAdditionalDocs()">Request Additional Documents</a>
                        <?php
                    }
                    if ($user['type'] == 'MCG3') {
                        ?>
                        <input class="btn btn-default hld commentSubmit" value="Reject" type="button">
                        <?php
                    }
                    ?>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="documentsModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Request Additional Documents</h4>
            </div>
            <div class="modal-body" id="documentsList" style="float: left; width: 100%;">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer"><p style="text-align: center;">Copyright © <?php echo date('Y'); ?> Outdoor Media Management System. All Rights Reserved.</p></div>
        </div>
    </div>
</div>
<script>
    var Devices = {
        mcgLevel: <?php echo $mcgUserLevel; ?>,
        addressProof: JSON.parse('<?php echo $addressProof; ?>'),
        deviceMcgMaxLevel: 0,
        lastStatus: '',
        deviceId: '',
        deviceData: JSON.parse('<?php echo $deviceData; ?>'),
        imagePath: '<?php echo $deviceImagePath; ?>',
        omdActions: {0: 'Under Process', 1: 'Approve', 2: 'Return for Resubmission', 3: 'Reject'},
        deviceImages: [],
        fields: JSON.parse('<?php echo $fields; ?>'),
        deviceFields: JSON.parse('<?php echo $deviceFields; ?>'),
        typologyData: JSON.parse('<?php echo $typologyData; ?>'),
        addressData: JSON.parse('<?php echo $addresses; ?>'),
        comments: JSON.parse('<?php echo $comments; ?>'),
        usersDocuments: JSON.parse('<?php echo $usersDocuments; ?>'),
        addionaldocs: JSON.parse('<?php echo $addionaldocs; ?>'),
        fieldszoneArr: JSON.parse('<?php echo $fieldszoneArr; ?>'),
        multipleOmdData: {},
        getDeviceInformation: function () {
            var self = this;
            var deviceId = self.deviceId;
            $('#mutipleOmdTable').html('');
            self.getDeviceDetailsHTML(deviceId);
            self.getDeviceImagesHTML();
            self.getDeviceAddressHTML(deviceId);
            self.getCommentsHTML();
            self.addtionaldocumenthtml();
            self.checkPermission();
            self.getTypologyAData();
            self.getMetroMtrsOMD();
            self.getMultipleOMD();
            self.showForm();
        },
        getTypologyAData: function(){
            var self = this;
            if(self.typologyData[self.deviceId].typology_id === '1' && self.typologyData[self.deviceId].sub_category_id !== 17 && self.typologyData[self.deviceId].sub_category_id !== 18 && self.typologyData[self.deviceId].sub_category_id !== 19) {
                var data = $.grep(self.deviceFields[self.deviceId],function(arr){
                    return $.inArray(arr.field,['other_type_a_data']) !== -1;
                });
                if(data.length) {
                    var labelData = $.grep(self.deviceFields[self.deviceId],function(arr){
                        return $.inArray(arr.field,['omd_type_type_a']) !== -1;
                    });
                    var label = labelData[0].value;
                    var counter = 1;
                    var html = '<div class="col-md-12">';
                        html += '<h3 class="imgsite formh2 mcgborder">'+ label +'</h3>';
                        html += '<table class="table">';
                        html += '<thead>';
                        html += '<tr>';
                        html += '<th>S. No.</th>';
                        html += '<th>Type of Asset</th>';
                        html += '<th>Location</th>';
                        html += '<th>Height (in m)</th>';
                        html += '<th>Width (in m)</th>';
                        html += '<th>Total Area(in sq m)</th>';
                        html += '<th>Latitude</th>';
                        html += '<th>Longitude</th>';
                        html += '</tr>';
                        html += '</thead>';
                        html += '<tbody>';
                        $.each(data[0].value,function(index,val){
                            html += '<tr>';
                            html += '<td>'+ counter+'</td>';
                            html += '<td>'+ label+'</td>';
                            html += '<td>'+ val.location +'</td>';
                            html += '<td>'+ val.height +'</td>';
                            html += '<td>'+ val.width +'</td>';
                            html += '<td>'+ val.area +'</td>';
                            html += '<td>'+ val.lat +'</td>';
                            html += '<td>'+ val.lng +'</td>';
                            html += '</tr>';
                           counter++;
                        });
                        html += '</tbody>';
                        html += '</table>';
                        html += '</div>';
                        $('#mutipleOmdTable').html(html);
                } else {
                    var data = $.grep(self.deviceFields[self.deviceId],function(arr){
                        return $.inArray(arr.field,['shelter_data','route_markers_data','foot_over_data','cycle_station_data','police_booth_data','sitting_bench_data']) !== -1;
                    });
                    var labelData = $.grep(self.deviceFields[self.deviceId],function(arr){
                        return $.inArray(arr.field,['shelters_type','route_markers_type','foot_over_type','cycle_station_type','police_booth_type','sitting_bench_type']) !== -1;
                    });
                    var isDevice  = $.grep(data[0].value,function(arr){
                        return (typeof arr.device !== 'undefined');
                    });
                    var deviceTypeLabel = 'No. Of panels';
                    if(isDevice.length) {
                        deviceTypeLabel = 'Number of Devices';
                    }
                    var label = labelData[0].value;
                    var counter = 1;
                    var html = '<div class="col-md-12">';
                    html += '<h3 class="imgsite formh2 mcgborder">'+ label +'</h3>';
                    html += '<table class="table">';
                    html += '<thead>';
                    html += '<tr>';
                    html += '<th>S. No.</th>';
                    html += '<th>Type of Asset</th>';
                    html += '<th>Location</th>';
                    html += '<th>'+ deviceTypeLabel +'</th>';
                    html += '<th>Total Area(in sq m)</th>';
                    html += '<th>Latitude</th>';
                    html += '<th>Longitude</th>';
                    html += '</tr>';
                    html += '</thead>';
                    html += '<tbody>';
                    $.each(data[0].value,function(index,val){
                        html += '<tr>';
                        html += '<td>'+ counter +'</td>';
                        html += '<td>'+ label +'</td>';
                        html += '<td>'+ val.location +'</td>';
                        html += '<td>'+ ((typeof val.device !== 'undefined') ? val.device : val.panels) +'</td>';
                        html += '<td>'+ val.area +'</td>';
                        html += '<td>'+ val.lat +'</td>';
                        html += '<td>'+ val.lng +'</td>';
                        html += '</tr>';
                        counter++;
                    });
                    html += '</tbody>';
                    html += '</table>';
                    html += '</div>';
                    $('#mutipleOmdTable').html(html);
                }
            }
        },
        getMetroMtrsOMD: function () {
            var self = this;
            var html = '';
            if (self.typologyData[self.deviceId].sub_category_id === 17) {
                var tableData = $.map(self.deviceFields[self.deviceId], function (item) {
                    if (item.field === 'metro_table') {
                        return item;
                    }
                });
                var data = tableData[0].value;
                if (data.metro_mrts_categories.toLowerCase() === 'rolling stock') {
                    $.each(data.data, function (index, val) {
                        var sno = 1;
                        html += '<div class="col-md-12">';
                        html += '<h3>' + val.metro_type + '</h3>';
                        html += '<table class="table">';
                        html += '<thead>';
                        html += '<tr>';
                        html += '<th>S.no</th>';
                        html += '<th>Train no.</th>';
                        if (val.metro_type.toLowerCase() === 'inside metro') {
                            html += '<th>No. Of coaches</th>';
                            html += '<th>No. Of Advertisement Panels</th>';
                        } else {
                            html += '<th>No. of coaches for display of advertisement</th>';
                        }
                        html += '</tr>';
                        html += '</thead>';
                        html += '<tbody>';
                        $.each(val.table, function (tIndex, tVal) {
                            html += '<tr>';
                            html += '<td>' + sno + '</td>';
                            html += '<td>' + tVal.train_no + '</td>';
                            html += '<td>' + tVal.coches + '</td>';
                            if (val.metro_type.toLowerCase() === 'inside metro') {
                                html += '<td>' + tVal.ad_panels + '</td>';
                            }
                            html += '</tr>';
                            sno++;
                        });
                        html += '</tbody>';
                        html += '</table>';
                        html += '</div>';
                    });
                } else if (data.metro_mrts_categories.toLowerCase() === 'inside station') {
                    var sno = 1;
                    html += '<div class="col-md-12">';
                    html += '<h3>' + data.metro_mrts_categories + '</h3>';
                    html += '<table class="table">';
                    html += '<thead>';
                    html += '<tr>';
                    html += '<th>S.no</th>';
                    html += '<th>OMD Site</th>';
                    html += '<th>Height(in m)</th>';
                    html += '<th>Width(in m)</th>';
                    html += '<th>Area(in sq. m)</th>';
                    html += '</tr>';
                    html += '</thead>';
                    html += '<tbody>';
                    $.each(data.data, function (index, val) {
                        html += '<tr>';
                        html += '<td>'+ sno +'</td>';
                        html += '<td>'+ val.omd_type +'</td>';
                        html += '<td>'+ val.omd_height +'</td>';
                        html += '<td>'+ val.omd_width +'</td>';
                        html += '<td>'+ val.omd_area +'</td>';
                        html += '</tr>';
                        sno++;
                    });
                    html += '</tbody>';
                    html += '<tfoot>';
                    html += '<tr>';
                    html += '<td colspan="6"><label><b>Total Area(in sq. m): </b></label>'+ data.total_area +'</td>';
                    html += '</tr>';
                    html += '</tfoot>';
                    html += '</table>';
                    html += '</div>';
                } else if(data.metro_mrts_categories.toLowerCase() === 'station branding') {
                    var sno = 1;
                    html += '<div class="col-md-12">';
                    html += '<h3>' + data.metro_mrts_categories + '</h3>';
                    html += '<table class="table">';
                    html += '<thead>';
                    html += '<tr>';
                    html += '<th>S. No</th>';
                    html += '<th>Facade</th>';
                    html += '<th>Total Facade area</th>';
                    html += '<th>Area of actual display</th>';
                    html += '</tr>';
                    html += '</thead>';
                    html += '<tbody>';
                    $.each(data.data,function(index, val) {
                        html += '<tr>';
                        html += '<td>'+ sno +'</td>';
                        html += '<td>'+ val.facade +'</td>';
                        html += '<td>'+ val.total_facade +'</td>';
                        html += '<td>'+ val.actual_facade +'</td>';
                        html += '</tr>';
                        sno++;
                    });
                    html += '</tbody>';
                    html += '</table>';
                    html += '</div>';
                }
                $('#mutipleOmdTable').html(html);
            }
        },
        getMultipleOMD: function () {
            var self = this;
            var html = '';
            self.multipleOmdData = {};
            if (self.typologyData[self.deviceId].typology_id === '10') {
                var tableData = $.map(self.deviceFields[self.deviceId], function (item) {
                    if (typeof item.value === 'object') {
                        return item;
                    }
                });
                $.each(tableData, function (index, val) {
                    self.multipleOmdData[val.field] = val.value;
                    html += '<div class="col-md-12">';
                    html += '<h3 class="imgsite formh2 mcgborder">' + self.fields[val.field].label + '</h3>';
                    html += '<table class="table">';
                    html += '<thead>';
                    html += '<tr>';
                    html += '<th>Sno.</th>';
                    html += '<th>OMD Site</th>';
                    html += '<th>Height(in m)</th>';
                    html += '<th>Width(in m)</th>';
                    html += '<th>Price(per sq. m)</th>';
                    html += '<th>Area(in sq. m)</th>';
                    if (self.mcgLevel === 2 && self.checkPermission()) {
                        html += '<th>Action</th>';
                    } else {
                        html += '<th>Status</th>';
                    }
                    html += '<th>Comment</th>';
                    html += '</tr>';
                    html += '</thead>';
                    html += '<tbody>';
                    var sNo = 1;
                    $.each(val.value.data, function (dataIndex, dataVal) {
                        var disabled = '';
                        var selectVal = '';
                        var comment = '--N/A--';
                        var status = 'Under Process';
                        if (typeof dataVal.action !== 'undefined') {
                            if (dataVal.action != 0) {
                                disabled = 'disabled="disabled"';
                            }
                            selectVal = dataVal.action;
                            comment = dataVal.comment;
                            status = self.omdActions[dataVal.action];
                        }
                        html += '<tr>';
                        html += '<td>' + sNo + '</td>';
                        html += '<td>' + dataVal.type + '</td>';
                        html += '<td>' + dataVal.height + '</td>';
                        html += '<td>' + dataVal.width + '</td>';
                        html += '<td>' + ( typeof dataVal.price !== 'undefined' ? dataVal.price : '--N/A--') + '</td>';
                        html += '<td>' + dataVal.area + '</td>';
                        if (self.mcgLevel === 2 && self.checkPermission()) {
                            html += '<td>';
                            html += '<select class="form-control omdActionApply" onchange="Devices.applyMultipleOmd(this,\'' + val.field + '\',' + dataIndex + ')" ' + disabled + '>';
                            html += '<option value="">Select Action</option>';
                            html += '<option value="1" ' + (selectVal === '1' ? 'selected="selected"' : '') + '>Approve</option>';
                            html += '<option value="2" ' + (selectVal === '2' ? 'selected="selected"' : '') + '>Re-submit</option>';
                            html += '<option value="3" ' + (selectVal === '3' ? 'selected="selected"' : '') + '>Reject</option>';
                            html += '</select>';
                            html += '</td>';
                        } else {
                            html += '<td>' + status + '</td>';
                        }
                        html += '<td class="omdComment">' + comment + '</td>';
                        html += '</tr>';
                        sNo++;
                    });
                    html += '</tbody>';
                    html += '<tfoot>';
                    if(typeof val.value.licence_fees !== 'undefined') {
                        html += '<tr><td colspan="8" style="text-align: right;"><label><b>Permission Fees(in Rs.): </b></label><span class="' + val.field + '_licence_fees">' + val.value.licence_fees + '</span></td></tr>';
                    }
                    html += '<tr><td colspan="8" style="text-align: right;"><label><b>Total Area(in sq. m): </b></label><span class="' + val.field + '_area">' + val.value.area + '</span></td></tr>';
                    html += '<tr><td colspan="8" style="text-align: right;"><label><b>Total Permission Fees(in Rs.): </b></label><span class="' + val.field + '_annual_fees">' + val.value.annual_fees + '</span></td></tr>';
                    html += '</tfoot>';
                    html += '</table>';
                    html += '</div>';
                });
                $('#mutipleOmdTable').html(html);
            }
        },
        applyMultipleOmd: function (element, field, index) {
            var self = this;
            var dataObj = self.multipleOmdData[field];
            var data = dataObj.data[index];
            swal({
                title: 'Do you want to mark "' + self.omdActions[$(element).val()] + '" to OMD',
                text: "You won't be able to revert this!",
                type: 'warning',
                input: 'textarea',
                inputPlaceholder: 'Comment',
                showLoaderOnConfirm: true,
                preConfirm: function (comment) {
                    return new Promise(function (resolve, reject) {
                        if (!$.trim(comment)) {
                            reject('Please enter comment');
                            return;
                        }
                        if ($(element).val() === '3') {
                            var totalLicenceFees = 0;
                            var totalArea = 0;
                            var newArea = dataObj.area - data.area;
                            var newAnnualFees = parseFloat(dataObj.annual_fees) - (data.price * data.area);
                            dataObj['area'] = String(newArea);
                            dataObj['annual_fees'] = String(newAnnualFees);
                            $.each(self.multipleOmdData, function (OmdDataIndex, OmdDataVal) {
                                totalLicenceFees = totalLicenceFees + parseFloat(OmdDataVal.annual_fees);
                                totalArea = totalArea + parseFloat(OmdDataVal.area);
                            });
                            $(document).find('.annual_license_fees').html(totalLicenceFees);
                            $(document).find('.total_advertising_space').html(totalArea);
                            $('.' + field + '_area').html(String(newArea));
                            $('.' + field + '_annual_fees').html(String(newAnnualFees));
                        }
                        var commentData = ($.trim(comment)).replace(/(\r\n|\n|\r)/gm, "<br>");
                        dataObj.data[index]['action'] = $(element).val();
                        dataObj.data[index]['comment'] = commentData;
                        $(element).parent('td').parent('tr').find('td.omdComment').html(commentData);
                        $.ajax({
                            url: '<?php echo $this->url->build(['action' => 'updateMultipleOmdStatus']); ?>',
                            type: 'POST',
                            data: {items: self.multipleOmdData, deviceId: self.deviceId, appliedOn: field},
                            success: function (response) {
                                resolve();
                            },
                            error: function (result) {
                                reject("Something went wrong on server. Please try again.");
                            }
                        });
                    });
                },
                showCancelButton: true,
                confirmButtonText: 'Yes!',
                cancelButtonText: 'No, cancel!'
            }).then(function () {
                swal(
                        'Success!',
                        'done',
                        'success'
                        );
                $(element).attr('disabled', 'disabled');
            }, function (dismiss) {
                $(element).val("");
            });
        },
        addtionaldocumenthtml: function () {
            var self = this;
            var html = '';
            var data = self.addionaldocs[self.deviceId];
            if (typeof data != 'undefined') {
                html += '<h2 class="imgsite formh2">Addtional Documents - </h2>';

                $.each(data, function (key, val) {
                    html += '<div class="col-md-12">';
                    html += '<h3>Requested on - ' + key + '</h3>';
                    $.each(val, function (keyn, valn) {
                        html += '<div class="col-md-4">';
                        html += '<a title="' + valn.name + '" download="' + valn.name + valn.ext + '" id="document_first" class="downloadDocument" href="' + valn.filename + '"><i class="glyphicon glyphicon-save"></i></a>  ';
                        html += '<p style="text-align: center">' + valn.name + '</p>';
                        html += '</div>';
                    });
                    html += '</div>';
                });
            }
            $('#addtionaldocs').html(html);

        },
        getDeviceDetailsHTML: function (deviceId) {
            var self = this;
            var html = '';
            var deviceFields = self.deviceFields[deviceId];
            var typologyData = self.typologyData[deviceId];
            var deviceData = self.deviceData[deviceId];
            html += '<div class="form-group clearfix">';
            html += '<label class="col-md-6" for="">Typology</label>';
            html += '<div class="col-md-6">';
            html += '<span>' + typologyData.typology + '</span>';
            html += '</div>';
            html += '</div>';
            html += '<div class="form-group clearfix">';
            html += '<label class="col-md-6" for="">Sub Category</label>';
            html += '<div class="col-md-6">';
            html += '<span>' + typologyData.sub_category + '</span>';
            html += '</div>';
            html += '</div>';
            var zoneArrLen = JSON.parse(self.fieldszoneArr.options).length;
            $.each(deviceFields, function (index, val) {
                var fieldData = self.fields[val.field];
                if(fieldData.type !== 'json') {
					/*
                    if (fieldData.type !== 'file' && (typeof val.value !== 'object'))  {
                    */
                     if (fieldData.type !== 'file' && (typeof val.value !== 'object') && 
                    (fieldData.label !== 'Zone'|| zoneArrLen != 1)) {
                        html += '<div class="form-group clearfix">';
                        html += '<label class="col-md-6" for="">' + fieldData.label + '</label>';
                        html += '<div class="col-md-6">';
                        html += '<span class="' + val.field + '">' + (val.value ? val.value : '--N/A--') + '</span>';
                        html += '</div>';
                        html += '</div>';
                    } else {
						if(fieldData.label !== 'Zone' || zoneArrLen != 1) {
							self.deviceImages.push({label: fieldData.label, image: self.imagePath + val.value});
						}
                    }
                }
            });
            $('#deviceDetailBlock').html(html);
        },
        getDeviceImagesHTML: function () {
            var self = this;
            var html = '';
            var extRegex = /(?:\.([^.]+))?$/;
            var deviceImages = self.deviceImages;
            $.each(deviceImages, function (index, image) {
                var fileExt = extRegex.exec(image.image);
                var ValidImageTypes = ["gif", "jpeg", "png", "jpg"];
                var imageHTML = '<a href="' + image.image + '" download="' + image.label + '.' + fileExt[1] + '" title="Download ' + image.label + '"><img src="' + image.image + '" /></a>';
                if ($.inArray(fileExt[1], ValidImageTypes) < 0) {
                    imageHTML = '<a href="' + image.image + '" download="' + image.label + '.' + fileExt[1] + '" class="downloadDocument" title="Download ' + image.label + '"><i class="glyphicon glyphicon-save"></i></a>';
                }
                html += '<div class="col-md-3">';
                html += '<div class="siteimg">';
                html += imageHTML;
                html += ' </div>';
                html += '<label style="float: left; width: 100%; text-align: center;">' + image.label + '</label>';
                html += ' </div>';
            });
            $('#siteImages').html(html);
        },
        getDeviceAddressHTML: function (deviceId) {
            var html = '';
            var extRegex = /(?:\.([^.]+))?$/;
            var addresses = this.addressData[deviceId];
            var deviceData = this.deviceData[deviceId];
            var userId = deviceData.user_id;
            var documents = userId ? this.usersDocuments[userId] : [];
            $.each(addresses, function (addressLabel, address) {
                if (addressLabel !== 'company') {
                    html += '<div class="form-group clearfix">';
                    html += '<label class="col-md-12 darkgrey" for="exampleInputEmail1">' + addressLabel + ' Address</label>';
                    html += '</div>';
                }
                $.each(address, function (index, val) {
                    html += '<div class="form-group clearfix">';
                    html += '<label class="col-md-6" for="exampleInputEmail1">' + val.label + '</label>';
                    html += '<div class="col-md-6">';
                    html += '<span>' + val.value + '</span>';
                    html += '</div>';
                    html += '</div>';
                });
            });
            if (parseInt(deviceData.representative_id) !== 1) {
                html += '<div class="form-group clearfix">';
                html += '<label class="col-md-6" for="">Company Name</label>';
                html += '<div class="col-md-6">';
                html += '<span>' + deviceData.company_name + '</span>';
                html += '</div>';
                html += '</div>';
                if (deviceData.director_name) {
                    html += '<div class="form-group clearfix">';
                    html += '<label class="col-md-6" for="">Director Name</label>';
                    html += '<div class="col-md-6">';
                    html += '<span>' + deviceData.director_name + '</span>';
                    html += '</div>';
                    html += '</div>';
                }
                html += '<div class="form-group clearfix">';
                html += '<label class="col-md-6" for="">Date of Incorporation</label>';
                html += '<div class="col-md-6">';
                html += '<span>' + deviceData.incorporation_date + '</span>';
                html += '</div>';
                html += '</div>';
            } else {
                html += '<div class="form-group clearfix">';
                html += '<label class="col-md-6" for="">Individual/Owner Name</label>';
                html += '<div class="col-md-6">';
                html += '<span>' + deviceData.name + '</span>';
                html += '</div>';
                html += '</div>';
            }
            html += '<div class="form-group clearfix">';
            html += '<label class="col-md-6" for="">Email</label>';
            html += '<div class="col-md-6">';
            html += '<span>' + deviceData.email + '</span>';
            html += '</div>';
            html += '</div>';
            html += '<div class="form-group clearfix">';
            html += '<label class="col-md-6" for="">Mobile</label>';
            html += '<div class="col-md-6">';
            html += '<span>' + deviceData.phone + '</span>';
            html += '</div>';
            html += '</div>';
            html += '<div class="form-group clearfix">';
            html += '<label class="col-md-6" for="">Entity Type</label>';
            html += '<div class="col-md-6">';
            html += '<span>' + deviceData.representative + '</span>';
            html += '</div>';
            html += '</div>';

            $.each(documents, function (index, val) {
                if (val.document) {
                    var fileEx = extRegex.exec(val.document);
                    html += '<div class="form-group clearfix">';
                    html += '<label class="col-md-6" for="">' + val.label + '</label>';
                    html += '<div class="col-md-6">';
                    html += '<span><a href="' + val.document + '" download="' + val.label + '.' + fileEx[1] + '">Download</a></span>';
                    html += '</div>';
                    html += '</div>';
                }
            });
            if (deviceData.authorised_contact_person) {
                html += '<div class="form-group clearfix">';
                html += '<label class="col-md-6" for="">Authorised Contact Person</label>';
                html += '<div class="col-md-6">';
                html += '<span>' + deviceData.authorised_contact_person + '</span>';
                html += '</div>';
                html += '</div>';
            }
            $('#deviceAddressDetailBlock').html(html);
        },
        getCommentsHTML: function () {
            var self = this;
            var html = '';
            var maxMCGLevel = 0;
            var comments = self.comments[self.deviceId];
            var lastStatus = '';
            if (typeof comments !== 'undefined') {
                maxMCGLevel = comments[0].mcg_level;
                lastStatus = comments[0].status;
                $.each(comments, function (index, comment) {
					var userType  = comment.type.replace("G", "F");
					//var userType  = comment.type;
                    html += '<div class="form-group clearfix">';
                    html += '<label class="col-md-12 darkgrey" for="">Comment ' + (index + 1) + '</label>';
                    html += '</div>';
                    html += '<div class="form-group clearfix">';
                    html += '<label class="col-md-6" for="">User Name</label>';
                    html += '<div class="col-md-6">';
                    html += '<span>' + comment.user_name + ' ( ' + userType + ' )</span>';
                    html += '</div>';
                    html += '</div>';
                    html += '<div class="form-group clearfix">';
                    html += '<label class="col-md-6" for="">Status</label>';
                    html += '<div class="col-md-6">';
                    html += '<span>' + comment.status + '</span>';
                    html += '</div>';
                    html += '</div>';
                    html += '<div class="form-group clearfix">';
                    html += '<label class="col-md-6" for="">Comment</label>';
                    html += '<div class="col-md-6">';
                    html += '<span>' + comment.comment + '</span>';
                    html += '</div>';
                    html += '</div>';
                    html += '<div class="form-group clearfix">';
                    html += '<label class="col-md-6" for="">Created At</label>';
                    html += '<div class="col-md-6">';
                    html += '<span>' + comment.created + '</span>';
                    html += '</div>';
                    html += '</div>';
                    $.each(comment.comment_data, function (commentIndex, commentVal) {
                        html += '<div class="form-group clearfix">';
                        html += '<label class="col-md-6" for="">' + self.fields[commentVal.field_key].label + '</label>';
                        html += '<div class="col-md-6">';
                        html += '<span>' + commentVal.input_value + '</span>';
                        html += '</div>';
                        html += '</div>';
                    });
                });
            }
            self.deviceMcgMaxLevel = maxMCGLevel;
            self.lastStatus = lastStatus;
            if (html) {
                $('#previous-comments').show();
            } else {
                $('#previous-comments').hide();
            }
            $('#previous-comments .form-group').html(html);
        },
        checkPermission: function () {
            var self = this;
            console.log(self.mcgLevel+'---'+self.deviceMcgMaxLevel);
            console.log('rohit kaushal');
            if (self.lastStatus === 'Return') {
                if ((parseInt(self.deviceMcgMaxLevel) - 1) === parseInt(self.mcgLevel)) {
                    return true;
                }
                return false;
            } else if (self.lastStatus === 'document requested') {
                if (parseInt(self.mcgLevel) === 2) {
                    return true;
                }
                return false;
            }
            
            if(self.lastStatus === 'Return for Re submission'){
							if (parseInt(self.mcgLevel) === 2) {
                    return true;
                }
                return false;
						}
            
            /*
            if (self.lastStatus === 'Return for Re submission' && self.mcgLevel === 2) {
                return true;
            }
            */
            
            if ((parseInt(self.deviceMcgMaxLevel)) === (parseInt(self.mcgLevel) - 1)) {
                return true;
            }
            return false;
        },
        showForm: function () {
            $(document).find('.error-message').remove();
            $('#mcgCommentForm').hide();
            if (this.checkPermission()) {
                $('#mcgCommentForm').show();
            } else if (this.lastStatus === 'Return') {
                $('#previous-comments').before('<p class="error-message">OMD is under the process of MCF' + (parseInt(this.deviceMcgMaxLevel) - 1) + '</p>');
            } else if (this.lastStatus === 'document requested') {
                $('#previous-comments').before('<p class="error-message">OMD is under the process of MCF2</p>');
            } else if(this.lastStatus === 'Return for Re submission') {
                $('#previous-comments').before('<p class="error-message">OMD is under the process of MCF2</p>');
            } else if (this.lastStatus !== 'Return' && this.lastStatus !== 'Approve') {
                $('#previous-comments').before('<p class="error-message">OMD is under the process of MCF' + (parseInt(this.deviceMcgMaxLevel) + 1) + '</p>');
            }
        }
    };

    var Mcg = {
        docCounter: 0,
        showLoader: function () {
            var docHeight = $(document).height();
            $('#loaderLayout span').css({height: docHeight + 'px'});
            $('#loaderLayout').show();
        },
        hideLoader: function () {
            $('#loaderLayout').hide();
        },
        addComment: function (element) {
            var omdActionButtons = $(document).find('.omdActionApply');
            if (omdActionButtons.length) {
                $(document).find('.error-message').remove();
                var error = 0;
                var isResumission = 0;
                omdActionButtons.each(function () {
                    if (!$.trim($(this).val())) {
                        error = 1;
                        $(this).after('<p class="error-message">This field is required</p>');
                    } else if ($.trim($(this).val()) === '2') {
                        isResumission = 1;
                    }
                });
                if (error) {
                    var errorTop = ($(document).find('.error-message').first().offset().top) - 100;
                    $('html, body').stop().animate({scrollTop: errorTop + 'px'}, 500, 'swing');
                    return false;
                }
                if (isResumission) {
                    var statusArr = $.map(element.serializeArray(), function (item) {
                        if (item.name === 'status') {
                            return item;
                        }
                    });
                    if ((statusArr[0].value).toLowerCase() === 'forward') {
                        swal(
                                'Operation denied!',
                                'You need to request resubmission before forwarding the application!',
                                'info'
                                );
                        return false;
                    }
                }
            }
            this.postComment(element.serialize());
        },
        postComment: function (data) {
            this.showLoader();
            var permission = Devices.checkPermission();
            if (!permission) {
                alert("You have no permission to perform this action");
                return false;
            }
            $.ajax({
                url: '<?php echo $this->Url->build(["controller" => "Mcg", "action" => "addComment"]); ?>',
                type: 'POST',
                data: data,
                dataType: 'JSON',
                success: function (response) {
                    $('#loaderLayout').hide();
                    if (response.error === 2) {
                        alert(response.message);
                        Devices.comments[Devices.deviceId] = response.data;
                        Devices.getCommentsHTML();
                        Devices.showForm();
                    } else if (response.error === 1) {
                        $.each(response.data, function (index, val) {
                            if (index === 'reasonCheck') {
                                index = index + '[]';
                            }
                            $('#mcgCommentForm [name="' + index + '"]').parents('.form-group').append('<p class="error-message">' + val[Object.keys(val)[0]] + '</p>');
                        });
                    } else {
                        Devices.comments[Devices.deviceId] = response.data;
                        Devices.getCommentsHTML();
                        Devices.showForm();
                        if (response.refresh) {
                            location.reload(true);
                        } else if (response.data[0].status === 'Return') {
                            $('.device_' + Devices.deviceId + ' td.sts-pending').html('under process - MCF' + (parseInt(Devices.mcgLevel) - 1));
                            $('.device_' + Devices.deviceId + ' td.sts-pending').attr('style', '');
                        } else {
                            $('.device_' + Devices.deviceId + ' td.sts-pending').html('under process - MCF' + (parseInt(Devices.mcgLevel) + 1));
                            $('.device_' + Devices.deviceId + ' td.sts-pending').attr('style', '');
                        }
                    }
                },
                error: function (result) {
                    $('#loaderLayout').hide();
                    alert('error');
                }
            });
        },
        requestAdditionalDocs: function () {
            var html = '<form action="javascript:void(0)" onsubmit="Mcg.saveDocRequest(this)">';
            html += '<input type="hidden" name="device" value="' + Devices.deviceId + '">';
            html += '<div class="col-md-11 requested-doc-con">';
            html += '<span style="float: left; width: 49%;"><input type="text" class="form-control requested-doc" placeholder="Document Name" name="document[' + this.docCounter + ']" style="float: left;"></span>';
            html += '<span style="float: right; width: 49%;"><input type="text" class="form-control requested-doc-reason" placeholder="Reason" name="reason[' + this.docCounter + ']" style="float: right;"></span>';
            html += '</div>';
            html += '<a href="javascript:void(0)" style="float: left; margin: 0 0 0 18px;" onclick="Mcg.addMoreDocs(this)">Add More Document</a>';
            html += '<div class="col-md-11">';
            html += '<input type="submit" class="btn btn-default approvd commentSubmit" style="margin: 13px 0 0 0" value="Notify">';
            html += '</div>';
            html += '</form>';
            $('#documentsList').html(html);
            $('#documentsModal').modal({backdrop: 'static', keyboard: false});
        },
        addMoreDocs: function (element) {
            this.docCounter++;
            var html = '<div class="col-md-11 requested-doc-con">';
            html += '<span style="float: left; width: 49%;"><input type="text" class="form-control requested-doc" placeholder="Document Name" name="document[' + this.docCounter + ']" style="float: left;"></span>';
            html += '<a href="javascript:void(0)" style="float: right; margin: 16px -17px 0 0" onclick="Mcg.removeDoc(this)">X</a>';
            html += '<span style="float: right; width: 49%;"><input type="text" class="form-control requested-doc-reason" placeholder="Reason" name="reason[' + this.docCounter + ']" style="float: right;"></span>';
            html += '</div>';
            $(element).before(html);
        },
        removeDoc: function (element) {
            $(element).parent('div').remove();
        },
        saveDocRequest: function (element) {
            var error = 0;
            var self = this;
            $(document).find('.error-message').remove();
            $(element).find('.requested-doc-con').each(function () {
                var docElement = $(this).find('.requested-doc');
                var reasonElement = $(this).find('.requested-doc-reason');
                var doc = $.trim(docElement.val());
                var reason = $.trim(reasonElement.val());
                if (!doc) {
                    error = 1;
                    docElement.after('<p class="error-message">Please enter document Name</p>');
                }
                if (!reason) {
                    error = 1;
                    reasonElement.after('<p class="error-message">Please enter reason</p>');
                }
            });
            if (!error) {
                self.showLoader();
                $.ajax({
                    url: '<?php echo $this->Url->build(['action' => 'requestAdditionalDocsOmd']); ?>',
                    type: 'POST',
                    data: $(element).serialize(),
                    dataType: 'JSON',
                    success: function (response) {
                        if (!response.error) {
                            location.reload();
                        } else {
                            self.hideLoader();
                        }
                    },
                    error: function () {
                        self.hideLoader();
                        alert("something went wrong on server.");
                    }
                });
            }
        }
    };

    $('.deviceRow').on('click', function () {
        $('#deviceBlock').slideDown();
        var deviceId = $(this).attr('devicerow');
        $('.deviceRow').removeClass('active-device');
        $(this).addClass('active-device');
        Devices.deviceId = deviceId;
        Devices.deviceImages = [];
        Devices.getDeviceInformation();
        $('html,body').animate({scrollTop: $("#deviceBlock").offset().top}, 'slow');
    });
    $('.commentSubmit').click(function () {
        var submitType = $(this).val();
        $('#mcgCommentForm input[type="hidden"][name="status"]').val(submitType);
        $('#mcgCommentForm input[type="hidden"][name="device"]').val(Devices.deviceId);
        $('#mcgCommentForm').submit();
    });
    $('#mcgCommentForm').on('submit', function (e) {
        e.preventDefault();
        $(document).find('.error-message').remove();
        Mcg.addComment($(this));
    });
</script>
