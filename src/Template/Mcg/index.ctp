<?php
$status = $this->request->query('status');
?>
<div id="page-content-wrapper">
    <!-- Keep all page content within the page-content inset div! -->
    <div class="page-content inset dashboard">
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-V-cjIUZu6DPHpERzLBIV5OnR_AxkRtw"></script>
        <div class="dash-head">
			<?php  $user['type'] = str_replace("G","F",$user['type']);?>
            <p>Welcome <?php echo $user['display_name'] . ' (' . $user['type'] . ')'; ?></p>
            <h1>Dashboard</h1>
        </div>
        <div class="map-vw">
            <div id="map-canvas" style="height: 400px;"></div>
        </div>
        <div class="city-status clearfix">
            <ul class="pull-right">
                <li><a href="<?php echo $this->Url->build([ "controller" => "Mcg"]); ?>" class="<?php echo empty($status) ? 'active' : ''; ?>">All</a></li>
                <li><a href="<?php echo $this->Url->build([ "controller" => "Mcg"]) . "?status=pending"; ?>" class="<?php echo!empty($status) && 'pending' == $status ? 'active' : ''; ?>">Pending</a></li>
                <li><a href="<?php echo $this->Url->build([ "controller" => "Mcg"]) . "?status=approved"; ?>" class="<?php echo!empty($status) && 'approved' == $status ? 'active' : ''; ?>">Approved</a></li>
                <li><a href="<?php echo $this->Url->build([ "controller" => "Mcg"]) . "?status=rejected"; ?>" class="<?php echo!empty($status) && 'rejected' == $status ? 'active' : ''; ?>">Rejected</a></li>
            </ul>
        </div>
        <div class="device-list">
            <h2>Outdoor Media Device List </h2>
            <div class="table-responsive">
                <table class="table">
                    <thead class="thead-default">
                        <tr>
                            <th>Device Id <?php echo $this->Paginator->sort("companyDevices.id", "<span class='glyphicon glyphicon-triangle-{$iconName}'></span>", ["escape" => false, "direction" => "asc"]); ?></th>
                            <th>Location</th>
                            <th>Status</th>
                            <th>Concession Fees</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($devices)) {
                            foreach ($devices as $key => $deviceArray) {
                                //pr($deviceArray);
                                ?>								
                                <tr>
                                    <th scope="row"><?php echo $deviceArray['companyDevices_id']; ?></th>
                                    <td><?php echo isset($deviceArray['companyDevice_location']) ? $deviceArray['companyDevice_location'] : 'N/A'; ?></td>
                                    <!--<td class="sts-pending sts-apprd sts-rejected">Pending</td>-->
                                    <td class="sts-<?php echo $deviceArray['companyDevices_status']; ?>"><?php echo ucfirst($deviceArray['companyDevices_status']); ?></td>
                                    <td><?php echo isset($deviceArray['companyDevice_concession_fees']) ? $deviceArray['companyDevice_concession_fees'] : 'N/A'; ?></td>	
                                </tr>							
                                <?php
                            }
                            if ($this->Paginator->numbers()) {
                                ?>
                                <tr>
                                    <td colspan="4"><ul class="pagingPages"><?php echo $this->Paginator->prev() . $this->Paginator->numbers() . $this->Paginator->next(); ?></ul></td>
                                </tr>
                                <?php
                            }
                            ?>
                        <?php } else {
                            ?>
                            <tr>
                                <td colspan="4" class="text-center">No results found.</td>
                            </tr>							
                            <?php
                        }
                        ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    function initialize() {

        var marker;
        var latLngObj = '<?php echo $latLngArray; ?>';

        var latLngObj = jQuery.parseJSON(latLngObj);

        var map;
        var i;
        var bounds = new google.maps.LatLngBounds();

        var mapOptions = {
            center: new google.maps.LatLng(28.4089, 77.3178),
            zoom: 12,
        };

        // Display a map on the page
        map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
        
        var myLatLng = {lat: 28.4089, lng: 77.3178};
        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'faridabad'
        });
       
		/*
        if (latLngObj.length > 0) {
			
            $.each(latLngObj, function (index, val) {
				

                var position = new google.maps.LatLng(val.lat, val.lng);
                bounds.extend(position);
                marker = new google.maps.Marker({
                    position: position,
                    map: map
                });
				// Automatically center the map fitting all markers on the screen
                map.fitBounds(bounds);
            });

        }
        */

    }

    initialize();

</script>
