<div id="page-content-wrapper">
    <!-- Keep all page content within the page-content inset div! -->
    <div class="page-content inset app-status">
        <div class="headings-all dash-in">
            <h1>Users Report</h1>
        </div>
        <div class="regist-form">
            <div class="row">
                <h2 class="col-md-12 formh2">User Information</h2>
                <?php
                echo $this->Form->create('Reports', ['novalidate' => true,'id' => 'exportReportForm']);
                echo $this->Form->input('display_name', ['type' => 'text', 'class' => 'form-control', 'label' => 'User Name', 'templates' => [
                        'inputContainer' => '<div class="form-group col-md-4">{{content}}</div>'
                    ], 'placeholder' => 'Enter User Name']);
                echo $this->Form->input('receipt_id', ['type' => 'text', 'class' => 'form-control', 'label' => 'Receipt Id', 'templates' => [
                        'inputContainer' => '<div class="form-group col-md-4">{{content}}</div>'
                    ], 'placeholder' => 'Enter Receipt Id']);
                echo $this->Form->input('representative_id', ['empty' => 'Select Type of entity', 'type' => 'select', 'options' => $representatives, 'label' => 'Type of entity', 'class' => 'form-control', 'templates' => [
                        'inputContainer' => '<div class="form-group col-md-4">{{content}}</div>'
                ]]);
                ?>
                <div class="clearfix"></div>
                <input type="submit" class="btn btn-default" value="Get Report" name="reportSubmit"/>
                <a href="javascript:void(0)" class="btn btn-default" id="exportReportButton">Download Report</a>
                </form>
            </div>
        </div>
        <div class="device-list">
            <h2>Outdoor Media Users Report</h2>
            <div class="table-responsive">
                <table class="table">
                    <thead class="thead-default">
                        <tr>
                            <th>Receipt No</th>
                            <th>Registration number</th>
                            <th>Company/Individual Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>G8 Receipt Number</th>
                            <th>Payment Tracking Id (ccavenue)</th>
                            <th>Bank Reference Number</th>
                            <th>Amount (With ccavenue charges)</th>
                            <th>Amount</th>
                            <th>Payment Mode</th>
                            <th>Card Name</th>
                            <th>Registered At</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($users as $user) {
							$g8_reponse	=	'';
							if(!empty($user['g8_response'])){
								$g8_reponse = json_decode($user['g8_response'],true);
							}
                            ?>
                            <tr>
                                <td><?php echo $user['receipt_no']; ?></td>
                                <td><?php echo $user['registration_number']; ?></td>
                                <td><?php echo $user['display_name']; ?></td>
                                <td><?php echo $user['email']; ?></td>
                                <td><?php echo $user['mobile']; ?></td>
                                <td><?php echo (!empty($g8_reponse)) ? $g8_reponse['RECEIPT_NUMBER'] : ''; ?></td>
                                <td><?php echo $user['payment_tracking_id']; ?></td>
                                <td><?php echo $user['bank_ref_no']; ?></td>
                                <td><?php echo $user['cc_amount']; ?></td>
                                <td><?php echo $user['amount']; ?></td>
                                <td><?php echo $user['payment_mode']; ?></td>
                                <td><?php echo $user['card_name']; ?></td>
                                <td><?php echo $user['registered_at']; ?></td>
                            </tr>
                            <?php
                        }
                        if (!$users) {
                            ?>
                            <tr>
                                <td colspan="12" style="text-align: center;">No Records</td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#exportReportButton').click(function(){
        var html = '<input type="hidden" value="export" name="request_type">';
        $('#exportReportForm').prepend(html).submit();
    });
</script>
