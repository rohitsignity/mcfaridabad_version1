<header class="header headerNew">
    <!-- navbar -->
    <nav class="navbar">
        
            <div class="cont-info">
                <div class="container">
                    <div class="helpdesk">Help Desk(9AM to 5PM)0129-2415549, 0129-2411664</div>
				<!---	
                <div class="email-blk"><img src="<?php echo $this->Url->image('email.png'); ?>" />
                    <p><a href="mailto: advt@mcg.gov.in" class="contact-links">advt@mcg.gov.in</a></p>
                </div>
                --->
                
                <?php
                if ($loggedIn) {
                    ?>
                    <a class="login btn" href="<?php echo $this->Url->build([ "controller" => "Dashboard", "action" => "index"]); ?>">Dashboard</a>
                    <a class="login btn" href="<?php echo $this->Url->build([ "controller" => "Users", "action" => "logout"]); ?>">Logout</a>
                    <?php
                } else {
                    ?>
                   
                    <a class="login btn" href="<?php echo $this->Url->build([ "controller" => "Users", "action" => "login","prefix" => false]); ?>"><img src="<?php echo $this->Url->image('login-icon.png'); ?>" /> Login</a>
                   
                    <a class="login register btn" href="<?php echo $this->Url->build([ "controller" => "Users", "action" => "registration","prefix" => false]); ?>"><img src="<?php echo $this->Url->image('registration.png'); ?>" /> Registration</a>
                  
                    <a class="login register btn" href="<?php echo $this->Url->build([ "controller" => "Users", "action" => "index","prefix" => "guest"]); ?>"><img src="<?php echo $this->Url->image('event-login.png'); ?>" /> Event Login</a>
                   
                    <?php
                }
                ?>
                </div>
            </div>
        <div class="container">
            <div class="navbar-header">
                <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="<?php echo $this->Url->build([ "controller" => "Home", "action" => "index", "prefix" => false]); ?>" class="navbar-brand">
					<img src="<?php echo $this->Url->image('faridabad-logo.jpg'); ?>" />
				</a>
            </div>
            <div class="navbar-collapse collapse pull-right m-t-24" id="navbar" aria-expanded="false">
                <ul class="nav navbar-nav sMain-navbar">
                    
                    <li class="sReal-link">
                        <a href="<?php echo $this->Url->build([ "controller" => "Home", "action" => "index","prefix" => false]); ?>" class="dropdown-toggle" data-toggle="dropdown" >Advertisement bye laws</a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a target="_blank" href="<?php echo $this->request->webroot . 'downloads/hry_advertisement_bye-law.pdf'; ?>">Out Door Media Bye laws</a></li>
                            <li><a target="_blank" href="<?php echo $this->request->webroot . 'downloads/byelaws_amendments.pdf'; ?>">Bye laws Amendments</a></li>
                            
                        </ul>
                        
 
 
                    </li>

                    <li class="sReal-link">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" >Fees</a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a target="_blank" href="<?php echo $this->request->webroot . 'downloads/Typology_Description.pdf'; ?>">Typology Description</a></li>
                            <li><a target="_blank" href="<?php echo $this->request->webroot . 'downloads/Permission_fee.pdf'; ?>">Permission Fee</a></li>
                            <!---
                            <li><a target="_blank" href="<?php echo $this->request->webroot . 'downloads/byelaws_amendments.pdf'; ?>">Bye Door Media</a></li> 
							---->
                        </ul>
                    </li>
                    
                    <li class="sReal-link">
                        <a href="#"  class="dropdown-toggle" data-toggle="dropdown" >Forms </a>
                         <ul class="dropdown-menu dropdown-menu-right">
							<!--- 
                            <li><a target="_blank" href="<?php echo $this->request->webroot . 'downloads/hry_advertisement_bye-law.pdf'; ?>">Registration Form </a></li>
                            <li><a target="_blank" href="<?php echo $this->request->webroot . 'downloads/byelaws_amendments.pdf'; ?>">Tripartite License Agreement</a></li>
                            <li><a target="_blank" href="<?php echo $this->request->webroot . 'downloads/byelaws_amendments.pdf'; ?>">Format for Affidavit Certifying that Entity / Directors of Entity are not Black listed </a></li> 
                            <li><a target="_blank" href="<?php echo $this->request->webroot . 'downloads/byelaws_amendments.pdf'; ?>">Format of Affidavit certifying No Pending Dues</a></li> 
                            <li><a target="_blank" href="<?php echo $this->request->webroot . 'downloads/byelaws_amendments.pdf'; ?>">Format for Work Experience in Advertising Business</a></li> 
							--->
							<li><a target="_blank" href="javascript:;">Registration Form </a></li>
                            <li><a target="_blank" href="javascript:;">Tripartite License Agreement</a></li>
                            <li><a target="_blank" href="javascript:;">Format for Affidavit Certifying that Entity / Directors of Entity are not Black listed </a></li> 
                            <li><a target="_blank" href="javascript:;">Format of Affidavit certifying No Pending Dues</a></li> 
                            <li><a target="_blank" href="javascript:;">Format for Work Experience in Advertising Business</a></li> 
							
                         </ul>
                    </li>
                    <li class="sReal-link">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" > Process </a>
                         <ul class="dropdown-menu dropdown-menu-right">
                            <li><a target="_blank" href="<?php echo $this->request->webroot . 'img/flowchart_MCG.jpg'; ?>">Flow Chart</a></li>
                            
                        </ul>
                    </li>
                    <li class="sReal-link">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                           Tender
                        </a>
                        
                    </li> 
                    <li class="sReal-link">
                        <a href="http://mcfaridabad.org/contact-us-new.aspx" target="_blank" class=""> Contact Us </a>
                    </li>
                    <li class="sReal-link">
                        <a href="javascript:;" href="#" class=""> Approved Locations </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
