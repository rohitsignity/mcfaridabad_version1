<header class="header">
    <!-- navbar -->
    <nav class="navbar">
        
            <div class="cont-info">
                <div class="container">
                <div class="email-blk"><img src="<?php echo $this->Url->image('email.png'); ?>" />
                    <p><a href="mailto: advt@mcg.gov.in" class="contact-links">advt@mcg.gov.in</a></p>
                </div>
                <div class="call-cont"><img src="<?php echo $this->Url->image('phone.png'); ?>" />
                    <p><a href="tel: 18001801817" class="contact-links">18001801817</a></p>
                </div>
                <?php
                if ($loggedIn) {
                    ?>
                    <a class="login btn" href="<?php echo $this->Url->build([ "controller" => "Dashboard", "action" => "index"]); ?>">Dashboard</a>
                    <a class="login btn" href="<?php echo $this->Url->build([ "controller" => "Users", "action" => "logout"]); ?>">Logout</a>
                    <?php
                } else {
                    ?>
                    <a class="login btn" href="<?php echo $this->Url->build([ "controller" => "Users", "action" => "login","prefix" => false]); ?>">Login</a>
                    <a class="login register btn" href="<?php echo $this->Url->build([ "controller" => "Users", "action" => "registration","prefix" => false]); ?>">Registration</a>
                    <a class="login register btn" href="<?php echo $this->Url->build([ "controller" => "Users", "action" => "index","prefix" => "guest"]); ?>">Event Login</a>
                    <?php
                }
                ?>
                </div>
            </div>
        <div class="container">
            <div class="navbar-header">
                <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="<?php echo $this->Url->build([ "controller" => "Home", "action" => "index", "prefix" => false]); ?>" class="navbar-brand"><img src="<?php echo $this->Url->image('logo.png'); ?>" /></a>
            </div>
            <div class="navbar-collapse collapse pull-right" id="navbar" aria-expanded="false">
                <ul class="nav navbar-nav">
                    <li class="">
                        <a href="<?php echo $this->Url->build([ "controller" => "Home", "action" => "index","prefix" => false]); ?>">Home</a>
                    </li>
<!--                    <li class="">
                        <a href="<?php //echo $this->Url->build(['controller' => 'aboutUs',"action" => "index"]);?>">About</a>
                    </li>-->
<!--                    
                    <li class="">
                        <a href="javascript:void(0)">Online Services</a>
                    </li>-->
                    <li class="">
                        <a href="http://www.mcg.gov.in/Contact-Us.aspx">Contact us</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
