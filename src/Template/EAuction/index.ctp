<div class="page-content inset dashboard">
    <div class="device-list">
        <h2>eAuction</h2>
        <?php
            if($receipt) {
                ?>
        <a href="<?php echo $receipt;?>" download="receipt.pdf" class="receiptDownload" style="margin: -58px 0px 0px;">Download Payment receipt</a>
                <script>
                    $(document).ready(function(){
                        $('.receiptDownload').get(0).click();
                    });
                </script>
            <?php
            }
        ?>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-default">
                    <tr>
                        <th>#</th>
                        <th>Tender Title</th>
                        <th>Tender Id</th>
                        <th>Tender Value</th>
                        <th>Tender closing date and time</th>
                        <th>Bid opening date and time</th>
                        <th>Application Last Date</th>
                        <th>Application Status</th>
                        <th>View Tender</th>
                        <th>Bid</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (count($tenders)) {
                        $sNo = (($this->Paginator->current() - 1) * $limit) + 1;
                        foreach ($tenders as $tender) {
                            $bid = '<a href="'.$this->Url->build([ 'controller' => 'EAuction','action' => 'startBid',$tender['id']]).'">Start Bid</a>';
                            if($tender['paid'] == 0) {
                                $bid = '<a href="'.$this->Url->build([ 'controller' => 'EAuction','action' => 'apply',$tender['id']]).'">Apply</a>';
                            } else if(!$tender['application_status'] && $tender['paid'] == 1) {
                                $bid = 'Under processing';
                            } else if($tender['application_status'] == 3 && $tender['paid'] == 1) {
                                $bid = '--N/A--';
                            } else if (date('Y-m-d H:i:s', strtotime($tender['start_date'])) > date('Y-m-d H:i:s')) {
                                $date1 = new DateTime(date('Y-m-d'));
                                $date2 = new DateTime(date('Y-m-d', strtotime($tender['start_date'])));
                                $diff = $date2->diff($date1)->format("%a");
                                if($diff != 0) {
                                    $bid = '<a href="javascript:void(0)">'.$diff.' day'.($diff > 1 ? 's' : '').' left to start</a>';
                                } else {
                                    $bid = '<a href="javascript:void(0)" onclick="Omd.checkBidStart('.$tender['id'].')">Bidding will Open at '.date('h:iA', strtotime($tender['start_date'])).'</a>';
                                }
                            } else if (date('Y-m-d H:i:s', strtotime($tender['end_date'])) < date('Y-m-d H:i:s')) {
                                $bid = '<a href="javascript:void(0)">Tender Expired</a>';
                            }
                            ?>
                            <tr>
                                <td><?= $sNo; ?></td>
                                <td><?= $tender['name']; ?></td>
                                <td><?= 'SMC-Tender-'.$tender['id']; ?></td>
                                <td>₹<?= number_format($tender['tender_value'],2); ?></td>
                                <td><?= date('d M, Y h:iA', strtotime($tender['end_date'])); ?></td>
                                <td><?= date('d M, Y h:iA', strtotime($tender['start_date'])); ?></td>
                                <td><?= date('d M, Y', strtotime($tender['application_date'])); ?></td>
                                <td><?= $tender['application_status'] == '2' ? 'Not Applied' : ($tender['application_status'] == '0' ? 'Processing' : ($tender['application_status'] == 1 ? 'Approved' : 'Rejected'))?></td>
                                <td><a href="javascript:void(0)" onclick="Omd.downloadBidDocument(<?= $tender['id']; ?>)">Download Document</a></td>
                                <td><?= $bid; ?></td>
                            </tr>
                            <?php
                            $sNo++;
                        }
                        if ($this->Paginator->numbers()) {
                            ?>
                            <tr>
                                <td colspan="10"><ul class="pagingPages"><?php echo $this->Paginator->prev() . $this->Paginator->numbers() . $this->Paginator->next(); ?></ul></td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr><td colspan="10" class="text-center">No Record Found</td></tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<a id="downloadBidDoc" style="display: none;">download</a>