<div class="page-content inset dashboard">
    <div class="device-list">
        <h2>Live Bidding <?= $tender['name']; ?></h2>
        <div class="table-responsive">
            <form action="javascript:void(0)" onsubmit="Bidding.addBid(this)">
                <div class="form-group">
                    <input type="text" name="bidAmount" class="form-control bidField" onkeyup="Bidding.isNumeric(this)" placeholder="Bid Amount*" autocomplete="off">
                </div>
                <input type="submit" value="Add Bid" class="btn btn-default bidButton">
            </form>
            <table class="table">
                <tbody>
                    <tr>
                        <th>Latest Bid</th>
                        <td id="latestBid">Start Bidding</td>
                    </tr>
                    <tr>
                        <th>Incremental</th>
                        <td><?= $bidIncrimentPercentage;?>%</td>
                    </tr>
                    <tr>
                        <th>Tender Value</th>
                        <td>₹<?= number_format($tender['tender_value'],2);?></td>
                    </tr>
                    <tr>
                        <th>Minimum Guarantee</th>
                        <td>₹<?= number_format($tender['min_amount'],2);?></td>
                    </tr>
                    <tr>
                        <th>Expiration Time</th>
                        <td id="expirationTime">--N/A--</td>
                    </tr>
                </tbody>
            </table>
            <table class="table">
                <thead class="thead-default">
                    <tr>
                        <th>Name</th>
                        <th>Existing bid</th>
                    </tr>
                </thead>
                <tbody id="bidsBody"></tbody>
            </table>
        </div>
    </div>
</div>
<?= $this->Html->script('numberFormat.js'); ?>
<?= $this->Html->script('bidding.js'); ?>
<script>
    Bidding.id = <?= $tender['id'] ?>;
    Bidding.bidsUrl = '<?= $this->Url->build(['controller' => 'EAuction', 'action' => 'getBids']) ?>';
    Bidding.addBidUrl = '<?= $this->Url->build(['controller' => 'EAuction', 'action' => 'addBid']) ?>';
    Bidding.start();
</script>