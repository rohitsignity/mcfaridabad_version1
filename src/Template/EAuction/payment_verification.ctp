<script>
    swal({
        title: 'Earnest Money Deposit',
        text: "To bid for this tender, please deposit ₹<?= $intialAmount ?> + ₹<?= $documentFee?>(Documents Fees) = ₹<?= ($intialAmount + $documentFee)?>/- as Earnest Money Deposit",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Pay Now',
        cancelButtonText: 'Cancel!',
        allowOutsideClick: false
    }).then(function () {
        var docHeight = $(document).height();
        $('#loaderLayout span').css({height: docHeight + 'px'});
        $('#loaderLayout').show();
        $('#payNow').submit();
    }, function (dismiss) {
        if (dismiss === 'cancel') {
            var docHeight = $(document).height();
            $('#loaderLayout span').css({height: docHeight + 'px'});
            $('#loaderLayout').show();
            window.location = '<?= $cancelUrl; ?>';
        }
    });
</script>
<form action="<?php echo $liveUrl; ?>" method="post" id="payNow">
    <input type="hidden" name="PageMode" value="<?php echo $pageMode; ?>" />
    <input type="hidden" name="GatewayString" value="<?php echo $gatewayString;?>">
</form>