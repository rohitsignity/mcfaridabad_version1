<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="message bg-success text-center alert" onclick="this.classList.add('hidden')"><?= $message ?></div>
