<div id="sidebar-wrapper">
    <ul id="sidebar_menu" class="sidebar-nav1">
        <a id="menu-toggle" href="javascript:void(0)" class="navtoggle"><span  class="menu-toggle-cust glyphicon glyphicon-align-justify"></span></a>
    </ul>
    <ul class="sidebar-nav" id="sidebar">
        <li class="<?php echo $this->request->here == $this->Url->build(["controller" => "Dashboard", "action" => "index"]) ? "active" : ""; ?>">
            <a href="<?php echo $this->Url->build(["controller" => "Dashboard", "action" => "index"]); ?>">Dashboard<span class="sub_icon"><img src="<?php echo $this->Url->image('dashboard.png'); ?>"/></span></a>
        </li>
        <li class="<?php echo $this->request->here == $this->Url->build(["controller" => "CompanyDevices", "action" => "register"]) ? "active" : ""; ?>">
            <a href="<?php echo $this->Url->build(["controller" => "CompanyDevices", "action" => "register"]); ?>">Outdoor media device application<span class="sub_icon"><img src="<?php echo $this->Url->image('om.png'); ?>"/></span></a>
        </li>
        <li class="<?php echo $this->request->here == $this->Url->build(["controller" => "CompanyDevices", "action" => "applicationStatus"]) ? "active" : ""; ?>">
            <a href="<?php echo $this->Url->build(["controller" => "CompanyDevices", "action" => "applicationStatus"]); ?>">Applications Status <span class="sub_icon"><img src="<?php echo $this->Url->image('application-status.png'); ?>"/></span></a>
        </li>
        <li class="<?php echo $this->request->here == $this->Url->build(["controller" => "Users", "action" => "profile"]) ? "active" : ""; ?>">
            <a href="<?php echo $this->Url->build(["controller" => "Users", "action" => "profile"]); ?>">Profile <span class="sub_icon"><img src="<?php echo $this->Url->image('profile.png'); ?>"/></span></a>
        </li>
    </ul>
</div>
