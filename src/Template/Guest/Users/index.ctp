<?php $this->assign('title', 'Guest Login');?>
<section class="content-wrap">
    <div class="container">
        <div class="row">
            <!-- Login section -->
            <div class="login-sec">
                <h1>GUEST LOG IN</h1>
                <?php echo $this->Form->create(null,['url' => 'javascript:void(0)']); ?>
                <div class="form-wrap">
                    <?php
                    echo $this->Form->input('user_name', ['type' => 'text', 'class' => 'form-control', 'label' => 'Mobile Number', 'templates' => [
                            'inputContainer' => '<div class="form-group">{{content}}</div>'
                        ], 'placeholder' => 'Enter your Mobile number']);
                    echo $this->Form->input('password', ['type' => 'password', 'class' => 'form-control', 'label' => 'One time Password', 'templates' => [
                            'inputContainer' => '<div class="form-group" style="display: none">{{content}}</div>'
                        ], 'placeholder' => 'Enter OTP']);
                    ?>
                    <p style="color: #782669; font-style: italic;">*Disclaimer- This is a newly launched website by MCF. In case of any error/omission, MCF reserves the right to raise additional claims on the applicant.</p>
                </div>
                <?php echo $this->Form->button('Request OTP', ['class' => 'btn btn-default', 'formnovalidate' => true,'id' => 'optRequest']); ?>
                <?php echo $this->Form->button('Validate OTP', ['class' => 'btn btn-default guestButton', 'formnovalidate' => true,'id' => 'validateOtp', 'style' => 'display: none;']); ?>
                <?php echo $this->Form->button('Resend OTP', ['class' => 'btn btn-default guestButton', 'formnovalidate' => true,'id' => 'resendOtp', 'style' => 'display: none;','disabled' => 'disabled']); ?>
                <?php echo $this->Form->end(); ?>
            </div>

            <!--/ Login section -->
        </div>                
    </div>
</section>
<?php echo $this->Html->script('guest.js');?>
<script>
    Guest.rquestUrl = '<?php echo $this->url->build(['controller' => 'users','action' => 'requestOtp']);?>';
    Guest.validateUrl = '<?php echo $this->url->build(['controller' => 'users','action' => 'validateOtp']);?>';
</script>
