<div id="page-content-wrapper">
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-V-cjIUZu6DPHpERzLBIV5OnR_AxkRtw"></script>
    <!-- Keep all page content within the page-content inset div! -->
    <div class="page-content inset app-status">

        <div class="company-reg devic-reg">
            <div class="headings-all text-center">
                <h1>Outdoor media device application</h1>
            </div>
            <div class="stepsblk clearfix">
                <ul>
                    <li class="step1-blk active">
                        <a href="#">
                            <span>1</span>
                            <h2>Step1</h2>
                            <p>Device Information</p>
                        </a>
                    </li>
                    <li class="step1-blk">
                        <a href="#">
                            <span>2</span>
                            <h2>Step2</h2>
                            <p>Device Information</p>
                        </a>
                    </li>
                    <li class="step1-blk">
                        <a href="#">
                            <span>3</span>
                            <h2>Step3</h2>
                            <p>Preview</p>
                        </a>
                    </li>

                </ul>
            </div>
            <div class="regist-form">
                <h2 class="formh2 orang">Device Information</h2>
                <div class="row">
                    <form method="POST" action="<?php echo $this->Url->build(["controller" => "CompanyDevices", "action" => "saveDevice"]); ?>" enctype="multipart/form-data">
                        <!--                        <div class="col-md-6 form-step1 form-steps">
                                                    <label class="col-md-12">Device Type</label>
                        <?php
//                            echo $this->Form->radio(
//                                    'devive_type', [
//                                ['value' => 0, 'text' => 'Public', 'onchange' => 'Typology.deviceType(this)'],
//                                ['value' => 1, 'text' => 'Private', 'onchange' => 'Typology.deviceType(this)']
//                                    ]
//                            );
                        ?>
                                                </div>-->
                        <!--                        <div class="col-md-6 form-step1 form-steps">
                                                    <label>Device Name*</label>
                                                    <input class="form-control" name="device_name" autocomplete="off" placeholder="Device Name" type="text" id="deviceName">
                                                </div>-->
                        <div class="col-md-6 form-step1 form-steps">
                            <?php
//                            echo $this->Form->input('category_id', ['empty' => 'Select Typology', 'type' => 'select', 'options' => [], 'label' => 'Typology', 'id' => 'typology', 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>']]);
                            echo $this->Form->input('category_id', ['empty' => 'Select Typology', 'type' => 'select', 'options' => $categories, 'label' => 'Typology', 'id' => 'typology', 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>']]);
                            ?>
                        </div>
                        <div class="col-md-6 form-step1 form-steps">
                            <?php echo $this->Form->input('subcategory', ['empty' => 'Select Subcategory', 'type' => 'select', 'options' => [], 'label' => 'Sub Category', 'id' => 'sub_category', 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>']]); ?>
                        </div>
                        <div id="formData"></div>
                    </form>
                </div>
            </div>

        </div>


    </div>
</div>
<script>
    $(document).on('change','select[name="metro_mrts_categories"]',function(){
        MetroMrts.getView($(this));
    });
    $(document).on('change','select[name="shelters_type"]',function(){
        Shelters.getView($(this));
    });
    $(document).on('change','select[name="route_markers_type"]',function(){
        RouteMaker.getView($(this));
    });
    $(document).on('change','select[name="foot_over_type"]',function(){
        FootOverBridges.getView($(this));
    });
    $(document).on('change','select[name="cycle_station_type"]',function(){
        CycleStation.getView($(this));
    });
    $(document).on('change','select[name="police_booth_type"]',function(){
        PoliceBooth.getView($(this));
    });
    $(document).on('change','select[name="sitting_bench_type"]',function(){
        SittingBench.getView($(this));
    });
    $(document).ready(function() {
        $(window).keydown(function(event){
          if(event.keyCode == 13) {
            event.preventDefault();
            return false;
          }
        });
    });
    var MathFunctions = {
        "+": function (x, y) {
            return x + y;
        },
        "-": function (x, y) {
            return x - y;
        },
        "*": function (x, y) {
            return Number(Math.round(x * y+'e2')+'e-2');
        },
        "greater": function (x, y) {
            return x > y;
        },
        zero: function(x, y) {
            return x;
        },
        "***": function(x,y) {
            var result= 1;
            var data = y.split('x');
            $.each(data,function(index,val){
                result = result * parseFloat(val);
            });
            return Number(Math.round(result+'e2')+'e-2');
        }
    };

    $('#typology').on('change', function () {
        var typologyId = $(this).val();
        SubCategory.clearFormPanel('formData');
        Typology.typologyData(typologyId, 'sub_category');

    });

    $('#sub_category').on('change', function () {
		console.log('rohit');
        var subCategoryId = $(this).val();
        $(document).off('focusout');
        SubCategory.subCategoryData(subCategoryId, 'formData');
    });
    $(document).on('click', '#formPreview', function () {
        SubCategory.moveStep2();
    });

    $(document).on('keyup', 'input[name="omd_no_venue"],input[name="omd_no_others"]', function (e) {
		/*
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || (e.keyCode >= 35 && e.keyCode <= 40)) {
            $(document).find('#' + 'omd_size_' + $(e.target).attr('name')).remove();
            SubCategory.clearOMDSizeFieldsValidations($(e.target));
            return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            $(e.target).val('');
            e.preventDefault();
        }
        */
        //============= add by rohit kaushal ==============
        if ($.inArray(e.keyCode, [46, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || (e.keyCode >= 35 && e.keyCode <= 40)) {
            $(document).find('#' + 'omd_size_' + $(e.target).attr('name')).remove();
            SubCategory.clearOMDSizeFieldsValidations($(e.target));
            return;
        }
      
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) && (e.keyCode!=8)) {
            $(e.target).val('');
            e.preventDefault();
        }
        //================================================== 
        if ($(e.target).val() < 1) {
            $(document).find('#' + 'omd_size_' + $(e.target).attr('name')).remove();
            SubCategory.clearOMDSizeFieldsValidations($(e.target));
            $(e.target).val('');
            return false;
        }
        //=============== add by rohit kaushal ======================
        if($(e.target).attr('name') == 'omd_no_others'){
			if ($(e.target).val() > 20) {
				alert("No. of OMD should be upto 20");
				$(document).find('#' + 'omd_size_' + $(e.target).attr('name')).remove();
				SubCategory.clearOMDSizeFieldsValidations($(e.target));
				$(e.target).val('');
				return false;
			}
			
		} else {
			if ($(e.target).val() > 10) {
				alert("No. of OMD should be upto 10");
				$(document).find('#' + 'omd_size_' + $(e.target).attr('name')).remove();
				SubCategory.clearOMDSizeFieldsValidations($(e.target));
				$(e.target).val('');
				return false;
			}
		}
        SubCategory.getOMDSizeFields($(e.target));
    });

    $(document).on('keyup', '.omd-size-meters', function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || (e.keyCode >= 35 && e.keyCode <= 40)) {
            return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            $(e.target).val('');
            e.preventDefault();
        }
        if ($(e.target).val() < 1) {
            $(e.target).val('');
            return false;
        }
        var sizeLimit = 13.5;
        /*
        if ($(e.target).val() > sizeLimit) {
            alert("More than " + sizeLimit + " meters not allowed");
            $(e.target).val('');
            return false;
        }
        */ 
    });
//    $(document).on('input', '#formData input[name="location"]', function () {
//        var element = $(this);
//        $(element).autocomplete({
//            source: function (request, response) {
//                $.ajax({
//                    url: '<?php //echo $this->Url->build(["controller" => "CompanyDevices", "action" => "getLocation"]);  ?>',
//                    type: 'GET',
//                    dataType: 'JSON',
//                    data: {s: request.term},
//                    success: response
//                });
//            },
//            minLength: 2,
//            select: function (event, ui) {
//                setLocation(ui.item.data.location.lat, ui.item.data.location.lng);
//                codeAddress(ui.item.data.location.lat, ui.item.data.location.lng);
//            },
//            delay: 1000
//        });
//    });

    var geocoder;
    var map;
    var markersArray = [];
    var mapOptions = {
        center: new google.maps.LatLng(28.4089, 77.3178),
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var marker;

    function createMarker(latLng) {
        if (!!marker && !!marker.setMap)
            marker.setMap(null);
        marker = new google.maps.Marker({
            map: map,
            position: latLng,
            draggable: true
        });
        google.maps.event.addListener(marker, "dragend", function () {
            setLocation(marker.getPosition().lat(), marker.getPosition().lng());
            codeAddress(marker.getPosition().lat(), marker.getPosition().lng());
        });
    }

    function initialize() {
        geocoder = new google.maps.Geocoder();
        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        codeAddress(28.4089, 77.3178, 1);

        google.maps.event.addListener(map, 'click', function (event) {
            map.panTo(event.latLng);
            map.setCenter(event.latLng);
            createMarker(event.latLng);
            codeAddress(event.latLng.lat(), event.latLng.lng());
            setLocation(event.latLng.lat(), event.latLng.lng());
        });

    }
    function codeAddress(lat, lng, intial) {
        var latlng = {lat: lat, lng: lng};
        geocoder.geocode({'location': latlng}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (!intial) {
                    var locationNames = [];
                    $.each(results[0].address_components, function (index, val) {
                        locationNames.push(val.long_name.toLowerCase());
                    });
                    if ($.inArray('faridabad', locationNames) !== -1) {
                        map.setCenter(results[0].geometry.location);
                        createMarker(latlng);
                        $('#formData input[name="location"]').val(results[0].formatted_address);
                    }
                    if ($.inArray('faridabad', locationNames) !== -1) {
                        map.setCenter(results[0].geometry.location);
                        createMarker(latlng);
                        $('#formData input[name="location"]').val(results[0].formatted_address);
                    } else {
                        alert("Invalid Location.");
                        codeAddress(28.4089, 77.3178, 1);
                        setLocation(28.4089, 77.3178);
                    }
                } else {
                    $('#formData input[name="location"]').val('');
                    map.setCenter(results[0].geometry.location);
                    createMarker(latlng);
                }
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }

    function setLocation(lat, lng) {
        $(document).find('#formData input[name="lat"]').val(lat);
        $(document).find('#formData input[name="lng"]').val(lng);
    }


    $(document).on("change", '.deviceImageUpload', function (e) {
        var self = $(this);
        var fileType = this.files[0].type;
        console.log(fileType);
        var ValidImageTypes = ["image/gif", "image/jpeg", "image/png", "image/jpg", "application/pdf", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/msword", "text/csv"];
        if ($.inArray(fileType, ValidImageTypes) < 0) {
            alert("Invalid Document");
            self.val("");
        }
    });
</script>
<?php echo $this->Html->script('typology.js');?>
<?php echo $this->Html->script('subCategory.js');?>
<?php echo $this->Html->script('metroMrts.js');?>
<?php echo $this->Html->script('shelters.js');?>
<?php echo $this->Html->script('routeMaker.js');?>
<?php echo $this->Html->script('foot_over_tb_urinals.js');?>
<?php echo $this->Html->script('cycle_station.js');?>
<?php echo $this->Html->script('police_booth.js');?>
<?php echo $this->Html->script('sitting_bench.js');?>
<?php echo $this->Html->script('typologyA_others.js');?>
<script>
    Typology.ajaxCatUrl = '<?php echo $this->Url->build(['controller' => 'CompanyDevices', 'action' => 'getCategories']); ?>';
    Typology.ajaxSubCatUrl = '<?php echo $this->Url->build(["controller" => "CompanyDevices", "action" => "getSubCategory"]); ?>';
    Typology.errorRedirectionUrl = '<?php echo $this->Url->build(["controller" => "Home", "action" => "index"]); ?>';
    SubCategory.ajaxUrl = '<?php echo $this->Url->build(["controller" => "CompanyDevices", "action" => "getFields"]); ?>';
    SubCategory.fieldszoneArr	=	JSON.parse('<?php echo $fieldszoneArr; ?>');
    SubCategory.errorRedirectionUrl = '<?php echo $this->Url->build(["controller" => "Home", "action" => "index"]); ?>';
</script>
