<div class="page-content inset app-status">
	<div class="headings-all dash-in">
		<h1>Applications Status</h1>
	</div>
        <?php /*?>
	<!--<div class="regist-form">
		<h2 class="formh2">Typology</h2>
		<div class="row">
			<?php //echo $this->Form->create(); ?>
				<div class="col-md-6">
					<?php// echo $this->Form->input('category_id', ['empty' => 'Select Typology', 'type' => 'select', 'options' => $categories, 'label' => 'Typology', 'id' => 'typology', 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>']]); ?>
				</div>
				<div class="col-md-6">
					<?php// echo $this->Form->input('subcategory', ['empty' => 'Select Subcategory', 'type' => 'select', 'options' => [], 'label' => 'Sub Category', 'id' => 'sub_category', 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>']]); ?>
				</div>
				<h2 class="col-md-12 formh2">Device Information</h2>
					<div class="col-md-4">
						<?php //echo $this->Form->input('device_id', ['type' => 'text', 'class' => 'form-control', 'label' => 'Device Id', 'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>'], 'placeholder' => 'Device Id']); ?>
					</div>
				<div class="clearfix"></div>
				<?php //echo $this->Form->button('Submit', ['class' => 'btn btn-default', 'formnovalidate' => true]) ?>
			<?php// echo $this->Form->end(); ?>
		</div>
	</div>-->
        <?php */?>
	<div class="device-list">
		<h2>Outdoor Media Device List </h2>
		<div class="table-responsive">
			<table class="table">
                <thead class="thead-default">
                    <tr>
                        <th>Device Id</th>
                        <th>Location</th>
                        <th>Status</th>
                        <th>Concession Fees</th>
                        <th>Created at</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($devices)) {
						//pr($devices);
						foreach ($devices as $key => $deviceArray) {
						?>								
                            <tr>
                                <th scope="row"><?php echo $deviceArray['companyDevices_id']; ?></th>
                                <td><?php echo isset($deviceArray['companyDevice_location']) ? $deviceArray['companyDevice_location'] : 'N/A'; ?></td>
                                <!--<td class="sts-pending sts-apprd sts-rejected">Pending</td>-->
                                <td class="sts-<?php echo $deviceArray['pay_status'] == 2 ? 'cancelled' : ($deviceArray['companyDevices_status'] == 'under process' ? 'pending' : $deviceArray['companyDevices_status']); ?>"><?php echo $deviceArray['pay_status'] == 2 ? 'Cancelled' : ucfirst($deviceArray['companyDevices_status']); ?></td>
                                <td><?php echo $deviceArray['fees'] ? $deviceArray['fees'] : 'N/A'; ?></td>
                                <td><?php echo date('d M, Y h:i A',strtotime($deviceArray['created']));?></td>
                                <td>
                                    <a href="<?php echo $this->Url->build(['action'=> 'device',$deviceArray['companyDevices_id']]);?>">View</a>
                                    <?php
                                    if ($deviceArray['companyDevices_status'] == 'resubmit') {
                                        ?>
                                        / <a href="<?php echo $this->Url->build(['action' => 'resubmit', $deviceArray['companyDevices_id']]); ?>">Resubmit</a>
                                        <?php
                                    } else if ($deviceArray['companyDevices_status'] == 'document requested') {
                                        ?>
                                        / <a href="javascript:void(0)" onclick="AdditionalDocuments.uploadDeviceDocuments(<?php echo $deviceArray['companyDevices_id']; ?>)">Upload Documents</a>
                                        <?php
                                    } else if(($deviceArray['companyDevices_status'] == 'approved' && ($deviceArray['sub_category_id'] != 17 && $deviceArray['sub_category_id'] != 24 && ($deviceArray['fees'] != 0 ) )) || $deviceArray['companyDevices_status'] == 'rejected') {
                                        ?>
                                        / <a download="omd_letter.pdf" href="<?php echo $docPath;?>omd_letter_<?php echo $deviceArray['companyDevices_id'];?>_<?php echo $user['id'];?>.pdf">Download LOI</a>
                                        <?php
                                    }  else if(($deviceArray['companyDevices_status'] == 'approved' && ($deviceArray['sub_category_id'] == 17 || $deviceArray['sub_category_id'] == 24 || ($deviceArray['sub_category_id'] == 26 && $deviceArray['fees'] == 0 ) ))) {
                                      ?>  
                                        / <a download="permission_letter.pdf" href="<?php echo $docPathPermission;?>permission_letter_<?php echo $deviceArray['companyDevices_id'];?>_<?php echo $user['id'];?>.pdf">Download Permission Letter</a>
                                        <?php
                                    }
                                    if($deviceArray['pay_status'] == 1 && $deviceArray['payment_id']) {
                                        ?>
                                        / <a download="permission_letter.pdf" href="<?php echo $docPathPermission;?>permission_letter_<?php echo $deviceArray['companyDevices_id'];?>_<?php echo $user['id'];?>.pdf">Download Permission Letter</a>
                                        <?php
                                    }
                                    if($deviceArray['pay_status'] == 1 && !$deviceArray['payment_id'] && ($deviceArray['sub_category_id'] != 17 && $deviceArray['sub_category_id'] != 24 && !$deviceArray['installmentId'] && ($deviceArray['fees'] != 0) )) {
                                        ?>
                                        / <a href="javascript:void(0)" data-fees="<?php echo $deviceArray['fees'];?>" onclick="Payment.payNow(this,'<?php echo $deviceArray['companyDevices_id']; ?>')">Pay Now</a>
                                        <?php
                                    }
                                    if($deviceArray['installmentId'] && !$deviceArray['installmentPayment']) {
                                        ?>
                                        / <a href="javascript:void(0)" data-fees="<?php echo $deviceArray['installmentAmount'];?>" data-start="<?php echo date('d M, Y',strtotime($deviceArray['installmentStartDate']));?>" data-end="<?php echo date('d M, Y',strtotime($deviceArray['installmentEndDate']));?>" onclick="Payment.payInstallment(this,'<?php echo $deviceArray['companyDevices_id']; ?>')">Pay Installment</a>
                                        <?php
                                    }
                                    if($deviceArray['pay_status'] == 1 && $deviceArray['installmentId'] && $deviceArray['installmentPayment']) {
                                        ?>
                                        / <a href="<?php echo $this->Url->build(['action' => 'installments',$deviceArray['companyDevices_id']]);?>">Installments <?php echo $deviceArray['pendingInstallments'] ? '('.$deviceArray['pendingInstallments'].' pending)' : '';?></a>
                                        <?php
                                    }
                                    ?>
                                </td>
                            </tr>							
                            <?php
                        }
                        if ($this->Paginator->numbers()) {
                            ?>
                            <tr>
                                <td colspan="6"><ul class="pagingPages"><?php echo $this->Paginator->prev() . $this->Paginator->numbers() . $this->Paginator->next(); ?></ul></td>
                            </tr>
                            <?php
                        }
                        ?>
                    <?php } else {
                        ?>
                        <tr>
                            <td colspan="6" class="text-center">No results found.</td>
                        </tr>							
                        <?php
                    }
                    ?>
                </tbody>
            </table>
		</div>
	</div>
</div>
<div class="modal fade" id="documentsModal" role="dialog" style="z-index: 2147483647;">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload Additional Documents</h4>
            </div>
            <div class="modal-body" id="documentsList" style="float: left; width: 100%;">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer"><p style="text-align: center;">Copyright © <?php echo date('Y'); ?> Outdoor Media Management System. All Rights Reserved.</p></div>
        </div>
    </div>
</div>
<?php 
    echo $this->Html->script('payment.js');
    echo $this->Html->script('additionalDocuments.js'); 
?>
<script>
    Payment.url = '<?php echo $this->Url->build(['action'=> 'payFinalAmount'],true);?>';
    Payment.installmentUrl = '<?php echo $this->Url->build(['action'=> 'payInstallment'],true);?>';
    AdditionalDocuments.url = '<?php echo $this->Url->build(['action'=> 'requestAdditionalDocs']);?>';
    AdditionalDocuments.validateUrl = '<?php echo $this->Url->build(['action'=> 'validateAdditionalDocs']);?>';
    AdditionalDocuments.additional_documenturl = '<?php echo $this->Url->build(['action'=> 'requestAdditionalDocs']);?>';
    AdditionalDocuments.fileUploadUrl = '<?php echo $this->Url->build(['action'=> 'uploadDoc']);?>';
    AdditionalDocuments.loaderImageUrl = '<?php echo $this->Url->build('/img/image_upload_loader.gif'); ?>';
</script>
<script>
    $('#documentsModal').on('hidden.bs.modal', function () {
             location.reload();
            }); 
	var Typology = {
		checkSubCat: 0,
        typologyData: function (typologyId, selectBoxId) {
            var self = this;
            if (self.typologoLocalData[typologyId]) {
                var responseData = self.typologoLocalData[typologyId];
                self.subCategoryHtml(responseData, selectBoxId);
            } else {
                $.ajax({
                    url: '<?php echo $this->Url->build([ "controller" => "CompanyDevices", "action" => "getSubCategory"]); ?>',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {id: typologyId},
                    success: function (response) {
                        var responseData = [];
                        var subCatId 	= '<?php echo isset($subcategory) ? $subcategory : "" ;?>';
                        
                        $.each(response, function (index, val) {
                            var obj = {};
                            obj.id = val.id;
                            obj.title = val.title;
                            responseData.push(obj);
                        });
                        self.typologoLocalData[typologyId] = responseData;
                        self.subCategoryHtml(responseData, selectBoxId);
                        if( subCatId != '' && self.checkSubCat == 1) {
							$(document).find("#sub_category").val(subCatId);
							self.checkSubCat = 0;
						}
                    }, error: function (error) {
                        //window.location = '<?php echo $this->Url->build([ "controller" => "Home", "action" => "index"]); ?>';
                    }
                });
            }

        },
        typologoLocalData: {},
        subCategoryHtml: function (dataArr, selectBoxId) {
            var html = '<option value="">Select Subcategory</option>';
            $.each(dataArr, function (index, val) {
                html += '<option value="' + val.id + '">' + val.title + '</option>';
            });
            $('#' + selectBoxId).html(html);
        }
    };    
    
    $('#typology').on('change', function () {
        var typologyId = $(this).val();
        Typology.typologyData(typologyId, 'sub_category');
    });
    
	$(document).ready(function(){
		var typologyId 	= $('#typology').val();
		
		if( typologyId != '' ) {
			Typology.checkSubCat = 1;
			Typology.typologyData(typologyId, 'sub_category');		
		} 
	}); 
    
</script>                
