<div class="page-content inset dashboard">
    <div class="device-list">
        <h2>Applied Tenders</h2>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-default">
                    <tr>
                        <th>#</th>
                        <th>Tender Name</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Device Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($tenders)) {
                        $sNo = (($this->Paginator->current() - 1) * $limit) + 1;
                        foreach ($tenders as $tender) {
                            ?>
                            <tr>
                                <td scope="row"><?php echo $sNo; ?></td>
                                <td><?php echo $tender['name']; ?></td>
                                <td><?php echo date('d-m-Y', strtotime($tender['start_date'])); ?></td>
                                <td><?php echo date('d-m-Y', strtotime($tender['end_date'])); ?></td>
                                <td><?php echo $tender['device_id']; ?></td>
                                <td><a href="<?php echo $this->Url->build(['controller' => 'Bids','action' => 'appliedBids',$tender['id']]);?>">View Applied users</a></td>
                            </tr>							
                            <?php
                            $sNo++;
                        }
                        if ($this->Paginator->numbers()) {
                            ?>
                            <tr>
                                <td colspan="7"><ul class="pagingPages"><?php echo $this->Paginator->prev() . $this->Paginator->numbers() . $this->Paginator->next(); ?></ul></td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr><td colspan="6" style="text-align: center;">No Record Found</td></tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>