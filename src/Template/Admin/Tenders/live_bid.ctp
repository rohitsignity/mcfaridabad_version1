<div class="page-content inset dashboard">
    <div class="device-list">
        <h2>Live Bidding <?= $tender['name']; ?></h2>
        <div class="table-responsive">
            <table class="table">
                <tbody>
                    <tr>
                        <th>Latest Bid</th>
                        <td id="latestBid">Start Bidding</td>
                    </tr>
                    <tr>
                        <th>Incremental</th>
                        <td><?= $bidIncrimentPercentage; ?>%</td>
                    </tr>
                    <tr>
                        <th>Tender Value</th>
                        <td>₹<?= number_format($tender['tender_value'], 2); ?></td>
                    </tr>
                    <tr>
                        <th>Minimum Guarantee</th>
                        <td>₹<?= number_format($tender['min_amount'], 2); ?></td>
                    </tr>
                    <tr>
                        <th>Expiration Time</th>
                        <td id="expirationTime">--N/A--</td>
                    </tr>
                </tbody>
            </table>
            <table class="table">
                <thead class="thead-default">
                    <tr>
                        <th>Name</th>
                        <th>Existing bid</th>
                    </tr>
                </thead>
                <tbody id="bidsBody"></tbody>
            </table>
        </div>
    </div>
</div>
<?= $this->Html->script('numberFormat.js'); ?>
<?= $this->Html->script('bidding.js'); ?>
<script>
    Bidding.id = <?= $tender['id'] ?>;
    Bidding.bidsUrl = '<?= $this->Url->build(['controller' => 'Tenders', 'action' => 'getBids']) ?>';
    Bidding.addBidUrl = '';
    Bidding.start();
</script>