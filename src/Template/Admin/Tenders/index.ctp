<div class="page-content inset dashboard">
    <div class="device-list">
        <h2>Tenders<?php echo $this->Html->link('Add Tender', ['action' => 'add'], ['class' => 'btn btn-default tender-add-btn pull-right']); ?></h2>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-default">
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Start date & Time</th>
                        <th>End Date & Time</th>
                        <th>Minimum Guarantee</th>
                        <th>Contact</th>
                        <!--<th>Publication Type</th>-->
                        <th>Status</th>
                        <th>Award Status</th>
                        <th>Approval Status</th>
                        <th>Action</th>
                        <th>Winner Detail</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (count($tenders)) {
                        $sNo = (($this->Paginator->current() - 1) * $limit) + 1;
                        foreach ($tenders as $tender) {
                            $tender->end_date = $tender->end_date_updated;
                            $action = '--N/A--';
                            $winner = '--N/A--';
                            if (!$tender->publish_status) {
                                //$action = '<a href="' . $this->Url->build(['controller' => 'Tenders', 'action' => 'edit', $tender->id]) . '">Edit</a>';
                            }
                            if ($tender->publish_type && !$tender->publish_status && date('Y-m-d', strtotime($tender->start_date)) <= date('Y-m-d') && date('Y-m-d', strtotime($tender->end_date)) >= date('Y-m-d')) {
                                $action = '<a href="' . $this->Url->build(['controller' => 'Tenders', 'action' => 'publishManual', $tender->id]) . '">Publish</a>';
                            } else if ($tender->publish_status && date('Y-m-d H:i:s', strtotime($tender->start_date)) <= date('Y-m-d H:i:s') && date('Y-m-d H:i:s', strtotime($tender->end_date)) >= date('Y-m-d H:i:s')) {
                                $action = '<a href="' . $this->Url->build(['controller' => 'Tenders', 'action' => 'liveBid', $tender->id]) . '">View Live Bidding</a>';
                            } else if ($tender->publish_status && date('Y-m-d H:i:s', strtotime($tender->start_date)) <= date('Y-m-d H:i:s')) {
                                $action = '<a href="' . $this->Url->build(['controller' => 'Tenders', 'action' => 'liveBid', $tender->id]) . '">View History</a>';
                                $winner = '<a href="javascript:void(0)" onclick="Award.showDetail('.$tender->id.')">Award Tender</a>';
                                if ($tender->is_awarded) {
                                    $winner = '<a href="javascript:void(0)" onclick="Award.showDetail('.$tender->id.')">View H1 Bidder</a>';
                                }
                            } else if($tender->status == 1 && !$tender->publish_status) {
                                $action = '<a href="' . $this->Url->build(['controller' => 'Tenders', 'action' => 'publishManual', $tender->id]) . '">Publish</a>';
                                $action .= '/<a href="'.$this->Url->build(['action' => 'view',$tender->id]).'">View</a>';
                            }
                            ?>
                            <tr>
                                <td><?= $sNo; ?></td>
                                <td><?= $tender->name; ?></td>
                                <td><?= date('d M, Y h:iA', strtotime($tender->start_date)); ?></td>
                                <td><?= date('d M, Y h:iA', strtotime($tender->end_date)); ?></td>
                                <td><?= $tender->min_amount; ?></td>
                                <td><?= $tender->contact; ?></td>
                                <!--<td><?= $tender->publish_type ? 'Manual' : 'Automatic'; ?></td>-->
                                <td><?= $tender->publish_status ? 'Published' : 'Not Published'; ?></td>
                                <td><?= $tender->is_awarded ? 'Awarded' : 'Not Awarded'; ?></td>
                                <td><?= $tender->status == 2 ? 'Rejected' : ($tender->status == 1 ? 'Approved' : 'Processing'); ?></td>
                                <td><?= $action; ?></td>
                                <td><?= $winner; ?></td>
                            </tr>
                            <?php
                            $sNo++;
                        }
                        if ($this->Paginator->numbers()) {
                            ?>
                            <tr>
                                <td colspan="10"><ul class="pagingPages"><?php echo $this->Paginator->prev() . $this->Paginator->numbers() . $this->Paginator->next(); ?></ul></td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr><td colspan="10" class="text-center">No Record Found</td></tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?= $this->Html->script('award.js'); ?>
<script>
    Award.authanticateUrl = '<?= $this->Url->build(['controller' => 'Users','action' => 'authanticateAdmin']);?>';
    Award.authanticateSuperAdminUrl = '<?= $this->Url->build(['controller' => 'SuperAdmins','action' => 'authanticateSuperAdmin']);?>';
</script>