<div class="page-content inset dashboard">
    <div class="device-list">
        <?php
        if (!$appliedBid['status']) {
            ?>
            <h2>Process Tender Application</h2>
            <?php
        } else {
            ?>
            <h2>Tender Application Detail (Status: <?= !$appliedBid['status'] ? 'pending' : ($appliedBid['status'] == 1 ? 'Correct' : 'Incorrect');?>)</h2>
            <?php
        }
        ?>
        <hr>
        <h2>User Detail</h2>
        <div class="table-responsive">
            <table class="table">
                <tr>
                    <th>Name</th>
                    <td><?php echo $appliedBid['display_name']; ?></td>
                </tr>
                <tr>
                    <th>User Name</th>
                    <td><?php echo $appliedBid['user_name']; ?></td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td><?php echo $appliedBid['email']; ?></td>
                </tr>
                <tr>
                    <th>Mobile</th>
                    <td><?php echo $appliedBid['mobile']; ?></td>
                </tr>
            </table>
        </div>
        <?php
        if (count($appliedBidDocuments)) {
            ?>
            <h2>Required Document(s)</h2>
            <div class="table-responsive">
                <table class="table">
                    <?php
                    foreach ($appliedBidDocuments as $appliedBidDocument) {
                        $documentId = $appliedBidDocument['document_id'];
                        $documentNameArr = array_values(array_filter($tenderDocuments, function($arr) use($documentId) {
                                    return $arr['id'] == $documentId;
                                }));
                        $ext = pathinfo($appliedBidDocument['document'], PATHINFO_EXTENSION);
                        ?>
                        <tr>
                            <th><?php echo $documentNameArr[0]['name']; ?></th>
                            <td><?php echo $this->Html->link('Download', '/uploads/applied_bids_documents/' . $appliedBidDocument['document'], ['download' => $documentNameArr[0]['name'] . '.' . $ext]); ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
            </div>
            <?php
        }
        ?>
        <h2>Tender Detail(<?php echo $tender['name']; ?>)</h2>
        <div class="table-responsive">
            <table class="table">
                <tbody>
                    <tr>
                        <th>Tender Name</th>
                        <td><?php echo $tender['name']; ?></td>
                    </tr>
                    <?php
                        $devicesArr = array_map(function($arr){
                            return '<a href="javascript:void(0)" onclick="Tender.getDeviceDetail('.$arr['id'].')">'.$arr['device_name'].'</a>';
                        }, $devices);
                    ?>
                    <tr>
                        <th>Devices</th>
                        <td><?php echo implode(', ', $devicesArr);?></td>
                    </tr>
                    <tr>
                        <th>Issuing Authority</th>
                        <td><?php echo $tender['issuing_authority']; ?></td>
                    </tr>
                    <tr>
                        <th>Authorized By</th>
                        <td><?php echo $tender['authorised_by']; ?></td>
                    </tr>
                    <tr>
                        <th>Department Name</th>
                        <td><?php echo $tender['department_name']; ?></td>
                    </tr>
                    <tr>
                        <th>Estimated Tender Value</th>
                        <td><?php echo $tender['tender_value']; ?></td>
                    </tr>
                    <tr>
                        <th>Minimum Guarantee</th>
                        <td><?php echo $tender['min_amount']; ?></td>
                    </tr>
                    <tr>
                        <th>Document Fee</th>
                        <td><?php echo $tender['document_fee']; ?></td>
                    </tr>
                    <tr>
                        <th>Document Download Date</th>
                        <td><?php echo date('d M, Y', strtotime($tender['doc_download_date'])); ?></td>
                    </tr>
                    <tr>
                        <th>Start Date & Time</th>
                        <td><?php echo date('d M, Y h:iA', strtotime($tender['start_date'])); ?></td>
                    </tr>
                    <tr>
                        <th>End Date & Time</th>
                        <td><?php echo date('d M, Y h:iA', strtotime($tender['end_date'])); ?></td>
                    </tr>
                    <tr>
                        <th>Tender application Last date</th>
                        <td><?php echo date('d M, Y', strtotime($tender['application_date'])); ?></td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td><?php echo $tender['email']; ?></td>
                    </tr>
                    <tr>
                        <th>Contact</th>
                        <td><?php echo $tender['contact']; ?></td>
                    </tr>
                    <tr>
                        <th>Web Link</th>
                        <td><?php echo $tender['web_link'] ? $tender['web_link'] : '--N/A--'; ?></td>
                    </tr>
                    <tr>
                        <?php
                        $tenderDocumentsArr = array_map(function($arr) {
                            return $arr['name'];
                        }, $tenderDocuments);
                        ?>
                        <th>Required Documents</th>
                        <td><?php echo count($tenderDocumentsArr) ? implode(', ', $tenderDocumentsArr) : '--N/A--'; ?></td>
                    </tr>
                    <tr>
                        <th>Terms & Conditions</th>
                        <td><?php echo $tender['terms_cond'] ? $tender['terms_cond'] : '--N/A--'; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <?php
    if ($appliedBid['status'] == 0) {
        echo $this->Form->create($post);
        echo $this->Form->button('Correct', ['class' => 'btn btn-default', 'formnovalidate' => true, 'name' => 'status', 'value' => '1']);
        echo $this->Form->button('Incorrect', ['class' => 'btn btn-default', 'style' => 'margin: 0 0 0px 10px', 'formnovalidate' => true, 'name' => 'status', 'value' => '3']);
        echo $this->Form->end();
    }
    ?>

</div>
<?= $this->Html->script('smcUser/tenders.js'); ?>
<script>
    Tender.deviceInfoUrl = '<?php echo $this->Url->build(['action' => 'getDeviceInfo']); ?>';
    Tender.devicePath = '<?php echo $devicePath;?>';
</script>
