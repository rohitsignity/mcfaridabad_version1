<div class="page-content inset dashboard">
    <div class="device-list">
        <h2>Tender Applications (Tender: <?php echo $tender["name"]; ?>)</h2>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-default">
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (count($appliedBids)) {
                        $sNo = (($this->Paginator->current() - 1) * $limit) + 1;
                        foreach ($appliedBids as $appliedBid) {
                            ?>
                            <tr>
                                <td><?= $sNo; ?></td>
                                <td><?= $appliedBid->display_name; ?></td>
                                <td><?= $appliedBid->email;?></td>
                                <td><?= $appliedBid->mobile;?></td>
                                <td><?= !$appliedBid->status ? 'pending' : ($appliedBid->status == 1 ? 'Correct' : 'Incorrect');?></td>
                                <td><a href="<?php echo $this->Url->build(['action' => 'tenderApplication',$appliedBid->id]);?>">View</a>/ <a href="javascript:void(0)" onclick="Tender.vendorDetail(<?php echo $appliedBid->user_id;?>)">Vendor Detail</a></td>
                            </tr>
                            <?php
                            $sNo++;
                        }
                        if ($this->Paginator->numbers()) {
                            ?>
                            <tr>
                                <td colspan="10"><ul class="pagingPages"><?php echo $this->Paginator->prev() . $this->Paginator->numbers() . $this->Paginator->next(); ?></ul></td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr><td colspan="10" class="text-center">No Record Found</td></tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?= $this->Html->script('tenders.js'); ?>
<script>
    Tender.userDetailUrl = '<?php echo $this->Url->build(['action' => 'getUserDetail']);?>';
</script>