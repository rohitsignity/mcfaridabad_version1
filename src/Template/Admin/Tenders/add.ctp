<div id="page-content-wrapper">
    <!-- Keep all page content within the page-content inset div! -->
    <div class="page-content inset app-status">
        <div class="company-reg devic-reg">
            <!--            <div class="headings-all text-center">
                            <h1>New Tender</h1>
                        </div>-->
            <div class="regist-form">
                <h2 class="formh2 orang"><a href="<?php echo $this->Url->build(['controller' => 'tenders', 'action' => 'index']); ?>">Tenders</a> > Add New Tender</h2>
                <div class="row">
                    <?php echo $this->Form->create($tender, ['type' => 'file', 'id' => 'tenderForm']); ?>
                    <div id="tenderBlock">
                        <div class="col-md-6">
                            <?php
                            echo $this->Form->input('name', ['type' => 'text', 'class' => 'form-control', 'autocomplete' => 'off', 'label' => 'Tender Name*', 'templates' => [
                                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                                ], 'placeholder' => 'Tender Name']);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                            echo $this->Form->input('typology_id', ['type' => 'select', 'class' => 'form-control', 'label' => 'Typology*', 'onchange' => 'Tender.getSubCategories(this)', 'templates' => [
                                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                                ], 'options' => $typologies, 'empty' => 'Select Typology']);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                            echo $this->Form->input('subcategory_id', ['type' => 'select', 'class' => 'form-control', 'label' => 'Sub Category*', 'onchange' => 'Tender.getDevices(this)', 'id' => 'subCategories', 'templates' => [
                                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                                ], 'options' => $subCategories, 'empty' => 'Select Sub Category']);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                            echo $this->Form->input('device_ids[]', ['type' => 'select', 'class' => 'form-control searchDevices', 'label' => 'Device*', 'templates' => [
                                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                                ], 'options' => $devices, 'empty' => 'Select Device', 'id' => 'deviceData','multiple']);
                            ?>
                            <a href="<?php echo $this->Url->build(['controller' => 'company_devices', 'action' => 'register']); ?>" id="addNewDevice" style="display: none;">Add New device</a>
                            <a href="javascript:void(0)" id="fetchDeviceDetail" style="<?php echo!$tender->device_id ? 'display: none;' : ''; ?>" onclick="Tender.fetchDevice()">View Device Detail</a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <?php
                            echo $this->Form->input('issuing_authority', ['type' => 'text', 'class' => 'form-control', 'autocomplete' => 'off', 'label' => 'Issuing Authority*', 'templates' => [
                                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                                ], 'placeholder' => 'Issuing Authority']);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                            echo $this->Form->input('authorised_by', ['type' => 'text', 'class' => 'form-control', 'autocomplete' => 'off', 'label' => 'Authorised by*', 'templates' => [
                                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                                ], 'placeholder' => 'Authorised by']);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                            echo $this->Form->input('user_segment', ['type' => 'select', 'class' => 'form-control', 'id' => 'userSegment', 'label' => 'Users Annual Turnover*', 'templates' => [
                                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                                ], 'options' => $userSegments, 'empty' => 'Select Users Annual Turnover']);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                            echo $this->Form->input('experience', ['type' => 'select', 'class' => 'form-control', 'id' => 'userExperience', 'label' => 'Users Years of experience*', 'templates' => [
                                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                                ], 'options' => $experiences, 'empty' => 'Select Users Years of experience']);
                            ?>
                        </div>
                        <div class="col-md-12">
                            <?php
                            echo $this->Form->input('users[]', ['type' => 'select', 'class' => 'form-control tagsData', 'label' => 'Eligible users *', 'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>'], 'placeholder' => 'Users', 'options' => $userOptions, 'multiple']);
                            if ($userError) {
                                ?>
                                <div class="error-message"><?= $userError; ?></div>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <?php
                            echo $this->Form->input('department_name', ['type' => 'text', 'class' => 'form-control', 'autocomplete' => 'off', 'label' => 'Department Name*', 'templates' => [
                                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                                ], 'placeholder' => 'Department Name']);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                            echo $this->Form->input('tender_value', ['type' => 'text', 'class' => 'form-control', 'autocomplete' => 'off', 'label' => 'Estimated Tender Value*', 'templates' => [
                                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                                ], 'placeholder' => 'Estimated Tender Value', 'onblur' => 'Tender.getMinimumGurantee(this)']);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                            echo $this->Form->input('min_amount', ['type' => 'text', 'class' => 'form-control', 'autocomplete' => 'off', 'label' => 'Minimum Guarantee*', 'templates' => [
                                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                                ], 'placeholder' => 'Minimum Guarantee']);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                            echo $this->Form->input('document_fee', ['type' => 'text', 'class' => 'form-control', 'autocomplete' => 'off', 'label' => 'Document Fee*', 'templates' => [
                                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                                ], 'placeholder' => 'Document Fee']);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                            echo $this->Form->input('bid_doc', ['type' => 'file', 'class' => 'form-control', 'autocomplete' => 'off', 'readonly' => 'readonly', 'label' => 'RFP Document*', 'templates' => [
                                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                                ]]);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                            echo $this->Form->input('doc_download_date', ['type' => 'text', 'class' => 'form-control docstartDate', 'autocomplete' => 'off', 'readonly' => 'readonly', 'label' => 'Document Download Date*', 'templates' => [
                                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                                ], 'placeholder' => 'Document Download Date']);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                            echo $this->Form->input('start_date', ['type' => 'text', 'class' => 'form-control startDate', 'autocomplete' => 'off', 'readonly' => 'readonly', 'label' => 'Start Date & Time*', 'templates' => [
                                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                                ], 'placeholder' => 'Start Date & Time']);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                            echo $this->Form->input('end_date', ['type' => 'text', 'class' => 'form-control endDate', 'autocomplete' => 'off', 'readonly' => 'readonly', 'label' => 'End Date & Time*', 'templates' => [
                                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                                ], 'placeholder' => 'End Date & Time']);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                            echo $this->Form->input('email', ['type' => 'text', 'class' => 'form-control', 'autocomplete' => 'off', 'label' => 'Email*', 'templates' => [
                                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                                ], 'placeholder' => 'Email']);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                            echo $this->Form->input('contact', ['type' => 'text', 'class' => 'form-control', 'autocomplete' => 'off', 'label' => 'Contact*', 'templates' => [
                                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                                ], 'placeholder' => 'Contact']);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                            echo $this->Form->input('web_link', ['type' => 'text', 'class' => 'form-control', 'autocomplete' => 'off', 'label' => 'Web Link', 'templates' => [
                                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                                ], 'placeholder' => 'Web Link']);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                            echo $this->Form->input('application_date', ['type' => 'text', 'class' => 'form-control applicationDate', 'autocomplete' => 'off', 'readonly' => 'readonly', 'label' => 'Tender application Last date*', 'templates' => [
                                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                                ], 'placeholder' => 'Tender application Last date']);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
//                        echo $this->Form->input('publish_type', ['type' => 'select', 'class' => 'form-control', 'label' => 'Publish Type*', 'templates' => [
//                                'inputContainer' => '<div class="form-group">{{content}}</div>'
//                            ], 'options' => ['0' => 'Automatic', '1' => 'Manual']]);
//                        echo $this->Form->input('publish_type', ['type' => 'select', 'class' => 'form-control', 'label' => 'Publish Type*', 'templates' => [
//                                'inputContainer' => '<div class="form-group">{{content}}</div>'
//                            ], 'options' => ['0' => 'Automatic']]);
                            echo $this->Form->hidden('publish_type', ['value' => 0]);
                            ?>
                        </div>
                        <div class="col-md-12">
                            <?php
                            echo $this->Form->input('document', [
                                'multiple' => 'checkbox',
                                'options' => $documents,
                                'templates' => [
                                    'inputContainer' => '<div class="form-group requiredDocs">{{content}}</div>',
                                    'checkboxWrapper' => '<div class="pull-left docCheck">{{label}}</div>'
                                ],
                                'label' => 'Required Documents',
                                'hiddenField' => false
                            ]);
                            ?>
                            <div class="clearfix"></div>
                            <a href="javascript:void(0)" onclick="Tender.addDocumentName()">Add Document</a>
                        </div>
                        <div class="col-md-12">
                            <?php
                            echo $this->Form->input('terms_cond', ['type' => 'textarea', 'class' => 'form-control', 'autocomplete' => 'off', 'label' => 'Terms & Conditions', 'templates' => [
                                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                                ], 'placeholder' => 'Terms & Conditions']);
                            ?>
                        </div>
                        <div class="clearfix"></div>
                        <div id="deviceinfo"></div>
                        <div class="clearfix"></div>
                        <?php
                        echo $this->Html->link('Preview', 'javascript:void(0)', array('class' => 'btn btn-default', 'onclick' => 'Tender.preview()'));
                        ?>
                    </div>
                    <div style="display: none;" id="tenderPreviewBlock">
                        <div></div>
                        <?php
                        echo $this->Form->button('Submit', ['class' => 'btn btn-default', 'formnovalidate' => true]);
                        echo $this->Html->link('Edit', 'javascript:void(0)', array('class' => 'btn btn-default', 'onclick' => 'Tender.edit()'));
                        echo $this->Form->end();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var devicePath = '<?php echo $devicePath; ?>';
    var segmentVendorUrl = '<?php echo $this->Url->build(['controller' => 'Tenders', 'action' => 'getSegmentVendors']); ?>';
    var deviceInfoUrl = '<?php echo $this->Url->build(['controller' => 'Tenders', 'action' => 'getDeviceInfo']); ?>';
    //var getDeviceUrl = '<?php //echo $this->Url->build(['controller' => 'Tenders', 'action' => 'getDevice']);                      ?>';
    var getDeviceUrl = '<?php echo $this->Url->build(['controller' => 'Tenders', 'action' => 'getPublicDevicesBySubCategory']); ?>';
    var subCatUrl = '<?php echo $this->Url->build(['controller' => 'CompanyDevices', 'action' => 'getSubCategory']); ?>';
    var addDeviceUrl = '<?php echo $this->Url->build(['controller' => 'CompanyDevices', 'action' => 'register']); ?>';
</script>
<?= $this->Html->script('tenders.js'); ?>
<script type="text/javascript">
    Tender.validateUrl = '<?php echo $this->Url->build(['action' => 'validateTender']); ?>';
    Tender.addDocUrl = '<?php echo $this->Url->build(['controller' => 'Documents','action' => 'addAjax']); ?>';
</script>
<?php
if (!is_null($deviceId)) {
    ?>
    <script type="text/javascript">
        Tender.deviceId = <?php echo $deviceId; ?>;
        //Tender.getDeviceInfo();
    </script>
    <?php
}