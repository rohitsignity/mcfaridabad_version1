<div class="page-content inset dashboard">
    <div class="device-list">
        <h2>Tender Detail(<?php echo $tender['name']; ?>)</h2>
        <div class="table-responsive">
            <table class="table">
                <tbody>
                    <tr>
                        <th>Tender Name</th>
                        <td><?php echo $tender['name']; ?></td>
                    </tr>
                    <?php
                        $devicesArr = array_map(function($arr){
                            return '<a href="javascript:void(0)" onclick="Tender.getDeviceDetail('.$arr['id'].')">'.$arr['device_name'].'</a>';
                        }, $devices);
                    ?>
                    <tr>
                        <th>Devices</th>
                        <td><?php echo implode(', ', $devicesArr);?></td>
                    </tr>
                    <tr>
                        <th>Issuing Authority</th>
                        <td><?php echo $tender['issuing_authority']; ?></td>
                    </tr>
                    <tr>
                        <th>Authorized By</th>
                        <td><?php echo $tender['authorised_by']; ?></td>
                    </tr>
                    <tr>
                        <th>Users Annual Turnover</th>
                        <td><?php echo $annualTurnover['annual_turnover']; ?></td>
                    </tr>
                    <tr>
                        <th>Users Years of experience</th>
                        <td><?php echo $experience['name']; ?></td>
                    </tr>
                    <tr>
                        <th>Eligible users</th>
                        <?php
                        $tenderUsersNames = array_map(function($arr) {
                            return $arr['display_name'] . ' (' . $arr['user_name'] . ')';
                        }, $tenderUsers);
                        ?>
                        <td><?php echo implode(', ', $tenderUsersNames); ?></td>
                    </tr>
                    <tr>
                        <th>Department Name</th>
                        <td><?php echo $tender['department_name']; ?></td>
                    </tr>
                    <tr>
                        <th>Estimated Tender Value</th>
                        <td><?php echo $tender['tender_value']; ?></td>
                    </tr>
                    <tr>
                        <th>Minimum Guarantee</th>
                        <td><?php echo $tender['min_amount']; ?></td>
                    </tr>
                    <tr>
                        <th>Document Fee</th>
                        <td><?php echo $tender['document_fee']; ?></td>
                    </tr>
                    <tr>
                        <?php
                        $ext = pathinfo($tender['bid_doc'], PATHINFO_EXTENSION);
                        ?>
                        <th>RFP Document</th>
                        <td><?php echo $this->Html->link('Download', '/uploads/bid_instructions_documents/' . $tender['bid_doc'], ['download' => 'RFP Document.' . $ext]); ?></td>
                    </tr>
                    <tr>
                        <th>Document Download Date</th>
                        <td><?php echo date('d M, Y', strtotime($tender['doc_download_date'])); ?></td>
                    </tr>
                    <tr>
                        <th>Start Date & Time</th>
                        <td><?php echo date('d M, Y h:iA', strtotime($tender['start_date'])); ?></td>
                    </tr>
                    <tr>
                        <th>End Date & Time</th>
                        <td><?php echo date('d M, Y h:iA', strtotime($tender['end_date'])); ?></td>
                    </tr>
                    <tr>
                        <th>Tender application Last date</th>
                        <td><?php echo date('d M, Y', strtotime($tender['application_date'])); ?></td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td><?php echo $tender['email']; ?></td>
                    </tr>
                    <tr>
                        <th>Contact</th>
                        <td><?php echo $tender['contact']; ?></td>
                    </tr>
                    <tr>
                        <th>Web Link</th>
                        <td><?php echo $tender['web_link'] ? $tender['web_link'] : '--N/A--'; ?></td>
                    </tr>
                    <tr>
                        <?php
                        $tenderDocumentsArr = array_map(function($arr) {
                            return $arr['name'];
                        }, $tenderDocuments);
                        ?>
                        <th>Required Documents</th>
                        <td><?php echo count($tenderDocumentsArr) ? implode(', ', $tenderDocumentsArr) : '--N/A--'; ?></td>
                    </tr>
                    <tr>
                        <th>Terms & Conditions</th>
                        <td><?php echo $tender['terms_cond'] ? $tender['terms_cond'] : '--N/A--'; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <a href="<?php echo $this->Url->build(['controller' => 'Tenders', 'action' => 'publishManual', $tender['id']]);?>" class="btn btn-default">Publish</a>
    </div>
</div>
<?= $this->Html->script('smcUser/tenders.js'); ?>
<script>
    Tender.deviceInfoUrl = '<?php echo $this->Url->build(['action' => 'getDeviceInfo']); ?>';
    Tender.devicePath = '<?php echo $devicePath;?>';
</script>