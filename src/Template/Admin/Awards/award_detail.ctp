<script>
    $('#loaderLayout').show();
</script>
<div class="page-content inset dashboard">
    <div class="device-list">
        <h2>H1 Bidder Detail</h2>
        <div id="winnerBlock"></div>
    </div>
</div>
<?= $this->Html->script('winner.js'); ?>
<script>
    Winner.detailUrl = '<?= $this->Url->build(['controller' => 'Awards', 'action' => 'getAwardDetail']) ?>';
    Winner.getWinner();
</script>