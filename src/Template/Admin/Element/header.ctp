<header class="header">
    <!-- navbar -->
    <nav class="navbar">
        <div class="container">
            <div class="cont-info pull-right">
                <?php /*
                if ($loggedIn) {
                    ?>
                    <a class="login btn" href="<?php echo $this->Url->build([ "controller" => "Users", "action" => "logout"]); ?>">Logout</a>
                    <?php
                } else {
                    ?>
                    <a class="login btn" href="<?php echo $this->Url->build([ "controller" => "Users", "action" => "login"]); ?>">Login</a>
                    <?php
                }*/
                ?>
            </div>
            <div class="navbar-header">
                <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="<?php echo $this->Url->build([ "controller" => "Users", "action" => "index"]); ?>" class="navbar-brand"><img src="<?php echo $this->Url->image('logo.png'); ?>" /></a>
            </div>
        </div>
    </nav>
</header>
