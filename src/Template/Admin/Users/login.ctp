<section class="content-wrap">
    <div class="container">
        <div class="row">
            <!-- Login section -->
            <div class="col-md-12">
                <div class="login-sec">
                    <h1>ADMINISTRATOR LOG IN</h1>
                    <?php echo $this->Form->create($post); ?>
                    <div class="form-wrap">
                        <?php
                        echo $this->Form->input('email', ['type' => 'text', 'class' => 'form-control', 'label' => 'Username', 'templates' => [
                                'inputContainer' => '<div class="form-group">{{content}}</div>'
                            ], 'placeholder' => 'Enter your username']);
                        echo $this->Form->input('password', ['type' => 'password', 'class' => 'form-control', 'label' => 'Password', 'templates' => [
                                'inputContainer' => '<div class="form-group">{{content}}</div>'
                            ], 'placeholder' => 'Password']);
                        ?>
                    </div>
                    <?php
                    echo $this->Form->button('Login', ['class' => 'btn btn-default', 'formnovalidate' => true]);
                    echo $this->Form->end();
                    ?>
                </div>
            </div>
            <!--/ Login section -->

            <!--/ Login section -->
        </div>                
    </div>
</section>