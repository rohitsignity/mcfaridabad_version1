<div id="page-content-wrapper">
    <!-- Keep all page content within the page-content inset div! -->
    <div class="page-content inset app-status">

        <div class="company-reg devic-reg">
            <div class="headings-all text-center">
                <h1>MCF Registration</h1>
            </div>
            <div class="regist-form">
                <h2 class="formh2 orang">MCF Information</h2>
                <div class="row">
                    <?php echo $this->Form->create($post); ?>
                    <div class="col-md-6">
                        <?php
                        echo $this->Form->input('display_name', ['type' => 'text', 'class' => 'form-control', 'label' => 'MCF Name', 'templates' => [
                                'inputContainer' => '<div class="form-group">{{content}}</div>'
                            ], 'placeholder' => 'Enter MCF Name']);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?php
                        echo $this->Form->input('email', ['type' => 'text', 'class' => 'form-control', 'label' => 'Email', 'templates' => [
                                'inputContainer' => '<div class="form-group">{{content}}</div>'
                            ], 'placeholder' => 'Enter MCF Email']);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?php
                        echo $this->Form->input('mobile', ['type' => 'text', 'class' => 'form-control', 'label' => 'Mobile', 'templates' => [
                                'inputContainer' => '<div class="form-group">{{content}}</div>'
                            ], 'placeholder' => 'Enter MCF Mobile']);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?php
                        echo $this->Form->input('type', ['type' => 'select','onchange' => 'Mcg.showZone(this)', 'class' => 'form-control', 'label' => 'MCF Level', 'templates' => [
                                'inputContainer' => '<div class="form-group">{{content}}</div>'
                            ], 'options' => ["MCG1"=> "MCF1","MCG2"=> "MCF2","MCG3"=> "MCF3"], 'empty' => 'Select MCF Level']);
                        ?>
                    </div>
                    <div class="col-md-6" id="zone" <?php echo $post->type != 'MCG1' ? 'style="display: none;"' : '';?>>
                        <?php
                        echo $this->Form->input('zone', ['type' => 'select', 'class' => 'form-control', 'label' => 'MCF Zone', 'templates' => [
                                'inputContainer' => '<div class="form-group">{{content}}</div>'
                            ], 'options' => ["Zone1"=> "Zone1"], 'empty' => 'Select MCF Zone']);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?php
                        echo $this->Form->input('password', ['type' => 'password', 'class' => 'form-control', 'label' => 'Password', 'templates' => [
                                'inputContainer' => '<div class="form-group">{{content}}</div>'
                            ], 'placeholder' => 'Enter Password']);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?php
                        echo $this->Form->input('conf_password', ['type' => 'password', 'class' => 'form-control', 'label' => 'Confirm Password', 'templates' => [
                                'inputContainer' => '<div class="form-group">{{content}}</div>'
                            ], 'placeholder' => 'Confirm Password']);
                        ?>
                    </div>
                    <div class="clearfix"></div>
                    <?php echo $this->Form->button('Submit', ['class' => 'btn btn-default', 'formnovalidate' => true]); ?>
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
echo $this->Html->script('mcg.js');
