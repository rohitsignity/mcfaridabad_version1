<div class="page-content inset dashboard">
    <div class="device-list">
        <h2>Users List</h2>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-default">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($users)) {
                        foreach ($users as $user) {
                            ?>
                            <tr userRow="<?php echo $user['id']; ?>" class="userRow">
                                <th scope="row"><?php echo $user['id']; ?></th>
                                <td><?php echo $user['display_name']; ?></td>
                                <td><?php echo $user['email']; ?></td>
                                <td><?php echo $user['mobile']; ?></td>
                                <td><?php echo $user['type']; ?></td>
                                <td><?php echo $user['status'] ? 'Enable' : 'Pending'; ?></td>
                                <td><a href="javascript:void(0)"><i class="glyphicon glyphicon-menu-hamburger"></i></a></td>
                            </tr>							
                            <?php
//                            echo $this->Form->hidden('user_id', ['value'=>$user['id']]);
//                            echo $this->Form->hidden('income_proof', ['value'=>$user['turnover_certificate']]);
//                            echo $this->Form->hidden('income_proof', ['value'=>$user['exp_certificate']]);
                        }
                        if ($this->Paginator->numbers()) {
                            ?>
                            <tr>
                                <td colspan="6"><ul class="pagingPages"><?php echo $this->Paginator->prev() . $this->Paginator->numbers() . $this->Paginator->next(); ?></ul></td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr><td colspan="6" style="text-align: center;">No Record Found</td></tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row" id="deviceBlock" style="display: none;">
        <?php
        echo $this->Form->hidden('user_id', ['value'=>'']);
        ?>
        <div class="col-md-6">
            <div class="mcgform regist-form" id="companyBlock">
                <h2 class="formh2">User Details</h2>
                <div class="row">
                    <form class="prevwfm"></form>
                </div>
            </div>
        </div>
        <div class="col-md-6" id="entityDetail">
            <div class="mcgform regist-form" id="entityBlock">
                <h2 class="formh2">Entity Details</h2>
                <div class="row">
                    <form class="prevwfm"></form>
                </div>
            </div>
        </div>
    </div>
<!--    <div id="entityCertificates" style="display: none;">
        <a href=""><input type="button" value="Download Income Proof"></a>
        <a href=""><input type="button" value="Download Turnover Certificate"></a>
        <a href=""><input type="button" value="Download Experience Certificate"></a>
    </div>-->
            <div class="siteimg" id="entityCertificates" style="display: none;">
            <h2 class="imgsite formh2">Company Documents</h2>
            <div class="col-md-4" >
                <?php
                echo $this->Html->link(
                        '<i class="glyphicon glyphicon-save"></i>', '', ['class' => 'downloadDocument', 'id' => 'document_first', 'target' => '_blank', 'escape' => false,'download' => 'pan_card.jpg', 'title' => "Download pan card", 'data-original-title' => 'Edit',]
                );
                ?>
                <p style="text-align: center">Pan Card</p>
            </div>
            <div class="col-md-4" >
                <?php
                echo $this->Html->link(
                        '<i class="glyphicon glyphicon-save"></i>', '', ['class' => 'downloadDocument', 'id' => 'document_second', 'target' => '_blank', 'escape' => false,'download' => 'services_tax.jpg', 'title' => "Download Service tax certificate"]
                );
                ?>
                <p style="text-align: center">Service tax certificate</p>
            </div>
            <div class="col-md-4">
                <?php
                echo $this->Html->link(
                        '<i class="glyphicon glyphicon-save"></i>', '', ['class' => 'downloadDocument', 'id' => 'document_third', 'target' => '_blank', 'escape' => false,'download' => 'turnover.jpg', 'title' => "Download Turnover certificate(for preceding 3 financial years)"]
                );
                ?>
                <p style="text-align: center">Turnover certificate(for preceding 3 financial years)</p>
            </div>
        </div>
        <div id="approveUsers" style="display: none;">
            <?php
             echo $this->Form->input('rejection_reason', ['type' => 'textarea', 'class' => 'form-control', 'label' => Null,'templates' => [
                'inputContainer' => '<div class="form-group">{{content}}</div>'
            ], 'placeholder' => 'Please give reason in case of rejection.']);
            echo $this->Form->button('Approve', array(
             'type' => 'button',
             'class' => 'btn btn-default approvd commentSubmit',
              'style'=>'border-radius: 4px; margin-right: 11px;',
             'onclick' => 'approveUsers(1);',
             ));
            echo $this->Form->button('Reject', array(
             'type' => 'button',
             'class' => 'btn btn-default hld commentSubmit',
             'style'=>'border-radius: 4px; margin-right: 11px;',
             'onclick' => 'approveUsers(2);',
             ));
            ?>
            <p id="reason_error"></p>
        </div>
    </div>
<script>
    var Vendor = {
        userId: 0,
        entityData: 0,
        downloadPath: '<?php echo  $this->request->webroot; ?>'+'uploads/register_documents/',
        userDetaiils: JSON.parse('<?php echo $userDetails; ?>'),
        entityDetaiils: JSON.parse('<?php echo $entityDetails; ?>'),
        getUserDetail: function () {
            var self = this;
            $('#entityCertificates').hide();
            var userDetail = self.userDetaiils[self.userId];
            var representativeArr = $.grep(userDetail,function(val){
                return val.label === "Representative_id";
            });
            var representative = representativeArr[0];
            self.entityData = parseInt(representative.value);
            this.getHTML();
        },
        getHTML: function () {
            $("#approveUsers").show();
            $("input[name='user_id']").val(this.userId);
            this.userDetailHTML();
            this.entityHTML();
            this.filesHTML();
        },
        userDetailHTML: function () {
            var html = '';
            var self = this;
            var userDetail = $.grep(self.userDetaiils[self.userId],function(val){
                return val.type === 'field';
            });
            var extRegex = /(?:\.([^.]+))?$/;
            $.each(userDetail, function (index, val) {
                var fileExt = extRegex.exec(val.value);
                if (val.label == 'income_proof')
                {
                    if (val.value != null)
                    {
                        $('#entityCertificates').show();
                        $('#document_first').attr('href', self.downloadPath + (val.value));
                        $('#document_first').attr('download', 'pan_card.' + fileExt[1]);
                    }
                } else if (val.label == 'turnover_certificate')
                {
                    if (val.value != null)
                    {
                        $('#entityCertificates').show();
                        $('#document_second').attr('href', self.downloadPath + (val.value));
                        $('#document_second').attr('download', 'services_tax.' + fileExt[1]);
                        $('#document_second').parent('div.col-md-4').show();
                    }
                    if(self.entityData === 1) {
                        $('#document_second').parent('div.col-md-4').hide();
                    }
                } else if (val.label == 'exp_certificate')
                {
                    if (val.value != null)
                    {
                        $('#entityCertificates').show();
                        $('#document_third').attr('href', self.downloadPath + (val.value));
                        $('#document_third').attr('download', 'turnover.' + fileExt[1]);
                        $('#document_third').parent('div.col-md-4').show();
                    }
                    if(self.entityData === 1) {
                        $('#document_third').parent('div.col-md-4').hide();
                    }
                } else if(val.label !== "Representative_id") {
                    html += '<div class="form-group clearfix">';
                    html += '<label class="col-md-6" for="">' + val.label + '</label>';
                    html += '<div class="col-md-6">';
                    html += '<span>' + val.value + '</span>';
                    html += '</div>';
                    html += '</div>';
                }
            });
            $('#companyBlock form').html(html);
        },
        entityHTML: function () {
            var self = this;
            var html = '';
            var entityDetaiils = this.entityDetaiils[this.userId];
            var excluded = ['SMC Party Code'];
            if(self.entityData === 1) {
                excluded = ['Company Name','Director Name','Annual Turnover','Date of Incorporation','SMC Party Code'];
            }
            $.each(entityDetaiils, function (index, val) {
                if($.inArray(val.label,excluded) == -1) {
                    var label = (val.label === 'Company Address' ? 'Address' : val.label);
                    html += '<div class="form-group clearfix">';
                    html += '<label class="col-md-6" for="">' + label + '</label>';
                    html += '<div class="col-md-6">';
                    html += '<span>' + val.value + '</span>';
                    html += '</div>';
                    html += '</div>';
                }
            });
            $('#entityBlock form').html(html);
        },
        filesHTML: function() {
            var self = this;
            var html = '<h2 class="imgsite formh2">Documents</h2>';
            var files = $.grep(self.userDetaiils[self.userId],function(val){
                return val.type === 'file';
            });
            var extRegex = /(?:\.([^.]+))?$/;
            $.each(files,function(index,file){
                var fileExt = extRegex.exec(file.value);
                html += '<div class="col-md-4">';
                html += '<a href="'+file.value+'" class="downloadDocument" download="'+file.label+'.'+fileExt[1]+'" title="Download pan card"><i class="glyphicon glyphicon-save"></i></a>';
                html += '<p style="text-align: center">'+file.label+'</p>';
                html += '</div>';
            });
            $('#entityCertificates').html(html);
            $('#entityCertificates').show();
        }
    };


    $('.userRow').on('click', function () {
        $('.userRow').removeClass('active-device');
        $(this).addClass('active-device');
        Vendor.userId = $(this).attr('userRow');
        Vendor.getUserDetail();
        $('#deviceBlock').slideDown();
    });
    
    function approveUsers(e)
    {  
        var docHeight = $(document).height();
        $('#loaderLayout span').css({height: docHeight + 'px'});
        
        var changeStatus = 0;
        var reason = '';
        if(e == 2)
        {
            reason = $("textarea#rejection-reason").val();
            if(reason == '')
            {
                $("#reason_error").css("color","red");
                $("#reason_error").html('Reason of rejection is required');
                return false;
            }
            else
            {
                $('#loaderLayout').show();
                changeStatus = 1;
            }
        }
        else if(e == 1)
        {
            $('#loaderLayout').show();
            changeStatus = 1;
        }
        
        if(changeStatus == 1)
        {
            var user_id = $('input[name="user_id"]').val();
            $.ajax({
                url: '<?php echo $this->Url->build(["controller" => "Users", "action" => "approveUsers"]); ?>',
                data: {user_id: user_id, value: e, rejection_reason: reason},
                dataType: 'JSON',
                type: 'POST',
                //xhrFields: { responseType: "json" },
                success: function (response) {
                    $('#loaderLayout').hide();
                    swal("Success", response.data,"success");
                    location.reload();
                },
                error: function (error) {
                    $('#loaderLayout').hide();
                    swal("Error!","Something went wrong on the Server. Please try again.","error");
                }
            });
        }
    }
</script>