<div class="page-content inset dashboard">
    <div class="device-list">
        <h2>Users List</h2>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-default">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($users)) {
                        foreach ($users as $user) {
							$new_type = str_replace("G","F",$user['type']);
                            ?>
                            <tr>
                                <th scope="row"><?php echo $user['id']; ?></th>
                                <td><?php echo $user['display_name']; ?></td>
                                <td><?php echo $user['email']; ?></td>
                                <td><?php echo $new_type.' '.($user['type'] == 'MCG11' ? '('.$user['zone'].')' : ''); ?></td>
                                <td><?php echo $user['status'] ? 'Enable' : 'Disable'; ?></td>
                                <td><a href="<?php echo $this->Url->build([ "controller" => "Users", "action" => "edit"]) . '/' . $user['id']; ?>"><i class="glyphicon glyphicon-edit"></i></a></td>
                            </tr>							
                            <?php
                        }
                        if ($this->Paginator->numbers()) {
                            ?>
                            <tr>
                                <td colspan="6"><ul class="pagingPages"><?php echo $this->Paginator->prev() . $this->Paginator->numbers() . $this->Paginator->next(); ?></ul></td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr><td colspan="6" style="text-align: center;">No Record Found</td></tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
