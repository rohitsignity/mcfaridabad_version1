<div id="page-content-wrapper">
    <!-- Keep all page content within the page-content inset div! -->
    <div class="page-content inset app-status">

        <div class="company-reg devic-reg">
            <div class="headings-all text-center">
                <h1 style="width: 100%; float: left;">Add New Document<p class="acceptedFormats">Accepted formats .png, .gif, .jpeg, .jpg, .doc, .docx, .xls, .xlsx, .pdf</p></h1>
            </div>
            <div class="regist-form">
                <div class="row">
                    <?php echo $this->Form->create($document); ?>
                    <div class="col-md-12">
                        <?php
                        echo $this->Form->input('name', ['type' => 'text', 'class' => 'form-control', 'label' => 'Document Name*', 'templates' => [
                                'inputContainer' => '<div class="form-group">{{content}}</div>'
                            ], 'placeholder' => 'Document Name']);
                        ?>
                    </div>
                    <?php echo $this->Form->button('Submit', ['class' => 'btn btn-default', 'formnovalidate' => true]); ?>
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>