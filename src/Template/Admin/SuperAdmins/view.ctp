<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Super Admin'), ['action' => 'edit', $superAdmin->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Super Admin'), ['action' => 'delete', $superAdmin->id], ['confirm' => __('Are you sure you want to delete # {0}?', $superAdmin->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Super Admins'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Super Admin'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="superAdmins view large-9 medium-8 columns content">
    <h3><?= h($superAdmin->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($superAdmin->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($superAdmin->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($superAdmin->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($superAdmin->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Level') ?></th>
            <td><?= $this->Number->format($superAdmin->level) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($superAdmin->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($superAdmin->modified) ?></td>
        </tr>
    </table>
</div>
