<section class="banner">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators
          <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
          </ol> -->
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="img/bannerbg1.jpg">
               <div class="carousel-caption">
                    <h1>Welcome to
                        <span>Outdoor Media Management Portal</span>
                        </h1>
                      <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam imperdiet ullamcorper faucibus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                  <a href="<?php echo $this->Url->build([ "controller" => "Users", "action" => "login"]); ?>" class="get-std btn btn-default">Get Started<span class="right-arw"><img src="img/arrow-right.png"/> </span></a>-->
                </div>
            </div>
        </div>
        <!--/ Wrapper for slides -->	
    </div>
</section>
<!-- News & updates -->
<!--<section class="news-strip">
    <div class="news clearfix">
        <h2>News & Updates</h2>
        <marquee direction="left">
            <ul class="upadtes">
                <li>Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. </li>
                <li>Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. </li>
                <li>Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. </li>
                <li>Duis sed odio sit amet nibh vulputate </li>

            </ul>
        </marquee>
    </div>
</section>-->
<!--/ News & updates -->
<!-- Content section -->
<section class="content-wrap">
    <div class="container">
        <!-- About-sectiion section -->
        <div class="about-outdoor">
        <div class="container"> 
         <div class="row">
        <h2>Outdoor Media Management System</h2> 
        <p>Municipal Corporation, Gurugram in its endeavor to adopt a fair and transparent process for its users has started an online system to grant permissions, approvals and monitoring of outdoor advertisements. It shall prepare a GIS based Outdoor Media Master Plan for the areas that fall under the jurisdiction of Municipal Corporation, Gurugram. All the existing and proposed Outdoor Media Devices with their GPS coordinates shall be marked on the GIS map of Gurugram and the same shall be available on its website www.mcg.gov.in/ </p>
        <p>As per the Digital India initiative of Government of India to transform India into a digitally empowered society and knowledge economy, Municipal Corporation, Gurugram has developed an e-auction portal that allows prospective bidders to bid online on real time basis in order to win a contract being let by Municipal Corporation, Gurugram. Bidders compete with each other by increasing their bids with the objective of winning the auction Bidding can only be possible for Outdoor Media Devices situated on MCG land & Properties. The whole process is very transparent as it provides a fair & competitive bidding environment to users/ bidders, also it is a quick and easy process, the turnaround time for awarding a tender can be reduced exponentially.</p>
             
        </div>
        </div>
        </div>

        <!--/ section -->
        <!-- Right section -->
     <!-- <div class="online-services">
            <section class="about-sec">
                <h2>ONLINE SERVICES</h2>
                <h3>Most Efficient E-Governance Suite that drives Outdoor Media Licensing</h3>
                
            </section>
            <section class="steps-apply">
               <!-- <div class="steps-head">
                    <h3>Steps to Apply</h3>
                </div>-->
                <!--<div class="row">
                    <div class="col-sm-4 step1">
                        <div class="thumbnail">
                            <span class="round-out"><div class="thumb-img"><img src="img/application-ad1.png" class="image-circle" alt=""></div></span>
                            <div class="caption">
                                <h3>eApplication</h3>
                                <p>Private Media Licensing: MCG online web suite enables private media owners to acquire media licenses in fast & convenient way.</p>
                                <p><a href="<?php echo $this->Url->build(['controller' => 'Users','action' => 'login']);?>" class="btn btn-empty" role="button">Login | Register</a> </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 step1">
                        <div class="thumbnail">
                            <span class="round-out"><div class="thumb-img"><img src="img/adroad2.png" class="image-circle" alt=""></div></span>
                            <div class="caption">
                                <h3>eAuction</h3>
                                <p>Online Bidding for Govt. Owned Sites: MCG eAuctioning Suite enabling bidders' to participate in real time bidding.</p>
                              
                                <p><a href="<?php echo $this->Url->build(['controller' => 'Users','action' => 'login']);?>" class="btn btn-empty" role="button">Login | Register</a> </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 step1">
                        <div class="thumbnail">
                            <span class="round-out"><div class="thumb-img"><img src="img/application-ad2.png" class="image-circle" alt=""></div></span>
                            <div class="caption">
                                <h3>eApproval</h3>
                                <p>Fast Approvals: Transit media and Event based Banner rights license made easy with fast and single click online approvals.</p>
                                <p><a href="<?php echo $this->Url->build(['controller' => 'Users','action' => 'login']);?>" class="btn btn-empty" role="button">Login | Register</a> </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
           
        </div>-->
        <!--/ Right section -->
    </div>
<!--    <section class="resources">
               <div class="container">
                <div class="about-sec title-wrapper">
                <h2>RESOURCES</h2>
                <h3>e-Auctioning Process Flow</h3>
                </div>
                <div class="flowchart">
                <img src="img/Flowchart.png"/>   
                </div>  
                   
                   </div>
            </section>-->
    <section class="msg-from">
        <div class="container">
        <div class="about-sec msg-box">
            <img src="img/message-chat.png"/><h2>Message from the <b>Municipal Commissioner</b></h2>    
        </div>
            <div class="media">
            <div class="media-left">
            <img src="img/user-icon.png"/>
            </div>
            <div class="media-body">
            <!--<h4 class="media-heading">John Doe</h4>-->
                <p style="height:100px;">&nbsp; </p>
                <span><b>Hon'ble Municipal Commissioner</b></span>
                </div>
            </div>
        </div>
    
    </section>
    <div class="left-bar">
        <div class="container">
                <div class="befor-apply">
                    <div class="headh1">
                        <h1>Important links </h1>
                    </div>
                    <div class="cont-apply">
                        <ul>
                            <li><img src="img/pdf.png"/><a href="javascript:void(0)">Outdoor Media Byelaws</a></li>
                            <li><img src="img/pdf.png"/><a href="javascript:void(0)">License fee of OMD</a></li>
                            <li><img src="img/pdf.png"/><a href="javascript:void(0)">Typology</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            </div>
</section>
