<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="login-panel panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Reset Password</h3>
            </div>
            <div class="panel-body">
				<?php if ($valid) { ?>
					<?php echo $this->Form->create($userEntity); ?>
					<fieldset>
						<?php
							echo $this->Form->input('password', ['type' => 'password', 'class' => 'form-control', 'label' => 'New Password', 'templates' => [
								'inputContainer' => '<div class="form-group">{{content}}</div>'
							], 'placeholder' => 'Enter New Password']);
							
							echo $this->Form->input('confirm_password', ['type' => 'password', 'class' => 'form-control', 'label' => 'Confirm Password', 'templates' => [
								'inputContainer' => '<div class="form-group">{{content}}</div>'
							], 'placeholder' => 'Enter Confirm Password']);
						?>
					</fieldset>
					<div class="submit">
						<?php echo $this->Form->button('Submit', ['class' => 'btn btn-default', 'formnovalidate' => true]) ?>
					</div>
					<?php echo $this->Form->end(); ?> 
				<?php } else { ?>
					<h2>Link is broken or invalid url.</h2>
				<?php } ?>
                
            </div>
        </div>
    </div>
</div>
