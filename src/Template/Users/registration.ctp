<section class="content-wrap">
    <div class="container">
        <div class="row">
            <!--/ Login section -->
            <!-- Register section -->
            <div class="">
                <div class="login-sec regist clearfix">
                    <h1>REGISTER</h1>
                    <?php
                    echo $this->Form->create('Register', ['id' => 'register', 'novalidate' => true, 'enctype' => 'multipart/form-data']);
                    echo $this->Form->input('representative_id', ['empty' => 'Select Type of entity', 'type' => 'select', 'options' => $representatives, 'label' => 'Type of entity*', 'class' => 'form-control', 'templates' => [
                            'inputContainer' => '<div class="form-group col-md-6">{{content}}</div>'
                    ]]);
                    echo $this->Form->input('display_name', ['type' => 'text', 'class' => 'form-control', 'label' => 'Company Name*', 'templates' => [
                            'inputContainer' => '<div class="form-group col-md-6">{{content}}</div>'
                        ], 'placeholder' => 'Company Name']);
                    ?>
                    <div id="entityInputs" style="display: none;">
                        <div id="directors">
                            <?php
                            echo $this->Form->input('director_name', ['type' => 'text', 'name' => 'director_name', 'class' => 'form-control', 'label' => 'Director Name*', 'templates' => [
                                    'inputContainer' => '<div class="form-group entityInput entityInput5 col-md-6">{{content}}</div>'
                                ], 'placeholder' => 'Director Name']);
                            echo $this->Form->input('din', ['type' => 'text', 'name' => 'din', 'class' => 'form-control', 'label' => 'Din Number*', 'templates' => [
                                    'inputContainer' => '<div class="form-group entityInput entityInput5 col-md-6">{{content}}</div>'
                                ], 'placeholder' => 'Din Number']);
                            ?>
                            <a class="entityInput entityInput5" href="javascript:void(0)" onclick="Registration.addMoreDirectors(this)" style="margin: 0 0 0 17px;">Add More Directors</a>
                        </div>
                        <?php
                        echo $this->Form->input('company_address', ['type' => 'textarea', 'class' => 'form-control', 'label' => 'Permanent/ Registered address*', 'templates' => [
                                'inputContainer' => '<div class="form-group col-md-6">{{content}}</div>'
                            ], 'placeholder' => 'Permanent/ Registered address']);
                        echo $this->Form->input('correspondence_address', ['type' => 'textarea', 'class' => 'form-control', 'label' => 'Correspondence address*', 'templates' => [
                                'inputContainer' => '<div class="form-group col-md-6">{{content}}</div>'
                            ], 'placeholder' => 'Correspondence address']);
                        echo $this->Form->input('incorporation_date', ['type' => 'text', 'class' => 'form-control incorporationDate', 'autocomplete' => 'off', 'readonly' => 'readonly', 'label' => 'Date of Incorporation*', 'templates' => [
                                'inputContainer' => '<div class="form-group col-md-6">{{content}}</div>'
                            ], 'placeholder' => 'Date of Incorporation']);
                        echo $this->Form->input('annual_turnover', [
                            'type' => 'radio',
                            'options' => $turnovers,
                            'templates' => [
                                'inputContainer' => '<div class="form-group col-md-6">{{content}}</div>',
                                'radioWrapper' => '<div class="radio">{{label}}</div>'
                            ],
                            'label' => 'Annual Turnover(Rs)*'
                        ]);
                        echo $this->Form->input('experience', [
                            'type' => 'radio',
                            'options' => $experiences,
                            'templates' => [
                                'inputContainer' => '<div class="form-group col-md-6">{{content}}</div>',
                                'radioWrapper' => '<div class="radio">{{label}}</div>'
                            ],
                            'label' => 'Years of experience*'
                        ]);
                        echo $this->Form->input('address_type', ['empty' => 'Select Type of entity', 'type' => 'select', 'options' => $addressProfes, 'label' => 'Type of address proof*', 'class' => 'form-control', 'templates' => [
                                'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput1 entityInput2 entityInput3 entityInput4 entityInput5 entityInput6">{{content}}</div>'
                        ]]);
                        echo $this->Form->input('address_proof', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Address Proof*', 'templates' => [
                                'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput1 entityInput2 entityInput3 entityInput4 entityInput5 entityInput6">{{content}}</div>'
                        ]]);
                        /* echo $this->Form->input('income_proof', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'PAN Card Copy*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput1 entityInput2 entityInput3">{{content}}</div>'
                          ]]); */
                        ?>
                        <div id="partnersBlock">
                            <?php
//                            echo $this->Form->input('partner_name', ['type' => 'text', 'class' => 'form-control', 'label' => 'Partner Name', 'templates' => [
//                                    'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput3">{{content}}</div>'
//                                ], 'placeholder' => 'Partner Name']);
//                            echo $this->Form->input('adhaar_card', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Adhaar card', 'templates' => [
//                                    'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput1 entityInput2 entityInput3 entityInput4 entityInput5 entityInput6">{{content}}</div>'
//                            ]]);
//                            echo $this->Form->input('ration_card', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Ration Card', 'templates' => [
//                                    'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput1 entityInput2 entityInput3 entityInput4 entityInput5 entityInput6">{{content}}</div>'
//                            ]]);
//                            echo $this->Form->input('driving_license', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Driving License', 'templates' => [
//                                    'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput1 entityInput2 entityInput3 entityInput4 entityInput5 entityInput6">{{content}}</div>'
//                            ]]);
//                            echo $this->Form->input('passport', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Passport', 'templates' => [
//                                    'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput1 entityInput2 entityInput3 entityInput4 entityInput5 entityInput6">{{content}}</div>'
//                            ]]);
                            ?>
                        </div>
                        <!--<a href="javascript:void(0)" class="entityInput entityInput3" onclick="Registration.addMorePartner()" style="display: none;">Add more Partners</a>-->
                        <div id="morePartners">

                        </div>
                        <?php
                        /*
                          echo $this->Form->input('tin_no', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'TIN no*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput2 entityInput3 entityInput4 entityInput5 entityInput6">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('service_tax', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Service tax no*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput2 entityInput3 entityInput4 entityInput5 entityInput6">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('income_tax_return', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Income tax return for the last year*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput1 entityInput2 entityInput3 entityInput4 entityInput5 entityInput6">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('partnership_deed', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Partnership Deed*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput3">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('address_proof_partnership_firm', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Address Proof of the Partnership Firm*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput3">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('partnership_registration_certificate', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Partnership Registration Certificate (if Registered Partnership)', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput3">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('pan_card_partners', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'PAN Card of the Partners*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput4">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('pan_card_llp', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'PAN card of LLP*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput4">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('din_number', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'DIN numbers*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput4 entityInput5">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('registration_certificate_llp', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Registration certificate of LLP*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput4">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('address_proof_llp', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Address proof of LLP*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput4">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('address_proof_partners', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Address Proof of the Partners*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput4">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('audited_balance_sheet', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Audited Balance sheet of last three years(if applicable)', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput4 entityInput5 entityInput6">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('power_of_attorney', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Authorization/power of attorney for authorised signatory/person*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput4 entityInput5 entityInput6">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('association_memorandum', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Memorandum of Association*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput4 entityInput5 entityInput6">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('undertaking_non_blacklisting', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Undertaking of non-Blacklisting*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput1 entityInput2 entityInput3 entityInput4 entityInput5 entityInput6">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('no_dues_pending', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Undertaking of No dues pending with any municipality of Haryana*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput1 entityInput2 entityInput3 entityInput4 entityInput5 entityInput6">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('advertising_experience', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Experience in the field of advertising in last three years*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput1 entityInput2 entityInput3 entityInput4 entityInput5 entityInput6">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('details_advertisement_permissions', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Details of advertisement rights/permissions secured in last 5 years in any of the municipality of Haryana*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput1 entityInput2 entityInput3 entityInput4 entityInput5 entityInput6">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('partner_cv', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Curriculum Vitae*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput1 entityInput2 entityInput3 entityInput4 entityInput5 entityInput6">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('directors_pan_card', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'PAN Card of the directors*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput5">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('company_pan_card', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'PAN card of Company*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput5">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('company_registration_certificate', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Registration certificate of company*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput5">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('company_address_proof', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Address proof of company*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput5">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('directors_address_proof', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Address Proof of the Directors*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput5">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('association_articles', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Articles of Association*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput5 entityInput6">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('director_cv', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Curriculum Vitae of each Director*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput5">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('organization_pan_card', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'PAN Card of the Organization*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput6">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('organization_registration_certificate', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Registration certificate of Organization*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput6">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('organisation_address_proof', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Address proof of Organisation*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput entityInput6">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('turnover_certificate', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Copy of CA certified Turnover certificate for preceding 3 financial years*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('exp_certificate', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Experience/ Work Order*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('balance_sheet', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Balance sheet for last 3 years*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('letter_by_board_directors', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Authorization letter by board of directors*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput">{{content}}</div>'
                          ]]);
                          echo $this->Form->input('incorporation_certificate', ['class' => 'form-control register-doc', 'type' => 'file', 'label' => 'Incorporation Certificate*', 'templates' => [
                          'inputContainer' => '<div class="form-group col-md-6 entityInput">{{content}}</div>'
                          ]]); */
                        ?>
                    </div>
                    <?php
                    echo $this->Form->input('email', ['type' => 'text', 'class' => 'form-control', 'label' => 'Email*', 'templates' => [
                            'inputContainer' => '<div class="form-group col-md-6">{{content}}</div>'
                        ], 'placeholder' => 'Email']);
                    echo $this->Form->input('mobile', ['type' => 'text', 'class' => 'form-control', 'label' => 'Mobile*', 'templates' => [
                            'inputContainer' => '<div class="form-group col-md-6">{{content}}</div>'
                        ], 'placeholder' => 'Mobile']);
                    echo $this->Form->input('password', ['type' => 'password', 'class' => 'form-control', 'label' => 'Password*', 'templates' => [
                            'inputContainer' => '<div class="form-group col-md-6">{{content}}</div>'
                        ], 'placeholder' => 'Password']);
                    echo $this->Form->input('conf_password', ['type' => 'password', 'class' => 'form-control', 'label' => 'Confirm Password*', 'templates' => [
                            'inputContainer' => '<div class="form-group col-md-6">{{content}}</div>'
                        ], 'placeholder' => 'Confirm Password']);
                    echo $this->Form->input('security_qus_id', ['empty' => 'Select Security Question', 'type' => 'select', 'options' => $securityQuestions, 'label' => 'Security Questions*', 'class' => 'form-control', 'templates' => [
                            'inputContainer' => '<div class="form-group col-md-12">{{content}}</div>'
                    ]]);
                    echo $this->Form->input('answer', ['type' => 'textarea', 'class' => 'form-control', 'label' => false, 'templates' => [
                            'inputContainer' => '<div class="form-group col-md-12">{{content}}</div>'
                        ], 'placeholder' => 'Your Answer', 'label' => 'Your Answer*']);
                    echo $this->Form->input('authorised_contact_person', ['class' => 'form-control', 'type' => 'text', 'label' => 'Authorised Contact Person*', 'templates' => [
                            'inputContainer' => '<div class="form-group col-md-12 entityInput entityInput4 entityInput5 entityInput6" style="display: none;">{{content}}</div>'
                    ]]);
                    ?>
                    <div class="checkbox col-md-12">
                        <label>
                            <?php echo $this->Form->input('terms', ['type' => 'checkbox', 'hiddenField' => false, 'label' => false, 'templates' => ['inputContainer' => '{{content}}']]); ?>
                            <p class="termsc">I/we shall adhere to the <b class="orang">terms & conditions</b> of outdoor media byelaws/policy framed by the MCF/Govt. of Haryana. I/we confirm that the above information is true to the best of my/ our knowledge. In case there is any discrepancy in the information provided above, the application shall be rejected</p>
                        </label>
                        <p style="color: #782669; font-style: italic;">*Disclaimer- This is a newly launched website by MCF. In case of any error/omission, MCF reserves the right to raise additional claims on the applicant.</p>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-default">Register</button>
                    </div>
                    </form>
                </div>
            </div>
            <!--/ Login section -->
        </div>                
    </div>
</section>
<!-- Modal -->
<div class="modal fade" id="documentsModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Upload Documents</h4>
            </div>
            <div class="modal-body" id="documentsList">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer"><p style="text-align: center; float: left; width: 100%;">Copyright © <?php echo date('Y'); ?> Outdoor Media Management System. All Rights Reserved.</p></div>
        </div>

    </div>
</div>
<script>
    $('#register').on('submit', function (e) {
        $(document).find('.error-message').remove();
        e.preventDefault();
        Registration.fileUploadUrl = '<?php echo $this->Url->build(['action' => 'uploadDoc']); ?>';
        Registration.loaderImageUrl = '<?php echo $this->Url->build('/img/image_upload_loader.gif'); ?>';
        Registration.documentsUrl = '<?php echo $this->Url->build(['action' => 'getRegistrationDocs']); ?>';
        Registration.redirectUrl = '<?php echo $this->Url->build(['controller' => 'Home', 'action' => 'index']);?>';
        Registration.notApplicableUrl = '<?php echo $this->Url->build(['action' => 'markNotApplicable']); ?>';
        var serializeData = $(this).serializeArray();
        var data = new FormData();
        $.each(serializeData, function (index, val) {
            data.append(val.name, val.value);
        });
        $('#register .register-doc').each(function (i) {
            var name = $(this).attr('name');
            data.append(name, '');
            $.each($(this)[0].files, function (n, file) {
                data.append(name, file);
            });
        });
        var docHeight = $(document).height();
        $('#loaderLayout span').css({height: docHeight + 'px'});
        $('#loaderLayout').show();
        $.ajax({
            url: '<?php echo $this->Url->build(["controller" => "Users", "action" => "register"]); ?>',
            data: data,
            dataType: 'JSON',
            type: 'POST',
            enctype: 'multipart/form-data',
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                $('#loaderLayout').hide();
                if (response.error === 1) {
                    $.each(response.data, function (index, val) {
                        var element = $('#register [name="' + index + '"]');
                        if (!element.is(':radio')) {
                            if (index === 'display_name') {
                                var representative_id = $('select[name="representative_id"] option:selected').val();
                                if (representative_id === '1') {
                                    element.after('<p class="error-message">Individual Name is required</p>');
                                } else {
                                    element.after('<p class="error-message">Company Name is required</p>');
                                }
                            } else {
                                element.after('<p class="error-message">' + val[Object.keys(val)[0]] + '</p>');
                            }
                        } else {
                            element.parent('label').parent('.radio').parent('.form-group').append('<p class="error-message">' + val[Object.keys(val)[0]] + '</p>');
                        }
                    });
                    var errorOffset = parseFloat($(document).find('.error-message').first().offset().top) - 80;
                    $("html, body").animate({scrollTop: errorOffset + 'px'}, 500);
                } else if (response.error === 3) {
                    swal({
                        title: 'Alert!',
                        text: response.data,
                        type: 'info',
                        showCancelButton: false
                    }).then(function () {
                        Registration.uploadDocuments();
                    }, function () {
                        Registration.uploadDocuments();
                    });
                } else {
                    Registration.uploadDocuments();
//                    swal({
//                        title: 'Success!',
//                        text: response.data,
//                        type: 'success',
//                        showCancelButton: false
//                    }).then(function () {
////                        window.location = '<?php //echo $this->Url->build(['controller' => 'Home', 'action' => 'index']);   ?>';
//                    }, function () {
//                        Registration.uploadDocuments();
//                        //window.location = '<?php //echo $this->Url->build(['controller' => 'Home', 'action' => 'index']);   ?>';
//                    });
                }
            },
            error: function (error) {
                $('#loaderLayout').hide();
                swal("Error!", "Something went wrong on the Server. Please try again.", "error");
            }
        });
    });

    $(document).ready(function () {
        var max_fields = 3;
        var wrapper = $("#directors");
        var add_button = $(".add_director_button");

        var x = 1;
        $(add_button).click(function (e) {
            e.preventDefault();
            if (x <= max_fields) {
                x++;
                $(wrapper).append('<div><input type="text" name="directors_name[]" placeholder="Director Name" required="required" class="form-control"/><a href="#" class="remove_field" title="Remove Director\'s Name">X</a></div>'); //add input box
            } else
            {
                $(this).attr('disabled', 'disabled');
                $("#directors p").css('color', 'red')
                $("#directors p").html('You can add only 3 fields');
            }
        });

        $(wrapper).on("click", ".remove_field", function (e) {
            e.preventDefault();
            $(this).parent('div').remove();
            x--;
            $("#directors p").html('');
            add_button.prop('disabled', false);
        });
        $('[data-toggle="tooltip"]').tooltip();

        $('#checkAvailability').on('click', function () {
            var userName = $('#user-name').val();
            if (!$.trim(userName)) {
                swal("Error!", "Please enter a User name", "error");
                return false;
            }
            $.ajax({
                url: '<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'checkAvailability']); ?>',
                data: {term: $.trim(userName)},
                type: 'POST',
                dataType: 'JSON',
                beforeSend: function (xhr) {
                    var docHeight = $(document).height();
                    $('#loaderLayout span').css({height: docHeight + 'px'});
                    $('#loaderLayout').show();
                },
                success: function (response) {
                    $('#loaderLayout').hide();
                    var type = 'success';
                    if (response.error) {
                        type = 'warning';
                    }
                    swal(response.title, response.message, type);
                },
                error: function () {
                    $('#loaderLayout').hide();
                    swal("Error!", "Something went wrong on the Server. Please try again.", "error");
                }
            });
        });

    });
</script>
<?php
echo $this->Html->script('registration.js');
