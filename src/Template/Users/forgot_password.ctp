<div class="forgot-password-wrap">
    <div class="forgot-password-pg">
     <h1>Forgot Password</h1>
            <div class="form-wrap">
            <?php echo $this->Form->create($userData); ?>
                <fieldset>
                    <input type="hidden" name="id" value="">
                    <?php
                    echo $this->Form->input('email', ['type' => 'text', 'class' => 'form-control', 'label' => false, 'templates' => [
                            'inputContainer' => '<div class="form-group">{{content}}</div>'
                        ], 'placeholder' => 'E-mail']);
                    
                    echo $this->Form->button('Submit', ['class' => 'btn btn-lg btn-block', 'formnovalidate' => true]);
                    ?>
                </fieldset>
                <?php echo $this->Form->end(); ?>
        </div>

    </div>
    </div>
