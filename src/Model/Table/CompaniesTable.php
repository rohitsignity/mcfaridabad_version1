<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Companies Model
 *
 * @property \Cake\ORM\Association\BelongsTo $SecurityQuses
 *
 * @method \App\Model\Entity\Company get($primaryKey, $options = [])
 * @method \App\Model\Entity\Company newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Company[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Company|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Company patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Company[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Company findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CompaniesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);
        $this->table('companies');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator->requirePresence('company_name', 'create')->notEmpty('company_name', 'Company Name is required');
        $validator->requirePresence('representative_id', 'create')->notEmpty('representative_id', 'Select Representative');
        $validator->requirePresence('name', 'create')->notEmpty('name', 'Name is Required');
        $validator->requirePresence('phone', 'create')->notEmpty('phone', 'Telephone number is Required')->add('phone','validFormat',[
            'rule' => 'numeric',
            'message' => 'Phone number should be numeric'
        ]);
        $validator->requirePresence('email', 'create')->notEmpty('email', 'Company E-mail is Required')->add('email', 'validFormat', [
            'rule' => 'email',
            'message' => 'E-mail must be valid'
        ]);
        
        $validator->requirePresence('name', 'create')->notEmpty('name', 'Name is Required');
        $validator->requirePresence('phone', 'create')->notEmpty('phone', 'Telephone number is Required')->add('phone','validFormat',[
            'rule' => 'numeric',
            'message' => 'Phone number should be numeric'
        ]);
        $validator->requirePresence('email', 'create')->notEmpty('email', 'Company E-mail is Required')->add('email', 'validFormat', [
            'rule' => 'email',
            'message' => 'E-mail must be valid'
        ]);
        
        $validator->notEmpty('name_1', 'Name is Required', function($context) {
            return isset($context['data']['name_1']);
        });
        $validator->notEmpty('phone_1', 'Telephone number is Required', function($context) {
            return isset($context['data']['phone_1']);
        })->add('phone_1','validFormat',[
            'rule' => 'numeric',
            'message' => 'Phone number should be numeric'
        ]);        
        $validator->notEmpty('email_1', 'Company E-mail is Required', function($context) {
            return isset($context['data']['email_1']);
        })->add('email_1', 'validFormat', [
            'rule' => 'email',
            'message' => 'E-mail must be valid'
        ]);
        
        $validator->notEmpty('name_2', 'Name is Required', function($context) {
            return isset($context['data']['name_2']);
        });
        $validator->notEmpty('phone_2', 'Telephone number is Required', function($context) {
            return isset($context['data']['phone_2']);
        })->add('phone_2','validFormat',[
            'rule' => 'numeric',
            'message' => 'Phone number should be numeric'
        ]);        
        $validator->notEmpty('email_2', 'Company E-mail is Required', function($context) {
            return isset($context['data']['email_2']);
        })->add('email_2', 'validFormat', [
            'rule' => 'email',
            'message' => 'E-mail must be valid'
        ]);
        
        
        $validator->requirePresence('pan_number', 'create')->notEmpty('pan_number', 'Pan number is required');
        $validator->requirePresence('incorporation_date', 'create')->notEmpty('incorporation_date', 'Date of incorporation is required');
        $validator->allowEmpty('tin_number');
        $validator->allowEmpty('service_tax_number');
        $validator->requirePresence('address', 'create')->notEmpty('address', 'Mailing Address is required');
        $validator->requirePresence('email_corresponding', 'create')->notEmpty('email_corresponding', 'Email is required')->add('email_corresponding', 'validFormat', [
            'rule' => 'email',
            'message' => 'E-mail must be valid'
        ]);
        $validator->requirePresence('city', 'create')->notEmpty('city', 'City is required');
        $validator->requirePresence('state', 'create')->notEmpty('state', 'State is required');
        $validator->requirePresence('pincode', 'create')->notEmpty('pincode', 'Pincode is required');
        $validator->allowEmpty('permanentAddressCheck');
        $validator->requirePresence('p_address', 'create')->notEmpty('p_address', 'Permanent Address is required', function($context) {
            return !isset($context['data']['permanentAddressCheck']);
        });
        $validator->requirePresence('p_city', 'create')->notEmpty('p_city', 'City is required', function($context) {
            return !isset($context['data']['permanentAddressCheck']);
        });
        $validator->requirePresence('p_state', 'create')->notEmpty('p_state', 'State is required', function($context) {
            return !isset($context['data']['permanentAddressCheck']);
        });
        $validator->requirePresence('p_pincode', 'create')->notEmpty('p_pincode', 'Pincode is required', function($context) {
            return !isset($context['data']['permanentAddressCheck']);
        });
        $validator->requirePresence('p_email_corresponding', 'create')->notEmpty('p_email_corresponding', 'Email is required', function($context) {
            return !isset($context['data']['permanentAddressCheck']);
        })->add('p_email_corresponding', 'validFormat', [
            'rule' => 'email',
            'message' => 'E-mail must be valid'
        ]);
        return $validator;
    }

}
