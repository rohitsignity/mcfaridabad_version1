<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class TenderCommentsTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);
        $this->addBehavior('Timestamp');
    }
    
    public function validationDefault(Validator $validator) {
        $validator->notEmpty('comment','Rejection reason is required.',function($context){
            if($context['data']) {
                return $context['data']['status'] == 0;
            }
        });
        return $validator;
    }

}
