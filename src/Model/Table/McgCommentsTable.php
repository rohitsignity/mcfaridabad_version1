<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use ArrayObject;

/**
 * Description of McgCommentsTable
 *
 * @author yogesh
 */
class McgCommentsTable extends Table {

    public function beforeMarshal(Event $event, ArrayObject $data) {
        $data['comment'] = trim($data['comment']);
    }

    public function initialize(array $config) {
        $this->addBehavior('Timestamp');
    }

    public function validationDefault(Validator $validator) {
//        $validator->requirePresence('omd_size_less_75');
//        $validator->requirePresence('intersection_distance');
//        $validator->requirePresence('two_omd_distance');
//        $validator->requirePresence('obstruction_to_road');
        $validator->notEmpty('comment', 'This field is required');
        return $validator;
    }

    public function validationHold(Validator $validator) {
//        $validator->requirePresence([
//            'reasonCheck' => [
//                'mode' => 'create',
//                'message' => 'Please select the reason',
//            ]
//        ]);
        $validator->notEmpty('comment', 'This field is required');
        return $validator;
    }

}
