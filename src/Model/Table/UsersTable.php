<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;

class UsersTable extends Table {

    public function initialize(array $config) {
        $this->addBehavior('Timestamp');

        $this->hasMany('UserProfiles', [
            'foreignKey' => 'user_profile_id',
            'joinType' => 'INNER'
        ]);
    }

    public function validationDefault(Validator $validator) {
        $validator->notEmpty('display_name', 'Company Name is required');
        $validator->notEmpty('email', 'E-mail is required')->add('email', 'validFormat', [
            'rule' => 'email',
            'message' => 'E-mail must be valid'
        ])->add('email', 'unique', [
            'rule' => function ($value, $context) {
                return !$this->exists(['email' => $value, 'status !=' => '2','type' => 'user']);
            },
            'provider' => 'table',
            'message' => 'E-mail already exists'
        ]);
        $validator->add('mobile', [
            'validFormat' => [
                'rule' => 'numeric',
                'message' => 'Mobile number have to be numeric',
            ],
            'length-min' => [
                'rule' => ['minLength', 10],
                'message' => 'Mobile number have to be at least 10 characters!',
            ],
            'length-max' => [
                'rule' => ['maxLength', 10],
                'message' => 'Mobile number have to be at most 10 characters!',
            ]
        ])->notEmpty('mobile', 'Mobile number is required');
        $validator->add('password', [
            'length' => [
                'rule' => ['minLength', 6],
                'message' => 'The password have to be at least 6 characters!',
            ]
        ])->notEmpty('password', 'Password is required');
        $validator->add('conf_password', [
            'match' => [
                'rule' => ['compareWith', 'password'],
                'message' => 'The passwords does not match!',
            ]
        ])->notEmpty('conf_password', 'Confirm Password is required');
        $validator->notEmpty('security_qus_id', 'Security Question is Required');
        $validator->notEmpty('answer', 'Answer is Required')->add('answer', [
            'length' => [
                'rule' => ['minLength', 4],
                'message' => 'Answer have to be at least 4 characters!',
            ]
        ]);
        $validator->requirePresence([
            'terms' => [
                'mode' => 'create',
                'message' => 'You must agree to the terms',
            ]
        ]);
        $validator->notEmpty('representative_id', 'Type of entity is Required');

        $validator->notEmpty('director_name', 'Director name is required', function($context) {
            if (isset($context['data']['representative_id']) && $context['data']['representative_id'] == 5) {
                return true;
            }
            return false;
        });
        $validator->notEmpty('din', 'Din Number is required', function($context) {
            if (isset($context['data']['representative_id']) && $context['data']['representative_id'] == 5) {
                return true;
            }
            return false;
        });
        $validator->notEmpty('authorised_contact_person', 'Authorised Contact Person is required', function($context) {
            $allowed = [4,5,6];
            if (isset($context['data']['representative_id']) && in_array($context['data']['representative_id'],$allowed)) {
                return true;
            }
            return false;
        });
        $validator->notEmpty('company_address', 'Company Address is required');
        $validator->notEmpty('correspondence_address', 'Correspondence address is required');
        $validator->notEmpty('incorporation_date', 'Incorporation date is required');
        $validator->notEmpty('annual_turnover', 'Annual Turnover is required');
        $validator->notEmpty('experience', 'Years of experience is required');
        $validator->notEmpty('property_id', 'Property I\'d is required');
        $validator->notEmpty('address_type', 'Type of address proof is required');
        $validator->add('address_proof', [
            'validExtension' => [
                'rule' => ['extension', ['gif', 'jpeg', 'png', 'jpg', 'doc', 'docx', 'xls', 'xlsx', 'pdf']],
                'message' => __('These files extension are allowed: .png, .gif, .jpeg, .jpg, .doc, .docx, .xls, .xlsx, .pdf')
            ]
        ])->notEmpty('address_proof', 'Address Proof is required', function($context) {
            $allowedRepresentativeId = [1, 2, 3, 4, 5, 6];
            if (isset($context['data']['representative_id']) && in_array($context['data']['representative_id'], $allowedRepresentativeId)) {
                return true;
            }
            return false;
        });
        return $validator;
    }
    
    public function validationGuestInfo(Validator $validator) {
        $validator->notEmpty('display_name', 'Company Name is required');
        /*
        $validator->notEmpty('email', 'E-mail is required')->add('email', 'validFormat', [
            'rule' => 'email',
            'message' => 'E-mail must be valid'
        ]);
        */
        $validator->notEmpty('email', 'E-mail is required')->add('email', 'validFormat', [
            'rule' => 'email',
            'message' => 'E-mail must be valid'
        ])->add('email', 'unique', [
            'rule' => function ($value, $context) {
                return !$this->exists(['email' => $value,'type' => 'guest']);
            },
            'provider' => 'table',
            'message' => 'E-mail already exists'
        ]);
        
        
        $validator->requirePresence([
            'terms' => [
                'mode' => 'create',
                'message' => 'You must agree to the terms',
            ]
        ]);
        $validator->notEmpty('representative_id', 'Type of entity is Required');

        $validator->notEmpty('director_name', 'Director name is required', function($context) {
            if (isset($context['data']['representative_id']) && $context['data']['representative_id'] == 5) {
                return true;
            }
            return false;
        });
        $validator->notEmpty('din', 'Din Number is required', function($context) {
            if (isset($context['data']['representative_id']) && $context['data']['representative_id'] == 5) {
                return true;
            }
            return false;
        });
        $validator->notEmpty('authorised_contact_person', 'Authorised Contact Person is required', function($context) {
            $allowed = [4,5,6];
            if (isset($context['data']['representative_id']) && in_array($context['data']['representative_id'],$allowed)) {
                return true;
            }
            return false;
        });
        $validator->notEmpty('company_address', 'Company Address is required');
        $validator->notEmpty('correspondence_address', 'Correspondence address is required');
        $validator->notEmpty('incorporation_date', 'Incorporation date is required');
        $validator->notEmpty('annual_turnover', 'Annual Turnover is required');
        $validator->notEmpty('experience', 'Years of experience is required');
        $validator->notEmpty('property_id', 'Property I\'d is required');
        $validator->notEmpty('address_type', 'Type of address proof is required');
        $validator->add('address_proof', [
            'validExtension' => [
                'rule' => ['extension', ['gif', 'jpeg', 'png', 'jpg', 'doc', 'docx', 'xls', 'xlsx', 'pdf']],
                'message' => __('These files extension are allowed: .png, .gif, .jpeg, .jpg, .doc, .docx, .xls, .xlsx, .pdf')
            ]
        ])->notEmpty('address_proof', 'Address Proof is required', function($context) {
            $allowedRepresentativeId = [1, 2, 3, 4, 5, 6];
            if (isset($context['data']['representative_id']) && in_array($context['data']['representative_id'], $allowedRepresentativeId)) {
                return true;
            }
            return false;
        });
        return $validator;
    }

    public function validationLogin(Validator $validator) {
        $validator->notEmpty('email', 'E-mail is required.');
        $validator->notEmpty('password', 'Password is required.');
        $validator->allowEmpty('remember');
        return $validator;
    }

    public function validationSmcUsers(Validator $validator) {
        $validator->notEmpty('email', 'E-mail is required.');
        $validator->notEmpty('password', 'Password is required.');
        $validator->notEmpty('type', 'Designation is required.');
        return $validator;
    }
    
    public function validationGuest(Validator $validator) {
        $validator->add('user_name', [
            'validFormat' => [
                'rule' => 'numeric',
                'message' => 'Mobile number have to be numeric',
            ],
            'length' => [
                'rule' => ['minLength', 10],
                'message' => 'Mobile number have to be at least 10 characters!',
            ]
        ])->notEmpty('user_name', 'Mobile number is required');
        return $validator;
    }
    
    public function validationGuestOtp(Validator $validator) {
        $validator->notEmpty('password','One time password is required');
        return $validator;
    }

    public function validationForgotPassword(Validator $validator) {
        $validator->notEmpty('email', 'E-mail is required.')->add('email', [
            'checkEmailExist' => [
                'rule' => 'checkEmailExist',
                'provider' => 'table',
                'message' => ' E-mail does not exists'
            ]
        ]);
        return $validator;
    }

    public function checkEmailExist($value, $context) {
        $usersTable = TableRegistry::get('Users');
        return $usersTable->exists(['email' => $context['data']['email']]);
    }

    public function validationPassword(Validator $validator) {
        /// current password check
        $validator
                ->add('current_password', 'custom', [
                    'rule' => function($value, $context) {
                        $user = $this->get($context['data']['id']);
                        if ($user) {
                            if ((new DefaultPasswordHasher)->check($value, $user->password)) {
                                return true;
                            }
                        }
                        return false;
                    },
                    'message' => 'The current password does not match!',
                ])
                ->notEmpty('current_password', 'Current password can not be left empty.');

        /// New password check	
        $validator
                ->add('new_password', [
                    'length' => [
                        'rule' => ['minLength', 6],
                        'message' => 'The password have to be at least 6 characters!',
                    ]
                ])
                //->add('new_password',[ 'match'=>[ 'rule'=> ['compareWith','confirm_password'], 'message'=>'New password does not match!', ] ]) 
                ->notEmpty('new_password', 'New password can not be left empty.');

        /// Confirm password check	
        $validator
                ->add('confirm_password', [
                    'length' => [
                        'rule' => ['minLength', 6],
                        'message' => 'The password have to be at least 6 characters!',
                    ]
                ])
                ->add('confirm_password', ['match' => ['rule' => ['compareWith', 'new_password'], 'message' => 'Confirm password and New password does not match!',]])
                ->notEmpty('confirm_password', 'Confirm password can not be left empty.');

        return $validator;
    }

    public function validationResetPassword(Validator $validator) {

        /// New password check	
        $validator
                ->add('password', [
                    'length' => [
                        'rule' => ['minLength', 6],
                        'message' => 'The password have to be at least 6 characters!',
                    ]
                ])
                ->notEmpty('password', 'New password can not be left empty.');

        /// Confirm password check	
        $validator
                ->add('confirm_password', [
                    'length' => [
                        'rule' => ['minLength', 6],
                        'message' => 'The password have to be at least 6 characters!',
                    ]
                ])
                ->add('confirm_password', ['match' => ['rule' => ['compareWith', 'password'], 'message' => 'Confirm password and New password does not match!',]])
                ->notEmpty('confirm_password', 'Confirm password can not be left empty.');

        return $validator;
    }

    public function validationMcgAdd(Validator $validator) {
        $validator->notEmpty('display_name', 'Please Enter SMC Name');
        $validator->add('mobile', [
            'validFormat' => [
                'rule' => 'numeric',
                'message' => 'Mobile number have to be numeric',
            ],
            'length' => [
                'rule' => ['minLength', 10],
                'message' => 'Mobile number have to be at least 10 characters!',
            ]
        ])->notEmpty('mobile', 'Mobile number is required');
        $validator->notEmpty('email', 'E-mail is required')->add('email', 'validFormat', [
            'rule' => 'email',
            'message' => 'E-mail must be valid'
        ])->add('email', 'unique', [
            'rule' => 'validateUnique',
            'provider' => 'table',
            'message' => 'E-mail already exists'
        ]);
        $validator->add('password', [
            'length' => [
                'rule' => ['minLength', 6],
                'message' => 'The password have to be at least 6 characters!',
            ]
        ])->notEmpty('password', 'Password is required');
        $validator->add('conf_password', [
            'match' => [
                'rule' => ['compareWith', 'password'],
                'message' => 'The passwords does not match!',
            ]
        ])->notEmpty('conf_password', 'Confirm Password is required');
        $validator->notEmpty('security_qus_id', 'Security Question is Required');
        $validator->notEmpty('answer', 'Answer is Required')->add('answer', [
            'length' => [
                'rule' => ['minLength', 4],
                'message' => 'Answer have to be at least 4 characters!',
            ]
        ]);
        $validator->notEmpty('type', 'Please select SMC Level.');
        
        
        $validator->notEmpty('zone', 'MCG1 Zone is required', function($context) {
            if (isset($context['data']['type']) && $context['data']['type'] == 'MCG1') {
                return true;
            }
            return false;
        });
        
        
        
        return $validator;
    }

    public function validationRepresentativeFields(Validator $validator) {
        /// current password check
        $validator
                ->add('current_password', 'custom', [
                    'rule' => function($value, $context) {
                        $user = $this->get($context['data']['id']);
                        if ($user) {
                            if ((new DefaultPasswordHasher)->check($value, $user->password)) {
                                return true;
                            }
                        }
                        return false;
                    },
                    'message' => 'The current password does not match!',
                ])
                ->notEmpty('current_password', 'Current password can not be left empty.');

        /// New password check	
        $validator
                ->add('new_password', [
                    'length' => [
                        'rule' => ['minLength', 6],
                        'message' => 'The password have to be at least 6 characters!',
                    ]
                ])
                //->add('new_password',[ 'match'=>[ 'rule'=> ['compareWith','confirm_password'], 'message'=>'New password does not match!', ] ]) 
                ->notEmpty('new_password', 'New password can not be left empty.');

        /// Confirm password check	
        $validator
                ->add('confirm_password', [
                    'length' => [
                        'rule' => ['minLength', 6],
                        'message' => 'The password have to be at least 6 characters!',
                    ]
                ])
                ->add('confirm_password', ['match' => ['rule' => ['compareWith', 'new_password'], 'message' => 'Confirm password and New password does not match!',]])
                ->notEmpty('confirm_password', 'Confirm password can not be left empty.');

        return $validator;
    }

}
