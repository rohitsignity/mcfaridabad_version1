<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class DocumentsTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);
        $this->addBehavior('Timestamp');
    }

    public function validationDefault(Validator $validator) {
        $validator->notEmpty('name','Document Name is Required')->add('name', 'unique', [
            'rule' => 'validateUnique',
            'provider' => 'table',
            'message' => 'Document already exists'
        ]);
        return $validator;
    }

}
