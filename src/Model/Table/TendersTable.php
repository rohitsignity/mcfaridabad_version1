<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class TendersTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);
        $this->addBehavior('Timestamp');
    }

    public function validationDefault(Validator $validator) {
        $validator->notEmpty('name', 'Tender Name is required');
        $validator->notEmpty('typology_id', 'Typology is required');
        $validator->notEmpty('subcategory_id', 'Sub Category is required');
        $validator->notEmpty('department_name', 'Department Name is required');
        $validator->notEmpty('tender_value', 'Estimated Tender Value is required')->add('tender_value', 'numeric', array('rule' => 'numeric', 'message' => 'Estimated Tender Value must be Numeric'));
        $validator->notEmpty('document_fee', 'Document Fee is required')->add('document_fee', 'numeric', array('rule' => 'numeric', 'message' => 'Document Fee must be Numeric'));
//        $validator->notEmpty('terms_cond', 'Terms & Conditions are required');
        $validator->notEmpty('user_segment', 'Users Annual Turnover is required');
        $validator->notEmpty('experience', 'Users Years of experience is required');
        $validator->notEmpty('users', 'Eligible users are required');
        $validator->notEmpty('issuing_authority', 'Issuing Authority is required');
        $validator->notEmpty('authorised_by', 'Authorised by is required');
        $validator->notEmpty('start_date', 'Start Date is required');
        $validator->notEmpty('end_date', 'End Date & Time is required')->add('end_date', 'comparison', [
            'rule' => function ($value, $context) {
                return date('Y-m-d H:i:s', strtotime($value)) >= date('Y-m-d H:i:s', strtotime($context['data']['start_date']));
            },
            'message' => 'End Date & Time must be greater than Start Date & Time'
        ]);
        $validator->notEmpty('application_date', 'Tender application Last date is required')->add('application_date', 'comparison', [
            'rule' => function ($value, $context) {
                return date('Y-m-d', strtotime($value)) <= date('Y-m-d', strtotime($context['data']['start_date']));
            },
            'message' => 'Tender application Last date must be less than equal to Start Date'
        ]);
        $validator->notEmpty('doc_download_date', 'Document Download Date is required');
//                ->add('doc_download_date', 'comparison', [
//            'rule' => function ($value, $context) {
//                return (date('Y-m-d', strtotime($value)) >= date('Y-m-d', strtotime($context['data']['start_date'])) && date('Y-m-d', strtotime($value)) <= date('Y-m-d', strtotime($context['data']['end_date'])));
//            },
//            'message' => 'Document Download Date must be between Start Date and End Date'
//        ]);
        $validator->notEmpty('bid_doc', 'RFP Document is required')->add('bid_doc', [
            'validExtension' => [
                'rule' => ['extension', ['gif', 'jpeg', 'png', 'jpg', 'doc', 'docx', 'xls', 'xlsx', 'pdf']],
                'message' => __('These files extension are allowed: .png, .gif, .jpeg, .jpg, .doc, .docx, .xls, .xlsx, .pdf')
            ]
        ]);
        $validator->notEmpty('min_amount', 'Minimum Guarantee is required')->add('min_amount', 'numeric', array('rule' => 'numeric', 'message' => 'Minimum Guarantee must be Numeric'))->add('min_amount', 'comparison', [
            'rule' => function ($value, $context) {
                return $value >= $context['data']['tender_value'];
            },
            'message' => 'Minimum Guarantee must be greater than equal to Tender Value'
        ]);
        $validator->notEmpty('email', 'E-mail is required')->add('email', 'validFormat', [
            'rule' => 'email',
            'message' => 'E-mail must be valid'
        ]);
        $validator->notEmpty('contact', 'Contact Number is Required')->add('contact', [
            'minLength' => [
                'rule' => ['minLength', 10],
                'message' => 'The Contact Number have to be at least 10 characters!',
            ],
            'maxLength' => [
                'rule' => ['maxLength', 10],
                'message' => 'The Contact Number must not larger than 10 characters!',
            ],
            'numeric' => [
                'rule' => 'numeric',
                'message' => 'Contact must be Numeric'
            ]
        ]);
        $validator->allowEmpty('web_link')->add('web_link', 'valid', ['rule' => 'url', 'message' => 'Invalid URL']);
        $validator->notEmpty('device_ids', 'Device is required');
        return $validator;
    }

}
