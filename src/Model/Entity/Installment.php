<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class Installment extends Entity {

    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

}
