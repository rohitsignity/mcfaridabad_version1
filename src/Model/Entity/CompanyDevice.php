<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CompanyDevice Entity
 *
 * @property int $id
 * @property string $company_code
 * @property int $category_id
 * @property int $sub_category_id
 * @property string $paid
 * @property float $fee
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\SubCategory $sub_category
 * @property \App\Model\Entity\CompanyDeviceField[] $company_device_fields
 */
class CompanyDevice extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
