<div id="page-content-wrapper">
    <!-- Keep all page content within the page-content inset div! -->
    <div class="page-content inset reports">
        <div class="headings-all dash-in">
            <h1>Reports</h1>
        </div>
        <ul>
            <?php
            foreach ($data as $item) {
                ?>
                <li><a href="<?php echo $item['url']; ?>"><?php echo $item['text']; ?><span class="glyphicon glyphicon-download pull-right"></span></a></li>
                <?php
            }
            ?>
        </ul>
    </div>
</div>