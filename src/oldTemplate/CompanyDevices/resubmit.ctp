<div id="page-content-wrapper">
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-V-cjIUZu6DPHpERzLBIV5OnR_AxkRtw"></script>
    <!-- Keep all page content within the page-content inset div! -->
    <div class="page-content inset app-status">
        <div class="company-reg devic-reg">
            <div class="headings-all text-center">
                <h1>Device Re-Submission</h1>
                <i>Reason: <?php echo $reason;?></i>
            </div>
            <div class="stepsblk clearfix">
                <ul>
                    <li class="step1-blk active">
                        <a href="#">
                            <span>1</span>
                            <h2>Step1</h2>
                            <p>Device Information</p>
                        </a>
                    </li>
                    <li class="step1-blk">
                        <a href="#">
                            <span>2</span>
                            <h2>Step2</h2>
                            <p>Device Information</p>
                        </a>
                    </li>
                    <li class="step1-blk">
                        <a href="#">
                            <span>3</span>
                            <h2>Step3</h2>
                            <p>Preview</p>
                        </a>
                    </li>

                </ul>
            </div>
            <div class="regist-form">
                <h2 class="formh2 orang">Device Information</h2>
                <div class="row">
                    <form method="POST" action="<?php echo $this->Url->build(["controller" => "CompanyDevices", "action" => "resubmitDevice",$deviceId]); ?>" enctype="multipart/form-data">
                        <div class="col-md-6 form-step1 form-steps">
                            <?php
//                            echo $this->Form->input('category_id', ['empty' => 'Select Typology', 'type' => 'select', 'options' => [], 'label' => 'Typology', 'id' => 'typology', 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>']]);
                            echo $this->Form->input('category_id', ['empty' => false, 'type' => 'select', 'options' => $categories, 'label' => 'Typology', 'id' => 'typology', 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>']]);
                            ?>
                        </div>
                        <div class="col-md-6 form-step1 form-steps">
                            <?php echo $this->Form->input('subcategory', ['empty' => false, 'type' => 'select', 'options' => $subCategories, 'label' => 'Sub Category', 'id' => 'sub_category', 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>']]); ?>
                        </div>
                        <div id="formData"></div>
                    </form>
                </div>
            </div>

        </div>


    </div>
</div>
<script>
    $(document).on('change', 'select[name="concessionaire_type"]', function () {
        SubCategory.calculatePayble();
        $(document).find('input[name="concessionaire_name"]').val('');
        if($(this).val() === 'MCG') {
            $(document).find('input[name="concessionaire_name"]').val('Municipal Corporation Gurugram');
        }
    });
    $(document).on('keyup', 'input[name="concession_fees"]', function () {
        SubCategory.calculatePayble();
    });
    var OmdSites = {
        'omds installed under one roof' : 'omds_installed_under_one_roof',
        'omds installed within premises but in open courtyard' : 'omds_installed_within_premises'
    };
    var OmdSitesTitles = {
        'omd_one_roof': 'OMDS installed under one roof',
        'omd_premises' : 'OMDs installed within premises but in open courtyard'
    };
    var Typology = {
        typologyId: '<?php echo $category;?>',
        deviceTypeId: '',
        categoryLocalData: {},
        typologoLocalData: {},
        deviceType: function (element) {
            SubCategory.clearFormPanel('formData');
            this.deviceTypeId = $(element).val();
            this.getCategories();
        },
        getCategories: function () {
            var self = this;
            var deviceType = self.deviceTypeId;
            if (self.categoryLocalData[deviceType]) {
                self.categoryHtml(self.categoryLocalData[deviceType]);
            } else {
                $.ajax({
                    url: '<?php echo $this->Url->build(['controller' => 'CompanyDevices', 'action' => 'getCategories']); ?>',
                    data: {deviceType: deviceType},
                    dataType: 'JSON',
                    type: 'POST',
                    success: function (response) {
                        var responseData = [];
                        $.each(response, function (index, val) {
                            var obj = {};
                            obj.id = val.id;
                            obj.title = val.title;
                            responseData.push(obj);
                        });
                        self.categoryLocalData[deviceType] = responseData;
                        self.categoryHtml(self.categoryLocalData[deviceType]);
                    },
                    error: function (result) {

                    }
                });
            }
        },
        typologyData: function (typologyId, selectBoxId) {
            var self = this;
            self.typologyId = typologyId;
            var allowedTypologies = ["4", "10"];
            if (typologyId && $.inArray(typologyId, allowedTypologies) === -1) {
                self.subCategoryHtml([], 'sub_category');
                swal({
                    title: 'Alert!',
                    text: 'Please choose from Typology D or J',
                    type: 'info'
                });
                return false;
            }
            if (self.typologoLocalData[typologyId])
            {
                var responseData = self.typologoLocalData[typologyId];
                self.subCategoryHtml(responseData, selectBoxId);
            } else {
                $.ajax({
                    url: '<?php echo $this->Url->build(["controller" => "CompanyDevices", "action" => "getSubCategory"]); ?>',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {id: typologyId},
                    success: function (response) {
                        var responseData = [];
                        $.each(response, function (index, val) {
                            var obj = {};
                            obj.id = val.id;
                            obj.title = val.title;
                            responseData.push(obj);
                        });
                        self.typologoLocalData[typologyId] = responseData;
                        self.subCategoryHtml(responseData, selectBoxId);
                    }, error: function (error) {
                        window.location = '<?php echo $this->Url->build(["controller" => "Home", "action" => "index"]); ?>';
                    }
                });
            }

        },
        categoryHtml: function (dataArr) {
            var html = '<option value="">Select Typology</option>';
            $.each(dataArr, function (index, val) {
                html += '<option value="' + val.id + '">' + val.title + '</option>';
            });
            $('#typology').html(html);
        },
        subCategoryHtml: function (dataArr, selectBoxId) {
            var html = '<option value="">Select Subcategory</option>';
            $.each(dataArr, function (index, val) {
                html += '<option value="' + val.id + '">' + val.title + '</option>';
            });
            $('#' + selectBoxId).html(html);
        }
    };


    var SubCategory = {
        subCategoryId: '',
        defaultValidations: {},
        customValidations: {},
        subCategoryLocalData: {},
        omdRoofCount: 0,
        omdPremisesCount: 0,
        typeIMultiple: {'Standard OMD' : 1, 'LED/LCD OMD' : 3},
        typeIPrice: {'omds_installed_under_one_roof' : 450, 'omds_installed_within_premises' : 700},
        tenureData: {"3 months": 3, "6 months": 6, "9 months": 9, "1 year": 12,"2 years": 24,"3 years": 36},
        typologyATypeFields: { shelter_data: 'shelters_type',route_markers_data : 'route_markers_type', foot_over_data : 'foot_over_type',cycle_station_data : 'cycle_station_type',police_booth_data : 'police_booth_type',sitting_bench_data: 'sitting_bench_type'},
        typologyAFieldNames: { shelter_data: 'shelter_', route_markers_data: 'route_markers_', foot_over_data: 'foot_over_', cycle_station_data: 'cycle_station_' ,police_booth_data : 'police_booth_',sitting_bench_data: 'sitting_bench_'},
        omdActions: {0: 'Under Process',1:'Approved',2:'Re-submit', 3:'Rejected'},
        fieldsData: JSON.parse('<?php echo json_encode($fieldsData);?>'),
        omdSiteData: ['Standard OMD','LED/LCD OMD'],
        selectDynData: {},
        clearFormPanel: function (panelId) {
            $('#' + panelId).html('');
        },
        subCategoryData: function (subCategoryId, panelId) {
			var self = this;
            this.subCategoryId = subCategoryId;
            self.defaultValidations = {};
            if (self.subCategoryLocalData[subCategoryId]) {
                self.getHtml(self.subCategoryLocalData[subCategoryId], panelId);
                if ($(document).find('#formData input[name="location"]').length) {
                    initialize();
                    $("body").tooltip({selector: '[data-toggle=tooltip]'});
                }
            } else {
                $.ajax({
                    url: '<?php echo $this->Url->build(["controller" => "CompanyDevices", "action" => "getFields"]); ?>',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {id: subCategoryId},
                    success: function (response) {
                        self.subCategoryLocalData[subCategoryId] = response;
                        self.getHtml(response, panelId);
                        if ($(document).find('#formData input[name="location"]').length) {
                            initialize();
                            $("body").tooltip({selector: '[data-toggle=tooltip]'});
                        }
                        self.getFieldsVals(self.fieldsData);
                    }, error: function (error) {
                        window.location = '<?php echo $this->Url->build(["controller" => "Home", "action" => "index"]); ?>';
                    }
                });
            }
        },
         getFieldsVals: function(data) {
			//console.log(data);
            var self = this;
            if(Object.keys(self.selectDynData).length) {
                $.each(self.selectDynData,function(selectKey,selectVal){
                    var selectElement = $(document).find('[name="'+ selectKey +'"]');
                    var selectLabel = selectElement.parents('.form-groupY').find('label').html();
                    if(typeof selectVal === 'object') {
                        $.each(selectVal,function(selectIndex,selectData){
                            var selectOnValue = $.grep(data,function(n){
                                return n.field === selectIndex;
                            });
                            if(selectOnValue.length) {
                                var selectHtml = '<option value="">'+ selectLabel +'</option>';
                                $.each(selectData[selectOnValue[0].value],function(index,val){
                                    selectHtml += '<option value="'+ val +'">'+ val +'</option>';
                                });
                                selectElement.html(selectHtml);
                            }
                        });
                    }
                });
            }
            $.each(data,function(index,val) {
                if($(document).find('[name="'+ val.field +'"]').attr('type') !== 'file') {
                    if(val.field == 'building_type') {
                        var checkboxValues = (val.value).split(",")
                        $.each(checkboxValues,function(index,checkboxValue){
                            $(document).find('input[type="checkbox"][value="'+ $.trim(checkboxValue) +'"]').prop("checked",true);
                        });
                    } else if($.inArray(val.field,['omd_one_roof','omd_premises']) !== -1) {
                        self.getOMDTable(val);
                    } else if($.inArray(val.field,['metro_table']) !== -1) {
                        self.getMetroMrtsTable(val);
                    } else if(val.field == 'lighting') {
                        $(document).find('input[name="'+ val.field +'"][value="'+ val.value +'"]').prop("checked",true);
                    } else {
                        $(document).find('[name="'+ val.field +'"]').val(val.value);
                    } 
                    if(val.field === 'omd_no_venue') {
                        var html = '<div id="omd_size_omd_no_venue">';
                        for(var i= 1; i<= val.value; i++) {
                            self.defaultValidations['omd_no_venue_size_' + i + '-input-1'] = '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}';
                            var omdNoVenueValue = $.grep(data, function(v) {
                                return v.field === "omd_no_venue_size_"+i;
                            });
                            html += '<div class="col-md-12">';
                            html += '<label>OMD ' + i + ' Size</label>';
                            html += '<input type="text" name="omd_no_venue_size_' + i + '" class="form-control omd-size-meters omd_no_venue omd_no_venue_class" autocomplete="off" placeholder="13.5 sq. m ( 4 X 3.5 )" value="'+ omdNoVenueValue[0].value +'">';
                            html += '</div>';
                        }
                        html += '<div>';
                        $(document).find('input[name="omd_no_venue"]').after(html);
                    }
                    if(val.field === 'omd_no_others') {
                        var html = '<div id="omd_size_omd_no_others">';
                        for(var i=1; i<=val.value; i++) {
                            self.defaultValidations['omd_no_others_size_' + i + '-input-1'] = '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}';
                            self.defaultValidations['omd_no_others_location_' + i + '-input-1'] = '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}';
                            var omdNoVenueSizeValue = $.grep(data, function(v) {
                                return v.field === "omd_no_others_size_"+i;
                            });
                            var omdNoVenueLocationValue = $.grep(data, function(v) {
                                return v.field === "omd_no_others_location_"+i;
                            });
                            html += '<div class="col-md-6">';
                            html += '<label>OMD ' + i + ' Size</label>';
                            html += '<input type="text" name="omd_no_others_size_' + i + '" class="form-control omd-size-meters omd_no_others omd_no_others_class" autocomplete="off" placeholder="13.5 sq. m ( 4 X 3.5 )" value="'+ omdNoVenueSizeValue[0].value +'">';
                            html += '</div>';
                            html += '<div class="col-md-6">';
                            html += '<label>OMD ' + i + ' Location</label>';
                            html += '<input type="text" name="omd_no_others_location_' + i + '" class="form-control omd-location-meters" autocomplete="off" placeholder="OMD '+ i + ' Location" value="'+ omdNoVenueLocationValue[0].value +'">';
                            html += '</div>';
                        }
                        html += '</div>';
                        $(document).find('input[name="omd_no_others"]').after(html);
                    }
                    if(val.field === 'other_type_a_data') {
                        setTimeout(function(){
                            self.typologyAOtherForm(val);
                        },500);
                    }
                    if($.inArray(val.field,['shelter_data','route_markers_data','foot_over_data','cycle_station_data','police_booth_data','sitting_bench_data']) !== -1) {
                        
                        setTimeout(function(){
                            self.typologyAForm(val);
                        },500);
                    }
                }
            });
            if(self.subCategoryId === 34) {
                if($(document).find('select[name="omd_type_private_list"] option:selected').val() === 'Unipole') {
                    $(document).find('input[name="total_facade_area"]').parents('.form-groupY').hide();
                }
            }
        },
        typologyAOtherForm: function(data) {
            var omdTypeElement = $(document).find('select[name="omd_type_type_a"]');
            omdTypeElement.parents('.form-groupY').removeClass('col-md-6').addClass('col-md-12');
            var label = omdTypeElement.find('option:selected').val();
            omdTypeElement.find('option').not(':selected').remove();
             var html = '<div class="col-md-12" id="typologyATableOthers">';
            html += '<h3>'+ label +'</h3>';
            html += '<table class="table">';
            html += '<thead>';
            html += '<tr>';
            html += '<th>S. No.</th>';
            html += '<th>Type of Asset</th>';
            html += '<th>Location</th>';
            html += '<th>Height (in m)</th>';
            html += '<th>Width (in m)</th>';
            html += '<th>Total Area(in sq m)</th>';
            html += '<th>Latitude</th>';
            html += '<th>Longitude</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';
            var sno = 1;
            var counter = 0;
            $.each(data.value,function(index,val){
                html += '<tr>';
                html += '<td>'+ sno +'</td>';
                html += '<td>'+ label +'</td>';
                html += '<td><input class="form-control other_type_a_location" type="text" value="'+ val.location +'" name="other_type_a_location['+ counter +']"></td>';
                html += '<td><input class="form-control other_type_a_height" type="text" value="'+ val.height +'" name="other_type_a_height['+ counter +']" onkeyup="SubCategory.calculateOtherArea(this)"></td>';
                html += '<td><input class="form-control other_type_a_width" type="text" value="'+ val.width +'" name="other_type_a_width['+ counter +']" onkeyup="SubCategory.calculateOtherArea(this)"></td>';
                html += '<td><input class="form-control other_type_a_area" type="text" value="'+ val.area +'" readonly="readonly" name="other_type_a_area['+ counter +']"></td>';
                html += '<td><input class="form-control other_type_a_lat" type="text" value="'+ val.lat +'" name="other_type_a_lat['+ counter +']"></td>';
                html += '<td><input class="form-control other_type_a_lng" type="text" value="'+ val.lng +'" name="other_type_a_lng['+ counter +']"></td>';
                html += '</tr>';
                sno++;
                counter++;
            });
            html += '</tbody>';
            html += '</table>';
            html += '</div>';
            omdTypeElement.parents('.form-groupY').after(html);
        },
        typologyAForm: function(data) {
			//console.log(data);
            var self = this; 
            var counter = 0;
            var selector = self.typologyATypeFields[data.field];
            var fieldNamePrefix = self.typologyAFieldNames[data.field];
            var element = $(document).find('select[name="'+ selector +'"]');
            element.parent('.form-groupY').removeClass('col-md-6').addClass('col-md-12');
            var label = element.find('option:selected').val();
            element.find('option').not(':selected').remove();
            var deviceLabel = 'Number of Devices';
            var deviceName = 'devices';
            var isDevice = $.grep(data.value,function(arr){
                return (typeof arr.device !== 'undefined');
            });
            if(!isDevice.length) {
                deviceLabel = 'No. Of panels';
                deviceName = 'panels';
            }
            var html = '<div class="col-md-12" id="typologyATable">';
            html += '<h3>'+ label +'</h3>';
            html += '<table class="table">';
            html += '<thead>';
            html += '<tr>';
            html += '<th>S. No.</th>';
            html += '<th>Type of Asset</th>';
            html += '<th>Location</th>';
            html += '<th>'+ deviceLabel +'</th>';
            html += '<th>Total Area(in sq m)</th>';
            html += '<th>Latitude</th>';
            html += '<th>Longitude</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';
            $.each(data.value,function(index,val){
                html += '<tr>';
                html += '<td>'+ (counter + 1) +'</td>';
                html += '<td>'+ label +'</td>';
                html += '<td><input type="text" class="form-control typologyA_location" value="'+ val.location +'" name="'+ fieldNamePrefix +'location['+ counter +']"></td>';
                html += '<td><input type="text" class="form-control typologyA_panels" value="'+ (typeof val.device !== 'undefined' ? val.device :  val.panels) +'" name="'+ fieldNamePrefix + deviceName +'['+ counter +']"></td>';
                html += '<td><input type="text" class="form-control typologyA_area" value="'+ val.area +'" name="'+ fieldNamePrefix +'area['+ counter +']"></td>';
                html += '<td><input type="text" class="form-control typologyA_lat" value="'+ val.lat +'" name="'+ fieldNamePrefix +'lat['+ counter +']"></td>';
                html += '<td><input type="text" class="form-control typologyA_lng" value="'+ val.lng +'" name="'+ fieldNamePrefix +'lng['+ counter +']"></td>';
                html += '</tr>';
                counter++;
            });
            html += '</tbody>';
            html += '</table>';
            html += '</div>';
            element.parent('.form-groupY').after(html);
        },
        calculatePayble: function () {
            $(document).find('.error-message').remove();
            var concessionFees = $.trim($(document).find('input[name="concession_fees"]').val());
            if (!concessionFees) {
                return false;
            } else if (isNaN(concessionFees)) {
                $(document).find('input[name="concession_fees"]').after('<p class="error-message">Value must be numeric</p>');
                return false;
            }
            var concessionaireType = $.trim($(document).find('select[name="concessionaire_type"] option:selected').val());
            if (!concessionaireType) {
                return false;
            }
            var amount = concessionFees;
            if (concessionaireType !== 'MCG') {
                amount = (parseFloat(concessionFees) * 25) / 100;
            }
            $(document).find('input[name="annual_license_fees"]').val(amount);
        },
        getMetroMrtsTable: function(data) {
            var html = '';
            if((data.value.metro_mrts_categories).toLowerCase() === 'rolling stock') {
                var rollingStackType = $.map(data.value.data,function(arr){
                    return arr.metro_type;
                });
                $.each(rollingStackType,function(index, val){
                    html += '<input type="hidden" value="'+ (val.toLowerCase()).replace(" ", "_") +'" name="metro_type[]">';
                });
                html += '<div class="col-md-12" id="metro_type_check">';
                html += '<h4>Rolling Stock Type</h4>';
                html += '<div class="col-md-6">';
                html += '<label><input type="checkbox" value="inside_metro" disabled="disabled" '+ ($.inArray('Inside Metro',rollingStackType) !== -1 ? 'checked="checked"' : '') +'>Inside Metro</label>';
                html += '</div>';
                html += '<div class="col-md-6">';
                html += '<label><input type="checkbox" value="outside_metro" disabled="disabled" '+ ($.inArray('Outside Metro',rollingStackType) !== -1 ? 'checked="checked"' : '') +'>Outside Metro</label>';
                html += '</div>';
                html += '</div>';
                html += '<div id="metro_type_block" class="col-md-12">';
                $.each(data.value.data,function(index, val) {
                    if((val.metro_type).toLowerCase() === 'inside metro') {
                        html += '<div id="inside_metro_block">';
                        html += '<div id="inside_metro_section">';
                        html += '<h3>'+ val.metro_type +'</h3>';
                        html += '<table class="table">';
                        html += '<thead>';
                        html += '<tr>';
                        html += '<th>S. No</th>';
                        html += '<th>Train no.</th>';
                        html += '<th>No. Of coaches</th>';
                        html += '<th>No. Of Advertisement Panels</th>';
                        html += '</tr>';
                        html += '</thead>';
                        html += '<tbody>';
                        $.each(val.table,function(Inindex,Inval){
                            html += '<tr>';
                            html += '<td class="serial-no">'+ (Inindex + 1) +'</td>';
                            html += '<td><input type="text" class="form-control train_no" name="inside_metro_train['+ Inindex +']" value="'+ Inval.train_no +'"></td>';
                            html += '<td><input type="text" class="form-control metro_coches" name="inside_metro_coches['+ Inindex +']" value="'+ Inval.coches +'"></td>';
                            html += '<td><input type="text" class="form-control ad_panels" name="inside_metro_ad_panels['+ Inindex +']" value="'+ Inval.ad_panels +'"></td>';
                            html += '</tr>';
                        });
                        html += '</tbody>';
                        html += '</table>';
                        html += '</div>';
                        html += '</div>';
                    } else if((val.metro_type).toLowerCase() === 'outside metro'){
                        html += '<div id="outside_metro_block">';
                        html += '<div id="outside_metro_section">';
                        html += '<h3>'+ val.metro_type +'</h3>';
                        html += '<table class="table">';
                        html += '<thead>';
                        html += '<tr>';
                        html += '<th>S. No</th>';
                        html += '<th>Train no.</th>';
                        html += '<th>No. of coaches for display of advertisement</th>';
                        html += '</tr>';
                        html += '</thead>';
                        html += '<tbody>';
                        $.each(val.table,function(Outindex, Outval) {
                            html += '<tr>';
                            html += '<td class="serial-no">'+ (Outindex + 1) +'</td>';
                            html += '<td><input type="text" class="form-control train_no" name="outside_metro_train['+ Outindex +']" value="'+ Outval.train_no +'"></td>';
                            html += '<td><input type="text" class="form-control metro_coches" name="outside_metro_coches['+ Outindex +']" value="'+ Outval.coches +'"></td>';
                            html += '</tr>';
                        });
                        html += '</tbody>';
                        html += '</table>';
                        html += '</div>';
                        html += '</div>';
                    }
                });
                html += '</div>';
                $(document).find('input[name="location"], #map-canvas, input[name="lat"], input[name="lng"]').parent('div').hide();
            } else if((data.value.metro_mrts_categories).toLowerCase() === 'inside station'){
                html += '<div class="col-md-12" id="inside_station_section">';
                html += '<h3>Inside Station</h3>';
                html += '<table class="table">';
                html += '<thead>';
                html += '<tr>';
                html += '<th>S.no</th>';
                html += '<th>OMD Site</th>';
                html += '<th>Height(in m)</th>';
                html += '<th>Width(in m)</th>';
                html += '<th>Area(in sq. m)</th>';
                html += '</tr>';
                html += '</thead>';
                html += '<tbody>';
                $.each(data.value.data,function(index, val){
                    html += '<tr>';
                    html += '<td class="serial-no">'+ (index + 1) +'</td>';
                    html += '<td>';
                    html += '<select class="form-control" name="inside_station_omd_type['+ index +']">';
                    html += '<option value="">Select OMD Site</option>';
                    html += '<option value="Unipole" '+ (val.omd_type === 'Unipole' ? 'selected="selected"' : '') +'>Unipole</option>';
                    html += '<option value="Wall Wrap" '+ (val.omd_type === 'Wall Wrap' ? 'selected="selected"' : '') +'>Wall Wrap</option>';
                    html += '<option value="Wall Mounted" '+ (val.omd_type === 'Wall Mounted' ? 'selected="selected"' : '') +'>Wall Mounted</option>';
                    html += '<option value="Others" '+ (val.omd_type === 'Others' ? 'selected="selected"' : '') +'>Others</option>';
                    html += '</select>';
                    html += '</td>';
                    html += '<td><input type="text" class="form-control heightData" autocomplete="off" onkeyup="SubCategory.calculateArea(this)" name="inside_station_omd_height['+ index +']" value="'+ val.omd_height +'"></td>';
                    html += '<td><input type="text" class="form-control widthData" autocomplete="off" onkeyup="SubCategory.calculateArea(this)" name="inside_station_omd_width['+ index +']" value="'+ val.omd_width +'"></td>';
                    html += '<td><input type="text" class="form-control areaData" readonly="readonly" name="inside_station_omd_area['+ index +']" value="'+ val.omd_area +'"></td>';
                    html += '</tr>';
                });
                html += '</tbody>';
                html += '<tfoot>';
                html += '<tr>';
                html += '<td colspan="6"><label>Total Area(in sq. m)</lable><input type="text" class="form-control total_area" readonly="readonly" name="omd_one_roof_total_area" value="'+ data.value.total_area +'"></td>';
                html += '</tr>';
                html += '</tfoot>';
                html += '</table>';
                html += '</div>';
            } else if((data.value.metro_mrts_categories).toLowerCase() === 'station branding'){
                html += '<div class="col-md-12" id="station_branding_section">';
                html += '<h3>Station Branding</h3>';
                html += '<table class="table">';
                html += '<thead>';
                html += '<tr>';
                html += '<th>S. No</th>';
                html += '<th>Facade</th>';
                html += '<th>Total Facade area(in sq. m)</th>';
                html += '<th>Area of actual display(in sq. m)</th>';
                html += '<th></th>';
                html += '</tr>';
                html += '</thead>';
                html += '<tbody>';
                $.each(data.value.data,function(index, val){
                    html += '<tr>';
                    html += '<td class="serial-no">'+ (index + 1) +'</td>';
                    html += '<td class="facade-no">'+ val.facade +'</td>';
                    html += '<td><input type="text" class="form-control station_branding_facade" name="station_branding_total_facade['+ index +']" value="'+val.total_facade+'"></td>';
                    html += '<td><input type="text" class="form-control station_branding_actual_facade" name="station_branding_actual_facade['+ index +']" value="'+ val.actual_facade +'"></td>';
                    html += '</tr>';
                });
                html += '</tbody>';
                html += '</table>';
                html += '</div>';
            }
            $(document).find('#metro_mrts_categories_block').html(html);
        },
        getOMDTable: function(data) {
            var counters = {
                'omd_one_roof' : 'omdRoofCount',
                'omd_premises' : 'omdPremisesCount'
            };
            var addFunctions = {
                'omd_one_roof':'addUnderOneRoof',
                'omd_premises':'addUnderPremises'
            };
            console.log(data);
            var self = this;
            var title = OmdSitesTitles[data.field];
            var blockId = OmdSites[title.toLowerCase()];
            var omdData = data.value.data;
            var html = '<div class="col-md-12">';
            html += '<h3>' + title +'</h3>';
            html += '<table class="table">';
            html += '<thead>';
            html += '<tr>';
            html += '<th>S.no</th>';
            html += '<th>OMD Site</th>';
            html += '<th>Height(in m)</th>';
            html += '<th>Width(in m)</th>';
            html += '<th>Price(per sq. m)</th>';
            html += '<th>Area(in sq. m)</th>';
            html += '<th>Status</th>';
            html += '<th>Commnet</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';
            var counter = 1;
            $.each(omdData,function(index,valData){
				//console.log(valData);
                var readOnly = '';
                var areaDataClass = 'areaData';
                if(valData.action !== '2') {
                    readOnly = 'readonly="readonly"';
                }
                if(valData.action === '3') {
                    areaDataClass = '';
                }
                html += '<tr>';
                html += '<td>'+ counter +'</td>';
                html += '<td>';
                html += '<select class="form-control" name="'+ data.field +'_omd_type['+ self[counters[data.field]] +']" '+ readOnly +' onchange="SubCategory.calculateArea(this)">';
                html += '<option value="">Select OMD Site</option>';
                $.each(self.omdSiteData,function(index,val) {
                    html += '<option value="' + val +'" '+ (valData.type === val ? 'selected="selected"' : '') +'>'+ val +'</option>';
                });
                html += '</select>';
                html += '</td>';
                html += '<td><input type="text" class="form-control heightData" autocomplete="off" name="'+ data.field +'_height['+ self[counters[data.field]] +']" onkeyup="SubCategory.calculateArea(this)" value="'+ valData.height +'" '+ readOnly +'></td>';
                html += '<td><input type="text" class="form-control widthData" autocomplete="off" name="'+ data.field +'_width['+ self[counters[data.field]] +']" onkeyup="SubCategory.calculateArea(this)" value="'+ valData.width +'" '+ readOnly +'></td>';
                html += '<td><input type="text" class="form-control priceData" readonly="readonly" name="'+ data.field +'_price['+ self[counters[data.field]] +']" onkeyup="SubCategory.calculateArea(this)" value="'+ valData.price +'"></td>';
                html += '<td><input type="text" class="form-control '+ areaDataClass +'" readonly="readonly" name="'+ data.field +'_area['+ self[counters[data.field]] +']" value="'+ valData.area +'"></td>';
                html += '<td>'+ self.omdActions[valData.action] +'</td>';
                html += '<td>'+ valData.comment +'</td>';
                html += '</tr>';
                self[counters[data.field]]++;
                counter++;
            });
            html += '</tbody>';
            html += '<tfoot>';
            html += '<tr>';
            if(typeof data.value.licence_fees !== 'undefined') {
                html += '<td><label>Permission Fees(in Rs.)</lable><input type="text" class="form-control licence_fees" readonly="readonly" value="' + data.value.licence_fees + '" name="'+ data.field +'_licence_fees"></td>';
            }
            html += '<td colspan="3"><label>Total Area(in sq. m)</lable><input type="text" class="form-control total_area" readonly="readonly" name="'+ data.field +'_total_area" value="' + data.value.area + '"></td>';
            html += '<td colspan="5"><label>Total Permission Fees(in Rs.)</lable><input type="text" class="form-control total_annual_fees" readonly="readonly" name="'+ data.field +'_total_annual_fees" value="' + data.value.annual_fees + '"></td>';
            html += '</tr>';
            html += '</tfoot>';
            html += '</table>';
            html += '</div>';
            $('#'+blockId).html(html);
        },
        getHtml: function (data, panelId) {
            var self = this;
            var html = '<div class="form-step1 form-steps">';
            $.each(data, function (index, val) {
                if (val.group_in === '1') {
                    var field = self[val.type];
                    html += field(val, self);
                }
            });
            if (html) {
                html += self.next();
            }
            html += '</div>';
            html += '<div class="form-step2 form-steps" style="display: none;">';
            $.each(data, function (index, val) {
                if (val.group_in === '2') {
                    var field = self[val.type];
                    html += field(val, self);
                }
            });
            html += self.previous(1);
            html += self.preview();
            html += '</div>';
            html += '<div id="previewPanel" style="display: none;" class="col-md-12 form-step3 form-steps">';
            $.each(data, function (index, val) {
                if(Typology.typologyId === '10') {
                    if(val.field_key === 'annual_license_fees') {
                        html += '<div id="omds_installed_under_one_roof_preview"></div>';
                        html += '<div id="omds_installed_within_premises_preview"></div>';
                    }
                }
                html += '<div class="col-md-12">';
                if(val.field_key !== 'building_type[]') {
                    html += '<label class="col-md-6">' + val.lable + '</label><span class="col-md-6" id="p_' + val.field_key + '">--N/A--</span>';
                } else {
                    html += '<label class="col-md-6">' + val.lable + '</label><span class="col-md-6" id="p_building_type">--N/A--</span>';
                }
                html += '</div>';
                if (self.subCategoryId === 17) {
                    if (val.field_key === 'metro_mrts_categories') {
                        html += '<div id="metro_mrts_preview"></div>';
                    }
                }
                if(Typology.typologyId === '1') {
                    if($.inArray(val.field_key,['shelters_type','route_markers_type','foot_over_type','cycle_station_type','police_booth_type','sitting_bench_type']) !== -1){
                        html += '<div id="typologyA_table_preview"></div>';
                    }
                }
                if (self.subCategoryId === 32) {
                    if(val.field_key === 'omd_type_type_a') {
                        html += '<div id="typologyA_others_preview"></div>';
                    }
                }
            });
            html +='<div class="checkbox col-md-12">';
            html +='<label>';
            html +='<input type="checkbox" id="agreeOMD">';
            html +='<p class="termsc">I/we shall adhere to the <b class="orang">terms & conditions</b> of outdoor media byelaws/policy framed by the MCF/Govt. of Haryana. I/we confirm that the above information is true to the best of my/ our knowledge. In case there is any discrepancy in the information provided above, the application shall be rejected</p>';
            html +='</label>';
            //html +='<p style="color: #782669; font-style: italic;">*Disclaimer- This is a newly launched website by MCG. In case of any error/omission, MCG reserves the right to raise additional claims on the applicant.</p>';
            html +='</div>';
            html += self.previous(2);
            html += self.submit();
            html += '</div>';
            $('#' + panelId).html('');
            if (data.length > 0) {
                html += '<p style="color: #782669; font-style: italic; float: left; width: 100%;">*Disclaimer- This is a newly launched website by MCF. In case of any error/omission, MCF reserves the right to raise additional claims on the applicant.</p>';
                $('#' + panelId).html(html);
            }
        },
            getOmdForm: function(element) {
            var self = this;
            if($(element).is(':checked')) {
                var selectedOption = ($(element).val()).toLowerCase();
                var html = self[OmdSites[selectedOption] + '_html'];
                $(document).find('#'+OmdSites[selectedOption]).html(html(self));
                var totalAnnualFeesToPay = 0;
                var totalAnnualFeesRoof = 0;
                var totalAnnualFeesPremises = 0;
                if($.trim($('#omds_installed_under_one_roof table tfoot tr .total_annual_fees').val())) {
                    totalAnnualFeesRoof = parseFloat($('#omds_installed_under_one_roof table tfoot tr .total_annual_fees').val());
                }
                if($.trim($('#omds_installed_within_premises table tfoot tr .total_annual_fees').val())) {
                    totalAnnualFeesPremises = parseFloat($('#omds_installed_within_premises table tfoot tr .total_annual_fees').val());
                }
                $('#formData .form-groupY input[name="annual_license_fees"]').val('');
                totalAnnualFeesToPay = totalAnnualFeesRoof + totalAnnualFeesPremises;
                if(totalAnnualFeesToPay) {
                    $('#formData .form-groupY input[name="annual_license_fees"]').val(totalAnnualFeesToPay);
                }
            } else {
               var unSelectedOption = ($(element).val()).toLowerCase();
                $(document).find('#'+OmdSites[unSelectedOption]).html('');
                var totalAnnualFeesToPay = 0;
                var totalAnnualFeesRoof = 0;
                var totalAnnualFeesPremises = 0;
                if($.trim($('#omds_installed_under_one_roof table tfoot tr .total_annual_fees').val())) {
                    totalAnnualFeesRoof = parseFloat($('#omds_installed_under_one_roof table tfoot tr .total_annual_fees').val());
                }
                if($.trim($('#omds_installed_within_premises table tfoot tr .total_annual_fees').val())) {
                    totalAnnualFeesPremises = parseFloat($('#omds_installed_within_premises table tfoot tr .total_annual_fees').val());
                }
                $('#formData .form-groupY input[name="annual_license_fees"]').val('');
                totalAnnualFeesToPay = totalAnnualFeesRoof + totalAnnualFeesPremises;
                if(totalAnnualFeesToPay) {
                    $('#formData .form-groupY input[name="annual_license_fees"]').val(totalAnnualFeesToPay);
                }
            }
        },
        omds_installed_under_one_roof_html: function(self) {
            var html = '<div class="col-md-12">';
            html += '<h3>OMDS Installed Under One Roof</h3>';
            html += '<table class="table">';
            html += '<thead>';
            html += '<tr>';
            html += '<th>S.no</th>';
            html += '<th>OMD Site</th>';
            html += '<th>Height(in m)</th>';
            html += '<th>Width(in m)</th>';
            html += '<th>Area(in sq. m)</th>';
            html += '<th></th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';
            html += '<tr>';
            html += '<td>1</td>';
            html += '<td>';
            html += '<select class="form-control" name="omd_one_roof_omd_type['+ self.omdRoofCount +']">';
            html += '<option value="">Select OMD Site</option>';
            html += '<option value="Unipole">Unipole</option>';
            html += '<option value="Wall Wrap">Wall Wrap</option>';
            html += '<option value="Wall Mounted">Wall Mounted</option>';
            html += '<option value="Others">Others</option>';
            html += '</select>';
            html += '</td>';
            html += '<td><input type="text" class="form-control heightData" autocomplete="off" name="omd_one_roof_height['+ self.omdRoofCount +']" onkeyup="SubCategory.calculateArea(this)"></td>';
            html += '<td><input type="text" class="form-control widthData" autocomplete="off" name="omd_one_roof_width['+ self.omdRoofCount +']" onkeyup="SubCategory.calculateArea(this)"></td>';
            html += '<td><input type="text" class="form-control areaData" readonly="readonly" name="omd_one_roof_area['+ self.omdRoofCount +']"></td>';
            html += '<td><a href="javascript:void(0)" onclick="SubCategory.addUnderOneRoof(this)" class="plus-btn">+ Add More</a></td>';
            html += '</tr>';
            html += '</tbody>';
            html += '<tfoot>';
            html += '<tr>';
            html += '<td><label>Licence Fees(in Rs.)</lable><input type="text" class="form-control licence_fees" readonly="readonly" value="430" name="omd_one_roof_licence_fees"></td>';
            html += '<td><label>Total Area(in sq. m)</lable><input type="text" class="form-control total_area" readonly="readonly" name="omd_one_roof_total_area"></td>';
            html += '<td colspan="4"><label>Annual Total Licence Fees(in Rs.)</lable><input type="text" class="form-control total_annual_fees" readonly="readonly" name="omd_one_roof_total_annual_fees"></td>';
            html += '</tr>';
            html += '</tfoot>';
            html += '</table>';
            html += '</div>';
            return html;
        },
        omds_installed_within_premises_html: function(self){
            var html = '<div class="col-md-12">';
            html += '<h3>OMDS installed within premises but in open courtyard</h3>';
            html += '<table class="table">';
            html += '<thead>';
            html += '<tr>';
            html += '<th>S.no</th>';
            html += '<th>OMD Site</th>';
            html += '<th>Height(in m)</th>';
            html += '<th>Width(in m)</th>';
            html += '<th>Area(in sq. m)</th>';
            html += '<th></th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';
            html += '<tr>';
            html += '<td>1</td>';
            html += '<td>';
            html += '<select class="form-control" name="omd_premises_omd_type['+ self.omdPremisesCount +']">';
            html += '<option value="">Select OMD Site</option>';
            html += '<option value="Unipole">Unipole</option>';
            html += '<option value="Wall Wrap">Wall Wrap</option>';
            html += '<option value="Wall Mounted">Wall Mounted</option>';
            html += '<option value="Others">Others</option>';
            html += '</select>';
            html += '</td>';
            html += '<td><input type="text" class="form-control heightData" autocomplete="off" name="omd_premises_height['+ self.omdPremisesCount +']" onkeyup="SubCategory.calculateArea(this)"></td>';
            html += '<td><input type="text" class="form-control widthData" autocomplete="off" name="omd_premises_width['+ self.omdPremisesCount +']" onkeyup="SubCategory.calculateArea(this)"></td>';
            html += '<td><input type="text" class="form-control areaData" readonly="readonly" name="omd_premises_area['+ self.omdPremisesCount +']"></td>';
            html += '<td><a href="javascript:void(0)" onclick="SubCategory.addUnderPremises(this)" class="plus-btn">+ Add More</a></td>';
            html += '</tr>';
            html += '</tbody>';
            html += '<tfoot>';
            html += '<tr>';
            html += '<td><label>Licence Fees(in Rs.)</lable><input type="text" class="form-control licence_fees" readonly="readonly" value="645" name="omd_premises_licence_fees"></td>';
            html += '<td><label>Total Area(in sq. m)</lable><input type="text" class="form-control total_area" readonly="readonly" name="omd_premises_total_area"></td>';
            html += '<td colspan="4"><label>Annual Total Licence Fees(in Rs.)</lable><input type="text" class="form-control total_annual_fees" readonly="readonly" name="omd_premises_total_annual_fees"></td>';
            html += '</tr>';
            html += '</tfoot>';
            html += '</table>';
            html += '</div>';
            return html;
        },
        addUnderOneRoof: function(element) {
            var self = this;
            self.omdRoofCount++;
            var html = '<tr>';
            html += '<td>1</td>';
            html += '<td>';
            html += '<select class="form-control" name="omd_one_roof_omd_type['+ self.omdRoofCount +']">';
            html += '<option value="">Select OMD Site</option>';
            html += '<option value="Unipole">Unipole</option>';
            html += '<option value="Wall Wrap">Wall Wrap</option>';
            html += '<option value="Wall Mounted">Wall Mounted</option>';
            html += '<option value="Others">Others</option>';
            html += '</select>';
            html += '</td>';
            html += '<td><input type="text" class="form-control heightData" autocomplete="off" name="omd_one_roof_height['+ self.omdRoofCount +']" onkeyup="SubCategory.calculateArea(this)"></td>';
            html += '<td><input type="text" class="form-control widthData" autocomplete="off" name="omd_one_roof_width['+ self.omdRoofCount +']" onkeyup="SubCategory.calculateArea(this)"></td>';
            html += '<td><input type="text" class="form-control areaData" readonly="readonly" name="omd_one_roof_area['+ self.omdRoofCount +']"></td>';
            if($(document).find('#omds_installed_under_one_roof tbody tr').length < 9) {
                html += '<td><a href="javascript:void(0)" onclick="SubCategory.addUnderOneRoof(this)" class="plus-btn">+ Add More</a></td>';
            } else {
                html += '<td><a href="javascript:void(0)" onclick="SubCategory.removeOmd(this)" class="close-btn">X</a></td>';
            }
            html += '</tr>';
            $(element).attr('onclick','SubCategory.removeOmd(this)');
            $(element).html('X');
            $(element).removeClass('plus-btn');
            $(element).addClass('close-btn');
            $('#omds_installed_under_one_roof tbody').append(html);
            self.omdSerailNos();
        },
        addUnderPremises: function(element) {
            var self = this;
            self.omdPremisesCount++;
            var html = '<tr>';
            html += '<th>S.no</th>';
            html += '<th>OMD Site</th>';
            html += '<th>Height</th>';
            html += '<th>Width</th>';
            html += '<th>Area</th>';
            html += '<th></th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';
            html += '<tr>';
            html += '<td>1</td>';
            html += '<td>';
            html += '<select class="form-control" name="omd_premises_omd_type['+ self.omdPremisesCount +']">';
            html += '<option value="">Select OMD Site</option>';
            html += '<option value="Unipole">Unipole</option>';
            html += '<option value="Wall Wrap">Wall Wrap</option>';
            html += '<option value="Wall Mounted">Wall Mounted</option>';
            html += '<option value="Others">Others</option>';
            html += '</select>';
            html += '</td>';
            html += '<td><input type="text" class="form-control heightData" autocomplete="off" name="omd_premises_height['+ self.omdPremisesCount +']" onkeyup="SubCategory.calculateArea(this)"></td>';
            html += '<td><input type="text" class="form-control widthData" autocomplete="off" name="omd_premises_width['+ self.omdPremisesCount +']" onkeyup="SubCategory.calculateArea(this)"></td>';
            html += '<td><input type="text" class="form-control areaData" readonly="readonly" name="omd_premises_area['+ self.omdPremisesCount +']"></td>';
            if($(document).find('#omds_installed_within_premises tbody tr').length < 9) {
                html += '<td><a href="javascript:void(0)" onclick="SubCategory.addUnderPremises(this)" class="plus-btn">+ Add More</a></td>';
            } else {
                html += '<td><a href="javascript:void(0)" onclick="SubCategory.removeOmd(this)" class="close-btn">X</a></td>';
            }
            html += '</tr>';
            $(element).attr('onclick','SubCategory.removeOmd(this)');
            $(element).html('X');
            $(element).removeClass('plus-btn');
            $(element).addClass('close-btn');
            $('#omds_installed_within_premises tbody').append(html);
            self.omdSerailNos();
        },
        removeOmd: function(element) {
            var tbody = $(element).parent('td').parent('tr').parent('tbody');
            var tableParentId = tbody.parent('table').parent('div').parent('div').attr('id');
            $(element).parent('td').parent('tr').remove();
            if(tableParentId === 'omds_installed_under_one_roof') {
                tbody.find('tr:last-of-type td:last-of-type').html('<a href="javascript:void(0)" onclick="SubCategory.addUnderOneRoof(this)" class="plus-btn">+ Add More</a>');
            } else {
               tbody.find('tr:last-of-type td:last-of-type').html('<a href="javascript:void(0)" onclick="SubCategory.addUnderPremises(this)" class="plus-btn">+ Add More</a>');
            }
            this.omdSerailNos();
        },
        omdSerailNos: function() {
            var sNo = 1;
            $(document).find('#omds_installed_under_one_roof table tbody tr').each(function() {
                $(this).find('td:first-of-type').html(sNo);
                sNo++;
            });
            sNo = 1;
            $(document).find('#omds_installed_within_premises table tbody tr').each(function() {
                $(this).find('td:first-of-type').html(sNo);
                sNo++;
            });
        },
        calculateArea: function (element) {
            var self = this;
            var input = $(element);
            var formIds = Object.keys(self.typeIPrice);
            var parentBlock = input.parents('#'+formIds.join(', #'));
            var currentParent = parentBlock.attr('id');
            var isSelect = input.is('select');
            var inputValue = input.val();
            if (inputValue && !isSelect) {
                if (!$.isNumeric(inputValue)) {
                    var inputValue = parseFloat(inputValue);
                    if (isNaN(inputValue)) {
                        inputValue = '';
                    }
                    input.val(inputValue);
                }
            }
            var rowPrice = 0;
            var totalArea = 0;
            var area = 0;
            var height = 0;
            var width = 0;
            var licence_fees = 0;
            var parentTable = $(element).parent('td').parent('tr').parent('tbody').parent('table');
            var parentTableBody = $(element).parent('td').parent('tr').parent('tbody');
            var parentRow = $(element).parent('td').parent('tr');
            if ($.isNumeric(parentRow.find('.heightData').val())) {
                height = parentRow.find('.heightData').val();
            }
            if ($.isNumeric(parentRow.find('.widthData').val())) {
                width = parentRow.find('.widthData').val();
            }
            area = height * width;
            parentRow.find('.areaData').val('');
            if (area) {
                parentRow.find('.areaData').val(area);
            }
            if(isSelect) {
                parentRow.find('.priceData').val('');
                var selectedOmd = input.find('option:selected').val();
                if(selectedOmd) {
                    rowPrice = self.typeIPrice[currentParent] * self.typeIMultiple[selectedOmd];
                    parentRow.find('.priceData').val(rowPrice);
                }
            }
            parentTableBody.find('tr').each(function () {
                var selectedOmdOtion = $(this).find('select option:selected').val();
                var omdHeight = $.trim($(this).find('.heightData').val());
                var omdWidth = $.trim($(this).find('.widthData').val());
                if ($.isNumeric($(this).find('.areaData').val())) {
                    totalArea += parseFloat($(this).find('.areaData').val());
                }
                if(selectedOmdOtion && omdHeight && omdWidth) {
                    var price = self.typeIPrice[currentParent] * self.typeIMultiple[selectedOmdOtion];
                    var area = parseFloat(omdHeight) * parseFloat(omdWidth);
                    licence_fees += (price * area);
                }
            });
            parentTable.find('tfoot .licence_fees').val('');
            parentTable.find('tfoot .total_area').val('');
            parentTable.find('tfoot .total_annual_fees').val('');
            if (totalArea) {
                parentTable.find('tfoot .total_area').val(totalArea);
    //            var total_annual_fees = licence_fees * totalArea;
    //            parentTable.find('tfoot .total_annual_fees').val(total_annual_fees);
            }
            if(licence_fees) {
    //            parentTable.find('tfoot .licence_fees').val(licence_fees);
                parentTable.find('tfoot .total_annual_fees').val(licence_fees);
            }
            var totalAnnualFeesToPay = 0;
            var totalAnnualFeesRoof = 0;
            var totalAnnualFeesPremises = 0;
            if ($.trim($('#omds_installed_under_one_roof table tfoot tr .total_annual_fees').val())) {
                totalAnnualFeesRoof = parseFloat($('#omds_installed_under_one_roof table tfoot tr .total_annual_fees').val());
            }
            if ($.trim($('#omds_installed_within_premises table tfoot tr .total_annual_fees').val())) {
                totalAnnualFeesPremises = parseFloat($('#omds_installed_within_premises table tfoot tr .total_annual_fees').val());
            }
            $('#formData .form-groupY input[name="total_permission_fees"]').val('');
            var tenure = 1;
            if ($.trim($(document).find('input[name="tenure"]').val())) {
                tenure = $.trim($(document).find('input[name="tenure"]').val());
            }
            totalAnnualFeesToPay = totalAnnualFeesRoof + totalAnnualFeesPremises;
            if (totalAnnualFeesToPay) {
                var result = totalAnnualFeesToPay * tenure;
                setTimeout(function () {
                    var discount = $(document).find('input[name="discount"]').val();
                    if ($.trim(discount)) {
                        var discountAmount = (parseFloat(discount) / 100) * result;
                        result = result - discountAmount;
                    }
                    $('#formData .form-groupY input[name="total_permission_fees"]').val(result);
                }, 200);
            }
        },
        calculateOtherArea: function(element) {
            var parentRow = $(element).parents('tr');
            var height = $.trim(parentRow.find('.other_type_a_height').val());
            var width = $.trim(parentRow.find('.other_type_a_width').val());
            if (height && width && !isNaN(height) && !isNaN(width)) {
                var area = parseFloat(height) * parseFloat(width);
                parentRow.find('.other_type_a_area').val(area);
            } else {
                parentRow.find('.other_type_a_area').val('');
            }
        },
        clearOMDSizeFieldsValidations: function (element) {
            var i;
            for (i = 1; i <= 6; i++) {
                this.defaultValidations[element.attr('name') + '_size_' + i + '-input-1'] = "";
                this.defaultValidations[element.attr('name') + '_location_' + i + '-input-1'] = "";
            }
        },
        getOMDSizeFields: function (element) {
            this.clearOMDSizeFieldsValidations(element);
             //================= add by rohit ================== 
			var class1 = '';
			if (element.attr('name') === 'omd_no_venue') {
				class1 = 'omd_no_venue_class';
			} else if(element.attr('name') === 'omd_no_others') {
				class1 = 'omd_no_others_class';
			}
			//================================================
            $(document).find('#' + 'omd_size_' + element.attr('name')).remove();
            var i;
            var addLocation = 0;
            var html = '<div id="omd_size_' + element.attr('name') + '">';
            var placeHolder = '13.5 sq. m ( 4 X 3.5 )';
            var LocationPlaceHolder = 'Location';
            if (element.attr('name') === 'omd_no_others') {
                placeHolder = '13.5 sq. m ( 4 X 3.5 )';
                addLocation = 1;
            }
            
            //============== add by rohit ==================
			for (i = 1; i <= 20; i++) {
				if(typeof this.defaultValidations[element.attr('name') + '_size_' + i + '-input-1'] != 'undefined'){
					this.defaultValidations[element.attr('name') + '_size_' + i + '-input-1'] = '';
				} 
				if(typeof this.defaultValidations[element.attr('name') + '_location_' + i + '-input-1'] != 'undefined'){
					this.defaultValidations[element.attr('name') + '_location_' + i + '-input-1'] = '';
				} 
			}
            
            for (i = 1; i <= element.val(); i++) {
                this.defaultValidations[element.attr('name') + '_size_' + i + '-input-1'] = '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}';
                html += '<div class="col-md-'+ (addLocation ? '6' : '12') +'">';
                html += '<label>OMD ' + i + ' Size</label>';
                html += '<input type="text" name="' + element.attr('name') + '_size_' + i + '" class="form-control omd-size-meters ' + element.attr('name') + ' '+class1+'" autocomplete="off" placeholder="' + placeHolder + '">';
                html += '</div>';
                if(addLocation) {
                    this.defaultValidations[element.attr('name') + '_location_' + i + '-input-1'] = '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}';
                    html += '<div class="col-md-6">';
                    html += '<label>OMD ' + i + ' Location</label>';
                    html += '<input type="text" name="' + element.attr('name') + '_location_' + i + '" class="form-control omd-location-meters" autocomplete="off" placeholder="OMD '+ i + ' ' +LocationPlaceHolder + '">';
                    html += '</div>';
                }
            }
            html += '</div>';
            element.after(html);
        },
        getPreviousForm: function (stepNumber) {
            $(document).find('.form-steps').hide();
            $(document).find('.form-step' + stepNumber).show();
            $('.stepsblk ul li').removeClass('active');
            $('.stepsblk ul li:eq(' + (stepNumber - 1) + ')').addClass('active');
        },
        next: function () {
            var html = '<input type="button" value="Next" id="formPreview" class="btn btn-default" style="float: left; clear: both; width: 18%;">';
            return html;
        },
        previous: function (formNumber) {
            var html = '<input type="button" value="Edit" class="btn btn-default" style="float: left; clear: both; width: 18%;" onclick="SubCategory.getPreviousForm(' + formNumber + ')">';
            return html;
        },
        preview: function () {
            var html = '<input type="button" value="Preview" class="btn btn-default" style="float: left; clear: both; width: 18%;" onclick="SubCategory.getPreview()">';
            return html;
        },
        text: function (data, self) {
			
            self.defaultValidations[data.field_key + '-input-' + data.group_in] = data.default_validations;
            self.customValidations[data.field_key] = data.custom_validation;
            var readonly = '';
            if (data.is_disabled === '1') {
                readonly = 'readonly="readonly"';
            }
            if (data.custom_disable === '1') {
                readonly = 'readonly="readonly"';
            }

            if (data.custom_calculations) {
				console.log(data.field_key);
				console.log(data.custom_calculations);
                self.customCalculations(data.field_key, JSON.parse(data.custom_calculations));
            }
            var html = '<div class="col-md-6 form-groupY">';
            if (data.field_key === 'location') {
                html = '<div class="col-md-12 form-groupY">';
            }
            html += '<label>' + data.lable + '</label>';
            html += '<input class="form-control" type="text" name="' + data.field_key + '" ' + readonly + ' autocomplete="off" placeholder="' + data.lable + '" tabindex="-1">';
            html += '</div>';
            if (data.field_key === 'location') {
                html += '<div class="col-md-12">';
                html += '<div id="map-canvas" style="height: 400px;"></div>';
                html += '</div>';
            }
            return html;
        },
        radio: function (data, self) {
            self.defaultValidations[data.field_key + '-input-' + data.group_in] = data.default_validations;
            self.customValidations[data.field_key] = data.custom_validation;
            var options = JSON.parse(data.options);
            var html = '<div class="col-md-6 form-groupY">';
            html += '<div class="form-group">';
            html += '<label>' + data.lable + '</label>';
            html += '<div class="radiobtn">';
            $.each(options, function (index, val) {
                html += '<label><input type="radio" name="' + data.field_key + '" value="' + val + '"><p>' + val + '</p></label>';
            });
            html += '</div>';
            html += '</div>';
            html += '</div>';
            return html;
        },
        select: function (data, self) {
            if(data.custom_calculations) {
                var custom_calculations = JSON.parse(data.custom_calculations);
                if(custom_calculations.fields.length) {
                    self.selectDynData[data.field_key] = {};
                    self.selectDynData[data.field_key][custom_calculations.fields[0]] = custom_calculations.options;
                }
            }
            var readonly = '';
            var bootClass = 'col-md-6 ';
            if(data.field_key === 'zone') {
                readonly = 'readonly="readonly"';
            }
            self.defaultValidations[data.field_key + '-select-' + data.group_in] = data.default_validations;
            self.customValidations[data.field_key] = data.custom_validation;
            var options = JSON.parse(data.options);
            if (self.subCategoryId === 17 && data.field_key === 'metro_mrts_categories') {
                bootClass = 'col-md-12 ';
                readonly = 'readonly="readonly"';
            }
            var html = '<div class="'+ bootClass +' form-groupY">';
            html += '<label>' + data.lable + '</label>';
            html += '<select class="form-control" name="' + data.field_key + '" '+ readonly +'>';
            html += '<option value="">Select ' + data.lable + '</option>';
            $.each(options, function (index, val) {
                html += '<option value="' + val + '">' + val + '</option>';
            });
            html += '</select>';
            html += '</div>';
            if (self.subCategoryId === 17 && data.field_key === 'metro_mrts_categories') {
                html += '<div id="metro_mrts_categories_block" class="form-groupY"></div>';
            }
            if (data.custom_calculations) {
                self.customCalculations(data.field_key, JSON.parse(data.custom_calculations));
            }
            return html;
        },
        date: function(data, self) {
            self.defaultValidations[data.field_key + '-input-' + data.group_in] = data.default_validations;
            self.customValidations[data.field_key] = data.custom_validation;
            var html = '<div class="col-md-6 form-groupY">';
            html += '<label>' + data.lable + '</label>';
            html += '<input class="form-control datePicker" type="text" name="' + data.field_key + '" readonly="readonly" autocomplete="off" placeholder="' + data.lable + '">';
            html += '</div>';
            return html;
        },
        checkbox: function(data, self) {
            var operation = '';
            self.defaultValidations[data.field_key + '-input-' + data.group_in] = data.default_validations;
            if(Typology.typologyId === '10') {
                operation = 'onchange="SubCategory.getOmdForm(this);"';
            }
            var html = '<div class="form-groupY col-md-12">';
            html += '<label style="float: left; width: 100%;">' + data.lable + '</label>';
            var options = JSON.parse(data.options);
            $.each(options,function(index,val){
                html += '<div class="col-md-6">';
                html += '<label><input name="'+ data.field_key +'" onclick="return false;" type="checkbox" value="' + val + '" '+ operation +'>'+ val +'<label>';
                html += '</div>';
            });
            html += '</div>';
            if(Typology.typologyId === '10') {
                if(data.field_key === 'building_type[]') {
                    html += '<div id="omds_installed_under_one_roof"></div>';
                    html += '<div id="omds_installed_within_premises"></div>';
                }
            }
            return html;
        },
        file: function (data, self) {
            self.defaultValidations[data.field_key + '-input-' + data.group_in] = data.default_validations;
            self.customValidations[data.field_key] = data.custom_validation;
            var html = '<div class="col-md-6 form-groupY">';
            html += '<div class="form-group">';
            html += '<label>' + data.lable + '<a href="javascript:void(0)" data-toggle="tooltip" title="' + data.info + '" class="infoTool">?</a></label>';
            html += '<input class="fileup deviceImageUpload" type="file" name="' + data.field_key + '">';
            html += '</div>';
            html += '</div>';
            return html;
        },
        hidden: function (data) {
            var html = '<input type="hidden" name="' + data.field_key + '">';
            return html;
        },
        submit: function () {
            var html = '<input type="submit" value="Submit" class="btn btn-default" style="float: left; clear: both; width: 18%;" id="submitOmdApp">';
            return html;
        },
        checkValidationStep1: function () {
            var self = this;
            var error = 0;
            if($(document).find('#omds_installed_under_one_roof').children().length) {
                $(document).find('#omds_installed_under_one_roof table tbody tr').each(function(){
                    var select = $(this).find('select');
                    var height = $(this).find('input[type="text"].heightData');
                    var width = $(this).find('input[type="text"].widthData');
                    var area = $(this).find('input[type="text"].areaData');
                    if(!select.find('option:selected').val()) {
                        select.after('<p class="error-message">This field is required</p>');
                        error = 1;
                    }
                    if(!$.trim(height.val())) {
                        height.after('<p class="error-message">This field is required</p>');
                        error = 1;
                    }
                    if(!$.trim(width.val())) {
                        width.after('<p class="error-message">This field is required</p>');
                        error = 1;
                    }
                    if(area.length) {
                        if(!$.trim(area.val())) {
                            area.after('<p class="error-message">This field is required</p>');
                            error = 1;
                        }
                    }
                });
            }
            if($(document).find('#omds_installed_within_premises').children().length) {
                $(document).find('#omds_installed_within_premises table tbody tr').each(function(){
                    var select = $(this).find('select');
                    var height = $(this).find('input[type="text"].heightData');
                    var width = $(this).find('input[type="text"].widthData');
                    var area = $(this).find('input[type="text"].areaData');
                    if(!select.find('option:selected').val()) {
                        select.after('<p class="error-message">This field is required</p>');
                        error = 1;
                    }
                    if(!$.trim(height.val())) {
                        height.after('<p class="error-message">This field is required</p>');
                        error = 1;
                    }
                    if(!$.trim(width.val())) {
                        width.after('<p class="error-message">This field is required</p>');
                        error = 1;
                    }
                    if(area.length) {   
                        if(!$.trim(area.val())) {
                            area.after('<p class="error-message">This field is required</p>');
                            error = 1;
                        }
                    }
                });
            }
            
            /*******************************/
        /****Metro/MRTS Validations*****/
        /*******************************/
        if ($(document).find('#metro_type_check').length) {
            if (!$(document).find('#metro_type_check input[type="checkbox"]').is(':checked')) {
                $(document).find('#metro_type_check').append('<p class="error-message">This field is required</p>');
                error = 1;
            }
        }
        if ($(document).find('#inside_metro_section').children().length) {
            $(document).find('#inside_metro_section table tbody tr').each(function () {
                var train_no = $.trim($(this).find('.train_no').val());
                var metro_coches = $.trim($(this).find('.metro_coches').val());
                var ad_panels = $.trim($(this).find('.ad_panels').val());
                if (!train_no) {
                    $(this).find('.train_no').after('<p class="error-message">This field is required</p>');
                    error = 1;
                }
                if (!metro_coches) {
                    $(this).find('.metro_coches').after('<p class="error-message">This field is required</p>');
                    error = 1;
                } else if (isNaN(parseFloat(metro_coches))) {
                    $(this).find('.metro_coches').after('<p class="error-message">Value must be numeric</p>');
                    error = 1;
                }
                if (!ad_panels) {
                    $(this).find('.ad_panels').after('<p class="error-message">This field is required</p>');
                    error = 1;
                } else if (isNaN(parseFloat(ad_panels))) {
                    $(this).find('.ad_panels').after('<p class="error-message">Value must be numeric</p>');
                    error = 1;
                }
            });
        }

        if ($(document).find('#outside_metro_section').children().length) {
            $(document).find('#outside_metro_section table tbody tr').each(function () {
                var train_no = $.trim($(this).find('.train_no').val());
                var metro_coches = $.trim($(this).find('.metro_coches').val());
                if (!train_no) {
                    $(this).find('.train_no').after('<p class="error-message">This field is required</p>');
                    error = 1;
                }
                if (!metro_coches) {
                    $(this).find('.metro_coches').after('<p class="error-message">This field is required</p>');
                    error = 1;
                } else if (isNaN(parseFloat(metro_coches))) {
                    $(this).find('.metro_coches').after('<p class="error-message">Value must be numeric</p>');
                    error = 1;
                }
            });
        }

        if ($(document).find('#inside_station_section').length) {
            $(document).find('#inside_station_section table tbody tr').each(function () {
                var select = $(this).find('select');
                var height = $(this).find('input[type="text"].heightData');
                var width = $(this).find('input[type="text"].widthData');
                var area = $(this).find('input[type="text"].areaData');
                if (!select.find('option:selected').val()) {
                    select.after('<p class="error-message">This field is required</p>');
                    error = 1;
                }
                if (!$.trim(height.val())) {
                    height.after('<p class="error-message">This field is required</p>');
                    error = 1;
                }
                if (!$.trim(width.val())) {
                    width.after('<p class="error-message">This field is required</p>');
                    error = 1;
                }
                if (!$.trim(area.val())) {
                    area.after('<p class="error-message">This field is required</p>');
                    error = 1;
                }
            });
        }

        if ($(document).find('#station_branding_section').length) {
            $(document).find('#station_branding_section table tbody tr').each(function () {
                var station_branding_facade = $.trim($(this).find('.station_branding_facade').val());
                var station_branding_actual_facade = $.trim($(this).find('.station_branding_actual_facade').val());
                if (!station_branding_facade) {
                    error = 1;
                    $(this).find('.station_branding_facade').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(parseFloat(station_branding_facade))) {
                    error = 1;
                    $(this).find('.station_branding_facade').after('<p class="error-message">Value must be numeric</p>');
                }
                if (!station_branding_actual_facade) {
                    error = 1;
                    $(this).find('.station_branding_actual_facade').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(parseFloat(station_branding_actual_facade))) {
                    error = 1;
                    $(this).find('.station_branding_actual_facade').after('<p class="error-message">Value must be numeric</p>');
                } else if (station_branding_facade) {
                    var actualValueLimit = (parseFloat(station_branding_facade) * 75) / 100;
                    if (parseFloat(station_branding_actual_facade) > actualValueLimit) {
                        error = 1;
                        $(this).find('.station_branding_actual_facade').after('<p class="error-message">Value must not be greater than ' + actualValueLimit + '</p>');
                    }
                }
            });
        }
        
        if($(document).find('#typologyATable').length) {
            $(document).find('#typologyATable table tbody tr').each(function(){
                var locationElement = $(this).find('.typologyA_location');
                var panelsElement = $(this).find('.typologyA_panels');
                var areaElement = $(this).find('.typologyA_area');
                var latElement = $(this).find('.typologyA_lat');
                var lngElement = $(this).find('.typologyA_lng');
                if(!$.trim(locationElement.val())) {
                    error = 1;
                    locationElement.after('<p class="error-message">This field is required</p>');
                }
                if(!$.trim(panelsElement.val())) {
                    error = 1;
                    panelsElement.after('<p class="error-message">This field is required</p>');
                }
                if(!$.trim(areaElement.val())) {
                    error = 1;
                    areaElement.after('<p class="error-message">This field is required</p>');
                }
                if(!$.trim(latElement.val())) {
                    error = 1;
                    latElement.after('<p class="error-message">This field is required</p>');
                }
                if(!$.trim(lngElement.val())) {
                    error = 1;
                    lngElement.after('<p class="error-message">This field is required</p>');
                }
            });
        }
        
        if($(document).find('#typologyATableOthers').length) {
            $(document).find('#typologyATableOthers table tbody tr').each(function(){
                var otherLocation = $.trim($(this).find('.other_type_a_location').val());
                var otherHeight = $.trim($(this).find('.other_type_a_height').val());
                var otherWidth = $.trim($(this).find('.other_type_a_width').val());
                var otherArea = $.trim($(this).find('.other_type_a_area').val());
                var otherLat = $.trim($(this).find('.other_type_a_lat').val());
                var otherLng = $.trim($(this).find('.other_type_a_lng').val());
                if(!otherLocation) {
                    error = 1;
                    $(this).find('.other_type_a_location').after('<p class="error-message">This field is required</p>');
                }
                if(!otherHeight) {
                    error = 1;
                    $(this).find('.other_type_a_height').after('<p class="error-message">This field is required</p>');
                } else if(isNaN(otherHeight)) {
                    error = 1;
                    $(this).find('.other_type_a_height').after('<p class="error-message">Value must be numeric</p>');
                }
                if(!otherWidth) {
                    error = 1;
                    $(this).find('.other_type_a_width').after('<p class="error-message">This field is required</p>');
                } else if(isNaN(otherWidth)) {
                    error = 1;
                    $(this).find('.other_type_a_width').after('<p class="error-message">Value must be numeric</p>');
                }
                if(!otherArea) {
                    error = 1;
                    $(this).find('.other_type_a_area').after('<p class="error-message">This field is required</p>');
                }
                if(!otherLat) {
                    error = 1;
                    $(this).find('.other_type_a_lat').after('<p class="error-message">This field is required</p>');
                }
                if(!otherLng) {
                    error = 1;
                    $(this).find('.other_type_a_lng').after('<p class="error-message">This field is required</p>');
                }
            });
        }
            
            
            $.each(self.customValidations, function (field, val) {
                if (val) {
                    var customValidation = JSON.parse(val);
                    $.each(customValidation, function (key, validationVal) {
                        var validate = self[key];
                        var element = $('[name="' + field + '"]');
                        if (validate(validationVal, element)) {
                            error = 1;
                            return false;
                        }
                        ;
                    });
                }
            });
            $.each(self.defaultValidations, function (field, val) {
                if (val) {
                    var fieldData = field.split('-');
                    if (fieldData[2] === '1') {
                        var element = $(fieldData[1] + '[name="' + fieldData[0] + '"]');
                        var validation = JSON.parse(val);
                        $.each(validation, function (key, validationVal) {
                            var validate = self[key];
                            if (validate(validationVal, element)) {
                                error = 1;
                                return false;
                            }
                        });
                    }
                }
            });
            if ($(document).find('.error-message').length) {
                var body = $("html, body");
                body.stop().animate({scrollTop: ($(document).find('.error-message').offset().top - 50)}, 500);
            }
            return error;
        },
        checkValidationStep2: function () {
            var self = this;
            var error = 0;
            $.each(self.defaultValidations, function (field, val) {
                if (val) {
                    var fieldData = field.split('-');
                    if (fieldData[2] === '2') {
                        var element = $(fieldData[1] + '[name="' + fieldData[0] + '"]');
                        var validation = JSON.parse(val);
                        $.each(validation, function (key, validationVal) {
                            var validateStep2 = self[key];
                            if (validateStep2(validationVal, element)) {
                                error = 1;
                                return false;
                            }
                        });
                    }
                }
            });
            if ($(document).find('.error-message').length) {
                var body = $("html, body");
                body.stop().animate({scrollTop: ($(document).find('.error-message').offset().top - 50)}, 500);
            }
            return error;
        },
        moveStep2: function () {
            $(document).find('.error-message').remove();
            if (!this.checkValidationStep1()) {
                $('.stepsblk ul li').removeClass('active');
                $('.stepsblk ul li:eq(1)').addClass('active');
                $(document).find('.form-step1').hide();
                $(document).find('.form-step2').show();
            }
        },
        getPreview: function () {
            $(document).find('.error-message').remove();
            if (!this.checkValidationStep2()) {
                $('.stepsblk ul li').removeClass('active');
                $('.stepsblk ul li:eq(2)').addClass('active');
                this.showPreview();
            }
        },
        showPreview: function () {
            var data = this.subCategoryLocalData[this.subCategoryId];
            $.each(data, function (index, val) {
                var checked = '';
                if ($('[name="' + val.field_key + '"]').attr('type') === 'radio') {
                    checked = ':checked';
                }
                if ($('[name="' + val.field_key + '"]' + checked).val()) {
                    if(val.field_key !== 'building_type[]') {
                        $('#p_' + val.field_key).html($('[name="' + val.field_key + '"]' + checked).val());
                    } else {
                        var selectedOMDS = [];
                        $('[name="' + val.field_key + '"]:checked').each(function() {
                            selectedOMDS.push($(this).val());
                        });
                        $('#p_building_type').html(selectedOMDS.join(', '));
                    }
                }
            });
            this.omdPreviewHtml();
            this.mertoMrtsPreviewHtml();
            this.typologyAPreview();
            this.typologyAOtherPreview();
            $('.form-step2').hide();
            $('#previewPanel').show();
        },
        typologyAOtherPreview: function() {
            var tableElement = $(document).find('#typologyATableOthers');
            $(document).find('#typologyA_others_preview').html('');
            if(tableElement.length){
                var label = tableElement.find('h3').text();
                var html = '<h4>'+ label +'</h4>';
                html += '<table class="table">';
                html += '<thead>';
                html += '<tr>';
                tableElement.find('table thead tr th').each(function() {
                    html += '<th>'+ $(this).html() +'</th>';
                });
                html += '</tr>';
                html += '</thead>';
                html += '<tbody>';
                tableElement.find('table tbody tr').each(function(){
                    html += '<tr>';
                    $(this).find('td').each(function(){
                        if(!$(this).find('input').length) {
                            html += '<td>'+ $(this).html() +'</td>';
                        } else {
                            html += '<td>'+ $(this).find('input').val() +'</td>';
                        }
                    });
                    html += '</tr>';
                });
                html += '</tbody>';
                html += '</table>';
                $(document).find('#typologyA_others_preview').html(html);
            }
        },
        typologyAPreview: function(){
            var tableElement = $(document).find('#typologyATable');
            $(document).find('#typologyA_table_preview').html('');
            if(tableElement.length) {
                var html = '<h4>'+ tableElement.find('h3').html() +'</h4>';
                html += '<table class="table">';
                html += '<thead>';
                html += '<tr>';
                tableElement.find('table thead tr th').each(function() {
                    html += '<th>'+ $(this).html() +'</th>';
                });
                html += '</tr>';
                html += '</thead>';
                html += '<tbody>';
                tableElement.find('table tbody tr').each(function(){
                    html += '<tr>';
                    $(this).find('td').each(function(){
                        if(!$(this).find('input').length) {
                            html += '<td>'+ $(this).html() +'</td>';
                        } else {
                            html += '<td>'+ $(this).find('input').val() +'</td>';
                        }
                    });
                    html += '</tr>';
                });
                html += '</tbody>';
                html += '</table>';
                $(document).find('#typologyA_table_preview').html(html);
            }
        },
        mertoMrtsPreviewHtml: function () {
            var html = '';
            var sno = 1;
            if ($(document).find('#metro_type_check').length) {
                var metroTypeCheck = [];
                $(document).find('#metro_type_check div.col-md-6').each(function () {
                    var checkbox = $(this).find('input[type="checkbox"]');
                    if (checkbox.is(':checked')) {
                        metroTypeCheck.push(checkbox.parent('label').text());
                    }
                });
                if (metroTypeCheck.length) {
                    html += '<div class="col-md-12">';
                    html += '<label class="col-md-6">Rolling Stock Type</label>';
                    html += '<span class="col-md-6">' + metroTypeCheck.join(', ') + '</span>';
                    html += '</div>';
                }
            }
            if ($(document).find('#inside_metro_section').length) {
                html += '<div class="col-md-12">';
                html += '<h3>Inside Metro</h3>';
                html += '<table class="table">';
                html += '<thead>';
                html += '<tr>';
                html += '<th>S. No</th>';
                html += '<th>Train no.</th>';
                html += '<th>No. Of coaches</th>';
                html += '<th>No. Of Advertisement Panels</th>';
                html += '</tr>';
                html += '</thead>';
                html += '<tbody>';
                $(document).find('#inside_metro_section table tbody tr').each(function () {
                    html += '<tr>';
                    html += '<td>' + sno + '</td>';
                    html += '<td>' + $(this).find('.train_no').val() + '</td>';
                    html += '<td>' + $(this).find('.metro_coches').val() + '</td>';
                    html += '<td>' + $(this).find('.ad_panels').val() + '</td>';
                    html += '</tr>';
                    sno++;
                });
                html += '</tbody>';
                html += '</table>';
                html += '</div>';
            }
            sno = 1;
            if ($(document).find('#outside_metro_section').length) {
                html += '<div class="col-md-12">';
                html += '<h3>Outside Metro</h3>';
                html += '<table class="table">';
                html += '<thead>';
                html += '<tr>';
                html += '<th>S. No</th>';
                html += '<th>Train no.</th>';
                html += '<th>No. of coaches for display of advertisement</th>';
                html += '</tr>';
                html += '</thead>';
                html += '<tbody>';
                $(document).find('#outside_metro_section table tbody tr').each(function () {
                    html += '<tr>';
                    html += '<td>' + sno + '</td>';
                    html += '<td>' + $(this).find('.train_no').val() + '</td>';
                    html += '<td>' + $(this).find('.metro_coches').val() + '</td>';
                    html += '</tr>';
                    sno++;
                });
                html += '</tbody>';
                html += '</table>';
                html += '</div>';
            }
            sno = 1;
            if ($(document).find('#inside_station_section').length) {
                html += '<div class="col-md-12">';
                html += '<h3>Inside Station</h3>';
                html += '<table class="table">';
                html += '<thead>';
                html += '<tr>';
                html += '<th>S.no</th>';
                html += '<th>OMD Site</th>';
                html += '<th>Height(in m)</th>';
                html += '<th>Width(in m)</th>';
                html += '<th>Area(in sq. m)</th>';
                html += '</tr>';
                html += '</thead>';
                html += '<tbody>';
                $(document).find('#inside_station_section table tbody tr').each(function () {
                    html += '<tr>';
                    html += '<td>' + sno + '</td>';
                    html += '<td>' + $(this).find('select option:selected').val() + '</td>';
                    html += '<td>' + $(this).find('.heightData').val() + '</td>';
                    html += '<td>' + $(this).find('.widthData').val() + '</td>';
                    html += '<td>' + $(this).find('.areaData').val() + '</td>';
                    html += '</tr>';
                    sno++;
                });
                html += '</tbody>';
                html += '<tfoot>';
                html += '<tr>';
                html += '<td colspan="5"><label><b>Total Area(in sq. m): </b></label><span>' + $(document).find('#inside_station_section table tfoot tr td input').val() + '</span></td>';
                html += '</tr>';
                html += '</tfoot>';
                html += '</table>';
                html += '</div>';
            }
            sno = 1;
            if($(document).find('#station_branding_section').length) {
                html += '<div class="col-md-12">';
                html += '<h3>Station Branding</h3>';
                html += '<table class="table">';
                html += '<thead>';
                html += '<th>S. No</th>';
                html += '<th>Facade</th>';
                html += '<th>Total Facade area(in sq. m)</th>';
                html += '<th>Area of actual display(in sq. m)</th>';
                html += '</thead>';
                html += '<tbody>';
                $(document).find('#station_branding_section table tbody tr').each(function() {
                    html += '<tr>';
                    html += '<td>'+ sno +'</td>';
                    html += '<td>F'+ sno +'</td>';
                    html += '<td>'+ $(this).find('.station_branding_facade').val() +'</td>';
                    html += '<td>'+ $(this).find('.station_branding_actual_facade').val() +'</td>';
                    html += '</tr>';
                    sno++;
                });
                html += '</tbody>';
                html += '</table>';
                html += '</div>';
            }
            $(document).find('#metro_mrts_preview').html(html);
        },
        omdPreviewHtml: function() {
            $('#omds_installed_under_one_roof_preview').html('');
            $('#omds_installed_within_premises_preview').html('');
            if($('#omds_installed_under_one_roof').children().length) {
                var html = '<div class="col-md-12">';
                html += '<h4>OMDS Installed Under One Roof</h4>';
                html += '<table class="table">';
                html += '<thead>';
                html += '<tr>';
                html += '<th>S.no</th>';
                html += '<th>OMD Site</th>';
                html += '<th>Height</th>';
                html += '<th>Width</th>';
                html += '<th>Area</th>';
                html += '</tr>';
                html += '</thead>';
                html += '<tbody>';
                var sno = 1;
                $(document).find('#omds_installed_under_one_roof table tbody tr').each(function() {
                    html += '<tr>';
                    html += '<td>'+ sno +'</td>';
                    html += '<td>';
                    html += $(this).find('select option:selected').val();
                    html += '</td>';
                    html += '<td>' + $(this).find('input[type="text"].heightData').val() + '</td>';
                    html += '<td>' + $(this).find('input[type="text"].widthData').val() + '</td>';
                    html += '<td>' + $(this).find('input[type="text"].areaData').val() + '</td>';
                    html += '</tr>';
                    sno++;
                });
                html += '</tbody>';
                html += '<tfoot>';
                html += '<tr>';
                html += '<td><label>Licence Fees: </lable>'+ $(document).find('#omds_installed_under_one_roof table tfoot .licence_fees').val() +'</td>';
                html += '<td><label>Total Area: </lable>'+ $(document).find('#omds_installed_under_one_roof table tfoot .total_area').val() +'</td>';
                html += '<td colspan="4"><label>Annual Total Licence Fees: </lable>'+ $(document).find('#omds_installed_under_one_roof table tfoot .total_annual_fees').val() +'</td>';
                html += '</tr>';
                html += '</tfoot>';
                html += '</table>';
                html += '</div>';
                $('#omds_installed_under_one_roof_preview').html(html);
            }
            if($('#omds_installed_within_premises').children().length) {
                var html = '<div class="col-md-12">';
                html += '<h4>OMDS installed within premises but in open courtyard</h4>';
                html += '<table class="table">';
                html += '<thead>';
                html += '<tr>';
                html += '<th>S.no</th>';
                html += '<th>OMD Site</th>';
                html += '<th>Height</th>';
                html += '<th>Width</th>';
                html += '<th>Area</th>';
                html += '</tr>';
                html += '</thead>';
                html += '<tbody>';
                var sno = 1;
                $(document).find('#omds_installed_within_premises table tbody tr').each(function() {
                    html += '<tr>';
                    html += '<td>'+ sno +'</td>';
                    html += '<td>';
                    html += $(this).find('select option:selected').val();
                    html += '</td>';
                    html += '<td>' + $(this).find('input[type="text"].heightData').val() + '</td>';
                    html += '<td>' + $(this).find('input[type="text"].widthData').val() + '</td>';
                    html += '<td>' + $(this).find('input[type="text"].areaData').val() + '</td>';
                    html += '</tr>';
                    sno++;
                });
                html += '</tbody>';
                html += '<tfoot>';
                html += '<tr>';
                html += '<td><label>Licence Fees: </lable>'+ $(document).find('#omds_installed_within_premises table tfoot .licence_fees').val() +'</td>';
                html += '<td><label>Total Area: </lable>'+ $(document).find('#omds_installed_within_premises table tfoot .total_area').val() +'</td>';
                html += '<td colspan="3"><label>Annual Total Licence Fees: </lable>'+ $(document).find('#omds_installed_within_premises table tfoot .total_annual_fees').val() +'</td>';
                html += '</tr>';
                html += '</tfoot>';
                html += '</table>';
                html += '</div>';
                $('#omds_installed_within_premises_preview').html(html);
            }
        },
        isAlphabit: function (validationVal, element) {
            if (!validationVal) {
                return false;
            }
        },
        isAlphanumeric: function (validationVal, element) {
            if (!validationVal) {
                return false;
            }
        },
        isNum: function (validationVal, element) {
            if (!validationVal) {
                return false;
            }
            var elementVal = $.trim(element.val());
            if (elementVal) {
                if (!$.isNumeric(elementVal)) {
                    element.after('<p class="error-message">Please enter numeric value</p>');
                    return true;
                }
            }
            return false;
        },
        isInteger: function(validationVal, element) {
            if (!validationVal) {
                return false;
            }
            var elementVal = $.trim(element.val());
            if (elementVal) {
                if(!Number.isInteger(parseFloat(elementVal))) {
                    element.after('<p class="error-message">Please enter integer value</p>');
                    return true;
                }
            }
            return false;
        },
        maxLimit: function (validationVal, element) {
            if (!validationVal) {
                return false;
            }
            var elementVal = $.trim(element.val());
            if (elementVal) {
                if (elementVal.length > validationVal) {
                    element.after('<p class="error-message">Maximum character should be ' + validationVal + '</p>');
                    return true;
                }
            }
        },
        minLimit: function (validationVal, element) {
            if (!validationVal) {
                return false;
            }
            var elementVal = $.trim(element.val());
            if (elementVal) {
                if (elementVal.length < validationVal) {
                    element.after('<p class="error-message">Mininum character should be ' + validationVal + '</p>');
                    return true;
                }
            }
            return false;
        },
        minValue: function (validationVal, element) {
            if (!validationVal) {
                return false;
            }
            var elementVal = $.trim(element.val());
            if (elementVal < validationVal) {
                element.after('<p class="error-message">Mininum value should be ' + validationVal + '</p>');
                return true;
            }
            return false;
        },
        maxValue: function(validationVal, element){
            if (!validationVal) {
                return false;
            }
            var elementVal = $.trim(element.val());
            if (elementVal > validationVal) {
                element.after('<p class="error-message">Maximum value should be ' + validationVal + '</p>');
                return true;
            }
            return false;
        },
        noEmpty: function (validationVal, element) {
            if (!validationVal) {
                return false;
            }
            if($(document).find('select[name="metro_mrts_categories"]').length) {
                var metroMrtsCategories = ($(document).find('select[name="metro_mrts_categories"] option:selected').val()).toLowerCase();
                if(metroMrtsCategories === 'rolling stock' && $.inArray(element.attr('name'),['lat','lng','location']) !== -1) {
                    return false;
                }
            }
            if(element.attr('type') === 'checkbox') {
                if (!element.is(':checked')) {
                    element.parents('.form-groupY').append('<p class="error-message">This field is required</p>');
                    return true;
                }
           } else if (element.attr('type') === 'radio') {
                if (!element.is(':checked')) {
                    element.parents('.form-groupY').append('<p class="error-message">This field is required</p>');
                    return true;
                }
            } else {
                var elementVal = $.trim(element.val());
                var omdTypeListElement = $(document).find('select[name="omd_type_private_list"]');
                if(element.attr('name') === 'total_facade_area' && omdTypeListElement.length) {
                    var omdTypeSelected = omdTypeListElement.find('option:selected').val();
                    if(omdTypeSelected === 'Unipole') {
                        return false;
                    }
                }
                if (!elementVal) {
                    element.after('<p class="error-message">This field is required</p>');
                    return true;
                }
            }
            return false;
        },
        depend: function (validationVal, element) {
            var error = false;
            var defaultFieldVal = element.val();
            if (element.attr('type') === 'radio') {
                defaultFieldVal = element.filter(':checked').val();
            }
            $.each(validationVal, function (field, val) {
                var checked = '';
                if (val.type === 'radio') {
                    checked = ':checked';
                }
                var fieldVal = $('input[name="' + field + '"]' + checked).val();
                if (typeof fieldVal === 'undefined') {
                    if (element.attr('type') === 'radio') {
                        if (typeof defaultFieldVal === 'undefined') {
                            element.parents('.form-groupY').append('<p class="error-message">This field is required</p>');
                            error = true;
                        }
                    }
                } else if ($.inArray(fieldVal, val.value) !== -1) {
                    if (element.attr('type') === 'radio') {
                        if (typeof defaultFieldVal === 'undefined') {
                            element.parents('.form-groupY').append('<p class="error-message">This field is required</p>');
                            error = true;
                        } else if ($.inArray(defaultFieldVal, val.value) !== -1) {
                            element.parents('.form-groupY').append('<p class="error-message">' + val.message + '</p>');
                            error = true;
                        }
                    }
                } else {
                    if (element.attr('type') !== 'radio') {
                        if (fieldVal === val.default && !defaultFieldVal) {
                            element.after('<p class="error-message">This field is required</p>');
                            error = true;
                        }
                    }
                }
                if (error) {
                    return false;
                }
            });
            return error;
        },
        operationalValidation: function (validationVal, element) {
            var error = 0;
            var result = parseFloat(validationVal.defaultValue);
            $.each(validationVal.field, function (index, val) {
                var fieldValue = $(document).find('[name="' + val + '"]').val();
                if (!fieldValue) {
                    error = 1;
                }
            });
            if (!element.val()) {
                error = 1;
            }
            if (error === 0) {
                $.each(validationVal.field, function (index, val) {
                    var fieldValue = $(document).find('[name="' + val + '"]').val();
                    var mathFunction = MathFunctions[validationVal.operator];
                    result = mathFunction(result, fieldValue);
                });
                var operandMathFunction = MathFunctions[validationVal.oprand];
                if (operandMathFunction(element.val(), result)) {
                    element.after('<p class="error-message">' + validationVal.title + ' should not ' + validationVal.oprand + ' than ' + result + ' of ' + validationVal.unit + '</p>');
                    return true;
                }
            }
            return false;
        },
        operationalMultyValidation: function (validationVal, element) {
            var error = 0;
            var resultData = false;
            var omdTypeListElement = $(document).find('select[name="omd_type_private_list"]');
            if(element.attr('name') === 'total_advertising_space' && omdTypeListElement.length) {
                var omdTypeSelected = omdTypeListElement.find('option:selected').val();
                if(omdTypeSelected === 'Unipole') {
                    return false;
                }
            }
            $.each(validationVal.field, function (index, val) {
                var fieldValue = $(document).find('[name="' + val + '"]').val();
                if (!fieldValue) {
                    error = 1;
                }
            });
            if (!element.val()) {
                error = 1;
            }
            if (error === 0) {
                $.each(validationVal.field, function (index, val) {
                    var result = parseFloat(validationVal[index].defaultValue);
                    if(error === 0) {
                        var fieldValue = $(document).find('[name="' + val + '"]').val();
                        var mathFunction = MathFunctions[validationVal[index].operator];
                        //============ add by rohit ====================
						if(($('select[name="subcategory"]').val() == 26) && ($('select[name="omd_type_shop_signage"]').val() == 'Inside Commercial Building or Public Building')){
							result = 1 ;
						}
						//============== end by rohit ===================
                        result = mathFunction(result, fieldValue);
                        var operandMathFunction = MathFunctions[validationVal[index].oprand];
                        if (operandMathFunction(element.val(), result)) {
                            element.after('<p class="error-message">' + validationVal[index].title + ' should not ' + validationVal[index].oprand + ' than ' + result + ' ' + validationVal[index].unit + '</p>');
                            error = 1;
                            resultData = true;
                        }
                    }
                });
            }
            return resultData;
        },
        customCalculations: function (fieldName, data) {
            var self = this;
            var operation = self[data.operation];
            operation(data, fieldName);
        },
        multiply: function (data, fieldName) {
            var fields = [];
            $.each(data.fields, function (index, val) {
                fields.push('[name="' + val + '"]');
            });
            var selector = fields.join(', ');
            $(document).on('focusout', selector, function () {
                var result = data.defaultValue;
                var error = 0;
                var selectorValue = $(this).val();
                if (!selectorValue) {
                    $(document).find('[name="' + fieldName + '"]').val('');
                    return false;
                }
                if (!$.isNumeric(selectorValue)) {
                    alert("Please enter a Numeric value");
                    $(document).find('[name="' + fieldName + '"]').val('');
                    $(this).val("");
                    return false;
                }
                $.each(data.fields, function (index, val) {
                    var fieldValue = $(document).find('[name="' + val + '"]').val();
                    if (!fieldValue) {
                        error = 1;
                    }
                });
                if (error === 0) {
                    $.each(data.fields, function (index, val) {
                        var fieldValue = $('[name="' + val + '"]').val();
                        result = fieldValue * result;
                    });
                    $(document).find('[name="' + fieldName + '"]').val(result);
                }
            });
            
            //============================ add by rohit kaushal ===============
			$(document).on('focusout','.omd_no_venue_class', function () {
				var omd_no_venue_count		=	0;
				var total_omd_no_venue_size =  0;
				$(".omd_no_venue_class").each(function(index, val) {
					if($(this).val()>13.5){
						omd_no_venue_count++;
					}
				});
				if(omd_no_venue_count){
					var price = 0;
					if(data.fields[0] == 'omd_no_venue'){
						price = data.defaultValue;
					}
					total_omd_no_venue_size = (omd_no_venue_count*price);
					if(total_omd_no_venue_size){
						jQuery("input[name='total_omd_fees_venue']").val(parseFloat(jQuery("input[name='omd_fees_venue']").val())+total_omd_no_venue_size);
					}
				} else {
					jQuery("input[name='total_omd_fees_venue']").val(parseFloat(jQuery("input[name='omd_fees_venue']").val()));
				}
				
			});
			
			$(document).on('focusout','.omd_no_others_class', function () {
				var omd_no_others_count	=	0;
				var total_omd_no_others_size =  0;
				$(".omd_no_others_class").each(function(index, val) {
					if($(this).val()>13.5){
						omd_no_others_count++;
					}
				});
				if(omd_no_others_count){
					var price = 0;
					if(data.fields[0] == 'omd_no_others'){
						price = data.defaultValue;
					}
					total_omd_no_others_size = (omd_no_others_count*price);
					if(total_omd_no_others_size){
						jQuery("input[name='total_omd_fees_other_location']").val(parseFloat(jQuery("input[name='omd_fees_other_location']").val())+total_omd_no_others_size);
					}
				} else {
					jQuery("input[name='total_omd_fees_other_location']").val(parseFloat(jQuery("input[name='omd_fees_other_location']").val()));
				}
				
			});
			//============ End by rohit kaushal ============================ 
            
        },
        addition: function(data, fieldName) {
            setInterval(function(){
                var sum = parseFloat(data.defaultValue);
                $.each(data.fields,function(index, val){
                    var value = $.trim($(document).find('[name="' + val + '"]').val()) ? parseFloat($(document).find('[name="' + val + '"]').val()) : 0;
                    sum = sum + value;
                });
                $(document).find('[name="' + fieldName + '"]').val('');
                if(sum) {
                    $(document).find('[name="' + fieldName + '"]').val(sum);
                }
            }, 200);
        },
        bodmas: function (data, fieldName) {
            var fields = [];
            var result = '';
            $.each(data.fields, function (index, val) {
                fields.push('[name="' + val + '"]');
            });
            var selector = fields.join(', ');
            $(document).off('change', selector).on('change', selector, function () {
                if(!$(document).find('[name="building_type[]"]').length) {
                    var multiply = 1;
                    $.each(data.equation.multiply, function (index, val) {
                        if (val === "defaultValue") {
                            multiply = multiply * data.defaultValue;
                        } else {
                            var field = $.trim($('[name="' + val + '"]').val()) || '0';
                            if (field) {
                                multiply = multiply * parseFloat(field);
                            }
                        }
                    });
                    if (multiply) {
                        result = multiply;
                    }
                    
                    $.each(data.equation.percentageDiscount, function (index, val) {
                        var field = $.trim($('[name="' + val + '"]').val());
                        if (field && result) {
                            var discountAmount = result * (parseFloat(field) / 100);
                            result = result - discountAmount;
                        }
                    });
                    
                    //============= add by rohit kaushal(23-07-2018) ================
					if(data.equation.exempted2percentage){
						var total_facade_field  		= Object.keys(data.equation.exempted2percentage.ON);
						let total_facade_input_name		=	total_facade_field[0];
						let total_facade_input_name_val	=	$('[name="' + total_facade_input_name + '"]').val();
						if(total_facade_input_name_val){
							var multiply_new = 1;
							$.each(data.equation.exempted2percentage.multiply, function (index, val) {
								var field_new = $.trim($('[name="' + val + '"]').val()) || '0';
								console.log(field_new);
								console.log('cccc');
								if (field_new) {
									multiply_new = multiply_new * parseFloat(field_new);
								}
							});
							if(multiply_new){
								var exempted_percentage = ((multiply_new*100)/total_facade_input_name_val);
								console.log(multiply_new);
								console.log(exempted_percentage);
								if(exempted_percentage<=2){
									//$('[name="' + fieldName + '"]').val('0');
									result = '0';
								}
							}
								
						}
					}
					//========== end by rohit kaushal(23-07-2018) ====================
                    
                    
                    $('[name="' + fieldName + '"]').val('');
                    if(result) {
                        $('[name="' + fieldName + '"]').val(Number(Math.round(result+'e2')+'e-2'));
                    }
                }
            });
        },
        dimensionChange: function(data, fieldName){
            var fields = [];
            $.each(data.fields, function (index, val) {
                fields.push('[name="' + val + '"]');
            });
            var selector = fields.join(', ');
            $(document).off('change', selector).on('change', selector, function () {
                $(document).find('input[name="omd_height"]').val('');
                $(document).find('input[name="omd_width"]').val('');
                $(document).find('input[name="total_advertising_space"]').val('');
                var sizeData = $(this).val();
                if(sizeData) {
                    var sizeDataArr = sizeData.split('x');
                    var height = parseFloat($.trim(sizeDataArr[0]));
                    var width = parseFloat($.trim(sizeDataArr[1]));
                    var result = height * width;
                    $(document).find('input[name="omd_height"]').val(height);
                    $(document).find('input[name="omd_width"]').val(width);
                    $(document).find('input[name="total_advertising_space"]').val(Number(Math.round(result+'e2')+'e-2'));
                    $(document).find('input[name="tenure"]').trigger('change');
                }
            });
        },
        onSelect: function (data, fieldName) {
            var options = data.options;
            var fields = [];
            $.each(data.fields, function (index, val) {
                fields.push('[name="' + val + '"]');
            });
            var selector = fields.join(', ');
            $(document).off('change', selector).on('change', selector, function () {
                $(document).find('[name="' + fieldName + '"]').val('');
                if (typeof options[$(this).val()] !== 'undefined' && !Array.isArray(options[$(this).val()])) {
                    $(document).find('[name="' + fieldName + '"]').val(options[$(this).val()]);
                    $(document).find('[name="' + fieldName + '"]').focus();
                } else {
                    var element = $(document).find('[name="' + fieldName + '"]');
                    var label = element.parents('.form-groupY').find('label').html();
                    var html = '<option value="">Select ' + label + '</option>';
                    if (typeof options[$(this).val()] !== 'undefined') {
                        $.each(options[$(this).val()], function (index, val) {
                            html += '<option value="' + val + '">' + val + '</option>';
                        });
                    }
                    element.html(html);
                }
            });
        },
        discountCalculate: function (data, fieldName) {
            var fields = [];
            $.each(data.fields, function (index, val) {
                fields.push('[name="' + val + '"]');
            });
            var selector = fields.join(', ');
            $(document).off('keyup', selector).on('keyup', selector, function () {
                var element = $(this);
                var noOfMonths = element.val();
                $(document).find('[name="discount"]').val('');
                if (!isNaN(noOfMonths)) {
                    $.each(data.data, function (index, val) {
                        if (typeof data.data[index + 1] !== 'undefined') {
                            var objectKeysMonths = Object.keys(val);
                            var objectKeysMonthsNext = Object.keys(data.data[index + 1]);
                            var discountValue = Object.values(val);
                            if((parseInt(objectKeysMonths[0]) <= parseInt(noOfMonths)) && (parseInt(objectKeysMonthsNext[0]) > parseInt(noOfMonths))) {
                                console.log("Match");
                                $(document).find('[name="discount"]').val(discountValue[0]);
                            }
                        } else {
                            var objectKeysMonths = Object.keys(val);
                            var discountValue = Object.values(val);
                            if((parseInt(objectKeysMonths[0]) <= parseInt(noOfMonths))) {   
                                $(document).find('[name="discount"]').val(discountValue[0]);
                            }
                        }
                    });
                }
            });
        }
    };

    var MathFunctions = {
        "+": function (x, y) {
            return x + y;
        },
        "-": function (x, y) {
            return x - y;
        },
        "*": function (x, y) {
            return Number(Math.round(x * y+'e2')+'e-2');
        },
        "greater": function (x, y) {
            return x > y;
        },
        zero: function(x, y) {
            return x;
        },
        "***": function(x,y) {
            var result= 1;
            var data = y.split('x');
            $.each(data,function(index,val){
                result = result * parseFloat(val);
            });
            return Number(Math.round(result+'e2')+'e-2');
        }
    };
    SubCategory.subCategoryData(<?php echo $subCategory;?>, 'formData');
    $('#typology').on('change', function () {
        var typologyId = $(this).val();
        SubCategory.clearFormPanel('formData');
        Typology.typologyData(typologyId, 'sub_category');

    });

    $('#sub_category').on('change', function () {
        var subCategoryId = $(this).val();
        $(document).off('focusout');
        SubCategory.subCategoryData(subCategoryId, 'formData');
    });
    $(document).on('click', '#formPreview', function () {
        SubCategory.moveStep2();
    });

    $(document).on('keyup', 'input[name="omd_no_venue"],input[name="omd_no_others"]', function (e) {
		/*
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || (e.keyCode >= 35 && e.keyCode <= 40)) {
            $(document).find('#' + 'omd_size_' + $(e.target).attr('name')).remove();
            SubCategory.clearOMDSizeFieldsValidations($(e.target));
            return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            $(e.target).val('');
            e.preventDefault();
        }
        */
        
        //============= add by rohit kaushal ==============
        if ($.inArray(e.keyCode, [46, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || (e.keyCode >= 35 && e.keyCode <= 40)) {
            $(document).find('#' + 'omd_size_' + $(e.target).attr('name')).remove();
            SubCategory.clearOMDSizeFieldsValidations($(e.target));
            return;
        }
      
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) && (e.keyCode!=8)) {
            $(e.target).val('');
            e.preventDefault();
        }
        //================================================== 
        
        if ($(e.target).val() < 1) {
            $(document).find('#' + 'omd_size_' + $(e.target).attr('name')).remove();
            SubCategory.clearOMDSizeFieldsValidations($(e.target));
            $(e.target).val('');
            return false;
        }
        $(e.target).attr('name')
        
        if($(e.target).attr('name') == 'omd_no_others'){
			if ($(e.target).val() > 20) {
				alert("No. of OMD should be upto 10");
				$(document).find('#' + 'omd_size_' + $(e.target).attr('name')).remove();
				SubCategory.clearOMDSizeFieldsValidations($(e.target));
				$(e.target).val('');
				return false;
			}
		} else {
			if ($(e.target).val() > 10) {
				alert("No. of OMD should be upto 10");
				$(document).find('#' + 'omd_size_' + $(e.target).attr('name')).remove();
				SubCategory.clearOMDSizeFieldsValidations($(e.target));
				$(e.target).val('');
				return false;
			}
		}
        SubCategory.getOMDSizeFields($(e.target));
    });

    $(document).on('keyup', '.omd-size-meters', function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || (e.keyCode >= 35 && e.keyCode <= 40)) {
            return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            $(e.target).val('');
            e.preventDefault();
        }
        if ($(e.target).val() < 1) {
            $(e.target).val('');
            return false;
        }
        /*
        var sizeLimit = 13.5;
        if ($(e.target).val() > sizeLimit) {
            alert("More than " + sizeLimit + " meters not allowed");
            $(e.target).val('');
            return false;
        }
        */ 
    });
//    $(document).on('input', '#formData input[name="location"]', function () {
//        var element = $(this);
//        $(element).autocomplete({
//            source: function (request, response) {
//                $.ajax({
//                    url: '<?php //echo $this->Url->build(["controller" => "CompanyDevices", "action" => "getLocation"]);  ?>',
//                    type: 'GET',
//                    dataType: 'JSON',
//                    data: {s: request.term},
//                    success: response
//                });
//            },
//            minLength: 2,
//            select: function (event, ui) {
//                setLocation(ui.item.data.location.lat, ui.item.data.location.lng);
//                codeAddress(ui.item.data.location.lat, ui.item.data.location.lng);
//            },
//            delay: 1000
//        });
//    });

    var geocoder;
    var map;
    var markersArray = [];
    var mapOptions = {
        center: new google.maps.LatLng(28.4089, 77.3178),
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var marker;

    function createMarker(latLng) {
        if (!!marker && !!marker.setMap)
            marker.setMap(null);
        marker = new google.maps.Marker({
            map: map,
            position: latLng,
            draggable: true
        });
        google.maps.event.addListener(marker, "dragend", function () {
            setLocation(marker.getPosition().lat(), marker.getPosition().lng());
            codeAddress(marker.getPosition().lat(), marker.getPosition().lng());
        });
    }

    function initialize() {
        geocoder = new google.maps.Geocoder();
        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        codeAddress(28.4089, 77.3178, 1);

        google.maps.event.addListener(map, 'click', function (event) {
            map.panTo(event.latLng);
            map.setCenter(event.latLng);
            createMarker(event.latLng);
            codeAddress(event.latLng.lat(), event.latLng.lng());
            setLocation(event.latLng.lat(), event.latLng.lng());
        });

    }
    function codeAddress(lat, lng, intial) {
        var latlng = {lat: lat, lng: lng};
        geocoder.geocode({'location': latlng}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (!intial) {
                    var locationNames = [];
                    $.each(results[0].address_components, function (index, val) {
                        locationNames.push(val.long_name.toLowerCase());
                    });
                    if ($.inArray('faridabad', locationNames) !== -1) {
                        map.setCenter(results[0].geometry.location);
                        createMarker(latlng);
                        $('#formData input[name="location"]').val(results[0].formatted_address);
                    }
                    if ($.inArray('faridabad', locationNames) !== -1) {
                        map.setCenter(results[0].geometry.location);
                        createMarker(latlng);
                        $('#formData input[name="location"]').val(results[0].formatted_address);
                    } else {
                        alert("Invalid Location.");
                        codeAddress(28.4089, 77.3178, 1);
                        setLocation(28.4089, 77.3178);
                    }
                } else {
                    $('#formData input[name="location"]').val('');
                    map.setCenter(results[0].geometry.location);
                    createMarker(latlng);
                }
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }

    function setLocation(lat, lng) {
        $(document).find('#formData input[name="lat"]').val(lat);
        $(document).find('#formData input[name="lng"]').val(lng);
    }


    $(document).on("change", '.deviceImageUpload', function (e) {
        var self = $(this);
        var fileType = this.files[0].type;
        var ValidImageTypes = ["image/gif", "image/jpeg", "image/png", "image/jpg", "application/pdf", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/msword", "text/csv"];
        if ($.inArray(fileType, ValidImageTypes) < 0) {
            alert("Invalid Document");
            self.val("");
        }
    });
    $('body').on('focus',".datePicker", function(){
    var today = new Date();
    $(this).datepicker({
        minDate: 0,
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        maxDate: new Date(today.getFullYear() +1, today.getMonth(), today.getDate())
    });
});
$(document).on('keyup','input[name="tenure"]',function(){
    if(SubCategory.subCategoryId === 29) {
        SubCategory.calculateArea('#sub_category option:selected'); // Providing the numeric value just only for calling the same function
    }
});
$(document).on('change','select[name="omd_type_private_list"]',function(){
    var element = $(document).find('input[name="total_facade_area"]');
    element.parents('.form-groupY').show();
    var omdTypeVal = $(this).val();
    if(omdTypeVal === 'Unipole') {
        element.val('');
        element.parents('.form-groupY').hide();
    }
});
$(document).on('click','#submitOmdApp',function(e){
    $(document).find('.error-message').remove();
    if(!$(document).find('#agreeOMD').is(':checked')) {
        $(document).find('.termsc').append('<p class="error-message">You must agree to the terms</p>');
        e.preventDefault();
    }
});
</script>
