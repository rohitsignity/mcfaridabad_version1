<div class="page-content inset app-status">
    <div class="headings-all dash-in">
        <h1>Installments</h1>
    </div>
    <div>
        <table class="table">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Amount</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($installments as $installment) {
                    ?>
                    <tr>
                        <td><?php echo 'OMD' . $installment['id']; ?></td>
                        <td><?php echo number_format($installment['amount'],2); ?></td>
                        <td><?php echo date('d M, Y', strtotime($installment['start_date'])); ?></td>
                        <td><?php echo date('d M, Y', strtotime($installment['end_date'])); ?></td>
                        <td><?php echo $installment['payment_id'] ? 'Paid' : 'Pending'; ?></td>
                        <td><?php
                            if ($installment['receiptUrl']) {
                                ?>
                                <a href="<?php echo $installment['receiptUrl']; ?>" download="receipt.pdf">Download receipt</a>
                                <?php
                            } else if ($installment['payNow']) {
                                ?>
                                <a href="javascript:void(0)" data-fees="<?php echo $installment['amount'];?>" data-start="<?php echo date('d M, Y',strtotime($installment['start_date']));?>" data-end="<?php echo date('d M, Y',strtotime($installment['end_date']));?>" onclick="Payment.payInstallment(this,'<?php echo $installment['device_id'];?>')">Pay Now</a>
                                <?php
                            } else {
                                echo '--N/A--';
                            }
                            ?></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
</div>
<?php echo $this->Html->script('payment.js'); ?>
<script>
    Payment.installmentUrl = '<?php echo $this->Url->build(['action'=> 'payInstallment'],true);?>';
</script>