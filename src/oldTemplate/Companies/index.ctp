<!-- Content section -->
<section class="content-wrap">
    <div class="container">
        <div class="company-reg">
            <div class="headings-all text-center">
                <h1>Company Registration</h1>
            </div>
            <div class="stepsblk clearfix">
                <ul>
                    <li class="step1-blk active tab-1">
                        <a href="javascript:;"><span>1</span>
                            <h2>Step1</h2>
                            <p>Company Information</p>
                            <p>Mailing Address</p></a>
                    </li>
                    <li class="step1-blk step2-tab">
                        <a href="javascript:;"><span>2</span>
                            <h2>Step2</h2>
                            <p>Preview</p>
                        </a>
                    </li>

                </ul>
            </div>
            <div class="regist-form" id="tab-1">
                <h2 class="formh2">Company Information</h2>				
                <div class="row">
                    <?php
                    $companyArray = $company->toArray();
                    //echo $this->Form->create($company);
                    echo $this->Form->create('Register', ['id' => 'register']);
                    ?>
                    <div class="col-md-6">
                        <?php
                        echo $this->Form->input('company_name', ['type' => 'text', 'class' => 'form-control', 'label' => 'Company Name/Building Owner', 'templates' => [
                                'inputContainer' => '<div class="form-group">{{content}}</div>'
                            ], 'placeholder' => 'Enter Company Name']);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?php
                        echo $this->Form->input('representative_id', [
                            'empty' => 'Select Representative',
                            'type' => 'select',
                            'options' => $representative,
                            'label' => 'Representative',
                            'class' => 'form-control',
                            'templates' => [
                                'inputContainer' => '<div class="form-group">{{content}}</div>'
                            ]
                        ]);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?php
                        echo $this->Form->input('name', ['type' => 'text', 'class' => 'form-control', 'label' => 'Name', 'templates' => [
                                'inputContainer' => '<div class="form-group">{{content}}</div>'
                            ], 'placeholder' => 'Enter Name']);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?php
                        echo $this->Form->input('phone', ['type' => 'text', 'class' => 'form-control', 'label' => 'Telephone No.', 'templates' => [
                                'inputContainer' => '<div class="form-group">{{content}}</div>'
                            ], 'placeholder' => 'Enter Telephone']);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?php
                        echo $this->Form->input('email', ['type' => 'text', 'class' => 'form-control', 'label' => 'E-mail', 'templates' => [
                                'inputContainer' => '<div class="form-group">{{content}}</div>'
                            ], 'placeholder' => 'E-mail']);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?php
                        echo $this->Form->input('incorporation_date', ['type' => 'text', 'class' => 'form-control', 'label' => 'Date of Incorporation', 'readonly' => 'readonly', 'templates' => [
                                'inputContainer' => '<div class="form-group">{{content}}</div>'
                            ], 'placeholder' => 'Date of Incorporation']);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?php
                        echo $this->Form->input('pan_number', ['type' => 'text', 'class' => 'form-control', 'templates' => [
                                'inputContainer' => '<div class="form-group">{{content}}</div>'
                            ], 'placeholder' => 'PAN']);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?php
                        echo $this->Form->input('tin_number', ['type' => 'text', 'class' => 'form-control', 'templates' => [
                                'inputContainer' => '<div class="form-group">{{content}}</div>'
                            ], 'placeholder' => 'TIN']);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?php
                        echo $this->Form->input('service_tax_number', ['type' => 'text', 'class' => 'form-control', 'templates' => [
                                'inputContainer' => '<div class="form-group">{{content}}</div>'
                            ], 'placeholder' => 'Service Tax Number']);
                        ?>
                    </div> 
                    <div class="clearfix"></div>
                    <div class="input_fields_wrap">
						<a href="javascript:;" class="collapsed addmore add_field_button" role="button"> Add More Fields <img src="<?php echo $this->Url->image('addmore.png'); ?>"/></a>
					</div>
                    <?php /*
                    <div class="col-md-12">
                        <div class="panel-border">
                            <a href="#collapseTwo" class="collapsed addmore" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="collapseTwo"> Add More Fields <img src="<?php echo $this->Url->image('addmore.png'); ?>"/></a>
                            <div class="panel-collapse collapse" role="tabpanel" id="collapseTwo" aria-labelledby="headingTwo" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <?php
                                            echo $this->Form->input('name12', ['type' => 'text', 'class' => 'form-control', 'templates' => [
                                                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                                                ], 'placeholder' => 'Name']);
                                            ?>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Telephone No.</label>
                                                <input class="form-control" id="exampleInputEmail1" placeholder="Enter Company Name" type="email">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">E-mail</label>
                                                <input class="form-control" id="exampleInputEmail1" placeholder="Enter Company Name" type="email">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    */ ?>
                    <div class="clearfix"></div>
                    <div class="mailng-add">
                        <h2 class="formh2">Mailing Address</h2>
                    </div>
                    <div class="col-md-12">
                        <?php
                        echo $this->Form->input('address', ['type' => 'textarea', 'class' => 'form-control', 'templates' => [
                                'inputContainer' => '<div class="form-group">{{content}}</div>'
                            ], 'placeholder' => 'Address']);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?php
                        echo $this->Form->input('city', ['type' => 'text', 'class' => 'form-control', 'templates' => [
                                'inputContainer' => '<div class="form-group">{{content}}</div>'
                            ], 'placeholder' => 'City']);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?php
                        echo $this->Form->input('state', ['type' => 'text', 'class' => 'form-control', 'templates' => [
                                'inputContainer' => '<div class="form-group">{{content}}</div>'
                            ], 'placeholder' => 'State']);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?php
                        echo $this->Form->input('pincode', ['type' => 'text', 'class' => 'form-control', 'templates' => [
                                'inputContainer' => '<div class="form-group">{{content}}</div>'
                            ], 'placeholder' => 'Pincode']);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?php
                        echo $this->Form->input('email_corresponding', ['type' => 'text', 'class' => 'form-control', 'templates' => [
                                'inputContainer' => '<div class="form-group">{{content}}</div>'
                            ], 'placeholder' => 'E-mail ( For correspondence )']);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?php
                        echo $this->Form->input('contact', ['type' => 'text', 'class' => 'form-control', 'templates' => [
                                'inputContainer' => '<div class="form-group">{{content}}</div>'
                            ], 'placeholder' => 'Contact No.']);
                        ?>
                    </div>
                    <div class="col-md-12">
                        <div class="checkbox">
                            <label>
                                <?php echo $this->Form->input('permanentAddressCheck', ['type' => 'checkbox', 'hiddenField' => false, 'label' => false, 'id' => 'permanentAddressCheck']); ?>
                                <p class="termsc">Check For Same Permanent Address As Mailing Address </p>
                            </label>
                        </div>
                    </div>
                    <div id="permanent-address" style="<?php echo isset($companyArray['permanentAddressCheck']) ? 'display: none;' : ''; ?>">
                        <div class="mailng-add" style="clear: both;">
                            <h2 class="formh2">Permanent Address</h2>
                        </div>
                        <div class="col-md-12">
                            <?php
                            echo $this->Form->input('p_address', ['type' => 'textarea', 'class' => 'form-control', 'label' => 'Address', 'templates' => [
                                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                                ], 'placeholder' => 'Address']);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                            echo $this->Form->input('p_city', ['type' => 'text', 'class' => 'form-control', 'label' => 'City', 'templates' => [
                                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                                ], 'placeholder' => 'City']);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                            echo $this->Form->input('p_state', ['type' => 'text', 'class' => 'form-control', 'label' => 'State', 'templates' => [
                                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                                ], 'placeholder' => 'State']);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                            echo $this->Form->input('p_pincode', ['type' => 'text', 'class' => 'form-control', 'label' => 'Pincode', 'templates' => [
                                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                                ], 'placeholder' => 'Pincode']);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                            echo $this->Form->input('p_email_corresponding', ['type' => 'text', 'class' => 'form-control', 'label' => 'Email Corresponding', 'templates' => [
                                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                                ], 'placeholder' => 'Email Corresponding']);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                            echo $this->Form->input('p_contact', ['type' => 'text', 'class' => 'form-control', 'label' => 'Contact', 'templates' => [
                                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                                ], 'placeholder' => 'Contact Number']);
                            ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <?php echo $this->Form->button('Next', ['class' => 'btn btn-default', 'formnovalidate' => true, 'id' => 'validateComapny']); ?>
                    <?php echo $this->Form->end(); ?>                    
                </div>
            </div>
			<div class="regist-form" id="tab-2" style="display:none">
				<h2 class="formh2">Preview</h2>
				<div class="row">
					<div class="prevwfm">

						<div class="form-group clearfix">
							<label class="col-md-7" for="exampleInputEmail1">Company Name/Building Owner</label>
							<div class="col-md-5">
								<span id="v-company_name">DIMTS</span>
							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-md-7" for="exampleInputEmail1">Representative</label>
							<div class="col-md-5">
								<span id="v-representative_id">Individual</span>
							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-md-7" for="exampleInputEmail1">Name</label>
							<div class="col-md-5">
								<span id="v-name">John fany</span>
							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-md-7" for="exampleInputEmail1">Telephone No.</label>
							<div class="col-md-5">
								<span id="v-phone">0123456789</span>
							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-md-7" for="exampleInputEmail1">E-mail</label>
							<div class="col-md-5">
								<span id="v-email">0123456789</span>
							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-md-7" for="exampleInputEmail1">Date Of Incorporation</label>
							<div class="col-md-5">
								<span id="v-incorporation_date">0123456789</span>
							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-md-12 darkgrey" for="exampleInputEmail1">Mailing Address</label>

						</div>
						<div class="form-group clearfix">
							<label class="col-md-7" for="exampleInputEmail1">Address</label>
							<div class="col-md-5">
								<span id="v-address">N/A</span>
							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-md-7" for="exampleInputEmail1">City</label>
							<div class="col-md-5">
								<span id="v-city">Chandigarh</span>
							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-md-7" for="exampleInputEmail1">State</label>
							<div class="col-md-5">
								<span id="v-state">Chandigarh</span>
							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-md-7" for="exampleInputEmail1">Pincode</label>
							<div class="col-md-5">
								<span id="v-pincode">160101</span>
							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-md-7" for="exampleInputEmail1">E-mail</label>
							<div class="col-md-5">
								<span id="v-email_corresponding">info@dummy.com</span>
							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-md-7" for="exampleInputEmail1">Telephone No.</label>
							<div class="col-md-5">
								<span id="v-contact">0123456789</span>
							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-md-12 darkgrey" for="exampleInputEmail1">Permanent Address</label>

						</div>
						<div class="form-group clearfix">
							<label class="col-md-7" for="exampleInputEmail1">Address</label>
							<div class="col-md-5">
								<span id="v-p_address">Lorem 1234 text</span>
							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-md-7" for="exampleInputEmail1">City</label>
							<div class="col-md-5">
								<span id="v-p_city">Chandigarh</span>
							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-md-7" for="exampleInputEmail1">State</label>
							<div class="col-md-5">
								<span id="v-p_state">Chandigarh</span>
							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-md-7" for="exampleInputEmail1">Pincode</label>
							<div class="col-md-5">
								<span id="v-p_pincode">160101</span>
							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-md-7" for="exampleInputEmail1">E-mail</label>
							<div class="col-md-5">
								<span id="v-p_email_corresponding">info@dummy.com</span>
							</div>
						</div>
						<div class="form-group clearfix">
							<label class="col-md-7" for="exampleInputEmail1">Telephone No.</label>
							<div class="col-md-5">
								<span id="v-p_contact">0123456789</span>
							</div>
						</div>
						<div class="clearfix"></div>
						<a href="javascript:;" class="btn btn-default" id="finishCompanyRegistration">Next</a>
					</div>
				</div>
			</div>
        </div>
    </div>
</section>
<script>
    $('#permanentAddressCheck').on('change', function () {
        var checked = $(this).is(':checked');
        if (checked) {
            $('#permanent-address').slideUp();
        } else {
            $('#permanent-address').slideDown();
        }
    });

    $('#addMore').on('click', function () {
        console.log("here");
    });

    $(function () {
		/// Adding datepicker ----------------------------------------------------------------------------
        $("#incorporation-date").datepicker({
            dateFormat: 'yy-mm-dd'
        });
        
        // Tab switching between steps -------------------------------------------------------------------
        $('.tab-1').click( function(){
		  $('#tab-1').show();
		  $('#tab-2').hide();
		  $('.tab-1').addClass('active');
		  $('.tab-2').removeClass('active');
		});
		
				
		/// Add more functionality -----------------------------------------------------------------------
		var max_fields      = 2; //maximum input boxes allowed
		var wrapper         = $(".input_fields_wrap"); //Fields wrapper
		var add_button      = $(".add_field_button"); //Add button ID
	   
		var x = 0; //initial text box count
		$(add_button).click(function(e){ //on add input button click
			e.preventDefault();
			if(x < max_fields){ //max input box allowed
				x++; //text box increment
				$(wrapper).append('<div class="group_id_'+x+'"><div class="col-md-6"><div class="form-group"><label for="name_'+x+'">Name</label><input type="text" id="name_'+x+'" placeholder="Enter Name" class="form-control" name="name_'+x+'"></div></div><div class="col-md-6"><div class="form-group"><label for="phone_'+x+'">Telephone No.</label><input type="text" id="phone_'+x+'" placeholder="Enter Telephone" class="form-control" name="phone_'+x+'"></div></div><div class="col-md-6"><div class="form-group"><label for="email_'+x+'">E-mail</label><input type="text" id="email_'+x+'" placeholder="E-mail" class="form-control" name="email_'+x+'"></div></div><a href="#" class="remove_field">Remove</a><div class="clearfix"></div></div>'); //add input box
				
				if( 2 == x ){
					$(".group_id_1").find('.remove_field').remove();
				} 
				//$(wrapper).append('<div><input type="text" name="mytext[]"/><a href="#" class="remove_field">Remove</a></div>');
			} else {
				alert('You can not add Organizor more than 3');
			}
		});
	   
		$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
			e.preventDefault(); $(this).parent('div').remove();
			if( 2 == x ){
				$(".group_id_1").find('.clearfix').before('<a href="#" class="remove_field">Remove</a>');
			}
			 x--;
		})
		
		/// Step1 register functionality  ------------------------------------------------------------------
		$('#register').on('submit', function (e) {	
			$(document).find('.error-message').remove();
			e.preventDefault();
			var data = $(this).serialize();
			$('#loaderLayout').show();
			$.ajax({
				url: '<?php echo $this->Url->build([ "controller" => "Companies", "action" => "validateCompany"]); ?>',
				type: 'POST',
				data: data,
				dataType: 'JSON',
				success: function (response) {
					$('#loaderLayout').hide();
					if (response.error === 1) {
						$.each(response.data, function (index, val) {
							$('#register [name="' + index + '"]').after('<p class="error-message">' + val[Object.keys(val)[0]] + '</p>');
						});
					} else {
						//console.log(response.data);
						$.each(response.data, function (index, val) {
							//console.log(index+' : '+val);
							$('#v-'+index).html(val);
						});
						$('.step2-tab').addClass('tab-2');
						
						$('.tab-2').on('click', function(){
						  $('#tab-2').show();
						  $('#tab-1').hide();
						  $('.tab-2').addClass('active');
						  $('.tab-1').removeClass('active');
						});
		
		
						$('#tab-2').show();
						$('#tab-1').hide();
						$('.tab-2').addClass('active');
						$('.tab-1').removeClass('active');
					}
				},
				error: function (error) {
					console.log(error);
				}
			});
			return false;
		});
		
		// Finish registration functionality
		$('#finishCompanyRegistration').on('click', function () {
			$('#register').unbind('submit').submit();
			$("#register").submit();
		});
    });
</script>
