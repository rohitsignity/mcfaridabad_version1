<script>
    var Mobile = {
        mobileNumber: '<?php echo $userArr['mobile']; ?>',
        validateMobile: function () {
            var self = this;
            swal({
                title: 'Verify Mobile Number',
                input: 'text',
                showCancelButton: false,
                confirmButtonText: 'Send OTP!',
                showLoaderOnConfirm: true,
                inputValue: self.mobileNumber,
                preConfirm: function (mobileNo) {
                    return new Promise(function (resolve, reject) {
                        if (!$.trim(mobileNo)) {
                            reject('Mobile Number is required');
                        }
                        if (!$.isNumeric(mobileNo)) {
                            reject('Mobile Number must be numeric');
                        } else if ($.trim(mobileNo).length < 10 || $.trim(mobileNo).length > 10) {
                            reject('Mobile Number should of of 10 digits');
                        } else {
                            $.when($.ajax({
                                url: '<?php echo $this->Url->build(['action' => 'sendOtp']); ?>',
                                type: 'POST',
                                dataType: 'JSON',
                                data: {mobile: mobileNo}
                            })).then(function (data, textStatus, jqXHR) {
                                if (data.error === 0) {
                                    resolve();
                                } else {
                                    reject(data.data);
                                }
                            }, function () {
                                reject("Something went wrong on server.");
                            });
                        }
                    });
                },
                allowOutsideClick: false
            }).then(function (mobileNo) {
                self.validateOtp();
            });
        },
        sendOtp: function (mobileNo) {
            var self = this;

        },
        validateOtp: function () {
            var self = this;
            swal({
                title: 'Enter OTP',
                input: 'text',
                showCancelButton: true,
                confirmButtonText: 'Verify',
                showLoaderOnConfirm: true,
                closeOnConfirm: false,
                animation: false,
                cancelButtonText: 'Resend OTP!',
                preConfirm: function (otp) {
                    return new Promise(function (resolve, reject) {
                        if(!$.trim(otp)) {
                            reject("OTP is required");
                        } else {
                            $.ajax({
                                url: '<?php echo $this->Url->build(['action' => 'verifyOtp']);?>',
                                data: { otp: otp },
                                type: 'POST',
                                dataType: 'JSON',
                                success: function(response) {
                                    if(response.error === 1) {
                                        reject(response.data);
                                    } else {
                                        window.location = response.data;
                                    }
                                },
                                error: function() {
                                    reject("Something went wrong on server.");
                                }
                            });
                        }
                    });
                },
                onOpen: function() {
                    $(document).find('.swal2-cancel').attr('disabled','disabled');
                    var time = 59;
                    $(document).find('.swal2-cancel').html('Resend OTP ('+ time +')');
                    var inter = setInterval(function() {
                        $(document).find('.swal2-cancel').html('Resend OTP ('+ time +')');
                        if(time === 0) {
                            clearInterval(inter);
                            $(document).find('.swal2-cancel').html('Resend OTP');
                            $(document).find('.swal2-cancel').removeAttr('disabled');
                        } else {
                            $(document).find('.swal2-cancel').attr('disabled','disabled');
                        }
                        time--;
                    },1000);
                },
                allowOutsideClick: false
            }).then(function (otp) {

            }, function () {
                $.when($.ajax({
                    url: '<?php echo $this->Url->build(['action' => 'sendOtp']); ?>',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {mobile: self.mobileNumber}
                })).then(function (data, textStatus, jqXHR) {
                    if (data.error === 0) {

                    }
                }, function () {
                    
                });
                self.validateOtp();
            });
        }
    };
    Mobile.validateMobile();
</script>
