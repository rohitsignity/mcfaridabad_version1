<!-- Keep all page content within the page-content inset div! -->
<div class="page-content inset app-status">
    <div class="headings-all dash-in">
        <h1 class="text-center">Manage Profile</h1>
    </div>
    <div class="regist-form profile-manage clearfix">
        <div class="profile-img"><span class="glyphicon glyphicon-user"></span></div>
        <div class="proftab clearfix">
            <ul>
                <li class="col-md-6 active tab-1"><a href="javascript:;">Change pasword</a></li>
                <li class="col-md-6 tab-2"><a href="javascript:;">Company Information</a></li>
            </ul>
        </div>
        <div class="changepwd" id="tab-1">
            <?php echo $this->Form->create($userData); ?>		 
            <div class="form-group">
                <?php
                echo $this->Form->input('current_password', ['type' => 'password', 'class' => 'form-control', 'label' => 'Current Password', 'templates' => [
                        'inputContainer' => '{{content}}'
                    ], 'placeholder' => 'Enter Current Password']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->input('new_password', ['type' => 'password', 'class' => 'form-control', 'label' => 'New Password', 'templates' => [
                        'inputContainer' => '{{content}}'
                    ], 'placeholder' => 'Enter New Password']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->input('confirm_password', ['type' => 'password', 'class' => 'form-control', 'label' => 'Confirm Password', 'templates' => [
                        'inputContainer' => '{{content}}'
                    ], 'placeholder' => 'Enter Confirm Password']);
                ?>	
            </div>
            <div class="submit">
                <?php echo $this->Form->button('Submit', ['class' => 'btn btn-default', 'formnovalidate' => true]) ?>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
        <div class="changepwd compny-info" id="tab-2" style="display:none;">
                <!--<div class="edit-info text-right"><span class="glyphicon glyphicon-pencil"></span> Edit</div>-->
            <form class="clearfix">
                <div class="form-group clearfix">
                    <label class="col-md-6 inner-pad" for="exampleInputEmail1">Name</label>
                    <div class="col-md-6 inner-pad">
                        <span><?php echo $companyInfo['display_name']; ?></span>
                    </div>
                </div>
                <div class="form-group clearfix">
                    <label class="col-md-6 inner-pad" for="exampleInputEmail1">Created At</label>
                    <div class="col-md-6 inner-pad">
                        <span><?php echo date('d M, Y h:i A',strtotime($companyInfo['created'])); ?></span>
                    </div>
                </div>
                <div class="form-group clearfix">
                    <label class="col-md-6 inner-pad" for="exampleInputEmail1">Mobile</label>
                    <div class="col-md-6 inner-pad">
                        <span><?php echo $companyInfo['mobile']; ?></span>
                    </div>
                </div>
                <div class="form-group clearfix">
                    <label class="col-md-6 inner-pad" for="exampleInputEmail1">E-mail</label>
                    <div class="col-md-6 inner-pad">
                        <span><?php echo $companyInfo['email']; ?></span>
                    </div>
                </div>
                <div class="form-group clearfix">
                    <label class="col-md-6 inner-pad" for="exampleInputEmail1">Date Of Incorporation</label>
                    <div class="col-md-6 inner-pad">
                        <span><?php echo date('d M, Y', strtotime($companyInfo['incorporation_date'])); ?></span>
                    </div>
                </div>
                <div class="form-group clearfix">
                    <label class="col-md-6 inner-pad" for="exampleInputEmail1">Representative</label>
                    <div class="col-md-6 inner-pad">
                        <span><?php echo $companyInfo['representative']; ?></span>
                    </div>
                </div>
                <?php
                if ($companyInfo['authorised_contact_person']) {
                    ?>
                    <div class="form-group clearfix">
                        <label class="col-md-6 inner-pad" for="exampleInputEmail1">Authorized Contact Person</label>
                        <div class="col-md-6 inner-pad">
                            <span><?php echo $companyInfo['authorised_contact_person']; ?></span>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <div class="form-group clearfix">
                    <label class="col-md-6 inner-pad" for="exampleInputEmail1">Address</label>
                    <div class="col-md-6 inner-pad">
                        <span><?php echo $companyInfo['address']; ?></span>
                    </div>
                </div>
                <div class="form-group clearfix">
                    <label class="col-md-6 inner-pad" for="exampleInputEmail1">Annual Turnover</label>
                    <div class="col-md-6 inner-pad">
                        <span><?php echo $companyInfo['annual_turnover']; ?></span>
                    </div>
                </div>
                <?php
                foreach ($userDocuments as $userDocument) {
                    $ext = pathinfo($userDocument['document'], PATHINFO_EXTENSION);
                    ?>
                    <div class="form-group clearfix">
                        <label class="col-md-6 inner-pad" for="<?php echo $userDocument['field']; ?>"><?php echo $userDocument['label']; ?></label>
                        <div class="col-md-6 inner-pad">
                            <span><a href="<?php echo $userDocument['document']; ?>" download="<?php echo $userDocument['label'].'.'.$ext; ?>">Download</a></span>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <div class="submit">
                    <input class="btn btn-default" value="Submit" type="submit">
                </div>
            </form>

        </div>
    </div>
</div>
<script>
    $(function () {
        // Tab switching between steps -------------------------------------------------------------------
        $('.tab-1').click(function () {
            $('#tab-1').show();
            $('#tab-2').hide();
            $('.tab-1').addClass('active');
            $('.tab-2').removeClass('active');
        });

        $('.tab-2').on('click', function () {
            $('#tab-2').show();
            $('#tab-1').hide();
            $('.tab-2').addClass('active');
            $('.tab-1').removeClass('active');
        });
    });
</script>            
