<script>
    swal({
        title: 'Payment for Registration',
        text: "Rs.10000",
        type: 'warning',
        showCancelButton: false,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Pay Now',
        cancelButtonText: 'Cancel!',
        allowOutsideClick: false
    }).then(function () {
        var docHeight = $(document).height();
        $('#loaderLayout span').css({height: docHeight + 'px'});
        $('#loaderLayout').show();
        $('#payForm').submit();
    }, function (dismiss) {

    });
</script>
<form method="post" name="redirect" action="<?php echo $url; ?>" id="payForm">
    <input type="hidden" name="encRequest" value="<?php echo $request;?>">
    <input type="hidden" name="access_code" value="<?php echo $accessCode;?>">
</form>
