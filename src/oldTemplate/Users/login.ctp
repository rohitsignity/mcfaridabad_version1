<section class="content-wrap">
    <div class="container">
        <div class="row">
            <!-- Login section -->
                <div class="login-sec">
                    <h1>LOG IN</h1>
                    <?php echo $this->Form->create($post); ?>
                    <div class="form-wrap">
                        <?php
                        echo $this->Form->input('email', ['type' => 'text', 'class' => 'form-control', 'label' => 'Email', 'templates' => [
                                'inputContainer' => '<div class="form-group">{{content}}</div>'
                            ], 'placeholder' => 'Enter your email']);
                        echo $this->Form->input('password', ['type' => 'password', 'class' => 'form-control', 'label' => 'Password', 'templates' => [
                                'inputContainer' => '<div class="form-group">{{content}}</div>'
                            ], 'placeholder' => 'Password']);
                        ?>
                        <?php echo $this->Form->input('remember', ['type' => 'checkbox', 'hiddenField' => false, 'label' => 'Remember me']); ?>
                        <p style="color: #782669; font-style: italic;">*Disclaimer- This is a newly launched website by MCF. In case of any error/omission, MCF reserves the right to raise additional claims on the applicant.</p>
                    </div>
                    <?php echo $this->Form->button('Login', ['class' => 'btn btn-default', 'formnovalidate' => true]); ?>
                    <a href="<?php echo $this->Url->build(["controller" => "Users", "action" => "forgotPassword"]); ?>" class="forgtpwd">Forgot password ?</a>
                    <?php echo $this->Form->end(); ?>
                </div>
           
            <!--/ Login section -->
        </div>                
    </div>
</section>
