<section class="content-wrap">
    <div class="container">
        <div class="row">
            <!-- Register section -->
            <div class="col-md-6">
                <div class="login-sec regist">
                    <h1>REGISTER</h1>
                    <?php
                    echo $this->Form->create($post, ['novalidate' => true, 'enctype' => 'multipart/form-data']);
                    echo $this->Form->input('display_name', ['type' => 'text', 'class' => 'form-control', 'label' => 'Full Name', 'templates' => [
                            'inputContainer' => '<div class="form-group">{{content}}</div>'
                        ], 'placeholder' => 'Full Name']);
                    echo $this->Form->input('email', ['type' => 'text', 'class' => 'form-control', 'label' => 'Email', 'templates' => [
                            'inputContainer' => '<div class="form-group">{{content}}</div>'
                        ], 'placeholder' => 'Email']);
                    echo $this->Form->input('mobile', ['type' => 'text', 'class' => 'form-control', 'label' => 'Mobile', 'templates' => [
                            'inputContainer' => '<div class="form-group">{{content}}</div>'
                        ], 'placeholder' => 'Mobile']);
                    echo $this->Form->input('password', ['type' => 'password', 'class' => 'form-control', 'label' => 'Password', 'templates' => [
                            'inputContainer' => '<div class="form-group">{{content}}</div>'
                        ], 'placeholder' => 'Password']);
                    echo $this->Form->input('conf_password', ['type' => 'password', 'class' => 'form-control', 'label' => 'Confirm Password', 'templates' => [
                            'inputContainer' => '<div class="form-group">{{content}}</div>'
                        ], 'placeholder' => 'Confirm Password']);
                    echo $this->Form->input('security_qus_id', ['empty' => 'Select Security Question', 'type' => 'select', 'options' => $securityQuestions, 'label' => 'Security Questions', 'class' => 'form-control', 'templates' => [
                            'inputContainer' => '<div class="form-group">{{content}}</div>'
                    ]]);
                    echo $this->Form->input('answer', ['type' => 'textarea', 'class' => 'form-control', 'label' => false, 'templates' => [
                            'inputContainer' => '<div class="form-group">{{content}}</div>'
                        ], 'placeholder' => 'Your Answer']);
                    echo $this->Form->input('representative_id', ['empty' => 'Select Representative Entity', 'type' => 'select', 'options' => $representatives, 'label' => 'Entity Type', 'class' => 'form-control', 'templates' => [
                            'inputContainer' => '<div class="form-group">{{content}}</div>'
                    ]]);
                    ?>
                    <div id="entityInputs" style="<?php echo ($isEntity == 0) ? 'display: none;' : ''; ?>">
                        <?php
                        echo $this->Form->input('company_name', ['type' => 'text', 'class' => 'form-control', 'label' => 'Company Name', 'templates' => [
                            'inputContainer' => '<div class="form-group">{{content}}</div>'
                        ], 'placeholder' => 'Company Name']);
                        echo $this->Form->input('director_name', ['type' => 'text', 'class' => 'form-control', 'label' => 'Director Name', 'templates' => [
                            'inputContainer' => '<div class="form-group">{{content}}</div>'
                        ], 'placeholder' => 'Director Name']);
                        echo $this->Form->input('company_address', ['type' => 'text', 'class' => 'form-control', 'label' => 'Company Address', 'templates' => [
                            'inputContainer' => '<div class="form-group">{{content}}</div>'
                        ], 'placeholder' => 'Company Address']);
                        echo $this->Form->input('incorporation_date', ['type' => 'text', 'class' => 'form-control incorporationDate', 'autocomplete' => 'off', 'readonly' => 'readonly', 'label' => 'Incorporation Date', 'templates' => [
                            'inputContainer' => '<div class="form-group">{{content}}</div>'
                        ], 'placeholder' => 'Incorporation Date']);
                        echo $this->Form->input('company_mobile', ['type' => 'text', 'class' => 'form-control', 'label' => 'Mobile', 'templates' => [
                            'inputContainer' => '<div class="form-group">{{content}}</div>'
                        ], 'placeholder' => 'Mobile']);
                        echo $this->Form->input('company_email', ['type' => 'text', 'class' => 'form-control', 'label' => 'Email', 'templates' => [
                            'inputContainer' => '<div class="form-group">{{content}}</div>'
                        ], 'placeholder' => 'Email']);
                        echo $this->Form->input('annual_turnover', ['type' => 'text', 'class' => 'form-control', 'label' => 'Annual Turnover', 'templates' => [
                            'inputContainer' => '<div class="form-group">{{content}}</div>'
                        ], 'placeholder' => 'Annual Turnover']);
                        echo $this->Form->input('SMC_party_code', ['type' => 'text', 'class' => 'form-control', 'label' => 'SMC Party Code', 'templates' => [
                            'inputContainer' => '<div class="form-group">{{content}}</div>'
                        ], 'placeholder' => 'SMC Party Code']);
                        echo $this->Form->input('company_password', ['type' => 'password', 'class' => 'form-control', 'label' => 'Password', 'templates' => [
                            'inputContainer' => '<div class="form-group">{{content}}</div>'
                        ], 'placeholder' => 'Password']);
                        echo $this->Form->input('conf_company_password', ['type' => 'password', 'class' => 'form-control', 'label' => 'Confirm Password', 'templates' => [
                            'inputContainer' => '<div class="form-group">{{content}}</div>'
                        ], 'placeholder' => 'Confirm Password']);
                        echo $this->Form->input('income_proof', ['class' => 'form-control','type' => 'file']);
                        echo $this->Form->input('turnover_certificate', ['class' => 'form-control','type' => 'file']);
                        echo $this->Form->input('exp_certificate', ['class' => 'form-control','type' => 'file']);
                        ?>
                    </div>
                    <!-- <div class="form-group">
                        <label for="" class="amount">Registration Amount : 2000/s</label>
                    </div> -->
                    <div class="checkbox">
                        <label>
                            <?php echo $this->Form->input('terms', ['type' => 'checkbox', 'hiddenField' => false, 'label' => false, 'templates' => ['inputContainer' => '{{content}}']]);?>
                            <p class="termsc">I/we shall hereby abide <b class="orang">terms & conditions</b> and guidelines of advertisement by laws framed by the SMC and all the information listed above is true and genuine and in case of adverse findings related to above information, the registration shall stand canceled.</p>
                        </label>
                    </div>
                    <button type="submit" class="btn btn-default">Register</button>
                    </form>
                </div>
            </div>
            <!--/ Login section -->
        </div>                
    </div>
</section>
<script>
    $(document).ready(function(){
        //$("#entityInputs").hide();
        $('select[name=representative_id]').change(function() {
            if($(this).val() == 1 || $(this).val() == '')
            {
                $("#entityInputs").hide();
            }
            else{
                $("#entityInputs").show();
                }
        });
        $(".incorporationDate").datepicker({
            changeMonth: true,
            changeYear: true,
            minDate: new Date(),
            dateFormat: 'yy-mm-dd'
        });
    });
    
//    $('#register').on('submit', function (e) {
//        $(document).find('.error-message').remove();
//        e.preventDefault();
//        var data = $(this).serialize();
//        $('#loaderLayout').show();
//        $.ajax({
//            url: '<?php //echo $this->Url->build([ "controller" => "Users", "action" => "register"]); ?>',
//            type: 'POST',
//            data: data,
//            dataType: 'JSON',
//            success: function (response) {
//                console.log('success');
//                $('#loaderLayout').hide();
//                if (response.error === 1) {
//                    console.log(response.data);
//                    $.each(response.data, function (index, val) {
//                        $('#register [name="' + index + '"]').after('<p class="error-message">' + val[Object.keys(val)[0]] + '</p>');
//                    });
//                } else {
//                    //window.location.href = response.data;
//                    alert(response.data);
//                }
//            },
//            error: function (error) {
//             console.log('error');
//                console.log(error);
//            }
//        });
//    });
</script>