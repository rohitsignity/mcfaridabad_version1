<div class="page-content inset dashboard">
    <div class="device-list">
        <h2>Users List</h2>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-default">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>Created At</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($users)) {
                        foreach ($users as $user) {
                            ?>
                            <tr userRow="<?php echo $user['id']; ?>" class="userRow">
                                <th scope="row"><?php echo $user['id']; ?></th>
                                <td><?php echo $user['display_name']; ?></td>
                                <td><?php echo $user['email']; ?></td>
                                <td><?php echo $user['mobile']; ?></td>
                                <td><?php echo $user['type']; ?></td>
                                <td><?php echo $user['status'] ? 'Enable' : 'Pending'; ?>
                                <?php if($user['admin_approved'] == 1){ echo "(Pending MCF4)"; } else if($user['admin_approved'] == 0) { echo "(Pending MCF1)"; }?>
                                </td>
                                <!-- <td><?php if($user['status'] == 2){ echo "Approved"; } else if($user['status'] == 1){ echo "Pending (MCF 4)"; }else{ echo "Pending";}?></td> -->
                                <td><?php echo date('d M, Y h:i A',strtotime($user['created']));?></td>
                                <td><a href="javascript:void(0)"><i class="glyphicon glyphicon-menu-hamburger"></i></a></td>
                            </tr>							
                            <?php
                        }
                        if ($this->Paginator->numbers()) {
                            ?>
                            <tr>
                                <td colspan="8"><ul class="pagingPages"><?php echo $this->Paginator->prev() . $this->Paginator->numbers() . $this->Paginator->next(); ?></ul></td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr><td colspan="8" style="text-align: center;">No Record Found</td></tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row" id="deviceBlock" style="display: none;">
        <?php
        echo $this->Form->hidden('user_id', ['value'=>'']);
        ?>
        <div class="col-md-6">
            <div class="mcgform regist-form" id="companyBlock">
                <h2 class="formh2">User Details</h2>
                <div class="row">
                    <form class="prevwfm"></form>
                </div>
            </div>
        </div>
        <div class="col-md-6" id="entityDetail">
            <div class="mcgform regist-form" id="entityBlock">
                <h2 class="formh2">Entity Details</h2>
                <div class="row">
                    <form class="prevwfm"></form>
                </div>
            </div>
        </div>
    </div>
<!--    <div id="entityCertificates" style="display: none;">
        <a href=""><input type="button" value="Download Income Proof"></a>
        <a href=""><input type="button" value="Download Turnover Certificate"></a>
        <a href=""><input type="button" value="Download Experience Certificate"></a>
    </div>-->
            <div class="siteimg" id="entityCertificates" style="display: none;">
            <h2 class="imgsite formh2">Company Documents</h2>
            <div class="col-md-4" >
                <?php
                echo $this->Html->link(
                        '<i class="glyphicon glyphicon-save"></i>', '', ['class' => 'downloadDocument', 'id' => 'document_first', 'target' => '_blank', 'escape' => false,'download' => 'pan_card.jpg', 'title' => "Download pan card", 'data-original-title' => 'Edit',]
                );
                ?>
                <p style="text-align: center">Pan Card</p>
            </div>
            <div class="col-md-4" >
                <?php
                echo $this->Html->link(
                        '<i class="glyphicon glyphicon-save"></i>', '', ['class' => 'downloadDocument', 'id' => 'document_second', 'target' => '_blank', 'escape' => false,'download' => 'services_tax.jpg', 'title' => "Download Service tax certificate"]
                );
                ?>
                <p style="text-align: center">Service tax certificate</p>
            </div>
            <div class="col-md-4">
                <?php
                echo $this->Html->link(
                        '<i class="glyphicon glyphicon-save"></i>', '', ['class' => 'downloadDocument', 'id' => 'document_third', 'target' => '_blank', 'escape' => false,'download' => 'turnover.jpg', 'title' => "Download Turnover certificate(for preceding 3 financial years)"]
                );
                ?>
                <p style="text-align: center">Turnover certificate(for preceding 3 financial years)</p>
            </div>
        </div>
        <div style="clear:both;"></div>
        <div id="addionalDocs" class="siteimg" style="display: none;">

        </div>
        
        <div style="clear:both;"></div>
        <div id="previous-comments" class="comt-blk" style="display: none;" >
			
				</div>	
			  
        <div id="approveUsers" style="display: none;">
            <div id="pending">
            <?php
            if($usertype == "mcg1"){
                echo $this->Form->input('rejection_reason', ['type' => 'textarea', 'class' => 'form-control', 'label' =>'Comments','templates' => [
                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                ], 'placeholder' => 'Please comment here.']);
                echo $this->Form->button('Approve', array(
                 'type' => 'button',
                 'class' => 'btn btn-default approvd commentSubmit',
                  'style'=>'border-radius: 4px; margin-right: 11px;',
                 'onclick' => 'approveUsers(1);',
                 ));
//                echo $this->Form->button('Reject', array(
//                 'type' => 'button',
//                 'class' => 'btn btn-default hld commentSubmit',
//                 'style'=>'border-radius: 4px; margin-right: 11px;',
//                 'onclick' => 'approveUsers(3);',
//                 ));
                echo $this->Form->button('Request Additional Documents', array(
                 'type' => 'button',
                 'class' => 'btn btn-default approvd commentSubmit',
                 'style'=>'border-radius: 4px; margin-right: 11px; width: auto;',
                 'onclick' => 'Vendor.requestRegisterDocuments()',
                 ));
            }else{
                echo $this->Form->input('rejection_reason', ['type' => 'textarea', 'class' => 'form-control', 'label' => Null,'templates' => [
                    'inputContainer' => '<div class="form-group">{{content}}</div>'
                ], 'placeholder' => 'Please give reason in case of rejection or clarification.']);
                echo $this->Form->button('Approve', array(
                 'type' => 'button',
                 'class' => 'btn btn-default approvd commentSubmit',
                  'style'=>'border-radius: 4px; margin-right: 11px;',
                 'onclick' => 'approveUsers(2);',
                 ));
                echo $this->Form->button('Reject', array(
                 'type' => 'button',
                 'class' => 'btn btn-default hld commentSubmit',
                 'style'=>'border-radius: 4px; margin-right: 11px;',
                 'onclick' => 'approveUsers(3);',
                 ));
                echo $this->Form->button('Request Clarification', array(
                 'type' => 'button',
                 'class' => 'btn btn-default commentSubmit',
                 'style'=>'border-radius: 4px; margin-right: 11px;',
                 'onclick' => 'approveUsers(1);',
                 ));
            }
            ?>
            <p id="reason_error"></p>
            </div>
        </div>
    </div>
<div class="modal fade" id="documentsModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Request Additional Documents</h4>
            </div>
            <div class="modal-body" id="documentsList" style="float: left; width: 100%;">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer"><p style="text-align: center;">Copyright © <?php echo date('Y'); ?> Outdoor Media Management System. All Rights Reserved.</p></div>
        </div>

    </div>
</div>
<script>
    var Vendor = {
        userId: 0,
        entityData: 0,
        downloadPath: '<?php echo  $this->request->webroot; ?>'+'uploads/register_documents/',
        userDetaiils: JSON.parse('<?php echo $userDetails; ?>'),
        comments	: JSON.parse('<?php echo $comments; ?>'),
        entityDetaiils: JSON.parse('<?php echo $entityDetails; ?>'),
        userDocuments: JSON.parse('<?php echo $usersDocuments;?>'),
        userAdditionalDocs: JSON.parse('<?php echo $usersAdditionalDocuments;?>'),
        usertype: '<?php echo $usertype; ?>',
        rejectionReason: JSON.parse('<?php echo json_encode($rejectionReassons);?>'),
        requestDocUrl: '<?php echo $this->Url->build(['action' => 'requestAdditionalDocs']); ?>',
        showLoader: function () {
            var docHeight = $(document).height();
            $('#loaderLayout span').css({height: docHeight + 'px'});
            $('#loaderLayout').show();
        },
        hideLoader: function () {
            $('#loaderLayout').hide();
        },
        getUserDetail: function () {
            var self = this;
            $("#pending").show();
            $('#entityCertificates').hide();
            var userDetail = self.userDetaiils[self.userId];
            $.each(userDetail,function(key,val){
                if(val.label == "admin_approved"){
                    if(val.value == 1 && self.usertype == 'mcg1'){
                        $(".siteimg").css('min-height','100px');
                        $("#pending").hide();
                    }else if(val.value == 1 && self.usertype == 'mcg4'){
                        $("#pending").show();
                    }else if(val.value == 0 && self.usertype == 'mcg4'){
//                        $(".siteimg").css('min-height','1000px');
                        $("#pending").hide();
                    }
                }
            });
            var representativeArr = $.grep(userDetail,function(val){
                return val.label === "Representative_id";
            });
            var representative = representativeArr[0];
            self.entityData = parseInt(representative.value);
            this.getHTML();
        },
        getHTML: function () {
            $("#approveUsers").show();
            $("input[name='user_id']").val(this.userId);
            this.userDetailHTML();
            this.entityHTML();
            this.filesHTML();
            this.addionalDocumentsHtml();
            this.commentsection();
        },
        
				commentsection : function(){
					var self = this;
					$('#previous-comments').html('');
					$('#previous-comments').hide();
					if(self.comments[self.userId]) {
						var html = '<label for="exampleInputEmail1"><h2 class="imgsite formh2">Previous Comments</h2></label>';
						html += '<div class="form-group">';
						let comments_rec 	=	self.comments[self.userId];
						$.each(comments_rec,function(index,val){
							html += '<div class="form-group clearfix"><label class="col-md-12 darkgrey" for="">Comment '+(index+1)+'</label></div>';
							html += '<div class="form-group clearfix"><label class="col-md-6" for="">User Name</label><div class="col-md-6"><span>'+val.type+'</span></div></div>';
							html += '<div class="form-group clearfix"><label class="col-md-6" for="">Comment</label><div class="col-md-6"><span>'+val.comment+'</span></div></div>';
							html += '<div class="form-group clearfix"><label class="col-md-6" for="">Created At</label><div class="col-md-6"><span>'+val.date+'</span></div></div>';
						});
						
						$('#previous-comments').html(html);
						$('#previous-comments').show();
					}
					//console.log(self.comments[self.userId]);
				},
        
        userDetailHTML: function () {
            var html = '';
            var self = this;
            var userDetail = $.grep(self.userDetaiils[self.userId],function(val){
                return val.type === 'field';
            });
            var extRegex = /(?:\.([^.]+))?$/;
            $.each(userDetail, function (index, val) {
                var fileExt = extRegex.exec(val.value);
                if (val.label == 'income_proof')
                {
                    if (val.value != null)
                    {
                        $('#entityCertificates').show();
                        $('#document_first').attr('href', self.downloadPath + (val.value));
                        $('#document_first').attr('download', 'pan_card.' + fileExt[1]);
                    }
                } else if (val.label == 'turnover_certificate')
                {
                    if (val.value != null)
                    {
                        $('#entityCertificates').show();
                        $('#document_second').attr('href', self.downloadPath + (val.value));
                        $('#document_second').attr('download', 'services_tax.' + fileExt[1]);
                        $('#document_second').parent('div.col-md-4').show();
                    }
                    if(self.entityData === 1) {
                        $('#document_second').parent('div.col-md-4').hide();
                    }
                } else if (val.label == 'exp_certificate')
                {
                    if (val.value != null)
                    {
                        $('#entityCertificates').show();
                        $('#document_third').attr('href', self.downloadPath + (val.value));
                        $('#document_third').attr('download', 'turnover.' + fileExt[1]);
                        $('#document_third').parent('div.col-md-4').show();
                    }
                    if(self.entityData === 1) {
                        $('#document_third').parent('div.col-md-4').hide();
                    }
                } else if(val.label !== "Representative_id") {
                    if(val.label !== 'admin_approved') {
                        html += '<div class="form-group clearfix">';
                        html += '<label class="col-md-6" for="">' + val.label + '</label>';
                        html += '<div class="col-md-6">';
                        html += '<span>' + val.value + '</span>';
                        html += '</div>';
                        html += '</div>';
                    }
                }
            });
            $('#companyBlock form').html(html);
        },
        entityHTML: function () {
            var self = this;
            var html = '';
            var entityDetaiils = this.entityDetaiils[this.userId];
            var excluded = ['SMC Party Code'];
            if(self.entityData === 1) {
                excluded = ['Company Name','Director Name','Annual Turnover','Date of Incorporation','SMC Party Code'];
            }
            $.each(entityDetaiils, function (index, val) {
                if($.inArray(val.label,excluded) == -1) {
                    var label = (val.label === 'Company Address' ? 'Address' : val.label);
                    html += '<div class="form-group clearfix">';
                    html += '<label class="col-md-6" for="">' + label + '</label>';
                    html += '<div class="col-md-6">';
                    html += '<span>' + val.value + '</span>';
                    html += '</div>';
                    html += '</div>';
                }
            });
            $('#entityBlock form').html(html);
        },
        filesHTML: function() {
            var self = this;
            var html = '<h2 class="imgsite formh2">Documents</h2>';
            var documents = self.userDocuments[self.userId];
            var extRegex = /(?:\.([^.]+))?$/;
            $.each(documents,function(index,file){
                var fileExt = extRegex.exec(file.document);
                if(file.document) {
                    html += '<div class="col-md-4" style="min-height:180px;">';
                    html += '<a href="'+file.document+'" class="downloadDocument" download="'+file.label+'.'+fileExt[1]+'" title="Download pan card"><i class="glyphicon glyphicon-save"></i></a>';
                    html += '<p style="text-align: center">'+file.label+'</p>';
                    html += '</div>';
                }
            });
            $('#entityCertificates').html(html);
            $('#entityCertificates').show();
        },
        addionalDocumentsHtml: function() {
            var self = this;
            var data = self.userAdditionalDocs[self.userId];
            var extRegex = /(?:\.([^.]+))?$/;
            $('#addionalDocs').html('');
            $('#addionalDocs').hide();
            if(typeof data !== 'undefined') {
                var html = '<h2>Additional Documents</h2>';
                $.each(data,function(date,val){
                    html += '<div class="col-md-12">';
                    html += '<h3>Requested At ' + date +'</h3>';
                    $.each(val,function(index,valData){
                        var fileExt = extRegex.exec(valData.filename);
                        html += '<div class="col-md-4" style="min-height:180px;">';
                        html += '<a href="'+ valData.filename +'" class="downloadDocument" download="' + valData.name + '.'+ fileExt[1] +'" title="Download ' + valData.name + '"><i class="glyphicon glyphicon-save"></i></a>';
                        html += '<p style="text-align: center">' + valData.name + '( '+ valData.reason +' )' + '</p>';
                        html += '</div>';
                    });
                    html += '</div>';
                });
                $('#addionalDocs').html(html);
                $('#addionalDocs').show();
            }
        },
        requestRegisterDocuments: function () {
            var html = '<form action="javascript:void(0)" onsubmit="Vendor.saveDocRequest(this)">';
            html += '<input type="hidden" name="user_id" value="' + this.userId + '">';
            html += '<div class="col-md-11 requested-doc-con">';
            html += '<span style="float: left; width: 49%;"><input type="text" class="form-control requested-doc" placeholder="Document Name" name="document[' + this.docCounter + ']" style="float: left;"></span>';
            html += '<span style="float: right; width: 49%;"><input type="text" class="form-control requested-doc-reason" placeholder="Reason" name="reason[' + this.docCounter + ']" style="float: right;"></span>';
            html += '</div>';
            html += '<a href="javascript:void(0)" style="float: left; margin: 0 0 0 18px;" onclick="Vendor.addMoreDocs(this)">Add More Document</a>';
            html += '<div class="col-md-11">';
            html += '<input type="submit" class="btn btn-default approvd commentSubmit" style="margin: 13px 0 0 0" value="Hold">';
            html += '</div>';
            html += '</form>';
            $('#documentsModal').modal({backdrop: 'static', keyboard: false});
            $('#documentsList').html(html);
        },
        addMoreDocs: function (element) {
            this.docCounter++;
            var html = '<div class="col-md-11 requested-doc-con">';
            html += '<span style="float: left; width: 49%;"><input type="text" class="form-control requested-doc" placeholder="Document Name" name="document[' + this.docCounter + ']" style="float: left;"></span>';
            html += '<a href="javascript:void(0)" style="float: right; margin: 16px -17px 0 0" onclick="Vendor.removeDoc(this)">X</a>';
            html += '<span style="float: right; width: 49%;"><input type="text" class="form-control requested-doc-reason" placeholder="Reason" name="reason[' + this.docCounter + ']" style="float: right;"></span>';
            html += '</div>';
            $(element).before(html);
        },
        removeDoc: function (element) {
            $(element).parent('div').remove();
        },
        saveDocRequest: function (element) {
            var error = 0;
            var self = this;
            $(document).find('.error-message').remove();
            $(element).find('.requested-doc-con').each(function () {
                var docElement = $(this).find('.requested-doc');
                var reasonElement = $(this).find('.requested-doc-reason');
                var doc = $.trim(docElement.val());
                var reason = $.trim(reasonElement.val());
                if (!doc) {
                    error = 1;
                    docElement.after('<p class="error-message">Please enter document Name</p>');
                }
                if (!reason) {
                    error = 1;
                    reasonElement.after('<p class="error-message">Please enter reason</p>');
                }
            });
            if (!error) {
                self.showLoader();
                $.ajax({
                    url: self.requestDocUrl,
                    type: 'POST',
                    data: $(element).serialize(),
                    dataType: 'JSON',
                    success: function (response) {
                        $("#documentsModal").modal('hide');
                        self.hideLoader();
                        if (!response.error) {
                            swal("Success!", "Request has been sent successfully.", "success");
                            location.reload();
                        } else {
                            swal("Alert!", response.message, "info");
                        }
                    },
                    error: function (result) {
                        self.hideLoader();
                        $("#documentsModal").modal('hide');
                        swal("Error!", "Something Went wrong on Server. Please try agian.", "error");
                    }
                });
            }
        }
    };


    $('.userRow').on('click', function () {
        $('.userRow').removeClass('active-device');
        $(this).addClass('active-device');
        Vendor.userId = $(this).attr('userRow');
        Vendor.getUserDetail();
        $('#deviceBlock').slideDown();
    });
    
    function approveUsers(e)
    {  

        var usertype  = '<?php echo $usertype; ?>';
        var docHeight = $(document).height();
        $('#loaderLayout span').css({height: docHeight + 'px'});

        var changeStatus = 0;
        var reason = '';


        if(usertype == "mcg1"){
            
            if(e == 3)
            {
                reason = $.trim($("textarea#rejection-reason").val());
                if(reason == '')
                {
                    $("#reason_error").css("color","red");
                    $("#reason_error").html('Reason of rejection is required');
                    return false;
                }
                else
                {
                    $('#loaderLayout').show();
                    changeStatus = 1;
                }
            } else if(e == 1) {
								//============= add by rohit ================
								reason = $.trim($("textarea#rejection-reason").val());
								if(reason == '')
                {
                    $("#reason_error").css("color","red");
                    $("#reason_error").html('Comment  is required');
                    return false;
                } else {
									//===========================================
									$('#loaderLayout').show();
									changeStatus = 1;
								}
            }
            
            if(changeStatus == 1)
            {
				//============= add by rohit ================
                var user_id = $('input[name="user_id"]').val();
                $.ajax({
                    url: '<?php echo $this->Url->build(["controller" => "Mcg", "action" => "approveUsers"]); ?>',
                    data: {user_id: user_id, value: e, rejection_reason: reason},
                    dataType: 'JSON',
                    type: 'POST',
                    //xhrFields: { responseType: "json" },
                    success: function (response) {
                        $('#loaderLayout').hide();
                        swal("Success", response.data,"success");
                        location.reload();
                    },
                    error: function (error) {
                        $('#loaderLayout').hide();
                        swal("Error!","Something went wrong on the Server. Please try again.","error");
                    }
                });
            }
        }else if(usertype == 'mcg4'){

            if(e == 1)
            {
                reason = $.trim($("textarea#rejection-reason").val());
                if(reason == '')
                {
                    $("#reason_error").css("color","red");
                    $("#reason_error").html('Reason of Clarification is required');
                    return false;
                }
                else
                {
                    $('#loaderLayout').show();
                    changeStatus = 1;
                }
            }
            else if(e == 3)
            {
                reason = $.trim($("textarea#rejection-reason").val());
                if(reason == '')
                {
                    $("#reason_error").css("color","red");
                    $("#reason_error").html('Reason of rejection is required');
                    return false;
                }
                else
                {
                    $('#loaderLayout').show();
                    changeStatus = 1;
                }
            }
            else if(e == 2)
            {
								//reason = $.trim($("textarea#rejection-reason").val());
                $('#loaderLayout').show();
                changeStatus = 1;
            }
            
            if(changeStatus == 1)
            {
                var user_id = $('input[name="user_id"]').val();
                $.ajax({
                    url: '<?php echo $this->Url->build(["controller" => "Mcg", "action" => "approveUsers"]); ?>',
                    data: {user_id: user_id, value: e, rejection_reason: reason},
                    dataType: 'JSON',
                    type: 'POST',
                    //xhrFields: { responseType: "json" },
                    success: function (response) {
                        $('#loaderLayout').hide();
                        swal("Success", response.data,"success");
                        location.reload();
                    },
                    error: function (error) {
                        $('#loaderLayout').hide();
                        swal("Error!","Something went wrong on the Server. Please try again.","error");
                    }
                });
            }
            
        }




    }
</script>
