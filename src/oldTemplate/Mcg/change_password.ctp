<!-- Keep all page content within the page-content inset div! -->
<div class="page-content inset app-status">
    <div class="headings-all dash-in">
        <h1 class="text-center">Change Password</h1>
    </div>
    <div class="regist-form profile-manage clearfix">
        <div class="profile-img"><span class="glyphicon glyphicon-user"></span></div>
        <div class="changepwd" id="tab-1">
            <?php echo $this->Form->create($userData); ?>		 
            <div class="form-group">
                <?php
                echo $this->Form->input('current_password', ['type' => 'password', 'class' => 'form-control', 'label' => 'Current Password', 'templates' => [
                        'inputContainer' => '{{content}}'
                    ], 'placeholder' => 'Enter Current Password']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->input('new_password', ['type' => 'password', 'class' => 'form-control', 'label' => 'New Password', 'templates' => [
                        'inputContainer' => '{{content}}'
                    ], 'placeholder' => 'Enter New Password']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->input('confirm_password', ['type' => 'password', 'class' => 'form-control', 'label' => 'Confirm Password', 'templates' => [
                        'inputContainer' => '{{content}}'
                    ], 'placeholder' => 'Enter Confirm Password']);
                ?>	
            </div>
            <div class="submit">
                <?php echo $this->Form->button('Submit', ['class' => 'btn btn-default', 'formnovalidate' => true]) ?>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>
<script>
    $(function () {
        // Tab switching between steps -------------------------------------------------------------------
        $('.tab-1').click(function () {
            $('#tab-1').show();
            $('#tab-2').hide();
            $('.tab-1').addClass('active');
            $('.tab-2').removeClass('active');
        });

        $('.tab-2').on('click', function () {
            $('#tab-2').show();
            $('#tab-1').hide();
            $('.tab-2').addClass('active');
            $('.tab-1').removeClass('active');
        });
    });
</script>            
