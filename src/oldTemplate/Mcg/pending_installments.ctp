<div id="page-content-wrapper">
    <!-- Keep all page content within the page-content inset div! -->
    <div class="page-content inset dashboard">
        <div class="dash-head">
            <h1>Pending Installments</h1>
        </div>
        <div class="device-list">
            <?php
              //  pr($installments);
            ?>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Installment Id</th>
                            <th>Amount</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Applicant Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if ($installments) {
                            foreach ($installments as $installment) {
                                ?>
                                <tr>
                                    <td><?php echo 'OMD' . $installment['id']; ?></td>
                                    <td><?php echo 'Rs. '.number_format($installment['amount'], 2); ?></td>
                                    <td><?php echo date('d M, Y', strtotime($installment['start_date'])); ?></td>
                                    <td><?php echo date('d M, Y', strtotime($installment['end_date'])); ?></td>
                                    <td><?php echo $installment['device']['company_name']; ?></td>
                                    <td><a href="mailto: <?php echo $installment['device']['email']; ?>"><?php echo $installment['device']['email']; ?></a></td>
                                    <td><?php echo $installment['device']['phone']; ?></td>
                                </tr>
                                <?php
                            }
                            if ($this->Paginator->numbers()) {
                                ?>
                                <tr>
                                    <td colspan="7"><ul class="pagingPages"><?php echo $this->Paginator->prev() . $this->Paginator->numbers() . $this->Paginator->next(); ?></ul></td>
                                </tr>
                                <?php
                            }
                        } else {
                            ?>
                            <tr><td colspan="7" style="text-align: center;">No Pending Installments Found</td></tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>