<div class="page-content inset dashboard">
    <h1>Device Detail</h1>
    <div class="row" id="deviceBlock" style="">
        <input name="user_id" value="35" type="hidden">        <div class="col-md-6">
            <div class="mcgform regist-form" id="companyBlock">
                <h2 class="formh2">Device Detail</h2>
                <div class="row">
                    <form class="prevwfm">
                        <div class="form-group clearfix">
                            <label class="col-md-6" for="">Status</label>
                            <div class="col-md-6">
                                <span><?php echo ucfirst($devideData['status']); ?></span>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-md-6" for="">Created at</label>
                            <div class="col-md-6">
                                <span><?php echo date('d M, Y h:iA', strtotime($devideData['created'])); ?></span>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-md-6" for="">Device Id</label>
                            <div class="col-md-6">
                                <span><?php echo $devideData['id']; ?></span>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-md-6" for="">Typology</label>
                            <div class="col-md-6">
                                <span><?php echo $devideData['typology']; ?></span>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="col-md-6" for="">Sub Category</label>
                            <div class="col-md-6">
                                <span><?php echo $devideData['sub_category']; ?></span>
                            </div>
                        </div>
                        <?php
                        foreach ($devideData['fields'] as $field) {
                            $fieldKey = $field['field'];
                            $fieldData = array_values(array_filter($devideData['fieldsData'], function($arr) use($fieldKey) {
                                        return $arr['field_key'] == $fieldKey;
                                    }));
                            if ($fieldData[0]['type'] == 'file' || $fieldData[0]['type'] == 'json') {
                                continue;
                            }
                            ?>
                            <div class="form-group clearfix">
                                <label class="col-md-6" for=""><?php echo $fieldData[0]['lable']; ?></label>
                                <div class="col-md-6">
                                    <span><?php echo $field['value']; ?></span>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6" id="entityDetail">
            <div class="mcgform regist-form" id="entityBlock">
                <h2 class="formh2">Documents</h2>
                <div class="row">
                    <form class="prevwfm">
                        <?php
                        $fileCounter = 0;
                        foreach ($devideData['fields'] as $field) {
                            $fieldKey = $field['field'];
                            $fieldData = array_values(array_filter($devideData['fieldsData'], function($arr) use($fieldKey) {
                                        return $arr['field_key'] == $fieldKey;
                                    }));
                            if ($fieldData[0]['type'] != 'file') {
                                continue;
                            }
                            $ext = pathinfo($field['value'], PATHINFO_EXTENSION);
                            ?>
                            <div class="form-group clearfix">
                                <label class="col-md-6" for=""><?php echo $fieldData[0]['lable']; ?></label>
                                <div class="col-md-6">
                                    <span><a href="<?php echo $this->Url->build('/uploads/devices/' . $field['value']); ?>" download="<?php echo $fieldData[0]['lable']; ?>.<?php echo $ext; ?>">Download</a></span>
                                </div>
                            </div>
                            <?php
                            $fileCounter++;
                        }
                        if (!$fileCounter) {
                            ?>
                            <p>No document for this device</p>
                            <?php
                        }
                        ?>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <?php
            $omdOneRoofData = array_values(array_filter($devideData['fields'], function($arr) {
                        return $arr['field'] == 'omd_one_roof';
                    }));
            $omdPremisesData = array_values(array_filter($devideData['fields'], function($arr) {
                        return $arr['field'] == 'omd_premises';
                    }));
            $typologyALabelsTypes = ['shelters_type','route_markers_type','foot_over_type','cycle_station_type','police_booth_type','sitting_bench_type'];
            $typologyALabels = array_values(array_filter($devideData['fields'],function($arr) use($typologyALabelsTypes){
                return in_array($arr['field'],$typologyALabelsTypes);
            }));
            $typologyAData = array_values(array_filter($devideData['fields'], function($arr) {
                        return in_array($arr['field'], array('shelter_data','route_markers_data','foot_over_data','cycle_station_data','police_booth_data','sitting_bench_data'));
                    }));
            if (count($omdOneRoofData)) {
                $fieldKey = $omdOneRoofData[0]['field'];
                $fieldData = array_values(array_filter($devideData['fieldsData'], function($arr) use($fieldKey) {
                            return $arr['field_key'] == $fieldKey;
                        }));
                $omdOneRoofDataArr = json_decode($omdOneRoofData[0]['value'], true);
                ?>
                <h1><?php echo $fieldData[0]['lable'] ?></h1>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Sno.</th>
                            <th>OMD site</th>
                            <th>Height(in m)</th>
                            <th>Width(in m)</th>
                            <th>Area(in sq. m)</th>
                            <th>Status</th>
                            <th>Comment</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sno = 1;
                        foreach ($omdOneRoofDataArr['data'] as $omdOneRoofDataItem) {
                            ?>
                            <tr>
                                <td><?php echo $sno; ?></td>
                                <td><?php echo $omdOneRoofDataItem['type']; ?></td>
                                <td><?php echo $omdOneRoofDataItem['height']; ?></td>
                                <td><?php echo $omdOneRoofDataItem['width']; ?></td>
                                <td><?php echo $omdOneRoofDataItem['area']; ?></td>
                                <td><?php echo isset($omdOneRoofDataItem['action']) ? ($omdOneRoofDataItem['action'] == 1 ? 'Approved' : ($omdOneRoofDataItem['action'] == 3 ? 'Rejected' : 'Under Process')) : 'Under Process'; ?></td>
                                <td><?php echo isset($omdOneRoofDataItem['comment']) ? $omdOneRoofDataItem['comment'] : '--N/A--'; ?></td>
                            </tr>
                            <?php
                            $sno++;
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr><td colspan="7" style="text-align: right;"><label><b>Licence Fees(in Rs.): </b></label><?php echo $omdOneRoofDataArr['licence_fees']; ?></td></tr>
                        <tr><td colspan="7" style="text-align: right;"><label><b>Total Area(in sq. m): </b></label><?php echo $omdOneRoofDataArr['area']; ?></td></tr>
                        <tr><td colspan="7" style="text-align: right;"><label><b>Annual Total Licence Fees(in Rs.): </b></label><?php echo $omdOneRoofDataArr['annual_fees']; ?></td></tr>
                    </tfoot>
                </table>
                <?php
            }
            if (count($omdPremisesData)) {
                $fieldKey = $omdPremisesData[0]['field'];
                $fieldData = array_values(array_filter($devideData['fieldsData'], function($arr) use($fieldKey) {
                            return $arr['field_key'] == $fieldKey;
                        }));
                $omdOneRoofDataArr = json_decode($omdPremisesData[0]['value'], true);
                ?>
                <h1><?php echo $fieldData[0]['lable'] ?></h1>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Sno.</th>
                            <th>OMD site</th>
                            <th>Height(in m)</th>
                            <th>Width(in m)</th>
                            <th>Area(in sq. m)</th>
                            <th>Status</th>
                            <th>Comment</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sno = 1;
                        foreach ($omdOneRoofDataArr['data'] as $omdOneRoofDataItem) {
                            ?>
                            <tr>
                                <td><?php echo $sno; ?></td>
                                <td><?php echo $omdOneRoofDataItem['type']; ?></td>
                                <td><?php echo $omdOneRoofDataItem['height']; ?></td>
                                <td><?php echo $omdOneRoofDataItem['width']; ?></td>
                                <td><?php echo $omdOneRoofDataItem['area']; ?></td>
                                <td><?php echo isset($omdOneRoofDataItem['action']) ? ($omdOneRoofDataItem['action'] == 1 ? 'Approved' : ($omdOneRoofDataItem['action'] == 3 ? 'Rejected' : 'Under Process')) : 'Under Process'; ?></td>
                                <td><?php echo isset($omdOneRoofDataItem['comment']) ? $omdOneRoofDataItem['comment'] : '--N/A--'; ?></td>
                            </tr>
                            <?php
                            $sno++;
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr><td colspan="7" style="text-align: right;"><label><b>Licence Fees(in Rs.): </b></label><?php echo $omdOneRoofDataArr['licence_fees']; ?></td></tr>
                        <tr><td colspan="7" style="text-align: right;"><label><b>Total Area(in sq. m): </b></label><?php echo $omdOneRoofDataArr['area']; ?></td></tr>
                        <tr><td colspan="7" style="text-align: right;"><label><b>Annual Total Licence Fees(in Rs.): </b></label><?php echo $omdOneRoofDataArr['annual_fees']; ?></td></tr>
                    </tfoot>
                </table>
                <?php
            }
            if (count($typologyAData)) {
                $typologyADataArr = json_decode($typologyAData[0]['value'], true);
                $isDevice = array_values(array_filter($typologyADataArr,function($arr){
                    return isset($arr['device']);
                }));
                $deviceLabel = 'No. Of panels';
                if(count($isDevice)) {
                    $deviceLabel = 'Number of Devices';
                }
                $label = $typologyALabels[0]['value'];
                ?>
                <h1><?php echo $label;?></h1>
                <table class="table">
                    <thead>
                        <tr>
                            <th>S. No</th>
                            <th>Type of Asset</th>
                            <th>Location</th>
                            <th><?php echo $deviceLabel;?></th>
                            <th>Total Area(in sq m)</th>
                            <th>Latitude</th>
                            <th>Longitude</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $counter = 1;
                        foreach ($typologyADataArr as $item) {
                            ?>
                            <tr>
                                <td><?php echo $counter; ?></td>
                                <td><?php echo $label;?></td>
                                <td><?php echo $item['location']; ?></td>
                                <td><?php echo isset($item['panels']) ? $item['panels'] : $item['device']; ?></td>
                                <td><?php echo $item['area']; ?></td>
                                <td><?php echo $item['lat']; ?></td>
                                <td><?php echo $item['lng']; ?></td>
                            </tr>
                            <?php
                            $counter++;
                        }
                        ?>
                    </tbody>
                </table>
                <?php
            }
            ?>
        </div>
    </div>
</div>