<?php $this->assign('title', 'Guest User Profile');?>
<div class="page-content inset dashboard">
    <div class="row">
        <div class="login-sec regist clearfix">
            <h1>Profile</h1>
            <div class="form-group col-md-6">
                <label>Type of entity</label>
                <input type="text" readonly="readonly" value="<?php echo $userData['representative']; ?>" class="form-control">
            </div>
            <div class="form-group col-md-6">
                <label>Company/Individual Name</label>
                <input type="text" readonly="readonly" value="<?php echo $userData['display_name']; ?>" class="form-control">
            </div>
            <div class="form-group col-md-6">
                <label>Email</label>
                <input type="text" readonly="readonly" value="<?php echo $userData['email']; ?>" class="form-control">
            </div>
            <div class="form-group col-md-6">
                <label>Mobile Number</label>
                <input type="text" readonly="readonly" value="<?php echo $userData['mobile']; ?>" class="form-control">
            </div>
            <div class="form-group col-md-6">
                <label>Years of experience</label>
                <input type="text" readonly="readonly" value="<?php echo $userData['experience']; ?>" class="form-control">
            </div>
            <div class="form-group col-md-6">
                <label>Annual Turnover(Rs)</label>
                <input type="text" readonly="readonly" value="<?php echo $userData['annual_turnover']; ?>" class="form-control">
            </div>
            <div class="form-group col-md-6">
                <label>Company Address</label>
                <input type="text" readonly="readonly" value="<?php echo $userData['company_address']; ?>" class="form-control">
            </div>
            <div class="form-group col-md-6">
                <label>Correspondence Address</label>
                <input type="text" readonly="readonly" value="<?php echo $userData['correspondence_address']; ?>" class="form-control">
            </div>
            <?php
            if ($userData['director_name']) {
                ?>
                <div class="form-group col-md-6">
                    <label>Director Name</label>
                    <input type="text" readonly="readonly" value="<?php echo $userData['director_name']; ?>" class="form-control">
                </div>
                <?php
            }
            if ($userData['din']) {
                ?>
                <div class="form-group col-md-6">
                    <label>Din Number</label>
                    <input type="text" readonly="readonly" value="<?php echo $userData['din']; ?>" class="form-control">
                </div>
                <?php
            }
            $directorCounter = 2;
            foreach ($userDirectors as $director) {
                ?>
                <div class="form-group col-md-6">
                    <label>Director <?php echo $directorCounter; ?> Name</label>
                    <input type="text" readonly="readonly" value="<?php echo $director['director_name']; ?>" class="form-control">
                </div>
                <div class="form-group col-md-6">
                    <label>Din Number <?php echo $directorCounter; ?></label>
                    <input type="text" readonly="readonly" value="<?php echo $director['din']; ?>" class="form-control">
                </div>
                <?php
                $directorCounter++;
            }
            ?>
            <div class="form-group col-md-6">
                <label>Date of Incorporation</label>
                <input type="text" readonly="readonly" value="<?php echo date('d M, Y', strtotime($userData['incorporation_date'])); ?>" class="form-control">
            </div>
            <div class="form-group col-md-6">
                <label>Authorised Contact Person</label>
                <input type="text" readonly="readonly" value="<?php echo $userData['authorised_contact_person']; ?>" class="form-control">
            </div>
            <h1 style="float: left; width: 100%;margin: 20px 0;">Uploaded Documents</h1>
            <?php
            foreach ($userDocuments as $document) {
                $ext = pathinfo($document['document'], PATHINFO_EXTENSION);
                ?>
                <div class="form-group col-md-3" title="<?php echo $document['label']; ?>">
                    <label><?php echo strlen($document['label']) > 15 ? substr($document['label'], 0, 14) . '..' : $document['label']; ?>:</label>
                    <a style="float: right;" href="<?php echo $document['document']; ?>" download="<?php echo $document['label'] . '.' . $ext; ?>">Download</a>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>