<div class="page-content inset dashboard">
    <div class="row">
        <div class="login-sec regist clearfix">
            <h1>Upload Documents</h1>
            <?php
            foreach ($response['data'] as $item) {
                ?>
                <div class="form-group col-md-6" id="doc_<?php echo $item['field']; ?>"> 
                    <label>
                        <?php echo $item['name']; ?>*
                        <?php
                        if ($item['uploaded']) {
                            ?>
                            <i class="glyphicon glyphicon-ok fileuploaded"></i>
                            <?php
                        } else {
                            ?>
                            <a href="javascript:void(0)" onclick="Registration.markNotApplicable(this)" data-name="<?php echo $item['field']; ?>">Not Applicable ?</a>
                            <?php
                        }
                        ?>
                    </label> 
                    <input <?php echo $item['uploaded'] == 1 ? 'disabled="disabled"' : ''; ?> type="file" name="<?php echo $item['field']; ?>" class="form-control" onchange="Registration.uploadDoc(this);"> 
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>
<?php echo $this->Html->script('registration.js'); ?>
<script>
    Registration.fileUploadUrl = '<?php echo $this->Url->build(['action' => 'uploadDoc']); ?>';
    Registration.loaderImageUrl = '<?php echo $this->Url->build('/img/image_upload_loader.gif'); ?>';
    Registration.redirectUrl = '<?php echo $this->Url->build(['controller' => 'Dashboard', 'action' => 'index']); ?>';
    Registration.notApplicableUrl = '<?php echo $this->Url->build(['action' => 'markNotApplicable']); ?>';
</script>