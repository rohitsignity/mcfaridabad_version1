<b>Hi <?php echo $user['display_name']; ?></b>
<p>A New Tender has been Published. Please check the Detail.</p>
<table>
    <tr>
        <td>Tender Name</td>
        <td><?php echo $tender['name']; ?></td>
    </tr>
    <tr>
        <td>Department Name</td>
        <td><?php echo $tender['department_name']; ?></td>
    </tr>
    <tr>
        <td>Tender Value</td>
        <td><?php echo $tender['tender_value']; ?></td>
    </tr>
    <tr>
        <td>Minimum Guarantee</td>
        <td><?php echo $tender['min_amount']; ?></td>
    </tr>
    <tr>
        <td>Issuing Authority</td>
        <td><?php echo $tender['issuing_authority']; ?></td>
    </tr>
    <tr>
        <td>Authorised By</td>
        <td><?php echo $tender['authorised_by']; ?></td>
    </tr>
    <tr>
        <td>Email</td>
        <td><?php echo $tender['email']; ?></td>
    </tr>
    <tr>
        <td>Contact</td>
        <td><?php echo $tender['contact']; ?></td>
    </tr>
    <?php
        $devicesArr = array_map(function($arr){
            return $arr['device_name'];
        }, $tender['devices']);
    ?>
    <tr>
        <td>Devices</td>
        <td><?php echo implode(', ', $devicesArr);?></td>
    </tr>
    <tr>
        <td>Terms & Conditions</td>
        <td><?php echo $tender['terms_cond']; ?></td>
    </tr>
</table>
<?php /*
<p>Please Click <a href="<?php echo $this->Url->build(["prefix" => false, "controller" => 'bids', 'action' => 'addBid', base64_encode($tender['id'])], true); ?>">here</a> for Start bidding.</p>
<p><?php echo $this->Url->build(["prefix" => false, "controller" => 'bids', 'action' => 'addBid', base64_encode($tender['id'])], true); ?></p>
 */?>
<p>Please Click <a href="<?php echo $this->Url->build(["prefix" => false, "controller" => 'Users', 'action' => 'login'], true); ?>">here</a> to login for Start bidding.</p>
<p><?php echo $this->Url->build(["prefix" => false, "controller" => 'Users', 'action' => 'login'], true); ?></p>
<br><br>
<i>Thank you</i>