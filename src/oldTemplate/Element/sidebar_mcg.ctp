<div id="sidebar-wrapper">
    <ul id="sidebar_menu" class="sidebar-nav1">
        <a id="menu-toggle" href="javascript:void(0)" class="navtoggle"><span  class="menu-toggle-cust glyphicon glyphicon-align-justify"></span></a>
    </ul>
    <ul class="sidebar-nav" id="sidebar">
        <li class="<?php echo $this->request->here == $this->Url->build(["controller" => "Mcg", "action" => "index"]) ? "active" : ""; ?>"><a href="<?php echo $this->Url->build(["controller" => "Mcg", "action" => "index"]); ?>">Dashboard<span class="sub_icon"><img src="<?php echo $this->Url->image('dashboard.png') ?>"></span></a></li>
        <li class="<?php echo $this->request->here == $this->Url->build(["controller" => "Mcg", "action" => "pendingDevices"]) ? "active" : ""; ?>"><a href="<?php echo $this->Url->build(["controller" => "Mcg", "action" => "pendingDevices"]); ?>">Pending Devices<span class="sub_icon"><img src="<?php echo $this->Url->image('om.png') ?>"/></span></a></li>
        <li class="<?php echo $this->request->here == $this->Url->build(["controller" => "Mcg", "action" => "rejectedDevices"]) ? "active" : ""; ?>"><a href="<?php echo $this->Url->build(["controller" => "Mcg", "action" => "rejectedDevices"]); ?>">Rejected Devices<span class="sub_icon"><img src="<?php echo $this->Url->image('om.png') ?>"/></span></a></li>
        <li class="<?php echo $this->request->here == $this->Url->build(["controller" => "Mcg", "action" => "approvedDevices"]) ? "active" : ""; ?>"><a href="<?php echo $this->Url->build(["controller" => "Mcg", "action" => "approvedDevices"]); ?>">Approved Devices <span class="sub_icon"><img src="<?php echo $this->Url->image('om.png') ?>"/></span></a></li>
        <?php
        if ((strtolower($user['type']) == 'mcg1' && strtolower($user['zone']) == 'zone4') || strtolower($user['type']) == 'mcg4') {
            ?>
            <li class="<?php echo $this->request->here == $this->Url->build(["controller" => "Mcg", "action" => "users"]) ? "active" : ""; ?>"><a href="<?php echo $this->Url->build(["controller" => "Mcg", "action" => "users"]); ?>">Users<span class="sub_icon"><img src="<?php echo $this->Url->image('om.png') ?>"/></span></a></li>
            <li class="<?php echo $this->request->here == $this->Url->build(["controller" => "Mcg", "action" => "pendingUsers"]) ? "active" : ""; ?>"><a href="<?php echo $this->Url->build(["controller" => "Mcg", "action" => "pendingUsers"]); ?>">Pending Users<span class="sub_icon"><img src="<?php echo $this->Url->image('om.png') ?>"/></span></a></li>
            <?php
        }
        if(strtolower($user['type']) == 'mcg1' && strtolower($user['zone']) == 'zone4') {
            ?>
            <li class="<?php echo $this->request->here == $this->Url->build(["controller" => "Mcg", "action" => "holdUsers"]) ? "active" : ""; ?>"><a href="<?php echo $this->Url->build(["controller" => "Mcg", "action" => "holdUsers"]); ?>">Hold Users<span class="sub_icon"><img src="<?php echo $this->Url->image('om.png') ?>"/></span></a></li>
            <?php
        }
        ?>
        <li class="<?php echo $this->request->params['controller'] == "Reports" ? "active" : ""; ?>"><a href="<?php echo $this->Url->build(["controller" => "Reports", "action" => "index"]); ?>">Reports<span class="sub_icon"><img src="<?php echo $this->Url->image('report.png') ?>"></span></a></li>
        <?php
            if(strtolower($user['type']) == 'mcg4') {
                ?>
        <li class="<?php echo ($this->request->params['controller'] == "Mcg" && $this->request->params['action'] == "pendingInstallments") ? "active" : ""; ?>"><a href="<?php echo $this->Url->build(['controller' => 'Mcg','action' => 'pendingInstallments']);?>">Pending Installments<span class="sub_icon"><img src="<?php echo $this->Url->image('report.png') ?>"></span><?php echo $pendingInstallments ? '<span id="pendingInstallments">'.$pendingInstallments.'</span>' : '';?></a></li>
            <?php
            }
        ?>
        <li class="<?php echo ($this->request->params['controller'] == "Mcg" && $this->request->params['action'] == "changePassword") ? "active" : ""; ?>"><a href="<?php echo $this->Url->build(['controller' => 'Mcg','action' => 'changePassword']);?>">Change Password<span class="sub_icon"><img src="<?php echo $this->Url->image('report.png') ?>"></span></a></li>
    </ul>
</div>