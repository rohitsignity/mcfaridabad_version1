<header class="header">
    <!-- navbar -->
    <div class="dashboard-head clearfix">
        <div class="logo-nw pull-left">
            <a href="<?php echo $this->Url->build(["controller" => "Dashboard", "action" => "index"]); ?>" class=""><img src="<?php echo $this->Url->image('logo2.png'); ?>" /></a>
        </div>
        <div class="vndr-profile pull-right">
            <ul class="clearfix">
                <li class=""><a href="#" class="prfl"><img src="<?php echo $this->Url->image('logo_admin.png')?>" alt="Admin Logo"><p><?php echo $user['display_name']; ?></p></a></li>
                <!--<li class="notify"><img src="<?php echo $this->Url->image('notification.png'); ?>" /><span class="badge">3</span></li>-->
                <li><a href="<?php echo $this->Url->build(["controller" => "Users", "action" => "logout"]); ?>" class="orang">Logout</a></li>
            </ul>
        </div>
        <?php
        /*
        if ($userType == 'user' && $user['representative_id']) {
            echo $this->Form->input('omdType', ['type' => 'select', 'options' => $omdTypes, 'label' => false, 'class' => 'form-control', 'onchange' => 'Omd.omdType(this)','value' => $omdType, 'templates' => [
                    'inputContainer' => '<div class="form-group pull-right">{{content}}</div>'
            ]]);
            echo $this->Html->script('omd.js');
            ?>
            <script>
                Omd.omdTypeUrl = '<?php echo $this->Url->build(['controller' => 'users','action' => 'setOmdType']);?>';
                Omd.downloadBidDoc = '<?php echo $this->Url->build(['controller' => 'EAuction','action' => 'downloadBidDocument']);?>';
                Omd.startCheckUrl = '<?php echo $this->Url->build(['controller' => 'EAuction','action' => 'checkStart']);?>';
                Omd.bidUrl = '<?php echo $this->Url->build(['controller' => 'EAuction','action' => 'startBid']);?>';
            </script>
            <?php
        }
        */
        ?>
    </div>

</header>

<div id="wrapper" class="active">
    <!-- Sidebar -->
    <?php echo $this->element('sidebar_' . $userType); ?>
