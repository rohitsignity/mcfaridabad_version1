<header class="header">
    <!-- navbar -->
    <nav class="navbar">
        
            <div class="cont-info">
                <div class="container">
				<!---	
                <div class="email-blk"><img src="<?php echo $this->Url->image('email.png'); ?>" />
                    <p><a href="mailto: advt@mcg.gov.in" class="contact-links">advt@mcg.gov.in</a></p>
                </div>
                --->
                
                <?php
                if ($loggedIn) {
                    ?>
                    <a class="login btn" href="<?php echo $this->Url->build([ "controller" => "Dashboard", "action" => "index"]); ?>">Dashboard</a>
                    <a class="login btn" href="<?php echo $this->Url->build([ "controller" => "Users", "action" => "logout"]); ?>">Logout</a>
                    <?php
                } else {
                    ?>
                   
                    <a class="login btn" href="<?php echo $this->Url->build([ "controller" => "Users", "action" => "login","prefix" => false]); ?>"><img src="<?php echo $this->Url->image('login-icon.png'); ?>" /> Login</a>
                   
                    <a class="login register btn" href="<?php echo $this->Url->build([ "controller" => "Users", "action" => "registration","prefix" => false]); ?>"><img src="<?php echo $this->Url->image('registration.png'); ?>" /> Registration</a>
                  
                    <a class="login register btn" href="<?php echo $this->Url->build([ "controller" => "Users", "action" => "index","prefix" => "guest"]); ?>"><img src="<?php echo $this->Url->image('event-login.png'); ?>" /> Event Login</a>
                   
                    <?php
                }
                ?>
                </div>
            </div>
        <div class="container">
            <div class="navbar-header">
                <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="<?php echo $this->Url->build([ "controller" => "Home", "action" => "index", "prefix" => false]); ?>" class="navbar-brand">
					<img src="<?php echo $this->Url->image('faridabad-logo.jpg'); ?>" />
				</a>
            </div>
            <div class="navbar-collapse collapse pull-right m-t-24" id="navbar" aria-expanded="false">
                <ul class="nav navbar-nav sMain-navbar">
                    
                    <li class="sReal-link">
                        <a href="<?php echo $this->Url->build([ "controller" => "Home", "action" => "index","prefix" => false]); ?>">Home</a>
                    </li>

                    <li class="sReal-link">
                        <a href="http://mcfaridabad.org/contact-us-new.aspx">Contact us</a>
                    </li>
                    <li>
                        <a href="tel: 18001801817" class="">
                            <img src="<?php echo $this->Url->image('phone-icon.png'); ?>" /> 
                            18001025953
                        </a>
                    </li>
                    <li>
                        <a href="#" class="">
                            <img src="<?php echo $this->Url->image('email-icon.png'); ?>" /> 
                            advt@mcf.gov.in
                        </a>
                    </li>
                    <li class="dropdown sDownload-opts">
                        <a href="#" class="sDownload-btn btn btn-sm dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo $this->Url->image('download-icon.png'); ?>" /> 
                            Download
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a target="_blank" href="<?php echo $this->request->webroot . 'downloads/hry_advertisement_bye-law.pdf'; ?>">Outdoor Media Byelaws</a></li>
                            <li><a target="_blank" href="<?php echo $this->request->webroot . 'downloads/byelaws_amendments.pdf'; ?>">Byelaws Amendments</a></li>
                            <li><a target="_blank" href="<?php echo $this->request->webroot . 'downloads/Doc2.pdf'; ?>">Permission fee of OMD</a></li>
                            <li><a target="_blank" href="<?php echo $this->request->webroot . 'downloads/FormatsforMCG.pdf'; ?>">Documents Format</a></li>
                            <li><a target="_blank" href="<?php echo $this->request->webroot . 'downloads/TRIPARTITE_LICENSE_AGREEMENT.pdf'; ?>">Tripartite License Agreement</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
