     <!-- Content section -->
        <section class="content-wrap">
            <div class="container">
                <!-- About section -->
                <div class="about-pg">
                <h1 class="inn-head">About Gurugram</h1>
                <div class="page-text">
                <p> Gurugram or Gurgaon is a city in the Indian state of Haryana and is part of the National Capital Region of India.</p>
                <p>Gurgaon was historically inhabited by the Hindu people, and in early times it formed a part of an extensive kingdom ruled over by Rajputs of Yaduvansi or Yadav clan. The Yadav were defeated by Muhammad of Ghor in 1196, but for two centuries they sturdily resisted the Muhammadian domination and they were subjected to punitive expedition. Under the rule of Feroz Shah Tughlaq, several were converted to Islam. This was followed by the invasion of Timur and the land was ruled by Khanzadas. It was then annexed by Babur. During Akbar's reign, Gurugram fell within the governing regions of Delhi and Agra. As the Mughal Empire started to decline, the place was torn between contending powers. By 1803 most of it came under the British rule through the treaty of Surji Arjungaon with Sindhia. The town was first occupied by the cavalry unit posted to watch the army of Begum Samru of Sirdhana. It became a part of the district, which was divided into units called parganas. These units were given to petty chiefs for the military service rendered by them. Eventually these units came under direct control of the British, with the last major administrative change in 1836. After the Revolt of 1857, it was transferred from the North-Western Provinces to Punjab Province. In 1861, the district, of which Gurugram was a part of, was rearranged into five tehsils: Gurgaon, Ferozepur Jhirka, Nuh, Palwal and Rewari and the modern-day city came under the control of Gurgaon tehsil. In 1947, Gurgaon became a part of independent India and fell under the Indian state of Punjab. In 1966, the city came under the administration of Haryana with the creation of the new state.</p>
                <p><b>Gurugram City -</b> National Capital Region of India</p>
                <p><b>Area -</b> 282.7 sq mi</p>
                <p><b>Population -</b> 876,824 (Census 2011)</p>
                <p><b>Location -</b> Latitude 28°27′22″N Longitude : 77°01′44″E</p>
<!--                    <h1 class="inn-head">About Municipal Corporation, Gurugram</h1>
                    <p>Surat Municipal Corporation is a local self government which has come into being under the Bombay Provincial Municipal Act, 1949. It carries out all the obligatory functions and discretionary functions entrusted by the BPMC Act, 1949.</p>
                    <p>Surat became one of the first municipalities of India in 1852, and was converted into a municipal corporation in 1966 under the Bombay Provincial Municipal (BPMC) Act, 1949.</p>
                    <p>Surat Municipal Corporation (SMC) perceives its role as the principal facilitator and provider of services to provide a better quality of life for the city of Surat. SMC has responded to the challenges of fastest population growth and high speed economic development by adopting the best urban management practices.</p>
                    <p>With a visionary leadership and strong support from citizens, SMC has been instrumental in the transformation of the city and its growth. It serves estimated 5.5 million citizens and promises to deliver modern, accountable, transparent and progressive governance.</p>
                    <p>SMC has now taken up the challenge of transforming Surat city into the SMART City.</p>-->
                </div>
                </div>
                <!--About end --->
            </div>

        </section>