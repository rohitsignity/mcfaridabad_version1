<section class="banner">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators
          <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
          </ol> -->
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="img/bannerbg1.jpg">
                <div class="carousel-caption">
                    <h1>Welcome to <br>
                        <span>Outdoor Media Management Portal</span>
                    </h1>
                  <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam imperdiet ullamcorper faucibus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
              <a href="<?php echo $this->Url->build(["controller" => "Users", "action" => "login"]); ?>" class="get-std btn btn-default">Get Started<span class="right-arw"><img src="img/arrow-right.png"/> </span></a>-->
                </div>
            </div>
        </div>
        <!--/ Wrapper for slides -->	
    </div>
</section>
<!-- News & updates -->
<!--<section class="news-strip">
    <div class="news clearfix">
        <h2>News & Updates</h2>
        <marquee direction="left">
            <ul class="upadtes">
                <li>Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. </li>
                <li>Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. </li>
                <li>Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. </li>
                <li>Duis sed odio sit amet nibh vulputate </li>

            </ul>
        </marquee>
    </div>
</section>-->
<!--/ News & updates -->
<!-- Content section -->
<section class="content-wrap">
    <div class="container">
        <!-- About-sectiion section -->
        <div class="about-outdoor">
            <div class="container"> 
                <h2>Outdoor Media Management System</h2>
                <div class="row">
                    <div class="col-md-4">
                        <div class="sImg-cont"><img class="img-responsive center-block" src="<?php echo $this->Url->image('about-img.jpg'); ?>" alt=""></div>
                    </div>
                    <div class="col-md-8">
						<p>Municipal Corporation, Faridabad in its endeavor to adopt a fair and transparent process 
						for its users has started an online system to grant permissions, approvals and monitoring 
						of outdoor advertisements. It shall prepare an Outdoor Media Master Plan for the areas 
						that fall under the jurisdiction of Municipal Corporation, Faridabad. </p>
						<p>All the existing and proposed Outdoor Media Devices with their GPS coordinates 
						shall be marked on the map of Faridabad and the same shall be available on its website for 
						the public. This shall help the advertisers and advertising agencies to plan and execute 
						their advertising campaigns more effectively and efficiently. </p>
						<p>The online system shall make the entire process and management of outdoor media more 
						transparent and proficient. Clear guidelines shall ensure ease of application and 
						approval process in a time bound manner. Regulation in Outdoor media shall result 
						in better management of Outdoor Media devices at large. This online process shall 
						help in determining the occupancy as well as available media options/ spaces quite 
						easily across the city. </p>
                    </div>
                </div>
            </div>
        </div>

        <!--/ section -->
        <!-- Right section -->
        <!-- <div class="online-services">
               <section class="about-sec">
                   <h2>ONLINE SERVICES</h2>
                   <h3>Most Efficient E-Governance Suite that drives Outdoor Media Licensing</h3>
                   
               </section>
               <section class="steps-apply">
        <!-- <div class="steps-head">
             <h3>Steps to Apply</h3>
         </div>-->
        <!--<div class="row">
            <div class="col-sm-4 step1">
                <div class="thumbnail">
                    <span class="round-out"><div class="thumb-img"><img src="img/application-ad1.png" class="image-circle" alt=""></div></span>
                    <div class="caption">
                        <h3>eApplication</h3>
                        <p>Private Media Licensing: MCG online web suite enables private media owners to acquire media licenses in fast & convenient way.</p>
                        <p><a href="<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'login']); ?>" class="btn btn-empty" role="button">Login | Register</a> </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 step1">
                <div class="thumbnail">
                    <span class="round-out"><div class="thumb-img"><img src="img/adroad2.png" class="image-circle" alt=""></div></span>
                    <div class="caption">
                        <h3>eAuction</h3>
                        <p>Online Bidding for Govt. Owned Sites: MCG eAuctioning Suite enabling bidders' to participate in real time bidding.</p>
                      
                        <p><a href="<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'login']); ?>" class="btn btn-empty" role="button">Login | Register</a> </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 step1">
                <div class="thumbnail">
                    <span class="round-out"><div class="thumb-img"><img src="img/application-ad2.png" class="image-circle" alt=""></div></span>
                    <div class="caption">
                        <h3>eApproval</h3>
                        <p>Fast Approvals: Transit media and Event based Banner rights license made easy with fast and single click online approvals.</p>
                        <p><a href="<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'login']); ?>" class="btn btn-empty" role="button">Login | Register</a> </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
   
</div>-->
        <!--/ Right section -->
    </div>



    <section class="imp-links">
        <div class="container">
            <!-- <div class="about-sec title-wrapper">
                <h2>IMPORTANT LINKS</h2>
                <h3>Some are the important links</h3>
            </div> -->
            <div class="links-cont">
                <div class="owl-carousel owl-theme">
                    <div class="item"><a href="#"><img class="img-responsive" src="<?php echo $this->Url->image('about-img.jpg'); ?>" alt=""></a></div>
                    <div class="item"><a href="#"><img class="img-responsive" src="<?php echo $this->Url->image('about-img.jpg'); ?>" alt=""></a></div>
                    <div class="item"><a href="#"><img class="img-responsive" src="<?php echo $this->Url->image('about-img.jpg'); ?>" alt=""></a></div>
                    <div class="item"><a href="#"><img class="img-responsive" src="<?php echo $this->Url->image('about-img.jpg'); ?>" alt=""></a></div>
                    <div class="item"><a href="#"><img class="img-responsive" src="<?php echo $this->Url->image('about-img.jpg'); ?>" alt=""></a></div>
                    <div class="item"><a href="#"><img class="img-responsive" src="<?php echo $this->Url->image('about-img.jpg'); ?>" alt=""></a></div>
                    <div class="item"><a href="#"><img class="img-responsive" src="<?php echo $this->Url->image('about-img.jpg'); ?>" alt=""></a></div>
                    <div class="item"><a href="#"><img class="img-responsive" src="<?php echo $this->Url->image('about-img.jpg'); ?>" alt=""></a></div>
                </div>
               <script>
                    $(document).ready(function(){
                        $('.owl-carousel').owlCarousel({
                            loop:true,
                            margin:30,
                            nav:false,
                            responsive:{
                                0:{
                                    items:1
                                },
                                600:{
                                    items:3
                                },
                                1000:{
                                    items:4
                                }
                            }
                        });
                    });
               </script>
            </div>  

        </div>
    </section>
    
    <section class="resources">
        <div class="container">
            <div class="about-sec title-wrapper">
                <h2>RESOURCES</h2>
                <h3>Outdoor Media Process Flow</h3>
            </div>
            <div class="flowchart">
                <img src="img/flowchart_MCG.jpg" style="width: 100%;"/>   
            </div>  

        </div>
    </section>
    
    <section class="resources">
		<div class="container">
			<div id="map-canvas" style="height: 400px;"></div>
		</div>
	</section>
    
    <!---
    <section class="msg-from">
        <div class="container">
            <div class="about-sec msg-box">
                <img src="img/message-chat.png"/><h2>Message from the <b>Municipal Commissioner</b></h2>    
            </div>
            <div class="media">
                <div class="media-left">
                    <img src="img/user-icon.png"/>
                </div>
                <div class="media-body">
                   <p style="height:100px;">&nbsp; </p>
                    <span><b>Hon'ble Municipal Commissioner</b></span>
                </div>
            </div>
        </div>
    </section>
    --->
    <div class="left-bar">
        <div class="container">
			<!---
            <div class="befor-apply">
                <div class="headh1">
                    <h1>Important links </h1>
                </div>
                <div class="cont-apply">
                    <ul>
                        <li><a href="<?php echo $this->Url->build('/downloads/hry_advertisement_bye-law.pdf'); ?>" target="_blank"><img src="img/pdf.png"/></a><a href="<?php echo $this->Url->build('/downloads/hry_advertisement_bye-law.pdf'); ?>" target="_blank">Outdoor Media Byelaws</a></li>
                        <li><a href="<?php echo $this->Url->build('/downloads/byelaws_amendments.pdf'); ?>" target="_blank"><img src="img/pdf.png"/></a><a href="<?php echo $this->Url->build('/downloads/byelaws_amendments.pdf'); ?>" target="_blank">Byelaws Amendments</a></li>
                        <li><a href="<?php echo $this->Url->build('/downloads/Doc2.pdf'); ?>" target="_blank"><img src="img/pdf.png"/></a><a href="<?php echo $this->Url->build('/downloads/Doc2.pdf'); ?>" target="_blank">Permission fee of OMD</a></li>
                        <li><a href="<?php echo $this->Url->build('/downloads/FormatsforMCG.pdf'); ?>" target="_blank"><img src="img/pdf.png"/></a><a href="<?php echo $this->Url->build('/downloads/FormatsforMCG.pdf'); ?>" target="_blank">Documents Format</a></li>
                        <li><a href="<?php echo $this->Url->build('/downloads/TRIPARTITE_LICENSE_AGREEMENT.pdf'); ?>" target="_blank"><img src="img/pdf.png"/></a><a href="<?php echo $this->Url->build('/downloads/TRIPARTITE_LICENSE_AGREEMENT.pdf'); ?>" target="_blank">TRIPARTITE LICENSE AGREEMENT</a></li>
                    </ul>
                </div>
            </div>
            ----->
        </div>
    </div>
</section>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-V-cjIUZu6DPHpERzLBIV5OnR_AxkRtw"></script>
<script>
function initialize() {
	var marker;
	var map;
	var mapOptions = {
		center: new google.maps.LatLng(21.256204997553017,81.63332000000003),
		zoom: 16,
	};
	// Display a map on the page
	map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
	/*
	var myLatLng = {lat: 21.256204997553017, lng:81.63332000000003};
	var marker = new google.maps.Marker({
	  position: myLatLng,
	  map: map,
	  title: 'Raipur'
	});
	
	var infowindow = new google.maps.InfoWindow({
		content: contentString
    });
    
    marker.addListener('click', function() {
       infowindow.open(map, marker);
    });
	*/
}
initialize();
</script>
