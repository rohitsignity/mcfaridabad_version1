<?php
$status = $this->request->query('status');
if ($adminApproved && $adminApproved == 2) {
    ?>
    <!-- Keep all page content within the page-content inset div! -->
    <div class="page-content inset dashboard">
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-V-cjIUZu6DPHpERzLBIV5OnR_AxkRtw"></script>
        <div class="dash-head">
            <p>Welcome <?php echo $user['display_name']; ?></p>
            <h1>Dashboard</h1>
            <?php
            if ($receipt) {
                ?>
                <a href="<?php echo $receipt; ?>" download="receipt.pdf" class="receiptDownload">Download Payment receipt</a>
                <script>
                    $(document).ready(function () {
                        $('.receiptDownload').get(0).click();
                    });
                </script>
                <?php
            }
            ?>
        </div>
        <!--        <div class="map-vw">
                    <div id="map-canvas" style="height: 400px;"></div>
        <?php /* <img src="<?php echo $this->Url->image('map-in.jpg'); ?>" /> */ ?>
                </div>-->
        <div class="city-status clearfix">
            <label style="color: #782669; float: right; margin: -32px 0px 0px;"><b>Account Expiration Date:</b> <?php echo $expirationDate;?></label>
            <ul class="pull-right">
                <li><a href="<?php echo $this->Url->build(["controller" => "Dashboard"]); ?>" class="<?php echo empty($status) ? 'active' : ''; ?>">All</a></li>
                <li><a href="<?php echo $this->Url->build(["controller" => "Dashboard"]) . "?status=pending"; ?>" class="<?php echo!empty($status) && 'pending' == $status ? 'active' : ''; ?>">Under Process</a></li>
                <li><a href="<?php echo $this->Url->build(["controller" => "Dashboard"]) . "?status=approved"; ?>" class="<?php echo!empty($status) && 'approved' == $status ? 'active' : ''; ?>">Approved</a></li>
                <li><a href="<?php echo $this->Url->build(["controller" => "Dashboard"]) . "?status=rejected"; ?>" class="<?php echo!empty($status) && 'rejected' == $status ? 'active' : ''; ?>">Rejected</a></li>

            </ul>
        </div>

        <div class="device-list">
            <h2>Outdoor Media Device List </h2>
            <div class="table-responsive">
                <table class="table">
                    <thead class="thead-default">
                        <tr>
                            <th>Device Id <?php echo $this->Paginator->sort("companyDevices.id", "<span class='glyphicon glyphicon-triangle-{$iconName}'></span>", ["escape" => false, "direction" => "asc"]); ?></th>
                            <th>Location</th>
                            <th>Status</th>
                            <th>Concession Fees</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($devices)) {
                            foreach ($devices as $key => $deviceArray) {
                                //pr($deviceArray);
                                ?>								
                                <tr>
                                    <th scope="row"><?php echo $deviceArray['companyDevices_id']; ?></th>
                                    <td><?php echo isset($deviceArray['companyDevice_location']) ? $deviceArray['companyDevice_location'] : 'N/A'; ?></td>
                                    <!--<td class="sts-pending sts-apprd sts-rejected">Pending</td>-->
                                    <td class="sts-<?php echo $deviceArray['companyDevices_status'] == 'under process' ? 'pending' : $deviceArray['companyDevices_status']; ?>"><?php echo ucfirst($deviceArray['companyDevices_status']); ?></td>
                                    <td><?php echo isset($deviceArray['companyDevice_concession_fees']) ? 'Rs. '.number_format($deviceArray['companyDevice_concession_fees'],2) : 'N/A'; ?></td>	
                                </tr>							
                                <?php
                            }
                            if ($this->Paginator->numbers()) {
                                ?>
                                <tr>
                                    <td colspan="5"><ul class="pagingPages"><?php echo $this->Paginator->prev() . $this->Paginator->numbers() . $this->Paginator->next(); ?></ul></td>
                                </tr>
                                <?php
                            }
                            ?>
                        <?php } else {
                            ?>
                            <tr>
                                <td colspan="4" class="text-center">No results found.</td>
                            </tr>							
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script>
        function initialize() {

            var marker;
            var latLngObj = '<?php echo $latLngArray; ?>';

            var latLngObj = jQuery.parseJSON(latLngObj);

            var map;
            var i;
            var bounds = new google.maps.LatLngBounds();

            var mapOptions = {
                center: new google.maps.LatLng(21.1701211, 72.8282404),
                zoom: 4,
            };

            // Display a map on the page
            map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

            // Loop through our array of markers & place each one on the map
            //console.log(latLngObj);

            $.each(latLngObj, function (index, val) {

                var position = new google.maps.LatLng(val.lat, val.lng);
                bounds.extend(position);
                marker = new google.maps.Marker({
                    position: position,
                    map: map
                });

                // Automatically center the map fitting all markers on the screen
                map.fitBounds(bounds);
            });

        }

        initialize();

    </script>
    <?php
} elseif ($adminApproved == 3) {
    ?>
    <div class="page-content inset dashboard">
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-V-cjIUZu6DPHpERzLBIV5OnR_AxkRtw"></script>
        <div class="dash-head">
            <p>Welcome <?php echo $user['display_name']; ?></p>
            <h1>Dashboard</h1>
            <?php
            if ($receipt) {
                ?>
                <a href="<?php echo $receipt; ?>" download="receipt.pdf" class="receiptDownload">Download Payment receipt</a>
                <script>
                    $(document).ready(function () {
                        $('.receiptDownload').get(0).click();
                    });
                </script>
                <?php
            }
            ?>
        </div>
        <div class="city-status clearfix">
            <p style="float: left; width: 100%; text-align: center; padding: 37px 0px; font-size: 25px; font-weight: bold;">Your profile is in Hold State. Please upload the folowing documents.</p>
        </div>
        <div class="col-md-12">
            <?php
            foreach ($userAdditionalDocs as $userAdditionalDoc) {
                $disabled = '';
                $event = 'onchange="Hold.uploadDoc(this)"';
                $name = 'name="'.$userAdditionalDoc['id'].'"';
                if($userAdditionalDoc['is_uploaded']) {
                    $disabled = 'disabled="disabled"';
                    $name = '';
                    $event = '';
                }
                ?>
                <div class="col-md-4">
                    <div class="form-group" id="doc_<?php echo $userAdditionalDoc['id'];?>">
                        <label><?php echo $userAdditionalDoc['name'];?></label>
                        <input type="file" <?php echo $name.' '.$disabled.' '.$event;?> class="form-control">
                        <span><?php echo $userAdditionalDoc['reason'];?></span>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
    <?php
    echo $this->Html->script('hold.js');
    ?>
    <script>
        Hold.fileUploadUrl = '<?php echo $this->Url->build(['action'=> 'uploadDoc']);?>';
        Hold.loaderImageUrl = '<?php echo $this->Url->build('/img/image_upload_loader.gif'); ?>';
    </script>
    <?php
} else {
    ?>
    <div class="page-content inset dashboard">
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-V-cjIUZu6DPHpERzLBIV5OnR_AxkRtw"></script>
        <div class="dash-head">
            <p>Welcome <?php echo $user['display_name']; ?></p>
            <h1>Dashboard</h1>
            <?php
            if ($receipt) {
                ?>
                <a href="<?php echo $receipt; ?>" download="receipt.pdf" class="receiptDownload">Download Payment receipt</a>
                <script>
        $(document).ready(function () {
            $('.receiptDownload').get(0).click();
        });
                </script>
                <?php
            }
            ?>
        </div>
        <div class="city-status clearfix">
            <p style="float: left; width: 100%; text-align: center; padding: 37px 0px; font-size: 25px; font-weight: bold;">Your profile is under Admin approval. We will get back to you soon.</p>
        </div>
    </div>
    <?php
}
?>
