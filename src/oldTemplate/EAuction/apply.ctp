<div class="page-content inset dashboard">
    <div class="device-list">
        <h2>Apply Tender</h2>
        <?php
        echo $this->Form->create($post, ['type' => 'file']);
        if (count($tenderDocuments)) {
            ?>
        <h4 style="float: left; width: 100%;">Required Documents*<p class="acceptedFormats">Accepted formats .png, .gif, .jpeg, .jpg, .doc, .docx, .xls, .xlsx, .pdf</p></h4>
            <?php
            foreach ($tenderDocuments as $tenderDocument) {
                ?>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('document[' . $tenderDocument['id'] . ']', ['type' => 'file', 'class' => 'form-control', 'autocomplete' => 'off', 'readonly' => 'readonly', 'label' => 'Upload ' . $tenderDocument['name'] . '*', 'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>']]);
                    if (isset($errors[$tenderDocument['id']])) {
                        ?>
                        <div class="error-message"><?php echo $tenderDocument['name'] . $errors[$tenderDocument['id']]; ?></div>
                        <?php
                    }
                    ?>
                </div>
                <?php
            }
        }
        echo $this->Form->button('Apply', ['class' => 'btn btn-default', 'formnovalidate' => true]);
        echo $this->Form->end();
        ?>
        <br>
        <h2>Tender Detail(<?php echo $tender['name']; ?>)</h2>
        <div class="table-responsive">
            <table class="table">
                <tbody>
                    <tr>
                        <th>Tender Name</th>
                        <td><?php echo $tender['name']; ?></td>
                    </tr>
                    <?php
                         $devicesArr = array_map(function($arr){
                             return '<a href="javascript:void(0)" onclick="Tender.getDeviceDetail('.$arr['id'].')">'.$arr['device_name'].'</a>';
                         }, $devices);
                    ?>
                    <tr>
                        <th>Device(s)</th>
                        <td><?php echo implode(', ', $devicesArr);?></td>
                    </tr>
                    <tr>
                        <th>Issuing Authority</th>
                        <td><?php echo $tender['issuing_authority']; ?></td>
                    </tr>
                    <tr>
                        <th>Authorized By</th>
                        <td><?php echo $tender['authorised_by']; ?></td>
                    </tr>
                    <tr>
                        <th>Department Name</th>
                        <td><?php echo $tender['department_name']; ?></td>
                    </tr>
                    <tr>
                        <th>Estimated Tender Value</th>
                        <td><?php echo $tender['tender_value']; ?></td>
                    </tr>
                    <tr>
                        <th>Minimum Guarantee</th>
                        <td><?php echo $tender['min_amount']; ?></td>
                    </tr>
                    <tr>
                        <th>Document Fee</th>
                        <td><?php echo $tender['document_fee']; ?></td>
                    </tr>
                    <tr>
                        <th>Document Download Date</th>
                        <td><?php echo date('d M, Y', strtotime($tender['doc_download_date'])); ?></td>
                    </tr>
                    <tr>
                        <th>Start Date & Time</th>
                        <td><?php echo date('d M, Y h:iA', strtotime($tender['start_date'])); ?></td>
                    </tr>
                    <tr>
                        <th>End Date & Time</th>
                        <td><?php echo date('d M, Y h:iA', strtotime($tender['end_date'])); ?></td>
                    </tr>
                    <tr>
                        <th>Tender application Last date</th>
                        <td><?php echo date('d M, Y', strtotime($tender['application_date'])); ?></td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td><?php echo $tender['email']; ?></td>
                    </tr>
                    <tr>
                        <th>Contact</th>
                        <td><?php echo $tender['contact']; ?></td>
                    </tr>
                    <tr>
                        <th>Web Link</th>
                        <td><?php echo $tender['web_link'] ? $tender['web_link'] : '--N/A--'; ?></td>
                    </tr>
                    <tr>
                        <?php
                        $tenderDocumentsArr = array_map(function($arr) {
                            return $arr['name'];
                        }, $tenderDocuments);
                        ?>
                        <th>Required Documents</th>
                        <td><?php echo count($tenderDocumentsArr) ? implode(', ', $tenderDocumentsArr) : '--N/A--'; ?></td>
                    </tr>
                    <tr>
                        <th>Terms & Conditions</th>
                        <td><?php echo $tender['terms_cond'] ? $tender['terms_cond'] : '--N/A--'; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?= $this->Html->script('smcUser/tenders.js'); ?>
<script>
    Tender.deviceInfoUrl = '<?php echo $this->Url->build(['action' => 'getDeviceInfo']); ?>';
    Tender.devicePath = '<?php echo $devicePath;?>';
</script>