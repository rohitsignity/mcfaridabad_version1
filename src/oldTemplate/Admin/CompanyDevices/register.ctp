<div id="page-content-wrapper">
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-V-cjIUZu6DPHpERzLBIV5OnR_AxkRtw"></script>
    <!-- Keep all page content within the page-content inset div! -->
    <div class="page-content inset app-status">

        <div class="company-reg devic-reg">
            <div class="headings-all text-center">
                <h1>Outdoor media device application</h1>
            </div>
            <div class="stepsblk clearfix">
                <ul>
                    <li class="step1-blk active">
                        <a href="javascript:void(0)">
                            <span>1</span>
                            <h2>Step1</h2>
                            <p>Device Information</p>
                        </a>
                    </li>
                    <?php
                    /*
                    <li class="step1-blk">
                        <a href="javascript:void(0)">
                            <span>2</span>
                            <h2>Step2</h2>
                            <p>Device Information</p>
                        </a>
                    </li>
                    */
                    ?>
                    <li class="step1-blk">
                        <a href="javascript:void(0)">
                            <span>2</span>
                            <h2>Step2</h2>
                            <p>Preview</p>
                        </a>
                    </li>

                </ul>
            </div>
          <div class="regist-form">
                <h2 class="formh2 orang">Device Information</h2>
                <div class="row">
                    <form method="POST" action="<?php echo $this->Url->build(["controller" => "CompanyDevices", "action" => "saveDevice"]); ?>" enctype="multipart/form-data">
                        <input type="hidden" name="devive_type" value="0">
                        <?php
                        /*
                        <div class="col-md-6 form-step1 form-steps">
                            <label class="col-md-12">Device Type</label>
                            <?php
                            echo $this->Form->radio(
                                    'devive_type', [
                                ['value' => 0, 'text' => 'Public', 'onchange' => 'Typology.deviceType(this)'],
                                ['value' => 1, 'text' => 'Private', 'onchange' => 'Typology.deviceType(this)']
                                    ]
                            );
                            ?>
                        </div>
                        */
                        ?>
<!--                        <div class="col-md-12 form-step1 form-steps">
                            <label>Device Name*</label>
                            <input class="form-control" name="device_name" autocomplete="off" placeholder="Device Name" type="text" id="deviceName">
                        </div>-->
                        <div class="col-md-6 form-step1 form-steps">
                            <?php
//                            echo $this->Form->input('category_id', ['empty' => 'Select Typology', 'type' => 'select', 'options' => [], 'label' => 'Typology', 'id' => 'typology', 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>']]);
                            echo $this->Form->input('category_id', ['empty' => 'Select Typology', 'type' => 'select', 'options' => $categories, 'label' => 'Typology', 'id' => 'typology', 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>']]);
                            ?>
                        </div>
                        <div class="col-md-6 form-step1 form-steps">
                            <?php echo $this->Form->input('subcategory', ['empty' => 'Select Subcategory', 'type' => 'select', 'options' => [], 'label' => 'Sub Category', 'id' => 'sub_category', 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>']]); ?>
                        </div>
                        <div id="formData" style="float: left;"></div>
                    </form>
                </div>
            </div>

        </div>


    </div>
</div>
<script>
    $(document).ready(function() {
        $(window).keydown(function(event){
          if(event.keyCode == 13) {
            event.preventDefault();
            return false;
          }
        });
    });
    var Typology = {
        deviceTypeId: '',
        categoryLocalData: {},
        typologoLocalData: {},
        deviceType: function (element) {
            SubCategory.clearFormPanel('formData');
            this.deviceTypeId = $(element).val();
            this.getCategories();
        },
        getCategories: function () {
            var self = this;
            var deviceType = self.deviceTypeId;
            if (self.categoryLocalData[deviceType]) {
                self.categoryHtml(self.categoryLocalData[deviceType]);
            } else {
                $.ajax({
                    url: '<?php echo $this->Url->build(['controller' => 'CompanyDevices', 'action' => 'getCategories']); ?>',
                    data: {deviceType: deviceType},
                    dataType: 'JSON',
                    type: 'POST',
                    success: function (response) {
                        var responseData = [];
                        $.each(response, function (index, val) {
                            var obj = {};
                            obj.id = val.id;
                            obj.title = val.title;
                            responseData.push(obj);
                        });
                        self.categoryLocalData[deviceType] = responseData;
                        self.categoryHtml(self.categoryLocalData[deviceType]);
                    },
                    error: function (result) {

                    }
                });
            }
        },
        typologyData: function (typologyId, selectBoxId) {
            var self = this;
//            var allowedTypologies = ["4", "9", "10"];
//            if (typologyId && $.inArray(typologyId, allowedTypologies) === -1) {
//                self.subCategoryHtml([], 'sub_category');
//                swal({
//                    title: 'Alert!',
//                    text: 'Please choose from Typology D, I or J',
//                    type: 'info'
//                });
//                return false;
//            }
            if (self.typologoLocalData[typologyId])
            {
                var responseData = self.typologoLocalData[typologyId];
                self.subCategoryHtml(responseData, selectBoxId);
            } else {
                $.ajax({
                    url: '<?php echo $this->Url->build([ "controller" => "CompanyDevices", "action" => "getSubCategory"]); ?>',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {id: typologyId},
                    success: function (response) {
                        var responseData = [];
                        $.each(response, function (index, val) {
                            var obj = {};
                            obj.id = val.id;
                            obj.title = val.title;
                            responseData.push(obj);
                        });
                        self.typologoLocalData[typologyId] = responseData;
                        self.subCategoryHtml(responseData, selectBoxId);
                    }, error: function (error) {
                        window.location = '<?php echo $this->Url->build([ "controller" => "Dashboard", "action" => "index"]); ?>';
                    }
                });
            }

        },
        categoryHtml: function (dataArr) {
            var html = '<option value="">Select Typology</option>';
            $.each(dataArr, function (index, val) {
                html += '<option value="' + val.id + '">' + val.title + '</option>';
            });
            $('#typology').html(html);
        },
        typologoLocalData: {},
        subCategoryHtml: function (dataArr, selectBoxId) {
            var html = '<option value="">Select Subcategory</option>';
            $.each(dataArr, function (index, val) {
                html += '<option value="' + val.id + '">' + val.title + '</option>';
            });
            $('#' + selectBoxId).html(html);
        }
    };


    var SubCategory = {
        subCategoryId: '',
        defaultValidations: {},
        customValidations: {},
        clearFormPanel: function (panelId) {
            $('#' + panelId).html('');
        },
        subCategoryData: function (subCategoryId, panelId) {
            var self = this;
            this.subCategoryId = subCategoryId;
            self.defaultValidations = {};
            if (self.subCategoryLocalData[subCategoryId]) {
                self.getHtml(self.subCategoryLocalData[subCategoryId], panelId);
                if ($(document).find('#formData input[name="location"]').length) {
                    initialize();
                }
            } else {
                $.ajax({
                    url: '<?php echo $this->Url->build([ "controller" => "CompanyDevices", "action" => "getFields"]); ?>',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {id: subCategoryId},
                    success: function (response) {
                        self.subCategoryLocalData[subCategoryId] = response;
                        self.getHtml(response, panelId);
                        if ($(document).find('#formData input[name="location"]').length) {
                            initialize();
                        }
                    }, error: function (error) {
                        window.location = '<?php echo $this->Url->build([ "controller" => "Dashboard", "action" => "index"]); ?>';
                    }
                });
            }
        },
        subCategoryLocalData: {},
        getHtml: function (data, panelId) {
            var self = this;
            var html = '<div class="form-step1 form-steps">';
            $.each(data, function (index, val) {
                if (val.group_in === '1') {
                    var field = self[val.type];
                    html += field(val, self);
                }
            });
            if (html) {
                html += self.next();
            }
            html += '</div>';
//            html += '<div class="form-step2 form-steps" style="display: none;">';
//            $.each(data, function (index, val) {
//                if (val.group_in === '2') {
//                    var field = self[val.type];
//                    html += field(val, self);
//                }
//            });
//            html += self.previous(1);
//            html += self.preview();
//            html += '</div>';
            html += '<div id="previewPanel" style="display: none;" class="col-md-12 form-step3 form-steps">';
            $.each(data, function (index, val) {
                html += '<div class="col-md-12">';
                html += '<label class="col-md-6">' + val.lable + '</label><span class="col-md-6" id="p_' + val.field_key + '">--N/A--</span>';
                html += '</div>';
            });
            html += self.previous(1);
            html += self.submit();
            html += '</div>';
            $('#' + panelId).html(html);
        },
        clearOMDSizeFieldsValidations: function (element) {
            var i;
            for (i = 1; i <= 6; i++) {
                this.defaultValidations[element.attr('name') + '_size_' + i + '-input-1'] = "";
            }
        },
        getOMDSizeFields: function (element) {
            this.clearOMDSizeFieldsValidations(element);
            $(document).find('#' + 'omd_size_' + element.attr('name')).remove();
            var i;
            var html = '<div id="omd_size_' + element.attr('name') + '">';
            var placeHolder = '15m';
            if (element.attr('name') === 'omd_no_others') {
                placeHolder = '6m';
            }
            for (i = 1; i <= element.val(); i++) {
                this.defaultValidations[element.attr('name') + '_size_' + i + '-input-1'] = '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}';
                html += '<div class="col-md-4">';
                html += '<label>OMD ' + i + ' Size</label>';
                html += '<input type="text" name="' + element.attr('name') + '_size_' + i + '" class="form-control omd-size-meters ' + element.attr('name') + '" autocomplete="off" placeholder="' + placeHolder + '">';
                html += '</div>';
            }
            html += '</div>';
            element.after(html);
        },
        getPreviousForm: function (stepNumber) {
            $(document).find('.form-steps').hide();
            $(document).find('.form-step' + stepNumber).show();
            $('.stepsblk ul li').removeClass('active');
            $('.stepsblk ul li:eq(' + (stepNumber - 1) + ')').addClass('active');
        },
        next: function () {
            var html = '<input type="button" value="Next" id="formPreview" class="btn btn-default" style="float: left; clear: both; width: 18%;">';
            return html;
        },
        previous: function (formNumber) {
            var html = '<input type="button" value="Previous" class="btn btn-default" style="float: left; clear: both; width: 18%;" onclick="SubCategory.getPreviousForm(' + formNumber + ')">';
            return html;
        },
        preview: function () {
            var html = '<input type="button" value="Preview" class="btn btn-default" style="float: left; clear: both; width: 18%;" onclick="SubCategory.getPreview()">';
            return html;
        },
        text: function (data, self) {
            self.defaultValidations[data.field_key + '-input-' + data.group_in] = data.default_validations;
            self.customValidations[data.field_key] = data.custom_validation;
            var readonly = '';
            if (data.is_disabled === '1') {
                readonly = 'readonly="readonly"';
            }
            if (data.custom_disable === '1') {
                readonly = 'readonly="readonly"';
            }

            if (data.custom_calculations) {
                self.customCalculations(data.field_key, JSON.parse(data.custom_calculations));
            }
            var html = '<div class="col-md-6 form-groupY">';
            if (data.field_key === 'location') {
                html = '<div class="col-md-12 form-groupY">';
            }
            html += '<label>' + data.lable + '</label>';
            html += '<input class="form-control" type="text" name="' + data.field_key + '" ' + readonly + ' autocomplete="off" placeholder="' + data.lable + '">';
            html += '</div>';
            if (data.field_key === 'location') {
                html += '<div class="col-md-12">';
                html += '<div id="map-canvas" style="height: 400px;"></div>';
                html += '</div>';
            }
            return html;
        },
        radio: function (data, self) {
            self.defaultValidations[data.field_key + '-input-' + data.group_in] = data.default_validations;
            self.customValidations[data.field_key] = data.custom_validation;
            var options = JSON.parse(data.options);
            var html = '<div class="col-md-6 form-groupY">';
            html += '<div class="form-group">';
            html += '<label>' + data.lable + '</label>';
            html += '<div class="radiobtn">';
            $.each(options, function (index, val) {
                html += '<label><input type="radio" name="' + data.field_key + '" value="' + val + '"><p>' + val + '</p></label>';
            });
            html += '</div>';
            html += '</div>';
            html += '</div>';
            return html;
        },
        select: function (data, self) {
            self.defaultValidations[data.field_key + '-select-' + data.group_in] = data.default_validations;
            self.customValidations[data.field_key] = data.custom_validation;
            var options = JSON.parse(data.options);
            var html = '<div class="col-md-6 form-groupY">';
            html += '<label>' + data.lable + '</label>';
            html += '<select class="form-control" name="' + data.field_key + '">';
            html += '<option value="">Select ' + data.lable + '</option>';
            $.each(options, function (index, val) {
                html += '<option value="' + val + '">' + val + '</option>';
            });
            html += '</select>';
            html += '</div>';
            return html;
        },
        file: function (data, self) {
            self.defaultValidations[data.field_key + '-input-' + data.group_in] = data.default_validations;
            self.customValidations[data.field_key] = data.custom_validation;
            var html = '<div class="col-md-6 form-groupY">';
            html += '<div class="form-group">';
            html += '<label>' + data.lable + '</label>';
            html += '<input class="fileup deviceImageUpload" type="file" name="' + data.field_key + '">';
            html += '</div>';
            html += '</div>';
            return html;
        },
        hidden: function (data) {
            var html = '<input type="hidden" name="' + data.field_key + '">';
            return html;
        },
        submit: function () {
            var html = '<input type="submit" value="Submit" class="btn btn-default" style="float: left; clear: both; width: 18%;">';
            return html;
        },
        checkValidationStep1: function () {
            var self = this;
            var error = 0;
//            var deviveNameElement = $('#deviceName');
//            if(!$.trim(deviveNameElement.val())) {
//                deviveNameElement.after('<p class="error-message">This field is required</p>');
//                error = 1;
//            }
            $.each(self.customValidations, function (field, val) {
                if (val) {
                    var customValidation = JSON.parse(val);
                    $.each(customValidation, function (key, validationVal) {
                        var validate = self[key];
                        var element = $('[name="' + field + '"]');
                        if (validate(validationVal, element)) {
                            error = 1;
                            return false;
                        }
                        ;
                    });
                }
            });
            $.each(self.defaultValidations, function (field, val) {
                if (val) {
                    var fieldData = field.split('-');
                    if (fieldData[2] === '1') {
                        var element = $(fieldData[1] + '[name="' + fieldData[0] + '"]');
                        var validation = JSON.parse(val);
                        $.each(validation, function (key, validationVal) {
                            var validate = self[key];
                            if (validate(validationVal, element)) {
                                error = 1;
                                return false;
                            }
                        });
                    }
                }
            });
            return error;
        },
        checkValidationStep2: function () {
            var self = this;
            var error = 0;
            $.each(self.defaultValidations, function (field, val) {
                if (val) {
                    var fieldData = field.split('-');
                    if (fieldData[2] === '2') {
                        var element = $(fieldData[1] + '[name="' + fieldData[0] + '"]');
                        var validation = JSON.parse(val);
                        $.each(validation, function (key, validationVal) {
                            var validateStep2 = self[key];
                            if (validateStep2(validationVal, element)) {
                                error = 1;
                                return false;
                            }
                        });
                    }
                }
            });
            return error;
        },
        moveStep2: function () {
            $(document).find('.error-message').remove();
            if (!this.checkValidationStep1()) {
                $('.stepsblk ul li').removeClass('active');
                $('.stepsblk ul li:eq(1)').addClass('active');
                $(document).find('.form-step1').hide();
                this.showPreview();
//                $(document).find('.form-step2').show();
            }
        },
        getPreview: function () {
            $(document).find('.error-message').remove();
            if (!this.checkValidationStep2()) {
                $('.stepsblk ul li').removeClass('active');
                $('.stepsblk ul li:eq(2)').addClass('active');
                this.showPreview();
            }
        },
        showPreview: function () {
            var data = this.subCategoryLocalData[this.subCategoryId];
            $.each(data, function (index, val) {
                var checked = '';
                if ($('[name="' + val.field_key + '"]').attr('type') === 'radio') {
                    checked = ':checked';
                }
                if ($('[name="' + val.field_key + '"]' + checked).val()) {
                    $('#p_' + val.field_key).html($('[name="' + val.field_key + '"]' + checked).val());
                }
            });
            $('.form-step2').hide();
            $('#previewPanel').show();
        },
        isAlphabit: function (validationVal, element) {
            if (!validationVal) {
                return false;
            }
        },
        isAlphanumeric: function (validationVal, element) {
            if (!validationVal) {
                return false;
            }
        },
        isNum: function (validationVal, element) {
            if (!validationVal) {
                return false;
            }
            var elementVal = $.trim(element.val());
            if (elementVal) {
                if (!$.isNumeric(elementVal)) {
                    element.after('<p class="error-message">Please enter numeric value</p>');
                    return true;
                }
            }
            return false;
        },
        maxLimit: function (validationVal, element) {
            if (!validationVal) {
                return false;
            }
            var elementVal = $.trim(element.val());
            if (elementVal) {
                if (elementVal.length > validationVal) {
                    element.after('<p class="error-message">Maximum character should be ' + validationVal + '</p>');
                    return true;
                }
            }
        },
        minLimit: function (validationVal, element) {
            if (!validationVal) {
                return false;
            }
            var elementVal = $.trim(element.val());
            if (elementVal) {
                if (elementVal.length < validationVal) {
                    element.after('<p class="error-message">Mininum character should be ' + validationVal + '</p>');
                    return true;
                }
            }
            return false;
        },
        noEmpty: function (validationVal, element) {
            if (!validationVal) {
                return false;
            }
            if (element.attr('type') === 'radio') {
                if (!element.is(':checked')) {
                    element.parents('.form-groupY').append('<p class="error-message">This field is required</p>');
                    return true;
                }
            } else {
                var elementVal = $.trim(element.val());
                if (!elementVal) {
                    element.after('<p class="error-message">This field is required</p>');
                    return true;
                }
            }
            return false;
        },
        depend: function (validationVal, element) {
            var error = false;
            var defaultFieldVal = element.val();
            if (element.attr('type') === 'radio') {
                defaultFieldVal = element.filter(':checked').val();
            }
            $.each(validationVal, function (field, val) {
                var checked = '';
                if (val.type === 'radio') {
                    checked = ':checked';
                }
                var fieldVal = $('input[name="' + field + '"]' + checked).val();
                if (typeof fieldVal === 'undefined') {
                    if (element.attr('type') === 'radio') {
                        if (typeof defaultFieldVal === 'undefined') {
                            element.parents('.form-groupY').append('<p class="error-message">This field is required</p>');
                            error = true;
                        }
                    }
                } else if ($.inArray(fieldVal, val.value) !== -1) {
                    if (element.attr('type') === 'radio') {
                        if (typeof defaultFieldVal === 'undefined') {
                            element.parents('.form-groupY').append('<p class="error-message">This field is required</p>');
                            error = true;
                        } else if ($.inArray(defaultFieldVal, val.value) !== -1) {
                            element.parents('.form-groupY').append('<p class="error-message">' + val.message + '</p>');
                            error = true;
                        }
                    }
                } else {
                    if (element.attr('type') !== 'radio') {
                        if (fieldVal === val.default && !defaultFieldVal) {
                            element.after('<p class="error-message">This field is required</p>');
                            error = true;
                        }
                    }
                }
                if (error) {
                    return false;
                }
            });
            return error;
        },
        operationalValidation: function (validationVal, element) {
            var error = 0;
            var result = parseFloat(validationVal.defaultValue);
            $.each(validationVal.field, function (index, val) {
                var fieldValue = $(document).find('[name="' + val + '"]').val();
                if (!fieldValue) {
                    error = 1;
                }
            });
            if (!element.val()) {
                error = 1;
            }
            if (error === 0) {
                $.each(validationVal.field, function (index, val) {
                    var fieldValue = $(document).find('[name="' + val + '"]').val();
                    var mathFunction = MathFunctions[validationVal.operator];
                    result = mathFunction(result, fieldValue);
                });
                var operandMathFunction = MathFunctions[validationVal.oprand];
                if (operandMathFunction(element.val(), result)) {
                    element.after('<p class="error-message">' + validationVal.title + ' should not ' + validationVal.oprand + ' than ' + result + ' of ' + validationVal.unit + '</p>');
                    return true;
                }
            }
            return false;
        },
        customCalculations: function (fieldName, data) {
            var self = this;
            var operation = self[data.operation];
            operation(data, fieldName);
        },
        multiply: function (data, fieldName) {
            var fields = [];
            $.each(data.fields, function (index, val) {
                fields.push('[name="' + val + '"]');
            });
            var selector = fields.join(', ');
            $(document).on('focusout', selector, function () {
                var result = data.defaultValue;
                var error = 0;
                var selectorValue = $(this).val();
                if (!selectorValue) {
                    $(document).find('[name="' + fieldName + '"]').val('');
                    return false;
                }
                if (!$.isNumeric(selectorValue)) {
                    alert("Please enter a Numeric value");
                    $(document).find('[name="' + fieldName + '"]').val('');
                    $(this).val("");
                    return false;
                }
                $.each(data.fields, function (index, val) {
                    var fieldValue = $(document).find('[name="' + val + '"]').val();
                    if (!fieldValue) {
                        error = 1;
                    }
                });
                if (error === 0) {
                    $.each(data.fields, function (index, val) {
                        var fieldValue = $('[name="' + val + '"]').val();
                        result = fieldValue * result;
                    });
                    $(document).find('[name="' + fieldName + '"]').val(result);
                }
            });
        },
        onSelect: function (data, fieldName) {
            console.log(fieldName);
            var options = data.options;
            var fields = [];
            $.each(data.fields, function (index, val) {
                fields.push('[name="' + val + '"]');
            });
            var selector = fields.join(', ');
            $(document).off('change').on('change', selector, function () {
                $(document).find('[name="' + fieldName + '"]').val('');
                if (typeof options[$(this).val()] !== 'undefined') {
                    $(document).find('[name="' + fieldName + '"]').val(options[$(this).val()]);
                    $(document).find('[name="' + fieldName + '"]').focus();
                }
            });
        }
    };

    var MathFunctions = {
        "+": function (x, y) {
            return x + y;
        },
        "-": function (x, y) {
            return x - y;
        },
        "*": function (x, y) {
            return x * y;
        },
        "greater": function (x, y) {
            return x > y;
        }
    };

    $('#typology').on('change', function () {
        var typologyId = $(this).val();
        SubCategory.clearFormPanel('formData');
        Typology.typologyData(typologyId, 'sub_category');

    });

    $('#sub_category').on('change', function () {
        var subCategoryId = $(this).val();
        $(document).off('focusout');
        SubCategory.subCategoryData(subCategoryId, 'formData');
    });
    $(document).on('click', '#formPreview', function () {
        SubCategory.moveStep2();
    });

    $(document).on('keyup', 'input[name="omd_no_venue"],input[name="omd_no_others"]', function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || (e.keyCode >= 35 && e.keyCode <= 40)) {
            $(document).find('#' + 'omd_size_' + $(e.target).attr('name')).remove();
            SubCategory.clearOMDSizeFieldsValidations($(e.target));
            return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            $(e.target).val('');
            e.preventDefault();
        }
        if ($(e.target).val() < 1) {
            $(document).find('#' + 'omd_size_' + $(e.target).attr('name')).remove();
            SubCategory.clearOMDSizeFieldsValidations($(e.target));
            $(e.target).val('');
            return false;
        }
        if ($(e.target).val() > 6) {
            alert("No. of OMD should be upto 6");
            $(document).find('#' + 'omd_size_' + $(e.target).attr('name')).remove();
            SubCategory.clearOMDSizeFieldsValidations($(e.target));
            $(e.target).val('');
            return false;
        }
        SubCategory.getOMDSizeFields($(e.target));
    });

    $(document).on('keyup', '.omd-size-meters', function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || (e.keyCode >= 35 && e.keyCode <= 40)) {
            return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            $(e.target).val('');
            e.preventDefault();
        }
        if ($(e.target).val() < 1) {
            $(e.target).val('');
            return false;
        }
        var sizeLimit = 15;
        if ($(e.target).hasClass('omd_no_others')) {
            sizeLimit = 6;
        }
        if ($(e.target).val() > sizeLimit) {
            alert("More than " + sizeLimit + " meters not allowed");
            $(e.target).val('');
            return false;
        }
    });
//    $(document).on('input', '#formData input[name="location"]', function () {
//        var element = $(this);
//        $(element).autocomplete({
//            source: function (request, response) {
//                $.ajax({
//                    url: '<?php //echo $this->Url->build([ "controller" => "CompanyDevices", "action" => "getLocation"]); ?>',
//                    type: 'GET',
//                    dataType: 'JSON',
//                    data: {s: request.term},
//                    success: response
//                });
//            },
//            minLength: 2,
//            select: function (event, ui) {
//                setLocation(ui.item.data.location.lat, ui.item.data.location.lng);
//                codeAddress(ui.item.data.location.lat, ui.item.data.location.lng);
//            },
//            delay: 1000
//        });
//    });

    var geocoder;
    var map;
    var markersArray = [];
    var mapOptions = {
        center: new google.maps.LatLng(28.4578215, 77.0246352),
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var marker;

    function createMarker(latLng) {
        if (!!marker && !!marker.setMap)
            marker.setMap(null);
        marker = new google.maps.Marker({
            map: map,
            position: latLng,
            draggable: true
        });
        google.maps.event.addListener(marker, "dragend", function () {
            setLocation(marker.getPosition().lat(), marker.getPosition().lng());
            codeAddress(marker.getPosition().lat(), marker.getPosition().lng());
        });
    }

    function initialize() {
        geocoder = new google.maps.Geocoder();
        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        codeAddress(28.4578215, 77.0246352, 1);

        google.maps.event.addListener(map, 'click', function (event) {
            map.panTo(event.latLng);
            map.setCenter(event.latLng);
            createMarker(event.latLng);
            codeAddress(event.latLng.lat(), event.latLng.lng());
            setLocation(event.latLng.lat(), event.latLng.lng());
        });

    }
    function codeAddress(lat, lng, intial) {
        var latlng = {lat: lat, lng: lng};
        geocoder.geocode({'location': latlng}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (!intial) {
                    var locationNames = [];
                    $.each(results[0].address_components, function (index, val) {
                        locationNames.push(val.long_name.toLowerCase());
                    });
                    if ($.inArray('gurugram', locationNames) !== -1) {
                        map.setCenter(results[0].geometry.location);
                        createMarker(latlng);
                        $('#formData input[name="location"]').val(results[0].formatted_address);
                    } if($.inArray('gurgaon', locationNames) !== -1) {
                        map.setCenter(results[0].geometry.location);
                        createMarker(latlng);
                        $('#formData input[name="location"]').val(results[0].formatted_address);
                    } else {
                        alert("Invalid Location.");
                        codeAddress(28.4578215, 77.0246352, 1);
                        setLocation(28.4578215, 77.0246352);
                    }
                } else {
                    $('#formData input[name="location"]').val('');
                    map.setCenter(results[0].geometry.location);
                    createMarker(latlng);
                }
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }

    function setLocation(lat, lng) {
        $(document).find('#formData input[name="lat"]').val(lat);
        $(document).find('#formData input[name="lng"]').val(lng);
    }


    $(document).on("change", '.deviceImageUpload', function (e) {
        var self = $(this);
        var fileType = this.files[0].type;
        console.log(fileType);
        var ValidImageTypes = ["image/gif", "image/jpeg", "image/png","image/jpg","application/pdf","application/zip","application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.wordprocessingml.document","text/csv"];
        if ($.inArray(fileType, ValidImageTypes) < 0) {
            alert("Invalid Document");
            self.val("");
        }
    });
</script>