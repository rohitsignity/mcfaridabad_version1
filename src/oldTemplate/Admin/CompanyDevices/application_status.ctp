<div class="page-content inset app-status">
	<div class="headings-all dash-in">
		<h1>Applications Status</h1>
	</div>
	<div class="regist-form">
		<h2 class="formh2">Typology</h2>
		<div class="row">
			<?php echo $this->Form->create(); ?>
				<div class="col-md-6">
					<?php echo $this->Form->input('category_id', ['empty' => 'Select Typology', 'type' => 'select', 'options' => $categories, 'label' => 'Typology', 'id' => 'typology', 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>']]); ?>
				</div>
				<div class="col-md-6">
					<?php echo $this->Form->input('subcategory', ['empty' => 'Select Subcategory', 'type' => 'select', 'options' => [], 'label' => 'Sub Category', 'id' => 'sub_category', 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>']]); ?>
				</div>
				<h2 class="col-md-12 formh2">Device Information</h2>
					<div class="col-md-4">
						<?php echo $this->Form->input('device_id', ['type' => 'text', 'class' => 'form-control', 'label' => 'Device Id', 'templates' => [
                            'inputContainer' => '<div class="form-group">{{content}}</div>'
                        ], 'placeholder' => 'Device Id']); ?>
					</div>
				<div class="clearfix"></div>
				<?php echo $this->Form->button('Submit', ['class' => 'btn btn-default', 'formnovalidate' => true]) ?>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
	<div class="device-list">
		<h2>Outdoor Media Device List </h2>
		<div class="table-responsive">
			<table class="table">
                <thead class="thead-default">
                    <tr>
                        <th>Device Id</th>
                        <th>Location</th>
                        <th>Status</th>
                        <th>Concession Fees</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($devices)) {
                        foreach ($devices as $key => $deviceArray) {
                            //pr($deviceArray);
                            ?>								
                            <tr>
                                <th scope="row"><?php echo $deviceArray['companyDevices_id']; ?></th>
                                <td><?php echo isset($deviceArray['companyDevice_location']) ? $deviceArray['companyDevice_location'] : 'N/A'; ?></td>
                                <!--<td class="sts-pending sts-apprd sts-rejected">Pending</td>-->
                                <td class="sts-<?php echo $deviceArray['companyDevices_status']; ?>"><?php echo ucfirst($deviceArray['companyDevices_status']); ?></td>
                                <td><?php echo isset($deviceArray['companyDevice_concession_fees']) ? $deviceArray['companyDevice_concession_fees'] : 'N/A'; ?></td>	
                            </tr>							
                            <?php
                        }
                        if ($this->Paginator->numbers()) {
                            ?>
                            <tr>
                                <td colspan="5"><ul class="pagingPages"><?php echo $this->Paginator->prev() . $this->Paginator->numbers() . $this->Paginator->next(); ?></ul></td>
                            </tr>
                            <?php
                        }
                        ?>
                    <?php } else {
                        ?>
                        <tr>
                            <td colspan="4" class="text-center">No results found.</td>
                        </tr>							
                        <?php
                    }
                    ?>
                </tbody>
            </table>
		</div>
	</div>
</div>
<script>
	var Typology = {
		checkSubCat: 0,
        typologyData: function (typologyId, selectBoxId) {
            var self = this;
            if (self.typologoLocalData[typologyId]) {
                var responseData = self.typologoLocalData[typologyId];
                self.subCategoryHtml(responseData, selectBoxId);
            } else {
                $.ajax({
                    url: '<?php echo $this->Url->build([ "controller" => "CompanyDevices", "action" => "getSubCategory"]); ?>',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {id: typologyId},
                    success: function (response) {
                        var responseData = [];
                        var subCatId 	= '<?php echo isset($subcategory) ? $subcategory : "" ;?>';
                        
                        $.each(response, function (index, val) {
                            var obj = {};
                            obj.id = val.id;
                            obj.title = val.title;
                            responseData.push(obj);
                        });
                        self.typologoLocalData[typologyId] = responseData;
                        self.subCategoryHtml(responseData, selectBoxId);
                        if( subCatId != '' && self.checkSubCat == 1) {
							$(document).find("#sub_category").val(subCatId);
							self.checkSubCat = 0;
						}
                    }, error: function (error) {
                        window.location = '<?php echo $this->Url->build([ "controller" => "Home", "action" => "index"]); ?>';
                    }
                });
            }

        },
        typologoLocalData: {},
        subCategoryHtml: function (dataArr, selectBoxId) {
            var html = '<option value="">Select Subcategory</option>';
            $.each(dataArr, function (index, val) {
                html += '<option value="' + val.id + '">' + val.title + '</option>';
            });
            $('#' + selectBoxId).html(html);
        }
    };    
    
    $('#typology').on('change', function () {
        var typologyId = $(this).val();
        Typology.typologyData(typologyId, 'sub_category');
    });
    
	$(document).ready(function(){
		var typologyId 	= $('#typology').val();
		
		if( typologyId != '' ) {
			Typology.checkSubCat = 1;
			Typology.typologyData(typologyId, 'sub_category');		
		} 
	}); 
    
</script>                
