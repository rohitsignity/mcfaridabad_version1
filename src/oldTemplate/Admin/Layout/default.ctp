<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?= $this->fetch('title') ?></title>
        <!-- Latest compiled and minified CSS -->
        <?php echo $this->Html->css(['bootstrap.min.css', 'custom.css', 'dev.css', 'lato/stylesheet.css', 'jquery-ui.css', 'jquery-ui.structure.css', 'jquery-ui.theme.css']); ?>
        <!-- Bootstrap core JavaScript
================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <?php echo $this->Html->script('jquery-2.2.4.js'); ?>
        <?php echo $this->Html->script(['bootstrap.min.js', 'utility.js', 'jquery-ui.js']); ?>
        <!-- Font File -->
    </head>    

    <body>
        <div id="loaderLayout" style="display: none;">
            <img src="<?php echo $this->Url->image('loader.gif'); ?>">
            <span></span>
        </div>
        <!-- Page Wrap -->
        <div class="page-wrap">
            <!-- Header -->
            <?php echo $this->element('header'); ?>
            <!--/ Header -->
            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>         
        </div>
        <!-- Footer -->
        <?php echo $this->element('footer'); ?>
        <!-- /.footer -->
    </body>
</html>
