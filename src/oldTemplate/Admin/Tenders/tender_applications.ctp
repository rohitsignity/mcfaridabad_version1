<div class="page-content inset dashboard">
    <div class="device-list">
        <h2>Tenders</h2>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-default">
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Start date & Time</th>
                        <th>End Date & Time</th>
                        <th>Minimum Guarantee</th>
                        <th>Contact</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (count($tenders)) {
                        $sNo = (($this->Paginator->current() - 1) * $limit) + 1;
                        foreach ($tenders as $tender) {
                            ?>
                            <tr>
                                <td><?= $sNo; ?></td>
                                <td><?= $tender->name; ?></td>
                                <td><?= date('d M, Y h:iA', strtotime($tender->start_date)); ?></td>
                                <td><?= date('d M, Y h:iA', strtotime($tender->end_date)); ?></td>
                                <td><?= $tender->min_amount; ?></td>
                                <td><?= $tender->contact; ?></td>
                                <td><a href="<?php echo $this->Url->build(['action' => 'tenderApplicationsList',$tender->id]);?>">View <?= $tender->applied_bids ? ' (New '.$tender->applied_bids.')' : ''; ?></a></td>
                            </tr>
                            <?php
                            $sNo++;
                        }
                        if ($this->Paginator->numbers()) {
                            ?>
                            <tr>
                                <td colspan="10"><ul class="pagingPages"><?php echo $this->Paginator->prev() . $this->Paginator->numbers() . $this->Paginator->next(); ?></ul></td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr><td colspan="10" class="text-center">No Record Found</td></tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>