<div id="page-content-wrapper">
    <!-- Keep all page content within the page-content inset div! -->
    <div class="page-content inset app-status">
        <div class="headings-all dash-in">
            <h1>OMD Devices Payment Report</h1>
        </div>
        <div class="regist-form">
            <h2 class="formh2">Typology</h2>
            <div class="row">
                <?php
                echo $this->Form->create('Reports', ['novalidate' => true, 'id' => 'exportReportForm']);
                echo $this->Form->input('typology', ['id' => 'typologies', 'empty' => 'Select Typology', 'type' => 'select', 'options' => $categories, 'label' => false, 'class' => 'form-control', 'templates' => [
                        'inputContainer' => '<div class="form-group col-md-6">{{content}}</div>'
                ]]);
                echo $this->Form->input('sub_category', ['id' => 'subCat', 'empty' => 'Select Sub Category', 'type' => 'select', 'options' => $subCategories, 'label' => false, 'class' => 'form-control', 'templates' => [
                        'inputContainer' => '<div class="form-group col-md-6">{{content}}</div>'
                ]]);
                ?>
                <h2 class="col-md-12 formh2">Applicant Information</h2>
                <?php
                echo $this->Form->input('display_name', ['type' => 'text', 'class' => 'form-control', 'label' => 'User Name', 'templates' => [
                        'inputContainer' => '<div class="form-group col-md-6">{{content}}</div>'
                    ], 'placeholder' => 'Enter User Name']);
                echo $this->Form->input('receipt_id', ['type' => 'text', 'class' => 'form-control', 'label' => 'Receipt Id', 'templates' => [
                        'inputContainer' => '<div class="form-group col-md-6">{{content}}</div>'
                    ], 'placeholder' => 'Enter Receipt Id']);
                ?>
                <h2 class="col-md-12 formh2">Type of Payment</h2>
                <?php
                 echo $this->Form->input('payment_type', ['id' => 'payment_type', 'empty' => 'Select Paymen type', 'type' => 'select', 'options' =>array('installment'=>'Installment','final_payment'=>'Final Payment'), 'label' => false, 'class' => 'form-control', 'templates' => [
                        'inputContainer' => '<div class="form-group col-md-6">{{content}}</div>'
                ]]);
                ?>
                <div class="clearfix"></div>
                <input type="submit" class="btn btn-default" value="Get Report" name="reportSubmit"/>
                <a href="javascript:void(0)" class="btn btn-default" id="exportReportButton">Download Report</a>
                </form>
            </div>
        </div>
        <div class="device-list">
            <h2>Outdoor Media Device Report</h2>
            <div class="table-responsive">
				
                <table class="table">
                    <thead class="thead-default">
                        <tr>
                            <th>Receipt No</th>
                            <th>OMD registration number</th>
                            <th>Company/Individual Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>G8 Receipt Number</th>
                            <th>Typology</th>
                            <th>Sub Category</th>
                            <th>Payment Tracking Id (ccavenue)</th>
                            <th>Bank Reference Number</th>
                            <th>Amount (With ccavenue charges)</th>
                            <th>Amount</th>
                            <th>Payment Mode</th>
                            <th>Card Name</th>
                            <th>Status</th>
                            <th>Registered At</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                       // pr($devices);
                        if($devices) {
                        foreach ($devices as $device) {
							$g8_reponse	=	'';
							if(!empty($device['g8_response'])){
								$g8_reponse = json_decode($device['g8_response'],true);
							}
                            ?>
                            <tr>
                                <td><?php echo $device['receipt_no']; ?></td>
                                <td><?php echo $device['omd_registration_number']; ?></td>
                                <td><?php echo $device['display_name']; ?></td>
                                <td><?php echo $device['email']; ?></td>
                                <td><?php echo $device['mobile']; ?></td>
                                <td><?php echo (!empty($g8_reponse)) ? $g8_reponse['RECEIPT_NUMBER'] : ''; ?></td>
                                <td><?php echo $device['typology']; ?></td>
                                <td><?php echo $device['sub_category']; ?></td>
                                <td><?php echo $device['payment_tracking_id']; ?></td>
                                <td><?php echo $device['bank_reference_number']; ?></td>
                                <td><?php echo $device['cc_amount']; ?></td>
                                <td><?php echo $device['amount']; ?></td>
                                <td><?php echo $device['payment_mode']; ?></td>
                                <td><?php echo $device['card_name']; ?></td>
                                <td><?php echo $device['status']; ?></td>
                                <td><?php echo $device['registered_at']; ?></td>
                            </tr>
                            <?php
                        } }
                        if(!$devices) {
                            ?>
                            <tr>
                                <td colspan="15" style="text-align: center;">No Record</td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
              
            </div>
        </div>
    </div>
</div>
<script>
    $('#typologies').on('change', function () {
        var id = $(this).val();
        $.ajax({
            url: '<?php echo $this->Url->build(["action" => "getSubCategory"]); ?>',
            type: 'POST',
            dataType: 'JSON',
            data: {id: id},
            beforeSend: function (xhr) {
                var docHeight = $(document).height();
                $('#loaderLayout span').css({height: docHeight + 'px'});
                $('#loaderLayout').show();
            },
            success: function (response) {
                $('#loaderLayout').hide();
                var html = '<option value="">Select Sub Category</option>';
                $.each(response, function (index, val) {
                    html += '<option value="' + val.id + '">' + val.title + '</option>';
                });
                $('#subCat').html(html);
            },
            error: function () {
                alert("Something went wrong on server. Please try again.");
            }
        });
    });
    $('#exportReportButton').click(function () {
        var html = '<input type="hidden" value="export" name="request_type">';
        $('#exportReportForm').prepend(html).submit();
    });
</script>
