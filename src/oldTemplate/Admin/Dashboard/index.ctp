<?php
$status = $this->request->query('status');
?>
<!-- Keep all page content within the page-content inset div! -->
<div class="page-content inset dashboard">
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-V-cjIUZu6DPHpERzLBIV5OnR_AxkRtw"></script>
    <div class="dash-head">
        <p>Welcome <?php echo $user['display_name'];?></p>
        <h1>Dashboard</h1>
    </div>
    <div class="map-vw">
		<div id="map-canvas" style="height: 400px;"></div>
    </div>
    <?php
    /*
    ?>
    <div class="city-status clearfix">
        <ul class="pull-right">
            <li><a href="<?php echo $this->Url->build([ "controller" => "Dashboard"]); ?>" class="<?php echo empty($status) ? 'active' : ''; ?>">All</a></li>
            <li><a href="<?php echo $this->Url->build([ "controller" => "Dashboard"]) . "?status=pending"; ?>" class="<?php echo!empty($status) && 'pending' == $status ? 'active' : ''; ?>">Pending</a></li>
            <li><a href="<?php echo $this->Url->build([ "controller" => "Dashboard"]) . "?status=approved"; ?>" class="<?php echo!empty($status) && 'approved' == $status ? 'active' : ''; ?>">Approved</a></li>
            <li><a href="<?php echo $this->Url->build([ "controller" => "Dashboard"]) . "?status=rejected"; ?>" class="<?php echo!empty($status) && 'rejected' == $status ? 'active' : ''; ?>">Rejected</a></li>

        </ul>
    </div>
    <div class="device-list">
        <h2>Outdoor Media Device List </h2>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-default">
                    <tr>
                        <th>Device Id</th>
                        <th>Location</th>
                        <th>Status</th>
                        <th>Concession Fees</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($devices)) {
                        foreach ($devices as $key => $deviceArray) {
                            //pr($deviceArray);
                            ?>								
                            <tr deviceRow="<?php echo $deviceArray['id']; ?>" class="deviceRow">
                                <td scope="row"><?php echo $deviceArray['id']; ?></td>
                                <td><?php echo isset($deviceArray['location']) ? $deviceArray['location'] : 'N/A'; ?></td>
                                <!--<td class="sts-pending sts-apprd sts-rejected">Pending</td>-->
                                <td class="sts-<?php echo $deviceArray['status']; ?>"><?php echo ucfirst($deviceArray['status']); ?></td>
                                <td><?php echo isset($deviceArray['concession_fees']) ? $deviceArray['concession_fees'] : 'N/A'; ?></td>	
                            </tr>							
                            <?php
                        }
                        if ($this->Paginator->numbers()) {
                            ?>
                            <tr>
                                <td colspan="5"><ul class="pagingPages"><?php echo $this->Paginator->prev() . $this->Paginator->numbers() . $this->Paginator->next(); ?></ul></td>
                            </tr>
                            <?php
                        }
                        ?>
                    <?php } else {
                        ?>
                        <tr>
                            <td colspan="4" class="text-center">No results found.</td>
                        </tr>							
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div style="display: none;" id="deviceBlock">
        <div class="row">
            <div class="col-md-6">
                <div class="mcgform regist-form">
                    <h2 class="formh2">Device Details</h2>
                    <div class="row">
                        <form class="prevwfm" id="deviceDetailBlock"></form>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="mcgform regist-form ">
                    <h2 class="formh2">Company Details</h2>
                    <div class="row">
                        <form class="prevwfm" id="deviceAddressDetailBlock"></form>
                    </div>
                </div>
            </div>
        </div>
        <h2 class="imgsite formh2">Photographs of Site</h2>
        <div class="row" id="siteImages">
            <div class="col-md-3">
                <div class="siteimg">
                    <img src="<?php echo $this->Url->image('notfound.jpg'); ?>" />
                </div>
            </div>
            <div class="col-md-3">
                <div class="siteimg">
                    <img src="<?php echo $this->Url->image('notfound.jpg'); ?>" />
                </div>
            </div>
            <div class="col-md-3">
                <div class="siteimg">
                    <img src="<?php echo $this->Url->image('notfound.jpg'); ?>" />
                </div>
            </div>
            <div class="col-md-3">
                <div class="siteimg">
                    <img src="<?php echo $this->Url->image('notfound.jpg'); ?>" />
                </div>
            </div>
        </div>
        <h2 class="imgsite formh2 mcgborder">Process OMD applications </h2>
        <div class="comt-blk" id="previous-comments" style="display: none;">
            <label for="exampleInputEmail1">Previous Comments</label>
            <div class="form-group">
                    <!--<textarea class="form-control" placeholder="Comments"></textarea>-->
            </div>
        </div>
    </div>
    <?php  
        */
    ?>
</div>
<script>

    function initialize() {

        var marker;
        var latLngObj = '<?php echo $latLngArray; ?>';

        var latLngObj = jQuery.parseJSON(latLngObj);

        var map;
        var i;
        var bounds = new google.maps.LatLngBounds();

        var mapOptions = {
            //center: new google.maps.LatLng(21.1701211, 72.8282404),
            center: new google.maps.LatLng(28.4089, 77.3178),
            zoom: 12,
        };

        // Display a map on the page
        map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
         var myLatLng = {lat: 28.4089, lng: 77.3178};
        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'faridabad'
        });

		/*
        $.each(latLngObj, function (index, val) {

            var position = new google.maps.LatLng(val.lat, val.lng);
            bounds.extend(position);
            marker = new google.maps.Marker({
                position: position,
                map: map
            });

            // Automatically center the map fitting all markers on the screen
            map.fitBounds(bounds);
        });
       */ 

    }

    //// Initialize map
    initialize();

    $('.deviceRow').on('click', function () {
        $('#deviceBlock').slideDown();
        var deviceId = $(this).attr('devicerow');
        $('.deviceRow').removeClass('active-device');
        $(this).addClass('active-device');
        Devices.deviceId = deviceId;
        Devices.deviceImages = [];
        Devices.getDeviceInformation();
        $('html,body').animate({scrollTop: $("#deviceBlock").offset().top}, 'slow');
    });

    var Devices = {
        deviceId: '',
        deviceData: JSON.parse('<?php echo $deviceData; ?>'),
        imagePath: '<?php echo $deviceImagePath; ?>',
        deviceImages: [],
        fields: JSON.parse('<?php echo $fields; ?>'),
        deviceFields: JSON.parse('<?php echo $deviceFields; ?>'),
        typologyData: JSON.parse('<?php echo $typologyData; ?>'),
        addressData: JSON.parse('<?php echo $addresses; ?>'),
        comments: JSON.parse('<?php echo $comments; ?>'),
        getDeviceInformation: function () {
            var self = this;
            var deviceId = self.deviceId;
            self.getDeviceDetailsHTML(deviceId);
            self.getDeviceImagesHTML();
            self.getDeviceAddressHTML(deviceId);
            self.getCommentsHTML();
        },
        getDeviceDetailsHTML: function (deviceId) {
            var self = this;
            var html = '';
            var deviceFields = self.deviceFields[deviceId];
            var typologyData = self.typologyData[deviceId];
            html += '<div class="form-group clearfix">';
            html += '<label class="col-md-6" for="">Typology</label>';
            html += '<div class="col-md-6">';
            html += '<span>' + typologyData.typology + '</span>';
            html += '</div>';
            html += '</div>';
            html += '<div class="form-group clearfix">';
            html += '<label class="col-md-6" for="">Sub Category</label>';
            html += '<div class="col-md-6">';
            html += '<span>' + typologyData.sub_category + '</span>';
            html += '</div>';
            html += '</div>';
            $.each(deviceFields, function (index, val) {
                var fieldData = self.fields[val.field];
                if (fieldData.type !== 'file') {
                    html += '<div class="form-group clearfix">';
                    html += '<label class="col-md-6" for="">' + fieldData.label + '</label>';
                    html += '<div class="col-md-6">';
                    html += '<span>' + (val.value ? val.value : '--N/A--') + '</span>';
                    html += '</div>';
                    html += '</div>';
                } else {
                    self.deviceImages.push({label: fieldData.label, image: self.imagePath + val.value});
                }
            });
            $('#deviceDetailBlock').html(html);
        },
        getDeviceImagesHTML: function () {
            var self = this;
            var html = '';
            var extRegex = /(?:\.([^.]+))?$/;
            var deviceImages = self.deviceImages;
            $.each(deviceImages, function (index, image) {
                var fileExt = extRegex.exec(image.image);
                var ValidImageTypes = ["gif", "jpeg", "png", "jpg"];
                var imageHTML = '<a href="' + image.image + '" download="' + image.label + '.' + fileExt[1] + '" title="Download ' + image.label + '"><img src="' + image.image + '" /></a>';
                if ($.inArray(fileExt[1], ValidImageTypes) < 0) {
                    imageHTML = '<a href="' + image.image + '" download="' + image.label + '.' + fileExt[1] + '" class="downloadDocument" title="Download ' + image.label + '"><i class="glyphicon glyphicon-save"></i></a>';
                }
                html += '<div class="col-md-3">';
                html += '<div class="siteimg">';
                html += imageHTML;
                html += ' </div>';
                html += '<label style="float: left; width: 100%; text-align: center;">' + image.label + '</label>';
                html += ' </div>';
            });
            $('#siteImages').html(html);
        },
        getDeviceAddressHTML: function (deviceId) {
            var html = '';
            var addresses = this.addressData[deviceId];
            var deviceData = this.deviceData[deviceId];
            if(!deviceData.company_name) {
                $('#deviceAddressDetailBlock').html('<div style="float: left; width: 100%; text-align: left; padding: 0px 0px 15px;">Company Information not Available, Device added by Admin</div>');
                return false;
            }
            $.each(addresses, function (addressLabel, address) {
                if (addressLabel !== 'company') {
                    html += '<div class="form-group clearfix">';
                    html += '<label class="col-md-12 darkgrey" for="exampleInputEmail1">' + addressLabel + ' Address</label>';
                    html += '</div>';
                }
                $.each(address, function (index, val) {
                    html += '<div class="form-group clearfix">';
                    html += '<label class="col-md-6" for="exampleInputEmail1">' + val.label + '</label>';
                    html += '<div class="col-md-6">';
                    html += '<span>' + val.value + '</span>';
                    html += '</div>';
                    html += '</div>';
                });
            });
             html += '<div class="form-group clearfix">';
            html += '<label class="col-md-6" for="">Company Name</label>';
            html += '<div class="col-md-6">';
            html += '<span>' + deviceData.company_name + '</span>';
            html += '</div>';
            html += '</div>';
            html += '<div class="form-group clearfix">';
            html += '<label class="col-md-6" for="">Director Name</label>';
            html += '<div class="col-md-6">';
            html += '<span>' + deviceData.director_name + '</span>';
            html += '</div>';
            html += '</div>';
            html += '<div class="form-group clearfix">';
            html += '<label class="col-md-6" for="">Date of Incorporation</label>';
            html += '<div class="col-md-6">';
            html += '<span>' + deviceData.incorporation_date + '</span>';
            html += '</div>';
            html += '</div>';
            html += '<div class="form-group clearfix">';
            html += '<label class="col-md-6" for="">SMC Party Code</label>';
            html += '<div class="col-md-6">';
            html += '<span>' + deviceData.smc_party_code + '</span>';
            html += '</div>';
            html += '</div>';
            html += '<div class="form-group clearfix">';
            html += '<label class="col-md-6" for="">Email</label>';
            html += '<div class="col-md-6">';
            html += '<span>' + deviceData.email + '</span>';
            html += '</div>';
            html += '</div>';
            html += '<div class="form-group clearfix">';
            html += '<label class="col-md-6" for="">Mobile</label>';
            html += '<div class="col-md-6">';
            html += '<span>' + deviceData.phone + '</span>';
            html += '</div>';
            html += '</div>';
            html += '<div class="form-group clearfix">';
            html += '<label class="col-md-6" for="">Entity Type</label>';
            html += '<div class="col-md-6">';
            html += '<span>' + deviceData.representative + '</span>';
            html += '</div>';
            html += '</div>';
            $('#deviceAddressDetailBlock').html(html);
        },
        getCommentsHTML: function () {
            var self = this;
            var html = '';
            var comments = self.comments[self.deviceId];
            if (typeof comments !== 'undefined') {
                $.each(comments, function (index, comment) {
                    html += '<div class="form-group clearfix">';
                    html += '<label class="col-md-12 darkgrey" for="">Comment ' + (index + 1) + '</label>';
                    html += '</div>';
                    html += '<div class="form-group clearfix">';
                    html += '<label class="col-md-6" for="">User Name</label>';
                    html += '<div class="col-md-6">';
                    html += '<span>' + comment.user_name + ' ( ' + comment.type + ' )</span>';
                    html += '</div>';
                    html += '</div>';
                    html += '<div class="form-group clearfix">';
                    html += '<label class="col-md-6" for="">Status</label>';
                    html += '<div class="col-md-6">';
                    html += '<span>' + comment.status + '</span>';
                    html += '</div>';
                    html += '</div>';
                    html += '<div class="form-group clearfix">';
                    html += '<label class="col-md-6" for="">Comment</label>';
                    html += '<div class="col-md-6">';
                    html += '<span>' + comment.comment + '</span>';
                    html += '</div>';
                    html += '</div>';
                    $.each(comment.comment_data, function (commentIndex, commentVal) {
                        html += '<div class="form-group clearfix">';
                        html += '<label class="col-md-6" for="">' + self.fields[commentVal.field_key].label + '</label>';
                        html += '<div class="col-md-6">';
                        html += '<span>' + commentVal.input_value + '</span>';
                        html += '</div>';
                        html += '</div>';
                    });
                });
            }
            if (html) {
                $('#previous-comments').show();
            } else {
                $('#previous-comments').hide();
            }
            $('#previous-comments .form-group').html(html);
        }
    };
</script>
