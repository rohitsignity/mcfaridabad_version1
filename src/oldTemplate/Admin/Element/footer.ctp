<footer>
    <section class="footer">
        <div class="container">
            <div class="foot-wrap">
<!--                <div class="social">
                    <ul class="clearfix">
                        <li><a href="javascript:void(0)"><img src="<?php //echo $this->Url->image('facebook1.png'); ?>" /></a></li>
                        <li><a href="javascript:void(0)"><img src="<?php //echo $this->Url->image('twitter.png'); ?>" /></a></li>
                        <li><a href="javascript:void(0)"><img src="<?php //echo $this->Url->image('linkdin.png'); ?>" /></a></li>
                    </ul>
                </div>-->
                <div class="footer-nav">
                    <ul class="nav">
                        <li><a href="<?php echo $this->Url->build([ "controller" => "Home", "action" => "index","prefix" => false]); ?>">Home</a></li>
                        <li><a href="<?php echo $this->Url->build(['controller' => 'aboutUs',"action" => "index","prefix" => false]);?>">About</a></li>
                        <li><a href="http://www.mcg.gov.in/Contact-Us.aspx">Contact us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <div class="cpoyr">
        <p>Copyright © <?php echo date('Y');?> Outdoor Media Management System. All Rights Reserved.</p>
    </div>
</footer>