<header class="header">
    <!-- navbar -->
    <div class="dashboard-head clearfix">
        <div class="logo-nw pull-left">
            <a href="<?php echo $this->Url->build([ "controller" => "Dashboard", "action" => "index"]); ?>" class=""><img src="<?php echo $this->Url->image('logo2.png'); ?>" /></a>
        </div>
        <div class="vndr-profile pull-right">
            <ul class="clearfix">
                <!--<li class=""><a href="#" class="prfl"><span class="glyphicon glyphicon-user"></span><p><?php echo $user['display_name']; ?></p></a></li>-->
                <li class=""><a href="#" class="prfl"><img src="<?php echo $this->Url->image('logo_admin.png')?>" alt="Admin Logo"><p><?php echo $user['display_name']; ?></p></a></li>
                <li class="notify"><img src="<?php echo $this->Url->image('notification.png'); ?>" /><span class="badge">3</span></li>
                <li><a href="<?php echo $this->Url->build([ "controller" => "Users", "action" => "logout"]); ?>" class="orang">Logout</a></li>
            </ul>
        </div>
    </div>

</header>

<div id="wrapper" class="active">
    <!-- Sidebar -->
<?php echo $this->element('sidebar');
