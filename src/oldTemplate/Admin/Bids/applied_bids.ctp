<div class="page-content inset dashboard">
    <div class="device-list">
        <h2><a href="<?php echo $this->Url->build(['controller' => 'Bids','action' => 'index']);?>">Applied Tenders</a> > Applied Bids</h2>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-default">
                    <tr>
                        <th>#</th>
                        <th>Tender Name</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Device Name</th>
                        <th>Applied By</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($appliedBids)) {
                        $sNo = (($this->Paginator->current() - 1) * $limit) + 1;
                        foreach ($appliedBids as $appliedBid) {
                            ?>
                            <tr onclick="AppliedBids.getDetail(<?php echo $appliedBid['id']; ?>, this)" style="cursor: pointer;">
                                <td scope="row"><?php echo $sNo; ?></td>
                                <td><?php echo $appliedBid['tender_name']; ?></td>
                                <td><?php echo date('d-m-Y', strtotime($appliedBid['start_date'])); ?></td>
                                <td><?php echo date('d-m-Y', strtotime($appliedBid['end_date'])); ?></td>
                                <td><?php echo $appliedBid['device_name']; ?></td>
                                <td><?php echo $appliedBid['applied_by']; ?></td>
                                <td><?php echo $appliedBid['status'] ? 'Enable' : 'Disable'; ?></td>
                            </tr>							
                            <?php
                            $sNo++;
                        }
                        if ($this->Paginator->numbers()) {
                            ?>
                            <tr>
                                <td colspan="7"><ul class="pagingPages"><?php echo $this->Paginator->prev() . $this->Paginator->numbers() . $this->Paginator->next(); ?></ul></td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr><td colspan="6" style="text-align: center;">No Record Found</td></tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div style="" id="deviceBlock">

        <div class="row">
            <div class="col-md-6">
                <div class="mcgform regist-form">
                    <h2 class="formh2">Device Details</h2>
                    <div class="row">
                        <form class="prevwfm" id="deviceinfo"></form>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="mcgform regist-form ">
                    <h2 class="formh2">Applied By</h2>
                    <div class="row">
                        <form class="prevwfm" id="appliedByInfo"></form>
                    </div>
                </div>
            </div>

        </div>
        <h2 class="imgsite formh2">Device documents</h2>
        <div class="row" id="siteImages">
            <div class="col-md-3">
                <div class="siteimg">
                    <img src="<?= $this->Url->image('notfound.jpg'); ?>" />
                </div>
            </div>
            <div class="col-md-3">
                <div class="siteimg">
                    <img src="<?= $this->Url->image('notfound.jpg'); ?>" />
                </div>
            </div>
            <div class="col-md-3">
                <div class="siteimg">
                    <img src="<?= $this->Url->image('notfound.jpg'); ?>" />
                </div>
            </div>
            <div class="col-md-3">
                <div class="siteimg">
                    <img src="<?= $this->Url->image('notfound.jpg'); ?>" />
                </div>
            </div>
        </div>
        <div id="bidDocuments">
            
        </div>
        <h2 class="imgsite formh2 mcgborder">Process OMD applications </h2>
        <div class="comt-blk">
            <label for="exampleInputEmail1">Comments</label>
            <div class="form-group">
                <textarea class="form-control" placeholder="Comments"></textarea>
            </div>
        </div>
        <div class="submit">
            <input class="btn btn-default approvd" value="Approve" type="submit">
            <input class="btn btn-default hld" value="Hold" type="submit">
        </div>
    </div>
</div>
<?= $this->Html->script('AppliedBids.js'); ?>
<script>
    AppliedBids.appliedBidUrl = '<?php echo $this->Url->build(['controller' => 'Bids', 'action' => 'getAppliedBidDetail']); ?>';
    AppliedBids.deviceImagePath = '<?php echo $deviceImagePath;?>';
    AppliedBids.bidDocumentsPath = '<?php echo $bidDocumentsPath;?>';
</script>