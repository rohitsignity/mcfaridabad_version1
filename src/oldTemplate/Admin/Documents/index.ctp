<div class="page-content inset dashboard">
    <div class="device-list">
        <h2>Documents List <?php echo $this->Html->link('Add Document', ['action' => 'add'], ['class' => 'btn btn-default tender-add-btn pull-right']);?></h2>
        <div class="table-responsive">
            <table class="table">
                <thead>
                <th>S No.</th>
                <th>Document Name</th>
                </thead>
                <tbody>
                    <?php
                    if (!empty($documents)) {
                        $sno = (($page * $limit) - $limit) + 1;
                        foreach ($documents as $document) {
                            ?>
                            <tr>
                                <td><?php echo $sno; ?></td>
                                <td><?php echo $document['name']; ?></td>
                            </tr>							
                            <?php
                            $sno++;
                        }
                        if ($this->Paginator->numbers()) {
                            ?>
                            <tr>
                                <td colspan="6"><ul class="pagingPages"><?php echo $this->Paginator->prev() . $this->Paginator->numbers() . $this->Paginator->next(); ?></ul></td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr><td colspan="6" style="text-align: center;">No Record Found</td></tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>