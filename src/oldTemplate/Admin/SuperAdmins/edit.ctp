<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $superAdmin->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $superAdmin->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Super Admins'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="superAdmins form large-9 medium-8 columns content">
    <?= $this->Form->create($superAdmin) ?>
    <fieldset>
        <legend><?= __('Edit Super Admin') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('email');
            echo $this->Form->input('password');
            echo $this->Form->input('level');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
