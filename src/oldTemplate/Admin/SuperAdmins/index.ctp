<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Super Admin'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="superAdmins index large-9 medium-8 columns content">
    <h3><?= __('Super Admins') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('password') ?></th>
                <th scope="col"><?= $this->Paginator->sort('level') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($superAdmins as $superAdmin): ?>
            <tr>
                <td><?= $this->Number->format($superAdmin->id) ?></td>
                <td><?= h($superAdmin->name) ?></td>
                <td><?= h($superAdmin->email) ?></td>
                <td><?= h($superAdmin->password) ?></td>
                <td><?= $this->Number->format($superAdmin->level) ?></td>
                <td><?= h($superAdmin->created) ?></td>
                <td><?= h($superAdmin->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $superAdmin->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $superAdmin->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $superAdmin->id], ['confirm' => __('Are you sure you want to delete # {0}?', $superAdmin->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
