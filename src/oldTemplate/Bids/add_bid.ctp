<div id="page-content-wrapper">
    <!-- Keep all page content within the page-content inset div! -->
    <div class="page-content inset paymentpg">
        <div class="headings-all dash-in">
            <h1>Tender</h1>
        </div>
        <div class="device-list">
            <div class="table-responsive">
                <form action="javascript:void(0)" onsubmit="Bids.bidIt(this);" enctype="multipart/form-data" method="POST">
                    <!--                    <div class="form-group">
                                            <input type="text" name="bid" class="form-control bidField" onkeyup="Bids.isNumeric(this)" autocomplete="off" placeholder="Bid Amount">
                                        </div>-->
                    <?php
                    foreach ($tender['doc_names'] as $key => $doc) {
                        ?>
                        <div class="form-group">
                            <label><?= $doc; ?></label>
                            <input name="document_name[<?= $key; ?>]" type="hidden" value="<?= $doc; ?>" class="docNames">
                            <input type="file" name="document[<?= $key; ?>]" class="docs">
                        </div>
                        <?php
                    }
                    ?>
                    <input type="submit" value="Apply for Bid" class="btn btn-default bidButton">
                </form>
                <table class="table">
                    <tbody>
                        <tr>
                            <td><b>Tender Name</b></td>
                            <td><?= $tender['name'] ?></td>
                        </tr>
                        <tr>
                            <td><b>Start Date</b></td>
                            <td><?= date('d M, Y', strtotime($tender['start_date'])); ?></td>
                        </tr>
                        <tr>
                            <td><b>End Date</b></td>
                            <td><?= date('d M, Y', strtotime($tender['end_date'])); ?></td>
                        </tr>
                        <tr>
                            <td><b>Minimum Amount</b></td>
                            <td><?= $tender['min_amount']; ?></td>
                        </tr>
                        <tr>
                            <td><b>Contact</b></td>
                            <td><?= $tender['contact']; ?></td>
                        </tr>
                        <?php
                        if ($tender['web_link']) {
                            ?>
                            <tr>
                                <td><b>Website</b></td>
                                <td><a href="<?= $tender['web_link']; ?>" target="_blank"><?= $tender['web_link']; ?></a></td>
                            </tr>
                            <?php
                        }
                        ?>
                        <tr>
                            <td><b>Typology</b></td>
                            <td><?= $deviceData['typology']; ?></td>
                        </tr>
                        <tr>
                            <td><b>Sub Category</b></td>
                            <td><?= $deviceData['sub_category']; ?></td>
                        </tr>
                        <?php
                        $filesData = [];
                        foreach ($deviceData['fields'] as $field) {
                            $fieldKey = $field['field'];
                            $fieldData = array_values(array_filter($deviceData['fieldsData'], function($arr) use($fieldKey) {
                                        return $arr['field_key'] == $fieldKey;
                                    }));
                            if ($fieldData[0]['type'] == 'file') {
                                $fileInfo = pathinfo($field['value']);
                                $filesData[] = [
                                    'key' => $fieldData[0]['lable'],
                                    'url' => $devicePath . $field['value'],
                                    'extension' => $fileInfo['extension']
                                ];
                                continue;
                            }
                            ?>
                            <tr>
                                <td><b><?= $fieldData[0]['lable']; ?></b></td>
                                <td><?= $field['value']; ?></td>
                            </tr>
                            <?php
                        }
                        foreach ($filesData as $file) {
                            ?>
                            <tr>
                                <td><b><?= $file['key']; ?></b></td>
                                <td><a href="<?= $file['url']; ?>" download="<?= $file['key'] . '.' . $file['extension']; ?>">Download</a></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    //var applyBidUrl = '<?php echo $this->Url->build(['controller' => 'Bids', 'action' => 'applyBid']); ?>';
</script>
<?= $this->Html->script('bids.js'); ?>
<script>
//    Bids.tenderId = <?php //echo $tender['id']; ?>;
//    Bids.minVal = <?php //echo $tender['min_amount']; ?>;
//    Bids.maxVal = '<?php //echo $tender['max_amount'] ? $tender['max_amount'] : ''; ?>';
</script>