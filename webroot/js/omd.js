var Omd = {
    omdTypeUrl: '',
    omdTypeVal: '',
    downloadBidDoc: '',
    startCheckUrl: '',
    bidUrl: '',
    showLoader: function () {
        var docHeight = $(document).height();
        $('#loaderLayout span').css({height: docHeight + 'px'});
        $('#loaderLayout').show();
    },
    hideLoader: function () {
        $('#loaderLayout').hide();
    },
    omdType: function (element) {
        this.omdTypeVal = $(element).val();
        this.setOmdType();
    },
    setOmdType: function () {
        var self = this;
        $.ajax({
            url: self.omdTypeUrl,
            type: 'PUT',
            data: {term: self.omdTypeVal},
            dataType: 'JSON',
            beforeSend: function (xhr) {
                self.showLoader();
            },
            success: function (response) {
                if (!response.error) {
                    window.location = response.url;
                } else {
                    self.hideLoader();
                    swal("Error!", "Something Went wrong on the Server. Please try again.", "error");
                }
            },
            error: function () {
                self.hideLoader();
                swal("Error!", "Something Went wrong on the Server. Please try again.", "error");
            }
        });
    },
    downloadBidDocument: function (id) {
        var self = this;
        $.ajax({
            url: self.downloadBidDoc,
            type: 'POST',
            data: {id: id},
            dataType: 'JSON',
            beforeSend: function (xhr) {
                self.showLoader();
            },
            success: function (response) {
                if (!response.error) {
                    var element = $('#downloadBidDoc');
                    element.attr({href: response.url, download: response.doc});
                    element.get(0).click();
                } else {
                    swal("Alert!", response.message, "warning");
                }
                self.hideLoader();
            },
            error: function (result) {
                self.hideLoader();
                swal("Error!", "Something went wrong on the server. Please try again.", "error");
            }
        });
    },
    checkBidStart: function(id) {
        var self = this;
        $.ajax({
            url: self.startCheckUrl,
            type: 'POST',
            data: { id: id },
            dataType: 'JSON',
            success: function(response) {
                if(!response.error) {
                    window.location = self.bidUrl + '/' + id;
                } else {
                    swal("Alert!",response.message,"warning");
                }
            },
            error: function(result) {
                swal("Error!","Something went wrong on the Server. Please try again.","error");
            }
        });
    }
};