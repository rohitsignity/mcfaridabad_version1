MetroMrts = {
    insideMetroCounter: 0,
    outsideMetroCounter: 0,
    insideStationCounter: 0,
    stationBrandingCounter: 0,
    getView: function (element) {
        var self = this;
        var metroMrtsCategory = $.trim((element.val()).toLowerCase());
        $(document).find('input[name="location"], #map-canvas, input[name="lat"], input[name="lng"]').parent('div').show();
        if(metroMrtsCategory === 'rolling stock') {
            $(document).find('input[name="location"], #map-canvas, input[name="lat"], input[name="lng"]').parent('div').hide();
        }
        if (metroMrtsCategory) {
            var fn = metroMrtsCategory.replace(" ", "_") + '_html';
            var htmlFn = self[fn];
            htmlFn();
        } else {
            $(document).find('#metro_mrts_categories_block').html('');
        }
    },
    rolling_stock_html: function () {
        var html = '<div class="col-md-12" id="metro_type_check">';
        html += '<h4>Rolling Stock Type</h4>';
        html += '<div class="col-md-6">';
        html += '<label><input type="checkbox" name="metro_type[]" value="inside_metro" onchange="MetroMrts.getRollingStock(this);">Inside Metro</label>';
        html += '</div>';
        html += '<div class="col-md-6">';
        html += '<label><input type="checkbox" name="metro_type[]" value="outside_metro" onchange="MetroMrts.getRollingStock(this);">Outside Metro</label>';
        html += '</div>';
        html += '</div>';
        html += '<div id="metro_type_block" class="col-md-12"><div id="inside_metro_block"></div><div id="outside_metro_block"></div></div>';
        $(document).find('#metro_mrts_categories_block').html(html);
    },
    inside_station_html: function () {
        var self = this;
        self.insideStationCounter = 0;
        var html = '<div class="col-md-12" id="inside_station_section">';
        html += '<h3>Inside Station</h3>';
        html += '<table class="table">';
        html += '<thead>';
        html += '<tr>';
        html += '<th>S.no</th>';
        html += '<th>OMD Site</th>';
        html += '<th>Height(in m)</th>';
        html += '<th>Width(in m)</th>';
        html += '<th>Area(in sq. m)</th>';
        html += '<th></th>';
        html += '</tr>';
        html += '</thead>';
        html += '<tbody>';
        html += '<tr>';
        html += '<td class="serial-no">1</td>';
        html += '<td>';
        html += '<select class="form-control" name="inside_station_omd_type['+ self.insideStationCounter +']">';
        html += '<option value="">Select OMD Site</option>';
        html += '<option value="Unipole">Unipole</option>';
        html += '<option value="Wall Wrap">Wall Wrap</option>';
        html += '<option value="Wall Mounted">Wall Mounted</option>';
        html += '<option value="Others">Others</option>';
        html += '</select>';
        html += '</td>';
        html += '<td><input type="text" class="form-control heightData" onkeyup="MetroMrts.calculateArea(this)" name="inside_station_omd_height['+ self.insideStationCounter +']"></td>';
        html += '<td><input type="text" class="form-control widthData" onkeyup="MetroMrts.calculateArea(this)" name="inside_station_omd_width['+ self.insideStationCounter +']"></td>';
        html += '<td><input type="text" class="form-control areaData" readonly="readonly" name="inside_station_omd_area['+ self.insideStationCounter +']"></td>';
        html += '<td><a href="javascript:void(0)" onclick="MetroMrts.addMoreInsideStaion(this)" class="plus-btn">+ Add More</a></td>';
        html += '</tr>';
        html += '</tbody>';
        html += '<tfoot>';
        html += '<tr>';
        html += '<td colspan="6"><label>Total Area(in sq. m)</lable><input type="text" class="form-control total_area" readonly="readonly" name="omd_one_roof_total_area"></td>';
        html += '</tr>';
        html += '</tfoot>';
        html += '</table>';
        html += '</div>';
        $(document).find('#metro_mrts_categories_block').html(html);
    },
    station_branding_html: function () {
        var self = this;
        self.stationBrandingCounter = 0;
        var html = '<div class="col-md-12" id="station_branding_section">';
        html += '<h3>Station Branding</h3>';
        html += '<table class="table">';
        html += '<thead>';
        html += '<tr>';
        html += '<th>S. No</th>';
        html += '<th>Facade</th>';
        html += '<th>Total Facade area(in sq. m)</th>';
        html += '<th>Area of actual display(in sq. m)</th>';
        html += '<th></th>';
        html += '</tr>';
        html += '</thead>';
        html += '<tbody>';
        html += '<tr>';
        html += '<td class="serial-no">1</td>';
        html += '<td class="facade-no">F1</td>';
        html += '<td><input type="text" class="form-control station_branding_facade" name="station_branding_total_facade['+ self.stationBrandingCounter +']"></td>';
        html += '<td><input type="text" class="form-control station_branding_actual_facade" name="station_branding_actual_facade['+ self.stationBrandingCounter +']"></td>';
        html += '<td><a href="javascript:void(0)" class="plus-btn" onclick="MetroMrts.addMoreStationBranding(this)">+ Add More</a></td>';
        html += '</tr>';
        html += '</tbody>';
        html += '</table>';
        html += '</div>';
        $(document).find('#metro_mrts_categories_block').html(html);
    },
    getRollingStock: function (element) {
        var self = this;
        var rollingStockVal = $(element).val();
        if ($(element).is(':checked')) {
            var htmlfn = self[rollingStockVal + '_html'];
            htmlfn();
        } else {
            $(document).find('#metro_type_block #' + rollingStockVal + '_block').html('');
        }
    },
    inside_metro_html: function () {
        var self = this;
        self.insideMetroCounter = 0;
        var html = '<div id="inside_metro_section">';
        html += '<h3>Inside Metro</h3>';
        html += '<table class="table">';
        html += '<thead>';
        html += '<tr>';
        html += '<th>S. No</th>';
        html += '<th>Train no.</th>';
        html += '<th>No. Of coaches</th>';
        html += '<th>No. Of Advertisement Panels</th>';
        html += '<th></th>';
        html += '</tr>';
        html += '</thead>';
        html += '<tbody>';
        html += '<tr>';
        html += '<td class="serial-no">1</td>';
        html += '<td><input type="text" class="form-control train_no" name="inside_metro_train[' + self.insideMetroCounter + ']"></td>';
        html += '<td><input type="text" class="form-control metro_coches" name="inside_metro_coches[' + self.insideMetroCounter + ']"></td>';
        html += '<td><input type="text" class="form-control ad_panels" name="inside_metro_ad_panels[' + self.insideMetroCounter + ']"></td>';
        html += '<td><a href="javascript:void(0)" class="plus-btn" onclick="MetroMrts.addMoreInsideMetro(this);">+ Add More</a></td>';
        html += '</tr>';
        html += '</tbody>';
        html += '</table>';
        html += '</div>';
        $('#metro_type_block #inside_metro_block').append(html);
    },
    outside_metro_html: function () {
        var self = this;
        self.outsideMetroCounter = 0;
        var html = '<div id="outside_metro_section">';
        html += '<h3>Outside Metro</h3>';
        html += '<table class="table">';
        html += '<thead>';
        html += '<tr>';
        html += '<th>S. No</th>';
        html += '<th>Train no.</th>';
        html += '<th>No. of coaches for display of advertisement</th>';
        html += '<th></th>';
        html += '</tr>';
        html += '</thead>';
        html += '<tbody>';
        html += '<tr>';
        html += '<td class="serial-no">1</td>';
        html += '<td><input type="text" class="form-control train_no" name="outside_metro_train[' + self.outsideMetroCounter + ']"></td>';
        html += '<td><input type="text" class="form-control metro_coches" name="outside_metro_coches[' + self.outsideMetroCounter + ']"></td>';
        html += '<td><a href="javascript:void(0)" class="plus-btn" onclick="MetroMrts.addMoreOutsideMetro(this)">+ Add More</a></td>';
        html += '</tr>';
        html += '</tbody>';
        html += '</table>';
        html += '</div>';
        $('#metro_type_block #outside_metro_block').append(html);
    },
    addMoreInsideMetro: function (element) {
        var self = this;
        self.insideMetroCounter++;
        var html = '<tr>';
        html += '<td class="serial-no">1</td>';
        html += '<td><input type="text" class="form-control train_no" name="inside_metro_train[' + self.insideMetroCounter + ']"></td>';
        html += '<td><input type="text" class="form-control metro_coches" name="inside_metro_coches[' + self.insideMetroCounter + ']"></td>';
        html += '<td><input type="text" class="form-control ad_panels" name="inside_metro_ad_panels[' + self.insideMetroCounter + ']"></td>';
        if ($(document).find('#inside_metro_section table tbody tr').length < 19) {
            html += '<td><a href="javascript:void(0)" class="plus-btn" onclick="MetroMrts.addMoreInsideMetro(this);">+ Add More</a></td>';
        } else {
            html += '<td><a href="javascript:void(0)" class="close-btn" onclick="MetroMrts.removeInsideMetro(this);">X</a></td>';
        }
        html += '</tr>';
        $(document).find('#inside_metro_section table tbody').append(html);
        $(element).attr('onclick', 'MetroMrts.removeInsideMetro(this)');
        $(element).html('X');
        $(element).removeClass('plus-btn');
        $(element).addClass('close-btn');
        self.serialNos();
    },
    removeInsideMetro: function (element) {
        $(element).parents('tr').remove();
        this.serialNos();
        if ($(document).find('#inside_metro_section table tbody tr').length < 20) {
            $(document).find('#inside_metro_section table tbody tr:last-child td:last-child').html('<a href="javascript:void(0)" class="plus-btn" onclick="MetroMrts.addMoreInsideMetro(this);">+ Add More</a>');
        }
    },
    addMoreOutsideMetro: function (element) {
        var self = this;
        self.outsideMetroCounter++;
        var html = '<tr>';
        html += '<td class="serial-no">1</td>';
        html += '<td><input type="text" class="form-control train_no" name="outside_metro_train[' + self.outsideMetroCounter + ']"></td>';
        html += '<td><input type="text" class="form-control metro_coches" name="outside_metro_coches[' + self.outsideMetroCounter + ']"></td>';
        if ($(document).find('#outside_metro_section table tbody tr').length < 19) {
            html += '<td><a href="javascript:void(0)" class="plus-btn" onclick="MetroMrts.addMoreOutsideMetro(this)">+ Add More</a></td>';
        } else {
            html += '<td><a href="javascript:void(0)" class="close-btn" onclick="MetroMrts.removeOutsideMetro(this)">X</a></td>';
        }
        html += '</tr>';
        $('#metro_type_block #outside_metro_block table tbody').append(html);
        $(element).attr('onclick', 'MetroMrts.removeOutsideMetro(this)');
        $(element).html('X');
        $(element).removeClass('plus-btn');
        $(element).addClass('close-btn');
        self.serialNos();
    },
    removeOutsideMetro: function (element) {
        $(element).parents('tr').remove();
        this.serialNos();
        if ($(document).find('#outside_metro_section table tbody tr').length < 20) {
            $(document).find('#outside_metro_section table tbody tr:last-child td:last-child').html('<a href="javascript:void(0)" class="plus-btn" onclick="MetroMrts.addMoreOutsideMetro(this);">+ Add More</a>');
        }
    },
    addMoreInsideStaion: function (element) {
        var self = this;
        self.insideStationCounter++;
        var html = '<tr>';
        html += '<td class="serial-no">1</td>';
        html += '<td>';
        html += '<select class="form-control" name="inside_station_omd_type['+ self.insideStationCounter +']">';
        html += '<option value="">Select OMD Site</option>';
        html += '<option value="Unipole">Unipole</option>';
        html += '<option value="Wall Wrap">Wall Wrap</option>';
        html += '<option value="Wall Mounted">Wall Mounted</option>';
        html += '<option value="Others">Others</option>';
        html += '</select>';
        html += '</td>';
        html += '<td><input type="text" class="form-control heightData" onkeyup="MetroMrts.calculateArea(this)" name="inside_station_omd_height['+ self.insideStationCounter +']"></td>';
        html += '<td><input type="text" class="form-control widthData" onkeyup="MetroMrts.calculateArea(this)" name="inside_station_omd_width['+ self.insideStationCounter +']"></td>';
        html += '<td><input type="text" class="form-control areaData" readonly="readonly" name="inside_station_omd_area['+ self.insideStationCounter +']"></td>';
        if($(document).find('#inside_station_section table tbody tr').length < 19) {
            html += '<td><a href="javascript:void(0)" onclick="MetroMrts.addMoreInsideStaion(this)" class="plus-btn">+ Add More</a></td>';
        } else {
            html += '<td><a href="javascript:void(0)" onclick="MetroMrts.removeInsideStaion(this)" class="close-btn">X</a></td>';
        }
        html += '</tr>';
        $(document).find('#inside_station_section table tbody').append(html);
        $(element).attr('onclick', 'MetroMrts.removeInsideStaion(this)');
        $(element).html('X');
        $(element).removeClass('plus-btn');
        $(element).addClass('close-btn');
        self.serialNos();
    },
    removeInsideStaion: function(element) {
        $(element).parents('tr').remove();
        if ($(document).find('#inside_station_section table tbody tr').length < 20) {
            $(document).find('#inside_station_section table tbody tr:last-child td:last-child').html('<a href="javascript:void(0)" class="plus-btn" onclick="MetroMrts.addMoreInsideStaion(this);">+ Add More</a>');
        }
        this.serialNos();
    },
    addMoreStationBranding: function(element) {
        var self = this;
        self.stationBrandingCounter++;
        var html = '<tr>';
        html += '<td class="serial-no">1</td>';
        html += '<td class="facade-no">F1</td>';
        html += '<td><input type="text" class="form-control station_branding_facade" name="station_branding_total_facade['+ self.stationBrandingCounter +']"></td>';
        html += '<td><input type="text" class="form-control station_branding_actual_facade" name="station_branding_actual_facade['+ self.stationBrandingCounter +']"></td>';
        if($(document).find('#station_branding_section table tbody tr').length < 19) {
            html += '<td><a href="javascript:void(0)" class="plus-btn" onclick="MetroMrts.addMoreStationBranding(this)">+ Add More</a></td>';
        } else {
            html += '<td><a href="javascript:void(0)" class="close-btn" onclick="MetroMrts.removeStationBranding(this)">X</a></td>';
        }
        html += '</tr>';
        $(document).find('#station_branding_section table tbody').append(html);
        $(element).attr('onclick', 'MetroMrts.removeStationBranding(this)');
        $(element).html('X');
        $(element).removeClass('plus-btn');
        $(element).addClass('close-btn');
        self.serialNos();
    },
    removeStationBranding: function(element) {
        $(element).parents('tr').remove();
        if ($(document).find('#station_branding_section table tbody tr').length < 20) {
            $(document).find('#station_branding_section table tbody tr:last-child td:last-child').html('<a href="javascript:void(0)" class="plus-btn" onclick="MetroMrts.addMoreStationBranding(this);">+ Add More</a>');
        }
        this.serialNos();
    },
    serialNos: function () {
        var counter = 1;
        $(document).find('#inside_metro_section table tbody tr').each(function () {
            $(this).find('.serial-no').html(counter);
            counter++;
        });
        counter = 1;
        $(document).find('#outside_metro_section table tbody tr').each(function () {
            $(this).find('.serial-no').html(counter);
            counter++;
        });
        counter = 1;
        $(document).find('#inside_station_section table tbody tr').each(function () {
            $(this).find('.serial-no').html(counter);
            counter++;
        });
        counter = 1;
        $(document).find('#station_branding_section table tbody tr').each(function () {
            $(this).find('.serial-no').html(counter);
            $(this).find('.facade-no').html('F' +counter);
            counter++;
        });
        
    },
    calculateArea: function (element) {
        var input = $(element);
        var inputValue = input.val();
        if (inputValue) {
            if (!$.isNumeric(inputValue)) {
                var inputValue = parseFloat(inputValue);
                if (isNaN(inputValue)) {
                    inputValue = '';
                }
                input.val(inputValue);
            }
        }
        var totalArea = 0;
        var area = 0;
        var height = 0;
        var width = 0;
        var parentTable = $(element).parent('td').parent('tr').parent('tbody').parent('table');
        var parentTableBody = $(element).parent('td').parent('tr').parent('tbody');
        var parentRow = $(element).parent('td').parent('tr');
        if ($.isNumeric(parentRow.find('.heightData').val())) {
            height = parentRow.find('.heightData').val();
        }
        if ($.isNumeric(parentRow.find('.widthData').val())) {
            width = parentRow.find('.widthData').val();
        }
        area = height * width;
        parentRow.find('.areaData').val('');
        if (area) {
            parentRow.find('.areaData').val(area);
        }
        parentTableBody.find('tr').each(function () {
            if ($.isNumeric($(this).find('.areaData').val())) {
                totalArea += parseFloat($(this).find('.areaData').val());
            }
        });
        parentTable.find('tfoot .total_area').val('');
        parentTable.find('tfoot .total_annual_fees').val('');
        if (totalArea) {
            parentTable.find('tfoot .total_area').val(totalArea);
            var licence_fees = parseFloat(parentTable.find('tfoot .licence_fees').val());
            var total_annual_fees = licence_fees * totalArea * 12;
            parentTable.find('tfoot .total_annual_fees').val(total_annual_fees);
        }
    }
};