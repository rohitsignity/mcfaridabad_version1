var Tender = {
    deviceInfoUrl: '',
    devicePath: '',
    showLoader: function () {
        var docHeight = $(document).height();
        $('#loaderLayout span').css({height: docHeight + 'px'});
        $('#loaderLayout').show();
    },
    hideLoader: function () {
        $('#loaderLayout').hide();
    },
    getDeviceDetail: function (id) {
        var self = this;
        self.showLoader();
        $.ajax({
            url: self.deviceInfoUrl,
            type: 'POST',
            data: {deviceId: id},
            dataType: 'JSON',
            success: function (response) {
                self.hideLoader();
                var deviveHtml = self.getDeviceHtml(response);
                swal({
                    title: 'Device Detail',
                    showCancelButton: false,
                    html: deviveHtml,
                    animation: false
                }).then(
                    function () {
                        
                    }
                );
            },
            error: function (result) {
                self.hideLoader();
                swal("Error!", "Something went wrong on Server, Please try again.", "error");
            }
        });
    },
    getDeviceHtml: function (data) {
        var self = this;
        var html = '<div class="col-md-6 deviceInfoBlock">';
        html += '<div class="form-group">';
        html += '<label for="contact">Typology</label>';
        html += '<p>' + data.typology + '</p>';
        html += '</div>';
        html += '</div>';
        html += '<div class="col-md-6 deviceInfoBlock">';
        html += '<div class="form-group">';
        html += '<label for="contact">Sub Category</label>';
        html += '<p>' + data.sub_category + '</p>';
        html += '</div>';
        html += '</div>';
        $.each(data.fields, function (index, val) {
            var field = $.grep(data.fieldsData, function (a) {
                return a.field_key === val.field;
            });
            html += '<div class="col-md-6 deviceInfoBlock">';
            html += '<div class="form-group">';
            html += '<label for="contact">' + field[0].lable + '</label>';
            if (field[0].type !== 'file') {
                html += '<p>' + val.value + '</p>';
            } else {
                html += '<p><a href="' + self.devicePath + val.value + '" download="' + field[0].lable + '">Download</a></p>';
            }
            html += '</div>';
            html += '</div>';
        });
        return html;
    }
};