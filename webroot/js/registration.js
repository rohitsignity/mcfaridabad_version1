$('select[name=representative_id]').change(function () {
    $(document).find('.error-message').remove();
    $(".entityInput").hide();
    var entity = $(this).val();
    if (entity === '') {
        $("#entityInputs").hide();
    } else {
        $("#entityInputs").show();
        $(".entityInput" + entity).show();
    }
    var companyElement = $('input[name="display_name"]');
    var companyElementParent = companyElement.parent('.form-group');
    if (entity === '1') {
        companyElement.attr('placeholder', 'Individual Name');
        companyElementParent.find('label').html('Individual Name*');
    } else {
        companyElement.attr('placeholder', 'Company Name');
        companyElementParent.find('label').html('Company Name*');
    }
});
$(".incorporationDate").datepicker({
    changeMonth: true,
    changeYear: true,
    maxDate: new Date(),
    dateFormat: 'yy-mm-dd'
});

var Registration = {
    uploadedIcon: '<i class="glyphicon glyphicon-ok fileuploaded"></i>',
    loaderImageUrl: '',
    documentsUrl: '',
    fileUploadUrl: '',
    notApplicableUrl: '',
    redirectUrl: '',
    directorCounter: 0,
    directorCount: 0,
    addMoreElement: '',
    showLoader: function() {
        var documentHeight = $(document).height();
        $('#loaderLayout span').css({ height: documentHeight + 'px' });
        $('#loaderLayout').show();
    },
    hideLoader: function() {
        $('#loaderLayout').hide();
    },
    uploadDocuments: function () {
        var self = this;
        self.getDocuments();
    },
    getDocuments: function () {
        var self = this;
        $.ajax({
            url: self.documentsUrl,
            type: 'POST',
            dataType: 'JSON',
            success: function (response) {
                if (!response.error) {
                    self.getHtml(response.data);
                }
                $('#documentsModal').modal({backdrop: 'static', keyboard: false});
            },
            error: function (result) {

            }
        });
    },
    getHtml: function (data) {
        var self = this;
        var html = '';
        $.each(data, function (index, val) {
            var name = '';
            var uploaded = self.uploadedIcon;
            var disabled = 'disabled="disabled"';
            var required = '';
            var notApplicable = ' <a href="javascript:void(0)" onclick="Registration.markNotApplicable(this)" data-name="' + val.field + '">Not Applicable ?</a> ';
            if (!val.uploaded) {
                name = val.field;
                disabled = '';
                uploaded = '';
            } else {
                notApplicable = '';
            }
            if (val.is_required) {
                required = '*';
                notApplicable = '';
            }
            html += '<div class="form-group col-md-6" id="doc_' + val.field + '">';
            html += '<label>' + val.name + required + notApplicable + uploaded + '</label>';
            html += '<input ' + disabled + ' type="file" name="' + name + '" class="form-control" onchange="Registration.uploadDoc(this);">';
            html += '</div>';
        });
        $(document).find('#documentsList').html(html);
    },
    uploadDoc: function (element) {
        var self = this;
        var loader = self.getloader();
        var name = $(element).attr('name');
        $(element).parent('.form-group').find('label').append(loader);
        $(element).parent('.form-group').find('.error-message').remove();
        var data = new FormData();
        $.each($(element)[0].files, function (n, file) {
            data.append(name, file);
        });
        $(element).attr('disabled', 'disabled');
        $.ajax({
            url: self.fileUploadUrl,
            type: 'POST',
            data: data,
            dataType: 'JSON',
            enctype: 'multipart/form-data',
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                if (!response.error) {
                    var element = $(document).find('#doc_' + response.file);
                    element.find('img').remove();
                    element.find('a').remove();
                    element.find('label').append(self.uploadedIcon);
                } else if (response.error === 2) {
                    swal({
                        title: 'Success!',
                        text: response.data,
                        type: 'success',
                        showCancelButton: false
                    }).then(function () {
                        window.location = self.redirectUrl;
                    }, function () {
                        window.location = self.redirectUrl;
                    });
                } else {
                    var element = $(document).find('#doc_' + response.file);
                    element.find('img').remove();
                    element.find('input').removeAttr('disabled');
                    element.find('input').after('<p class="error-message">' + response.data + '</p>');
                }
            },
            error: function (result) {
                swal("Error!", "Something went wrong on the Server. Please try again.", "error");
            }
        });
    },
    getloader: function () {
        var html = '<img src="' + this.loaderImageUrl + '" alt="Loader">';
        return html;
    },
    markNotApplicable: function (element) {
        var self = this;
        var eventElement = $(element);
        var name = eventElement.attr('data-name');
        swal({
            title: 'Are you sure?',
            text: "You want to mark this document as Not Applicable!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#782669',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then(function () {
            self.showLoader();
            $.ajax({
                url: self.notApplicableUrl,
                type: 'POST',
                dataType: 'JSON',
                data: {name: name},
                success: function (response) {
                    self.hideLoader();
                    if (!response.error) {
                        var element = $(document).find('#doc_' + response.file);
                        element.find('input').attr('disabled', 'disabled');
                        element.find('label').append(self.uploadedIcon);
                        eventElement.remove();
                    } else if (response.error === 2) {
                        swal({
                            title: 'Success!',
                            text: response.data,
                            type: 'success',
                            showCancelButton: false
                        }).then(function () {
                            window.location = self.redirectUrl;
                        }, function () {
                            window.location = self.redirectUrl;
                        });
                    } else {
                        swal("Error!", "Something went wrong on the Server. Please try again.", "error");
                    }
                },
                error: function (result) {
                    self.hideLoader();
                    swal("Error!", "Something went wrong on the Server. Please try again.", "error");
                }
            });
        });
    },
    addMoreDirectors: function(element) {
        var self = this;
        self.addMoreElement = element;
        if(self.directorCount < 3) {
            var html = self.getDirectorHtml();
            $('#directors').append(html);
        }
        self.showAddMoreDirectors();
    },
    getDirectorHtml: function() {
        var self = this;
        var html = '<div class="addional_director"><div class="form-group entityInput entityInput5 col-md-6">';
        html += '<label>Director Name*</label>';
        html += '<input type="text" name="directors_name['+ self.directorCounter +']" placeholder="Director Name" class="form-control"/>';
        html += '</div>';
        html += '<div class="form-group entityInput entityInput5 col-md-6">';
        html += '<label>Din Number*</label>';
        html += '<input type="text" name="dins['+ self.directorCounter +']" placeholder="Din Number" class="form-control"/>';
        html += '</div>';
        html += '<a href="javascript:void(0)" onclick="Registration.removeDirector(this)" style="float: right; margin: 0 -13px 0 0;">X</a>';
        html += '</div>';
        self.directorCounter++;
        self.directorCount++;
        return html;
    },
    removeDirector: function(element) {
        var self = this;
        $(element).parent('.addional_director').remove();
        self.directorCount--;
        self.showAddMoreDirectors();
    },
    showAddMoreDirectors: function() {
        var self = this;
        if(self.directorCount === 3) {
            $(self.addMoreElement).hide();
        } else if(self.directorCount < 3) {
            $(self.addMoreElement).show();
        }
    }
};