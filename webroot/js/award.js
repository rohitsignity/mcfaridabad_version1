var Award = {
    id: '',
    authanticateUrl: '',
    authanticateSuperAdminUrl: '',
    redirectUrl: '',
    showDetail: function (id) {
        var self = this;
        self.id = id;
        self.getDetail();
    },
    getDetail: function () {
        var self = this;
        self.authanticateAdmin();
    },
    authanticateAdmin: function () {
        var self = this;
        swal({
            title: 'Enter your Password',
            input: 'password',
            showCancelButton: true,
            confirmButtonText: 'Validate',
            showLoaderOnConfirm: true,
            inputAttributes: {
                autocomplete: 'off'
            },
            preConfirm: function (password) {
                return new Promise(function (resolve, reject) {
                    if (!$.trim(password)) {
                        reject("Password is required");
                        return false;
                    }
                    $.ajax({
                        url: self.authanticateUrl,
                        type: 'POST',
                        data: {password: password},
                        dataType: 'JSON',
                        success: function (response) {
                            if (response.error) {
                                reject(response.message);
                            } else {
                                resolve();
                            }
                        },
                        error: function (result) {
                            reject('Something went wrong on server. Please try again.');
                        }
                    });
                });
            },
            allowOutsideClick: false
        }).then(function () {
            swal.setDefaults({
                input: 'password',
                confirmButtonText: 'Next &rarr;',
                showCancelButton: true,
                animation: false,
                progressSteps: ['1', '2', '3'],
                showLoaderOnConfirm: true,
                preConfirm: function (password) {
                    return new Promise(function (resolve, reject) {
                        var currentStep = parseInt(swal.getQueueStep()) + 1;
                        if (!$.trim(password)) {
                            reject("Level " + currentStep + " password is required");
                            return false;
                        }
                        $.ajax({
                            url: self.authanticateSuperAdminUrl,
                            type: 'POST',
                            dataType: 'JSON',
                            data: { level: currentStep, password: password },
                            success: function(response) {
                                if(response.error) {
                                    reject(response.message);
                                } else {
                                    if(response.url) {
                                        self.redirectUrl = response.url;
                                        localStorage.setItem("awardMask", response.mask + '_' + self.id);
                                    }
                                    resolve();
                                }
                            },
                            error: function() {
                                reject("Something went wrong on Server. Please try again.");
                            }
                        });
                    });
                }
            });
            var steps = [
                {
                    title: 'Level 1 Password',
                    text: 'Enter level 1 password'
                },
                {
                    title: 'Level 2 Password',
                    text: 'Enter level 2 password'
                },
                {
                    title: 'Level 3 Password',
                    text: 'Enter level 3 password'
                }
            ];
            swal.queue(steps).then(function (result) {
                swal.resetDefaults();
//                window.open(self.redirectUrl, "_blank");
                window.location = self.redirectUrl;
            }, function () {
                swal.resetDefaults();
            });
        });
    }
};