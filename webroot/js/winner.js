var Winner = {
    detailUrl: '',
    mask: '',
    responseUrl: '',
    getWinner: function () {
        var self = this;
        self.mask = localStorage.getItem("awardMask");
        self.getDetail();
    },
    getDetail: function () {
        var self = this;
        $.ajax({
            url: self.detailUrl,
            type: 'POST',
            data: {mask: self.mask},
            dataType: 'JSON',
            success: function (response) {
                $('#loaderLayout').hide();
                if (response.error) {
                    swal({
                        title: response.title,
                        text: response.message,
                        allowOutsideClick: false
                    }).then(
                        function () {
                            $('#loaderLayout').show();
                            window.location = response.url;
                        },
                                function () {
                                }
                    );
                } else {
                    self.responseUrl = response.url;
                    var html = self.getHtml(response.data);
                    $('#winnerBlock').html(html);
                }
            },
            error: function () {
                swal("Error!", "Something went wrong on the server. Please try again.", "error");
            }
        });
    },
    getHtml: function (data) {
        var self = this;
        if(!data.user) {
            swal({
                title: 'No Bid found',
                text: 'No Bid found for this Tender',
                allowOutsideClick: false
            }).then(function(){
                window.location = self.responseUrl;
            });
            return false;
        }
        var html = self.getUserHtml(data.user);
        return html;
    },
    getUserHtml: function (data) {
        var html = '<div class="table-responsive">';
        html += '<table class="table">';
        html += '<tbody>';
        html += '<tr>';
        html += '<th>H1 Bidder Name</th>';
        html += '<td>' + data.display_name + '</td>';
        html += '</tr>';
        html += '<tr>';
        html += '<th>H1 Bidder Email</th>';
        html += '<td>' + data.email + '</td>';
        html += '</tr>';
        html += '<tr>';
        html += '<th>H1 Bidder Mobile</th>';
        html += '<td>' + data.mobile + '</td>';
        html += '</tr>';
//        html += '<tr>';
//        html += '<th>H1 Bidder User Name</th>';
//        html += '<td>' + data.user_name + '</td>';
//        html += '</tr>';
        html += '<tr>';
        html += '<th>Bid Amount</th>';
        html += '<td>₹' + data.amount + '</td>';
        html += '</tr>';
        html += '</tbody>';
        html += '</table>';
        html += '</div>';
        return html;
    }
};