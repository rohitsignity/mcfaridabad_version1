var Shelters = {
    fieldCounter: 0,
    getView: function (element) {
        var self = this;
        self.fieldCounter = 0;
        var shelterType = $.trim((element.val()).toLowerCase());
        if (shelterType) {
            self.getHtml(self, element);
        } else {
            $(document).find('#shelters_type_block').html('');
        }
    },
    getHtml: function (self, element) {
        var html = '<div class="col-md-12">';
        html += '<h3>' + element.val() + '</h3>';
        html += '<table class="table">';
        html += '<thead>';
        html += '<tr>';
        html += '<th>S. No.</th>';
        html += '<th>Type of Asset</th>';
        html += '<th>Location</th>';
        html += '<th>No. Of panels</th>';
        html += '<th>Total Area(in sq m)</th>';
        html += '<th>Latitude</th>';
        html += '<th>Longitude</th>';
        html += '<th></th>';
        html += '</tr>';
        html += '</thead>';
        html += '<tbody>';
        html += '<tr>';
        html += '<td>1</td>';
        html += '<td>' + (element.val()).replace(/.$/, "") + '</td>';
        html += '<td><input type="text" class="form-control shelter_location" name="shelter_location[' + self.fieldCounter + ']"></td>';
        html += '<td><input type="text" class="form-control shelter_panels" name="shelter_panels[' + self.fieldCounter + ']"></td>';
        html += '<td><input type="text" class="form-control shelter_area" name="shelter_area[' + self.fieldCounter + ']"></td>';
        html += '<td><input type="text" class="form-control shelter_lat" name="shelter_lat[' + self.fieldCounter + ']"></td>';
        html += '<td><input type="text" class="form-control shelter_lng" name="shelter_lng[' + self.fieldCounter + ']"></td>';
        html += '<td><a href="javascript:void(0)" class="plus-btn" onclick="Shelters.addMore(this,\'' + (element.val()).replace(/.$/, "") + '\')">+ Add More</a></td>';
        html += '</tr>';
        html += '</tbody>';
        html += '</table>';
        html += '</div>';
        $(document).find('#shelters_type_block').html(html);
    },
    addMore: function (element, type) {
        var self = this;
        self.fieldCounter++;
        var html = '<tr>';
        html += '<td>1</td>';
        html += '<td>' + type + '</td>';
        html += '<td><input type="text" class="form-control shelter_location" name="shelter_location[' + self.fieldCounter + ']"></td>';
        html += '<td><input type="text" class="form-control shelter_panels" name="shelter_panels[' + self.fieldCounter + ']"></td>';
        html += '<td><input type="text" class="form-control shelter_area" name="shelter_area[' + self.fieldCounter + ']"></td>';
        html += '<td><input type="text" class="form-control shelter_lat" name="shelter_lat[' + self.fieldCounter + ']"></td>';
        html += '<td><input type="text" class="form-control shelter_lng" name="shelter_lng[' + self.fieldCounter + ']"></td>';
        if ($(document).find('#shelters_type_block table tbody tr').length < 49) {
            html += '<td><a href="javascript:void(0)" class="plus-btn" onclick="Shelters.addMore(this,\'' + type + '\')">+ Add More</a></td>';
        } else {
            html += '<td><a href="javascript:void(0)" class="close-btn" onclick="Shelters.remove(this,\'' + type + '\')">X</a></td>';
        }
        html += '</tr>';
        $(document).find('#shelters_type_block table tbody').append(html);
        $(element).attr('onclick', 'Shelters.remove(this,\'' + type + '\')');
        $(element).html('X');
        $(element).removeClass('plus-btn');
        $(element).addClass('close-btn');
        self.serialNos();
    },
    remove: function (element, type) {
        $(element).parents('tr').remove();
        if ($(document).find('#shelters_type_block table tbody tr').length < 50) {
            $(document).find('#shelters_type_block table tbody tr:last-child td:last-child').html('<a href="javascript:void(0)" class="plus-btn" onclick="Shelters.addMore(this,\'' + type + '\')">+ Add More</a>');
        }
        this.serialNos();
    },
    serialNos: function () {
        var counter = 1;
        $(document).find('#shelters_type_block table tbody tr').each(function () {
            $(this).find('td').first().html(counter);
            counter++;
        });
    },
    calculatePayble: function () {
        $(document).find('.error-message').remove();
        var concessionFees = $.trim($(document).find('input[name="concession_fees"]').val());
        if (!concessionFees) {
            return false;
        } else if (isNaN(concessionFees)) {
            $(document).find('input[name="concession_fees"]').after('<p class="error-message">Value must be numeric</p>');
            return false;
        }
        var concessionaireType = $.trim($(document).find('select[name="concessionaire_type"] option:selected').val());
        if (!concessionaireType) {
            return false;
        }
        var amount = concessionFees;
        if (concessionaireType !== 'MCG') {
            amount = (parseFloat(concessionFees) * 25) / 100;
        }
        $(document).find('input[name="annual_license_fees"]').val(amount);
    },
    preview: function () {
        if ($('#shelters_type_block').length) {
            var label = $('#shelters_type_block h3').html();
            var html = '<div class="col-md-12">';
            html += '<h3>' + label + '</h3>';
            html += '<table class="table">';
            html += '<thead>';
            html += '<tr>';
            html += '<th>S. No.</th>';
            html += '<th>Type of Asset</th>';
            html += '<th>Location</th>';
            html += '<th>No. Of panels</th>';
            html += '<th>Total Area(in sq m)</th>';
            html += '<th>Latitude</th>';
            html += '<th>Longitude</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';
            var counter = 1;
            $('#shelters_type_block table tbody tr').each(function(){
                html += '<tr>';
                html += '<td>'+ counter +'</td>';
                html += '<td>' + (label).replace(/.$/, "") + '</td>';
                html += '<td>'+ $(this).find('.shelter_location').val() +'</td>';
                html += '<td>'+ $(this).find('.shelter_panels').val() +'</td>';
                html += '<td>'+ $(this).find('.shelter_area').val() +'</td>';
                html += '<td>'+ $(this).find('.shelter_lat').val() +'</td>';
                html += '<td>'+ $(this).find('.shelter_lng').val() +'</td>';
                html += '</tr>';
                counter++;
            });
            html += '</tbody>';
            html += '</table>';
            html += '</div>';
            $(document).find('#shelters_type_preview').html(html);
        }
    }
};
$(document).on('change', 'select[name="concessionaire_type"]', function () {
    if ($(document).find('#shelters_type_block').length) {
        Shelters.calculatePayble();
        $(document).find('input[name="concessionaire_name"]').val('');
        if($(this).val() === 'MCG') {
            $(document).find('input[name="concessionaire_name"]').val('Municipal Corporation Gurugram');
        }
    }
});
$(document).on('keyup', 'input[name="concession_fees"]', function () {
    if ($(document).find('#shelters_type_block').length) {
        Shelters.calculatePayble();
    }
});