var Bids = {
    tenderId: '',
    minVal: '',
    maxVal: '',
    bidIt: function (element) {
        var self = this;
        self.sendBid(element);
    },
    sendBid: function (element) {
        var self = this;
        var data = new FormData();
        data.append('id', self.tenderId);
        $('.docNames').each(function (i) {
            var nameVal = $(this).val();
            data.append('document_name[' + i + ']', nameVal);
        });
        $('.docs').each(function (i) {
            $.each($(this)[0].files, function (n, file) {
                data.append('document_' + i, file);
            });
        });
        if (self.validate(element)) {
            $.ajax({
                url: applyBidUrl,
                data: data,
                dataType: 'JSON',
                type: 'POST',
                enctype: 'multipart/form-data',
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    if (!response.error) {
                        swal('Success!', response.message, 'success');
                    } else {
                        swal('', response.message, 'error');
                    }
                    self.clearFiles(element);
                },
                error: function (result) {
                    swal('Error!', 'Something Went Wrong', 'error');
                }
            });
        }
    },
    validate: function (element) {
        var self = this;
        var validate = true;
        $(document).find('.error-message').remove();
        $(element).find('input[type="file"]').each(function () {
            var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'doc', 'docx', 'xls', 'xlsx', 'pdf'];
            if (!$(this).val()) {
                validate = false;
                var label = $(this).parent('.form-group').find('.docNames').val();
                $(this).after('<div class="error-message text-left pull-left" style="width: 100%;">' + label + ' is Required</div>');
            } else if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) === -1) {
                validate = false;
                $(this).val('');
                $(this).after('<div class="error-message text-left pull-left" style="width: 100%;">Invalid format. Allowed formats are : ' + fileExtension.join(', ') + '</div>');
            }
        });
        return validate;
    },
    clearFiles: function(element) {
        $(element).find('input[type="file"]').each(function () {
            $(this).val('');
        });
    }
};