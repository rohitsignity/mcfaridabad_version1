var Typology = {
    ajaxCatUrl: '',
    ajaxSubCatUrl: '',
    errorRedirectionUrl: '',
    typologyId: '',
    deviceTypeId: '',
    categoryLocalData: {},
    typologoLocalData: {},
    showLoader: function() {
        var docHeight  = $(document).height();
        $('#loaderLayout span').css({ height: docHeight + 'px' });
        $('#loaderLayout').show();
    },
    hideLoader: function() {
        $('#loaderLayout').hide();
    },
    deviceType: function (element) {
        SubCategory.clearFormPanel('formData');
        this.deviceTypeId = $(element).val();
        this.getCategories();
    },
    getCategories: function () {
        var self = this;
        var deviceType = self.deviceTypeId;
        if (self.categoryLocalData[deviceType]) {
            self.categoryHtml(self.categoryLocalData[deviceType]);
        } else {
            $.ajax({
                url: self.ajaxCatUrl,
                data: {deviceType: deviceType},
                dataType: 'JSON',
                type: 'POST',
                success: function (response) {
                    var responseData = [];
                    $.each(response, function (index, val) {
                        var obj = {};
                        obj.id = val.id;
                        obj.title = val.title;
                        responseData.push(obj);
                    });
                    self.categoryLocalData[deviceType] = responseData;
                    self.categoryHtml(self.categoryLocalData[deviceType]);
                },
                error: function (result) {

                }
            });
        }
    },
    typologyData: function (typologyId, selectBoxId) {
        var self = this;
        self.typologyId = typologyId;
            var allowedTypologies = ["1","2","4","5", "9", "10"];
//            if (typologyId && $.inArray(typologyId, allowedTypologies) === -1) {
//                self.subCategoryHtml([], 'sub_category');
//                swal({
//                    title: 'Alert!',
//                    text: 'Please choose from Typology A, B, D, E, I or J',
//                    type: 'info'
//                });
//                return false;
//            }
        if (self.typologoLocalData[typologyId])
        {
            var responseData = self.typologoLocalData[typologyId];
            self.subCategoryHtml(responseData, selectBoxId);
        } else {
            self.showLoader();
            $.ajax({
                url: self.ajaxSubCatUrl,
                type: 'POST',
                dataType: 'JSON',
                data: {id: typologyId},
                success: function (response) {
                    self.hideLoader();
                    var responseData = [];
                    $.each(response, function (index, val) {
                        var obj = {};
                        obj.id = val.id;
                        obj.title = val.title;
                        responseData.push(obj);
                    });
                    self.typologoLocalData[typologyId] = responseData;
                    self.subCategoryHtml(responseData, selectBoxId);
                }, error: function (error) {
                    self.hideLoader();
                    window.location = self.errorRedirectionUrl;
                }
            });
        }

    },
    categoryHtml: function (dataArr) {
        var html = '<option value="">Select Typology</option>';
        $.each(dataArr, function (index, val) {
            html += '<option value="' + val.id + '">' + val.title + '</option>';
        });
        $('#typology').html(html);
    },
    subCategoryHtml: function (dataArr, selectBoxId) {
        var html = '<option value="">Select Subcategory</option>';
        $.each(dataArr, function (index, val) {
            html += '<option value="' + val.id + '">' + val.title + '</option>';
        });
        $('#' + selectBoxId).html(html);
    }
};