var TypologyAOthers = {
    fieldCounter: 0,
    getView: function (element) {
        var self = this;
        var type = $.trim(element.val());
        if (type) {
            self.getHtml(self, type);
        } else {
            $(document).find('#omd_type_a_block').html('');
        }
    },
    getHtml: function (self, type) {
        var html = '<div class="col-md-12">';
        html += '<h3>' + type + '</h3>';
        html += '<table class="table">';
        html += '<thead>';
        html += '<tr>';
        html += '<th>S. No.</th>';
        html += '<th>Type of Asset</th>';
        html += '<th>Location</th>';
        html += '<th>Height (in m)</th>';
        html += '<th>Width (in m)</th>';
        html += '<th>Total Area(in sq m)</th>';
        html += '<th>Latitude</th>';
        html += '<th>Longitude</th>';
        html += '<th></th>';
        html += '</tr>';
        html += '</thead>';
        html += '<tbody>';
        html += '<tr>';
        html += '<td>1</td>';
        html += '<td>' + type + '</td>';
        html += '<td><input type="text" class="form-control other_type_a_location" name="other_type_a_location[' + self.fieldCounter + ']"></td>';
        html += '<td><input type="text" class="form-control other_type_a_height" name="other_type_a_height[' + self.fieldCounter + ']" onkeyup="TypologyAOthers.calculateArea(this)"></td>';
        html += '<td><input type="text" class="form-control other_type_a_width" name="other_type_a_width[' + self.fieldCounter + ']" onkeyup="TypologyAOthers.calculateArea(this)"></td>';
        html += '<td><input type="text" class="form-control other_type_a_area" name="other_type_a_area[' + self.fieldCounter + ']" readonly="readonly"></td>';
        html += '<td><input type="text" class="form-control other_type_a_lat" name="other_type_a_lat[' + self.fieldCounter + ']"></td>';
        html += '<td><input type="text" class="form-control other_type_a_lng" name="other_type_a_lng[' + self.fieldCounter + ']"></td>';
        html += '<td><a href="javascript:void(0)" class="plus-btn" onclick="TypologyAOthers.addMore(this,\'' + type + '\')">+ Add More</a></td>';
        html += '</tr>';
        html += '</tbody>';
        html += '</table>';
        html += '</div>';
        $(document).find('#omd_type_a_block').html(html);
    },
    addMore: function (element, type) {
        var self = this;
        self.fieldCounter++;
        var html = '<tr>';
        html += '<td>1</td>';
        html += '<td>' + type + '</td>';
        html += '<td><input type="text" class="form-control other_type_a_location" name="other_type_a_location[' + self.fieldCounter + ']"></td>';
        html += '<td><input type="text" class="form-control other_type_a_height" name="other_type_a_height[' + self.fieldCounter + ']" onkeyup="TypologyAOthers.calculateArea(this)"></td>';
        html += '<td><input type="text" class="form-control other_type_a_width" name="other_type_a_width[' + self.fieldCounter + ']" onkeyup="TypologyAOthers.calculateArea(this)"></td>';
        html += '<td><input type="text" class="form-control other_type_a_area" name="other_type_a_area[' + self.fieldCounter + ']" readonly="readonly"></td>';
        html += '<td><input type="text" class="form-control other_type_a_lat" name="other_type_a_lat[' + self.fieldCounter + ']"></td>';
        html += '<td><input type="text" class="form-control other_type_a_lng" name="other_type_a_lng[' + self.fieldCounter + ']"></td>';
        if ($(document).find('#omd_type_a_block table tbody tr').length < 49) {
            html += '<td><a href="javascript:void(0)" class="plus-btn" onclick="TypologyAOthers.addMore(this,\'' + type + '\')">+ Add More</a></td>';
        } else {
            html += '<td><a href="javascript:void(0)" class="close-btn" onclick="TypologyAOthers.remove(this,\'' + type + '\')">X</a></td>';
        }
        html += '</tr>';
        $(document).find('#omd_type_a_block table tbody').append(html);
        $(element).attr('onclick', 'TypologyAOthers.remove(this,\'' + type + '\')');
        $(element).html('X');
        $(element).removeClass('plus-btn');
        $(element).addClass('close-btn');
        self.serialNos();
    },
    remove: function (element, type) {
        $(element).parents('tr').remove();
        if ($(document).find('#omd_type_a_block table tbody tr').length < 50) {
            $(document).find('#omd_type_a_block table tbody tr:last-child td:last-child').html('<a href="javascript:void(0)" class="plus-btn" onclick="TypologyAOthers.addMore(this,\'' + type + '\')">+ Add More</a>');
        }
        this.serialNos();
    },
    serialNos: function () {
        var counter = 1;
        $(document).find('#omd_type_a_block table tbody tr').each(function () {
            $(this).find('td').first().html(counter);
            counter++;
        });
    },
    calculateArea: function (element) {
        var parentRow = $(element).parents('tr');
        var height = $.trim(parentRow.find('.other_type_a_height').val());
        var width = $.trim(parentRow.find('.other_type_a_width').val());
        if (height && width && !isNaN(height) && !isNaN(width)) {
            var area = parseFloat(height) * parseFloat(width);
            parentRow.find('.other_type_a_area').val(area);
        } else {
            parentRow.find('.other_type_a_area').val('');
        }
    },
    preview: function () {
        var html = '';
        if ($(document).find('#omd_type_a_block').length) {
            var label = $('#omd_type_a_block h3').html();
            var html = '<div class="col-md-12">';
            html += '<h3>' + label + '</h3>';
            html += '<table class="table">';
            html += '<thead>';
            html += '<tr>';
            html += '<th>S. No.</th>';
            html += '<th>Type of Asset</th>';
            html += '<th>Location</th>';
            html += '<th>Height (in m)</th>';
            html += '<th>Width (in m)</th>';
            html += '<th>Total Area(in sq m)</th>';
            html += '<th>Latitude</th>';
            html += '<th>Longitude</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';
            var conter = 1;
            $(document).find('#omd_type_a_block table tbody tr').each(function () {
                html += '<tr>';
                html += '<td>' + conter + '</td>';
                html += '<td>' + label + '</td>';
                html += '<td>' + $(this).find('.other_type_a_location').val() + '</td>';
                html += '<td>' + $(this).find('.other_type_a_height').val() + '</td>';
                html += '<td>' + $(this).find('.other_type_a_width').val() + '</td>';
                html += '<td>' + $(this).find('.other_type_a_area').val() + '</td>';
                html += '<td>' + $(this).find('.other_type_a_lat').val() + '</td>';
                html += '<td>' + $(this).find('.other_type_a_lng').val() + '</td>';
                html += '</tr>';
                conter++;
            });
            html += '</tbody>';
            html += '</table>';
            html += '</div>';
        }
        $(document).find('#omd_type_a_preview').html(html);
    },
    calculatePayble: function () {
        $(document).find('.error-message').remove();
        var concessionFees = $.trim($(document).find('input[name="concession_fees"]').val());
        if (!concessionFees) {
            return false;
        } else if (isNaN(concessionFees)) {
            $(document).find('input[name="concession_fees"]').after('<p class="error-message">Value must be numeric</p>');
            return false;
        }
        var concessionaireType = $.trim($(document).find('select[name="concessionaire_type"] option:selected').val());
        if (!concessionaireType) {
            return false;
        }
        var amount = concessionFees;
        if (concessionaireType !== 'MCG') {
            amount = (parseFloat(concessionFees) * 25) / 100;
        }
        $(document).find('input[name="annual_license_fees"]').val(amount);
    }
};
$(document).on('change', 'select[name="omd_type_type_a"]', function () {
    TypologyAOthers.getView($(this));
});

$(document).on('change', 'select[name="concessionaire_type"]', function () {
    if ($(document).find('#omd_type_a_block').length) {
        TypologyAOthers.calculatePayble();
        $(document).find('input[name="concessionaire_name"]').val('');
        if($(this).val() === 'MCG') {
            $(document).find('input[name="concessionaire_name"]').val('Municipal Corporation Gurugram');
        }
    }
});
$(document).on('keyup', 'input[name="concession_fees"]', function () {
    if ($(document).find('#omd_type_a_block').length) {
        TypologyAOthers.calculatePayble();
    }
});