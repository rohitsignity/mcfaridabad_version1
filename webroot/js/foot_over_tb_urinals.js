var FootOverBridges = {
    fieldCounter: 0,
    getView: function(element) {
        var self = this;
        self.fieldCounter = 0;
        var type = $.trim(element.val());
        if(type) {
            self.getHtml(self,type);
        } else {
            $(document).find('#foot_over_type_block').html('');
        }
    },
    getHtml: function(self,type) {
        var html = '<div class="col-md-12">';
        html += '<h3>' + type + '</h3>';
        html += '<table class="table">';
        html += '<thead>';
        html += '<tr>';
        html += '<th>S. No.</th>';
        html += '<th>Type of Asset</th>';
        html += '<th>Location</th>';
        html += '<th>No. Of panels</th>';
        html += '<th>Total Area(in sq m)</th>';
        html += '<th>Latitude</th>';
        html += '<th>Longitude</th>';
        html += '<th></th>';
        html += '</tr>';
        html += '</thead>';
        html += '<tbody>';
        html += '<tr>';
        html += '<td>1</td>';
        html += '<td>' + type.replace(/.$/, "") + '</td>';
        html += '<td><input type="text" class="form-control foot_over_location" name="foot_over_location[' + self.fieldCounter + ']"></td>';
        html += '<td><input type="text" class="form-control foot_over_panels" name="foot_over_panels[' + self.fieldCounter + ']"></td>';
        html += '<td><input type="text" class="form-control foot_over_area" name="foot_over_area[' + self.fieldCounter + ']"></td>';
        html += '<td><input type="text" class="form-control foot_over_lat" name="foot_over_lat[' + self.fieldCounter + ']"></td>';
        html += '<td><input type="text" class="form-control foot_over_lng" name="foot_over_lng[' + self.fieldCounter + ']"></td>';
        html += '<td><a href="javascript:void(0)" class="plus-btn" onclick="FootOverBridges.addMore(this,\'' + type + '\')">+ Add More</a></td>';
        html += '</tr>';
        html += '</tbody>';
        html += '</table>';
        html += '</div>';
        $(document).find('#foot_over_type_block').html(html);
    },
    addMore: function(element,type) {
        var self = this;
        self.fieldCounter++;
        var html = '<tr>';
        html += '<td>1</td>';
        html += '<td>' + type.replace(/.$/, "") + '</td>';
        html += '<td><input type="text" class="form-control foot_over_location" name="foot_over_location[' + self.fieldCounter + ']"></td>';
        html += '<td><input type="text" class="form-control foot_over_panels" name="foot_over_panels[' + self.fieldCounter + ']"></td>';
        html += '<td><input type="text" class="form-control foot_over_area" name="foot_over_area[' + self.fieldCounter + ']"></td>';
        html += '<td><input type="text" class="form-control foot_over_lat" name="foot_over_lat[' + self.fieldCounter + ']"></td>';
        html += '<td><input type="text" class="form-control foot_over_lng" name="foot_over_lng[' + self.fieldCounter + ']"></td>';
        if ($(document).find('#foot_over_type_block table tbody tr').length < 49) {
            html += '<td><a href="javascript:void(0)" class="plus-btn" onclick="FootOverBridges.addMore(this,\'' + type + '\')">+ Add More</a></td>';
        } else {
            html += '<td><a href="javascript:void(0)" class="close-btn" onclick="FootOverBridges.remove(this,\'' + type + '\')">X</a></td>';
        }
        html += '</tr>';
        $(document).find('#foot_over_type_block table tbody').append(html);
        $(element).attr('onclick', 'FootOverBridges.remove(this,\'' + type + '\')');
        $(element).html('X');
        $(element).removeClass('plus-btn');
        $(element).addClass('close-btn');
        self.serialNos();
    },
    remove: function(element,type) {
        $(element).parents('tr').remove();
        if ($(document).find('#foot_over_type_block table tbody tr').length < 50) {
            $(document).find('#foot_over_type_block table tbody tr:last-child td:last-child').html('<a href="javascript:void(0)" class="plus-btn" onclick="FootOverBridges.addMore(this,\'' + type + '\')">+ Add More</a>');
        }
        this.serialNos();
    },
    serialNos: function () {
        var counter = 1;
        $(document).find('#foot_over_type_block table tbody tr').each(function () {
            $(this).find('td').first().html(counter);
            counter++;
        });
    },
    preview: function() {
        var html = '';
        if($(document).find('#foot_over_type_block').length) {
            var label = $('#foot_over_type_block h3').html();
            var html = '<div class="col-md-12">';
            html += '<h3>' + label + '</h3>';
            html += '<table class="table">';
            html += '<thead>';
            html += '<tr>';
            html += '<th>S. No.</th>';
            html += '<th>Type of Asset</th>';
            html += '<th>Location</th>';
            html += '<th>No. Of panels</th>';
            html += '<th>Total Area(in sq m)</th>';
            html += '<th>Latitude</th>';
            html += '<th>Longitude</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';
            var conter = 1;
            $(document).find('#foot_over_type_block table tbody tr').each(function(){
                html += '<tr>';
                html += '<td>'+ conter +'</td>';
                html += '<td>' + label.replace(/.$/, "") + '</td>';
                html += '<td>'+ $(this).find('.foot_over_location').val() +'</td>';
                html += '<td>'+ $(this).find('.foot_over_panels').val() +'</td>';
                html += '<td>'+ $(this).find('.foot_over_area').val() +'</td>';
                html += '<td>'+ $(this).find('.foot_over_lat').val() +'</td>';
                html += '<td>'+ $(this).find('.foot_over_lng').val() +'</td>';
                html += '</tr>';
                conter++;
            });
            html += '</tbody>';
            html += '</table>';
            html += '</div>';
        }
        $(document).find('#foot_over_type_preview').html(html);
    }
};