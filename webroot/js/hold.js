var Hold = {
    fileUploadUrl: '',
    loaderImageUrl: '',
    getloader: function () {
        var html = '<img src="' + this.loaderImageUrl + '" alt="Loader">';
        return html;
    },
    uploadDoc: function (element) {
        var self = this;
        var loader = self.getloader();
        var name = $(element).attr('name');
        $(element).parent('.form-group').find('label').append(loader);
        $(element).parent('.form-group').find('.error-message').remove();
        var data = new FormData();
        $.each($(element)[0].files, function (n, file) {
            data.append(name, file);
        });
        $(element).attr('disabled', 'disabled');
        $.ajax({
            url: self.fileUploadUrl,
            type: 'POST',
            data: data,
            dataType: 'JSON',
            enctype: 'multipart/form-data',
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                if (!response.error) {
                    var element = $(document).find('#doc_' + response.file);
                    element.find('img').remove();
                    element.find('a').remove();
                    element.find('label').append(self.uploadedIcon);
                } else if (response.error === 2) {
                    swal({
                        title: 'Success!',
                        text: response.data,
                        type: 'success',
                        showCancelButton: false
                    }).then(function () {
                        location.reload();
                    }, function () {
                        location.reload();
                    });
                } else {
                    var element = $(document).find('#doc_' + response.file);
                    element.find('img').remove();
                    element.find('input').removeAttr('disabled');
                    element.find('input').after('<p class="error-message">' + response.data + '</p>');
                }
            },
            error: function (result) {
                swal("Error!", "Something went wrong on the Server. Please try again.", "error");
            }
        });
    }
};