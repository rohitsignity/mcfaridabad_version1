var AdditionalDocuments = {
    docCounter: 0,
    url: '',
    validateUrl: '',
    additional_documenturl: '',
    loaderImageUrl: '',
    showLoader: function() {
        var docHeight = $(document).height();
        $('#loaderLayout span').css({ height: docHeight + 'px' });
        $('#loaderLayout').show();
    },
    hideLoader: function() {
        $('#loaderLayout').hide();
    },
    requestRegisterDocuments: function () {
        var self = this;
        self.showLoader();
        var html = '<form action="javascript:void(0)" onsubmit="AdditionalDocuments.saveDocRequest(this)">';
        html += '<div class="col-md-11">';
        html += '<input type="text" readonly="readonly" name="last_date" class="form-control lastDatePic" placeholder="Last Date of Document Submission">';
        html += '</div>';
        html += '<div class="col-md-11 requested-doc-con">';
        html += '<span style="float: left; width: 49%;"><input type="text" class="form-control requested-doc" placeholder="Document Name" name="document['+ this.docCounter +']" style="float: left;"></span>';
        html += '<span style="float: right; width: 49%;"><input type="text" class="form-control requested-doc-reason" placeholder="Reason" name="reason[' + this.docCounter + ']" style="float: right;"></span>';
        html += '</div>';
        html += '<a href="javascript:void(0)" style="float: left; margin: 0 0 0 18px;" onclick="AdditionalDocuments.addMoreDocs(this)">Add More Document</a>';
        html += '<div class="col-md-11">';
        html += '<input type="submit" class="btn btn-default approvd commentSubmit" style="margin: 13px 0 0 0" value="Notify">';
        html += '</div>';
        html += '</form>';
        $.ajax({
            url: self.validateUrl,
            type: 'GET',
            dataType: 'JSON',
            success: function(response) {
                self.hideLoader();
                if(!response.error) {
                    $('#documentsList').html(html);
                    $(".lastDatePic").datepicker({
                        changeMonth: true,
                        changeYear: true,
                        minDate: new Date(),
                        dateFormat: 'yy-mm-dd'
                    });
                    $('#documentsModal').modal({backdrop: 'static', keyboard: false});
                } else {
                    swal("Alert!",response.message,"info");
                }
            },
            error: function() {
                self.hideLoader();
                swal("Error!","Something Went wrong on Server. Please try agian.","error");
            }
        });
    },
    addMoreDocs: function (element) {
        this.docCounter++;
        var html = '<div class="col-md-11 requested-doc-con">';
        html += '<span style="float: left; width: 49%;"><input type="text" class="form-control requested-doc" placeholder="Document Name" name="document['+  this.docCounter +']" style="float: left;"></span>';
        html += '<a href="javascript:void(0)" style="float: right; margin: 16px -17px 0 0" onclick="AdditionalDocuments.removeDoc(this)">X</a>';
        html += '<span style="float: right; width: 49%;"><input type="text" class="form-control requested-doc-reason" placeholder="Reason" name="reason['+  this.docCounter +']" style="float: right;"></span>';
        html += '</div>';
        $(element).before(html);
    },
    removeDoc: function (element) {
        $(element).parent('div').remove();
    },
    saveDocRequest: function (element) {
        var error = 0;
        var self = this;
        $(document).find('.error-message').remove();
        var lastDatePic = $.trim($(document).find('.lastDatePic').val());
        if(!lastDatePic) {
            error = 1;
            $(document).find('.lastDatePic').after('<p class="error-message">Please Last Date of Document Submission</p>');
        }
        $(element).find('.requested-doc-con').each(function () {
            var docElement = $(this).find('.requested-doc');
            var reasonElement = $(this).find('.requested-doc-reason');
            var doc = $.trim(docElement.val());
            var reason = $.trim(reasonElement.val());
            if (!doc) {
                error = 1;
                docElement.after('<p class="error-message">Please enter document Name</p>');
            }
            if(!reason) {
                error = 1;
                reasonElement.after('<p class="error-message">Please enter reason</p>');
            }
        });
        if (!error) {
            self.showLoader();
            $.ajax({
                url: self.url,
                type: 'POST',
                data: $(element).serialize(),
                dataType: 'JSON',
                success: function(response) {
                    $("#documentsModal").modal('hide');
                    self.hideLoader();
                    if(!response.error) {
                        swal("Success!","Request has been sent successfully.","success");
                    } else {
                        swal("Alert!",response.message,"info");
                    }
                },
                error: function(result) {
                    self.hideLoader();
                    $("#documentsModal").modal('hide');
                    swal("Error!","Something Went wrong on Server. Please try agian.","error");
                }
            });
        }
    },
    uploadDeviceDocuments: function (id) {
        var self = this;
        $.ajax({
            url: self.additional_documenturl,
            type: 'POST',
            data: {id: id},
            async: true,
            cache: false,
            dataType: 'JSON',
            success: function (response) {
                var html = '';
                    $.each(response,function(key,val){
                        if(val.uploaded == '1'){
                            html+='<div id="" class="form-group col-md-6"><label>'+val.doc_name+'</label><input type="file" disabled="disabled" class="form-control"><span><span>'+val.reason+'</span></div>';
                        }else{
                            html+='<div id="doc_'+ val.id +'" class="form-group col-md-6"><label>'+val.doc_name+'</label><input type="file" onchange="AdditionalDocuments.uploadDoc(this,'+val.id+');" name="'+val.id+'" class="form-control"><span>'+val.reason+'</span></div>';
                        }
                    });
                    $('#documentsList').html(html);
                    $('#documentsModal').modal({backdrop: 'static', keyboard: false});
            },
            error: function (result) {
                swal("Error!", "Something went wrong on the Server. Please try again.", "error");
            }
        });
    },
    uploadDoc: function (element) {
        var self = this;
        var loader = self.getloader();
        var name = $(element).attr('name');
        $(element).parent('.form-group').find('label').append(loader);
        $(element).parent('.form-group').find('.error-message').remove();
        var data = new FormData();
        $.each($(element)[0].files, function (n, file) {
            data.append(name, file);
        });
        $(element).attr('disabled', 'disabled');
        $.ajax({
            url: self.fileUploadUrl,
            type: 'POST',
            data: data,
            dataType: 'JSON',
            enctype: 'multipart/form-data',
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                if (!response.error) {
                    var element = $(document).find('#doc_' + response.file);
                    element.find('img').remove();
                    element.find('a').remove();
                    element.find('label').append(self.uploadedIcon);
                } else {
                    var element = $(document).find('#doc_' + response.file);
                    element.find('img').remove();
                    element.find('input').removeAttr('disabled');
                    element.find('input').after('<p class="error-message">' + response.data + '</p>');
                }
            },
            error: function (result) {
                swal("Error!", "Something went wrong on the Server. Please try again.", "error");
            }
        });
    },
    getloader: function () {
        var html = '<img src="' + this.loaderImageUrl + '" alt="Loader">';
        return html;
    }
};