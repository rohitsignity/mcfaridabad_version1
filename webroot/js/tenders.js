
var Tender = {
    tagsElement: '',
    tagsElementSelect: '',
    deviceId: '',
    segmentid: '',
    experienceid: '',
    docCounter: 1,
    validateUrl: '',
    addDocUrl: '',
    userDetailUrl: '',
    showLoader: function () {
        var docHeight = $(document).height();
        $('#loaderLayout span').css({height: docHeight + 'px'});
        $('#loaderLayout').show();
    },
    hideLoader: function () {
        $('#loaderLayout').hide();
    },
    clearDeviceInfo: function () {
        $(document).find('.deviceInfoBlock').remove();
    },
    getVendors: function () {
        var self = this;
        self.segmentVendors();
    },
    addOtions: function (response) {
        var html = '';
        $.each(response, function (index, val) {
            html += '<option value="' + val.id + '">' + val.text + '</option>';
        });
        this.tagsElement.html(html);
    },
    segmentVendors: function () {
        var self = this;
        if (self.segmentid && self.experienceid) {
            $.ajax({
                url: segmentVendorUrl,
                type: 'POST',
                dataType: 'JSON',
                data: {segmentId: self.segmentid, experienceId: self.experienceid},
                beforeSend: function (xhr) {
                    self.showLoader();
                },
                success: function (response) {
                    self.addOtions(response.results);
                    var vendorIds = $.map(response.results, function (n) {
                        return n.id;
                    });
                    self.tagsElementSelect.val(vendorIds).trigger('change');
                    self.hideLoader();
                    if (!(response.results).length) {
                        swal("No eligible users found!", "Please add users manually.", "info");
                    }
                },
                error: function (xhr, status, error) {
                    self.hideLoader();
                    alert(xhr.status);
                }
            });
        } else {
            self.tagsElement.html('');
            self.tagsElementSelect.val(null).trigger('change');
        }
    },
    getDeviceInfo: function () {
        var self = this;
        self.getDevice();
    },
    getDevice: function () {
        var self = this;
        if (self.deviceId) {
            $.ajax({
                url: deviceInfoUrl,
                type: 'POST',
                data: {deviceId: self.deviceId},
                dataType: 'JSON',
                success: function (response) {
                    var deviveHtml = self.getDeviceHtml(response);
                    self.clearDeviceInfo();
                    $('#deviceinfo').after(deviveHtml);
                },
                error: function (result) {
                    alert(result.status);
                }
            });
        } else {
            self.clearDeviceInfo();
        }
    },
    getDeviceHtml: function (data) {
        var html = '<div class="col-md-6 deviceInfoBlock">';
        html += '<div class="form-group">';
        html += '<label for="contact">Typology</label>';
        html += '<p>' + data.typology + '</p>';
        html += '</div>';
        html += '</div>';
        html += '<div class="col-md-6 deviceInfoBlock">';
        html += '<div class="form-group">';
        html += '<label for="contact">Sub Category</label>';
        html += '<p>' + data.sub_category + '</p>';
        html += '</div>';
        html += '</div>';
        $.each(data.fields, function (index, val) {
            var field = $.grep(data.fieldsData, function (a) {
                return a.field_key === val.field;
            });
            html += '<div class="col-md-6 deviceInfoBlock">';
            html += '<div class="form-group">';
            html += '<label for="contact">' + field[0].lable + '</label>';
            if (field[0].type !== 'file') {
                html += '<p>' + val.value + '</p>';
            } else {
                html += '<p><a href="' + devicePath + val.value + '" download="' + field[0].lable + '">Download</a></p>';
            }
            html += '</div>';
            html += '</div>';
        });
        return html;
    },
    addDocument: function () {
        var html = this.documentHtml();
        $('#deviceinfo').before(html);
    },
    documentHtml: function () {
        var html = '<div class="col-md-6 required-document">';
        html += '<div class="form-group">';
        html += '<label>Document Name ' + this.docCounter + '*</label>';
        html += '<input type="text" name="doc_names[' + (this.docCounter - 1) + ']" class="form-control" placeholder="Document Name ' + this.docCounter + '*">';
        html += '<a href="javascript:void(0)" onclick="Tender.deleteDoc(this)">X</a>';
        html += '</div>';
        html += '</div>';
        this.docCounter++;
        return html;
    },
    deleteDoc: function (element) {
        $(element).parent('.form-group').parent('.col-md-6').remove();
        this.resetDocCounter();
    },
    resetDocCounter: function () {
        var self = this;
        self.docCounter = 1;
        $(document).find('.required-document').each(function () {
            $(this).find('.form-group label').html('Document Name ' + self.docCounter + '*');
            $(this).find('.form-group input').attr('placeholder', 'Document Name ' + self.docCounter);
            $(this).find('.form-group input').attr('name', 'doc_names[' + (self.docCounter - 1) + ']');
            self.docCounter++;
        });
    },
    fetchDevice: function () {
        var self = this;
        var html = '';
        $('#deviceData option:selected').each(function(){
            html += '<p><a href="javascript:void(0)" onclick="Tender.showDeviceDetail('+$(this).val()+')">'+$(this).text()+'</a></p>';
        });
        swal({
            title: 'Selected Devices',
            type: 'info',
            html: html,
            showCloseButton: true,
            showCancelButton: false,
            animation: false
        });
    },
    showDeviceDetail: function(id) {
        var self = this;
        self.showLoader();
        $.ajax({
            url: deviceInfoUrl,
            type: 'POST',
            data: {deviceId: id},
            dataType: 'JSON',
            success: function (response) {
                $('#loaderLayout').hide();
                var deviveHtml = self.getDeviceHtml(response);
                swal({
                    title: 'Device Detail',
                    showCancelButton: true,
                    html: deviveHtml,
                    cancelButtonText: 'close',
                    confirmButtonText: 'Back',
                    animation: false
                }).then(
                    function () {
                        self.fetchDevice();
                    }
                );
            },
            error: function (result) {
                $('#loaderLayout').hide();
                swal("Error!", "Something went wrong on Server, Please try again.", "error");
            }
        });
    },
    getSubCategories: function (element) {
        $(document).find('#addNewDevice').hide();
        var self = this;
        var id = $(element).val();
        $.ajax({
            url: subCatUrl,
            type: 'POST',
            data: {id: id},
            dataType: 'JSON',
            beforeSend: function (xhr) {
                self.showLoader();
            },
            success: function (response) {
                var html = self.getSubCategoryHtml(response);
                $('#subCategories').html(html);
                $('#fetchDeviceDetail').hide();
                $('#deviceData').html('');
                self.hideLoader();
            },
            error: function (result) {
                self.hideLoader();
                swal("Error!", "Something Went wrong on Server. Please try again.", "error");
            }
        });
    },
    getSubCategoryHtml: function (data) {
        var html = '<option value="">Select Sub Category</option>';
        $.each(data, function (index, val) {
            html += '<option value="' + val.id + '">' + val.title + '</option>';
        });
        return html;
    },
    getDevices: function (element) {
        $(document).find('#addNewDevice').show();
        $(document).find('#fetchDeviceDetail').hide();
        var self = this;
        var id = $(element).val();
        $.ajax({
            url: getDeviceUrl,
            type: 'POST',
            data: {id: id},
            dataType: 'JSON',
            beforeSend: function (xhr) {
                self.showLoader();
            },
            success: function (response) {
                self.getDeviceData(response);
                self.hideLoader();
            },
            error: function (result) {
                self.hideLoader();
                swal("Error!", "Something went wrong on Server. Please try again.", "error");
            }
        });
    },
    getDeviceData: function (data) {
        var html = '';
        if (!data.length) {
            swal({
                title: 'No Device Found',
                text: "Do you want to add new device ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Add New Device',
                cancelButtonText: 'Cancel',
                allowOutsideClick: false
            }).then(function () {
                window.location = addDeviceUrl;
            }, function (dismiss) {
                if (dismiss === 'cancel') {
                    $('#fetchDeviceDetail').hide();
                    $('#deviceData').html(html);
                    return false;
                }
            });
        }
        $.each(data, function (index, val) {
            html += '<option value="' + val.id + '">' + val.text + '</option>';
        });
        $('#deviceData').html(html);
        $('.searchDevices').val(null).trigger('change.select2');
    },
    getMinimumGurantee: function (element) {
        var tenderValue = $(element).val();
        if ($.isNumeric(tenderValue)) {
            $('#min-amount').val(tenderValue);
        }
    },
    preview: function () {
        var self = this;
        self.validateTender();
    },
    loadPreview: function () {
        var html = '<table class="table">';
        var data = $('#tenderForm').serializeArray();
        var file = $("#bid-doc").val().replace(/C:\\fakepath\\/i, '');
        var usersListed = 0;
        var docsListed = 0;
        var deviceListed = 0;
        data.splice(13, 0, {name: 'bid_doc', value: file});
        var label = '';
        $.each(data, function (index, val) {
            var element = $('[name="' + val.name + '"]');
            var parent = element.parent('.form-group');
            var elementType = element.prop('tagName').toLowerCase();
            if (elementType === 'input') {
                var inputType = element.attr('type');
                if (inputType === 'checkbox') {
                    if (docsListed === 0) {
                        var checkBoxVal = [];
                        label = element.parent('label').parent('div').parent('.form-group').find('label:first').text();
                        element.each(function () {
                            if($(this).is(':checked')) {
                                checkBoxVal.push($(this).parent('label').text());
                            }
                        });
                        html += '<tr>';
                        html += '<td>' + label + '</td>';
                        html += '<td>' + checkBoxVal.join(', ') + '</td>';
                        html += '</tr>';
                        docsListed = 1;
                    }
                } else if (inputType !== 'hidden') {
                    label = parent.find('label').text();
                    html += '<tr>';
                    html += '<td>' + label + '</td>';
                    html += '<td>' + element.val() + '</td>';
                    html += '</tr>';
                }
            } else if (elementType === 'select' && val.name !== 'users[]' && val.name !== 'device_ids[]') {
                label = parent.find('label').text();
                html += '<tr>';
                html += '<td>' + label + '</td>';
                html += '<td>' + element.find('option:selected').text() + '</td>';
                html += '</tr>';
            } else if (elementType === 'select' && val.name === 'users[]') {
                if (usersListed === 0) {
                    var selectedUsers = [];
                    element.find('option:selected').each(function(){
                        selectedUsers.push($(this).text());
                    });
                    label = parent.find('label').text();
                    html += '<tr>';
                    html += '<td>' + label + '</td>';
                    html += '<td>' + selectedUsers.join(', ') + '</td>';
                    html += '</tr>';
                    usersListed = 1;
                }
            } else if(elementType === 'select' && val.name === 'device_ids[]') {
                if (deviceListed === 0) {
                    var selectedUsers = [];
                    element.find('option:selected').each(function(){
                        selectedUsers.push($(this).text());
                    });
                    label = parent.find('label').text();
                    html += '<tr>';
                    html += '<td>' + label + '</td>';
                    html += '<td>' + selectedUsers.join(', ') + '</td>';
                    html += '</tr>';
                    deviceListed = 1;
                }
            } else {
                label = parent.find('label').text();
                html += '<tr>';
                html += '<td>' + label + '</td>';
                html += '<td>' + element.val() + '</td>';
                html += '</tr>';
            }
        });
        html += '</table>';
        $('#tenderPreviewBlock div').html(html);
        $('#tenderBlock, #tenderPreviewBlock').toggle();
    },
    validateTender: function () {
        var self = this;
        self.showLoader();
        $(document).find('.error-message').remove();
        var data = $('#tenderForm').serializeArray();
        var file = $("#bid-doc").val();
        data.push({name: 'bid_doc', value: file});
        $.ajax({
            url: self.validateUrl,
            data: $.param(data),
            type: 'POST',
            dataType: 'JSON',
            success: function (response) {
                self.hideLoader();
                if (response.error) {
                    $.each(response.data, function (fieldName, messages) {
                        $.each(messages, function (index, val) {
                            var name = '[name="' + fieldName + '"]';
                            if (fieldName === 'users') {
                                name = '[name="' + fieldName + '[]"]';
                            } else if(fieldName === 'device_ids') {
                                name = '[name="' + fieldName + '[]"]';
                            }
                            var element = $('#tenderForm ' + name);
                            element.parent('.form-group').append('<div class="error-message">' + val + '</div>');
                        });
                    });
                    var errorOffset = parseFloat($(document).find('.error-message').first().offset().top) - 80;
                    $("html, body").animate({scrollTop: errorOffset + 'px'}, 500);
                } else {
                    self.loadPreview();
                }
            },
            error: function () {
                self.hideLoader();
                swal("Error", "Something went wrong on Server. Please try again.", "error");
            }
        });
    },
    edit: function () {
        $('#tenderBlock, #tenderPreviewBlock').toggle();
    },
    addDocumentName: function() {
            var self = this;
            swal({
                title: 'Add New document',
                input: 'text',
                showCancelButton: true,
                confirmButtonText: 'Save',
                showLoaderOnConfirm: true,
                preConfirm: function (doc) {
                  return new Promise(function (resolve, reject) {
                      if(!$.trim(doc)) {
                          reject("Document name is required");
                          return false;
                      }
                      $.ajax({
                          url: self.addDocUrl,
                          type: 'POST',
                          dataType: 'JSON',
                          data: { name: doc },
                          success: function(response) {
                              if(response.error) {
                                  reject(response.data.name.unique);
                              } else {
                                  self.docHtml(response.data);
                                  resolve();
                              }
                          },
                          error: function() {
                              
                          }
                      });
                  });
                },
                allowOutsideClick: false
            }).then(function (doc) {
              swal({
                type: 'success',
                title: 'Document Added!',
                html: 'Document "' + doc + '" has been successfully added'
              });
            });
    },
    docHtml: function(data) {
        var html = '<div class="pull-left docCheck">';
        html += '<label for="document-'+data.id+'">';
        html += '<input name="document[]" value="'+data.id+'" id="document-'+data.id+'" type="checkbox">'+data.name+'</label>';
        html += '</div>';
        $('#tenderForm .requiredDocs').append(html);
    },
    vendorDetail: function(id) {
        var self = this;
        self.showLoader();
        $.ajax({
            url: self.userDetailUrl ,
            data: { user_id: id },
            type: 'POST',
            dataType: 'JSON',
            success: function(response) {
                self.hideLoader();
                if(!response.error) {
                    swal({
                        title: 'Vendor Detail',
                        type: 'info',
                        html: '<p><b>Vendor Name:</b> '+ response.data.display_name +'</p>'+
                              '<p><b>Email:</b> '+ response.data.email +'</p>'+
                              '<p><b>Moile:</b> '+ response.data.mobile +'</p>'+
                              '<p><b>Company Name:</b> '+ response.data.company_name +'</p>'+
                              '<p><b>SMC Party Code:</b> '+ response.data.SMC_party_code +'</p>'+
                              '<p><b>Incorporation Date:</b> '+ response.data.incorporation_date +'</p>'+
                              '<p><b>Annual Turnover:</b> '+ response.data.annual_turnover +'</p>'+
                              '<p><b>Experience:</b> '+ response.data.experience +' year(s)</p>'+
                              '<p><b>Income Proof:</b> <a href="'+response.data.income_proof+'" download="income_proof.'+response.data.income_proof_ext+'">download</a></p>'+
                              '<p><b>Turnover Certificate:</b> <a href="'+response.data.turnover_certificate+'" download="turnover_certificate.'+response.data.turnover_certificate_ext+'">download</a></p>'+
                              '<p><b>Experience Certificate:</b> <a href="'+response.data.exp_certificate+'" download="experience_certificate.'+response.data.exp_certificate_ext+'">download</a></p>',
                        showCloseButton: true,
                        showCancelButton: false
                    });
                } else {
                    swal("Error!","Something went wrong on server. Please try again.","error");
                }
            },
            error: function(result) {
                self.hideLoader();
                swal("Error!","Something went wrong on server. Please try again.","error");
            }
        });
    }
};
Tender.tagsElement = $(".tagsData");
Tender.tagsElementSelect = Tender.tagsElement.select2({
    minimumInputLength: 1,
    tags: true,
    placeholder: "Eligible users",
    createTag: function (params) {
        return undefined;
    },
    ajax: {
        url: segmentVendorUrl,
        dataType: 'JSON',
        type: "POST",
        data: function (term) {
            return term;
        },
        results: function (data) {
            return data;
        }
    },
});


$('.searchDevices').select2({
//    minimumInputLength: 1,
//    tags: true,
    allowClear: true,
    placeholder: "Search Devices",
    createTag: function (params) {
        return undefined;
    },
//    ajax: {
//        url: getDeviceUrl,
//        dataType: 'JSON',
//        type: "POST",
//        data: function (term) {
//            return term;
//        },
//        results: function (data) {
//            return data;
//        }
//    }
});

$('.searchDevices').on("select2:select", function (e) {
    $('#fetchDeviceDetail').show();
    $(document).find('#addNewDevice').hide();
});
$('.searchDevices').on("select2:unselect", function (e) {
    if(!$('#deviceData option:selected').length) {
        $('#fetchDeviceDetail').hide();
        $(document).find('#addNewDevice').show();
    }
});
$(".startDate").datetimepicker({
    changeMonth: true,
    changeYear: true,
    minDate: new Date(),
    dateFormat: 'yy-mm-dd',
    onSelect: function (dateText, inst) {
        $(".endDate").datetimepicker("option", "minDate", new Date(dateText));
    }
});
$(".applicationDate").datepicker({
    changeMonth: true,
    changeYear: true,
    minDate: new Date(),
    dateFormat: 'yy-mm-dd'
});
$(".endDate").datetimepicker({
    changeMonth: true,
    changeYear: true,
    minDate: new Date(),
    dateFormat: 'yy-mm-dd',
    timeFormat: "HH:mm"
});
$(".docstartDate").datepicker({
    changeMonth: true,
    changeYear: true,
    minDate: new Date(),
    dateFormat: 'yy-mm-dd'
});
$('#userSegment, #userExperience').on('change', function () {
    Tender.segmentid = $('#userSegment option:selected').val();
    Tender.experienceid = $('#userExperience option:selected').val();
    Tender.getVendors();
});
$('#deviceData').on('change', function () {
    Tender.deviceId = $(this).val();
    //Tender.getDeviceInfo();
});