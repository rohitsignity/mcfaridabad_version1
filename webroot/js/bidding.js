var Bidding = {
    id: '',
    bidsUrl: '',
    addBidUrl: '',
    nextValue: 0,
    loaderHide: 0,
    showLoader: function () {
        var docHeight = $(document).height();
        $('#loaderLayout span').css({height: docHeight + 'px'});
        $('#loaderLayout').show();
    },
    hideLoader: function () {
        $('#loaderLayout').hide();
    },
    start: function () {
        var self = this;
        $.ajax({
            url: self.bidsUrl,
            type: 'POST',
            dataType: 'JSON',
            data: {id: self.id},
            success: function (response) {
                if ($.isNumeric(response.lastestBid)) {
                    $('#latestBid').html('₹' + (response.lastestBid).format(2));
                } else {
                    $('#latestBid').html(response.lastestBid);
                }
                $('#expirationTime').html(response.expirationTime);
                self.nextValue = response.nextValue;
                self.getHtml(response.bids);
                if (!response.error) {
                    if (self.loaderHide) {
                        self.hideLoader();
                        self.loaderHide = 0;
                    }
                    self.start();
                    console.clear();
                } else {
                    swal(response.title, response.message, "warning");
                }
            },
            error: function () {
                self.hideLoader();
//                swal("Error!","Something went wrong on Server. Please try again.","error");
            }
        });
    },
    getHtml: function (data) {
        var html = '';
        $.each(data, function (index, val) {
            html += '<tr>';
            html += '<td>' + val.name + '</td>';
            html += '<td>₹' + (val.amount).format(2) + '</td>';
            html += '</tr>';
        });
        if (!html) {
            html = '<tr><td colspan="2" class="text-center">Nobody has made a bid</td></tr>';
        }
        $('#bidsBody').html(html);
    },
    isNumeric: function (element) {
        var input = $(element);
        var inputValue = input.val();
        input.parent('.form-group').find('.error-message').remove();
        if (inputValue) {
            var placeholder = input.attr('placeholder');
            if (!$.isNumeric(inputValue)) {
                var inputValue = parseFloat(inputValue);
                if (isNaN(inputValue)) {
                    inputValue = '';
                }
                input.parent('.form-group').append('<p class="error-message" style="float: left; width: 100%;">' + placeholder + ' must be Numeric</p>');
            }
        }
        input.val($.trim(inputValue));
    },
    addBid: function (element) {
        var self = this;
        var formData = $(element).serializeArray();
        var bidAmount = formData[0].value;
        if (self.validate(bidAmount, element)) {
            swal({
                title: '',
                text: "Are you sure to bid for the amount ₹"+ parseFloat(bidAmount).format(2) +"/- ?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Add Bid!',
                cancelButtonText: 'Change amount'
            }).then(function () {
                $.ajax({
                    url: self.addBidUrl,
                    data: {id: self.id, amount: bidAmount},
                    type: 'POST',
                    dataType: 'JSON',
                    beforeSend: function (xhr) {
                        self.showLoader();
                    },
                    success: function (response) {
                        $(element).find('input[type="text"]').val('');
                        if (!response.error) {
                            self.loaderHide = 1;
                            self.start();
                        } else {
                            self.hideLoader();
                            swal(response.title, response.message, "warning");
                        }
                    },
                    error: function () {
                        self.hideLoader();
                        swal("Error!", "Something went wrong on Server. Please try again.", "error");
                    }
                });
            });
        }
    },
    validate: function (bidAmount, element) {
        var self = this;
        $(document).find('.error-message').remove();
        var validate = true;
        if (!bidAmount) {
            validate = false;
            $(element).find('.form-group').append('<p class="error-message" style="float: left; width: 100%;">Bid Amount is required</p>');
        } else if (parseFloat(bidAmount) < self.nextValue) {
            validate = false;
            $(element).find('.form-group').append('<p class="error-message" style="float: left; width: 100%;">Bid Amount must be greater than equals to ' + self.nextValue + '</p>');
        }
        return validate;
    }
};