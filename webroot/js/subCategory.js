var OmdSites = {
    'omds installed under one roof': 'omds_installed_under_one_roof',
    'omds installed within premises but in open courtyard': 'omds_installed_within_premises'
};
var SubCategory = {
    ajaxUrl: '',
    errorRedirectionUrl: '',
    subCategoryId: '',
    omdRoofCount: 0,
    omdPremisesCount: 0,
    typeIMultiple: {'Standard OMD' : 1, 'LED/LCD OMD' : 3},
    typeIPrice: {'omds_installed_under_one_roof' : 450, 'omds_installed_within_premises' : 700},
    defaultValidations: {},
    customValidations: {},
    subCategoryLocalData: {},
    tenureData: {"3 months": 3, "6 months": 6, "9 months": 9, "1 year": 12, "2 years": 24, "3 years": 36},
    clearFormPanel: function (panelId) {
        $('#' + panelId).html('');
    },
    subCategoryData: function (subCategoryId, panelId) {
        var self = this;
        this.subCategoryId = subCategoryId;
        self.defaultValidations = {};
        if (self.subCategoryLocalData[subCategoryId]) {
            self.getHtml(self.subCategoryLocalData[subCategoryId], panelId);
            if ($(document).find('#formData input[name="location"]').length) {
                initialize();
                $("body").tooltip({selector: '[data-toggle=tooltip]'});
            }
        } else {
            Typology.showLoader();
            $.ajax({
                url: self.ajaxUrl,
                type: 'POST',
                dataType: 'JSON',
                data: {id: subCategoryId},
                success: function (response) {
                    Typology.hideLoader();
                    self.subCategoryLocalData[subCategoryId] = response;
                    self.getHtml(response, panelId);
                    if ($(document).find('#formData input[name="location"]').length) {
                        initialize();
                        $("body").tooltip({selector: '[data-toggle=tooltip]'});
                    }
                }, error: function (error) {
                    Typology.hideLoader();
                   // window.location = self.errorRedirectionUrl;
                }
            });
        }
    },
    getHtml: function (data, panelId) {
        var self = this;
        var html = '<div class="form-step1 form-steps">';
        $.each(data, function (index, val) {
            if (val.group_in === '1') {
                var field = self[val.type];
                html += field(val, self);
            }
        });
        if (html) {
            html += self.next();
        }
        html += '</div>';
        //console.log(html);
        html += '<div class="form-step2 form-steps" style="display: none;">';
        $.each(data, function (index, val) {
            if (val.group_in === '2') {
                var field = self[val.type];
                html += field(val, self);
            }
        });
        html += self.previous(1);
        html += self.preview();
        html += '</div>';
        html += '<div id="previewPanel" style="display: none;" class="col-md-12 form-step3 form-steps">';
        $.each(data, function (index, val) {
            if (Typology.typologyId === '10') {
                if (val.field_key === 'total_permission_fees') {
                    html += '<div id="omds_installed_under_one_roof_preview"></div>';
                    html += '<div id="omds_installed_within_premises_preview"></div>';
                }
            }
            html += '<div class="col-md-12">';
            if (val.field_key !== 'building_type[]') {
                html += '<label class="col-md-6">' + val.lable + '</label><span class="col-md-6" id="p_' + val.field_key + '">--N/A--</span>';
            } else {
                html += '<label class="col-md-6">' + val.lable + '</label><span class="col-md-6" id="p_building_type">--N/A--</span>';
            }
            html += '</div>';
            if (self.subCategoryId === '17') {
                if (val.field_key === 'metro_mrts_categories') {
                    html += '<div id="metro_mrts_preview"></div>';
                }
            }
            if (self.subCategoryId === '11') {
                if (val.field_key === 'shelters_type') {
                    html += '<div id="shelters_type_preview"></div>';
                }
            }
            if (self.subCategoryId === '12') {
                if (val.field_key === 'route_markers_type') {
                    html += '<div id="route_markers_type_preview"></div>';
                }
            }
            if (self.subCategoryId === '13') {
                if (val.field_key === 'foot_over_type') {
                    html += '<div id="foot_over_type_preview"></div>';
                }
            }
            if (self.subCategoryId === '14') {
                if (val.field_key === 'cycle_station_type') {
                    html += '<div id="cycle_station_type_preview"></div>';
                }
            }
            if (self.subCategoryId === '15') {
                if (val.field_key === 'police_booth_type') {
                    html += '<div id="police_booth_type_preview"></div>';
                }
            }
            if (self.subCategoryId === '16') {
                if (val.field_key === 'sitting_bench_type') {
                    html += '<div id="sitting_bench_type_preview"></div>';
                }
            }
            if (self.subCategoryId === '32') {
                if (val.field_key === 'omd_type_type_a') {
                    html += '<div id="omd_type_a_preview"></div>';
                }
            }
        });
        html +='<div class="checkbox col-md-12">';
        html +='<label>';
        html +='<input type="checkbox" id="agreeOMD">';
        html +='<p class="termsc">I/we shall adhere to the <b class="orang">terms & conditions</b> of outdoor media byelaws/policy framed by the MCF/Govt. of Haryana. I/we confirm that the above information is true to the best of my/ our knowledge. In case there is any discrepancy in the information provided above, the application shall be rejected</p>';
        html +='</label>';
        //html +='<p style="color: #782669; font-style: italic;">*Disclaimer- This is a newly launched website by MCG. In case of any error/omission, MCG reserves the right to raise additional claims on the applicant.</p>';
        html +='</div>';
        html += self.previous(2);
        html += self.submit();
        html += '</div>';
        $('#' + panelId).html('');
        if (data.length > 0) {
            html += '<p style="color: #782669; font-style: italic; float: left; width: 100%;">*Disclaimer- This is a newly launched website by MCF. In case of any error/omission, MCF reserves the right to raise additional claims on the applicant.</p>';
            $('#' + panelId).html(html);
        }
    },
    getOmdForm: function (element) {
        var self = this;
        if ($(element).is(':checked')) {
            var selectedOption = ($(element).val()).toLowerCase();
            console.log(OmdSites[selectedOption] + '_html');
            var html = self[OmdSites[selectedOption] + '_html'];
            $(document).find('#' + OmdSites[selectedOption]).html(html(self));
            var totalAnnualFeesToPay = 0;
            var totalAnnualFeesRoof = 0;
            var totalAnnualFeesPremises = 0;
            if ($.trim($('#omds_installed_under_one_roof table tfoot tr .total_annual_fees').val())) {
                totalAnnualFeesRoof = parseFloat($('#omds_installed_under_one_roof table tfoot tr .total_annual_fees').val());
            }
            if ($.trim($('#omds_installed_within_premises table tfoot tr .total_annual_fees').val())) {
                totalAnnualFeesPremises = parseFloat($('#omds_installed_within_premises table tfoot tr .total_annual_fees').val());
            }
            $('#formData .form-groupY input[name="annual_license_fees"]').val('');
            totalAnnualFeesToPay = totalAnnualFeesRoof + totalAnnualFeesPremises;
            if (totalAnnualFeesToPay) {
                $('#formData .form-groupY input[name="annual_license_fees"]').val(totalAnnualFeesToPay);
            }
        } else {
            var unSelectedOption = ($(element).val()).toLowerCase();
            $(document).find('#' + OmdSites[unSelectedOption]).html('');
            var totalAnnualFeesToPay = 0;
            var totalAnnualFeesRoof = 0;
            var totalAnnualFeesPremises = 0;
            if ($.trim($('#omds_installed_under_one_roof table tfoot tr .total_annual_fees').val())) {
                totalAnnualFeesRoof = parseFloat($('#omds_installed_under_one_roof table tfoot tr .total_annual_fees').val());
            }
            if ($.trim($('#omds_installed_within_premises table tfoot tr .total_annual_fees').val())) {
                totalAnnualFeesPremises = parseFloat($('#omds_installed_within_premises table tfoot tr .total_annual_fees').val());
            }
            $('#formData .form-groupY input[name="annual_license_fees"]').val('');
            totalAnnualFeesToPay = totalAnnualFeesRoof + totalAnnualFeesPremises;
            if (totalAnnualFeesToPay) {
                $('#formData .form-groupY input[name="annual_license_fees"]').val(totalAnnualFeesToPay);
            }
        }
        $(document).find('#omds_installed_under_one_roof table tbody tr:first-child td .heightData, #omds_installed_within_premises table tbody tr:first-child td .heightData').trigger('keyup');
    },
    omds_installed_under_one_roof_html: function (self) {
        var html = '<div class="col-md-12">';
        html += '<h3>OMDS Installed Under One Roof</h3>';
        html += '<table class="table">';
        html += '<thead>';
        html += '<tr>';
        html += '<th>S.no</th>';
        html += '<th>OMD Site</th>';
        html += '<th>Height(in m)</th>';
        html += '<th>Width(in m)</th>';
        html += '<th>Price(per sq. m)</th>';
        html += '<th>Area(in sq. m)</th>';
        html += '<th></th>';
        html += '</tr>';
        html += '</thead>';
        html += '<tbody>';
        html += '<tr>';
        html += '<td>1</td>';
        html += '<td>';
        html += '<select class="form-control" name="omd_one_roof_omd_type[' + self.omdRoofCount + ']" onchange="SubCategory.calculateArea(this)">';
        html += '<option value="">Select OMD Site</option>';
        html += '<option value="Standard OMD">Standard OMD</option>';
        html += '<option value="LED/LCD OMD">LED/LCD OMD</option>';
        html += '</select>';
        html += '</td>';
        html += '<td><input type="text" class="form-control heightData" autocomplete="off" name="omd_one_roof_height[' + self.omdRoofCount + ']" onkeyup="SubCategory.calculateArea(this)"></td>';
        html += '<td><input type="text" class="form-control widthData" autocomplete="off" name="omd_one_roof_width[' + self.omdRoofCount + ']" onkeyup="SubCategory.calculateArea(this)"></td>';
        html += '<td><input type="text" class="form-control priceData" name="omd_one_roof_price[' + self.omdRoofCount + ']" readonly="readonly"></td>';
        html += '<td><input type="text" class="form-control areaData" readonly="readonly" name="omd_one_roof_area[' + self.omdRoofCount + ']"></td>';
        html += '<td><a href="javascript:void(0)" onclick="SubCategory.addUnderOneRoof(this)" class="plus-btn">+ Add More</a></td>';
        html += '</tr>';
        html += '</tbody>';
        html += '<tfoot>';
        html += '<tr>';
//        html += '<td><label>Licence Fees(in Rs.)</lable><input type="text" class="form-control licence_fees" readonly="readonly" value="" name="omd_one_roof_licence_fees"></td>';
        html += '<td colspan="2"><label>Total Area(in sq. m)</lable><input type="text" class="form-control total_area" readonly="readonly" name="omd_one_roof_total_area"></td>';
        html += '<td colspan="5"><label>Permission Fees(in Rs.)</lable><input type="text" class="form-control total_annual_fees" readonly="readonly" name="omd_one_roof_total_annual_fees"></td>';
        html += '</tr>';
        html += '</tfoot>';
        html += '</table>';
        html += '</div>';
        return html;
    },
    omds_installed_within_premises_html: function (self) {
        var html = '<div class="col-md-12">';
        html += '<h3>OMDS installed within premises but in open courtyard</h3>';
        html += '<table class="table">';
        html += '<thead>';
        html += '<tr>';
        html += '<th>S.no</th>';
        html += '<th>OMD Site</th>';
        html += '<th>Height(in m)</th>';
        html += '<th>Width(in m)</th>';
        html += '<th>Price(per sq. m)</th>';
        html += '<th>Area(in sq. m)</th>';
        html += '<th></th>';
        html += '</tr>';
        html += '</thead>';
        html += '<tbody>';
        html += '<tr>';
        html += '<td>1</td>';
        html += '<td>';
        html += '<select class="form-control" name="omd_premises_omd_type[' + self.omdPremisesCount + ']" onchange="SubCategory.calculateArea(this)">';
        html += '<option value="">Select OMD Site</option>';
        html += '<option value="Standard OMD">Standard OMD</option>';
        html += '<option value="LED/LCD OMD">LED/LCD OMD</option>';
        html += '</select>';
        html += '</td>';
        html += '<td><input type="text" class="form-control heightData" autocomplete="off" name="omd_premises_height[' + self.omdPremisesCount + ']" onkeyup="SubCategory.calculateArea(this)"></td>';
        html += '<td><input type="text" class="form-control widthData" autocomplete="off" name="omd_premises_width[' + self.omdPremisesCount + ']" onkeyup="SubCategory.calculateArea(this)"></td>';
        html += '<td><input type="text" class="form-control priceData" name="omd_premises_price[' + self.omdPremisesCount + ']" readonly="readonly"></td>';
        html += '<td><input type="text" class="form-control areaData" readonly="readonly" name="omd_premises_area[' + self.omdPremisesCount + ']"></td>';
        html += '<td><a href="javascript:void(0)" onclick="SubCategory.addUnderPremises(this)" class="plus-btn">+ Add More</a></td>';
        html += '</tr>';
        html += '</tbody>';
        html += '<tfoot>';
        html += '<tr>';
//        html += '<td><label>Licence Fees(in Rs.)</lable><input type="text" class="form-control licence_fees" readonly="readonly" value="" name="omd_premises_licence_fees"></td>';
        html += '<td colspan="2"><label>Total Area(in sq. m)</lable><input type="text" class="form-control total_area" readonly="readonly" name="omd_premises_total_area"></td>';
        html += '<td colspan="5"><label>Permission Fees(in Rs.)</lable><input type="text" class="form-control total_annual_fees" readonly="readonly" name="omd_premises_total_annual_fees"></td>';
        html += '</tr>';
        html += '</tfoot>';
        html += '</table>';
        html += '</div>';
        return html;
    },
    addUnderOneRoof: function (element) {
        var self = this;
        self.omdRoofCount++;
        var html = '<tr>';
        html += '<td>1</td>';
        html += '<td>';
        html += '<select class="form-control" name="omd_one_roof_omd_type[' + self.omdRoofCount + ']" onchange="SubCategory.calculateArea(this)">';
        html += '<option value="">Select OMD Site</option>';
        html += '<option value="Standard OMD">Standard OMD</option>';
        html += '<option value="LED/LCD OMD">LED/LCD OMD</option>';
        html += '</select>';
        html += '</td>';
        html += '<td><input type="text" class="form-control heightData" autocomplete="off" name="omd_one_roof_height[' + self.omdRoofCount + ']" onkeyup="SubCategory.calculateArea(this)"></td>';
        html += '<td><input type="text" class="form-control widthData" autocomplete="off" name="omd_one_roof_width[' + self.omdRoofCount + ']" onkeyup="SubCategory.calculateArea(this)"></td>';
        html += '<td><input type="text" class="form-control priceData" name="omd_one_roof_price[' + self.omdRoofCount + ']" readonly="readonly"></td>';
        html += '<td><input type="text" class="form-control areaData" readonly="readonly" name="omd_one_roof_area[' + self.omdRoofCount + ']"></td>';
        if ($(document).find('#omds_installed_under_one_roof tbody tr').length < 19) {
            html += '<td><a href="javascript:void(0)" onclick="SubCategory.addUnderOneRoof(this)" class="plus-btn">+ Add More</a></td>';
        } else {
            html += '<td><a href="javascript:void(0)" onclick="SubCategory.removeOmd(this)" class="close-btn">X</a></td>';
        }
        html += '</tr>';
        $(element).attr('onclick', 'SubCategory.removeOmd(this)');
        $(element).html('X');
        $(element).removeClass('plus-btn');
        $(element).addClass('close-btn');
        $('#omds_installed_under_one_roof tbody').append(html);
        self.omdSerailNos();
    },
    addUnderPremises: function (element) {
        var self = this;
        self.omdPremisesCount++;
        var html = '<tr>';
        html += '<th>S.no</th>';
        html += '<th>OMD Site</th>';
        html += '<th>Height</th>';
        html += '<th>Width</th>';
        html += '<th>Area</th>';
        html += '<th></th>';
        html += '</tr>';
        html += '</thead>';
        html += '<tbody>';
        html += '<tr>';
        html += '<td>1</td>';
        html += '<td>';
        html += '<select class="form-control" name="omd_premises_omd_type[' + self.omdPremisesCount + ']" onchange="SubCategory.calculateArea(this)">';
        html += '<option value="">Select OMD Site</option>';
        html += '<option value="Standard OMD">Standard OMD</option>';
        html += '<option value="LED/LCD OMD">LED/LCD OMD</option>';
        html += '</select>';
        html += '</td>';
        html += '<td><input type="text" class="form-control heightData" autocomplete="off" name="omd_premises_height[' + self.omdPremisesCount + ']" onkeyup="SubCategory.calculateArea(this)"></td>';
        html += '<td><input type="text" class="form-control widthData" autocomplete="off" name="omd_premises_width[' + self.omdPremisesCount + ']" onkeyup="SubCategory.calculateArea(this)"></td>';
        html += '<td><input type="text" class="form-control priceData" name="omd_premises_price[' + self.omdPremisesCount + ']" readonly="readonly"></td>';
        html += '<td><input type="text" class="form-control areaData" readonly="readonly" name="omd_premises_area[' + self.omdPremisesCount + ']"></td>';
        if ($(document).find('#omds_installed_within_premises tbody tr').length < 19) {
            html += '<td><a href="javascript:void(0)" onclick="SubCategory.addUnderPremises(this)" class="plus-btn">+ Add More</a></td>';
        } else {
            html += '<td><a href="javascript:void(0)" onclick="SubCategory.removeOmd(this)" class="close-btn">X</a></td>';
        }
        html += '</tr>';
        $(element).attr('onclick', 'SubCategory.removeOmd(this)');
        $(element).html('X');
        $(element).removeClass('plus-btn');
        $(element).addClass('close-btn');
        $('#omds_installed_within_premises tbody').append(html);
        self.omdSerailNos();
    },
    removeOmd: function (element) {
        var tbody = $(element).parent('td').parent('tr').parent('tbody');
        var tableParentId = tbody.parent('table').parent('div').parent('div').attr('id');
        $(element).parent('td').parent('tr').remove();
        if (tableParentId === 'omds_installed_under_one_roof') {
            tbody.find('tr:last-of-type td:last-of-type').html('<a href="javascript:void(0)" onclick="SubCategory.addUnderOneRoof(this)" class="plus-btn">+ Add More</a>');
        } else {
            tbody.find('tr:last-of-type td:last-of-type').html('<a href="javascript:void(0)" onclick="SubCategory.addUnderPremises(this)" class="plus-btn">+ Add More</a>');
        }
        $(document).find('#omds_installed_under_one_roof table tbody tr:first-child td .heightData, #omds_installed_within_premises table tbody tr:first-child td .heightData').trigger('keyup');
        this.omdSerailNos();
    },
    omdSerailNos: function () {
        var sNo = 1;
        $(document).find('#omds_installed_under_one_roof table tbody tr').each(function () {
            $(this).find('td:first-of-type').html(sNo);
            sNo++;
        });
        sNo = 1;
        $(document).find('#omds_installed_within_premises table tbody tr').each(function () {
            $(this).find('td:first-of-type').html(sNo);
            sNo++;
        });
    },
    calculateArea: function (element) {
        var self = this;
        var input = $(element);
        var formIds = Object.keys(self.typeIPrice);
        var parentBlock = input.parents('#'+formIds.join(', #'));
        var currentParent = parentBlock.attr('id');
        var isSelect = input.is('select');
        var inputValue = input.val();
        if (inputValue && !isSelect) {
            if (!$.isNumeric(inputValue)) {
                var inputValue = parseFloat(inputValue);
                if (isNaN(inputValue)) {
                    inputValue = '';
                }
                input.val(inputValue);
            }
        }
        var rowPrice = 0;
        var totalArea = 0;
        var area = 0;
        var height = 0;
        var width = 0;
        var licence_fees = 0;
        var parentTable = $(element).parent('td').parent('tr').parent('tbody').parent('table');
        var parentTableBody = $(element).parent('td').parent('tr').parent('tbody');
        var parentRow = $(element).parent('td').parent('tr');
        if ($.isNumeric(parentRow.find('.heightData').val())) {
            height = parentRow.find('.heightData').val();
        }
        if ($.isNumeric(parentRow.find('.widthData').val())) {
            width = parentRow.find('.widthData').val();
        }
        area = height * width;
        parentRow.find('.areaData').val('');
        if (area) {
            parentRow.find('.areaData').val(area);
        }
        if(isSelect) {
            parentRow.find('.priceData').val('');
            var selectedOmd = input.find('option:selected').val();
            if(selectedOmd) {
                rowPrice = self.typeIPrice[currentParent] * self.typeIMultiple[selectedOmd];
                parentRow.find('.priceData').val(rowPrice);
            }
        }
        parentTableBody.find('tr').each(function () {
            var selectedOmdOtion = $(this).find('select option:selected').val();
            var omdHeight = $.trim($(this).find('.heightData').val());
            var omdWidth = $.trim($(this).find('.widthData').val());
            if ($.isNumeric($(this).find('.areaData').val())) {
                totalArea += parseFloat($(this).find('.areaData').val());
            }
            if(selectedOmdOtion && omdHeight && omdWidth) {
                var price = self.typeIPrice[currentParent] * self.typeIMultiple[selectedOmdOtion];
                var area = parseFloat(omdHeight) * parseFloat(omdWidth);
                licence_fees += (price * area);
            }
        });
        parentTable.find('tfoot .licence_fees').val('');
        parentTable.find('tfoot .total_area').val('');
        parentTable.find('tfoot .total_annual_fees').val('');
        if (totalArea) {
            parentTable.find('tfoot .total_area').val(totalArea);
//            var total_annual_fees = licence_fees * totalArea;
//            parentTable.find('tfoot .total_annual_fees').val(total_annual_fees);
        }
        if(licence_fees) {
//            parentTable.find('tfoot .licence_fees').val(licence_fees);
            parentTable.find('tfoot .total_annual_fees').val(licence_fees);
        }
        var totalAnnualFeesToPay = 0;
        var totalAnnualFeesRoof = 0;
        var totalAnnualFeesPremises = 0;
        if ($.trim($('#omds_installed_under_one_roof table tfoot tr .total_annual_fees').val())) {
            totalAnnualFeesRoof = parseFloat($('#omds_installed_under_one_roof table tfoot tr .total_annual_fees').val());
        }
        if ($.trim($('#omds_installed_within_premises table tfoot tr .total_annual_fees').val())) {
            totalAnnualFeesPremises = parseFloat($('#omds_installed_within_premises table tfoot tr .total_annual_fees').val());
        }
        $('#formData .form-groupY input[name="total_permission_fees"]').val('');
        var tenure = 1;
        if ($.trim($(document).find('input[name="tenure"]').val())) {
            tenure = $.trim($(document).find('input[name="tenure"]').val());
        }
        totalAnnualFeesToPay = totalAnnualFeesRoof + totalAnnualFeesPremises;
        if (totalAnnualFeesToPay) {
            var result = totalAnnualFeesToPay * tenure;
            setTimeout(function () {
                var discount = $(document).find('input[name="discount"]').val();
                if ($.trim(discount)) {
                    var discountAmount = (parseFloat(discount) / 100) * result;
                    result = result - discountAmount;
                }
                $('#formData .form-groupY input[name="total_permission_fees"]').val(result);
            }, 200);
        }
    },
    clearOMDSizeFieldsValidations: function (element) {
        var i;
        for (i = 1; i <= 6; i++) {
            this.defaultValidations[element.attr('name') + '_size_' + i + '-input-1'] = "";
            this.defaultValidations[element.attr('name') + '_location_' + i + '-input-1'] = "";
        }
    },
    getOMDSizeFields: function (element) {
        this.clearOMDSizeFieldsValidations(element);
        $(document).find('#' + 'omd_size_' + element.attr('name')).remove();
        var i;
        var addLocation = 0;
        var html = '<div id="omd_size_' + element.attr('name') + '">';
        var placeHolder = '13.5 sq. m ( 4 X 3.5 )';
        var LocationPlaceHolder = 'Location';
        if (element.attr('name') === 'omd_no_others') {
            placeHolder = '13.5 sq. m ( 4 X 3.5 )';
            addLocation = 1;
        }
        for (i = 1; i <= element.val(); i++) {
            this.defaultValidations[element.attr('name') + '_size_' + i + '-input-1'] = '{"noEmpty": true,"isNum": true,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}';
            html += '<div class="col-md-' + (addLocation ? '6' : '12') + '">';
            html += '<label>OMD ' + i + ' Size</label>';
            html += '<input type="text" name="' + element.attr('name') + '_size_' + i + '" class="form-control omd-size-meters ' + element.attr('name') + '" autocomplete="off" placeholder="' + placeHolder + '">';
            html += '</div>';
            if (addLocation) {
                this.defaultValidations[element.attr('name') + '_location_' + i + '-input-1'] = '{"noEmpty": true,"isNum": false,"isAlphabit": false,"isAlphanumeric": false,"minLimit": false,"maxLimit": false}';
                html += '<div class="col-md-6">';
                html += '<label>OMD ' + i + ' Location</label>';
                html += '<input type="text" name="' + element.attr('name') + '_location_' + i + '" class="form-control omd-location-meters" autocomplete="off" placeholder="OMD ' + i + ' ' + LocationPlaceHolder + '">';
                html += '</div>';
            }
        }
        html += '</div>';
        element.after(html);
    },
    getPreviousForm: function (stepNumber) {
        $(document).find('.form-steps').hide();
        $(document).find('.form-step' + stepNumber).show();
        $('.stepsblk ul li').removeClass('active');
        $('.stepsblk ul li:eq(' + (stepNumber - 1) + ')').addClass('active');
    },
    next: function () {
        var html = '<input type="button" value="Next" id="formPreview" class="btn btn-default" style="float: left; clear: both; width: 18%;">';
        return html;
    },
    previous: function (formNumber) {
        var html = '<input type="button" value="Edit" class="btn btn-default" style="float: left; clear: both; width: 18%;" onclick="SubCategory.getPreviousForm(' + formNumber + ')">';
        return html;
    },
    preview: function () {
        var html = '<input type="button" value="Preview" class="btn btn-default" style="float: left; clear: both; width: 18%;" onclick="SubCategory.getPreview()">';
        return html;
    },
    text: function (data, self) {
        self.defaultValidations[data.field_key + '-input-' + data.group_in] = data.default_validations;
        self.customValidations[data.field_key] = data.custom_validation;
        var readonly = '';
        if (data.is_disabled === '1') {
            readonly = 'readonly="readonly"';
        }
        if (data.custom_disable === '1') {
            readonly = 'readonly="readonly"';
        }
       

        if (data.custom_calculations) {
            self.customCalculations(data.field_key, JSON.parse(data.custom_calculations));
        }
        var html = '<div class="col-md-6 form-groupY">';
        if (data.field_key === 'location') {
            html = '<div class="col-md-12 form-groupY">';
        }
        html += '<label>' + data.lable + '</label>';
        html += '<input class="form-control" type="text" name="' + data.field_key + '" ' + readonly + ' autocomplete="off" placeholder="' + data.lable + '" tabindex="-1">';
        html += '</div>';
        if (data.field_key === 'location') {
            html += '<div class="col-md-12">';
            html += '<div id="map-canvas" style="height: 400px;"></div>';
            html += '</div>';
        }
        return html;
    },
    radio: function (data, self) {
        self.defaultValidations[data.field_key + '-input-' + data.group_in] = data.default_validations;
        self.customValidations[data.field_key] = data.custom_validation;
        var options = JSON.parse(data.options);
        var html = '<div class="col-md-6 form-groupY">';
        html += '<div class="form-group">';
        html += '<label>' + data.lable + '</label>';
        html += '<div class="radiobtn">';
        $.each(options, function (index, val) {
            html += '<label><input type="radio" name="' + data.field_key + '" value="' + val + '"><p>' + val + '</p></label>';
        });
        html += '</div>';
        html += '</div>';
        html += '</div>';
        return html;
    },
    select: function (data, self) {
        var mutiple = '';
        var id = '';
        var bootClass = 'col-md-6 ';
        if (Typology.typologyId === '10') {
            if (data.field_key === 'building_type[]') {
                mutiple = 'multiple="multiple"';
                id = 'multipleSelect2';
                bootClass = 'col-md-12 ';
            }
        }
        if ((self.subCategoryId === '17' && data.field_key === 'metro_mrts_categories') || (self.subCategoryId === '11' && data.field_key === 'shelters_type') || (self.subCategoryId === '12' && data.field_key === 'route_markers_type') || (self.subCategoryId === '13' && data.field_key === 'foot_over_type') || (self.subCategoryId === '14' && data.field_key === 'cycle_station_type') || (self.subCategoryId === '15' && data.field_key === 'police_booth_type') || (self.subCategoryId === '16' && data.field_key === 'sitting_bench_type') || (self.subCategoryId === '32' && data.field_key === 'omd_type_type_a')) {
            bootClass = 'col-md-12 ';
        }
        self.defaultValidations[data.field_key + '-select-' + data.group_in] = data.default_validations;
        self.customValidations[data.field_key] = data.custom_validation;
        var options = JSON.parse(data.options);
        
        if(data.field_key == 'zone') {
			
			console.log(options)
			console.log(options.length);
			var html = '<div class="' + bootClass + ' form-groupY" style="display:none;">';
			html += '<label>' + data.lable + '</label>';
			html += '<select class="form-control" name="' + data.field_key + '" ' + mutiple + ' id="' + id + '">';
			html += '<option value="">Select ' + data.lable + '</option>';
			$.each(options, function (index, val) {
				html += '<option selected value="' + val + '">' + val + '</option>';
			});
			html += '</select>';
			html += '</div>';
			
		} else {
			var html = '<div class="' + bootClass + ' form-groupY">';
			html += '<label>' + data.lable + '</label>';
			html += '<select class="form-control" name="' + data.field_key + '" ' + mutiple + ' id="' + id + '">';
			html += '<option value="">Select ' + data.lable + '</option>';
			$.each(options, function (index, val) {
				html += '<option value="' + val + '">' + val + '</option>';
			});
			html += '</select>';
			html += '</div>';
        
			
		}
        /*
        var html = '<div class="' + bootClass + ' form-groupY">';
        html += '<label>' + data.lable + '</label>';
        html += '<select class="form-control" name="' + data.field_key + '" ' + mutiple + ' id="' + id + '">';
        html += '<option value="">Select ' + data.lable + '</option>';
        $.each(options, function (index, val) {
            html += '<option value="' + val + '">' + val + '</option>';
        });
        html += '</select>';
        html += '</div>';
        */
        
        
        if (self.subCategoryId === '17' && data.field_key === 'metro_mrts_categories') {
            html += '<div id="metro_mrts_categories_block" class="form-groupY"></div>';
        }
        if (self.subCategoryId === '11' && data.field_key === 'shelters_type') {
            html += '<div id="shelters_type_block" class="form-groupY"></div>';
        }
        if (self.subCategoryId === '12' && data.field_key === 'route_markers_type') {
            html += '<div id="route_markers_type_block" class="form-groupY"></div>';
        }
        if (self.subCategoryId === '13' && data.field_key === 'foot_over_type') {
            html += '<div id="foot_over_type_block" class="form-groupY"></div>';
        }
        if (self.subCategoryId === '14' && data.field_key === 'cycle_station_type') {
            html += '<div id="cycle_station_type_block" class="form-groupY"></div>';
        }
        if (self.subCategoryId === '15' && data.field_key === 'police_booth_type') {
            html += '<div id="police_booth_type_block" class="form-groupY"></div>';
        }
        if (self.subCategoryId === '16' && data.field_key === 'sitting_bench_type') {
            html += '<div id="sitting_bench_type_block" class="form-groupY"></div>';
        }
        if (self.subCategoryId === '32' && data.field_key === 'omd_type_type_a') {
            html += '<div id="omd_type_a_block" class="form-groupY"></div>';
        }
        if (data.custom_calculations) {
            self.customCalculations(data.field_key, JSON.parse(data.custom_calculations));
        }
        return html;
    },
    date: function (data, self) {
        self.defaultValidations[data.field_key + '-input-' + data.group_in] = data.default_validations;
        self.customValidations[data.field_key] = data.custom_validation;
        var html = '<div class="col-md-6 form-groupY">';
        html += '<label>' + data.lable + '</label>';
        html += '<input class="form-control datePicker" type="text" name="' + data.field_key + '" readonly="readonly" autocomplete="off" placeholder="' + data.lable + '">';
        html += '</div>';
        return html;
    },
    checkbox: function (data, self) {
        var operation = '';
        self.defaultValidations[data.field_key + '-input-' + data.group_in] = data.default_validations;
        if (Typology.typologyId === '10') {
            operation = 'onchange="SubCategory.getOmdForm(this);"';
        }
        var html = '<div class="form-groupY col-md-12">';
        html += '<label style="float: left; width: 100%;">' + data.lable + '</label>';
        var options = JSON.parse(data.options);
        $.each(options, function (index, val) {
            html += '<div class="col-md-6">';
            html += '<label><input name="' + data.field_key + '" type="checkbox" value="' + val + '" ' + operation + '>' + val + '</label>';
            html += '</div>';
        });
        html += '</div>';
        if (Typology.typologyId === '10') {
            if (data.field_key === 'building_type[]') {
                html += '<div id="omds_installed_under_one_roof"></div>';
                html += '<div id="omds_installed_within_premises"></div>';
            }
        }
        return html;
    },
    file: function (data, self) {
        self.defaultValidations[data.field_key + '-input-' + data.group_in] = data.default_validations;
        self.customValidations[data.field_key] = data.custom_validation;
        var html = '<div class="col-md-6 form-groupY">';
        html += '<div class="form-group">';
        html += '<label>' + data.lable + '<a href="javascript:void(0)" data-toggle="tooltip" title="' + data.info + '" class="infoTool">?</a></label>';
        html += '<input class="fileup deviceImageUpload" type="file" name="' + data.field_key + '">';
        html += '</div>';
        html += '</div>';
        return html;
    },
    hidden: function (data) {
        var html = '<input type="hidden" name="' + data.field_key + '">';
        return html;
    },
    submit: function () {
        var html = '<input type="submit" value="Submit" class="btn btn-default" style="float: left; clear: both; width: 18%;" id="submitOmdApp">';
        return html;
    },
    checkValidationStep1: function () {
        var self = this;
        var error = 0;
        if ($(document).find('#omds_installed_under_one_roof').children().length) {
            $(document).find('#omds_installed_under_one_roof table tbody tr').each(function () {
                var select = $(this).find('select');
                var height = $(this).find('input[type="text"].heightData');
                var width = $(this).find('input[type="text"].widthData');
                var area = $(this).find('input[type="text"].areaData');
                if (!select.find('option:selected').val()) {
                    select.after('<p class="error-message">This field is required</p>');
                    error = 1;
                }
                if (!$.trim(height.val())) {
                    height.after('<p class="error-message">This field is required</p>');
                    error = 1;
                }
                if (!$.trim(width.val())) {
                    width.after('<p class="error-message">This field is required</p>');
                    error = 1;
                }
                if (!$.trim(area.val())) {
                    area.after('<p class="error-message">This field is required</p>');
                    error = 1;
                }
            });
        }
        if ($(document).find('#omds_installed_within_premises').children().length) {
            $(document).find('#omds_installed_within_premises table tbody tr').each(function () {
                var select = $(this).find('select');
                var height = $(this).find('input[type="text"].heightData');
                var width = $(this).find('input[type="text"].widthData');
                var area = $(this).find('input[type="text"].areaData');
                if (!select.find('option:selected').val()) {
                    select.after('<p class="error-message">This field is required</p>');
                    error = 1;
                }
                if (!$.trim(height.val())) {
                    height.after('<p class="error-message">This field is required</p>');
                    error = 1;
                }
                if (!$.trim(width.val())) {
                    width.after('<p class="error-message">This field is required</p>');
                    error = 1;
                }
                if (!$.trim(area.val())) {
                    area.after('<p class="error-message">This field is required</p>');
                    error = 1;
                }
            });
        }
        /*******************************/
        /****Metro/MRTS Validations*****/
        /*******************************/
        if ($(document).find('#metro_type_check').length) {
            if (!$(document).find('#metro_type_check input[type="checkbox"]').is(':checked')) {
                $(document).find('#metro_type_check').append('<p class="error-message">This field is required</p>');
                error = 1;
            }
        }
        if ($(document).find('#inside_metro_section').children().length) {
            $(document).find('#inside_metro_section table tbody tr').each(function () {
                var train_no = $.trim($(this).find('.train_no').val());
                var metro_coches = $.trim($(this).find('.metro_coches').val());
                var ad_panels = $.trim($(this).find('.ad_panels').val());
                if (!train_no) {
                    $(this).find('.train_no').after('<p class="error-message">This field is required</p>');
                    error = 1;
                }
                if (!metro_coches) {
                    $(this).find('.metro_coches').after('<p class="error-message">This field is required</p>');
                    error = 1;
                } else if (isNaN(parseFloat(metro_coches))) {
                    $(this).find('.metro_coches').after('<p class="error-message">Value must be numeric</p>');
                    error = 1;
                }
                if (!ad_panels) {
                    $(this).find('.ad_panels').after('<p class="error-message">This field is required</p>');
                    error = 1;
                } else if (isNaN(parseFloat(ad_panels))) {
                    $(this).find('.ad_panels').after('<p class="error-message">Value must be numeric</p>');
                    error = 1;
                }
            });
        }

        if ($(document).find('#outside_metro_section').children().length) {
            $(document).find('#outside_metro_section table tbody tr').each(function () {
                var train_no = $.trim($(this).find('.train_no').val());
                var metro_coches = $.trim($(this).find('.metro_coches').val());
                if (!train_no) {
                    $(this).find('.train_no').after('<p class="error-message">This field is required</p>');
                    error = 1;
                }
                if (!metro_coches) {
                    $(this).find('.metro_coches').after('<p class="error-message">This field is required</p>');
                    error = 1;
                } else if (isNaN(parseFloat(metro_coches))) {
                    $(this).find('.metro_coches').after('<p class="error-message">Value must be numeric</p>');
                    error = 1;
                }
            });
        }

        if ($(document).find('#inside_station_section').length) {
            $(document).find('#inside_station_section table tbody tr').each(function () {
                var select = $(this).find('select');
                var height = $(this).find('input[type="text"].heightData');
                var width = $(this).find('input[type="text"].widthData');
                var area = $(this).find('input[type="text"].areaData');
                if (!select.find('option:selected').val()) {
                    select.after('<p class="error-message">This field is required</p>');
                    error = 1;
                }
                if (!$.trim(height.val())) {
                    height.after('<p class="error-message">This field is required</p>');
                    error = 1;
                }
                if (!$.trim(width.val())) {
                    width.after('<p class="error-message">This field is required</p>');
                    error = 1;
                }
                if (!$.trim(area.val())) {
                    area.after('<p class="error-message">This field is required</p>');
                    error = 1;
                }
            });
        }

        if ($(document).find('#station_branding_section').length) {
            $(document).find('#station_branding_section table tbody tr').each(function () {
                var station_branding_facade = $.trim($(this).find('.station_branding_facade').val());
                var station_branding_actual_facade = $.trim($(this).find('.station_branding_actual_facade').val());
                if (!station_branding_facade) {
                    error = 1;
                    $(this).find('.station_branding_facade').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(parseFloat(station_branding_facade))) {
                    error = 1;
                    $(this).find('.station_branding_facade').after('<p class="error-message">Value must be numeric</p>');
                }
                if (!station_branding_actual_facade) {
                    error = 1;
                    $(this).find('.station_branding_actual_facade').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(parseFloat(station_branding_actual_facade))) {
                    error = 1;
                    $(this).find('.station_branding_actual_facade').after('<p class="error-message">Value must be numeric</p>');
                } else if (station_branding_facade) {
                    var actualValueLimit = (parseFloat(station_branding_facade) * 20) / 100;
                    if (parseFloat(station_branding_actual_facade) > actualValueLimit) {
                        error = 1;
                        $(this).find('.station_branding_actual_facade').after('<p class="error-message">Value must not be greater than ' + actualValueLimit + '</p>');
                    }
                }
            });
        }
        /******************************************/
        /***********Shalters Validation************/
        /******************************************/
        if ($(document).find('#shelters_type_block table').length) {
            $(document).find('#shelters_type_block table tbody tr').each(function () {
                var shelter_location = $.trim($(this).find('.shelter_location').val());
                var shelter_panels = $.trim($(this).find('.shelter_panels').val());
                var shelter_area = $.trim($(this).find('.shelter_area').val());
                var shelter_lat = $.trim($(this).find('.shelter_lat').val());
                var shelter_lng = $.trim($(this).find('.shelter_lng').val());
                if (!shelter_location) {
                    error = 1;
                    $(this).find('.shelter_location').after('<p class="error-message">This field is required</p>');
                }
                if (!shelter_panels) {
                    error = 1;
                    $(this).find('.shelter_panels').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(shelter_panels)) {
                    error = 1;
                    $(this).find('.shelter_panels').after('<p class="error-message">Value must be numeric</p>');
                }
                if (!shelter_area) {
                    error = 1;
                    $(this).find('.shelter_area').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(shelter_area)) {
                    error = 1;
                    $(this).find('.shelter_area').after('<p class="error-message">Value must be numeric</p>');
                }
                if (!shelter_lat) {
                    error = 1;
                    $(this).find('.shelter_lat').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(shelter_lat)) {
                    error = 1;
                    $(this).find('.shelter_lat').after('<p class="error-message">Value must be numeric</p>');
                }
                if (!shelter_lng) {
                    error = 1;
                    $(this).find('.shelter_lng').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(shelter_lng)) {
                    error = 1;
                    $(this).find('.shelter_lng').after('<p class="error-message">Value must be numeric</p>');
                }
            });
        }

        /******************************************/
        /***********Route Markers Validation************/
        /******************************************/
        if ($(document).find('#route_markers_type_block table').length) {
            $(document).find('#route_markers_type_block table tbody tr').each(function () {
                var shelter_location = $.trim($(this).find('.route_markers_location').val());
                var shelter_panels = $.trim($(this).find('.route_markers_devices').val());
                var shelter_area = $.trim($(this).find('.route_markers_area').val());
                var shelter_lat = $.trim($(this).find('.route_markers_lat').val());
                var shelter_lng = $.trim($(this).find('.route_markers_lng').val());
                if (!shelter_location) {
                    error = 1;
                    $(this).find('.route_markers_location').after('<p class="error-message">This field is required</p>');
                }
                if (!shelter_panels) {
                    error = 1;
                    $(this).find('.route_markers_devices').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(shelter_panels)) {
                    error = 1;
                    $(this).find('.route_markers_panels').after('<p class="error-message">Value must be numeric</p>');
                }
                if (!shelter_area) {
                    error = 1;
                    $(this).find('.route_markers_area').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(shelter_area)) {
                    error = 1;
                    $(this).find('.route_markers_area').after('<p class="error-message">Value must be numeric</p>');
                }
                if (!shelter_lat) {
                    error = 1;
                    $(this).find('.route_markers_lat').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(shelter_lat)) {
                    error = 1;
                    $(this).find('.route_markers_lat').after('<p class="error-message">Value must be numeric</p>');
                }
                if (!shelter_lng) {
                    error = 1;
                    $(this).find('.route_markers_lng').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(shelter_lng)) {
                    error = 1;
                    $(this).find('.route_markers_lng').after('<p class="error-message">Value must be numeric</p>');
                }
            });
        }

        /******************************************/
        /***********Foot Over Validation************/
        /******************************************/
        if ($(document).find('#foot_over_type_block table').length) {
            $(document).find('#foot_over_type_block table tbody tr').each(function () {
                var shelter_location = $.trim($(this).find('.foot_over_location').val());
                var shelter_panels = $.trim($(this).find('.foot_over_panels').val());
                var shelter_area = $.trim($(this).find('.foot_over_area').val());
                var shelter_lat = $.trim($(this).find('.foot_over_lat').val());
                var shelter_lng = $.trim($(this).find('.foot_over_lng').val());
                if (!shelter_location) {
                    error = 1;
                    $(this).find('.foot_over_location').after('<p class="error-message">This field is required</p>');
                }
                if (!shelter_panels) {
                    error = 1;
                    $(this).find('.foot_over_panels').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(shelter_panels)) {
                    error = 1;
                    $(this).find('.foot_over_panels').after('<p class="error-message">Value must be numeric</p>');
                }
                if (!shelter_area) {
                    error = 1;
                    $(this).find('.foot_over_area').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(shelter_area)) {
                    error = 1;
                    $(this).find('.foot_over_area').after('<p class="error-message">Value must be numeric</p>');
                }
                if (!shelter_lat) {
                    error = 1;
                    $(this).find('.foot_over_lat').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(shelter_lat)) {
                    error = 1;
                    $(this).find('.foot_over_lat').after('<p class="error-message">Value must be numeric</p>');
                }
                if (!shelter_lng) {
                    error = 1;
                    $(this).find('.foot_over_lng').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(shelter_lng)) {
                    error = 1;
                    $(this).find('.foot_over_lng').after('<p class="error-message">Value must be numeric</p>');
                }
            });
        }
        /******************************************/
        /***********Cycle Station Validation************/
        /******************************************/
        if ($(document).find('#cycle_station_type_block table').length) {
            $(document).find('#cycle_station_type_block table tbody tr').each(function () {
                var shelter_location = $.trim($(this).find('.cycle_station_location').val());
                var shelter_panels = $.trim($(this).find('.cycle_station_panels').val());
                var shelter_area = $.trim($(this).find('.cycle_station_area').val());
                var shelter_lat = $.trim($(this).find('.cycle_station_lat').val());
                var shelter_lng = $.trim($(this).find('.cycle_station_lng').val());
                if (!shelter_location) {
                    error = 1;
                    $(this).find('.cycle_station_location').after('<p class="error-message">This field is required</p>');
                }
                if (!shelter_panels) {
                    error = 1;
                    $(this).find('.cycle_station_panels').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(shelter_panels)) {
                    error = 1;
                    $(this).find('.cycle_station_panels').after('<p class="error-message">Value must be numeric</p>');
                }
                if (!shelter_area) {
                    error = 1;
                    $(this).find('.cycle_station_area').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(shelter_area)) {
                    error = 1;
                    $(this).find('.cycle_station_area').after('<p class="error-message">Value must be numeric</p>');
                }
                if (!shelter_lat) {
                    error = 1;
                    $(this).find('.cycle_station_lat').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(shelter_lat)) {
                    error = 1;
                    $(this).find('.cycle_station_lat').after('<p class="error-message">Value must be numeric</p>');
                }
                if (!shelter_lng) {
                    error = 1;
                    $(this).find('.cycle_station_lng').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(shelter_lng)) {
                    error = 1;
                    $(this).find('.cycle_station_lng').after('<p class="error-message">Value must be numeric</p>');
                }
            });
        }
        /******************************************/
        /***********Police booth Validation************/
        /******************************************/
        if ($(document).find('#police_booth_type_block table').length) {
            $(document).find('#police_booth_type_block table tbody tr').each(function () {
                var shelter_location = $.trim($(this).find('.police_booth_location').val());
                var shelter_panels = $.trim($(this).find('.police_booth_panels').val());
                var shelter_area = $.trim($(this).find('.police_booth_area').val());
                var shelter_lat = $.trim($(this).find('.police_booth_lat').val());
                var shelter_lng = $.trim($(this).find('.police_booth_lng').val());
                if (!shelter_location) {
                    error = 1;
                    $(this).find('.police_booth_location').after('<p class="error-message">This field is required</p>');
                }
                if (!shelter_panels) {
                    error = 1;
                    $(this).find('.police_booth_panels').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(shelter_panels)) {
                    error = 1;
                    $(this).find('.police_booth_panels').after('<p class="error-message">Value must be numeric</p>');
                }
                if (!shelter_area) {
                    error = 1;
                    $(this).find('.police_booth_area').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(shelter_area)) {
                    error = 1;
                    $(this).find('.police_booth_area').after('<p class="error-message">Value must be numeric</p>');
                }
                if (!shelter_lat) {
                    error = 1;
                    $(this).find('.police_booth_lat').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(shelter_lat)) {
                    error = 1;
                    $(this).find('.police_booth_lat').after('<p class="error-message">Value must be numeric</p>');
                }
                if (!shelter_lng) {
                    error = 1;
                    $(this).find('.police_booth_lng').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(shelter_lng)) {
                    error = 1;
                    $(this).find('.police_booth_lng').after('<p class="error-message">Value must be numeric</p>');
                }
            });
        }
        /******************************************/
        /***********Sitting Bench Validation************/
        /******************************************/
        if ($(document).find('#sitting_bench_type_block table').length) {
            $(document).find('#sitting_bench_type_block table tbody tr').each(function () {
                var shelter_location = $.trim($(this).find('.sitting_bench_location').val());
                var shelter_panels = $.trim($(this).find('.sitting_bench_panels').val());
                var shelter_area = $.trim($(this).find('.sitting_bench_area').val());
                var shelter_lat = $.trim($(this).find('.sitting_bench_lat').val());
                var shelter_lng = $.trim($(this).find('.sitting_bench_lng').val());
                if (!shelter_location) {
                    error = 1;
                    $(this).find('.sitting_bench_location').after('<p class="error-message">This field is required</p>');
                }
                if (!shelter_panels) {
                    error = 1;
                    $(this).find('.sitting_bench_panels').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(shelter_panels)) {
                    error = 1;
                    $(this).find('.sitting_bench_panels').after('<p class="error-message">Value must be numeric</p>');
                }
                if (!shelter_area) {
                    error = 1;
                    $(this).find('.sitting_bench_area').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(shelter_area)) {
                    error = 1;
                    $(this).find('.sitting_bench_area').after('<p class="error-message">Value must be numeric</p>');
                }
                if (!shelter_lat) {
                    error = 1;
                    $(this).find('.sitting_bench_lat').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(shelter_lat)) {
                    error = 1;
                    $(this).find('.sitting_bench_lat').after('<p class="error-message">Value must be numeric</p>');
                }
                if (!shelter_lng) {
                    error = 1;
                    $(this).find('.sitting_bench_lng').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(shelter_lng)) {
                    error = 1;
                    $(this).find('.sitting_bench_lng').after('<p class="error-message">Value must be numeric</p>');
                }
            });
        }

        if ($(document).find('#omd_type_a_block').length) {
            $(document).find('#omd_type_a_block table tbody tr').each(function () {
                var otherLocation = $.trim($(this).find('.other_type_a_location').val());
                var otherHeight = $.trim($(this).find('.other_type_a_height').val());
                var otherWidth = $.trim($(this).find('.other_type_a_width').val());
                var otherArea = $.trim($(this).find('.other_type_a_area').val());
                var otherLat = $.trim($(this).find('.other_type_a_lat').val());
                var otherLng = $.trim($(this).find('.other_type_a_lng').val());
                if (!otherLocation) {
                    error = 1;
                    $(this).find('.other_type_a_location').after('<p class="error-message">This field is required</p>');
                }
                if (!otherHeight) {
                    error = 1;
                    $(this).find('.other_type_a_height').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(otherHeight)) {
                    error = 1;
                    $(this).find('.other_type_a_height').after('<p class="error-message">Value must be numeric</p>');
                }
                if (!otherWidth) {
                    error = 1;
                    $(this).find('.other_type_a_width').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(otherWidth)) {
                    error = 1;
                    $(this).find('.other_type_a_width').after('<p class="error-message">Value must be numeric</p>');
                }
                if (!otherArea) {
                    error = 1;
                    $(this).find('.other_type_a_area').after('<p class="error-message">This field is required</p>');
                }
                if (!otherLat) {
                    error = 1;
                    $(this).find('.other_type_a_lat').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(otherLat)) {
                    error = 1;
                    $(this).find('.other_type_a_lat').after('<p class="error-message">Value must be numeric</p>');
                }
                if (!otherLng) {
                    error = 1;
                    $(this).find('.other_type_a_lng').after('<p class="error-message">This field is required</p>');
                } else if (isNaN(otherLng)) {
                    error = 1;
                    $(this).find('.other_type_a_lng').after('<p class="error-message">Value must be numeric</p>');
                }
            });
        }

        $.each(self.customValidations, function (field, val) {
            if (val) {
                var customValidation = JSON.parse(val);
                $.each(customValidation, function (key, validationVal) {
                    var validate = self[key];
                    var element = $('[name="' + field + '"]');
                    if (validate(validationVal, element)) {
                        error = 1;
                        return false;
                    }
                    ;
                });
            }
        });
        $.each(self.defaultValidations, function (field, val) {
            if (val) {
                var fieldData = field.split('-');
                if (fieldData[2] === '1') {
                    var element = $(fieldData[1] + '[name="' + fieldData[0] + '"]');
                    var validation = JSON.parse(val);
                    $.each(validation, function (key, validationVal) {
                        var validate = self[key];
                        if (validate(validationVal, element)) {
                            error = 1;
                            return false;
                        }
                    });
                }
            }
        });
        if ($(document).find('.error-message').length) {
            var body = $("html, body");
            body.stop().animate({scrollTop: ($(document).find('.error-message').offset().top - 50)}, 500);
        }
        return error;
    },
    checkValidationStep2: function () {
        var self = this;
        var error = 0;
        $.each(self.defaultValidations, function (field, val) {
            if (val) {
                var fieldData = field.split('-');
                if (fieldData[2] === '2') {
                    var element = $(fieldData[1] + '[name="' + fieldData[0] + '"]');
                    var validation = JSON.parse(val);
                    $.each(validation, function (key, validationVal) {
                        var validateStep2 = self[key];
                        if (validateStep2(validationVal, element)) {
                            error = 1;
                            return false;
                        }
                    });
                }
            }
        });
        if ($(document).find('.error-message').length) {
            var body = $("html, body");
            body.stop().animate({scrollTop: ($(document).find('.error-message').offset().top - 50)}, 500);
        }
        return error;
    },
    moveStep2: function () {
        $(document).find('.error-message').remove();
        if (!this.checkValidationStep1()) {
            $('.stepsblk ul li').removeClass('active');
            $('.stepsblk ul li:eq(1)').addClass('active');
            $(document).find('.form-step1').hide();
            $(document).find('.form-step2').show();
        }
    },
    getPreview: function () {
        $(document).find('.error-message').remove();
        if (!this.checkValidationStep2()) {
            $('.stepsblk ul li').removeClass('active');
            $('.stepsblk ul li:eq(2)').addClass('active');
            this.showPreview();
        }
    },
    showPreview: function () {
        var data = this.subCategoryLocalData[this.subCategoryId];
        $.each(data, function (index, val) {
            var checked = '';
            if ($('[name="' + val.field_key + '"]').attr('type') === 'radio') {
                checked = ':checked';
            }
            if ($('[name="' + val.field_key + '"]' + checked).val()) {
                if (val.field_key !== 'building_type[]') {
                    $('#p_' + val.field_key).html($('[name="' + val.field_key + '"]' + checked).val());
                } else {
                    var selectedOMDS = [];
                    $('[name="' + val.field_key + '"]:checked').each(function () {
                        selectedOMDS.push($(this).val());
                    });
                    $('#p_building_type').html(selectedOMDS.join(', '));
                }
            }
        });
        this.omdPreviewHtml();
        this.mertoMrtsPreviewHtml();
        Shelters.preview();
        RouteMaker.preview();
        FootOverBridges.preview();
        CycleStation.preview();
        PoliceBooth.preview();
        SittingBench.preview();
        TypologyAOthers.preview();
        $('.form-step2').hide();
        $('#previewPanel').show();
    },
    mertoMrtsPreviewHtml: function () {
        var html = '';
        var sno = 1;
        if ($(document).find('#metro_type_check').length) {
            var metroTypeCheck = [];
            $(document).find('#metro_type_check div.col-md-6').each(function () {
                var checkbox = $(this).find('input[type="checkbox"]');
                if (checkbox.is(':checked')) {
                    metroTypeCheck.push(checkbox.parent('label').text());
                }
            });
            if (metroTypeCheck.length) {
                html += '<div class="col-md-12">';
                html += '<label class="col-md-6">Rolling Stock Type</label>';
                html += '<span class="col-md-6">' + metroTypeCheck.join(', ') + '</span>';
                html += '</div>';
            }
        }
        if ($(document).find('#inside_metro_section').length) {
            html += '<div class="col-md-12">';
            html += '<h3>Inside Metro</h3>';
            html += '<table class="table">';
            html += '<thead>';
            html += '<tr>';
            html += '<th>S. No</th>';
            html += '<th>Train no.</th>';
            html += '<th>No. Of coaches</th>';
            html += '<th>No. Of Advertisement Panels</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';
            $(document).find('#inside_metro_section table tbody tr').each(function () {
                html += '<tr>';
                html += '<td>' + sno + '</td>';
                html += '<td>' + $(this).find('.train_no').val() + '</td>';
                html += '<td>' + $(this).find('.metro_coches').val() + '</td>';
                html += '<td>' + $(this).find('.ad_panels').val() + '</td>';
                html += '</tr>';
                sno++;
            });
            html += '</tbody>';
            html += '</table>';
            html += '</div>';
        }
        sno = 1;
        if ($(document).find('#outside_metro_section').length) {
            html += '<div class="col-md-12">';
            html += '<h3>Outside Metro</h3>';
            html += '<table class="table">';
            html += '<thead>';
            html += '<tr>';
            html += '<th>S. No</th>';
            html += '<th>Train no.</th>';
            html += '<th>No. of coaches for display of advertisement</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';
            $(document).find('#outside_metro_section table tbody tr').each(function () {
                html += '<tr>';
                html += '<td>' + sno + '</td>';
                html += '<td>' + $(this).find('.train_no').val() + '</td>';
                html += '<td>' + $(this).find('.metro_coches').val() + '</td>';
                html += '</tr>';
                sno++;
            });
            html += '</tbody>';
            html += '</table>';
            html += '</div>';
        }
        sno = 1;
        if ($(document).find('#inside_station_section').length) {
            html += '<div class="col-md-12">';
            html += '<h3>Inside Station</h3>';
            html += '<table class="table">';
            html += '<thead>';
            html += '<tr>';
            html += '<th>S.no</th>';
            html += '<th>OMD Site</th>';
            html += '<th>Height(in m)</th>';
            html += '<th>Width(in m)</th>';
            html += '<th>Area(in sq. m)</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';
            $(document).find('#inside_station_section table tbody tr').each(function () {
                html += '<tr>';
                html += '<td>' + sno + '</td>';
                html += '<td>' + $(this).find('select option:selected').val() + '</td>';
                html += '<td>' + $(this).find('.heightData').val() + '</td>';
                html += '<td>' + $(this).find('.widthData').val() + '</td>';
                html += '<td>' + $(this).find('.areaData').val() + '</td>';
                html += '</tr>';
                sno++;
            });
            html += '</tbody>';
            html += '<tfoot>';
            html += '<tr>';
            html += '<td colspan="5"><label><b>Total Area(in sq. m): </b></label><span>' + $(document).find('#inside_station_section table tfoot tr td input').val() + '</span></td>';
            html += '</tr>';
            html += '</tfoot>';
            html += '</table>';
            html += '</div>';
        }
        sno = 1;
        if ($(document).find('#station_branding_section').length) {
            html += '<div class="col-md-12">';
            html += '<h3>Station Branding</h3>';
            html += '<table class="table">';
            html += '<thead>';
            html += '<th>S. No</th>';
            html += '<th>Facade</th>';
            html += '<th>Total Facade area(in sq. m)</th>';
            html += '<th>Area of actual display(in sq. m)</th>';
            html += '</thead>';
            html += '<tbody>';
            $(document).find('#station_branding_section table tbody tr').each(function () {
                html += '<tr>';
                html += '<td>' + sno + '</td>';
                html += '<td>F' + sno + '</td>';
                html += '<td>' + $(this).find('.station_branding_facade').val() + '</td>';
                html += '<td>' + $(this).find('.station_branding_actual_facade').val() + '</td>';
                html += '</tr>';
                sno++;
            });
            html += '</tbody>';
            html += '</table>';
            html += '</div>';
        }
        $(document).find('#metro_mrts_preview').html(html);
    },
    omdPreviewHtml: function () {
        $('#omds_installed_under_one_roof_preview').html('');
        $('#omds_installed_within_premises_preview').html('');
        if ($('#omds_installed_under_one_roof').children().length) {
            var html = '<div class="col-md-12">';
            html += '<h4>OMDS Installed Under One Roof</h4>';
            html += '<table class="table">';
            html += '<thead>';
            html += '<tr>';
            html += '<th>S.no</th>';
            html += '<th>OMD Site</th>';
            html += '<th>Height</th>';
            html += '<th>Width</th>';
            html += '<th>Price(per sq. m)</th>';
            html += '<th>Area</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';
            var sno = 1;
            $(document).find('#omds_installed_under_one_roof table tbody tr').each(function () {
                html += '<tr>';
                html += '<td>' + sno + '</td>';
                html += '<td>';
                html += $(this).find('select option:selected').val();
                html += '</td>';
                html += '<td>' + $(this).find('input[type="text"].heightData').val() + '</td>';
                html += '<td>' + $(this).find('input[type="text"].widthData').val() + '</td>';
                html += '<td>' + $(this).find('input[type="text"].priceData').val() + '</td>';
                html += '<td>' + $(this).find('input[type="text"].areaData').val() + '</td>';
                html += '</tr>';
                sno++;
            });
            html += '</tbody>';
            html += '<tfoot>';
            html += '<tr>';
//            html += '<td><label>Licence Fees: </lable>' + $(document).find('#omds_installed_under_one_roof table tfoot .licence_fees').val() + '</td>';
            html += '<td colspan="2"><label>Total Area: </lable>' + $(document).find('#omds_installed_under_one_roof table tfoot .total_area').val() + '</td>';
            html += '<td colspan="5"><label>Annual Total Licence Fees: </lable>' + $(document).find('#omds_installed_under_one_roof table tfoot .total_annual_fees').val() + '</td>';
            html += '</tr>';
            html += '</tfoot>';
            html += '</table>';
            html += '</div>';
            $('#omds_installed_under_one_roof_preview').html(html);
        }
        if ($('#omds_installed_within_premises').children().length) {
            var html = '<div class="col-md-12">';
            html += '<h4>OMDS installed within premises but in open courtyard</h4>';
            html += '<table class="table">';
            html += '<thead>';
            html += '<tr>';
            html += '<th>S.no</th>';
            html += '<th>OMD Site</th>';
            html += '<th>Height</th>';
            html += '<th>Width</th>';
            html += '<th>Price(per sq. m)</th>';
            html += '<th>Area</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';
            var sno = 1;
            $(document).find('#omds_installed_within_premises table tbody tr').each(function () {
                html += '<tr>';
                html += '<td>' + sno + '</td>';
                html += '<td>';
                html += $(this).find('select option:selected').val();
                html += '</td>';
                html += '<td>' + $(this).find('input[type="text"].heightData').val() + '</td>';
                html += '<td>' + $(this).find('input[type="text"].widthData').val() + '</td>';
                html += '<td>' + $(this).find('input[type="text"].priceData').val() + '</td>';
                html += '<td>' + $(this).find('input[type="text"].areaData').val() + '</td>';
                html += '</tr>';
                sno++;
            });
            html += '</tbody>';
            html += '<tfoot>';
            html += '<tr>';
//            html += '<td><label>Licence Fees: </lable>' + $(document).find('#omds_installed_within_premises table tfoot .licence_fees').val() + '</td>';
            html += '<td colspan="2"><label>Total Area: </lable>' + $(document).find('#omds_installed_within_premises table tfoot .total_area').val() + '</td>';
            html += '<td colspan="5"><label>Annual Total Licence Fees: </lable>' + $(document).find('#omds_installed_within_premises table tfoot .total_annual_fees').val() + '</td>';
            html += '</tr>';
            html += '</tfoot>';
            html += '</table>';
            html += '</div>';
            $('#omds_installed_within_premises_preview').html(html);
        }
    },
    isAlphabit: function (validationVal, element) {
        if (!validationVal) {
            return false;
        }
    },
    isAlphanumeric: function (validationVal, element) {
        if (!validationVal) {
            return false;
        }
    },
    isNum: function (validationVal, element) {
        if (!validationVal) {
            return false;
        }
        var elementVal = $.trim(element.val());
        if (elementVal) {
            if (!$.isNumeric(elementVal)) {
                element.after('<p class="error-message">Please enter numeric value</p>');
                return true;
            }
        }
        return false;
    },
    isInteger: function(validationVal, element) {
        if (!validationVal) {
            return false;
        }
        var elementVal = $.trim(element.val());
        if (elementVal) {
            if(!Number.isInteger(parseFloat(elementVal))) {
                element.after('<p class="error-message">Please enter integer value</p>');
                return true;
            }
        }
        return false;
    },
    maxLimit: function (validationVal, element) {
        if (!validationVal) {
            return false;
        }
        var elementVal = $.trim(element.val());
        if (elementVal) {
            if (elementVal.length > validationVal) {
                element.after('<p class="error-message">Maximum character should be ' + validationVal + '</p>');
                return true;
            }
        }
    },
    minLimit: function (validationVal, element) {
        if (!validationVal) {
            return false;
        }
        var elementVal = $.trim(element.val());
        if (elementVal) {
            if (elementVal.length < validationVal) {
                element.after('<p class="error-message">Mininum character should be ' + validationVal + '</p>');
                return true;
            }
        }
        return false;
    },
    minValue: function (validationVal, element) {
        if (!validationVal) {
            return false;
        }
        var elementVal = $.trim(element.val());
        if (elementVal < validationVal) {
            element.after('<p class="error-message">Mininum value should be ' + validationVal + '</p>');
            return true;
        }
        return false;
    },
    maxValue: function(validationVal, element){
        if (!validationVal) {
            return false;
        }
        var elementVal = $.trim(element.val());
        if (elementVal > validationVal) {
            element.after('<p class="error-message">Maximum value should be ' + validationVal + '</p>');
            return true;
        }
        return false;
    },
    noEmpty: function (validationVal, element) {
        if (!validationVal) {
            return false;
        }
        if ($(document).find('select[name="metro_mrts_categories"]').length) {
            var metroMrtsCategories = ($(document).find('select[name="metro_mrts_categories"] option:selected').val()).toLowerCase();
            if (metroMrtsCategories === 'rolling stock' && $.inArray(element.attr('name'), ['lat', 'lng', 'location']) !== -1) {
                return false;
            }
        }
        if (element.attr('type') === 'checkbox') {
            if (!element.is(':checked')) {
                element.parents('.form-groupY').append('<p class="error-message">This field is required</p>');
                return true;
            }
        } else if (element.attr('type') === 'radio') {
            if (!element.is(':checked')) {
                element.parents('.form-groupY').append('<p class="error-message">This field is required</p>');
                return true;
            }
        } else {
            var elementVal = $.trim(element.val());
            var omdTypeListElement = $(document).find('select[name="omd_type_private_list"]');
            if(element.attr('name') === 'total_facade_area' && omdTypeListElement.length) {
                var omdTypeSelected = omdTypeListElement.find('option:selected').val();
                if(omdTypeSelected === 'Unipole') {
                    return false;
                }
            }
            if (!elementVal) {
                element.after('<p class="error-message">This field is required</p>');
                return true;
            }
        }
        return false;
    },
    depend: function (validationVal, element) {
        var error = false;
        var defaultFieldVal = element.val();
        if (element.attr('type') === 'radio') {
            defaultFieldVal = element.filter(':checked').val();
        }
        $.each(validationVal, function (field, val) {
            var checked = '';
            if (val.type === 'radio') {
                checked = ':checked';
            }
            var fieldVal = $('input[name="' + field + '"]' + checked).val();
            if (typeof fieldVal === 'undefined') {
                if (element.attr('type') === 'radio') {
                    if (typeof defaultFieldVal === 'undefined') {
                        element.parents('.form-groupY').append('<p class="error-message">This field is required</p>');
                        error = true;
                    }
                }
            } else if ($.inArray(fieldVal, val.value) !== -1) {
                if (element.attr('type') === 'radio') {
                    if (typeof defaultFieldVal === 'undefined') {
                        element.parents('.form-groupY').append('<p class="error-message">This field is required</p>');
                        error = true;
                    } else if ($.inArray(defaultFieldVal, val.value) !== -1) {
                        element.parents('.form-groupY').append('<p class="error-message">' + val.message + '</p>');
                        error = true;
                    }
                }
            } else {
                if (element.attr('type') !== 'radio') {
                    if (fieldVal === val.default && !defaultFieldVal) {
                        element.after('<p class="error-message">This field is required</p>');
                        error = true;
                    }
                }
            }
            if (error) {
                return false;
            }
        });
        return error;
    },
    operationalValidation: function (validationVal, element) {
        var error = 0;
        var result = parseFloat(validationVal.defaultValue);
        $.each(validationVal.field, function (index, val) {
            var fieldValue = $(document).find('[name="' + val + '"]').val();
            if (!fieldValue) {
                error = 1;
            }
        });
        if (!element.val()) {
            error = 1;
        }
        if (error === 0) {
            $.each(validationVal.field, function (index, val) {
                var fieldValue = $(document).find('[name="' + val + '"]').val();
                var mathFunction = MathFunctions[validationVal.operator];
                result = mathFunction(result, fieldValue);
            });
            var operandMathFunction = MathFunctions[validationVal.oprand];
            if (operandMathFunction(element.val(), result)) {
                element.after('<p class="error-message">' + validationVal.title + ' should not ' + validationVal.oprand + ' than ' + result + ' ' + validationVal.unit + '</p>');
                return true;
            }
        }
        return false;
    },
    operationalMultyValidation: function (validationVal, element) {
        var error = 0;
        var resultData = false;
        var omdTypeListElement = $(document).find('select[name="omd_type_private_list"]');
        if(element.attr('name') === 'total_advertising_space' && omdTypeListElement.length) {
            var omdTypeSelected = omdTypeListElement.find('option:selected').val();
            if(omdTypeSelected === 'Unipole') {
                return false;
            }
        }
        $.each(validationVal.field, function (index, val) {
            var fieldValue = $(document).find('[name="' + val + '"]').val();
            if (!fieldValue) {
                error = 1;
            }
        });
        if (!element.val()) {
            error = 1;
        }
        if (error === 0) {
            $.each(validationVal.field, function (index, val) {
                var result = parseFloat(validationVal[index].defaultValue);
                if (error === 0) {
                    var fieldValue = $(document).find('[name="' + val + '"]').val();
                    var mathFunction = MathFunctions[validationVal[index].operator];
                    result = mathFunction(result, fieldValue);
                    var operandMathFunction = MathFunctions[validationVal[index].oprand];
                    if (operandMathFunction(element.val(), result)) {
                        element.after('<p class="error-message">' + validationVal[index].title + ' should not ' + validationVal[index].oprand + ' than ' + result + ' ' + validationVal[index].unit + '</p>');
                        error = 1;
                        resultData = true;
                    }
                }
            });
        }
        return resultData;
    },
    customCalculations: function (fieldName, data) {
        var self = this;
        var operation = self[data.operation];
        operation(data, fieldName);
    },
    multiply: function (data, fieldName) {
        var fields = [];
        $.each(data.fields, function (index, val) {
            fields.push('[name="' + val + '"]');
        });
        var selector = fields.join(', ');
        $(document).on('focusout', selector, function () {
            var result = data.defaultValue;
            var error = 0;
            var selectorValue = $(this).val();
            if (!selectorValue) {
                $(document).find('[name="' + fieldName + '"]').val('');
                return false;
            }
            if (!$.isNumeric(selectorValue)) {
                alert("Please enter a Numeric value");
                $(document).find('[name="' + fieldName + '"]').val('');
                $(this).val("");
                return false;
            }
            $.each(data.fields, function (index, val) {
                var fieldValue = $(document).find('[name="' + val + '"]').val();
                if (!fieldValue) {
                    error = 1;
                }
            });
            if (error === 0) {
                $.each(data.fields, function (index, val) {
                    var fieldValue = $('[name="' + val + '"]').val();
                    result = fieldValue * result;
                });
                $(document).find('[name="' + fieldName + '"]').val(result);
            }
        });
    },
    addition: function (data, fieldName) {
        setInterval(function () {
            var sum = parseFloat(data.defaultValue);
            $.each(data.fields, function (index, val) {
                var value = $.trim($(document).find('[name="' + val + '"]').val()) ? parseFloat($(document).find('[name="' + val + '"]').val()) : 0;
                sum = sum + value;
            });
            $(document).find('[name="' + fieldName + '"]').val('');
            if (sum) {
                $(document).find('[name="' + fieldName + '"]').val(sum);
            }
        }, 200);
    },
    bodmas: function (data, fieldName) {
		//console.log('rohitkaushal');
        var fields = [];
        var result = '';
        $.each(data.fields, function (index, val) {
            fields.push('[name="' + val + '"]');
        });
        var selector = fields.join(', ');
        $(document).off('change', selector).on('change', selector, function () {
			//console.log('yogesh');
            if(!$(document).find('[name="building_type[]"]').length) {
                var multiply = 1;
                $.each(data.equation.multiply, function (index, val) {
                    if (val === "defaultValue") {
                        multiply = multiply * data.defaultValue;
                    } else {
                        var field = $.trim($('[name="' + val + '"]').val()) || '0';
                        if (field) {
                            multiply = multiply * parseFloat(field);
                        }
                    }
                });
                if (multiply) {
                    result = multiply;
                }
                $.each(data.equation.percentageDiscount, function (index, val) {
                    var field = $.trim($('[name="' + val + '"]').val());
                    if (field && result) {
                        var discountAmount = result * (parseFloat(field) / 100);
                        result = result - discountAmount;
                    }
                });
                //============= add by rohit kaushal(23-07-2018) ================
                if(data.equation.exempted2percentage){
					var total_facade_field  		= Object.keys(data.equation.exempted2percentage.ON);
					let total_facade_input_name		=	total_facade_field[0];
					let total_facade_input_name_val	=	$('[name="' + total_facade_input_name + '"]').val();
					if(total_facade_input_name_val){
						var multiply_new = 1;
						$.each(data.equation.exempted2percentage.multiply, function (index, val) {
							var field_new = $.trim($('[name="' + val + '"]').val()) || '0';
							console.log(field_new);
							console.log('cccc');
							if (field_new) {
								multiply_new = multiply_new * parseFloat(field_new);
							}
						});
						if(multiply_new){
							var exempted_percentage = ((multiply_new*100)/total_facade_input_name_val);
							console.log(multiply_new);
							console.log(exempted_percentage);
							if(exempted_percentage<=2){
								//$('[name="' + fieldName + '"]').val('0');
								result = '0';
							}
						}
							
					}
				}
                //========== end by rohit kaushal(23-07-2018) ====================
                $('[name="' + fieldName + '"]').val('');
                if(result) {
                    $('[name="' + fieldName + '"]').val(Number(Math.round(result+'e2')+'e-2'));
                }
            }
        });
    },
    dimensionChange: function(data, fieldName){
        var fields = [];
        $.each(data.fields, function (index, val) {
            fields.push('[name="' + val + '"]');
        });
        var selector = fields.join(', ');
        $(document).off('change', selector).on('change', selector, function () {
            $(document).find('input[name="omd_height"]').val('');
            $(document).find('input[name="omd_width"]').val('');
            $(document).find('input[name="total_advertising_space"]').val('');
            var sizeData = $(this).val();
            if(sizeData) {
                var sizeDataArr = sizeData.split('x');
                var height = parseFloat($.trim(sizeDataArr[0]));
                var width = parseFloat($.trim(sizeDataArr[1]));
                var result = height * width;
                $(document).find('input[name="omd_height"]').val(height);
                $(document).find('input[name="omd_width"]').val(width);
                $(document).find('input[name="total_advertising_space"]').val(Number(Math.round(result+'e2')+'e-2'));
                $(document).find('input[name="tenure"]').trigger('change');
            }
        });
    },
    onSelect: function (data, fieldName) {
        var options = data.options;
        var fields = [];
        $.each(data.fields, function (index, val) {
            fields.push('[name="' + val + '"]');
        });
        var selector = fields.join(', ');
        $(document).off('change', selector).on('change', selector, function () {
            $(document).find('[name="' + fieldName + '"]').val('');
            if (typeof options[$(this).val()] !== 'undefined' && !Array.isArray(options[$(this).val()])) {
                $(document).find('[name="' + fieldName + '"]').val(options[$(this).val()]);
                $(document).find('[name="' + fieldName + '"]').focus();
            } else {
                var element = $(document).find('[name="' + fieldName + '"]');
                var label = element.parents('.form-groupY').find('label').html();
                var html = '<option value="">Select ' + label + '</option>';
                if (typeof options[$(this).val()] !== 'undefined') {
                    $.each(options[$(this).val()], function (index, val) {
                        html += '<option value="' + val + '">' + val + '</option>';
                    });
                }
                element.html(html);
            }
        });
    },
    discountCalculate: function (data, fieldName) {
        var fields = [];
        $.each(data.fields, function (index, val) {
            fields.push('[name="' + val + '"]');
        });
        var selector = fields.join(', ');
        $(document).off('keyup', selector).on('keyup', selector, function () {
            var element = $(this);
            var noOfMonths = element.val();
            $(document).find('[name="discount"]').val('');
            if (!isNaN(noOfMonths)) {
                $.each(data.data, function (index, val) {
                    if (typeof data.data[index + 1] !== 'undefined') {
                        var objectKeysMonths = Object.keys(val);
                        var objectKeysMonthsNext = Object.keys(data.data[index + 1]);
                        var discountValue = Object.values(val);
                        if((parseInt(objectKeysMonths[0]) <= parseInt(noOfMonths)) && (parseInt(objectKeysMonthsNext[0]) > parseInt(noOfMonths))) {
                            console.log("Match");
                            $(document).find('[name="discount"]').val(discountValue[0]);
                        }
                    } else {
                        var objectKeysMonths = Object.keys(val);
                        var discountValue = Object.values(val);
                        if((parseInt(objectKeysMonths[0]) <= parseInt(noOfMonths))) {   
                            $(document).find('[name="discount"]').val(discountValue[0]);
                        }
                    }
                });
            }
        });
    }
};
$('body').on('focus', ".datePicker", function () {
    var today = new Date();
    $(this).datepicker({
        minDate: 0,
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        maxDate: new Date(today.getFullYear() + 1, today.getMonth(), today.getDate())
    });
});

$(document).on('keyup', 'input[name="tenure"]', function () {
    if (SubCategory.subCategoryId === "29") {
        $(document).find('#omds_installed_under_one_roof table tbody tr:first-child td .heightData, #omds_installed_within_premises table tbody tr:first-child td .heightData').trigger('keyup');
    }
});

$(document).on('change','select[name="omd_type_private_list"]',function(){
    var element = $(document).find('input[name="total_facade_area"]');
    element.parents('.form-groupY').show();
    var omdTypeVal = $(this).val();
    if(omdTypeVal === 'Unipole') {
        element.val('');
        element.parents('.form-groupY').hide();
    }
});

$(document).on('click','#submitOmdApp',function(e){
    $(document).find('.error-message').remove();
    if(!$(document).find('#agreeOMD').is(':checked')) {
        $(document).find('.termsc').append('<p class="error-message">You must agree to the terms</p>');
        e.preventDefault();
    }
});
