var AppliedBids = {
    id: '',
    appliedBidUrl: '',
    deviceImagePath: '',
    bidDocumentsPath: '',
    deviceImagesKeys: [],
    startLoader: function () {
        $('#loaderLayout').show();
    },
    stopLoader: function () {
        $('#loaderLayout').hide()
    },
    getDetail: function (id, element) {
        var self = this;
        self.id = id;
        self.getAppiliedBid();
    },
    getAppiliedBid: function () {
        var self = this;
        $.ajax({
            url: self.appliedBidUrl,
            dataType: 'JSON',
            type: 'POST',
            data: {id: self.id},
            beforeSend: function (xhr) {
                self.startLoader();
            },
            success: function (response) {
                self.stopLoader();
                if (!response.error) {
                    self.getHtml(response);
                }
            },
            error: function (result) {
                self.stopLoader();
                alert("Something went wrong on Server, Please try again.");
            }
        });
    },
    getHtml: function (data) {
        var self = this;
        self.deviceImagesKeys = data.deviceImagesKeys;
        var bidDocuments = data.bidDocuments;
        var deviceData = data.deviceData;
        var userData = data.userData;
        self.getDeviceHtml(deviceData);
        self.getUserHtml(userData);
        self.getBidDocumentsHtml(bidDocuments);
    },
    getDeviceHtml: function (data) {
        var self = this;
        var deviceDocHtml = '';
        var html = '<div class="form-group clearfix">';
        html += '<label class="col-md-6">Device Name</label>';
        html += '<div class="col-md-6">';
        html += '<span>' + data.device_name + '</span>';
        html += '</div>';
        html += '</div>';
        if (data.company_code === "") {
            html += '<div class="form-group clearfix">';
            html += '<label class="col-md-6">Added by</label>';
            html += '<div class="col-md-6">';
            html += '<span>Admin</span>';
            html += '</div>';
            html += '</div>';
        }
        html += '<div class="form-group clearfix">';
        html += '<label class="col-md-6">Status</label>';
        html += '<div class="col-md-6">';
        html += '<span>' + data.status + '</span>';
        html += '</div>';
        html += '</div>';
        html += '<div class="form-group clearfix">';
        html += '<label class="col-md-6">Typology</label>';
        html += '<div class="col-md-6">';
        html += '<span>' + data.typology + '</span>';
        html += '</div>';
        html += '</div>';
        html += '<div class="form-group clearfix">';
        html += '<label class="col-md-6">Sub Category</label>';
        html += '<div class="col-md-6">';
        html += '<span>' + data.sub_category + '</span>';
        html += '</div>';
        html += '</div>';
        $.each(data.fields, function (index, val) {
            if ($.inArray(val.field, self.deviceImagesKeys) === -1) {
                var fielddata = $.grep(data.fieldsData, function (obj) {
                    return obj.field_key === val.field;
                });
                html += '<div class="form-group clearfix">';
                html += '<label class="col-md-6">' + fielddata[0].lable + '</label>';
                html += '<div class="col-md-6">';
                html += '<span>' + val.value + '</span>';
                html += '</div>';
                html += '</div>';
            }
        });
        $.each(data.fields, function (index, val) {
            var extRegex = /(?:\.([^.]+))?$/;
            if ($.inArray(val.field, self.deviceImagesKeys) !== -1) {
                var fielddata = $.grep(data.fieldsData, function (obj) {
                    return obj.field_key === val.field;
                });
                var fileExt = extRegex.exec(val.value);
                val.value = self.deviceImagePath + val.value;
                var ValidImageTypes = ["gif", "jpeg", "png", "jpg"];
                var imageHTML = '<a href="' + val.value + '" download="' + fielddata[0].lable + '.' + fileExt[1] + '" title="Download ' + fielddata[0].lable + '"><img src="' + val.value + '" /></a>';
                if ($.inArray(fileExt[1], ValidImageTypes) < 0) {
                    imageHTML = '<a href="' + val.value + '" download="' + fielddata[0].lable + '.' + fileExt[1] + '" class="downloadDocument" title="Download ' + fielddata[0].lable + '"><i class="glyphicon glyphicon-save"></i></a>';
                }
                deviceDocHtml += '<div class="col-md-3">';
                deviceDocHtml += '<label class="col-md-6">' + fielddata[0].lable + '</label>';
                deviceDocHtml += '<div class="col-md-6">';
                deviceDocHtml += '<span class="siteimg">' + imageHTML + '</span>';
                deviceDocHtml += '</div>';
                deviceDocHtml += '</div>';
            }
        });
        $('#siteImages').html(deviceDocHtml);
        $('#deviceinfo').html(html);
    },
    getUserHtml: function (data) {
        var html = '<div class="form-group clearfix">';
        html += '<label class="col-md-6">Name</label>';
        html += '<div class="col-md-6">';
        html += '<span>' + data.display_name + '</span>';
        html += '</div>';
        html += '</div>';
        html += '<div class="form-group clearfix">';
        html += '<label class="col-md-6">Email</label>';
        html += '<div class="col-md-6">';
        html += '<span>' + data.email + '</span>';
        html += '</div>';
        html += '</div>';
        html += '<div class="form-group clearfix">';
        html += '<label class="col-md-6">Mobile</label>';
        html += '<div class="col-md-6">';
        html += '<span>' + data.mobile + '</span>';
        html += '</div>';
        html += '</div>';
        html += '<div class="form-group clearfix">';
        html += '<label class="col-md-6">Company Code</label>';
        html += '<div class="col-md-6">';
        html += '<span>' + data.company_code + '</span>';
        html += '</div>';
        html += '</div>';
        html += '<div class="form-group clearfix">';
        html += '<label class="col-md-6">Company Name</label>';
        html += '<div class="col-md-6">';
        html += '<span>' + data.company_name + '</span>';
        html += '</div>';
        html += '</div>';
        html += '<div class="form-group clearfix">';
        html += '<label class="col-md-6">Representative</label>';
        html += '<div class="col-md-6">';
        html += '<span>' + data.representative + '</span>';
        html += '</div>';
        html += '</div>';
        html += '<div class="form-group clearfix">';
        html += '<label class="col-md-6">Pan Number</label>';
        html += '<div class="col-md-6">';
        html += '<span>' + data.pan_number + '</span>';
        html += '</div>';
        html += '</div>';
        $('#appliedByInfo').html(html);
    },
    getBidDocumentsHtml: function (data) {
        var html = '';
        var extRegex = /(?:\.([^.]+))?$/;
        $.each(data, function (index, val) {
            var fileExt = extRegex.exec(val.doc_file);
            var ValidImageTypes = ["gif", "jpeg", "png", "jpg"];
            var imageHTML = '<a href="' + val.doc_file + '" download="' + val.doc_name + '.' + fileExt[1] + '" title="Download ' + val.doc_name + '"><img src="' + val.doc_file + '" /></a>';
            if ($.inArray(fileExt[1], ValidImageTypes) < 0) {
                imageHTML = '<a href="' + val.doc_file + '" download="' + val.doc_name + '.' + fileExt[1] + '" class="downloadDocument" title="Download ' + val.doc_name + '"><i class="glyphicon glyphicon-save"></i></a>';
            }
            html += '<div class="col-md-3">';
            html += '<label class="col-md-6">' + val.doc_name + '</label>';
            html += '<div class="col-md-6">';
            html += '<span class="siteimg">' + imageHTML + '</span>';
            html += '</div>';
            html += '</div>';
        });
    }
};