var Guest = {
    rquestUrl: '',
    validateUrl: '',
    resendCounter: 59,
    interval: '',
    requestOtp: function (element) {
        var self = this;
        var mobile = element.parents('form').find('input[name="user_name"]').val();
        $.ajax({
            url: self.rquestUrl,
            type: 'POST',
            data: {user_name: mobile},
            beforeSend: function (xhr) {
                self.resendCounter = 59;
                self.showLoader();
                $(document).find('.error-message, .labelMobile').remove();
            },
            dataType: 'JSON',
            success: function (resposne) {
                if(resposne.error) {
                    $.each(resposne.data, function(index, val){
                        element.parents('form').find('input[name="'+ index +'"]').after('<p class="error-message">'+ val[Object.keys(val)[0]] +'</p>');
                    });
                    return false;
                }
                $('input[name="user_name"]').parents('.form-group').hide();
                $('input[name="password"]').parents('.form-group').show();
                $('input[name="password"]').parents('.form-group').before('<div class="form-group labelMobile" style=""><label for="mobile" style="float: left;width: 100%;">Mobile Number<a href="javascript:void(0)" style="float: right;" onclick="Guest.changeNumber(this)">Change</a></label><input class="form-control" id="mobileLabel" type="text" value="'+ mobile +'" disabled="disabled"></div>');
                $('#optRequest').hide();
                $('#validateOtp, #resendOtp').show();
                self.resendTimer();
            },
            error: function (result) {
                swal("Error!","Something went wrong on server. Please try again");
                self.hideLoader();
            },
            complete: function (jqXHR, textStatus) {
                self.hideLoader();
            }
        });
    },
    validateOtp: function(element) {
        var self = this;
        var mobile = element.parents('form').find('input[name="user_name"]').val();
        var otp = element.parents('form').find('input[name="password"]').val();
        $.ajax({
            url: self.validateUrl,
            type: 'POST',
            data: { user_name: mobile, password: otp },
            dataType: 'JSON',
            beforeSend: function (xhr) {
                self.showLoader();
                $(document).find('.error-message').remove();
            },
            success: function(resposne) {
                if(resposne.error) {
                    $.each(resposne.data, function(index, val){
                        element.parents('form').find('input[name="'+ index +'"]').after('<p class="error-message">'+ val[Object.keys(val)[0]] +'</p>');
                    });
                    self.hideLoader();
                    return false;
                }
                window.location = resposne.data;
            },
            error: function (result) {
                self.hideLoader();
            },
        });
        
    },
    showLoader: function () {
        var documentHeight = $(document).height();
        $('#loaderLayout span').css({height: documentHeight + 'px'});
        $('#loaderLayout').show();
    },
    hideLoader: function () {
        $('#loaderLayout').hide();
    },
    changeNumber: function(element) {
        $(element).parents('.form-group').remove();
        $('input[name="user_name"]').parents('.form-group').show();
        $('input[name="password"]').parents('.form-group').hide();
        $('#optRequest').show();
        $('#validateOtp, #resendOtp').hide();
        clearInterval(this.interval);
        $('#resendOtp').html('Resend OTP').attr('disabled');
    },
    resendTimer: function() {
        var self = this;
        var element = $('#resendOtp');
        element.attr('disabled','disabled');
        self.interval = setInterval(function(){
            element.html('Resend OTP 00:'+ self.twoDigits(self.resendCounter));
            self.resendCounter--;
            if(!self.resendCounter) {
                clearInterval(self.interval);
                element.html('Resend OTP').removeAttr('disabled');
            }
        }, 1000);
    },
    twoDigits: function(number) {
        return number > 9 ? "" + number: "0" + number;
    }
};
$('#optRequest, #resendOtp').on('click', function () {
    Guest.requestOtp($(this));
});

$('#validateOtp').on('click',function(){
    Guest.validateOtp($(this));
});