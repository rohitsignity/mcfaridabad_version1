var Payment = {
    url: '',
    installmentUrl: '',
    payNow: function (element, id) {
        var self = this;
        var fees = $(element).attr('data-fees');
        var amount = fees.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        swal({
            title: 'Do you want to pay the final amount now ?',
            text: "Amount you need to Pay is Rs. "+amount,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Pay Now'
        }).then((result) => {
            window.location = self.url + '/' + id;
        });
    },
    payInstallment: function(element, id) {
        var self = this;
        var fees = $(element).attr('data-fees');
        var amount = fees.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        var start = $(element).attr('data-start');
        var end = $(element).attr('data-end');
        swal({
            title: 'Do you want to pay the installment now ?',
            html: "Amount you need to Pay is <b>Rs. "+amount + "</b> <br>"+ start + " to "+ end,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Pay Now'
        }).then((result) => {
            window.location = self.installmentUrl + '/' + id;
        });
    }
};